{
  /*const listApi = [  `${process.env.API_BE}/api/auth/change-password`,
  `${process.env.API_BE}/api/auth/profile`,
  `${process.env.API_BE}/api/auth/public/student`,
  `${process.env.API_BE}/api/auth/pubnub-token`,
  `${process.env.API_BE}/api/auth/request-reset-password`,
  `${process.env.API_BE}/api/auth/reset-password`,
  `${process.env.API_BE}/api/auth/update-profile`,
  `${process.env.API_BE}/api/city?query=${searchQuery}&limit=50&page=${page}`,
  `${process.env.API_BE}/api/class-assignment/${assignmentId}/submission`,
  `${process.env.API_BE}/api/class-assignment/${assignmentId}`,
  `${process.env.API_BE}/api/class-assignment`,
  `${process.env.API_BE}/api/class-attendance/${metadataId}/attend`,
  `${process.env.API_BE}/api/class-exam/${examId}/grade`,
  `${process.env.API_BE}/api/class-exam/${examId}/submission`,
  `${process.env.API_BE}/api/class-exam/${type}`,
  `${process.env.API_BE}/api/class-material/${delmode}`,
  `${process.env.API_BE}/api/class-material/${edit}`,
  `${process.env.API_BE}/api/class-material/${materialId}`,
  `${process.env.API_BE}/api/class-material`,
  `${process.env.API_BE}/api/class-quiz/${assignmentId}/submission`,
  `${process.env.API_BE}/api/class-quiz/${quizId}/submission`,
  `${process.env.API_BE}/api/class-quiz/${quizId}`,
  `${process.env.API_BE}/api/class-quiz/${tempQuizId}/${id}/grade`,
  `${process.env.API_BE}/api/class-quiz`,
  `${process.env.API_BE}/api/class/${classId}/activate-next-session`,
  `${process.env.API_BE}/api/class/${classId}/assignment`,
  `${process.env.API_BE}/api/class/${classId}/attendance?withUserList=true`,
  `${process.env.API_BE}/api/class/${classId}/material`,
  `${process.env.API_BE}/api/class/${classId}/members`,
  `${process.env.API_BE}/api/class/${classId}/quiz`,
  `${process.env.API_BE}/api/class/${classId}/zoom-url`,
  `${process.env.API_BE}/api/class/${classId}`,
  `${process.env.API_BE}/api/class/join-paid-class`,
  `${process.env.API_BE}/api/class/join`,
  `${process.env.API_BE}/api/class`,
  `${process.env.API_BE}/api/feeds/${postId}/comment/${commentId}`,
  `${process.env.API_BE}/api/feeds/${postId}/comment?limit=10&page=1`,
  `${process.env.API_BE}/api/feeds/${postId}/comment`,
  `${process.env.API_BE}/api/feeds/${postId}/like`,
  `${process.env.API_BE}/api/feeds/favorites/${postId}`,
  `${process.env.API_BE}/api/feeds/post/simple`,
  `${process.env.API_BE}/api/feeds/story?limit=50&page=1`,
  `${process.env.API_BE}/api/feeds?limit=10&page=${pageParam}`,
  `${process.env.API_BE}/api/finance/invoice/detail/${id}`,
  `${process.env.API_BE}/api/finance/invoice/detail/${invoiceIdentifier}`,
  `${process.env.API_BE}/api/finance/invoice/detail/${invoice}`,
  `${process.env.API_BE}/api/finance/notification`,
  `${process.env.API_BE}/api/finance/pay/cancel/${transactionIdentifier}`,
  `${process.env.API_BE}/api/finance/pay/preview`,
  `${process.env.API_BE}/api/finance/pay`,
  `${process.env.API_BE}/api/finance/payment-method?limit=8&page=1`,
  `${process.env.API_BE}/api/friend/add`,
  `${process.env.API_BE}/api/friend/block?limit=10&page=1`,
  `${process.env.API_BE}/api/friend/block`,
  `${process.env.API_BE}/api/friend/remove`,
  `${process.env.API_BE}/api/friend/unblock`,
  `${process.env.API_BE}/api/friend?limit=10&page=1`,
  `${process.env.API_BE}/api/friend?query=${query}&limit=100&page=1`,
  `${process.env.API_BE}/api/group-chat/${groupId}/admin/${userId}`,
  `${process.env.API_BE}/api/group-chat/${groupId}/leave`,
  `${process.env.API_BE}/api/group-chat/${groupId}/member/${userId}`,
  `${process.env.API_BE}/api/group-chat/${id}`,
  `${process.env.API_BE}/api/group-chat/${userChatAktif?.groupId}`,
  `${process.env.API_BE}/api/group-chat/invite`,
  `${process.env.API_BE}/api/group-chat`,
  `${process.env.API_BE}/api/material/${profileData?.data?.id}`,
  `${process.env.API_BE}/api/material/${user.id}`,
  `${process.env.API_BE}/api/pubnub/metadata-detail`,
  `${process.env.API_BE}/api/storage`,
  `${process.env.API_BE}/api/university/search/univ?query=${searchQuery}&limit=50&page=${page}`,
  `${process.env.API_BE}/api/user/class-pagination?by=assignment&limit=30&page=${pageParam}`,
  `${process.env.API_BE}/api/user/class-pagination?by=quiz&limit=30&page=${pageParam}`,
  `${process.env.API_BE}/api/user/favorites-feeds?limit=10&page=1`,
];
*/
}
{/*  `${API_BE}/api/class-assignment/${assignmentId}/submission`,
  `${API_BE}/api/class-assignment`,
  `${API_BE}/api/class-quiz`,
  `${API_BE}/api/class-assignment/${assignmentId}`,
  `${API_BE}/api/class-exam/${examId}/grade`,
  `${API_BE}/api/class-exam/${examId}/submission`,
  `${API_BE}/api/class-exam/${type}`,
  `${API_BE}/api/class-material/${delmode}`,
  `${API_BE}/api/class-material/${edit}`,
  `${API_BE}/api/class-material/${materialId}`,
  `${API_BE}/api/class-quiz/${assignmentId}/submission`,
  `${API_BE}/api/class-quiz/${quizId}/submission`,
  `${API_BE}/api/class-quiz/${quizId}`,
  `${API_BE}/api/class-quiz/${tempQuizId}/${id}/grade`,
  `${API_BE}/api/class/${classId}/activate-next-session`,
  `${API_BE}/api/class/${classId}/assignment`,
  `${API_BE}/api/class/${classId}/attendance?withUserList=true`,
  `${API_BE}/api/class/${classId}/material`,
  `${API_BE}/api/class/${classId}/members`,
  `${API_BE}/api/class/${classId}/quiz`,
  `${API_BE}/api/class/${classId}/zoom-url`,
  `${API_BE}/api/class/${classId}`,
  `${API_BE}/api/class/join-paid-class`,
  `${API_BE}/api/class/join`,
  `${API_BE}/api/feeds/${postId}/comment/${commentId}`,
  `${API_BE}/api/feeds/${postId}/comment?limit=10&page=1`,
  `${API_BE}/api/feeds/${postId}/comment`,
  `${API_BE}/api/feeds/${postId}/like`,
  `${API_BE}/api/feeds/favorites/${postId}`,
  `${API_BE}/api/feeds/post/simple`,
  `${API_BE}/api/feeds?limit=10&page=${pageParam}`,
  `${API_BE}/api/finance/invoice/detail/${id}`,
  `${API_BE}/api/finance/invoice/detail/${invoiceIdentifier}`,
  `${API_BE}/api/finance/invoice/detail/${invoice}`,
  `${API_BE}/api/finance/pay/cancel/${transactionIdentifier}`,
  `${API_BE}/api/finance/pay/preview`,
  `${API_BE}/api/finance/pay`,
  `${API_BE}/api/finance/payment-method?limit=8&page=1`,
  `${API_BE}/api/friend/add`,
  `${API_BE}/api/friend/block?limit=10&page=1`,
  `${API_BE}/api/friend/remove`,
  `${API_BE}/api/friend/unblock`,
  `${API_BE}/api/friend?query=${query}&limit=100&page=1`,
  `${API_BE}/api/group-chat/${groupId}/admin/${userId}`,
  `${API_BE}/api/group-chat/${groupId}/leave`,
  `${API_BE}/api/group-chat/${groupId}/member/${userId}`,
  `${API_BE}/api/group-chat/${id}`,
  `${API_BE}/api/group-chat/${userChatAktif?.groupId}`,
  `${API_BE}/api/group-chat/invite`,
  `${API_BE}/api/group-chat`,
  `${API_BE}/api/material/${profileData?.data?.id}`,
  `${API_BE}/api/material/${user.id}`,
  `${API_BE}/api/pubnub/metadata-detail`,
  `${API_BE}/api/storage`,*/}

const axios = require("axios");
const API_BE = "https://kc-develop.dev-backend.kelas-digital.id";
const pageParam = 1;

// Daftar API
const apiEndpoints = [
  `${API_BE}/api/auth/profile`,
  `${API_BE}/api/university/search/univ`,
  `${API_BE}/api/auth/pubnub-token`,
  `${API_BE}/api/city?limit=50&pageParam=${pageParam}`,
  `${API_BE}/api/feeds/story?limit=50&page=1`,
  `${API_BE}/api/user/favorites-feeds?limit=10&page=1`,
  `${API_BE}/api/finance/notification`,
  `${API_BE}/api/user/class-pagination?by=material&limit=30&page=${pageParam}`,
  `${API_BE}/api/user/class-pagination?by=assignment&limit=30&page=${pageParam}`,
  `${API_BE}/api/user/class-pagination?by=quiz&limit=30&page=${pageParam}`,
  `${API_BE}/api/friend?limit=10&page=1`,
  `${API_BE}/api/friend/block`,
  `${API_BE}/api/class-material`,

];

const token = `eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJpZCI6IjM1NzY1IiwiZW1haWwiOiJ5ZWRpa3Vybmlhd2FuMDIxQGdtYWlsLmNvbSIsImZ1bGxOYW1lIjoiWWVkaV9LdXJuaWF3YW56IiwicGhvbmUiOiIrNjItODU4MTM0MjEwNDAiLCJpc1ZlcmlmaWVkQWNjb3VudCI6ZmFsc2UsInByb3ZpZGVyIjoibG9jYWwiLCJ1dWlkIjoiNzNlNDI1MmUtZmEzOS00OTA4LWI4ODItNzAyNDYyNGI0MTYxIiwidXNlcm5hbWUiOiJHZWFyNTYiLCJsaW5rZWRXaXRoR29vZ2xlIjpmYWxzZSwiaWF0IjoxNzE2OTY4NzcxLCJleHAiOjE3MTcwNTUxNzF9.rocQO63OS7ZNnnI0FjjaO0Y6D1WsExqfOstpRhevpgY`;

async function checkEndpoint(endpoint, token) {
  try {
    const response = await axios.get(endpoint, {
      headers: {
        "Content-Type": "application/json",
        Authorization: `Bearer ${token}`,
      },
    });
    return response.status === 200 || response.status === 201;
  } catch (error) {
    return false;
  }
}

describe("API Endpoint Tests", () => {
  apiEndpoints.forEach((endpoint) => {
    const path = new URL(endpoint).pathname;
    test(`GET ${path} should return status OK`, async () => {
      const isAvailable = await checkEndpoint(endpoint, token);
      expect(isAvailable).toBe(true);
    });
  });
});
