/** @type {import('next').NextConfig} */
const nextConfig = {
    output: "standalone",
  eslint: {
    // Warning: This allows production builds to successfully complete even if
    // your project has ESLint errors.
    ignoreDuringBuilds: true,
  },
  typescript: {
    // !! WARN !!
    // Dangerously allow production builds to successfully complete even if
    // your project has type errors.
    // !! WARN !!
    ignoreBuildErrors: true,
  },
  reactStrictMode: false,
  i18n: {
    locales: ["en", "id"],
    defaultLocale: "id",
  },
  images: {
    remotePatterns: [
      {
        protocol: "https",
        hostname: "storage.googleapis.com",
        port: "",
        pathname: "/**",
      },
      {
        protocol: "https",
        hostname: "res.cloudinary.com",
        port: "",
        pathname: "/**",
      },
      {
        protocol: "https",
        hostname: "pertaniansehat.com",
        port: "",
        pathname: "/**",
      },
    ],
  },
  env: {
    API_BE: process.env.API_BE,
    API_LINK: getEnvironmentName(process.env.API_URL),
  },
  async rewrites() {
    return {
      beforeFiles: [
        {
          source: "/be/api/:path*",
          // destination: 'http://localhost:3002/api/:path*'
          // destination: "https://be.kelas-digital.id/api/:path*",
          destination: process.env.API_URL + "/api/:path*",
        },
      ],
      afterFiles: [],
      fallback: [],
    };
  },
};

function getEnvironmentName(apiUrl) {
    if (apiUrl === "https://kc-develop.dev-backend.kelas-digital.id") {
        return "Development";
    } else if (apiUrl === "https://be.kelas-digital.id") {
        return "Production";
    } else if (apiUrl === "https://staging-be.kelas-digital.id") {
        return "Staging";
    } else {
        return "Unknown";
    }
}

module.exports = nextConfig;
