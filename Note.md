## Daftar Halaman dan Fiturnya

* [x] Beranda
  * [x] Like / Dislike Postingan
  * [x] Komentar Postingan
  * [x] Balas Komentar (nested comment)
  * [x] Save Postingan
  * [x] Cari Pengguna dan Kelas
  * [x] Tambah teman dari Postingan
  * [x] Tambah teman dari Pencarian (Kadang Bug karena data dari BE)
* [x] Profile
  * [ ] Masih progress (Belum tau data API apa saya yang di tampilkan)
* [x] Detail Profile
  * [x] Edit Foto Profile
  * [x] Edit Data Profile (Hanya untuk user Publik)
  * [ ] Edit Data Profile (Mahasiswa dan Dosen belum di test karena hanya punya akun
    publik)
  * [ ] Akun Privasi (belum tau trigger APInya)
  * [ ] Hapus akun
  * [ ] Link / Unlink Akun Google
* [x] Buat Kelas
  * [x] Sudah jalan semua sesuai dokumentasi API (kurang Fitur Repeated)
  * [ ] Ujian (masih nunggu data API yang sesuai)
* [x] Buat Post
  * [x] Buat Postingan dengan gambar dan text
* [x] Kelas
  * [ ] Responsive (Masih menemukan Bug screen \< 400px)
  * [x] Cari Pengguna dan Daftar Kelas
  * [x] Hapus chat (bisa multiple)
  * [x] Salin chat (bisa multiple hanya text)
  * [x] Hapus Kelas (Api Erorr)
  * [x] Chat Kontak
    * [x] Bisa kirim text file dan hapus pesan
  * [x] Chat Group
    * [x] Bisa kirim text file dan hapus pesan
  * [x] Chat Kelas
    * [x] Bisa kirim text file
    * [x] Upload Tugas , Materi , Kuis , Ujian
    * [x] Absensi (**Error return data API**)
    * [x] Filter chat dengan dropdown

### Note

> Semua halaman di render menggunakan Server Side Rendering (Kecuali halaman kelas).
