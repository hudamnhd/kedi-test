import downloadapp from "@/assets/donwloadapp.png";
import appstore from "@/assets/appstore.svg";
import appgalery from "@/assets/appgallery.svg";
import appgoogle from "@/assets/appgoogle.svg";
import logoLogin from "@/assets/logo_login.png";
import type_doc from "@/assets/filetype/doc.png";
import type_jpg from "@/assets/filetype/jpg.png";
import type_pdf from "@/assets/filetype/pdf.png";
import type_png from "@/assets/filetype/png.png";
import type_ppt from "@/assets/filetype/ppt.png";
import logo_kedi_med from "@/assets/logo_kedi.png";
import logo_kedi_white from "@/assets/logo_kedi_white.png";
import help_center from "@/assets/help-center.png";

export {
  downloadapp,
  appstore,
  appgalery,
  appgoogle,
  logoLogin,
  type_doc,
  type_jpg,
  type_pdf,
  type_png,
  type_ppt,
  logo_kedi_med,
  logo_kedi_white,
  help_center,
};
