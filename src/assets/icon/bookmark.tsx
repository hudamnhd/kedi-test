export const Bookmark = (props: {
  size?: number;
  stroke?: string; // recently not used
}) => {
  return (
    <svg
      width={props.size ?? 18}
      height={props.size ?? 18}
      viewBox="0 0 32 32"
      fill="currentColor"
      xmlns="http://www.w3.org/2000/svg"
    >
      <g clip-path="url(#clip0_1139_284)">
        <path
          d="M22.6667 4H9.33335C7.86669 4 6.66669 5.2 6.66669 6.66667V28L16 24L25.3334 28V6.66667C25.3334 5.2 24.1334 4 22.6667 4ZM22.6667 24L16 21.0933L9.33335 24V6.66667H22.6667V24Z"
          fill="currentColor"
        />
      </g>
      <defs>
        <clipPath id="clip0_1139_284">
          <rect width="32" height="32" fill="white" />
        </clipPath>
      </defs>
    </svg>
  );
};
