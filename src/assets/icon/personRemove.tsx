export const PersonRemove = (props: {
  size?: number;
  stroke?: string; // recently not used
}) => {
  return (
    <svg
      width={props.size ?? 18}
      height={props.size ?? 18}
      viewBox="0 0 32 32"
      fill="currentColor"
      xmlns="http://www.w3.org/2000/svg"
    >
      <g clip-path="url(#clip0_31_2783)">
        <path
          d="M18.6667 10.6663C18.6667 7.71967 16.28 5.33301 13.3333 5.33301C10.3867 5.33301 8 7.71967 8 10.6663C8 13.613 10.3867 15.9997 13.3333 15.9997C16.28 15.9997 18.6667 13.613 18.6667 10.6663ZM16 10.6663C16 12.133 14.8 13.333 13.3333 13.333C11.8667 13.333 10.6667 12.133 10.6667 10.6663C10.6667 9.19967 11.8667 7.99967 13.3333 7.99967C14.8 7.99967 16 9.19967 16 10.6663Z"
          fill="currentColor"
        />
        <path
          d="M2.66669 23.9993V26.666H24V23.9993C24 20.4527 16.8934 18.666 13.3334 18.666C9.77335 18.666 2.66669 20.4527 2.66669 23.9993ZM5.33335 23.9993C5.60002 23.0527 9.73335 21.3327 13.3334 21.3327C16.92 21.3327 21.0267 23.0393 21.3334 23.9993H5.33335Z"
          fill="currentColor"
        />
        <path
          d="M30.6667 13.333H22.6667V15.9997H30.6667V13.333Z"
          fill="currentColor"
        />
      </g>
      <defs>
        <clipPath id="clip0_31_2783">
          <rect width="32" height="32" fill="white" />
        </clipPath>
      </defs>
    </svg>
  );
};
