export const Disturb = (props: {
  size?: number;
  stroke?: string; // recently not used
}) => {
  return (
    <svg
      width={props.size ?? 18}
      height={props.size ?? 18}
      viewBox="0 0 32 32"
      fill="currentColor"
      xmlns="http://www.w3.org/2000/svg"
    >
      <g clip-path="url(#clip0_1139_1165)">
        <path
          d="M16 2.66699C8.66669 2.66699 2.66669 8.66699 2.66669 16.0003C2.66669 23.3337 8.66669 29.3337 16 29.3337C23.3334 29.3337 29.3334 23.3337 29.3334 16.0003C29.3334 8.66699 23.3334 2.66699 16 2.66699ZM5.33335 16.0003C5.33335 10.1337 10.1334 5.33366 16 5.33366C18.4 5.33366 20.6667 6.13366 22.5334 7.60033L7.60002 22.5337C6.13335 20.667 5.33335 18.4003 5.33335 16.0003ZM16 26.667C13.6 26.667 11.3334 25.867 9.46669 24.4003L24.4 9.46699C25.8667 11.3337 26.6667 13.6003 26.6667 16.0003C26.6667 21.867 21.8667 26.667 16 26.667Z"
          fill="currentColor"
        />
      </g>
      <defs>
        <clipPath id="clip0_1139_1165">
          <rect width="32" height="32" fill="white" />
        </clipPath>
      </defs>
    </svg>
  );
};
