export const IconIdn = (props: {
  size?: number;
  stroke?: string; // recently not used
}) => {
  return (
    <svg
      width={60}
      height={60}
      viewBox="0 0 80 80"
      fill="none"
      xmlns="http://www.w3.org/2000/svg"
      xmlnsXlink="http://www.w3.org/1999/xlink"
    >
      <mask
        id="mask0_31_3242"
        style={{ maskType: "alpha" }}
        maskUnits="userSpaceOnUse"
        x={0}
        y={0}
        width={80}
        height={80}
      >
        <rect width={80} height={80} rx={20} fill="#6E00CE" />
      </mask>
      <g mask="url(#mask0_31_3242)">
        <rect
          x="-5.71436"
          width="123.429"
          height="82.2857"
          fill="url(#pattern0)"
        />
      </g>
      <defs>
        <pattern
          id="pattern0"
          patternContentUnits="objectBoundingBox"
          width={1}
          height={1}
        >
          <use
            xlinkHref="#image0_31_3242"
            transform="matrix(0.00364299 0 0 0.00546448 -0.000910731 0)"
          />
        </pattern>
        <image
          id="image0_31_3242"
          width={275}
          height={183}
          xlinkHref="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAARMAAAC3CAMAAAAGjUrGAAAADFBMVEX////tHCTsAA32paeadC+7AAAA20lEQVR4nO3QsRGAQAzAsAD770wf139QSBP4PBfbfB3wQ56UJ+VJeVKelCflSXlSnpQn5Ul5Up6UJ+VJeVKelCflSXlSnpQn5Ul5Up6UJ+VJeVKelCflSXlSnpQn5Ul5Up6UJ+VJeVKelCflSXlSnpQn5Ul5Up6UJ+VJeVKelCflSXlSnpQn5Ul5Up6UJ+VJeVKelCflSXlSnpQn5Ul5Up6UJ+VJeVKelCflSXlSnpQn5Ul5Up6UJ+VJeVJzs83DNgAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAABzzAnoGZg5GBxcoAAAAAElFTkSuQmCC"
        />
      </defs>
    </svg>
  );
};
