export const Telegram = (props: {
  size?: number;
  stroke?: string; // recently not used
}) => {
  return (
    <svg
      width={24}
      height={24}
      viewBox="0 0 24 24"
      fill="none"
      xmlns="http://www.w3.org/2000/svg"
      xmlnsXlink="http://www.w3.org/1999/xlink"
    >
      <rect width={24} height={24} fill="url(#pattern0)" />
      <defs>
        <pattern
          id="pattern0"
          patternContentUnits="objectBoundingBox"
          width={1}
          height={1}
        >
          <use
            xlinkHref="#image0_938_6441"
            transform="translate(-0.00342466) scale(0.00684932)"
          />
        </pattern>
        <image
          id="image0_938_6441"
          width={147}
          height={146}
          xlinkHref="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAJMAAACSCAYAAABBufiwAAAACXBIWXMAABcRAAAXEQHKJvM/AAAL8klEQVR4nO2d/3FbNxLH8W7uf6sDKxWIrMB0BdFVYKoCMhVEruD4KghVQagKQlZAsQOxgpgVMIO7L5LnZ4lYALv4RXxmOJ6xZRLi+2KxWCx2u/P5rBoNDv7VvsUGF01MDTaamBpsNDE12GhiarDRxNRgo4mpwUYTU4ONJqYGG01MDTaamBpsNDE12Ph3+yqVWnT7W6WUft0opSajf57g778ppV5G//Zi/r4/T79FHHKWXF3WwKLbz5RSM4hEC+iO8e13SqlXiEwLbMv43tlTvZgW3V6L5h4vTuFQ0QLbKKW2/Xk6tmxVUaWYFt3+fiCgDxkMyXCEsNY1CqsaMcHvmeP1MYMh2dDCWkFYVfhbxYsJPtBSKfVzBsPx5Ukp9difp69lDv//FCsmiOhRKfUpg+Fw8aytVamOe3FiqlREY4q0VMWICT6RFtEX4Y86YntveMXLxJsMtxF8sx6iKsKnKkJMi26/hJA4d2YHEw8axIWcH9og4Dkb/MkpspPeVPTn6YbxPUXIWkx4UBum+JDZlm8R8xGb7Ytuf4OwxIwxPPEMUWVrpbIVE5M1yiKug8DpkkFYWVup7MSEWb0O3Oo/Q0DZfemLbm9iYSEbiL4/T5eMw2IhKzFhBq8DlrVidkEDa+W7odDHNPc5LXvZiAlb/o3nMlBs0A9+4crTEh8hqCyOZrIQE0z/bx7/dQcfoujIsfpnMq08rLL2o2Y5CCq5mDyFpGfksoTtsisBG4+H/jxdpxx7UjF5CqmoQJ4PWPrWHk56UkElE5OHkIoJ3nEBK/Vfx7dLJqgkYvIQ0gGOZvG+kSvY9W0co+qfUxwWR79Q4CGkJziYVyckDRzrCSYUlQ1EGJWolgm/4NbBufzan6ePwsMqhkW3XzvEpbRbMIk5CaOJCZHtVwchJd+d5IijoA6w6lE2KzGXOReL1IT0Dv15OsfST+EOsasoRBHTotu7BOOakCw4CuoL/FRxxJc53BT5nfjjTUgOOCx5UaLkopZpkAFA4akJyQ1YqGfCf/rg8By8kV7m1kQ/6RlfTMOdOTFscLfo9qI7YzEx4eCSchJ+xBfS8AA7tTmWMhu/4qhGBEnLRDWrWeXklAh8IeqEFFvuRMQEc0oJ//9S+/37WODMsid83CesGuywiwlONyWldNefp9FiIFfCI9wGGyLWScIyLQlO96n5SfwM/CcbHyViT6xignP3K+FHV9d6cCsNsgUoAU32nR23ZaKo/dgOb8VZEnZ37NaJTUwOvlJ2V3RqA8sdxR9lndScNS3nBF9pd02ZkhQGle0MK6ZQyYrgv2rrNONKpOMUE8XitOWNVpgs+HvSgsQBu82HXSKjIxiWg17ELf6w/NihP0+jZ//lAtwAI6BLGRTaerPEgRxyyH7i2BBx+UwUR+4qY0rayV10e720/4nLAbZUHLZdroPvdE/4GStcy5xtMKdrygiApZ57FqrgDpmsCUvdnGOyB1sm5CvZvrDqhaT9IO2jLLr9K5b8L55X3VnFhOXLlqZyx3EAzGGZKCayyiXOwQ9yQSKYS6kqcx/6nIId8EW3/2aZgdU53gj23UtU+O3P0477PRXtOQU7/kGWCTGSq1jiGAt2XcLlbpwrG0uKr84muAmJcYUucxQlFxukZChUv0O9zAXx5yXTcWxiUnie3s9LWkzH0g50B/UoQ6q7nWDFXhwDgpJiooxjEiKm0N2cTUzFFEfXu1Lc9vgT19d9hdSj6u6L411BJSkmLF+2ZTSNz4QlwPZFZS0m/A7GDwott3xAzajt4FaOk28VodjE1rLrDCrUH7LMUXZo2YlpsIwtmbbzJ9SLWg3e3/bQ3mLHMBYbW5v/pieYr2siKqac/KVB2zDODgff1eb2LH9jiJELT/mMW99YV4iYbBHTGDPtIszL2JAjRPS35fWo8DJG3Irryb3o9ifLGGe+Y5EUUxKrJLCMjfk6zjliEJKKZJnM51zyjW4u/NtFqhFThKDiDg72dw+dSUgxQyivFjF5n1aEiMm2bIjPtMHZ2FKww9IJIvohks8kJBXRKinJSS7Zil6y0Y1J8ZBuF/Zu8xucz62YrGBOu964lknyvrrlc+eCvtCQHxzsN8bhUwT/PWKKaWvJb/KeHL6WiSImNtMdELvx4QcHezQWbiGpWq7Iiy1zzMUoQprzUHnTwR6CGgqUS6aun1sFkj5TKXwXwX4Px8KkLhTZ3PktotcB98TUcKTUIHJBO9i3CYWkahJTEZZpWJBhcCwSEk+66GAbIvlq1ZQUkqwc5x1JvYS+EaxLFvbnqX7//3hYrB7F1nMQ0qGmQme+lokymybSJhxXzf+XzEWwWAdYI+vYGTpyUqlmiVO+YsLVY/7RBDAS1mRwb+2G4mAbGKPaFFKIyZYA5+2XSvpMIsscBVifpWvFlcDWrj7k6C95jynEZ7KVuyvqehOCkX9EFFKq/Pgsq+3avogkRy4+SES1CaTyl2zPJYllqkJMKDsTW0gqoZhsed7eu0tJMQUlp0ck1XIcXUzEA3rvcYWIyfqhqbILHEkhplT+EuV39R6XpGVSofewIhHL4R6SaomzPo8QkXuLCR9qi0mUIKYUpAoJ2J5HUAZD6HGKbYZlLSaptg8EUvhLN4SIftC4pMX0MUUX68w5JUqGo0ycoHFJi0llbp1SROlT+UuUomzpLBNmmM1vyrlHCsVq6gPiqe5AxZRPlauYdqEZDBwpKLYSLCz1EhNhWrm/4KB4wlCQK4W/RMn9Cq6jFUNMKuMWF5cs0zOE9Pds1TtYlFT86vl5qfwlyuqQXkxI/Sh1qXvPZ9LNp9/t0IlGQp+Jvd2GpIp622pvHnIqKm9T9YdYvfUZeKI0n0am5oTYvduQwl+ifO8sdUe5xEQZTI5L3fj8kCQkg7Zc2oIppR6IznlUMTl02mKpO8oiJsxSm8m/SxgkpOAkpCGoQzCzOOcp/CVKV9JnrnNCzgsF0fubMXCARXnwFZIBQpld6EIZteqwg1ViK/jP0tVJuXUQeqi9j8obN3+P2BlGyxQg3j7W2QtsYRs2y5Sq+2KOYLf3E3Z8n3G1KqaQblP0/2OzTMrNOn1tfXrlQEsyWziA1Sop7kuYDtZpWXBUPGuwyaH0dMm+e7iCmGzb5A/X0DYsNoP64zaOEn4ru5gcrJNu/NI6ifOyIpZjFAkgs/pMQ9DEj/KLTWspdpUSHOb+ThgCWw/gMZIldajq30gVubgWBrURbJwkz0nFxISoOOXc6mPJbcRS49inZSUZopAu9jUnnll9QkGthjsbYrWWg3Q4RlRMcMapbc6/FJRZkAWYgJTLrieudvOXEC9DiOWuJ/74b01QNBxLIy5jRODFdnNjFt3+xaF4VvXndyE4Csk7G8KVmAVSZw4J+c1CvYOjkA4x88iiiQn+k6ug2NIjasBRSKdxDrs00ZY5g0ctpCes+dUUEnXFo1irEVLUYHB0MSk/QWlzfV9aJ3IOPGtsfo7Q7/cHkhSVh3P94PBf9Ix8wZHB1YCzy72jkB5SCEmlskwGz/J/1S97SM+hxpCGJN0FJ2134WGhFBzQ11qtFKyRrfXpmFMO4ZSklskQUKB0h0LxxftSSGpbeRSyT+Jsv0UWYlLhNbifUDi+OFFhSVsRsyPHHLExySKFJxsxKZ42E8WIiqE17A5CysZ3zEpM6p+YyiqwJZdOfVmjDkJWYEmfB1YjzvJCRnZiMjA1VD5i6VynXAqY2popc/qfautvI1sxqbAt8lsYYekHsZVcHjDuGV6hAjK828k8F7IWkwHb5UfmMssHbMHN69XH14JwjHgmeFFy36kcEVfLPhu1CDEpPl+KwpFY43wSoYb4xU7muVGMmAzYBT0W1E7DhyJDHcWJyVCpqIqNl6mSxWRgiNek5oSNQbEiMhQvJgMcYRPD4XSApThgp7qu5dC6GjENGVgrrm05F1nEvaSoUkxDcERzj617Cv9qZ+JbtV+Dr15MY2C1ZogNTZjbze8QVni5BvGMuToxvcUg8Hjj2MzQHGvoDgZXm6NuaGJqsJE007JRF01MDTaamBpsNDE12GhiarDRxNRgo4mpwUYTU4ONJqYGG01MDTaamBpsNDE1eFBK/QWcx0eVP5XPwAAAAABJRU5ErkJggg=="
        />
      </defs>
    </svg>
  );
};
