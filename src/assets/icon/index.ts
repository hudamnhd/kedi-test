import { Translate } from "@/assets/icon/translate";
import { Settings } from "@/assets/icon/settings";
import { IconIdn } from "@/assets/icon/idn";
import { IconEn } from "@/assets/icon/en";
import { Assignment } from "@/assets/icon/assignment";
import { Bookmark } from "@/assets/icon/bookmark";
import { ContactSupport } from "@/assets/icon/contactSupport";
import { Disturb } from "@/assets/icon/disturb";
import { Dolar } from "@/assets/icon/dolar";
import { Info } from "@/assets/icon/info";
import { Lock } from "@/assets/icon/lock";
import { Logout } from "@/assets/icon/logout";
import { Payments } from "@/assets/icon/payments";
import { PersonRemove } from "@/assets/icon/personRemove";
import { Privacy } from "@/assets/icon/privacy";
import { SupportAgent } from "@/assets/icon/supportAgent";
import { WhatsApp } from "@/assets/icon/wa";
import { Telegram } from "@/assets/icon/tg";
import { Email } from "@/assets/icon/em";
import { Threedots } from "@/assets/icon/threedot";

export {
  Translate,
  Settings,
  IconEn,
  IconIdn,
  Assignment,
  Bookmark,
  ContactSupport,
  Disturb,
  Dolar,
  Info,
  Lock,
  Logout,
  Payments,
  PersonRemove,
  Privacy,
  SupportAgent,
  WhatsApp,
  Telegram,
  Email,
  Threedots,
};
