export const Info = (props: {
  size?: number;
  stroke?: string; // recently not used
}) => {
  return (
    <svg
      width={props.size ?? 18}
      height={props.size ?? 18}
      viewBox="0 0 32 32"
      fill="currentColor"
      xmlns="http://www.w3.org/2000/svg"
    >
      <g clip-path="url(#clip0_1139_1135)">
        <path
          d="M14.6667 9.33366H17.3334V12.0003H14.6667V9.33366ZM14.6667 14.667H17.3334V22.667H14.6667V14.667ZM16 2.66699C8.64002 2.66699 2.66669 8.64033 2.66669 16.0003C2.66669 23.3603 8.64002 29.3337 16 29.3337C23.36 29.3337 29.3334 23.3603 29.3334 16.0003C29.3334 8.64033 23.36 2.66699 16 2.66699ZM16 26.667C10.12 26.667 5.33335 21.8803 5.33335 16.0003C5.33335 10.1203 10.12 5.33366 16 5.33366C21.88 5.33366 26.6667 10.1203 26.6667 16.0003C26.6667 21.8803 21.88 26.667 16 26.667Z"
          fill="#686464"
        />
      </g>
      <defs>
        <clipPath id="clip0_1139_1135">
          <rect width="32" height="32" fill="white" />
        </clipPath>
      </defs>
    </svg>
  );
};
