export const Translate = (props: {
  size?: number;
  stroke?: string; // recently not used
}) => {
  return (
    <svg
      width={props.size ?? 18}
      height={props.size ?? 18}
      viewBox="0 0 32 32"
      fill="currentColor"
      xmlns="http://www.w3.org/2000/svg"
    >
      <g clip-path="url(#clip0_31_2851)">
        <path
          d="M17.16 20.0937L13.7733 16.747L13.8133 16.707C16.1333 14.1203 17.7866 11.147 18.76 8.00033H22.6666V5.33366H13.3333V2.66699H10.6666V5.33366H1.33331V7.98699H16.2266C15.3333 10.5603 13.92 13.0003 12 15.1337C10.76 13.7603 9.73331 12.2537 8.91998 10.667H6.25331C7.22665 12.8403 8.55998 14.8937 10.2266 16.747L3.43998 23.4403L5.33331 25.3337L12 18.667L16.1466 22.8137L17.16 20.0937ZM24.6666 13.3337H22L16 29.3337H18.6666L20.16 25.3337H26.4933L28 29.3337H30.6666L24.6666 13.3337ZM21.1733 22.667L23.3333 16.8937L25.4933 22.667H21.1733Z"
          fill="currentColor"
        />
      </g>
      <defs>
        <clipPath id="clip0_31_2851">
          <rect width="32" height="32" fill="white" />
        </clipPath>
      </defs>
    </svg>
  );
};
