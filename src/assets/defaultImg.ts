export const defaultImgProduction = [
  {
    documentStorage: {
      id: "2172",
    },
    url: "https://storage.googleapis.com/kedi_storage/DEVELOPER/developer_1712329047_b0wi4h.webp",
  },
  {
    documentStorage: {
      id: "2173",
    },
    url: "https://storage.googleapis.com/kedi_storage/DEVELOPER/developer_1712329207_1fmrg.webp",
  },
  {
    documentStorage: {
      id: "2174",
    },
    url: "https://storage.googleapis.com/kedi_storage/DEVELOPER/developer_1712329233_u7koti.webp",
  },
  {
    documentStorage: {
      id: "2175",
    },
    url: "https://storage.googleapis.com/kedi_storage/DEVELOPER/developer_1712329255_8v37qh.webp",
  },
  {
    documentStorage: {
      id: "2176",
    },
    url: "https://storage.googleapis.com/kedi_storage/DEVELOPER/developer_1712329298_zn1tq.webp",
  },
  {
    documentStorage: {
      id: "2177",
    },
    url: "https://storage.googleapis.com/kedi_storage/DEVELOPER/developer_1712329325_j2zxh.webp",
  },
];

export const defaultImgDevelopment = [
  {
    documentStorage: {
      id: "902",
    },
    url: "https://storage.googleapis.com/kedi_storage_dev/DEVELOPER/developer_1701925567_0tgjf.webp",
  },
  {
    documentStorage: {
      id: "903",
    },
    url: "https://storage.googleapis.com/kedi_storage_dev/DEVELOPER/developer_1701925694_y6x57.webp",
  },
  {
    documentStorage: {
      id: "912",
    },
    url: "https://storage.googleapis.com/kedi_storage_dev/DEVELOPER/developer_1702406150_dbimw.webp",
  },
  {
    documentStorage: {
      id: "913",
    },
    url: "https://storage.googleapis.com/kedi_storage_dev/DEVELOPER/developer_1702406335_gbe4i.webp",
  },
  {
    documentStorage: {
      id: "914",
    },
    url: "https://storage.googleapis.com/kedi_storage_dev/DEVELOPER/developer_1702406366_r0yn1j.webp",
  },
  {
    documentStorage: {
      id: "915",
    },
    url: "https://storage.googleapis.com/kedi_storage_dev/DEVELOPER/developer_1702407529_kcr4x.webp",
  },
];

export const defaultImgStaging = [
  {
    documentStorage: {
      isPrivate: false,
      id: "791",
    },
    url: "https://storage.googleapis.com/kedi_storage_staging/DEVELOPER/developer_1711050902_ff268.jpg",
  },
  {
    documentStorage: {
      id: "786",
    },
    url: "https://storage.googleapis.com/kedi_storage_staging/DEVELOPER/developer_1711050425_y2in0i.webp",
  },

  {
    documentStorage: {
      isPrivate: false,
      id: "787",
    },
    url: "https://storage.googleapis.com/kedi_storage_staging/DEVELOPER/developer_1711050663_mb5f5.webp",
  },
  {
    documentStorage: {
      isPrivate: false,
      id: "788",
    },
    url: "https://storage.googleapis.com/kedi_storage_staging/DEVELOPER/developer_1711050674_dod83.webp",
  },
  {
    documentStorage: {
      isPrivate: false,
      id: "789",
    },
    url: "https://storage.googleapis.com/kedi_storage_staging/DEVELOPER/developer_1711050706_0lsmc.webp",
  },
  {
    documentStorage: {
      isPrivate: false,
      id: "790",
    },
    url: "https://storage.googleapis.com/kedi_storage_staging/DEVELOPER/developer_1711050714_r1x69.webp",
  },
];
