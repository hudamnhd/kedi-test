import { trans as enTrans } from "@/lang/en";
import { trans as idTrans } from "@/lang/id";

export const trans = {
  en: { ...enTrans.en },
  id: { ...idTrans.id },
};
