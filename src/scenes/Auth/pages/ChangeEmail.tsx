import {yupResolver} from "@hookform/resolvers/yup";
import {resetPasswordDto, schema} from "../validations/reset-password";
import {useForm} from "react-hook-form";
import logo from "../../../assets/images/Logo-KeDiM.png";
import React, {useEffect, useState} from "react";
import {useParams, useSearchParams} from "react-router-dom";


function ResetPassword() {
  const [params, setSearchParam] = useSearchParams({})
  const [status, setStatus] = useState('test')
  const userId = params.get('userId')
  const token = params.get('token')
  useEffect(() => {
    setStatus('Email anda sedang diubah')
    const aquery = new URLSearchParams({
      userId: `${userId}`,
      token: `${token}`
    })
    const query = aquery.toString()

    fetch('/api/academics/user/validate-email-change?' + query, {
      method: 'GET'
    }).then((response) => {
      if (response.ok) {
        setStatus('Email anda berhasil diubah')
      } else {
        setStatus('API ERROR atau URL tidak valid')
      }
    }).catch((e) => {
      console.error(e)
    })
  }, [])

  return (
    <div
      className="flex w-screen h-screen items-center justify-center bg-[#cfb6ff]">
      <div className="flex flex-col justify-center w-full lg:w-[472px]">
        <div className="flex w-full justify-center">
          <img alt="logo kedi" src={logo} width="305"/>
        </div>
        <div className="flex w-full justify-center">
          <div className="text-xl mt-[30px]">
            {status}
          </div>
        </div>
      </div>
    </div>

  )
}

export default ResetPassword