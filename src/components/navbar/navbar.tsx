import React, {
  useEffect,
  useState,
  useCallback,
  useMemo,
  useRef,
} from "react";
import { useMutation, useQuery, useQueryClient } from "@tanstack/react-query";
import { signOut } from "next-auth/react";
import PubNub from "pubnub";
import axios from "axios";
import Image from "next/image";
import Link from "next/link";
import { useRouter } from "next/router";
import toast from "react-hot-toast";
import { toast as toastSoner } from "sonner";
import { trans } from "@/lang";
import {
  converDateNoDay,
  formatTimeHourOrDate,
  formatTime,
} from "@/utils/convertTime";
import { PostsView } from "@/components/navbar/postView";
import { Threedots } from "@/assets/icon";
import { useMediaQuery } from "react-responsive";
import { EmptyState } from "@/components/ui/empty-state";
import { LoadingSpinner } from "@/components/ui/loading";
import { Tabs, TabsContent, TabsList, TabsTrigger } from "@/components/ui/tabs";
import {
  Select,
  SelectContent,
  SelectItem,
  SelectTrigger,
  SelectValue,
} from "@/components/ui/select";
import {
  Dialog,
  DialogContent,
  DialogDescription,
  DialogHeader,
  DialogTitle,
} from "@/components/ui/dialog";
import {
  DropdownMenu,
  DropdownMenuContent,
  DropdownMenuItem,
  DropdownMenuLabel,
  DropdownMenuSeparator,
  DropdownMenuTrigger,
} from "@/components/ui/dropdown-menu";
import { encryptData, decryptData } from "@/utils/helper";
import useUserChat from "@/store/useUserChat";
import { getMessageUser } from "@/utils/getMessageUser";
import { getPubNub } from "@/utils/getPubNub";
import { getMetadata } from "@/pages/class/ClassRoom";

const MAX_ITEMS_PER_TAB = 30;

const compareDates = (a, b) => {
  return new Date(b.sortTime) - new Date(a.sortTime);
};

function formatCurrency(amount) {
  if (!amount) return 0;
  const formattedAmount = new Intl.NumberFormat("id-ID", {
    style: "currency",
    currency: "IDR",
    minimumFractionDigits: 2,
  }).format(amount);

  return formattedAmount;
}

const SearchMenu = ({ user, lang }: any) => {
  const [modalClass, setModalClass] = useState(false);

  return (
    <>
      <div
        onClick={() => setModalClass(true)}
        className="sm:ml-6 order-1 sm:order-none flex relative justify-end  sm:max-w-[215px]  md:w-full sm:flex-1 "
      >
        <div className="lg:hidden inline text-slate-500 flex items-center rounded-lg pointer-events-none ">
          <svg
            xmlns="http://www.w3.org/2000/svg"
            width="20"
            height="20"
            viewBox="0 0 32 32"
          >
            <path
              fill="currentColor"
              d="m29 27.586l-7.552-7.552a11.018 11.018 0 1 0-1.414 1.414L27.586 29ZM4 13a9 9 0 1 1 9 9a9.01 9.01 0 0 1-9-9Z"
            />
          </svg>
        </div>

        <div className="lg:inline hidden">
          <div className="absolute inset-y-0 left-0 flex items-center pl-3 pointer-events-none">
            <svg
              className="w-4 h-4 text-gray-500 dark:text-gray-400"
              aria-hidden="true"
              xmlns="http://www.w3.org/2000/svg"
              fill="none"
              viewBox="0 0 20 20"
            >
              <path
                stroke="currentColor"
                strokeLinecap="round"
                strokeLinejoin="round"
                strokeWidth={2.5}
                d="m19 19-4-4m0-7A7 7 0 1 1 1 8a7 7 0 0 1 14 0Z"
              />
            </svg>
          </div>
          <input
            type="search"
            id="default-search"
            name="query"
            className=" block w-full p-2.5 pl-9 text-sm text-gray-900 border border-gray-300 rounded-lg bg-gray-50 outline-none "
            placeholder={lang.search}
            autoComplete="off"
          />
        </div>
      </div>
      {modalClass && (
        <CommandMenu
          lang={lang}
          user={user}
          modalClass={modalClass}
          setModalClass={setModalClass}
        />
      )}
    </>
  );
};

function CommandMenu({ lang, user, modalClass, setModalClass }: any) {
  const chatMode = useUserChat((state) => state.chatMode);
  const setChatMode = (value) => {
    useUserChat.setState({
      chatMode: value,
    });
  };

  const setUserChatAktif = (value) => {
    useUserChat.setState({
      userChatAktif: value,
    });
  };

  const setTabChat = (value) => {
    useUserChat.setState({
      tabChat: value,
    });
  };

  const [isLoading, setIsloading] = useState(false);
  const [_query, setQuery] = useState("");
  const [searchQuery, setSearchQuery] = useState("");
  const [tabSearch, setTabSearh] = useState("contacts");
  const [searchUser, setSearchUser] = useState([]);
  const [searchClass, setSearchClass] = useState([]);

  const router = useRouter();
  const sessionToken = user?.accessToken;

  React.useEffect(() => {
    // eslint-disable-next-line react-hooks/exhaustive-deps
    if (tabSearch === "class") {
      setTabSearh("contacts");
      setTimeout(() => {
        setTabSearh("class");
      }, 500);
    }
  }, [_query]);

  const fetchDataSearch = async (query: string) => {
    const headers = {
      Authorization: `Bearer ${sessionToken}`,
    };
    const apiEndpoint = process.env.API_BE + "/api";
    const userData = await axios.get(
      `${apiEndpoint}/user/search?q=${query}&limit=50&page=1`,
      {
        headers,
      },
    );

    return {
      user: userData.data.data,
    };
  };

  const updateQuery = async (query: string) => {
    setIsloading(true);
    try {
      const response = await fetchDataSearch(query);
      setSearchUser(response.user);
    } catch (error) {
      console.error(
        "Gagal mengambil data pencarian.",
        error.message || error.response,
      );
    } finally {
      setIsloading(false);
    }
  };

  const queryClient = useQueryClient();

  const handleSubmit = useCallback(
    async (e: React.FormEvent<HTMLFormElement>) => {
      e.preventDefault();
      const formData = new FormData(e.target as HTMLFormElement);
      const query = formData.get("query")?.toString().trim();
      if (query) {
        queryClient.clear();
        setQuery(query);
        updateQuery(query);
      }
    },
    [],
  );

  interface ApiResponse {
    data: {
      message: string;
    };
  }

  interface ToggleFavoriteResponse extends ApiResponse {}

  const addFriend = useMutation(
    async (userId: number): Promise<ToggleFavoriteResponse> => {
      if (!sessionToken) throw new Error("Missing session token");
      try {
        const response = await axios.post<ToggleFavoriteResponse>(
          process.env.API_BE + `/api/friend/add`,
          {
            id: Number(userId),
          },
          {
            headers: {
              Authorization: `Bearer ${sessionToken}`,
            },
          },
        );
        toast.success(response.data.data.message);
        updateQuery(_query);
        return response.data;
      } catch (error) {
        console.error(error);
        throw error;
      }
    },
  );

  return (
    <>
      <Dialog
        open={modalClass}
        onOpenChange={setModalClass}
        onOpenAutoFocus={(e) => e.preventDefault()}
      >
        <DialogContent className="outline-none max-w-xl">
          <DialogHeader>
            <DialogTitle>{lang.search}</DialogTitle>
            <DialogDescription>{lang.searchByName}</DialogDescription>
          </DialogHeader>
          <form onSubmit={handleSubmit} className="flex justify-end  w-full">
            <label
              htmlFor="default-search"
              className="mb-2 text-sm font-medium text-gray-900 sr-only dark:text-white"
            >
              {lang.search}
            </label>
            <div className="relative w-full">
              <div className="absolute inset-y-0 left-0 flex items-center pl-3 pointer-events-none">
                <svg
                  className="w-4 h-4 text-gray-500 dark:text-gray-400"
                  aria-hidden="true"
                  xmlns="http://www.w3.org/2000/svg"
                  fill="none"
                  viewBox="0 0 20 20"
                >
                  <path
                    stroke="currentColor"
                    strokeLinecap="round"
                    strokeLinejoin="round"
                    strokeWidth={2.5}
                    d="m19 19-4-4m0-7A7 7 0 1 1 1 8a7 7 0 0 1 14 0Z"
                  />
                </svg>
              </div>
              <input
                type="search"
                id="default-search"
                name="query"
                value={searchQuery}
                onChange={(e) => setSearchQuery(e.target.value)}
                className="disabled:bg-purple-100 block w-full p-3 pl-10 text-sm text-gray-900 border border-gray-300 rounded-lg bg-gray-50 outline-none "
                placeholder={lang.search}
                disabled={isLoading}
                autoComplete="off"
              />
              <button
                type="submit"
                disabled={searchQuery.length === 0}
                className="w-[75px] text-center disabled:opacity-50 outline-none flex justify-center items-center text-white absolute  right-1.5 bottom-[7px] bg-[#9D21E6] hover:bg-purple-600  focus:outline-none  font-medium rounded-lg text-sm px-4 py-1.5 "
              >
                {isLoading ? (
                  <LoadingSpinner stroke={`#FFF`} size={20} />
                ) : (
                  <span>{lang.search}</span>
                )}
              </button>
            </div>
          </form>

          <Tabs value={tabSearch} onValueChange={setTabSearh}>
            <TabsList className="mt-2 grid grid-cols-2">
              {isLoading ? (
                <div className="col-span-2 flex items-center justify-center">
                  <Threedots
                    width={35}
                    height={36}
                    strokeWidth={2}
                    fill="#9D21E6"
                  />
                </div>
              ) : (
                <>
                  <TabsTrigger value="class">{lang.class}</TabsTrigger>
                  <TabsTrigger value="contacts">{lang.contacts}</TabsTrigger>
                </>
              )}
            </TabsList>
            <TabsContent
              forceMount
              hidden={tabSearch !== "class"}
              value="class"
              className="h-[60vh] overflow-y-auto px-1.5 max-w-lg"
            >
              <PostsView user={user} query={_query} />
            </TabsContent>
            <TabsContent
              forceMount
              hidden={tabSearch !== "contacts"}
              value="contacts"
              className="h-[60vh] overflow-y-auto px-1.5"
            >
              {searchUser.length > 0 ? (
                searchUser?.map((item: any) => (
                  <div
                    key={item.id}
                    className="w-full flex items-center justify-between items-center gap-x-2 hover:bg-[#9D21E6]/10 border p-2.5 rounded-lg mb-2.5 cursor-pointer"
                  >
                    <div className="flex items-center gap-x-2.5 ">
                      <div className="relative h-12 w-12 border p-0.5 rounded-lg border-gray-300">
                        <Image
                          width={60}
                          height={60}
                          src={
                            item?.profilePictureLink ??
                            `/static/images/logokedi.svg`
                          }
                          className=" absolute inset-0 h-full w-full object-cover  rounded-lg"
                          alt=""
                        />
                      </div>
                      <div>
                        <p className="font-semibold text-ellipsis">
                          {item.fullName}
                        </p>
                        {item.username && (
                          <p className="text-sm text-[#9D21E6]">
                            {"@" + item.username}
                          </p>
                        )}
                      </div>
                    </div>
                    {!item.isFriend ? (
                      <div
                        onClick={() => addFriend.mutate(item.id)}
                        className="text-[#9D21E6] cursor-pointer"
                      >
                        <svg
                          xmlns="http://www.w3.org/2000/svg"
                          width="24px"
                          height="24px"
                          viewBox="0 0 24 24"
                        >
                          <g
                            fill="none"
                            stroke="currentColor"
                            strokeLinecap="round"
                            strokeWidth="2.5"
                          >
                            <path
                              strokeDasharray="20"
                              strokeDashoffset="20"
                              d="M3 21V20C3 17.7909 4.79086 16 7 16H11C13.2091 16 15 17.7909 15 20V21"
                            >
                              <animate
                                fill="freeze"
                                attributeName="stroke-dashoffset"
                                dur="0.4s"
                                values="20;0"
                              ></animate>
                            </path>
                            <path
                              strokeDasharray="20"
                              strokeDashoffset="20"
                              d="M9 13C7.34315 13 6 11.6569 6 10C6 8.34315 7.34315 7 9 7C10.6569 7 12 8.34315 12 10C12 11.6569 10.6569 13 9 13Z"
                            >
                              <animate
                                fill="freeze"
                                attributeName="stroke-dashoffset"
                                begin="0.5s"
                                dur="0.4s"
                                values="20;0"
                              ></animate>
                            </path>
                            <path
                              strokeDasharray="8"
                              strokeDashoffset="8"
                              d="M15 6H21"
                            >
                              <animate
                                fill="freeze"
                                attributeName="stroke-dashoffset"
                                begin="1s"
                                dur="0.2s"
                                values="8;0"
                              ></animate>
                            </path>
                            <path
                              strokeDasharray="8"
                              strokeDashoffset="8"
                              d="M18 3V9"
                            >
                              <animate
                                fill="freeze"
                                attributeName="stroke-dashoffset"
                                begin="1.2s"
                                dur="0.2s"
                                values="8;0"
                              ></animate>
                            </path>
                          </g>
                        </svg>
                      </div>
                    ) : (
                      <div
                        onClick={() => {
                          if (item?.channel) {
                            setModalClass(false);
                            if (router.pathname !== "/class") {
                              router.push("/class");
                            }
                            getMessageUser({ channel: item.channel });
                            setChatMode("private");
                            setTabChat("channelActivePrivate");
                            setUserChatAktif({
                              channel: "private." + item.chatRoomId,
                              receiver: {
                                id: item.id,
                                name: item.fullName,
                                photo: item.profilePict,
                              },
                              sender: {
                                id: user?.uuid,
                                name: user?.fullName,
                                photo: user?.photo,
                              },
                            });
                            console.log({
                              channel: "private." + item.chatRoomId,
                              receiver: {
                                id: item.id,
                                name: item.fullName,
                                photo: item.profilePict,
                              },
                              sender: {
                                id: user?.uuid,
                                name: user?.fullName,
                                photo: user?.photo,
                              },
                            });
                          } else {
                            console.log("anda belum berteman");
                          }
                        }}
                        className="text-[#9D21E6] cursor-pointer mr-1"
                      >
                        <svg
                          xmlns="http://www.w3.org/2000/svg"
                          fill="none"
                          viewBox="0 0 24 24"
                          strokeWidth={3}
                          stroke="currentColor"
                          className="w-5 h-5"
                        >
                          <path
                            strokeLinecap="round"
                            strokeLinejoin="round"
                            d="M2.25 12.76c0 1.6 1.123 2.994 2.707 3.227 1.087.16 2.185.283 3.293.369V21l4.076-4.076a1.526 1.526 0 0 1 1.037-.443 48.282 48.282 0 0 0 5.68-.494c1.584-.233 2.707-1.626 2.707-3.228V6.741c0-1.602-1.123-2.995-2.707-3.228A48.394 48.394 0 0 0 12 3c-2.392 0-4.744.175-7.043.513C3.373 3.746 2.25 5.14 2.25 6.741v6.018Z"
                          />
                        </svg>
                      </div>
                    )}
                  </div>
                ))
              ) : (
                <div className="flex items-center justify-center text-gray-600 mt-5">
                  <EmptyState />
                </div>
              )}
            </TabsContent>
          </Tabs>
        </DialogContent>
      </Dialog>
    </>
  );
}

const NotifChat = React.memo(({ user, notifMessage }) => {
  const privateMessage = useUserChat((state) => state.privateMessage);

  const setChatMode = (value) => {
    useUserChat.setState({
      chatMode: value,
    });
  };
  const setIsMobileView = (value) => {
    useUserChat.setState({
      isMobileView: value,
    });
  };
  const setPrivateMessage = (value) => {
    useUserChat.setState({
      privateMessage: value,
    });
  };
  const setTabChat = (value) => {
    useUserChat.setState({
      tabChat: value,
    });
  };
  const setUserChatAktif = (value) => {
    useUserChat.setState({
      userChatAktif: value,
    });
  };
  const router = useRouter();
  const lastTimetokenKey = `LastTimetoken_${user?.id}`;
  const isSM = useMediaQuery({ query: "(max-width: 899px)" });

  return (
    <>
      {notifMessage?.filter((d) => d.publisher !== user.uuid).length > 0 ? (
        notifMessage
          .filter((d) => d.publisher !== user.uuid)
          .reverse()
          .map((data, index) => {
            const room = data.channel ?? data.actualChannel;
            const channel = data.channel ?? data.actualChannel;
            const message = data.message;
            let parts = room?.split(".");
            let firstWord = parts ? parts[0] : null;
            let sent_to;

            switch (firstWord) {
              case "group":
                sent_to = "sent message in class chat";
                break;
              case "private":
                sent_to = "sent your message";
                break;
              case "chat-group":
                sent_to = "sent message in group chat";
                break;
              default:
                sent_to = "sent your message";
            }

            const firstName =
              message?.pn_gcm?.data?.custom_data?.author?.firstName ??
              message?.author?.firstName ??
              "";
            const lastName =
              message?.pn_gcm?.data?.custom_data?.author?.lastName ??
              message?.author?.lastName ??
              "";

            const fullName = lastName ? `${firstName} ${lastName}` : firstName;

            const metadata = message?.metadata;

            const chatCategory = metadata?.category;

            const isAdminSystem =
              chatCategory === "created_zoom" ||
              chatCategory === "removed_zoom" ||
              chatCategory === "created_group" ||
              chatCategory === "add_member" ||
              chatCategory === "leave_member" ||
              chatCategory === "removed_member";

            const isClassSystem =
              chatCategory === "assignment" ||
              chatCategory === "quiz" ||
              chatCategory === "uas" ||
              chatCategory === "uts" ||
              chatCategory === "material";

            const isSession = chatCategory === "session";

            const text =
              message?.pn_gcm?.data?.custom_data?.text ??
              message?.metadata?.title;
            const adminText = isAdminSystem ? message?.text : "";
            const combinedText = text || adminText;

            // Gunakan combinedText untuk apa pun yang perlu Anda tampilkan
            return (
              <div
                onClick={() => {
                  if (firstWord === "private") {
                    const updatedAllData = [...privateMessage];
                    const filtered = privateMessage.filter(
                      (dt) => dt.channel === channel,
                    );
                    const d = filtered?.length > 0 ? filtered[0] : null;

                    if (!d) return null;
                    const readMessage = {
                      ...d,
                      unread: 0,
                    };
                    const indexItem = privateMessage.findIndex(
                      (item) => item.channel === channel,
                    );
                    updatedAllData[indexItem] = readMessage;

                    const lastMessStr = localStorage.getItem(lastTimetokenKey);
                    let updatedAllDataLS = {};

                    if (lastMessStr) {
                      const lastMessObj = JSON.parse(lastMessStr);
                      updatedAllDataLS = {
                        channels: [...lastMessObj.channels],
                        timetokens: [...lastMessObj.timetokens],
                      };
                      const indexItemLS = lastMessObj.channels.findIndex(
                        (item) => item === channel,
                      );
                      updatedAllDataLS.timetokens[indexItemLS] = data.timetoken;
                    }

                    if (d?.unread > 0) {
                      setPrivateMessage(updatedAllData);
                      localStorage.setItem(
                        lastTimetokenKey,
                        JSON.stringify(updatedAllDataLS),
                      );
                    }

                    getMessageUser({ channel });
                    if (isSM) {
                      setIsMobileView(true);
                    }
                    if (router.pathname !== "/class") {
                      router.push("/class");
                    }
                    setChatMode("private");
                    setTabChat("channelActivePrivate");
                    setUserChatAktif({
                      channel: channel,
                      receiver: {
                        id: message?.pn_gcm?.data?.custom_data?.author?.id,
                        name: message?.pn_gcm?.data?.custom_data?.author
                          ?.firstName,
                        photo:
                          message?.pn_gcm?.data?.custom_data?.author?.imageUrl,
                      },
                      sender: {
                        id: user.uuid,
                        name: user.fullName,
                        photo: user.photo,
                      },
                    });
                  }
                }}
                key={index}
              >
                <div className="flex items-center border border-gray-200  rounded-lg px-3 py-1 cursor-pointer">
                  <div className="relative w-14 h-14 flex flex-shrink-0 items-end">
                    <Image
                      width={60}
                      height={60}
                      src={
                        message?.pn_gcm?.data?.custom_data?.author?.imageUrl ||
                        `/static/images/logokedi.svg`
                      }
                      className="absolute inset-0 h-full w-full object-cover rounded-xl shadow-md border border-gray-300"
                      alt=""
                    />
                    <span className="hidden absolute h-4 w-4 bg-green-400 rounded-full bottom-0 right-0 border-2 border-white" />
                  </div>
                  <div className="ml-3">
                    <span className="font-semibold tracking-tight text-xs">
                      {fullName}
                    </span>{" "}
                    <span className="text-xs leading-none opacity-50">
                      {sent_to}
                    </span>
                    <p className="text-[13px] leading-4 pt-2 italic opacity-70">
                      {combinedText}
                    </p>
                    <span className="text-[10px] text-blue-500 font-semibold leading-4 opacity-75">
                      {data?.timetoken && formatTimeHourOrDate(data?.timetoken)}
                    </span>
                  </div>
                </div>
              </div>
            );
          })
      ) : (
        <div className="flex items-center justify-center text-gray-600 h-full">
          <EmptyState />
        </div>
      )}
    </>
  );
});

const NotifPayment = React.memo(({ notifFinance }) => {
  const [valueFilterNotif, setValueFilterNotif] = useState("Semua");

  // Memoize unique statuses to avoid recomputing on every render
  const filterNotifFinance = useMemo(() => {
    const uniqueStatus = new Set(["Semua"]);
    notifFinance?.forEach((item) => {
      const status = getStatusLabel(item.status);
      uniqueStatus.add(status);
    });

    return Array.from(uniqueStatus);
  }, [notifFinance]);

  // Memoize filtered data based on selected filter
  const dataNotifFinance = useMemo(() => {
    return notifFinance?.filter((d) => {
      const isCancel =
        d.status === "CANCEL" ||
        d.status === "EXPIRE" ||
        d.status === "EXPIRED" ||
        d.status === "ERROR";
      const isSuccess = d.status === "SETTLEMENT";
      const isPending = d.status === "PENDING";
      const isWaitApprove = d.status === "awaiting-approval";
      const isWaitPay = d.status === "NEW" || d.status === "BARU";

      switch (valueFilterNotif) {
        case "Semua":
          return true;
        case "Belum Dibayar":
          return isWaitPay;
        case "Transaksi Dibatalkan":
          return isCancel;
        case "Menunggu Disetujui":
          return isWaitApprove;
        case "Sudah Dibayar":
          return isSuccess;
        case "Menunggu Pembayaran":
          return isPending;
        default:
          return true;
      }
    });
  }, [valueFilterNotif, notifFinance]);

  function getStatusLabel(status) {
    switch (status) {
      case "SETTLEMENT":
        return "Sudah Dibayar";
      case "CANCEL":
      case "EXPIRE":
      case "EXPIRED":
      case "ERROR":
        return "Transaksi Dibatalkan";
      case "awaiting-approval":
        return "Menunggu Disetujui";
      case "PENDING":
        return "Menunggu Pembayaran";
      case "NEW":
      case "BARU":
        return "Belum Dibayar";
      default:
        return "Status tidak diketahui";
    }
  }

  return (
    <>
      <Select
        defaultValue="Semua"
        value={valueFilterNotif}
        onValueChange={setValueFilterNotif}
      >
        <SelectTrigger className="w-full mb-2.5 rounded-lg">
          <SelectValue placeholder="Filter" />
        </SelectTrigger>
        <SelectContent
          ref={(ref) =>
            ref?.addEventListener("touchend", (e) => e.preventDefault())
          }
        >
          {filterNotifFinance.length > 0 &&
            filterNotifFinance.map((status) => {
              return (
                <SelectItem key={status} value={status} className="py-2.5">
                  {status}
                </SelectItem>
              );
            })}
        </SelectContent>
      </Select>
      <div className="max-h-[55vh] overflow-y-auto border p-1.5 rounded-lg">
        {dataNotifFinance?.length > 0 ? (
          dataNotifFinance?.map((d, index) => {
            const status = getStatusLabel(d.status);
            const textStatus =
              d?.status === "SETTLEMENT"
                ? "text-[#1DA26A]"
                : d?.status === "CANCEL" ||
                    d?.status === "EXPIRE" ||
                    d?.status === "EXPIRED" ||
                    d?.status === "ERROR"
                  ? "text-red-500"
                  : d?.status === "PENDING"
                    ? "text-yellow-500"
                    : d?.status === "awaiting-approval"
                      ? "text-yellow-600"
                      : d?.status === "NEW" || d?.status === "BARU"
                        ? "text-amber-600"
                        : "text-gray-700";

            const isType =
              d?.notif_type === "payment"
                ? "Total Bayar : "
                : d?.notif_type === "payout"
                  ? "Total Penarikan : "
                  : "";
            const isPayment = d?.notif_type === "payment" ? true : false;

            return (
              <div
                key={index}
                className="text-sm justify-between py-2 px-4 border-2 rounded-lg mb-1.5 hover:bg-purple-100"
              >
                <Link
                  href={
                    isPayment ? "/payment/" + d?.data?.invoiceIdentifier : "#"
                  }
                >
                  <div className="flex items-center justify-between space-x-4 mb-2">
                    <div className="flex-none px-1  text-stone-600 text-xs">
                      {converDateNoDay(d?.date)}
                    </div>
                    <div className={` ${textStatus} font-bold  text-xs`}>
                      {status}
                    </div>
                  </div>
                  <div className="flex items-center space-x-4">
                    <div className="flex-none relative w-14 h-14">
                      <Image
                        width={60}
                        height={60}
                        src={`/static/images/logokedi.svg`}
                        className="absolute inset-0 h-full w-full object-cover rounded-xl shadow-md border border-gray-300"
                        alt=""
                      />
                    </div>
                    <div className="flex flex-col space-y-1">
                      <span className="font-bold">
                        {d?.data?.invoiceItems?.length > 0
                          ? "( " + d?.data?.invoiceItems[0]?.name + " )"
                          : d?.data?.extra_data?.class_name
                            ? "( " + d?.data?.extra_data?.class_name + " )"
                            : d?.data?.invoiceIdentifier
                              ? "( " + d?.data?.invoiceIdentifier + " )"
                              : "Tidak diketahui"}
                      </span>
                      <span className="text-sm">
                        {isType}
                        <span className="text-[#1DA26A] font-semibold">
                          {d?.data?.invoiceItems?.length > 0
                            ? formatCurrency(d?.data?.invoiceItems[0]?.amount)
                            : d?.data?.extra_data?.class_price
                              ? formatCurrency(d?.data?.extra_data?.class_price)
                              : formatCurrency(d?.data?.totalAmount)}
                        </span>
                      </span>
                    </div>
                  </div>
                </Link>
              </div>
            );
          })
        ) : (
          <div className="flex items-center justify-center h-full text-sm opacity-70">
            No notification
          </div>
        )}
      </div>
    </>
  );
});

const PopupNotif = React.memo(({ user }) => {
  const userChatAktif = useUserChat((state) => state.userChatAktif);
  const allClassMessage = useUserChat((state) => state.allClassMessage);
  const groupChatMessage = useUserChat((state) => state.groupChatMessage);
  const privateMessage = useUserChat((state) => state.privateMessage);
  const notifDelete = useUserChat((state) => state.notifDelete);
  const notifMessage = useUserChat((state) => state.notifMessage);
  const seenMessages = useUserChat((state) => state.seenMessages);
  const setChatMode = (value) => {
    useUserChat.setState({
      chatMode: value,
    });
  };
  const setPrivateMessage = (value) => {
    useUserChat.setState({
      privateMessage: value,
    });
  };
  const setIsMobileView = (value) => {
    useUserChat.setState({
      isMobileView: value,
    });
  };
  const setIsTyping = (value) => {
    useUserChat.setState({
      isTyping: value,
    });
  };
  const setResponses = (value) => {
    useUserChat.setState({
      responses: value,
    });
  };
  const setTabChat = (value) => {
    useUserChat.setState({
      tabChat: value,
    });
  };
  const setUserChatAktif = (value) => {
    useUserChat.setState({
      userChatAktif: value,
    });
  };

  const [value, setValue] = useState("contacts");
  const router = useRouter();
  const sessionToken = user?.accessToken;
  const lastTimetokenKey = `LastTimetoken_${user?.id}`;
  const isSM = useMediaQuery({ query: "(max-width: 899px)" });

  const { data: notifFinance } = useQuery(
    ["notifFinance"],
    async () => {
      if (!sessionToken) return null;

      const response = await axios.get(
        process.env.API_BE + "/api/finance/notification",
        {
          headers: {
            Authorization: `Bearer ${sessionToken}`,
          },
        },
      );

      if (response.status !== 200) {
        throw new Error("Gagal mengambil data");
      }

      return response.data.data;
    },
    {
      refetchInterval: 30000,
    },
  );

  {
    /*  useEffect(() => {
    if (notifFinance) {
      let dataClass = localStorage.getItem("channelActiveClass");
      let initialClass = dataClass ? JSON.parse(dataClass) : [];

      const filterFinanceSuccess = notifFinance
        ?.filter((d) => d.status === "SETTLEMENT")
        .flatMap((d) => d?.data?.invoiceItems)
        .map((item) => "group." + item?.productReference.relId); // Mendapatkan relId dari setiap objek dalam array

      filterFinanceSuccess.forEach((item) => {
        // Periksa apakah item sudah ada di dalam initialClass
        if (!initialClass.includes(item)) {
          // initialClass.push(item);
          console.log("ADA YANG BARU BRO");
        } else {
          console.log("SUDAH ADA BRO");
        }
      });

        console.log('initialClass:',  initialClass);
      // localStorage.setItem("channelActiveClass", JSON.stringify(initialClass));
    }
  }, [notifFinance]);*/
  }

  // HANDLE NOTIF MESSAGE CLASS & PRIVATE & GROUP
  React.useEffect(() => {
    if (notifMessage.length > 0) {
      const data = notifMessage[notifMessage.length - 1];
      let room = data?.channel ?? data?.actualChannel;
      let message = data.message;
      let channel = data?.channel;
      let timetoken = data?.timetoken;

      const chatCategory = message?.metadata?.category;
      const videoUrl = message?.metadata?.url;
      const isAdminSystemZoom =
        chatCategory === "created_zoom" || chatCategory === "removed_zoom";

      if (!seenMessages[channel] || seenMessages[channel] !== timetoken) {
        useUserChat.setState((prevState) => ({
          seenMessages: {
            ...prevState.seenMessages,
            [channel]: timetoken,
          },
        }));
      } else {
        return;
      }

      if (router && router.pathname === "/class") {
        if (userChatAktif?.channel === room) {
          getMessageUser({ channel: room, isDelete: true });
          setIsTyping({});
          if (isAdminSystemZoom) {
            setUserChatAktif({
              ...userChatAktif,
              videoUrl: videoUrl ?? null,
            });
          }
        }
      }
      let parts = room?.split(".");
      let firstWord = parts[0];

      switch (firstWord) {
        case "group":
          replaceText(allClassMessage, room, message, data, "group");
          break;
        case "private":
          replaceText(privateMessage, room, message, data, "private");
          break;
        case "chat-group":
          replaceText(groupChatMessage, room, message, data, "chat-group");
          break;
        default:
      }
    }
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [notifMessage]);

  const channelStore = useUserChat((state) => state.channelStore);
  const responses = useUserChat((state) => state.responses);

  const setChannelStore = (value) => {
    useUserChat.setState({
      channelStore: value,
    });
  };

  // HANDLE DELETE MESSAGE PRIVATE & GROUP
  React.useEffect(() => {
    if (notifDelete) {
      console.log("notifDelete:", notifDelete);
      const lastMessageKey = `LastMessage_${user?.id}`;
      const channel = notifDelete.channel;
      const lastAllMsg = localStorage.getItem(lastMessageKey);

      let parts = channel?.split(".");
      let firstWord = parts[0];

      switch (firstWord) {
        case "group":
          break;
        case "private":
          // UNTUK HANDLE DELETE ALL MESSAGE PRIVATE
          if (lastAllMsg) {
            const lastMessObj = JSON.parse(lastAllMsg);
            const decrypted = decryptData(lastMessObj, "KEDI");
            const newData = decrypted?.privateMessage?.filter(
              (d) => d.channel !== channel,
            );
            const encryptedData = encryptData(
              {
                classMessage: decrypted?.classMessage,
                privateMessage: newData,
                groupChatMessage: decrypted?.groupChatMessage,
              },
              "KEDI",
            );
            localStorage.setItem(lastMessageKey, JSON.stringify(encryptedData));

            const existingActivePrivate = localStorage.getItem(
              "channelActivePrivate",
            );

            if (existingActivePrivate) {
              const existingLastMessObj = JSON.parse(existingActivePrivate);

              const index = existingLastMessObj.findIndex(function (item) {
                return item === channel;
              });

              if (index !== -1) {
                console.log("ADA");
              } else {
                existingLastMessObj.push(channel);
              }

              localStorage.setItem(
                "channelActivePrivate",
                JSON.stringify(existingLastMessObj),
              );
            }

            useUserChat.setState((prevState) => {
              const newDataArray = [...prevState.privateMessage];
              if (newDataArray.length > 0) {
                const newData = newDataArray?.filter(
                  (d) => d.channel !== channel,
                );
                const new_d = newData?.map((d) => {
                  const _sort = d.timetoken
                    ? formatTime(d.timetoken)
                    : d.createdDate;
                  let obj = {
                    ...d,
                    sortTime: _sort,
                  };
                  return obj;
                });

                new_d?.sort(compareDates);
                console.log("new_d:", new_d);

                //return new_d;
                return { ...prevState, privateMessage: new_d };
              } else {
                console.log("newDataArray:", newDataArray);
                return { ...prevState, privateMessage: newDataArray };
                //return newDataArray;
              }
            });

            if (userChatAktif?.channel === channel) {
              router.push("/class");
              setUserChatAktif({});
            }
          }
          break;
        case "chat-group": {
          console.log("DELETE ALL CHAT GROUP");
          const groupData = channelStore.storedGroup.slice(-MAX_ITEMS_PER_TAB);

          (async () => {
            await getMetadata(
              groupData,
              user,
              "channelActiveGroup",
              responses,
              allClassMessage,
              privateMessage,
              groupChatMessage,
            );
            getMessageUser({ channel: channel, isDelete: true });
            setChannelStore({
              ...channelStore,
              storedGroup: groupData,
            });
          })();
          break;
        }
        default:
      }
    }

    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [notifDelete]);

  let cachedDataArray;
  const getMetadataUser = async (dataArray, _data) => {
    if (
      cachedDataArray &&
      JSON.stringify(dataArray) === JSON.stringify(cachedDataArray)
    ) {
      return cachedDataArray;
    }

    cachedDataArray = dataArray;

    if (dataArray.length > 0) {
      try {
        const response = await axios.post(
          process.env.API_BE + "/api/pubnub/metadata-detail",
          {
            channelList: dataArray,
          },
          {
            headers: {
              Authorization: `Bearer ${sessionToken}`,
            },
          },
        );

        const privateCh = response?.data?.data?.private[0];
        const merged = {
          ...privateCh,
          message: _data?.message,
          unread: 1,
          timetoken: _data?.timetoken,
        };
        return merged;
      } catch (error) {
        console.error("Gagal mengambil data metadataUsers.", error);
      }
    }
  };

  async function replaceText(dataArray, channel, newText, data, type) {
    let room = data?.channel ?? data?.actualChannel;
    let message = data.message;
    let new_channel = null;

    const firstName =
      message?.pn_gcm?.data?.custom_data?.author?.firstName ??
      message?.author?.firstName ??
      "";
    const lastName =
      message?.pn_gcm?.data?.custom_data?.author?.lastName ??
      message?.author?.lastName ??
      "";

    const fullName = lastName ? `${firstName} ${lastName}` : firstName;

    const metadata = message?.metadata;

    const chatCategory = metadata?.category;

    const isAdminSystem =
      chatCategory === "created_zoom" ||
      chatCategory === "removed_zoom" ||
      chatCategory === "created_group" ||
      chatCategory === "add_member" ||
      chatCategory === "leave_member" ||
      chatCategory === "removed_member";

    const isClassSystem =
      chatCategory === "assignment" ||
      chatCategory === "quiz" ||
      chatCategory === "uas" ||
      chatCategory === "uts" ||
      chatCategory === "material";

    const isSession = chatCategory === "session";

    let index = dataArray.findIndex(function (item) {
      return item.channel === channel;
    });

    if (index === -1) {
      new_channel = await getMetadataUser([data.channel], data);
    }

    const lastMessageKey = `LastMessage_${user?.id}`;
    const lastAllMsg = localStorage.getItem(lastMessageKey);

    if (lastAllMsg) {
      const lastMessObj = JSON.parse(lastAllMsg);
      const decrypted = decryptData(lastMessObj, "KEDI");

      const d_classMessage = decrypted?.classMessage;
      const d_privateMessage = decrypted?.privateMessage;
      const d_groupChatMessage = decrypted?.groupChatMessage;
    }

    const channelActive = JSON.parse(
      localStorage.getItem("channelActive") ?? "",
    );

    let index_all = channelActive.findIndex(function (item) {
      return item === channel;
    });

    // Jika indeks ditemukan, mengganti teks
    if (index !== -1) {
      if (dataArray && dataArray.length > 0) {
        //     dataArray[index].message = newText;
        // if (data.publisher !== userChatAktif?.sender?.id && userChatAktif?.channel !== room) {
        //     dataArray[index].unread = dataArray[index].unread + 1;
        // }
        if (
          data.publisher !== userChatAktif?.sender?.id &&
          userChatAktif?.channel !== room
        ) {
          // UNTUK PENERIMA CHAT
          console.log("JALAN PALING ATAS");

          const allMessages = [
            ...allClassMessage,
            ...privateMessage,
            ...groupChatMessage,
          ];

          const existingLastMess = localStorage.getItem(lastTimetokenKey);
          const _date = +new Date() * 10000;
          const dateNow = _date.toString();

          if (existingLastMess) {
            // console.log("JALAN PALING ATAS 2")

            const mappedMessages = allMessages
              .filter((message) => message?.unread === 0)
              .map((message) => ({
                channel: message.channel,
                timetoken: message?.timetoken ?? dateNow,
              }));
            const existingLastMessObj = JSON.parse(existingLastMess);

            mappedMessages.forEach((message) => {
              const index = existingLastMessObj.channels.indexOf(
                message.channel,
              );
              if (index !== -1) {
                existingLastMessObj.timetokens[index] = message.timetoken;
              } else {
                existingLastMessObj.channels.push(message.channel);
                existingLastMessObj.timetokens.push(message.timetoken);
              }
            });

            localStorage.setItem(
              lastTimetokenKey,
              JSON.stringify(existingLastMessObj),
            );
          } else {
            // console.log("JALAN PALING ATAS 3")

            const mappedMessages = allMessages.map((message) => ({
              channel: message.channel,
              timetoken: message?.timetoken ?? dateNow,
            }));
            const channels = mappedMessages.map((message) => message.channel);
            const timetokens = mappedMessages.map(
              (message) => message.timetoken,
            );
            const lastMess = { channels, timetokens };
            localStorage.setItem(lastTimetokenKey, JSON.stringify(lastMess));
          }
        }

        console.log("type:", type);
        switch (type) {
          case "group":
            useUserChat.setState((prevState) => {
              const newDataArray = [...prevState.allClassMessage];
              if (newDataArray[index]) {
                newDataArray[index].message = newText;
                newDataArray[index].timetoken = data.timetoken;
                if (
                  data.publisher !== userChatAktif?.sender?.id &&
                  userChatAktif?.channel !== room
                ) {
                  newDataArray[index].unread = newDataArray[index].unread + 1;
                }

                const new_d = newDataArray.map((d) => {
                  const _sort = d.timetoken
                    ? formatTime(d.timetoken)
                    : d.createdDate;
                  let obj = {
                    ...d,
                    sortTime: _sort,
                  };
                  return obj;
                });

                new_d.sort(compareDates);

                const encryptedData = encryptData(
                  { classMessage: new_d, privateMessage, groupChatMessage },
                  "KEDI",
                );
                localStorage.setItem(
                  lastMessageKey,
                  JSON.stringify(encryptedData),
                );
                return { ...prevState, allClassMessage: new_d };
              } else {
                if (new_channel?.channel) {
                  const newDataArray = [...dataArray, new_channel];

                  const new_d = newDataArray.map((d) => {
                    const _sort = d.timetoken
                      ? formatTime(d.timetoken)
                      : d.createdDate;
                    let obj = {
                      ...d,
                      sortTime: _sort,
                    };
                    return obj;
                  });

                  new_d.sort(compareDates);
                  return { ...prevState, allClassMessage: new_d };
                } else {
                  return { ...prevState, allClassMessage: dataArray };
                }
              }
            });
            break;
          case "private":
            useUserChat.setState((prevState) => {
              const existingActivePrivate = localStorage.getItem(
                "channelActivePrivate",
              );
              console.log("existingActivePrivate:", existingActivePrivate);
              if (existingActivePrivate) {
                const existingLastMessObj = JSON.parse(existingActivePrivate);

                const index = existingLastMessObj.findIndex(function (item) {
                  return item === channel;
                });

                if (index === -1) {
                  existingLastMessObj.push(channel);
                }

                localStorage.setItem(
                  "channelActivePrivate",
                  JSON.stringify(existingLastMessObj),
                );
              }
              const newDataArray = [...prevState.privateMessage];
              console.log("newDataArray:", newDataArray);
              if (newDataArray[index]) {
                newDataArray[index].message = newText;
                newDataArray[index].timetoken = data.timetoken;
                if (
                  data.publisher !== userChatAktif?.sender?.id &&
                  userChatAktif?.channel !== room
                ) {
                  newDataArray[index].unread = newDataArray[index].unread + 1;
                }

                const new_d = newDataArray.map((d) => {
                  const _sort = d.timetoken
                    ? formatTime(d.timetoken)
                    : d.createdDate;
                  let obj = {
                    ...d,
                    sortTime: _sort,
                  };
                  return obj;
                });

                new_d.sort(compareDates);

                const encryptedData = encryptData(
                  {
                    classMessage: allClassMessage,
                    privateMessage: new_d,
                    groupChatMessage,
                  },
                  "KEDI",
                );
                localStorage.setItem(
                  lastMessageKey,
                  JSON.stringify(encryptedData),
                );

                //return new_d;
                return { ...prevState, privateMessage: new_d };
              } else {
                if (new_channel?.channel) {
                  const newDataArray = [...dataArray, new_channel];

                  const new_d = newDataArray.map((d) => {
                    const _sort = d.timetoken
                      ? formatTime(d.timetoken)
                      : d.createdDate;
                    let obj = {
                      ...d,
                      sortTime: _sort,
                    };
                    return obj;
                  });

                  new_d.sort(compareDates);
                  console.log("new_d:", new_d);
                  return { ...prevState, privateMessage: new_d };
                  //return new_d;
                } else {
                  //return dataArray;
                  console.log("dataArray:", dataArray);
                  return { ...prevState, privateMessage: dataArray };
                }
              }
            });
            break;
          case "chat-group":
            useUserChat.setState((prevState) => {
              const newDataArray = [...prevState.groupChatMessage];
              if (newDataArray[index]) {
                newDataArray[index].message = newText;
                newDataArray[index].timetoken = data.timetoken;
                if (
                  data.publisher !== userChatAktif?.sender?.id &&
                  userChatAktif?.channel !== room
                ) {
                  newDataArray[index].unread = newDataArray[index].unread + 1;
                }

                const new_d = newDataArray.map((d) => {
                  const _sort = d.timetoken
                    ? formatTime(d.timetoken)
                    : d.createdDate;
                  let obj = {
                    ...d,
                    sortTime: _sort,
                  };
                  return obj;
                });

                new_d.sort(compareDates);
                let fnew_d;

                const isLeaved = new_d.filter(
                  (d) =>
                    d?.message?.metadata?.category === "leave_member" &&
                    d?.message?.metadata?.uuid === user?.uuid,
                );
                if (isLeaved?.length > 0) {
                  const leave_member = isLeaved[0].channel;
                  fnew_d = new_d.filter((d) => d.channel !== leave_member);
                } else {
                  fnew_d = new_d;
                }

                console.log("fnew_d:", fnew_d);
                console.log("new_d:", new_d);
                const encryptedData = encryptData(
                  {
                    classMessage: allClassMessage,
                    privateMessage,
                    groupChatMessage: fnew_d,
                  },
                  "KEDI",
                );
                localStorage.setItem(
                  lastMessageKey,
                  JSON.stringify(encryptedData),
                );

                //return new_d;
                return { ...prevState, groupChatMessage: fnew_d };
              } else {
                //return dataArray;
                return { ...prevState, groupChatMessage: dataArray };
              }
            });
            break;
          default:
        }
      }

      if (
        data.publisher !== userChatAktif?.sender?.id &&
        userChatAktif?.channel !== room
      ) {
        let parts = room?.split(".");
        let firstWord = parts ? parts[0] : null;
        let sent_to;
        let type_msg = message?.pn_apns?.aps?.alert?.body ?? "sent a message";

        switch (firstWord) {
          case "group":
            sent_to = type_msg + " in class chat";
            break;
          case "private":
            sent_to = type_msg;
            break;
          case "chat-group":
            sent_to = type_msg + " in group chat";
            break;
          default:
            sent_to = type_msg;
        }

        toastSoner(
          <div
            onClick={() => {
              if (firstWord === "private") {
                const updatedAllData = [...privateMessage];
                const filtered = privateMessage.filter(
                  (dt) => dt.channel === channel,
                );
                const d = filtered?.length > 0 ? filtered[0] : null;

                if (!d) return null;
                const readMessage = {
                  ...d,
                  unread: 0,
                };
                const indexItem = privateMessage.findIndex(
                  (item) => item.channel === channel,
                );
                updatedAllData[indexItem] = readMessage;

                const lastMessStr = localStorage.getItem(lastTimetokenKey);
                let updatedAllDataLS = {};

                if (lastMessStr) {
                  const lastMessObj = JSON.parse(lastMessStr);
                  updatedAllDataLS = {
                    channels: [...lastMessObj.channels],
                    timetokens: [...lastMessObj.timetokens],
                  };
                  const indexItemLS = lastMessObj.channels.findIndex(
                    (item) => item === channel,
                  );
                  updatedAllDataLS.timetokens[indexItemLS] = data.timetoken;
                }

                if (d?.unread > 0) {
                  setPrivateMessage(updatedAllData);
                  localStorage.setItem(
                    lastTimetokenKey,
                    JSON.stringify(updatedAllDataLS),
                  );
                }

                getMessageUser({ channel });
                if (isSM) {
                  setIsMobileView(true);
                }
                if (router.pathname !== "/class") {
                  router.push("/class");
                }
                setChatMode("private");
                setTabChat("channelActivePrivate");
                setUserChatAktif({
                  channel: channel,
                  receiver: {
                    id: message?.pn_gcm?.data?.custom_data?.author?.id,
                    name: message?.pn_gcm?.data?.custom_data?.author?.firstName,
                    photo: message?.pn_gcm?.data?.custom_data?.author?.imageUrl,
                  },
                  sender: {
                    id: user.uuid,
                    name: user.fullName,
                    photo: user.photo,
                  },
                });
              }
            }}
            className="flex items-center  rounded-lg px-1 py-1 cursor-pointer"
          >
            <div className="relative w-14 h-14 flex flex-shrink-0 items-end">
              <Image
                width={60}
                height={60}
                src={
                  message?.pn_gcm?.data?.custom_data?.author?.imageUrl ||
                  `/static/images/logokedi.svg`
                }
                className="absolute inset-0 h-full w-full object-cover rounded-xl shadow-md border border-gray-300"
                alt=""
              />
              <span className="absolute h-4 w-4 bg-green-400 rounded-full bottom-0 right-0 border-2 border-white" />
            </div>
            <div className="ml-3">
              <span className="font-semibold tracking-tight text-xs">
                {fullName}
              </span>{" "}
              <span className="text-xs leading-none opacity-50">{sent_to}</span>
              <p className="text-[13px] leading-4 pt-2 italic opacity-70">
                {message?.pn_gcm?.data?.custom_data?.text ||
                  message?.pn_gcm?.data?.custom_data?.name ||
                  message?.metadata?.title}
                {isAdminSystem ? message?.text : ""}
              </p>
              <span className="text-[10px] text-blue-500 font-medium leading-4 opacity-75">
                a few seconds ago
              </span>
            </div>
          </div>,
        );
      } else {
        // console.log("JALAN PALING BAWAH")
        const allMessages = [
          ...allClassMessage,
          ...privateMessage,
          ...groupChatMessage,
        ];
        const _date = +new Date() * 10000;
        const dateNow = _date.toString();

        const mappedMessages = allMessages
          .filter((message) => message?.unread === 0)
          .map((message) => ({
            channel: message.channel,
            timetoken: message?.timetoken ?? dateNow,
          }));

        const existingLastMess = localStorage.getItem(lastTimetokenKey);

        if (existingLastMess) {
          // console.log("JALAN PALING BAWAH 1")
          const existingLastMessObj = JSON.parse(existingLastMess);

          const index = existingLastMessObj.channels.findIndex(function (item) {
            return item === channel;
          });

          if (index !== -1) {
            existingLastMessObj.timetokens[index] = data.timetoken;
          } else {
            existingLastMessObj.channels.push(channel);
            existingLastMessObj.timetokens.push(data.timetoken);
          }

          localStorage.setItem(
            lastTimetokenKey,
            JSON.stringify(existingLastMessObj),
          );
        } else {
          // console.log("JALAN PALING BAWAH 2")
          const channels = mappedMessages.map((message) => message.channel);
          const timetokens = mappedMessages.map((message) => message.timetoken);
          const lastMess = { channels, timetokens };

          const index = lastMess.channels.findIndex(function (item) {
            return item === channel;
          });

          if (index !== -1) {
            lastMess.timetokens[index] = data.timetoken;
          } else {
            lastMess.channels.push(channel);
            lastMess.timetokens.push(data.timetoken);
          }
          localStorage.setItem(lastTimetokenKey, JSON.stringify(lastMess));
        }
      }
    } else {
      if (
        data.publisher !== userChatAktif?.sender?.id &&
        userChatAktif?.channel !== room
      ) {
        // UNTUK PENERIMA CHAT
        console.log("JALAN PALING ATAS");

        const allMessages = [
          ...allClassMessage,
          ...privateMessage,
          ...groupChatMessage,
        ];

        const existingLastMess = localStorage.getItem(lastTimetokenKey);
        console.log("existingLastMess:", existingLastMess);
        const _date = +new Date() * 10000;
        const dateNow = _date.toString();

        if (existingLastMess) {
          // console.log("JALAN PALING ATAS 2")

          const mappedMessages = allMessages
            .filter((message) => message?.unread === 0)
            .map((message) => ({
              channel: message.channel,
              timetoken: message?.timetoken ?? dateNow,
            }));
          const existingLastMessObj = JSON.parse(existingLastMess);

          mappedMessages.forEach((message) => {
            const index = existingLastMessObj.channels.indexOf(message.channel);
            if (index !== -1) {
              existingLastMessObj.timetokens[index] = message.timetoken;
            } else {
              existingLastMessObj.channels.push(message.channel);
              existingLastMessObj.timetokens.push(message.timetoken);
            }
          });

          localStorage.setItem(
            lastTimetokenKey,
            JSON.stringify(existingLastMessObj),
          );
        } else {
          // console.log("JALAN PALING ATAS 3")

          const mappedMessages = allMessages.map((message) => ({
            channel: message.channel,
            timetoken: message?.timetoken ?? dateNow,
          }));
          const channels = mappedMessages.map((message) => message.channel);
          const timetokens = mappedMessages.map((message) => message.timetoken);
          const lastMess = { channels, timetokens };
          localStorage.setItem(lastTimetokenKey, JSON.stringify(lastMess));
        }
      }

      console.log("type:", type);
      switch (type) {
        case "group":
          useUserChat.setState((prevState) => {
            const newDataArray = [...prevState.allClassMessage];
            if (new_channel?.channel) {
              const newDataArray = [...dataArray, new_channel];

              const new_d = newDataArray.map((d) => {
                const _sort = d.timetoken
                  ? formatTime(d.timetoken)
                  : d.createdDate;
                let obj = {
                  ...d,
                  sortTime: _sort,
                };
                return obj;
              });

              new_d.sort(compareDates);
              return { ...prevState, allClassMessage: new_d };
            } else {
              return { ...prevState, allClassMessage: dataArray };
            }
          });
          break;
        case "private":
          useUserChat.setState((prevState) => {
            const existingActivePrivate = localStorage.getItem(
              "channelActivePrivate",
            );
            if (existingActivePrivate) {
              const existingLastMessObj = JSON.parse(existingActivePrivate);

              const index = existingLastMessObj.findIndex(function (item) {
                return item === channel;
              });

              if (index === -1) {
                existingLastMessObj.push(channel);
              }

              localStorage.setItem(
                "channelActivePrivate",
                JSON.stringify(existingLastMessObj),
              );
            }
            const newDataArray = [...prevState.privateMessage];
            if (new_channel?.channel) {
              const newDataArray = [...dataArray, new_channel];

              const new_d = newDataArray.map((d) => {
                const _sort = d.timetoken
                  ? formatTime(d.timetoken)
                  : d.createdDate;
                let obj = {
                  ...d,
                  sortTime: _sort,
                };
                return obj;
              });

              new_d.sort(compareDates);
              return { ...prevState, privateMessage: new_d };
              //return new_d;
            }
          });
          break;
        case "chat-group":
          useUserChat.setState((prevState) => {
            const newDataArray = [...prevState.groupChatMessage];
            if (newDataArray[index]) {
              newDataArray[index].message = newText;
              newDataArray[index].timetoken = data.timetoken;
              if (
                data.publisher !== userChatAktif?.sender?.id &&
                userChatAktif?.channel !== room
              ) {
                newDataArray[index].unread = newDataArray[index].unread + 1;
              }

              const new_d = newDataArray.map((d) => {
                const _sort = d.timetoken
                  ? formatTime(d.timetoken)
                  : d.createdDate;
                let obj = {
                  ...d,
                  sortTime: _sort,
                };
                return obj;
              });

              new_d.sort(compareDates);
              let fnew_d;

              const isLeaved = new_d.filter(
                (d) =>
                  d?.message?.metadata?.category === "leave_member" &&
                  d?.message?.metadata?.uuid === user?.uuid,
              );
              if (isLeaved?.length > 0) {
                const leave_member = isLeaved[0].channel;
                fnew_d = new_d.filter((d) => d.channel !== leave_member);
              } else {
                fnew_d = new_d;
              }

              console.log("fnew_d:", fnew_d);
              console.log("new_d:", new_d);
              const encryptedData = encryptData(
                {
                  classMessage: allClassMessage,
                  privateMessage,
                  groupChatMessage: fnew_d,
                },
                "KEDI",
              );
              localStorage.setItem(
                lastMessageKey,
                JSON.stringify(encryptedData),
              );

              //return new_d;
              return { ...prevState, groupChatMessage: fnew_d };
            }
          });
          break;
        default:
      }
      console.log("Channel tidak ditemukan:", channel);
    }

    (async () => {
      const lastMessStr = localStorage.getItem(lastTimetokenKey);
      const pubnub = new PubNub({
        publishKey: user?.publishKey,
        subscribeKey: user?.subscribeKey,
        uuid: user?.uuid,
      });

      if (lastMessStr) {
        try {
          const lastMessObj = JSON.parse(lastMessStr);
          const channels = lastMessObj.channels.slice(0, 100);
          const timetokens = lastMessObj.timetokens.slice(0, 100);

          const res = await pubnub.messageCounts({
            channels: channels,
            channelTimetokens: timetokens,
          });

          setResponses(res);
        } catch (error) {
          console.error(error);
        }
      }
    })();
  }

  return (
    <>
      <DropdownMenu>
        <DropdownMenuTrigger className="order-1 sm:order-none h-[36px] sm:h-[40px] w-[36px]  sm:w-[40px] rounded-lg relative">
          {notifMessage?.filter((d) => d.publisher !== user.uuid).length >
            0 && (
            <span className="absolute py-0.5 px-1.5 bg-green-400 rounded-full -top-1 right-1 border-2 border-white text-sm font-medium">
              {notifMessage?.filter((d) => d.publisher !== user.uuid).length}
            </span>
          )}
          <svg
            xmlns="http://www.w3.org/2000/svg"
            viewBox="0 0 32 32"
            className="sm:h-6 sm:w-6 w-5 h-5"
          >
            <path
              fill="currentColor"
              d="M28.707 19.293L26 16.586V13a10.014 10.014 0 0 0-9-9.95V1h-2v2.05A10.014 10.014 0 0 0 6 13v3.586l-2.707 2.707A1 1 0 0 0 3 20v3a1 1 0 0 0 1 1h7v.777a5.152 5.152 0 0 0 4.5 5.199A5.006 5.006 0 0 0 21 25v-1h7a1 1 0 0 0 1-1v-3a1 1 0 0 0-.293-.707ZM19 25a3 3 0 0 1-6 0v-1h6Zm8-3H5v-1.586l2.707-2.707A1 1 0 0 0 8 17v-4a8 8 0 0 1 16 0v4a1 1 0 0 0 .293.707L27 20.414Z"
            />
          </svg>
        </DropdownMenuTrigger>
        <DropdownMenuContent className="w-[90vw] sm:w-[500px] mx-5 sm:mr-8">
          <Tabs value={value} onValueChange={setValue}>
            <TabsList className="m-2 grid grid-cols-2">
              <TabsTrigger value="class">Notifikasi</TabsTrigger>
              <TabsTrigger value="contacts">Status Transaksi</TabsTrigger>
            </TabsList>
            <TabsContent
              forceMount
              hidden={value !== "class"}
              value="class"
              className="h-[60vh] overflow-y-auto px-1.5 space-y-2 pt-1.5 border rounded-lg mx-2 mb-2 mt-3 shadow-sm"
            >
              <NotifChat
                user={user}
                notifMessage={notifMessage}
                notifDelete={notifDelete}
              />
            </TabsContent>
            <TabsContent
              forceMount
              hidden={value !== "contacts"}
              value="contacts"
              className="px-1.5 pb-4"
            >
              <NotifPayment user={user} notifFinance={notifFinance} />
            </TabsContent>
          </Tabs>
        </DropdownMenuContent>
      </DropdownMenu>
    </>
  );
});

const handleSignOut = () => {
  void signOut();
};

const LinkNavbar = ({ user, lang, custom }: any) => {
  const sessionToken = user?.accessToken;
  const router = useRouter();
  const disabled = false;
  const isSM = useMediaQuery({ query: "(max-width: 768)" });

  return <div>huda </div>;
};

const PubNubMain = React.memo(
  ({ user, setNotifCall, setIsTypingCall, setNotifDeleteCall }) => {
    useEffect(() => {
      if (user !== undefined || user !== null) {
        console.log("MOUNT");
        subscribeChannels();
      }

      return () => {
        console.log("UNMOUNT");
        unsubscribeChannels(subscribeChannels());
      };
      // eslint-disable-next-line react-hooks/exhaustive-deps
    }, []);

    const unsubscribeChannels = (listener) => {
      const pubnub = new PubNub({
        publishKey: user?.publishKey,
        subscribeKey: user?.subscribeKey,
        uuid: user?.uuid,
      });
      pubnub.removeListener(listener);
      pubnub.unsubscribe({
        channels: ["private.*", "group.*", "chat-group.*"],
        withPresence: true,
      });
    };

    const subscribeChannels = () => {
      const pubnub = new PubNub({
        publishKey: user?.publishKey,
        subscribeKey: user?.subscribeKey,
        uuid: user?.uuid,
      });

      pubnub.subscribe({
        channels: ["private.*", "group.*", "chat-group.*"],
        withPresence: true,
      });

      const listener = {
        signal(sig) {
          if (sig?.message === "delete_all") {
            setNotifDeleteCall(sig);
          } else {
            setIsTypingCall(sig);
          }
        },
        message(msg) {
          setNotifCall(msg);
        },
      };
      pubnub.addListener(listener);
      return listener;
    };

    return <div className="hidden">PUBNUB</div>;
  },
);

const PubNubData = React.memo(({ user, linkRef }) => {
  const initPubNub = useUserChat((state) => state.initPubNub);

  const setallClassMessage = (value) => {
    useUserChat.setState({
      allClassMessage: value,
    });
  };
  const setPrivateMessage = (value) => {
    useUserChat.setState({
      privateMessage: value,
    });
  };
  const setGroupChatMessage = (value) => {
    useUserChat.setState({
      groupChatMessage: value,
    });
  };

  const setChannelStore = (value) => {
    useUserChat.setState({
      channelStore: value,
    });
  };

  if (!initPubNub) {
    if (user) {
      (async () => {
        if (linkRef.current) {
          linkRef.current.style.pointerEvents = "none";
        }
        const msg = await getPubNub(user);

        setPrivateMessage(msg?.privateMessage);
        setallClassMessage(msg?.classMessage);
        setGroupChatMessage(msg?.groupChatMessage);

        const storagePubnub = JSON.parse(
          localStorage.getItem("sessionPubnub") ?? "",
        );

        const groupChannels = storagePubnub?.channels?.filter((channelName) =>
          channelName.startsWith("chat-group."),
        );

        const classChannels = storagePubnub?.channels?.filter((channelName) =>
          channelName.startsWith("group."),
        );

        let initialClass = [...classChannels];
        let initialGroup = [...groupChannels];

        let dataPrivate = localStorage.getItem("channelActivePrivate");
        let initialPrivate = dataPrivate ? JSON.parse(dataPrivate) : [];

        // Set semua data yang tersedia dari sumber eksternal

        setChannelStore({
          storedGroup: initialGroup,
          storedPrivate: initialPrivate,
          storedClass: initialClass,
        });

        if (linkRef.current) {
          linkRef.current.style.pointerEvents = "auto";
        }
        useUserChat.setState({
          initPubNub: true,
        });
      })();
    }
  }
  return null;
});

export const Navbar = ({ user }: any) => {
  const router = useRouter();
  const navRef = useRef(null);
  const linkRef = useRef(null);

  const { locales, locale: activeLocale, query } = router;

  const locale: "en" | "id" = router.locale as "en" | "id";
  const lang = trans[locale].home;
  const channelId = query.message as string;

  const otherLocales = (locales || []).filter(
    (locale) => locale !== activeLocale,
  );

  const isSM = useMediaQuery({ query: "(max-width: 899px)" });

  const isMobileView = useUserChat((state) => state.isMobileView);
  const notifMessage = useUserChat((state) => state.notifMessage);

  const setallClassMessage = (value) => {
    useUserChat.setState({
      allClassMessage: value,
    });
  };
  const setPrivateMessage = (value) => {
    useUserChat.setState({
      privateMessage: value,
    });
  };
  const setGroupChatMessage = (value) => {
    useUserChat.setState({
      groupChatMessage: value,
    });
  };

  const setChannelStore = (value) => {
    useUserChat.setState({
      channelStore: value,
    });
  };

  const setNotifCall = useCallback((msg) => {
    const storagePubnub = JSON.parse(
      localStorage.getItem("sessionPubnub") ?? "",
    );
    const channelActive = storagePubnub?.channels
      ? storagePubnub?.channels
      : [];
    console.log("msg:", msg);
    if (Array.isArray(channelActive) && channelActive.includes(msg.channel)) {
      console.log("EVENT INC");
      useUserChat.setState({
        notifMessage: [...notifMessage, msg],
      });
    } else if (
      Array.isArray(channelActive) &&
      !channelActive.includes(msg.channel)
    ) {
      (async () => {
        console.log("EVENT EXC");

        let parts = msg?.channel?.split(".");
        let firstWord = parts ? parts[0] : null;

        const _msg = await getPubNub(user);

        setPrivateMessage(_msg?.privateMessage);
        setallClassMessage(_msg?.classMessage);
        setGroupChatMessage(_msg?.groupChatMessage);

        const storagePubnub = JSON.parse(
          localStorage.getItem("sessionPubnub") ?? "",
        );

        const groupChannels = storagePubnub?.channels?.filter((channelName) =>
          channelName.startsWith("chat-group."),
        );

        const classChannels = storagePubnub?.channels?.filter((channelName) =>
          channelName.startsWith("group."),
        );

        let initialClass = [...classChannels];
        let initialGroup = [...groupChannels];

        let dataPrivate = localStorage.getItem("channelActivePrivate");
        let initialPrivate = dataPrivate ? JSON.parse(dataPrivate) : [];

        // Set semua data yang tersedia dari sumber eksternal

        setChannelStore({
          storedGroup: initialGroup,
          storedPrivate: initialPrivate,
          storedClass: initialClass,
        });
        //localStorage.setItem("lastTab", "channelActiveGroup");
        switch (firstWord) {
          case "group":
            useUserChat.setState({
              tabChat: "channelActiveClass",
            });
            break;
          case "private":
            getMessageUser({ channel: msg?.channel, isDelete: true });
            useUserChat.setState({
              tabChat: "channelActivePrivate",
            });
            break;
          case "chat-group":
            useUserChat.setState({
              tabChat: "channelActiveGroup",
            });
            break;
          default:
            useUserChat.setState({
              tabChat: "channelActiveClass",
            });
        }
      })();
    } else {
      console.log("EVENT NOT EXC INC");
    }
  }, []);

  const setIsTypingCall = useCallback((sig) => {
    useUserChat.setState({
      isTyping: sig,
    });
  }, []);

  const setNotifDeleteCall = useCallback((sig) => {
    console.log("sig:", sig);
    useUserChat.setState({
      notifDelete: sig,
    });
  }, []);

  useEffect(() => {
    if (navRef.current) {
      if (isMobileView) {
        navRef.current.classList.add("hidden");
      } else {
        navRef.current.classList.remove("hidden");
      }
    }
    console.log("RENDER");
  }, [isMobileView]);

  useEffect(() => {
    if (!isSM && router && !router.pathname === "/class") {
      if (isMobileView) {
        useUserChat.setState({
          isMobileView: false,
        });
      }
    }
    if (!isSM) {
      if (isMobileView) {
        useUserChat.setState({
          isMobileView: false,
        });
      }
    }
    if (
      channelId === undefined &&
      router &&
      router.pathname === "/class" &&
      isSM
    ) {
      if (isMobileView) {
        useUserChat.setState({
          isMobileView: false,
        });

        useUserChat.setState({
          userChatAktif: {},
        });
      }
    }
  }, [isSM, router]);

  useEffect(() => {
    if (channelId === undefined && router && router.pathname !== "/class") {
      useUserChat.setState({
        userChatAktif: {},
      });
    }
  }, [router]);

  return (
    <nav ref={navRef}>
      <PubNubData user={user} linkRef={linkRef} />
      <PubNubMain
        user={user}
        setNotifCall={setNotifCall}
        setIsTypingCall={setIsTypingCall}
        setNotifDeleteCall={setNotifDeleteCall}
      />
      <div className="fixed top-0 z-20 bg-white h-[60px] sm:h-[70px]  items-center w-full grid grid-cols-2 md:grid-cols-3 gap-5  px-4 sm:px-6 border-b shadow-md pb-0.5">
        <Link href="/" className="hidden sm:inline relative max-w-[208px]">
          <Image
            src="/static/images/logomain.svg"
            style={{ width: 208, height: 66 }}
            width={208}
            height={66}
            priority
            className="w-auto h-auto"
            alt="Logo Kedi"
          />
        </Link>
        <Link href="/" className="inline sm:hidden  max-h-[45px] max-w-[150px]">
          <Image
            src="/static/images/logotext.svg"
            priority
            width="0"
            height="0"
            sizes="100vw"
            className="pt-2 w-full h-auto"
            alt="Logo Kedi"
          />
        </Link>
        <div className="hidden md:flex items-center justify-center gap-x-3 lg:gap-x-5">
          <Link
            href="/"
            className={` ${
              router.pathname === "/" ? "text-[#9D21E6]" : "text-gray-400"
            } flex flex-col gap-0.5 items-center `}
          >
            <svg
              viewBox="0 0 34 29"
              fill="none"
              xmlns="http://www.w3.org/2000/svg"
              className="md:w-[30px] w-[34px] md:h-[30px] lg:h-[34px]"
            >
              <path
                d="M17 4.48333L25.3333 11.9833V25H22V15H12V25H8.66666V11.9833L17 4.48333ZM17 0L0.333328 15H5.33333V28.3333H15.3333V18.3333H18.6667V28.3333H28.6667V15H33.6667L17 0Z"
                fill="currentColor"
              />
            </svg>
            <span className="text-sm font-medium">{lang.home}</span>
          </Link>
          <Link
            href="/dashboard"
            className={` ${
              router.pathname.includes("/dashboard")
                ? "text-[#9D21E6]"
                : "text-gray-400"
            } relative flex flex-col gap-0.5 items-center `}
          >
            <svg
              className="md:w-[30px] w-[34px] md:h-[30px] lg:h-[34px]"
              viewBox="0 0 40 40"
              fill="none"
              xmlns="http://www.w3.org/2000/svg"
            >
              <g clipPath="url(#clip0_955_262)">
                <path
                  d="M31.6667 5H8.33333C6.5 5 5 6.5 5 8.33333V31.6667C5 33.5 6.5 35 8.33333 35H31.6667C33.5 35 35 33.5 35 31.6667V8.33333C35 6.5 33.5 5 31.6667 5ZM31.6667 31.6667H8.33333V8.33333H31.6667V31.6667ZM11.6667 16.6667H15V28.3333H11.6667V16.6667ZM18.3333 11.6667H21.6667V28.3333H18.3333V11.6667ZM25 21.6667H28.3333V28.3333H25V21.6667Z"
                  fill="currentColor"
                />
              </g>
              <defs>
                <clipPath id="clip0_955_262">
                  <rect width={40} height={40} fill="white" />
                </clipPath>
              </defs>
            </svg>
            <span className="text-sm font-medium">{lang.dashboard}</span>
          </Link>
          <Link
            ref={linkRef}
            href="/class"
            className={` ${
              router.pathname === "/class" ? "text-[#9D21E6]" : "text-gray-400"
            } relative flex flex-col gap-0.5 items-center `}
          >
            <Image
              src="/static/images/logokedi.svg"
              width={40}
              height={40}
              alt="Logo Kedi"
              className="absolute -top-0"
            />
            <div className="md:w-[30px] w-[34px] md:h-[30px] lg:h-[34px]" />

            <span className="text-sm font-medium">{lang.class}</span>
          </Link>
          <Link
            href="/"
            className="flex flex-col gap-0.5 items-center text-gray-400"
          >
            <svg
              className="md:w-[30px] w-[34px] md:h-[30px] lg:h-[34px]"
              viewBox="0 0 40 40"
              fill="none"
              xmlns="http://www.w3.org/2000/svg"
            >
              <g clipPath="url(#clip0_955_268)">
                <path
                  d="M11.6667 18.3333H15V21.6666H11.6667V18.3333ZM35 9.99998V33.3333C35 35.1666 33.5 36.6666 31.6667 36.6666H8.33333C6.48333 36.6666 5 35.1666 5 33.3333L5.01667 9.99998C5.01667 8.16665 6.48333 6.66665 8.33333 6.66665H10V3.33331H13.3333V6.66665H26.6667V3.33331H30V6.66665H31.6667C33.5 6.66665 35 8.16665 35 9.99998ZM8.33333 13.3333H31.6667V9.99998H8.33333V13.3333ZM31.6667 33.3333V16.6666H8.33333V33.3333H31.6667ZM25 21.6666H28.3333V18.3333H25V21.6666ZM18.3333 21.6666H21.6667V18.3333H18.3333V21.6666Z"
                  fill="#9D9797"
                />
              </g>
              <defs>
                <clipPath id="clip0_955_268">
                  <rect width="40" height="40" fill="white" />
                </clipPath>
              </defs>
            </svg>
            <span className="text-sm font-medium">{lang.calendar}</span>
          </Link>
          <Link
            href="/"
            className="flex flex-col gap-0.5 items-center text-gray-400"
          >
            <svg
              className="md:w-[30px] w-[34px] md:h-[30px] lg:h-[34px]"
              viewBox="0 0 40 40"
              fill="none"
              xmlns="http://www.w3.org/2000/svg"
            >
              <g clipPath="url(#clip0_955_274)">
                <path
                  d="M36.5 14.8167L34.75 7.53333C34.3833 6.03333 33.0833 5 31.5667 5H8.41666C6.91666 5 5.6 6.05 5.25 7.53333L3.5 14.8167C3.1 16.5167 3.46666 18.25 4.53333 19.6167C4.66666 19.8 4.85 19.9333 5 20.1V31.6667C5 33.5 6.5 35 8.33333 35H31.6667C33.5 35 35 33.5 35 31.6667V20.1C35.15 19.95 35.3333 19.8 35.4667 19.6333C36.5333 18.2667 36.9167 16.5167 36.5 14.8167ZM31.5167 8.31667L33.2667 15.6C33.4333 16.3 33.2833 17 32.85 17.55C32.6167 17.85 32.1167 18.3333 31.2833 18.3333C30.2667 18.3333 29.3833 17.5167 29.2667 16.4333L28.3 8.33333L31.5167 8.31667ZM21.6667 8.33333H24.9333L25.8333 15.8667C25.9167 16.5167 25.7167 17.1667 25.2833 17.65C24.9167 18.0833 24.3833 18.3333 23.7 18.3333C22.5833 18.3333 21.6667 17.35 21.6667 16.15V8.33333ZM14.15 15.8667L15.0667 8.33333H18.3333V16.15C18.3333 17.35 17.4167 18.3333 16.1833 18.3333C15.6167 18.3333 15.1 18.0833 14.7 17.65C14.2833 17.1667 14.0833 16.5167 14.15 15.8667ZM6.73333 15.6L8.41666 8.33333H11.7L10.7333 16.4333C10.6 17.5167 9.73333 18.3333 8.71666 18.3333C7.9 18.3333 7.38333 17.85 7.16666 17.55C6.71666 17.0167 6.56666 16.3 6.73333 15.6ZM8.33333 31.6667V21.6167C8.46666 21.6333 8.58333 21.6667 8.71666 21.6667C10.1667 21.6667 11.4833 21.0667 12.45 20.0833C13.45 21.0833 14.7833 21.6667 16.3 21.6667C17.75 21.6667 19.05 21.0667 20.0167 20.1167C21 21.0667 22.3333 21.6667 23.8333 21.6667C25.2333 21.6667 26.5667 21.0833 27.5667 20.0833C28.5333 21.0667 29.85 21.6667 31.3 21.6667C31.4333 21.6667 31.55 21.6333 31.6833 21.6167V31.6667H8.33333Z"
                  fill="currentColor"
                />
              </g>
              <defs>
                <clipPath id="clip0_955_274">
                  <rect width="40" height="40" fill="white" />
                </clipPath>
              </defs>
            </svg>
            <span className="text-sm font-medium">{lang.market}</span>
          </Link>
        </div>
        <div className="flex items-center justify-end gap-x-5">
          <SearchMenu user={user} lang={lang} />
          <DropdownMenu>
            <DropdownMenuTrigger className="order-1 sm:order-none h-[36px] sm:h-[40px] w-[36px]  sm:w-[40px] rounded-lg relative">
              <Image
                width={60}
                height={60}
                src={user?.photo ? user?.photo : `/static/images/logokedi.svg`}
                className="absolute inset-0 h-full w-full object-cover  rounded-lg  shadow-md"
                alt=""
              />
            </DropdownMenuTrigger>
            <DropdownMenuContent className="mr-8">
              <DropdownMenuLabel className="text-center">
                Menu Profile
              </DropdownMenuLabel>
              <DropdownMenuSeparator />
              {otherLocales.map((locale) => {
                const { pathname, query, asPath } = router;
                return (
                  <DropdownMenuItem
                    key={locale}
                    className="flex w-full items-center gap-1.5 rounded-md p-2 text-sm text-gray-900 hover:bg-purple-100 cursor-pointer"
                    role="menuitem"
                    onClick={() => {
                      router.push(
                        {
                          pathname,
                          query,
                        },
                        asPath,
                        { locale },
                      );
                    }}
                  >
                    <svg
                      xmlns="http://www.w3.org/2000/svg"
                      fill="none"
                      viewBox="0 0 24 24"
                      strokeWidth={1.5}
                      stroke="currentColor"
                      className="w-5 h-5"
                    >
                      <path
                        strokeLinecap="round"
                        strokeLinejoin="round"
                        d="m10.5 21 5.25-11.25L21 21m-9-3h7.5M3 5.621a48.474 48.474 0 0 1 6-.371m0 0c1.12 0 2.233.038 3.334.114M9 5.25V3m3.334 2.364C11.176 10.658 7.69 15.08 3 17.502m9.334-12.138c.896.061 1.785.147 2.666.257m-4.589 8.495a18.023 18.023 0 0 1-3.827-5.802"
                      />
                    </svg>
                    <span className="text-sm ">
                      {locale === "en" ? "ID" : "EN"}
                    </span>
                  </DropdownMenuItem>
                );
              })}
              <DropdownMenuItem className="hover:bg-purple-100" asChild>
                <Link
                  href="/profile"
                  className="flex w-full items-center gap-1.5 rounded-md p-2 text-sm hover:bg-purple-100"
                  role="menuitem"
                >
                  <svg
                    xmlns="http://www.w3.org/2000/svg"
                    viewBox="0 0 32 32"
                    strokeWidth={1.5}
                    className="h-5 w-5"
                  >
                    <path
                      fill="currentColor"
                      d="M16 4a5 5 0 1 1-5 5a5 5 0 0 1 5-5m0-2a7 7 0 1 0 7 7a7 7 0 0 0-7-7zm10 28h-2v-5a5 5 0 0 0-5-5h-6a5 5 0 0 0-5 5v5H6v-5a7 7 0 0 1 7-7h6a7 7 0 0 1 7 7z"
                    ></path>
                  </svg>
                  {lang.viewProfile}
                </Link>
              </DropdownMenuItem>
              <DropdownMenuItem className="hover:bg-purple-100" asChild>
                <button
                  onClick={() => handleSignOut()}
                  className="flex w-full items-center gap-1.5 rounded-md p-2 text-sm hover:bg-purple-100"
                  role="menuitem"
                >
                  <svg
                    xmlns="http://www.w3.org/2000/svg"
                    fill="none"
                    viewBox="0 0 24 24"
                    strokeWidth={1.5}
                    stroke="currentColor"
                    className="h-5 w-5"
                  >
                    <path
                      strokeLinecap="round"
                      strokeLinejoin="round"
                      d="M15.75 9V5.25A2.25 2.25 0 0013.5 3h-6a2.25 2.25 0 00-2.25 2.25v13.5A2.25 2.25 0 007.5 21h6a2.25 2.25 0 002.25-2.25V15m3 0l3-3m0 0l-3-3m3 3H9"
                    />
                  </svg>
                  {lang.signout}
                </button>
              </DropdownMenuItem>
            </DropdownMenuContent>
          </DropdownMenu>
          <PopupNotif user={user} />
        </div>
      </div>
      <div className="md:hidden fixed bottom-0 left-0 z-50 w-full h-[60px] bg-white border-t border-gray-200 ">
        <div className="grid h-full max-w-lg grid-cols-5 mx-auto font-medium pt-2.5">
          <Link
            href="/"
            className={` ${
              router.pathname === "/" ? "text-[#9D21E6]" : "text-gray-500"
            } flex flex-col gap-0.5 items-center `}
          >
            <svg
              viewBox="0 0 34 29"
              fill="none"
              xmlns="http://www.w3.org/2000/svg"
              className="w-[26px] h-[26px]"
            >
              <path
                d="M17 4.48333L25.3333 11.9833V25H22V15H12V25H8.66666V11.9833L17 4.48333ZM17 0L0.333328 15H5.33333V28.3333H15.3333V18.3333H18.6667V28.3333H28.6667V15H33.6667L17 0Z"
                fill="currentColor"
              />
            </svg>
            <span className="text-sm font-medium">{lang.home}</span>
          </Link>
          <Link
            href="/dashboard"
            className={` ${
              router.pathname.includes("/dashboard")
                ? "text-[#9D21E6]"
                : "text-gray-500"
            } flex flex-col gap-0.5 items-center `}
          >
            <svg
              className="w-[26px] h-[26px]"
              viewBox="0 0 40 40"
              fill="none"
              xmlns="http://www.w3.org/2000/svg"
            >
              <g clipPath="url(#clip0_955_262)">
                <path
                  d="M31.6667 5H8.33333C6.5 5 5 6.5 5 8.33333V31.6667C5 33.5 6.5 35 8.33333 35H31.6667C33.5 35 35 33.5 35 31.6667V8.33333C35 6.5 33.5 5 31.6667 5ZM31.6667 31.6667H8.33333V8.33333H31.6667V31.6667ZM11.6667 16.6667H15V28.3333H11.6667V16.6667ZM18.3333 11.6667H21.6667V28.3333H18.3333V11.6667ZM25 21.6667H28.3333V28.3333H25V21.6667Z"
                  fill="currentColor"
                />
              </g>
              <defs>
                <clipPath id="clip0_955_262">
                  <rect width={40} height={40} fill="white" />
                </clipPath>
              </defs>
            </svg>
            <span className="text-sm font-medium">{lang.dashboard}</span>
          </Link>
          <Link
            ref={linkRef}
            href="/class"
            className={` ${
              router.pathname === "/class" ? "text-[#9D21E6]" : "text-gray-500"
            } relative flex flex-col gap-0.5 items-center `}
          >
            <Image
              src="/static/images/logokedi.svg"
              width={34}
              height={34}
              alt="Logo Kedi"
              className="absolute -top-1"
            />
            <div className="h-[26px] w-[26px]" />

            <span className="text-sm font-medium">{lang.class}</span>
          </Link>
          <Link
            href="/"
            className="flex flex-col gap-0.5 items-center text-gray-500"
          >
            <svg
              className="w-[26px] h-[26px]"
              viewBox="0 0 40 40"
              fill="none"
              xmlns="http://www.w3.org/2000/svg"
            >
              <g clipPath="url(#clip0_955_268)">
                <path
                  d="M11.6667 18.3333H15V21.6666H11.6667V18.3333ZM35 9.99998V33.3333C35 35.1666 33.5 36.6666 31.6667 36.6666H8.33333C6.48333 36.6666 5 35.1666 5 33.3333L5.01667 9.99998C5.01667 8.16665 6.48333 6.66665 8.33333 6.66665H10V3.33331H13.3333V6.66665H26.6667V3.33331H30V6.66665H31.6667C33.5 6.66665 35 8.16665 35 9.99998ZM8.33333 13.3333H31.6667V9.99998H8.33333V13.3333ZM31.6667 33.3333V16.6666H8.33333V33.3333H31.6667ZM25 21.6666H28.3333V18.3333H25V21.6666ZM18.3333 21.6666H21.6667V18.3333H18.3333V21.6666Z"
                  fill="#9D9797"
                />
              </g>
              <defs>
                <clipPath id="clip0_955_268">
                  <rect width="40" height="40" fill="white" />
                </clipPath>
              </defs>
            </svg>
            <span className="text-sm font-medium">{lang.calendar}</span>
          </Link>
          <Link
            href="/"
            className="flex flex-col gap-0.5 items-center text-gray-500"
          >
            <svg
              className="w-[26px] h-[26px]"
              viewBox="0 0 40 40"
              fill="none"
              xmlns="http://www.w3.org/2000/svg"
            >
              <g clipPath="url(#clip0_955_274)">
                <path
                  d="M36.5 14.8167L34.75 7.53333C34.3833 6.03333 33.0833 5 31.5667 5H8.41666C6.91666 5 5.6 6.05 5.25 7.53333L3.5 14.8167C3.1 16.5167 3.46666 18.25 4.53333 19.6167C4.66666 19.8 4.85 19.9333 5 20.1V31.6667C5 33.5 6.5 35 8.33333 35H31.6667C33.5 35 35 33.5 35 31.6667V20.1C35.15 19.95 35.3333 19.8 35.4667 19.6333C36.5333 18.2667 36.9167 16.5167 36.5 14.8167ZM31.5167 8.31667L33.2667 15.6C33.4333 16.3 33.2833 17 32.85 17.55C32.6167 17.85 32.1167 18.3333 31.2833 18.3333C30.2667 18.3333 29.3833 17.5167 29.2667 16.4333L28.3 8.33333L31.5167 8.31667ZM21.6667 8.33333H24.9333L25.8333 15.8667C25.9167 16.5167 25.7167 17.1667 25.2833 17.65C24.9167 18.0833 24.3833 18.3333 23.7 18.3333C22.5833 18.3333 21.6667 17.35 21.6667 16.15V8.33333ZM14.15 15.8667L15.0667 8.33333H18.3333V16.15C18.3333 17.35 17.4167 18.3333 16.1833 18.3333C15.6167 18.3333 15.1 18.0833 14.7 17.65C14.2833 17.1667 14.0833 16.5167 14.15 15.8667ZM6.73333 15.6L8.41666 8.33333H11.7L10.7333 16.4333C10.6 17.5167 9.73333 18.3333 8.71666 18.3333C7.9 18.3333 7.38333 17.85 7.16666 17.55C6.71666 17.0167 6.56666 16.3 6.73333 15.6ZM8.33333 31.6667V21.6167C8.46666 21.6333 8.58333 21.6667 8.71666 21.6667C10.1667 21.6667 11.4833 21.0667 12.45 20.0833C13.45 21.0833 14.7833 21.6667 16.3 21.6667C17.75 21.6667 19.05 21.0667 20.0167 20.1167C21 21.0667 22.3333 21.6667 23.8333 21.6667C25.2333 21.6667 26.5667 21.0833 27.5667 20.0833C28.5333 21.0667 29.85 21.6667 31.3 21.6667C31.4333 21.6667 31.55 21.6333 31.6833 21.6167V31.6667H8.33333Z"
                  fill="currentColor"
                />
              </g>
              <defs>
                <clipPath id="clip0_955_274">
                  <rect width="40" height="40" fill="white" />
                </clipPath>
              </defs>
            </svg>
            <span className="text-sm font-medium">{lang.market}</span>
          </Link>
        </div>
      </div>
    </nav>
  );
};
