import React, { useState, useEffect } from "react";
import axios from "axios";
import PubNub from "pubnub";
import toast from "react-hot-toast";
import Image from "next/image";
import { LoadingSpinner } from "@/components/ui/loading";
import { useMediaQuery } from "react-responsive";
import { useInView } from "react-intersection-observer";
import { useMutation } from "@tanstack/react-query";
import { ListItem } from "@/components/kelas/ListItem";
import { useRouter } from "next/router";
import { encryptData, decryptData } from "@/utils/helper";
import { Tabs, TabsContent, TabsList, TabsTrigger } from "@/components/ui/tabs";
import {
  Dialog,
  DialogContent,
  DialogHeader,
  DialogTitle,
} from "@/components/ui/dialog";
import { ToggleGroup, ToggleGroupItem } from "@/components/ui/toggle-group";
import {
  Select,
  SelectContent,
  SelectItem,
  SelectTrigger,
  SelectValue,
} from "@/components/ui/select";
import {
  DropdownMenu,
  DropdownMenuContent,
  DropdownMenuItem,
  DropdownMenuLabel,
  DropdownMenuSeparator,
  DropdownMenuTrigger,
} from "@/components/ui/dropdown-menu";
import useUserChat from "@/store/useUserChat";
import { getMessageUser } from "@/utils/getMessageUser";

interface PostResponse extends Response {}

type PostNewGroup = {
  name: string;
  description: string;
  profilePictureId: number;
  userIdList: [];
};

let isFirstInView = true; // Menandai apakah kondisi inView pertama kali menjadi true

const InfiniteScroll = ({
  loadMoreItemsGroup,
  loadMoreItemsPrivate,
  loadMoreItemsClass,
  storedClass,
  setClassItems,
  storedGroup,
  setGroupItems,
  storedPrivate,
  setPrivateItems,
  tabChat,
}) => {
  const [ref, inView] = useInView();
  const [isFirstLoad, setIsFirstLoad] = useState(true);

  // Fungsi untuk mengeksekusi kode
  const executeInViewLogic = () => {
    if (tabChat === "channelActivePrivate") {
      loadMoreItemsPrivate(storedPrivate, setPrivateItems);
    } else if (tabChat === "channelActiveClass") {
      loadMoreItemsClass(storedClass, setClassItems);
    } else if (tabChat === "channelActiveGroup") {
      loadMoreItemsGroup(storedGroup, setGroupItems);
    }

    useUserChat.setState({
      isLoadMoreList: true,
    });

    if (isFirstInView) {
      setIsFirstLoad(false); // Sekali eksekusi, set isFirstLoad menjadi false
      setTimeout(() => {
        setIsFirstLoad(true); // Sekali eksekusi, set isFirstLoad menjadi false
        isFirstInView = false; // Sekali eksekusi, set isFirstLoad menjadi false
      }, 1500);
    }
  };

  // Logika untuk memeriksa kondisi inView dan eksekusi kode
  useEffect(() => {
    if (inView && isFirstLoad) {
      executeInViewLogic(); // Eksekusi kode saat awal load
    }
  }, [inView, isFirstLoad]);

  return (
    <>
      {" "}
      <div ref={ref} style={{ height: "20px" }} />
      {isFirstInView && (
        <div className="hidden absolute bg-white/30 h-full w-full flex items-center justify-center bottom-0 left-1/2 transform -translate-x-1/2  z-50">
          <LoadingSpinner stroke={`#9D21E6`} size={50} />
        </div>
      )}
    </>
  );
};

function SkeltonList() {
  return (
    <div className="absolute bg-white/10 backdrop-blur-[1px] h-full w-full flex items-center justify-center bottom-0 left-1/2 transform -translate-x-1/2  z-50">
      <LoadingSpinner stroke={`#9D21E6`} size={50} />
    </div>
  );
}

const ModalGroup = ({
  lang,
  user,
  modalGroup,
  setModalGroup,
  addData,
  isLoading,
  handleChange,
  search,
}) => {
  const [_group, setGroup] = useState({ name: "", description: "" });
  const [tempGroup, setTempGroup] = useState([]);
  const [displayGroup, setDisplayGroup] = useState([]);
  const [stepGroup, setStepGoup] = useState("1");
  const [selectedImage, setSelectedImage] = useState(
    "/static/images/logokedi.svg",
  );
  const [selectedFile, setSelectedFile] = useState(null);
  const sessionToken = user?.accessToken;

  const handleImageChange = (event) => {
    const file = event.target.files[0];

    if (file) {
      // Menggunakan URL.createObjectURL untuk membuat URL gambar dari file yang dipilih
      const imageUrl = URL.createObjectURL(file);
      setSelectedImage(imageUrl);
      setSelectedFile(file);
    }
  };

  const uploadFileClass = (file: any, sessionToken: string) => {
    return new Promise((resolve, reject) => {
      const formData = new FormData();
      formData.append("file", file);
      formData.append("documentTypeId", "1");
      formData.append("isPrivate", "true");

      axios
        .post(process.env.API_BE + "/api/storage", formData, {
          headers: {
            "Content-Type": "multipart/form-data",
            Authorization: `Bearer ${sessionToken}`,
          },
        })
        .then((res) => {
          resolve(res.data.documentStorage.id);
        })
        .catch((err) => {
          reject(err.response.data.meta.message);
        });
    });
  };

  const handleUploadFiles = async (file) => {
    if (!file) return 0;
    try {
      const responses = await uploadFileClass(file, sessionToken ?? "");
      toast.success(lang.successUploadImg);
      console.log("Responses:", responses);
      return responses;
    } catch (error) {
      toast.error(lang.errorUpload);
      console.error("Error uploading files:", error);
    }
  };

  const postNewGroup = useMutation(
    async (data: PostNewGroup): Promise<PostResponse> => {
      const { name, description, userIdList } = data;
      if (!sessionToken) throw new Error("Missing session token");
      try {
        const uploading = await handleUploadFiles(selectedFile);
        const url = `${process.env.API_BE}/api/group-chat`;
        const response = await axios({
          method: "post",
          url: url,
          data: {
            name: String(name),
            profilePictureId: uploading,
            description: String(description),
            userIdList: userIdList,
          },
          headers: {
            Authorization: `Bearer ${sessionToken}`,
          },
        });
        return response.data;
      } catch (error) {
        console.error(error);
        throw error;
      }
    },
    {
      onSuccess: (data) => {
        const channel = data?.data?.channelName;
        addData(channel);
        toast.success("Berhasil Buat Group Chat");
        setGroup({ name: "", description: "" });
        setDisplayGroup([]);
        setTempGroup([]);
        setStepGoup("1");
        setSelectedFile(null);
        setSelectedImage("/static/images/logokedi.svg");
        setModalGroup(false);
      },

      onError: (error) =>
        toast.error(`Error: ${error.response?.data?.meta?.message ?? error}`),
    },
  );

  return (
    <Dialog
      open={modalGroup}
      onOpenChange={(open) => {
        setModalGroup(open);
        setGroup({ name: "", description: "" });
        setDisplayGroup([]);
        setTempGroup([]);
        setStepGoup("1");
        setSelectedFile(null);
        setSelectedImage("/static/images/logokedi.svg");
      }}
    >
      <DialogContent className="sm:max-w-[425px]">
        <DialogHeader>
          <DialogTitle>{lang.titleCreateGroupChat}</DialogTitle>
        </DialogHeader>
        {postNewGroup.isLoading && (
          <div className="absolute h-full w-[98%] flex items-center justify-center backdrop-blur-sm bg-white/30 z-10">
            <LoadingSpinner stroke={`#9D21E6`} size={60} />
          </div>
        )}
        {stepGroup === "1" ? (
          <div className="">
            <div className="mb-3">
              <label
                htmlFor="title"
                className="block  font-medium text-gray-700"
              >
                {lang.whoAdded}
              </label>
              {displayGroup.length > 0 && (
                <div className="flex items-center gap-x-2.5 overflow-x-auto mt-3 max-w-sm">
                  {displayGroup.map((item) => (
                    <div
                      key={item.id}
                      className="flex flex-col items-center gap-x-2.5"
                    >
                      <div className="relative h-10 w-10 border p-0.5 rounded-lg border-gray-300">
                        <Image
                          width={50}
                          height={50}
                          src={
                            item?.profilePict ?? `/static/images/logokedi.svg`
                          }
                          className="absolute inset-0 h-full w-full object-cover  rounded-lg"
                          alt=""
                        />
                      </div>
                      <div>
                        <p className="text-xs truncate w-10">{item.fullName}</p>
                      </div>
                    </div>
                  ))}
                </div>
              )}
              <form
                onSubmit={(e) => e.preventDefault()}
                className="flex justify-end  w-full"
              >
                <label
                  htmlFor="default-search"
                  className="mb-2 text-sm font-medium text-gray-900 sr-only dark:text-white"
                >
                  {lang.search}
                </label>
                <div className="relative w-full">
                  <div className="absolute inset-y-0 left-0 flex items-center pl-3 pointer-events-none">
                    <svg
                      className="w-4 h-4 text-gray-500 dark:text-gray-400"
                      aria-hidden="true"
                      xmlns="http://www.w3.org/2000/svg"
                      fill="none"
                      viewBox="0 0 20 20"
                    >
                      <path
                        stroke="currentColor"
                        strokeLinecap="round"
                        strokeLinejoin="round"
                        strokeWidth={2.5}
                        d="m19 19-4-4m0-7A7 7 0 1 1 1 8a7 7 0 0 1 14 0Z"
                      />
                    </svg>
                  </div>
                  <input
                    type="search"
                    id="default-search"
                    name="query"
                    onChange={handleChange}
                    className="pl-10 pr-2 py-2 h-11 text-sm mt-1 w-full border-b border-black  outline-none  shadow-sm"
                    placeholder={lang.search}
                    autoComplete="off"
                  />
                </div>
              </form>
              <div className="text-sm w-fit mx-auto mt-4 space-x-4">
                <button
                  className="px-8  py-2   bg-[#9d21e6] text-white shadow-xl rounded-[10px] font-medium"
                  onClick={() => tempGroup.length !== 0 && setStepGoup("2")}
                  type="button"
                >
                  {lang.next}
                </button>
                <button
                  className="px-8  py-2  bg-[#d7b9fd] text-[#9d21e6]  shadow-xl rounded-[10px] font-medium"
                  type="button"
                >
                  {lang.cancel}
                </button>
              </div>
            </div>
            <ToggleGroup
              type="multiple"
              value={tempGroup}
              onValueChange={(value) => {
                if (value) setTempGroup(value);
              }}
              className="h-[55vh] pt-2 relative overflow-y-auto px-1.5"
            >
              {isLoading && (
                <div className="absolute h-[54vh] w-[98%] flex items-center justify-center backdrop-blur-sm bg-white/30 z-10">
                  <LoadingSpinner stroke={`#9D21E6`} size={60} />
                </div>
              )}
              {search?.length > 0
                ? search?.map((item: any) => {
                    if (item.id !== user?.id) {
                      return (
                        <ToggleGroupItem
                          value={item.id}
                          key={item.id}
                          onClick={() => {
                            if (
                              !displayGroup.some(
                                (existingItem) => existingItem.id === item.id,
                              )
                            ) {
                              if (!tempGroup.includes(item.id)) {
                                setDisplayGroup([...displayGroup, item]);
                              } else {
                                console.log(
                                  "user sudah ada di dalam tempGroup",
                                );
                              }
                            } else {
                              const updatedDisplayGroup = displayGroup.filter(
                                (existingItem) => existingItem.id !== item.id,
                              );
                              setDisplayGroup(updatedDisplayGroup);
                            }
                          }}
                          className={` w-full flex items-center justify-between items-center gap-x-2 hover:bg-[#9D21E6]/10 border p-2.5 rounded-lg mb-1.5 cursor-pointer `}
                        >
                          <div className="flex items-center gap-x-2.5">
                            <div className="relative h-12 w-12 border p-0.5 rounded-lg border-gray-300">
                              <Image
                                width={60}
                                height={60}
                                src={
                                  item?.profilePict ??
                                  `/static/images/logokedi.svg`
                                }
                                className="absolute inset-0 h-full w-full object-cover  rounded-lg"
                                alt=""
                              />
                            </div>
                            <div>
                              <p className="font-semibold text-ellipsis">
                                {item.fullName}
                              </p>
                            </div>
                          </div>
                        </ToggleGroupItem>
                      );
                    }
                  })
                : !isLoading && (
                    <div className="h-full flex items-center justify-center">
                      {lang.dataNotFound}
                    </div>
                  )}
            </ToggleGroup>
          </div>
        ) : (
          <div className="">
            <div className="my-3">
              <form className="flex justify-end  w-full">
                <div className="flex  gap-x-5 w-full">
                  <div className="mx-auto h-24 w-24 text-center mb-5">
                    <div className="relative w-64">
                      <label className="cursor-pointer">
                        <input
                          type="file"
                          accept="image/*"
                          className="hidden"
                          onChange={handleImageChange}
                        />
                        <Image
                          className="h-24 w-24 rounded-xl absolute object-cover"
                          width={500}
                          height={500}
                          src={selectedImage}
                          alt="profile image"
                        />
                        <div className="w-24 h-24 group hover:bg-gray-200 opacity-40 rounded-full absolute flex justify-center items-center cursor-pointer transition duration-500">
                          <img
                            className="hidden group-hover:block w-12"
                            src="https://www.svgrepo.com/show/33565/upload.svg"
                            alt=""
                          />
                        </div>
                      </label>
                    </div>
                  </div>
                  <div className="flex-1">
                    <label
                      htmlFor="title"
                      className="block text-sm font-medium text-gray-700"
                    >
                      {lang.titleGroupChat}
                    </label>
                    <input
                      type="text"
                      name="name"
                      value={_group.name}
                      onChange={(e) =>
                        setGroup({ ..._group, name: e.target.value })
                      }
                      className="py-2 text-sm w-full border-b border-black  outline-none  shadow-sm"
                      placeholder="Nama Group"
                      disabled={postNewGroup.isLoading}
                      autoComplete="off"
                    />
                    <label
                      htmlFor="title"
                      className=" mt-3 block text-sm font-medium text-gray-700"
                    >
                      {lang.descGroupChat}
                    </label>
                    <textarea
                      id="description"
                      value={_group.description}
                      maxLength="50"
                      onChange={(e) =>
                        setGroup({ ..._group, description: e.target.value })
                      }
                      className="w-full resize-none py-2 text-sm mt-1 border-b border-black  outline-none"
                      rows={2}
                      placeholder="Deskripsi Group"
                    ></textarea>
                  </div>
                </div>
              </form>
              <div className="text-sm w-fit mx-auto mt-4 space-x-4">
                <button
                  className="px-8  py-2   bg-[#9d21e6] text-white shadow-xl rounded-[10px] font-medium"
                  onClick={() =>
                    postNewGroup.mutate({
                      name: _group.name,
                      description: _group.description,
                      userIdList: tempGroup,
                    })
                  }
                  type="button"
                >
                  {lang.submit}
                </button>
                <button
                  className="px-8  py-2  bg-[#d7b9fd] text-[#9d21e6]  shadow-xl rounded-[10px] font-medium"
                  onClick={() => setStepGoup("1")}
                  type="button"
                >
                  {lang.back}
                </button>
              </div>
            </div>
            <div className="px-1.5 text-sm font-medium">
              {displayGroup?.length}{" "}
              {displayGroup?.length > 1 ? lang.members : lang.member}
            </div>
            <ToggleGroup
              type="multiple"
              value={tempGroup}
              onValueChange={(value) => {
                if (value) setTempGroup(value);
              }}
              className="h-[45vh] pt-2 relative overflow-y-auto px-1.5"
            >
              {displayGroup?.length > 0 ? (
                displayGroup?.map((item: any) => (
                  <div
                    value={item.id}
                    key={item.id}
                    className="w-full flex items-center justify-between items-center gap-x-2 hover:bg-[#9D21E6]/10 border p-2.5 rounded-lg mb-2.5 cursor-pointer"
                  >
                    <div className="flex items-center gap-x-2.5">
                      <div className="relative h-12 w-12 border p-0.5 rounded-lg border-gray-300">
                        <Image
                          width={60}
                          height={60}
                          src={
                            item?.profilePict ?? `/static/images/logokedi.svg`
                          }
                          className="absolute inset-0 h-full w-full object-cover  rounded-lg"
                          alt=""
                        />
                      </div>
                      <div>
                        <p className="font-semibold text-ellipsis">
                          {item.fullName}
                        </p>
                      </div>
                    </div>
                  </div>
                ))
              ) : (
                <div className="h-full flex items-center justify-center">
                  {lang.dataNotFound}
                </div>
              )}
            </ToggleGroup>
          </div>
        )}
      </DialogContent>
    </Dialog>
  );
};

const ModalPrivate = ({
  lang,
  modalPrivate,
  setModalPrivate,
  handleChange,
  search,
  isLoading,
  user,
  getMessageUser,
  setIsMobileView,
  isSM,
  setUserChatAktif,
  setChatMode,
}) => {
  return (
    <Dialog
      open={modalPrivate}
      onOpenChange={(open) => {
        setModalPrivate(open);
      }}
    >
      <DialogContent className="sm:max-w-[425px]">
        <DialogHeader>
          <DialogTitle>Kirim Pesan</DialogTitle>
        </DialogHeader>
        <div className="">
          <div className="mb-3">
            <label htmlFor="title" className="block  font-medium text-gray-700">
              Daftar Teman
            </label>
            <form
              onSubmit={(e) => e.preventDefault()}
              className="flex justify-end  w-full"
            >
              <label
                htmlFor="default-search"
                className="mb-2 text-sm font-medium text-gray-900 sr-only dark:text-white"
              >
                {lang.search}
              </label>
              <div className="relative w-full">
                <div className="absolute inset-y-0 left-0 flex items-center pl-3 pointer-events-none">
                  <svg
                    className="w-4 h-4 text-gray-500 dark:text-gray-400"
                    aria-hidden="true"
                    xmlns="http://www.w3.org/2000/svg"
                    fill="none"
                    viewBox="0 0 20 20"
                  >
                    <path
                      stroke="currentColor"
                      strokeLinecap="round"
                      strokeLinejoin="round"
                      strokeWidth={2.5}
                      d="m19 19-4-4m0-7A7 7 0 1 1 1 8a7 7 0 0 1 14 0Z"
                    />
                  </svg>
                </div>
                <input
                  type="search"
                  id="default-search"
                  name="query"
                  onChange={handleChange}
                  className="pl-10 pr-2 py-2 h-11 text-sm mt-1 w-full border-b border-black  outline-none  shadow-sm"
                  placeholder={lang.search}
                  autoComplete="off"
                />
                {/*  <button
                        type="submit"
                        className="w-[75px] text-center outline-none flex justify-center items-center text-white absolute  right-1.5 bottom-[7px] bg-[#9D21E6] hover:bg-purple-600  focus:outline-none  font-medium rounded-lg text-sm px-4 py-1.5 "
                    >
                        {isLoading ? (
                        <LoadingSpinner stroke={`#FFF`} size={20} />
                        ) : (
                        lang.search
                        )}
                    </button>*/}
              </div>
            </form>
          </div>
          <div className="h-[55vh] pt-2 relative overflow-y-auto px-1.5">
            {isLoading && (
              <div className="absolute h-[54vh] w-[98%] flex items-center justify-center backdrop-blur-sm bg-white/30 z-10">
                <LoadingSpinner stroke={`#9D21E6`} size={60} />
              </div>
            )}
            {search?.length > 0
              ? search?.map((item: any) => {
                  if (item.id !== user?.id) {
                    return (
                      <div
                        key={item.id}
                        onClick={() => {
                          const opt = {
                            channel: "private" + item.chatRoomId,
                            init: true,
                          };

                          getMessageUser(opt);

                          if (isSM) {
                            setIsMobileView(true);
                          }
                          setChatMode("private");
                          setUserChatAktif({
                            channel: "private." + item.chatRoomId,
                            receiver: {
                              id: item?.id, // harusnya uuid
                              name: item?.fullName,
                              photo: item?.profilePict,
                            },
                            sender: {
                              id: user.uuid,
                              name: user.fullName,
                              photo: user.photo,
                            },
                          });
                          setModalPrivate(false);
                        }}
                        className="w-full flex items-center justify-between items-center gap-x-2 hover:bg-[#9D21E6]/10 border p-2.5 rounded-lg mb-1.5 cursor-pointer"
                      >
                        <div className="flex items-center gap-x-2.5">
                          <div className="relative h-12 w-12 border p-0.5 rounded-lg border-gray-300">
                            <Image
                              width={60}
                              height={60}
                              src={
                                item?.profilePict ??
                                `/static/images/logokedi.svg`
                              }
                              className="absolute inset-0 h-full w-full object-cover  rounded-lg"
                              alt=""
                            />
                          </div>
                          <div>
                            <p className="font-semibold text-ellipsis">
                              {item.fullName}
                            </p>
                          </div>
                        </div>
                      </div>
                    );
                  }
                })
              : !isLoading && (
                  <div className="h-full flex items-center justify-center">
                    {lang.dataNotFound}
                  </div>
                )}
          </div>
        </div>
      </DialogContent>
    </Dialog>
  );
};

export const ListChat = (props: any) => {
  const {
    addData,
    classItems,
    groupItems,
    lang,
    loadMoreItemsClass,
    loadMoreItemsGroup,
    loadMoreItemsPrivate,
    privateItems,
    setClassItems,
    setGroupItems,
    setPrivateItems,
    storedClass,
    storedGroup,
    storedPrivate,
    user,
    viewportHeight,
  } = props;

  const [value, setValue] = useState("semua");
  //const responses = useUserChat((state) => state.responses);
  const tabChat = useUserChat((state) => state.tabChat);
  const userChatAktif = useUserChat((state) => state.userChatAktif);
  const allClassMessage = useUserChat((state) => state.allClassMessage);
  const classMessage =
    value === "academic"
      ? allClassMessage.filter((item) => item.isAcademic === true)
      : value === "nonAcademic"
        ? allClassMessage.filter((item) => item.isAcademic === false)
        : allClassMessage;
  const privateMessage = useUserChat((state) => state.privateMessage);
  const groupChatMessage = useUserChat((state) => state.groupChatMessage);
  const initPubNub = useUserChat((state) => state.initPubNub);
  const isLoadMetadata = useUserChat((state) => state.isLoadMetadata);
  const isLoadMoreList = useUserChat((state) => state.isLoadMoreList);

  const setIsMobileView = (value) => {
    useUserChat.setState({
      isMobileView: value,
    });
  };
  const setChatMode = (value) => {
    useUserChat.setState({
      chatMode: value,
    });
  };
  const setTabChat = (value) => {
    useUserChat.setState({
      tabChat: value,
    });
  };
  const setResponses = (value) => {
    useUserChat.setState({
      responses: value,
    });
  };
  const setUserChatAktif = (value) => {
    useUserChat.setState({
      userChatAktif: value,
    });
  };

  const setallClassMessage = (value) => {
    useUserChat.setState({
      allClassMessage: value,
    });
  };
  const setPrivateMessage = (value) => {
    useUserChat.setState({
      privateMessage: value,
    });
  };
  const setGroupChatMessage = (value) => {
    useUserChat.setState({
      groupChatMessage: value,
    });
  };

  const sessionToken = user?.accessToken;
  const router = useRouter();
  const lastTimetokenKey = `LastTimetoken_${user?.id}`;
  const lastMessageKey = `LastMessage_${user?.id}`;

  const [modalGroup, setModalGroup] = useState(false);
  const [modalPrivate, setModalPrivate] = useState(false);
  const [search, setSearch] = useState([]);
  const [isLoading, setIsloading] = useState(false);

  const fetchDataSearch = async () => {
    setIsloading(true);
    try {
      const response = await axios.get(
        process.env.API_BE + `/api/friend?limit=100&page=1`,
        {
          headers: {
            Authorization: `Bearer ${sessionToken}`,
          },
        },
      );

      setSearch(response.data.data.items);
      // queryClient.setQueryData(["queryResult"], response.data.data.items);
    } catch (error) {
      console.error("Gagal mengambil data pencarian.", error);
    } finally {
      setIsloading(false);
    }
  };

  const updateQuery = async (query: string) => {
    setIsloading(true);
    try {
      const response = await axios.get(
        process.env.API_BE + `/api/friend?query=${query}&limit=100&page=1`,
        {
          headers: {
            Authorization: `Bearer ${sessionToken}`,
          },
        },
      );

      if (response.data.data.items.length > 0) {
        // BE return duplicate data dan ini hanya sementara harusnya tidak perlu
        const data = response.data.data.items;

        setSearch(data);
      } else {
        setSearch([]);
      }
      // return queryClient.setQueryData(
      //   ["queryResult"],
      //   response.data.data.items,
      // );
    } catch (error) {
      console.error("Gagal mengambil data pencarian.", error);
    } finally {
      setIsloading(false);
    }
  };

  let typingTimer; // variabel untuk menyimpan timer

  const handleChange = async (e: React.ChangeEvent<HTMLInputElement>) => {
    clearTimeout(typingTimer); // menghapus timer sebelumnya
    const query = e.target.value.trim();
    if (query.length !== 0) {
      // memulai timer baru setelah 500 milidetik (0.5 detik)
      typingTimer = setTimeout(() => {
        updateQuery(query);
      }, 500);
    } else {
      setSearch([]);
      fetchDataSearch();
    }
  };

  // const isMD = useMediaQuery({ query: "(min-width: 900px)" });
  // const isPreSM = useMediaQuery({ query: "(max-width: 766px)" });
  const isSM = useMediaQuery({ query: "(max-width: 899px)" });
  const isPreMD = useMediaQuery({ query: "(min-width: 767px)" });

  const getPathFromUrl = (url: string): string | undefined => {
    if (url === null || url === undefined) return null;
    const match = url.match(/\/([^?]+)/);
    return match?.[1];
  };

  const categorizeAndSum = (channels) => {
    if (channels) {
      const result = {
        private: 0,
        group: 0,
        chatgroup: 0,
      };

      for (const [key, value] of Object.entries(channels)) {
        if (key.startsWith("private")) {
          result.private += value;
        } else if (key.startsWith("group")) {
          result.group += value;
        } else if (key.startsWith("chat-group")) {
          result.chatgroup += value;
        }
      }

      return result;
    }
  };

  //const countSums = categorizeAndSum(responses?.channels);

  return (
    <>
      {modalGroup && (
        <ModalGroup
          lang={lang}
          user={user}
          modalGroup={modalGroup}
          setModalGroup={setModalGroup}
          addData={addData}
          isLoading={isLoading}
          handleChange={handleChange}
          search={search}
        />
      )}
      {modalPrivate && (
        <ModalPrivate
          lang={lang}
          modalPrivate={modalPrivate}
          setModalPrivate={setModalPrivate}
          handleChange={handleChange}
          search={search}
          isLoading={isLoading}
          user={user}
          getMessageUser={getMessageUser}
          setIsMobileView={setIsMobileView}
          isSM={isSM}
          setUserChatAktif={setUserChatAktif}
          setChatMode={setChatMode}
        />
      )}
      <Tabs
        value={tabChat}
        onValueChange={setTabChat}
        // className="relative [@media(min-width:900px)]:max-w-[368px] flex-1 pt-[4.4rem] sm:pt-[5.2rem] pb-[3.8rem] md:pb-0 pb-2.5 className flex flex-col h-screen shadow-md px-2.5"
      >
        <TabsList className="w-full grid grid-cols-3 bg-white  shadow-md">
          <TabsTrigger value="channelActiveClass">
            {lang.activeCLass}{" "}
{/*            {countSums?.group > 0 ? `(${countSums?.group})` : null}*/}
          </TabsTrigger>
          <TabsTrigger value="channelActivePrivate">
            {lang.chats}{" "}
{/*            {countSums?.private > 0 ? `(${countSums?.private})` : null}*/}
          </TabsTrigger>
          <TabsTrigger value="channelActiveGroup">
            {lang.group}{" "}
{/*            {countSums?.chatgroup > 0 ? `(${countSums?.chatgroup})` : null}*/}
          </TabsTrigger>
        </TabsList>

        {tabChat === "channelActivePrivate" && (
          <TabsContent
            value="channelActivePrivate"
            className={` overflow-y-auto relative  pr-2 h-full`}
            style={
              isPreMD
                ? { height: viewportHeight - 135 }
                : { height: viewportHeight - 175 }
            }
          >
            {!initPubNub && <SkeltonList />}
            {privateMessage?.length === 0  && initPubNub ? (
              <div
                className="h-full w-full text-center flex items-center justify-center"
              >
                {" "}
                <div className="">{lang.noMessage}</div>{" "}
              </div>
            ) : (
              privateMessage?.map((d: any, index: number) => {
                const text = d.message?.pn_gcm?.data.custom_data.text;
                const receiverId = user.uuid === d.uuid1 ? d.uuid2 : d.uuid1;
                const receiverName =
                  user.fullName === d.user1 ? d.user2 : d.user1;

                const url1 = d?.profilePictureLink1;
                const urlSesi = user.photo;
                const receiverPhoto =
                  getPathFromUrl(urlSesi) === getPathFromUrl(url1)
                    ? d.profilePictureLink2
                    : d.profilePictureLink1;
                const unread = d?.unread ?? 0;

                const updatedAllData = [...privateMessage];
                const readMessage = {
                  ...d,
                  unread: 0,
                };
                const indexItem = privateMessage.findIndex(
                  (item) => item.channel === d.channel,
                );
                updatedAllData[indexItem] = readMessage;

                const lastMessStr = localStorage.getItem(lastTimetokenKey);
                let updatedAllDataLS = {};

                if (lastMessStr) {
                  const lastMessObj = JSON.parse(lastMessStr);
                  updatedAllDataLS = {
                    channels: [...lastMessObj.channels],
                    timetokens: [...lastMessObj.timetokens],
                  };
                  const indexItemLS = lastMessObj.channels.findIndex(
                    (item) => item === d.channel,
                  );
                  updatedAllDataLS.timetokens[indexItemLS] = d.timetoken;
                }

                const lastAllMsg = localStorage.getItem(lastMessageKey);

                return (
                  <ListItem
                    key={index}
                    onClick={() => {
                      if (isSM) {
                        setIsMobileView(true);
                      }
                      setChatMode("private");
                      setUserChatAktif({
                        channel: d.channel,
                        // name: d.description,
                        receiver: {
                          id: receiverId,
                          name: receiverName,
                          photo: receiverPhoto,
                        },
                        sender: {
                          id: user.uuid,
                          name: user.fullName,
                          photo: user.photo,
                        },
                      });

                      const opt = {
                        channel: d.channel,
                        init: true,
                      };
                      const opt_d = {
                        channel: d.channel,
                        isDelete: true,
                      };

                      if (unread > 0) {
                        getMessageUser(opt_d);
                      } else {
                        getMessageUser(opt);
                      }

                      if (unread > 0) {
                        setPrivateMessage(updatedAllData);
                        localStorage.setItem(
                          lastTimetokenKey,
                          JSON.stringify(updatedAllDataLS),
                        );

                        if (lastAllMsg) {
                          const lastMessObj = JSON.parse(lastAllMsg);
                          const decrypted = decryptData(lastMessObj, "KEDI");
                          const updatedAllDataMsg = [
                            ...decrypted?.privateMessage,
                          ];
                          updatedAllDataMsg[indexItem] = readMessage;

                          const updatedData = {
                            classMessage: decrypted?.classMessage,
                            privateMessage: updatedAllDataMsg,
                            groupChatMessage,
                          };

                          const encryptedMergedData = encryptData(
                            updatedData,
                            "KEDI",
                          );

                          localStorage.setItem(
                            lastMessageKey,
                            JSON.stringify(encryptedMergedData),
                          );
                        }

                        (async () => {
                          const lastMessStr =
                            localStorage.getItem(lastTimetokenKey);
                          const pubnub = new PubNub({
                            publishKey: user?.publishKey,
                            subscribeKey: user?.subscribeKey,
                            uuid: user?.uuid,
                          });

                          if (lastMessStr) {
                            try {
                              const lastMessObj = JSON.parse(lastMessStr);
                              const channels = lastMessObj.channels.slice(
                                0,
                                100,
                              );
                              const timetokens = lastMessObj.timetokens.slice(
                                0,
                                100,
                              );

                              const res = await pubnub.messageCounts({
                                channels: channels,
                                channelTimetokens: timetokens,
                              });

                              setResponses(res);
                            } catch (error) {
                              console.error(error);
                            }
                          }
                        })();
                      }
                      router.push("class?message=" + d.channel);
                    }}
                    unread={unread}
                    channel={d.channel}
                    name={receiverName}
                    profilePictureLink={receiverPhoto}
                    text={text ?? d.message?.pn_gcm?.notification.body}
                    timetoken={d.timetoken}
                    userChatAktif={userChatAktif}
                  />
                );
              })
            )}
            {privateItems.length < storedPrivate.length && (
              <InfiniteScroll
                loadMoreItemsGroup={loadMoreItemsGroup}
                loadMoreItemsPrivate={loadMoreItemsPrivate}
                loadMoreItemsClass={loadMoreItemsClass}
                storedClass={storedClass}
                setClassItems={setClassItems}
                classItems={classItems}
                storedGroup={storedGroup}
                setGroupItems={setGroupItems}
                groupItems={groupItems}
                storedPrivate={storedPrivate}
                setPrivateItems={setPrivateItems}
                privateItems={privateItems}
                tabChat={tabChat}
              />
            )}
            {isLoadMetadata && isLoadMoreList && <SkeltonList />}
          </TabsContent>
        )}
        {tabChat === "channelActiveGroup" && (
          <TabsContent
            value="channelActiveGroup"
            className={`overflow-y-auto relative  pr-2 h-full`}
            style={
              isPreMD
                ? { height: viewportHeight - 135 }
                : { height: viewportHeight - 175 }
            }
          >
            {!initPubNub && <SkeltonList />}
            {groupChatMessage?.length === 0 && initPubNub ? (
              <div className="h-full w-full text-center flex items-center justify-center">
                <div className="">{lang.noMessage}</div>{" "}
              </div>
            ) : (
              groupChatMessage?.length > 0 &&
              groupChatMessage.map((d: any, index: number) => {
                //const chatCategory = d?.message?.metadata?.category;
                const text =
                  d.message?.pn_gcm?.data?.custom_data?.text ??
                  d.message?.pn_gcm?.notification.body;
                const isSystemMess =
                  d.message?.text && d.message?.metadata?.name
                    ? d.message?.text +
                      " " +
                      ' "' +
                      d.message?.metadata?.name +
                      '" '
                    : null;
                const isText = text
                  ? text
                  : isSystemMess
                    ? isSystemMess
                    : "No message";
                const receiverId = d.ownerUuid;
                const unread = d?.unread ?? 0;
                const receiverPhoto = d?.profilePictureLink;
                const updatedAllData = [...groupChatMessage];
                const readMessage = {
                  ...d,
                  unread: 0,
                };
                const indexItem = groupChatMessage.findIndex(
                  (item) => item.channel === d.channel,
                );
                updatedAllData[indexItem] = readMessage;

                const lastMessStr = localStorage.getItem(lastTimetokenKey);
                let updatedAllDataLS = {};

                if (lastMessStr) {
                  const lastMessObj = JSON.parse(lastMessStr);
                  updatedAllDataLS = {
                    channels: [...lastMessObj.channels],
                    timetokens: [...lastMessObj.timetokens],
                  };
                  const indexItemLS = lastMessObj.channels.findIndex(
                    (item) => item === d.channel,
                  );
                  updatedAllDataLS.timetokens[indexItemLS] = d.timetoken;
                }

                const lastAllMsg = localStorage.getItem(lastMessageKey);
                return (
                  <ListItem
                    key={index}
                    onClick={() => {
                      if (isSM) {
                        setIsMobileView(true);
                      }
                      setChatMode("group");
                      setUserChatAktif({
                        channel: d.channel,
                        groupId: d.id,
                        name: d.description,
                        isAdmin: d.isAdmin,
                        receiver: {
                          id: receiverId,
                          name: d.name,
                          photo: receiverPhoto,
                        },
                        sender: {
                          id: user.uuid,
                          name: user.fullName,
                          photo: user.photo,
                        },
                      });

                      const opt = {
                        channel: d.channel,
                        init: true,
                      };

                      const opt_d = {
                        channel: d.channel,
                        isDelete: true,
                      };

                      if (unread > 0) {
                        getMessageUser(opt_d);
                      } else {
                        getMessageUser(opt);
                      }

                      if (unread > 0) {
                        setGroupChatMessage(updatedAllData);
                        localStorage.setItem(
                          lastTimetokenKey,
                          JSON.stringify(updatedAllDataLS),
                        );

                        if (lastAllMsg) {
                          const lastMessObj = JSON.parse(lastAllMsg);
                          const decrypted = decryptData(lastMessObj, "KEDI");
                          const updatedAllDataMsg = [
                            ...decrypted?.groupChatMessage,
                          ];
                          updatedAllDataMsg[indexItem] = readMessage;

                          const updatedData = {
                            classMessage: decrypted?.classMessage,
                            privateMessage,
                            groupChatMessage: updatedAllDataMsg,
                          };

                          const encryptedMergedData = encryptData(
                            updatedData,
                            "KEDI",
                          );

                          localStorage.setItem(
                            lastMessageKey,
                            JSON.stringify(encryptedMergedData),
                          );
                        }

                        (async () => {
                          const lastMessStr =
                            localStorage.getItem(lastTimetokenKey);
                          const pubnub = new PubNub({
                            publishKey: user?.publishKey,
                            subscribeKey: user?.subscribeKey,
                            uuid: user?.uuid,
                          });

                          if (lastMessStr) {
                            try {
                              const lastMessObj = JSON.parse(lastMessStr);
                              const channels = lastMessObj.channels.slice(
                                0,
                                100,
                              );
                              const timetokens = lastMessObj.timetokens.slice(
                                0,
                                100,
                              );

                              const res = await pubnub.messageCounts({
                                channels: channels,
                                channelTimetokens: timetokens,
                              });

                              setResponses(res);
                            } catch (error) {
                              console.error(error);
                            }
                          }
                        })();
                      }
                      router.push("class?message=" + d.channel);
                    }}
                    unread={unread}
                    channel={d.channel}
                    name={d.name}
                    profilePictureLink={d.profilePictureLink}
                    text={isText}
                    created={d.createdDate}
                    timetoken={d.timetoken}
                    isAdmin={d.isAdmin}
                    userChatAktif={userChatAktif}
                  />
                );
              })
            )}
            {groupItems.length < storedGroup.length && (
              <InfiniteScroll
                loadMoreItemsGroup={loadMoreItemsGroup}
                loadMoreItemsPrivate={loadMoreItemsPrivate}
                loadMoreItemsClass={loadMoreItemsClass}
                storedClass={storedClass}
                setClassItems={setClassItems}
                classItems={classItems}
                storedGroup={storedGroup}
                setGroupItems={setGroupItems}
                groupItems={groupItems}
                storedPrivate={storedPrivate}
                setPrivateItems={setPrivateItems}
                privateItems={privateItems}
                tabChat={tabChat}
              />
            )}
            {isLoadMetadata && isLoadMoreList && <SkeltonList />}
          </TabsContent>
        )}
        {tabChat === "channelActiveClass" && (
          <TabsContent value="channelActiveClass" className="">
            <Select className="z-[100]" value={value} onValueChange={setValue}>
              <SelectTrigger
                open={true}
                className="mb-2.5 mt-1.5 shadow-md rounded-lg"
              >
                <SelectValue placeholder={lang.class} />
              </SelectTrigger>
              <SelectContent
                ref={(ref) =>
                  ref?.addEventListener("touchend", (e) => e.preventDefault())
                }
              >
                <SelectItem value="academic">{lang.academic}</SelectItem>
                <SelectItem value="umum">{lang.public}</SelectItem>
                <SelectItem value="semua">{lang.all}</SelectItem>
              </SelectContent>
            </Select>

            <div
              className={`overflow-y-auto  pr-2 h-full`}
              style={
                isPreMD
                  ? { height: viewportHeight - 190 }
                  : { height: viewportHeight - 230 }
              }
            >
              {!initPubNub && <SkeltonList />}
              {classMessage?.length > 0 ? (
                <>
                  {classMessage?.map((d: any, index: number) => {
                    const text = d.message?.text;
                    const videoUrl = d.message?.metadata?.url;
                    const isLecturer = d?.lecturerUuid == user?.uuid;
                    const unread = d?.unread ?? 0;
                    const updatedAllData = [...allClassMessage];
                    const readMessage = {
                      ...d,
                      unread: 0,
                    };
                    const indexItem = allClassMessage.findIndex(
                      (item) => item.channel === d.channel,
                    );
                    updatedAllData[indexItem] = readMessage;

                    const lastMessStr = localStorage.getItem(lastTimetokenKey);
                    let updatedAllDataLS = {};

                    if (lastMessStr) {
                      const lastMessObj = JSON.parse(lastMessStr);
                      updatedAllDataLS = {
                        channels: [...lastMessObj.channels],
                        timetokens: [...lastMessObj.timetokens],
                      };
                      const indexItemLS = lastMessObj.channels.findIndex(
                        (item) => item === d.channel,
                      );
                      updatedAllDataLS.timetokens[indexItemLS] = d.timetoken;
                    }

                    const lastAllMsg = localStorage.getItem(lastMessageKey);

                    return (
                      <ListItem
                        key={index}
                        onClick={() => {
                          if (isSM) {
                            setIsMobileView(true);
                          }
                          setChatMode("class");
                          setUserChatAktif({
                            channel: d.channel,
                            classId: d.classId,
                            videoUrl: videoUrl,
                            name: d.name,
                            receiver: {
                              id: d.lecturerUuid,
                              name: d.lecturerName ?? "Data tidak valid",
                              photo: d.profilePictureLink,
                            },
                            sender: {
                              id: user.uuid,
                              name: user.fullName,
                              photo: user.photo,
                            },
                          });

                          const opt = {
                            channel: d.channel,
                            init: true,
                          };
                          const opt_d = {
                            channel: d.channel,
                            isDelete: true,
                          };

                          if (unread > 0) {
                            getMessageUser(opt_d);
                          } else {
                            getMessageUser(opt);
                          }

                          if (unread > 0) {
                            setallClassMessage(updatedAllData);
                            localStorage.setItem(
                              lastTimetokenKey,
                              JSON.stringify(updatedAllDataLS),
                            );

                            if (lastAllMsg) {
                              const lastMessObj = JSON.parse(lastAllMsg);
                              const decrypted = decryptData(
                                lastMessObj,
                                "KEDI",
                              );
                              const updatedAllDataMsg = [
                                ...decrypted?.classMessage,
                              ];
                              updatedAllDataMsg[indexItem] = readMessage;

                              const updatedData = {
                                classMessage: updatedAllDataMsg,
                                privateMessage,
                                groupChatMessage,
                              };

                              const encryptedMergedData = encryptData(
                                updatedData,
                                "KEDI",
                              );

                              localStorage.setItem(
                                lastMessageKey,
                                JSON.stringify(encryptedMergedData),
                              );
                            }

                            (async () => {
                              const lastMessStr =
                                localStorage.getItem(lastTimetokenKey);
                              const pubnub = new PubNub({
                                publishKey: user?.publishKey,
                                subscribeKey: user?.subscribeKey,
                                uuid: user?.uuid,
                              });

                              if (lastMessStr) {
                                try {
                                  const lastMessObj = JSON.parse(lastMessStr);
                                  const channels = lastMessObj.channels.slice(
                                    0,
                                    100,
                                  );
                                  const timetokens =
                                    lastMessObj.timetokens.slice(0, 100);

                                  const res = await pubnub.messageCounts({
                                    channels: channels,
                                    channelTimetokens: timetokens,
                                  });

                                  setResponses(res);
                                } catch (error) {
                                  console.error(error);
                                }
                              }
                            })();
                          }
                          router.push("class?message=" + d.channel);
                        }}
                        unread={unread}
                        channel={d.channel}
                        name={d.name}
                        lecturerName={d.lecturerName ?? "Data tidak valid"}
                        profilePictureLink={d.profilePictureLink}
                        text={text ?? d.message?.pn_gcm?.notification.body}
                        timetoken={d.timetoken}
                        created={d.createdDate}
                        isLecturer={isLecturer}
                        userChatAktif={userChatAktif}
                      />
                    );
                  })}
                  {value === "semua" &&
                    classItems.length < storedClass.length && (
                      <InfiniteScroll
                        loadMoreItemsGroup={loadMoreItemsGroup}
                        loadMoreItemsPrivate={loadMoreItemsPrivate}
                        loadMoreItemsClass={loadMoreItemsClass}
                        storedClass={storedClass}
                        setClassItems={setClassItems}
                        classItems={classItems}
                        storedGroup={storedGroup}
                        setGroupItems={setGroupItems}
                        groupItems={groupItems}
                        storedPrivate={storedPrivate}
                        setPrivateItems={setPrivateItems}
                        privateItems={privateItems}
                        tabChat={tabChat}
                      />
                    )}
                  {isLoadMetadata && isLoadMoreList && <SkeltonList />}
                </>
              ) : (
                !isLoadMetadata &&
                initPubNub && (
                  <div className="h-[90%] w-full text-center flex items-center justify-center">
                    {" "}
                    <div className="">{lang.noMessage}</div>{" "}
                  </div>
                )
              )}
            </div>
          </TabsContent>
        )}
        {tabChat !== "channelActiveClass" && (
          <DropdownMenu>
            <DropdownMenuTrigger className="bg-purple-700 text-white flex items-center justify-center absolute bottom-20 md:bottom-6 right-6 h-10 w-10 rounded-lg transition duration-500 ease-in-out focus:outline-none">
              <svg
                xmlns="http://www.w3.org/2000/svg"
                fill="none"
                viewBox="0 0 24 24"
                strokeWidth={2}
                stroke="currentColor"
                className="w-6 h-6"
              >
                <path
                  strokeLinecap="round"
                  strokeLinejoin="round"
                  d="M12 4.5v15m7.5-7.5h-15"
                />
              </svg>
            </DropdownMenuTrigger>
            <DropdownMenuContent className="mr-8">
              <DropdownMenuLabel>{lang.adtMenu}</DropdownMenuLabel>
              <DropdownMenuSeparator />
              {tabChat === "channelActivePrivate" && (
                <DropdownMenuItem
                  onClick={() => {
                    setModalPrivate(true);
                    fetchDataSearch();
                  }}
                >
                  {lang.btnChatContact}
                </DropdownMenuItem>
              )}
              {tabChat === "channelActiveGroup" && (
                <DropdownMenuItem
                  onClick={() => {
                    setModalGroup(true);
                    fetchDataSearch();
                  }}
                >
                  {lang.btnCreateGroupChat}
                </DropdownMenuItem>
              )}
            </DropdownMenuContent>
          </DropdownMenu>
        )}
      </Tabs>
    </>
  );
};
