import React, { ChangeEvent, useEffect, useState } from "react";
import Image from "next/image";
import Link from "next/link";
import axios from "axios";
import dynamic from "next/dynamic";
import toast from "react-hot-toast";
import PubNub from "pubnub";
import Linkify from "linkify-react";
import { Dialog, DialogContent, DialogTrigger } from "@/components/ui/dialog";
import { ToggleGroup, ToggleGroupItem } from "@/components/ui/toggle-group";
import { LoadingSpinner } from "@/components/ui/loading";
import { ReusableAlertDialog } from "@/components/kelas/module/element/Alert";
import { convertTime, isStartTimeBeforeNow } from "@/utils/convertTime";
import { useMediaQuery } from "react-responsive";
import { useMutation, useQuery, useQueryClient } from "@tanstack/react-query";
import { useSession } from "next-auth/react";
import { useInView } from "react-intersection-observer";
import * as Progress from "@radix-ui/react-progress";
import {
  Popover,
  PopoverContent,
  PopoverTrigger,
} from "@/components/ui/popover";
import {
  DropdownMenu,
  DropdownMenuContent,
  DropdownMenuItem,
  DropdownMenuLabel,
  DropdownMenuSeparator,
  DropdownMenuTrigger,
} from "@/components/ui/dropdown-menu";
import { useRouter } from "next/router";
import { Threedots } from "@/assets/icon";
import {
  ContextMenu,
  ContextMenuContent,
  ContextMenuItem,
  ContextMenuTrigger,
} from "@/components/ui/context-menu";
import useUserChat from "@/store/useUserChat";

const ModalChat = dynamic(() => import("./ModalChat"), {
  ssr: false,
  loading: () => (
    <div className="absolute inset-0 flex items-center justify-center backdrop-blur-[1px] bg-white/20 z-10">
      <LoadingSpinner size={70} strokeWidth={2} />
    </div>
  ),
});

const TypingMessage = () => {
  const userChatAktif = useUserChat((state) => state.userChatAktif);
  const isTyping = useUserChat((state) => state.isTyping);
  const chatMode = useUserChat((state) => state.chatMode);

  return (
    <div
      className={` ${
        chatMode === "class" ? "pl-16" : "pl-5"
      }  py-2  flex items-center gap-2 truncate max-w-[220px] md:max-w-[270px] text-[13px] font-medium`}
    >
      {userChatAktif.sender?.id !== isTyping?.publisher &&
        userChatAktif.channel === isTyping?.channel &&
        isTyping?.message !== "typing_off" && (
          <>
            <Threedots /> {isTyping?.message}
          </>
        )}
    </div>
  );
};

export const ChatBox = React.memo((props) => {
  const isMobileView = useUserChat((state) => state.isMobileView);
  const isFetchFirst = useUserChat((state) => state.isFetchFirst);
  const isFetchMessage = useUserChat((state) => state.isFetchMessage);

  const setUserChatAktif = (value) => {
    useUserChat.setState({
      userChatAktif: value,
    });
  };

  const { ref: refObserver, inView } = useInView();
  const {
    inputText,
    setInputText,
    isHeight,
    inputTextRef,
    lang,
    userChatAktif,
    messageHistory,
    todoListRef,
    sendMessage,
    chatMode,
    removeItem,
    formatBytes,
    handleButtonClick,
    handleFileChange,
    sendFile,
    handleDelete,
    handleDragOver,
    handleDragEnter,
    handleDrop,
    handleDragLeave,
    selectedFile,
    isLoadingFile,
    isSentMessage,
    modalType,
    setModalType,
    getExam,
    dataUjianUts,
    dataUjianUas,
    subExamUts,
    subExamUas,
    getInfoClass,
    dataInfoKelas,
    getInfoMembers,
    dataMember,
    getMaterial,
    getAllMaterial,
    getAllAssignment,
    getAllQuiz,
    allMaterial,
    allAssignment,
    allQuiz,
    material,
    getQuiz,
    getAssignment,
    singleSubAssign,
    singleSubQuiz,
    dataKuis,
    dataTugas,
    subQuiz,
    subAssignment,
    addFriend,
    modalClass,
    isLoadingModal,
    setIsloadingModal,
    setModalClass,
    setHeading,
    setSideType,
    setSidebar,
    inputFileRef,
    multiMessage,
    setMultiMessage,
    deleteMessage,
    viewportHeight,
    getMessageUser,
    tempQuizId,
    settempQuizId,
    getInfoGroup,
    fetchDataSearch,
    setalert3,
    setAlertGroup,
  } = props;

  React.useEffect(() => {
    if (inView && !isFetchFirst) {
      const opt = {
        channel: userChatAktif?.channel,
        isMore: true,
      };

      getMessageUser(opt);

      useUserChat.setState({
        isLoadMoreMsg: true,
      });
    }
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [inView]);
  const isAdmin = userChatAktif?.receiver?.id === userChatAktif?.sender?.id;

  const router = useRouter();

  const queryClient = useQueryClient();
  const { data: sessionData } = useSession();
  const sessionToken = sessionData?.user?.accessToken;
  const [date, setDate] = React.useState<Date>();
  const [fileClass, setFileClass] = React.useState<any>([]);
  const [xpop, setXpop] = React.useState(false);
  const [open, setOpen] = useState(false);
  const [edit, setEdit] = useState("");
  const [delmode, setdelmode] = useState("");
  const [alert, setalert] = useState(false);
  const [alert2, setalert2] = useState(false);
  const [inputQuiz, setInputQuiz] = useState<any>({
    id: "",
    grade: 0,
    comment: "",
  });

  const [urlConf, setUrlConf] = useState("");
  const [urlConfSucces, setUrlConfSucces] = useState("");
  const [createMaterial, setCreateMaterial] = useState<any>({
    classId: userChatAktif?.classId,
    title: "",
    description: "",
    documentIdList: [],
  });

  const [tempDocId, setTempDocId] = useState([]);
  const [createAssignment, setCreateAssignment] = useState<any>({
    classId: userChatAktif?.classId,
    name: "",
    instruction: "",
    startDate: new Date().toISOString(),
  });

  const [createExam, setCreateExam] = useState<any>({
    classId: 0,
    sessionId: 0,
    dueDate: null,
    name: "",
    description: "",
  });

  const isSM = useMediaQuery({ query: "(max-width: 899px)" });

  const handleFileChangeClass = (event: ChangeEvent<HTMLInputElement>) => {
    const file = event.target.files ?? null;
    if (file) {
      const filesArray = Array.from(file);
      setFileClass((prevFiles: any) => [...prevFiles, ...filesArray]);
    }
  };

  const handleDropClass = (event: any) => {
    event.preventDefault();

    const file = event.dataTransfer.files ?? null;
    if (file) {
      const filesArray = Array.from(file);
      setFileClass((prevFiles: any) => [...prevFiles, ...filesArray]);
    }
  };

  const uploadFileClass = (file: any, sessionToken: string) => {
    return new Promise((resolve, reject) => {
      const formData = new FormData();
      formData.append("file", file);
      formData.append("documentTypeId", "1");
      formData.append("isPrivate", "false");

      axios
        .post(process.env.API_BE + "/api/storage", formData, {
          headers: {
            "Content-Type": "multipart/form-data",
            Authorization: `Bearer ${sessionToken}`,
          },
        })
        .then((res) => {
          // console.log('res:',  res);
          resolve(res.data.documentStorage.id);
        })
        .catch((err) => {
          // console.log('err:',  err);
          reject(
            err?.response?.data?.meta?.message ?? err?.response?.statusText,
          );
        });
    });
  };

  const handleUploadFiles = async () => {
    const uploadPromises = fileClass.map((file: any) =>
      uploadFileClass(file, sessionToken ?? ""),
    );

    try {
      const responses = await Promise.all(uploadPromises);
      if (fileClass.length > 0) {
        toast.success("Semua file berhasil diunggah");
        console.log("Responses:", responses);
      }
      return responses;
    } catch (error) {
      // toast.error("Terjadi kesalahan saat mengunggah file");
      toast.error("Error uploading files:", error);
      return null;
    }
  };

  interface PostResponse extends Response {}

  type ActVideoConf = {
    classId: number;
    urlConf: string;
    type: string;
  };

  const actVideoConf = useMutation(
    async (data: ActVideoConf): Promise<PostResponse> => {
      const { classId, urlConf, type } = data;
      if (!sessionToken) throw new Error("Missing session token");
      setIsloadingModal(true);
      try {
        const url = `${process.env.API_BE}/api/class/${classId}/zoom-url`;
        const data = type === "put" ? { url: urlConf } : {};
        const response = await axios({
          method: type === "put" ? "put" : "delete",
          url: url,
          data: data,
          headers: {
            Authorization: `Bearer ${sessionToken}`,
          },
        });

        if (type === "put") {
          toast.success(lang.successUpdateZoom);
          setUrlConfSucces(urlConf);
        } else {
          toast.success(lang.successDeleteZoom);
          setUrlConfSucces("");
          setUserChatAktif({
            ...userChatAktif,
            videoUrl: undefined,
          });
          setAlertVideo(false);
        }
        setUrlConf("");
        setModalClass(false);
        getMessageUser({ channel: userChatAktif?.channel, isDelete: true });
        setIsloadingModal(false);
        return response.data;
      } catch (error) {
        console.error(error);
        setIsloadingModal(false);
        throw error;
      }
    },
    {
      onError: (error) =>
        toast.error(`Error: ${error.response?.data?.meta?.message ?? error}`),
    },
  );

  type PostMaterial = {
    classId: number;
    title: string;
    description: string;
    documentIdList: [];
  };

  const postMaterial = useMutation(
    async (data: PostMaterial): Promise<PostResponse> => {
      const { classId, title, description, documentIdList } = data;
      if (!sessionToken) throw new Error("Missing session token");
      setIsloadingModal(true);
      try {
        const url =
          edit !== ""
            ? `${process.env.API_BE}/api/class-material/${edit}`
            : delmode !== ""
              ? `${process.env.API_BE}/api/class-material/${delmode}`
              : `${process.env.API_BE}/api/class-material`;
        const uploading = await handleUploadFiles();
        if (!uploading) throw new Error("Error uploading files");
        const response = await axios({
          method: edit !== "" ? "put" : delmode !== "" ? "delete" : "post",
          url: url,
          data: {
            classId: Number(classId),
            title: String(title),
            description: String(description),
            documentIdList:
              documentIdList.length !== 0 && uploading
                ? [...documentIdList, ...uploading]
                : uploading,
          },
          headers: {
            Authorization: `Bearer ${sessionToken}`,
          },
        });
        if (edit !== "") {
          toast.success(lang.successUpdateMaterial);
        } else if (delmode !== "") {
          toast.success(lang.successDeleteMaterial);
        } else {
          toast.success(lang.successUploadMaterial);
        }
        setModalClass(false);
        return response.data;
      } catch (error) {
        console.error(error);
        setIsloadingModal(false);
        throw error;
      } finally {
        setIsloadingModal(false);
        setFileClass([]);
        setCreateMaterial({
          classId: userChatAktif?.classId,
          title: "",
          description: "",
        });
      }
    },
  );

  type PostAssignment = {
    classId: number;
    name: string;
    instruction: string;
    startDate: string;
    dueDate: string | undefined;
  };

  const postAssignment = useMutation(
    async (data: PostAssignment): Promise<PostResponse> => {
      const { classId, name, instruction, startDate, dueDate } = data;
      if (!sessionToken) throw new Error("Missing session token");
      setIsloadingModal(true);
      try {
        const uploading = await handleUploadFiles();

        const response = await axios.post<PostResponse>(
          process.env.API_BE + `/api/class-assignment`,
          {
            classId: Number(classId),
            name: String(name),
            instruction: String(instruction),
            startDate: String(startDate),
            dueDate: dueDate,
            documentIdList: uploading,
          },
          {
            headers: {
              Authorization: `Bearer ${sessionToken}`,
            },
          },
        );
        toast.success(lang.successUploadAssignment);
        setFileClass([]);
        setCreateAssignment({
          classId: userChatAktif?.classId,
          name: "",
          instruction: "",
          startDate: new Date().toISOString(),
        });
        setDate(undefined);
        setModalClass(false);
        return response.data;
      } catch (error) {
        console.error(error);
        setIsloadingModal(false);
        throw error;
      } finally {
        setIsloadingModal(false);
      }
    },
    {
      onError: (error) =>
        toast.error(`Error: ${error.response?.data?.meta?.message}`),
    },
  );

  const postQuiz = useMutation(
    async (data: PostAssignment): Promise<PostResponse> => {
      const { classId, name, instruction, startDate, dueDate } = data;
      if (!sessionToken) throw new Error("Missing session token");
      setIsloadingModal(true);
      try {
        const uploading = await handleUploadFiles();
        const response = await axios.post<PostResponse>(
          process.env.API_BE + `/api/class-quiz`,
          {
            classId: Number(classId),
            name: String(name),
            instruction: String(instruction),
            startDate: String(startDate),
            dueDate: dueDate,
            documentIdList: uploading,
          },
          {
            headers: {
              Authorization: `Bearer ${sessionToken}`,
            },
          },
        );
        return response.data;
      } catch (error) {
        console.error(error);
        setIsloadingModal(false);
        throw error;
      } finally {
        toast.success(lang.successUploadQuiz);
        setModalClass(false);
        setIsloadingModal(false);
        setFileClass([]);
        setCreateAssignment({
          classId: userChatAktif?.classId,
          name: "",
          instruction: "",
          startDate: new Date().toISOString(),
        });
        setDate(undefined);
      }
    },
    {
      onError: (error) =>
        toast.error(`Error: ${error.response?.data?.meta?.message}`),
    },
  );

  type PostExam = {
    classId: number;
    sessionId: number;
    name: string;
    description: string;
    startDate: string;
    dueDate: string | undefined;
    type: string;
  };

  const postExam = useMutation(
    async (data: PostExam): Promise<PostResponse> => {
      const { classId, sessionId, name, description, dueDate, type } = data;
      if (!sessionToken) throw new Error("Missing session token");
      setIsloadingModal(true);
      try {
        const uploading = await handleUploadFiles();
        const response = await axios.post<PostResponse>(
          process.env.API_BE + `/api/class-exam/${type}`,
          {
            classId: Number(classId),
            sessionId: Number(sessionId),
            name: String(name),
            description: String(description),
            dueDate: dueDate,
            documentIdList: uploading,
          },
          {
            headers: {
              Authorization: `Bearer ${sessionToken}`,
            },
          },
        );
        if (type === "uts") {
          toast.success(lang.successUploadExamUts);
        } else {
          toast.success(lang.successUploadExamUas);
        }
        return response.data;
      } catch (error) {
        setIsloadingModal(false);
        console.error(error);
        throw error;
      } finally {
        setModalClass(false);
        setIsloadingModal(false);
        setFileClass([]);
        setCreateExam({
          classId: userChatAktif?.classId,
          sessionId: 0,
          name: "",
          description: "",
        });
        setDate(undefined);
      }
    },
    {
      onError: (error) =>
        toast.error(`Error: ${error.response.data.meta.message}`),
    },
  );

  type PostSubmission = {
    id: number;
    type: string;
    description: string;
  };

  const postSubmission = useMutation(
    async (data: PostSubmission): Promise<PostResponse> => {
      const { type, id, description } = data;
      if (description.length === 0 && fileClass.length === 0)
        return toast.error("Semua field wajib di isi");
      if (!sessionToken) throw new Error("Missing session token");
      setIsloadingModal(true);
      try {
        const uploading = await handleUploadFiles();
        const response = await axios.post<PostResponse>(
          process.env.API_BE + `/api/${type}/${id}/submit`,
          {
            description: String(description),
            documentIdList: uploading,
          },
          {
            headers: {
              Authorization: `Bearer ${sessionToken}`,
            },
          },
        );
        if (type === "class-assignment") {
          toast.success(lang.successUploadSubAssg);
        } else if (type === "class-quiz") {
          toast.success(lang.successUploadSubQuiz);
        } else {
          toast.success(lang.successUploadSubQuiz);
        }

        setModalClass(false);
        return response.data;
      } catch (error) {
        console.error(error);
        setIsloadingModal(false);
        throw error;
      } finally {
        setIsloadingModal(false);
        setFileClass([]);
        setCreateExam({
          classId: userChatAktif?.classId,
          sessionId: 0,
          name: "",
          description: "",
        });
        setDate(undefined);
      }
    },
    {
      onError: (error) =>
        toast.error(`Error: ${error?.response?.data?.meta?.message}`),
    },
  );

  const handleAttend = useMutation(
    async (metadataId): Promise<PostResponse> => {
      if (!sessionToken) throw new Error("Missing session token");
      try {
        const response = await axios.post<PostResponse>(
          process.env.API_BE + `/api/class-attendance/${metadataId}/attend`,
          {},
          {
            headers: {
              Authorization: `Bearer ${sessionToken}`,
            },
          },
        );
        toast.success(lang.successPresent);
        return response.data;
      } catch (error) {
        console.error(error);
        throw error;
      } finally {
        getAttendance();
      }
    },
    {
      onError: (error) =>
        toast.error(`Error: ${error?.response?.data?.meta?.message}`),
    },
  );

  interface DeleteResponse extends Response {}
  const deleteClass = useMutation(async (classId): Promise<DeleteResponse> => {
    if (!sessionToken) throw new Error("Missing session token");
    try {
      const response = await axios.delete<DeleteResponse>(
        process.env.API_BE + `/api/class/${classId}`,
        {
          headers: {
            Authorization: `Bearer ${sessionToken}`,
          },
        },
      );
      console.log(response);
      toast.success(lang.successDeleteClass);
      setalert(false);
      return response.data;
    } catch (error) {
      console.error(error);
      throw error;
    }
  });

  type PostGradeQuiz = {
    id: number;
    grade: number;
    comment: string | undefined;
  };

  const postGradeQuiz = useMutation(
    async (data: PostGradeQuiz): Promise<PostResponse> => {
      const { id, grade, comment } = data;
      if (!sessionToken) throw new Error("Missing session token");
      try {
        const response = await axios.post<PostResponse>(
          process.env.API_BE + `/api/class-quiz/${tempQuizId}/${id}/grade`,
          {
            grade: Number(grade),
            comment: String(comment),
          },
          {
            headers: {
              Authorization: `Bearer ${sessionToken}`,
            },
          },
        );
        toast.success(lang.successScored);
        getQuiz(tempQuizId);
        setOpen(false);
        return response.data;
      } catch (error) {
        console.error(error);
        throw error;
      }
    },
  );

  type PostGradeAssignment = {
    id: number;
    grade: number;
    comment: string | undefined;
  };

  const postGradeAssignment = useMutation(
    async (data: PostGradeAssignment): Promise<PostResponse> => {
      const { id, grade, comment } = data;
      if (!sessionToken) throw new Error("Missing session token");
      try {
        const response = await axios.post<PostResponse>(
          process.env.API_BE +
            `/api/class-assignment/${tempQuizId}/${id}/grade`,
          {
            grade: Number(grade),
            comment: String(comment),
          },
          {
            headers: {
              Authorization: `Bearer ${sessionToken}`,
            },
          },
        );
        toast.success(lang.successScored);
        getAssignment(tempQuizId);
        setOpen(false);
        return response.data;
      } catch (error) {
        console.error(error);
        throw error;
      }
    },
  );

  type PostGradeExam = {
    studentId: number;
    grade: number;
    comment: string;
    examId: number;
  };

  const postGradeExam = useMutation(
    async (data: PostGradeExam): Promise<PostResponse> => {
      const { studentId, grade, comment, examId } = data;
      if (!sessionToken) throw new Error("Missing session token");
      try {
        const response = await axios.post<PostResponse>(
          process.env.API_BE + `/api/class-exam/${examId}/grade`,
          {
            studentId: Number(studentId),
            grade: Number(grade),
            comment: String(comment),
          },
          {
            headers: {
              Authorization: `Bearer ${sessionToken}`,
            },
          },
        );
        toast.success(lang.successScored);
        getExam(userChatAktif?.classId, false, "uas");
        getExam(userChatAktif?.classId, false, "uts");
        setOpen(false);
        return response.data;
      } catch (error) {
        console.error(error);
        throw error;
      }
    },
  );
  const allTimetoken = messageHistory?.map((d) => d.timetoken);

  const filter = messageHistory?.reduce((accumulator, message) => {
    // const metadataId = message.message.metadata?.id;
    const createdAt = message.message.createdAt;
    const textSeason = message.message?.metadata?.title;

    // Tambahkan data tanpa metadata langsung ke dalam accumulator
    if (!createdAt) {
      accumulator.push({
        message: message.message,
        timetoken: message.timetoken,
        // tambahkan properti lain yang ingin Anda tampilkan
      });
    } else {
      // Jika metadata.id belum ada di dalam accumulator, tambahkan data
      // Harusnya ini gaperlu karena duplicate dari pubnub
      // Masalahnya ketka zoom ternyata metadata idnya dari class wkwkkw
      if (
        !accumulator.some(
          (item) => item.message.createdAt === createdAt,
          // (item) =>  item.message.metadata?.id === metadataId,
        )
      ) {
        if (!accumulator.some((item) => item.message?.metadata?.title === textSeason && item.message?.text === "SESSION")) {
          accumulator.push({
            message: message.message,
            timetoken: message.timetoken,
            // tambahkan properti lain yang ingin Anda tampilkan
          });
        }
      }
    }

    return accumulator;
  }, []);

  const groupByDate = filter?.reduce((accumulator: any, message: any) => {
    const date = convertTime(
      message.message.pn_gcm?.data?.custom_data?.metadata?.timetoken ||
        message.message?.metadata?.timetoken ||
        message?.timetoken,
    );
    if (!accumulator[date]) accumulator[date] = [];
    accumulator[date].push(message);
    return accumulator;
  }, {});

  const [isLoadAttend, setIsLoadAttend] = useState(false);

  const getAttendance = async () => {
    setIsLoadAttend(true);
    try {
      const classId = userChatAktif?.classId;
      const res = await axios.get(
        `${process.env.API_BE}/api/class/${classId}/attendance?withUserList=true`,
        {
          headers: {
            Authorization: `Bearer ${sessionToken}`,
          },
        },
      );
      // console.log("res:", res.data.data.items);
      // console.log(res.data.data.items);
      queryClient.setQueryData(["attendanceClass"], res.data.data.items);
      setIsLoadAttend(false);
    } catch (error) {
      setIsLoadAttend(false);
      console.error("Gagal mengambil data Absen", error);
    }
  };

  const { data: attendanceClass } = useQuery<any>(["attendanceClass"], {
    enabled: false,
    cacheTime: 0,
    retry: 2,
  });

  let idToCheck = sessionData?.user?.id;

  // Menggunakan reduce untuk menghitung jumlah kemunculan id
  let count = attendanceClass?.reduce((accumulator, currentValue) => {
    // Filter userlist untuk setiap item dalam data
    let userCount = currentValue?.userList?.filter(
      (user) => user.userId === idToCheck,
    ).length;
    return accumulator + userCount;
  }, 0);

  let totalData = attendanceClass?.length;

  let percentage;
  if (
    typeof totalData !== "undefined" &&
    !isNaN(totalData) &&
    totalData !== 0
  ) {
    percentage = (count / totalData) * 100;
  } else {
    percentage = 0; // Default value if totalData is undefined, NaN, or 0
  }

  let roundedValue = Math.round(percentage);
  let todayAttendance;

  if (attendanceClass) {
    todayAttendance = checkTodayAttendance(attendanceClass);
  }

  function checkTodayAttendance(attendanceClass) {
    let latestClass = null;

    for (let i = 0; i < attendanceClass.length; i++) {
      const classObj = attendanceClass[i];

      // Set waktu mulai hari ini menjadi 00:00:00
      const startTimeToday = new Date(classObj.startTime);
      startTimeToday.setHours(0);
      startTimeToday.setMinutes(0);
      startTimeToday.setSeconds(0);

      let endTimeToday = null;

      // Cek apakah index selanjutnya tersedia
      if (i + 1 < attendanceClass.length) {
        const nextClassStartTime = new Date(attendanceClass[i + 1].startTime);
        endTimeToday = new Date(nextClassStartTime);
        endTimeToday.setHours(0);
        endTimeToday.setMinutes(0);
        endTimeToday.setSeconds(0);
      } else {
        endTimeToday = new Date(classObj.startTime);
        endTimeToday.setHours(0);
        endTimeToday.setMinutes(0);
        endTimeToday.setSeconds(0);
      }

      const currentTime = new Date();
      if (currentTime >= startTimeToday && currentTime <= endTimeToday) {
        return classObj;
      } else {
        latestClass = classObj;
      }
    }

    return latestClass;
  }

  const [isSelectChat, setIsSelectChat] = useState(true);

  useEffect(() => {
    if (userChatAktif?.classId) {
      getAttendance();
    }

    setIsSelectChat(true);
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [userChatAktif]);

  useEffect(() => {
    if (selectedFile?.file) {
      setXpop(false);
    }
  }, [selectedFile]);

  useEffect(() => {
    if (!modalClass) {
      setCreateMaterial({
        classId: userChatAktif?.classId,
        title: "",
        description: "",
        documentIdList: [],
      });
      setTempDocId([]);
      setDate(null);
      setFileClass([]);
      setEdit("");
      setdelmode("");
      setdelmode("");
    } else {
      setMultiMessage([]);
    }
  }, [modalClass, userChatAktif, setMultiMessage]);

  function isEndTimePassed(endTime) {
    const currentTime = new Date();
    const endTimeDate = new Date(endTime);

    return currentTime > endTimeDate;
  }

  const [tempContext, setTempContext] = useState({});
  const [chatHistory, setChatHistory] = useState([]);
  const handleCopyClick = (chat) => {
    const source = chat ?? chatHistory;
    const textToCopy = source.join("\n");
    navigator.clipboard.writeText(textToCopy);
    toast.success("Sukses menyalin text");
    setChatHistory([]);
    setMultiMessage([]);
  };

  useEffect(() => {
    if (!alert2) {
      setMultiMessage([]);
      setChatHistory([]);
    }
  }, [alert2, setMultiMessage, setChatHistory]);

  const isLG = useMediaQuery({ query: "(max-width: 1230px)" });
  const roleDelete =
    chatMode === "private" ||
    (chatMode === "group" && isAdmin) ||
    (chatMode === "group" && userChatAktif?.isAdmin) ||
    (chatMode === "class" && isAdmin);

  const [isCalendarOpen, setIsCalendarOpen] = useState({
    createUts: false,
    createUas: false,
    createAssign: false,
    createQuiz: false,
  });
  const [tabExam, setTabExam] = useState("uts");
  const isSubmittedUas = subExamUas?.some(
    (item) => item.studentId === sessionData?.user?.id,
  );
  const isSubmittedUts = subExamUts?.some(
    (item) => item.studentId === sessionData?.user?.id,
  );

  const [alertVideo, setAlertVideo] = useState(false);
  // Inisialisasi variabel

  const [nameColors, setNameColors] = useState({});

  const customColors = ["#3366cc", "#dc3912", "#ff9900", "#109618", "#990099"];

  const generateRandomColor = (name) => {
    if (nameColors[name]) {
      return nameColors[name];
    }

    const randomColor =
      customColors[Math.floor(Math.random() * customColors.length)];

    setNameColors((prevColors) => ({
      ...prevColors,
      [name]: randomColor,
    }));

    return randomColor;
  };

  function isPrivateChannel(channel: string): boolean {
    return channel.startsWith("private.");
  }
  const storagePubnub = JSON.parse(localStorage.getItem("sessionPubnub") ?? "");
  const pubnub = new PubNub({
    publishKey: storagePubnub.publishKey,
    subscribeKey: storagePubnub.subscribeKey,
    uuid: storagePubnub.uuid,
  });

  const [typingTimeout, setTypingTimeout] = useState(null);
  const handleTyping = async (e) => {
    setInputText(e.target.value);
    const isPrivate = isPrivateChannel(userChatAktif?.channel);

    const fullName: string = userChatAktif?.sender?.name;

    let firstName: string;

    if (fullName.includes(" ")) {
      const nameParts: string[] = fullName.split(" ");

      firstName = nameParts[0];
    } else {
      firstName = fullName;
    }

    let msg;
    if (isPrivate) {
      msg = lang.typing;
    } else {
      msg = firstName + " " + lang.typing;
    }

    pubnub.signal(
      {
        message: msg,
        channel: userChatAktif?.channel,
      },
      (status, response) => {
        //console.log("response:", response);
        //console.log("status:", status);
        // handle status, response
      },
    );

    if (typingTimeout) {
      clearTimeout(typingTimeout);
    }

    const timeout = setTimeout(() => {
      pubnubSignalOff();
    }, 1000);

    setTypingTimeout(timeout);
  };

  const pubnubSignalOff = () => {
    pubnub.signal(
      {
        message: "typing_off",
        channel: userChatAktif?.channel,
      },
      (status, response) => {
        console.log("response:", response);
        // handle status, response
        if (status.error) {
          // Jika terjadi kesalahan, coba kembali setelah beberapa waktu
          console.error("Error while sending signal:", status.errorData);
          setTypingTimeout(setTimeout(pubnubSignalOff, 1000));
        }
      },
    );
  };
  const handleStoppedTyping = () => {
    console.log("typing_off event triggered");
  };

  const isPreMD = useMediaQuery({ query: "(min-width: 767px)" });

  const [isClicked, setIsClicked] = useState(false);

  useEffect(() => {
    let timer;
    if (isClicked) {
      // Set state menjadi false setelah 20 detik
      timer = setTimeout(() => {
        setIsClicked(false);
      }, 10000);
    }

    // Membersihkan timer jika komponen tidak lagi dirender
    return () => clearTimeout(timer);
  }, [isClicked]);

  useEffect(() => {
    setIsClicked(false);
  }, [chatMode]);

  const handleClick = () => {
    setIsClicked(true);
  };

  // filter metadata.category "created_zoom" atau "removed_zoom"
  const filteredMessages = messageHistory.filter((message) => {
    const chatCategory = message?.message?.metadata?.category;
    return chatCategory === "created_zoom" || chatCategory === "removed_zoom";
  });

  filteredMessages.sort((a, b) => b.timetoken - a.timetoken);

  let zoomMessage = null;
  let urlZoomMeet = null;

  if (filteredMessages.length > 0) {
    zoomMessage = filteredMessages[0];
    if (filteredMessages[0]?.message?.metadata?.url) {
      urlZoomMeet = filteredMessages[0]?.message?.metadata?.url;
    }
  }

  return (
    <>
      <ReusableAlertDialog
        open={alertVideo}
        onOpenChange={setAlertVideo}
        title={lang.promptDeactivedZoom}
        description={lang.promptDesc}
        textDelete={lang.btnDeactivedZoom}
        isLoading={actVideoConf?.isLoading}
        onDelete={() =>
          actVideoConf.mutate({
            classId: userChatAktif?.classId,
            urlConf: null,
            type: "delete",
          })
        }
      />
      <ReusableAlertDialog
        open={alert}
        onOpenChange={setalert}
        title={lang.promptDeleteClass}
        description={lang.promptDesc}
        onDelete={() => deleteClass.mutate(userChatAktif.classId)}
        isLoading={deleteClass?.isLoading}
      />
      <ReusableAlertDialog
        open={alert2}
        onOpenChange={setalert2}
        title={
          multiMessage.length > 1
            ? "Apakah Anda yakin ingin menghapus semua pesan ?"
            : lang.promptDeleteMsg
        }
        description={lang.promptDesc}
        isLoading={null}
        onDelete={() => {
          deleteMessage();
          setalert2(false);
        }}
      />
      {/*INI MODAL*/}
      {modalClass && (
        <ModalChat
          getMessageUser={getMessageUser}
          viewportHeight={viewportHeight}
          inputText={inputText}
          setInputText={setInputText}
          isHeight={isHeight}
          inputTextRef={inputTextRef}
          lang={lang}
          userChatAktif={userChatAktif}
          isFetchMessage={isFetchMessage}
          messageHistory={messageHistory}
          todoListRef={todoListRef}
          sendMessage={sendMessage}
          chatMode={chatMode}
          removeItem={removeItem}
          formatBytes={formatBytes}
          handleButtonClick={handleButtonClick}
          handleFileChange={handleFileChange}
          sendFile={sendFile}
          handleDelete={handleDelete}
          handleDragOver={handleDragOver}
          handleDragEnter={handleDragEnter}
          handleDrop={handleDrop}
          handleDragLeave={handleDragLeave}
          selectedFile={selectedFile}
          isLoadingFile={isLoadingFile}
          isSentMessage={isSentMessage}
          modalType={modalType}
          setModalType={setModalType}
          getExam={getExam}
          dataUjianUts={dataUjianUts}
          dataUjianUas={dataUjianUas}
          subExamUts={subExamUts}
          subExamUas={subExamUas}
          getInfoClass={getInfoClass}
          dataInfoKelas={dataInfoKelas}
          getInfoMembers={getInfoMembers}
          dataMember={dataMember}
          getMaterial={getMaterial}
          getAllMaterial={getAllMaterial}
          getAllAssignment={getAllAssignment}
          getAllQuiz={getAllQuiz}
          allMaterial={allMaterial}
          allQuiz={allQuiz}
          allAssignment={allAssignment}
          material={material}
          getQuiz={getQuiz}
          getAssignment={getAssignment}
          singleSubAssign={singleSubAssign}
          singleSubQuiz={singleSubQuiz}
          dataKuis={dataKuis}
          subQuiz={subQuiz}
          dataTugas={dataTugas}
          subAssignment={subAssignment}
          addFriend={addFriend}
          modalClass={modalClass}
          isLoadingModal={isLoadingModal}
          setIsloadingModal={setIsloadingModal}
          setModalClass={setModalClass}
          setHeading={setHeading}
          setSideType={setSideType}
          setSidebar={setSidebar}
          inputFileRef={inputFileRef}
          multiMessage={multiMessage}
          setMultiMessage={setMultiMessage}
          deleteMessage={deleteMessage}
          handleAttend={handleAttend}
          attendanceClass={attendanceClass}
          tabExam={tabExam}
          edit={edit}
          isSubmittedUas={isSubmittedUas}
          isSubmittedUts={isSubmittedUts}
          setInputQuiz={setInputQuiz}
          open={open}
          setOpen={setOpen}
          handleDropClass={handleDropClass}
          handleFileChangeClass={handleFileChangeClass}
          fileClass={fileClass}
          setFileClass={setFileClass}
          tempDocId={tempDocId}
          setTempDocId={setTempDocId}
          setCreateMaterial={setCreateMaterial}
          createMaterial={createMaterial}
          postSubmission={postSubmission}
          inputQuiz={inputQuiz}
          postGradeExam={postGradeExam}
          setCreateExam={setCreateExam}
          createExam={createExam}
          isCalendarOpen={isCalendarOpen}
          setIsCalendarOpen={setIsCalendarOpen}
          date={date}
          postExam={postExam}
          postGradeAssignment={postGradeAssignment}
          urlConf={urlConf}
          setUrlConf={setUrlConf}
          urlConfSucces={urlConfSucces}
          actVideoConf={actVideoConf}
          setAlertVideo={setAlertVideo}
          postMaterial={postMaterial}
          postAssignment={postAssignment}
          postQuiz={postQuiz}
          setCreateAssignment={setCreateAssignment}
          createAssignment={createAssignment}
          setDate={setDate}
          tempQuizId={tempQuizId}
          settempQuizId={settempQuizId}
          postGradeQuiz={postGradeQuiz}
          setEdit={setEdit}
          setdelmode={setdelmode}
        />
      )}
      {
        <div
          className={`h-[66px] px-4 flex sm:items-center justify-between gapx-3 [@media(min-width:900px)]:gap-x-5 py-3 border-b-2 border-gray-200  bg-[#9d21e6] text-white shadow-md ${
            isSM ? "rounded-b-xl" : "rounded-t-xl"
          }`}
        >
          <div className="relative flex items-center flex-1">
            <svg
              onClick={() => router.push("/class")}
              xmlns="http://www.w3.org/2000/svg"
              fill="none"
              viewBox="0 0 24 24"
              strokeWidth={2.5}
              stroke="currentColor"
              className=" w-6 h-6 mr-3 cursor-pointer [@media(min-width:900px)]:hidden flex-none"
            >
              <path
                strokeLinecap="round"
                strokeLinejoin="round"
                d="M15.75 19.5 8.25 12l7.5-7.5"
              />
            </svg>
            {chatMode === "private" || chatMode === "group" ? (
              <>
                <div className={` flex-none relative`}>
                  <Image
                    src={
                      userChatAktif?.receiver?.photo
                        ? userChatAktif?.receiver?.photo
                        : `/static/images/logokedi.svg`
                    }
                    alt="profilePict"
                    width={40}
                    height={40}
                    className="w-10 h-10 rounded-full ring-1 ring-white object-cover"
                  />
                </div>
                <div className="ml-4 flex flex-1 flex-col leading-tight ">
                  <div className=" mt-1 font-semibold">
                    {userChatAktif?.receiver?.name}
                  </div>
                  <div className="line-clamp-1 text-[13px]">
                    {userChatAktif?.name}
                  </div>
                </div>
              </>
            ) : chatMode === "class" && isAdmin ? (
              <>
                {isLoadAttend ? (
                  <div className="translate-x-10 absolute z-50">
                    <LoadingSpinner stroke={`#FFF`} size={30} strokeWidth={2} />
                  </div>
                ) : (
                  todayAttendance && (
                    <div className="[@media(max-width:900px)]:hidden text-white border-white border rounded-lg py-1 px-10 hover:bg-purple-600 duration-300  hover:text-white cursor-pointer text-sm font-medium">
                      {todayAttendance && todayAttendance?.name}
                    </div>
                  )
                )}

                <div
                  className={`flex-none relative [@media(min-width:900px)]:hidden inline-flex`}
                >
                  <Image
                    src={
                      userChatAktif?.receiver?.photo
                        ? userChatAktif?.receiver?.photo
                        : `/static/images/logokedi.svg`
                    }
                    alt="profilePict"
                    width={40}
                    height={40}
                    className="w-10 h-10 rounded-full ring-1 ring-white object-cover"
                  />
                </div>
                <div className=" [@media(min-width:900px)]:hidden ml-4 flex flex-1 flex-col leading-tight ">
                  <div className=" mt-1 font-semibold">
                    {userChatAktif?.receiver?.name}
                  </div>
                  <div className="line-clamp-1 text-[13px]">
                    {userChatAktif?.name}
                  </div>
                </div>
              </>
            ) : (
              <>
                <>
                  <div
                    className={`flex-none relative [@media(min-width:900px)]:hidden inline-flex`}
                  >
                    <Image
                      src={
                        userChatAktif?.receiver?.photo
                          ? userChatAktif?.receiver?.photo
                          : `/static/images/logokedi.svg`
                      }
                      alt="profilePict"
                      width={40}
                      height={40}
                      className="w-10 h-10 rounded-full ring-1 ring-white"
                    />
                  </div>
                  <div className=" [@media(min-width:900px)]:hidden ml-4 flex flex-1 flex-col leading-tight ">
                    <div className=" mt-1 font-semibold">
                      {userChatAktif?.receiver?.name}
                    </div>
                    <div className="line-clamp-1 text-[13px]">
                      {userChatAktif?.name}
                    </div>
                  </div>
                </>
                <div className="h-[45px] hidden [@media(min-width:900px)]:flex gap-x-2.5  sm:gap-x-5 items-center bg-[#D7B9FD] text-black rounded-[10px] px-3 py-3 w-full lg:max-w-[550px]">
                  <div className="flex-1">
                    <div className="text-xs font-semibold pb-0.5">
                      {sessionData?.user?.fullName}
                    </div>
                    <Progress.Root className="relative h-1.5 w-full overflow-hidden rounded-full bg-white">
                      <Progress.Indicator
                        className="h-full w-full flex-1 bg-[#17A56A] transition-all "
                        style={{
                          transform: `translateX(-${
                            100 - (roundedValue | 0)
                          }%)`,
                        }}
                      />
                    </Progress.Root>
                    <div className="pt-0.5 flex justify-between gap-x-2">
                      <div className="text-xs">Keaktifan</div>
                      <div className="text-xs">{roundedValue || 0}%</div>
                    </div>
                  </div>
                  <div>
                    {todayAttendance && (
                      <div className="text-[#9D21E6] border-[#9D21E6] border rounded-md py-1 px-10 hover:bg-purple-600 duration-300  hover:text-white cursor-pointer text-sm">
                        {todayAttendance?.name}
                      </div>
                    )}
                  </div>
                </div>
              </>
            )}
          </div>
          <div className="flex items-center space-x-2 ">
            {chatMode === "class" && isAdmin ? (
              <button
                className="z-20"
                onClick={() => {
                  setModalType("Video Conf");
                  setModalClass(true);
                  setUrlConf(
                    urlConfSucces
                      ? urlConfSucces
                      : userChatAktif?.videoUrl ?? "",
                  );

                  if (urlConfSucces === "") {
                    setUrlConfSucces(userChatAktif?.videoUrl ?? "");
                  }
                }}
                type="button"
                id="buttonVideoConf"
              >
                <svg
                  xmlns="http://www.w3.org/2000/svg"
                  fill="none"
                  viewBox="0 0 24 24"
                  strokeWidth={1.5}
                  stroke="currentColor"
                  className="w-6 h-6"
                >
                  <path
                    strokeLinecap="round"
                    strokeLinejoin="round"
                    d="m15.75 10.5 4.72-4.72a.75.75 0 0 1 1.28.53v11.38a.75.75 0 0 1-1.28.53l-4.72-4.72M4.5 18.75h9a2.25 2.25 0 0 0 2.25-2.25v-9a2.25 2.25 0 0 0-2.25-2.25h-9A2.25 2.25 0 0 0 2.25 7.5v9a2.25 2.25 0 0 0 2.25 2.25Z"
                  />
                </svg>
              </button>
            ) : zoomMessage &&
              zoomMessage?.message?.metadata?.category === "created_zoom" &&
              urlZoomMeet ? (
              <Link
                className="z-20"
                id="buttonVideoConfNonAdm"
                href={urlZoomMeet}
                target="_blank"
              >
                <svg
                  xmlns="http://www.w3.org/2000/svg"
                  fill="none"
                  viewBox="0 0 24 24"
                  strokeWidth={1.5}
                  stroke="currentColor"
                  className="w-6 h-6"
                >
                  <path
                    strokeLinecap="round"
                    strokeLinejoin="round"
                    d="m15.75 10.5 4.72-4.72a.75.75 0 0 1 1.28.53v11.38a.75.75 0 0 1-1.28.53l-4.72-4.72M4.5 18.75h9a2.25 2.25 0 0 0 2.25-2.25v-9a2.25 2.25 0 0 0-2.25-2.25h-9A2.25 2.25 0 0 0 2.25 7.5v9a2.25 2.25 0 0 0 2.25 2.25Z"
                  />
                </svg>
              </Link>
            ) : null}
            <DropdownMenu>
              {((chatMode === "private" && allTimetoken.length > 0) ||
                chatMode === "group" ||
                chatMode === "class") && (
                <DropdownMenuTrigger
                  className={` inline-flex items-center justify-center  h-10 sm:w-10 rounded-lg transition duration-500 ease-in-out focus:outline-none `}
                >
                  <svg
                    xmlns="http://www.w3.org/2000/svg"
                    width={20}
                    height={20}
                    fill="currentColor"
                    className="bi bi-three-dots-vertical"
                    viewBox="0 0 16 16"
                  >
                    <path d="M9.5 13a1.5 1.5 0 1 1-3 0 1.5 1.5 0 0 1 3 0zm0-5a1.5 1.5 0 1 1-3 0 1.5 1.5 0 0 1 3 0zm0-5a1.5 1.5 0 1 1-3 0 1.5 1.5 0 0 1 3 0z" />
                  </svg>
                </DropdownMenuTrigger>
              )}
              {chatMode === "private" && (
                <DropdownMenuContent className="mr-8">
                  <DropdownMenuLabel>Chat Menu</DropdownMenuLabel>
                  <DropdownMenuSeparator />
                  {roleDelete && allTimetoken.length > 0 && (
                    <DropdownMenuItem
                      onClick={() => {
                        setMultiMessage(allTimetoken);
                        setalert2(true);
                      }}
                    >
                      Clear Chat
                    </DropdownMenuItem>
                  )}
                </DropdownMenuContent>
              )}
              {chatMode === "group" && (
                <DropdownMenuContent className="mr-8">
                  <DropdownMenuLabel>{lang.groupMenu}</DropdownMenuLabel>
                  <DropdownMenuSeparator />
                  <DropdownMenuItem
                    onClick={() => {
                      getInfoGroup(userChatAktif?.groupId);
                      fetchDataSearch();
                    }}
                  >
                    {lang.groupInfo}
                  </DropdownMenuItem>
                  {isAdmin && (
                    <DropdownMenuItem onClick={() => setalert3(true)}>
                      {" "}
                      Hapus Group{" "}
                    </DropdownMenuItem>
                  )}
                  <DropdownMenuItem onClick={() => setAlertGroup(true)}>
                    {" "}
                    Keluar Group{" "}
                  </DropdownMenuItem>
                  {roleDelete && allTimetoken.length > 0 && (
                    <DropdownMenuItem
                      onClick={() => {
                        setMultiMessage(allTimetoken);
                        setalert2(true);
                      }}
                    >
                      Clear Chat
                    </DropdownMenuItem>
                  )}
                </DropdownMenuContent>
              )}
              {chatMode === "class" && (
                <DropdownMenuContent className="mr-8">
                  <DropdownMenuLabel>{lang.activeCLass}</DropdownMenuLabel>
                  <DropdownMenuSeparator />
                  <DropdownMenuItem
                    onClick={() => getInfoClass(userChatAktif?.classId)}
                  >
                    {lang.classInfo}
                  </DropdownMenuItem>
                  <DropdownMenuItem
                    onClick={() => getInfoMembers(userChatAktif?.classId)}
                  >
                    {lang.members}
                  </DropdownMenuItem>
                  <DropdownMenuItem
                    onClick={() => getAllMaterial(userChatAktif?.classId)}
                  >
                    {lang.material}
                  </DropdownMenuItem>
                  <DropdownMenuItem
                    onClick={() => getAllAssignment(userChatAktif?.classId)}
                  >
                    {lang.assignments}
                    {lang.noM}
                  </DropdownMenuItem>
                  <DropdownMenuItem
                    onClick={() => getAllQuiz(userChatAktif?.classId)}
                  >
                    {lang.quizzes}
                  </DropdownMenuItem>
                  <DropdownMenuItem
                    onClick={() => {
                      if (isLG) {
                        setModalType("Absensi");
                        setModalClass(true);
                      } else {
                        setSideType("Absensi");
                        setSidebar(true);
                      }
                      setHeading("Absensi");
                    }}
                  >
                    {lang.attendance}
                  </DropdownMenuItem>
                  <DropdownMenuItem
                    onClick={() => {
                      const isStudent =
                        userChatAktif?.receiver.id !== sessionData?.user.uuid;
                      getExam(userChatAktif?.classId, isStudent, "uas");
                      getExam(userChatAktif?.classId, isStudent, "uts");
                    }}
                  >
                    {lang.exam}
                  </DropdownMenuItem>
                  {isAdmin && (
                    <DropdownMenuItem onClick={() => setalert(true)}>
                      {lang.menuDeleteClass}
                    </DropdownMenuItem>
                  )}
                  {roleDelete && allTimetoken.length > 0 && (
                    <DropdownMenuItem
                      onClick={() => {
                        setMultiMessage(allTimetoken);
                        setalert2(true);
                      }}
                    >
                      Clear Chat
                    </DropdownMenuItem>
                  )}
                </DropdownMenuContent>
              )}
            </DropdownMenu>
          </div>
        </div>
      }

      {isClicked && chatMode === "class" ? (
        <>
          {isAdmin ? (
            <div
              onClick={() => setIsClicked(false)}
              className={`z-20 absolute ${isMobileView ? "top-[4.2rem]" : "top-[8.2rem] sm:top-[9rem]"} [@media(min-width:900px)]:hidden flex gap-x-2.5  sm:gap-x-5 items-center bg-[#D7B9FD] text-black rounded-[10px] h-[60px] px-3 py-2 w-full lg:max-w-[550px] shadow-xl `}
            >
              <div className="flex-1">
                <div className="text-xs font-semibold pb-0.5">
                  {sessionData?.user?.fullName}
                </div>
                <Progress.Root className="relative h-2 w-full overflow-hidden rounded-full bg-white">
                  <Progress.Indicator
                    className="h-full w-full flex-1 bg-[#17A56A] transition-all "
                    style={{
                      transform: `translateX(-${100 - (roundedValue | 0)}%)`,
                    }}
                  />
                </Progress.Root>
                <div className="pt-0.5 flex justify-between gap-x-2">
                  <div className="text-xs">Kehadiran</div>
                  <div className="text-xs">{roundedValue || 0}%</div>
                </div>
              </div>
              <div>
                <div className="text-[#9D21E6] border-[#9D21E6] border rounded-md py-1 px-10 hover:bg-purple-600 duration-300  hover:text-white cursor-pointer text-sm">
                  {todayAttendance && todayAttendance?.name}
                </div>
              </div>
            </div>
          ) : (
            <div
              onClick={() => setIsClicked(false)}
              className={`z-20 absolute ${isMobileView ? "top-[4.2rem]" : "top-[8.2rem] sm:top-[9rem]"}  [@media(min-width:900px)]:hidden flex gap-x-2.5  sm:gap-x-5 items-center bg-[#D7B9FD] text-black rounded-[10px] h-[60px] px-3 py-2 w-full lg:max-w-[550px] shadow-xl`}
            >
              <div className="flex-1">
                <div className="text-xs font-semibold pb-0.5">
                  {sessionData?.user?.fullName}
                </div>
                <Progress.Root className="relative h-2 w-full overflow-hidden rounded-full bg-white">
                  <Progress.Indicator
                    className="h-full w-full flex-1 bg-[#17A56A] transition-all "
                    style={{
                      transform: `translateX(-${100 - (roundedValue | 0)}%)`,
                    }}
                  />
                </Progress.Root>
                <div className="pt-0.5 flex justify-between gap-x-2">
                  <div className="text-xs">Keaktifan</div>
                  <div className="text-xs">{roundedValue || 0}%</div>
                </div>
              </div>
              <div>
                {todayAttendance && (
                  <div className="text-[#9D21E6] border-[#9D21E6] border rounded-md py-1 px-10 hover:bg-purple-600 duration-300  hover:text-white cursor-pointer text-sm">
                    {todayAttendance?.name}
                  </div>
                )}
              </div>
            </div>
          )}
        </>
      ) : (
        chatMode === "class" && (
          <button
            onClick={handleClick}
            className={` z-20 absolute ${isMobileView ? "top-[4rem]" : "top-[8rem] sm:top-[8.8rem] "}  [@media(min-width:900px)]:hidden flex justify-center items-center left-[50%] transform translate-x-[-50%]  right-0 w-fit`}
          >
            <svg
              xmlns="http://www.w3.org/2000/svg"
              fill="none"
              viewBox="0 0 24 24"
              strokeWidth={3}
              stroke="currentColor"
              className="w-5 h-5"
            >
              <path
                strokeLinecap="round"
                strokeLinejoin="round"
                d="m19.5 8.25-7.5 7.5-7.5-7.5"
              />
            </svg>
          </button>
        )
      )}

      {handleAttend?.isLoading && (
        <div
          className={`absolute bg-white/10 left-1/2 top-1/2 transform -translate-x-1/2 -translate-y-1/2 h-full w-full ${
            isSM ? "" : ""
          } flex justify-center items-center z-[70]`}
        >
          <LoadingSpinner size={70} strokeWidth={2} />
        </div>
      )}
      {isFetchMessage && isFetchFirst ? (
        <div
          role="status"
          className="p-4 space-y-5 divide-y divide-gray-200 rounded  animate-pulse dark:divide-gray-700 overflow-y-auto"
          style={
            isPreMD && selectedFile?.file !== null
              ? { height: viewportHeight - 280 - isHeight }
              : selectedFile?.file !== null && !isMobileView
                ? { height: viewportHeight - 300 }
                : isPreMD && selectedFile?.file === null && !isMobileView
                  ? {
                      height: viewportHeight - 215 - isHeight,
                      transition: "height 0.3s ease-in-out",
                    }
                  : isMobileView && selectedFile?.file === null
                    ? {
                        height: viewportHeight - 135 - isHeight,
                        transition: "height 0.3s ease-in-out",
                      }
                    : isMobileView && selectedFile?.file !== null
                      ? { height: viewportHeight - 180 }
                      : { height: viewportHeight - 270 }
          }
        >
          {[...Array(20)].map((_, index) => (
            <div key={index} className="flex items-center justify-between pt-3">
              <div className="flex items-center">
                <svg
                  className="w-10 h-10 me-3 text-gray-200 dark:text-gray-700"
                  aria-hidden="true"
                  xmlns="http://www.w3.org/2000/svg"
                  fill="currentColor"
                  viewBox="0 0 20 20"
                >
                  <path d="M10 0a10 10 0 1 0 10 10A10.011 10.011 0 0 0 10 0Zm0 5a3 3 0 1 1 0 6 3 3 0 0 1 0-6Zm0 13a8.949 8.949 0 0 1-4.951-1.488A3.987 3.987 0 0 1 9 13h2a3.987 3.987 0 0 1 3.951 3.512A8.949 8.949 0 0 1 10 18Z" />
                </svg>
                <div>
                  <div className="h-2.5 bg-gray-200 rounded-full dark:bg-gray-700 w-32 mb-2" />
                  <div className="w-48 h-2 bg-gray-200 rounded-full dark:bg-gray-700" />
                </div>
              </div>
              <div className="h-2.5 bg-gray-300 rounded-full dark:bg-gray-700 w-12" />
            </div>
          ))}
        </div>
      ) : messageHistory?.length === 0 ? (
        <>
          {!isFetchMessage && (
            <div className="text-lg flex items-center justify-center border-x h-full">
              {lang.noMessage}
            </div>
          )}
          <div
            className={` ${
              chatMode === "class" ? "pl-16" : "pl-5"
            }  py-2  flex items-center gap-2 truncate max-w-[220px] md:max-w-[270px] text-[13px] font-medium`}
          >
            <TypingMessage />
          </div>
        </>
      ) : (
        <ContextMenu>
          <ContextMenuTrigger disabled={!roleDelete}>
            <ToggleGroup
              // disabled={false}
              disabled={isSelectChat}
              value={multiMessage}
              onValueChange={(value) => {
                if (value) setMultiMessage(value);
              }}
              type="multiple"
              onDragOver={handleDragOver}
              onDragEnter={handleDragEnter}
              onDragLeave={handleDragLeave}
              onDrop={handleDrop}
              id="messages"
              ref={todoListRef}
              style={
                isPreMD && selectedFile?.file !== null
                  ? { height: viewportHeight - 280 - isHeight }
                  : selectedFile?.file !== null && !isMobileView
                    ? { height: viewportHeight - 300 }
                    : isPreMD && selectedFile?.file === null && !isMobileView
                      ? {
                          height: viewportHeight - 215 - isHeight,
                          transition: "height 0.3s ease-in-out",
                        }
                      : isMobileView && selectedFile?.file === null
                        ? {
                            height: viewportHeight - 135 - isHeight,
                            transition: "height 0.3s ease-in-out",
                          }
                        : isMobileView && selectedFile?.file !== null
                          ? { height: viewportHeight - 180 }
                          : { height: viewportHeight - 270 }
              }
              className={` flex flex-col  flex-1 p-2  border-x  overflow-y-auto`}
            >
              {messageHistory.length === 25 && (
                <div
                  ref={messageHistory.length === 25 ? refObserver : null}
                  className=""
                >
                  <div className="flex items-center justify-center w-full ">
                    <LoadingSpinner size={30} strokeWidth={2} />
                  </div>
                </div>
              )}
              {!groupByDate && !isFetchMessage && (
                <div className="flex items-center justify-center h-full opacity-80">
                  {" "}
                  {lang.noMessage}{" "}
                </div>
              )}
              <div className="mt-auto"></div>
              {groupByDate &&
                Object.keys(groupByDate)?.map((key) => (
                  <React.Fragment key={key}>
                    <div className="mx-auto text-center my-2.5 px-2 py-0.5 bg-[#f3f4f6] w-fit text-gray-500 rounded-xl  text-xs">
                      {key}
                    </div>
                    {groupByDate[key].map((item: any, index: number, array) => {
                      // const id = item?.message?.metadata?.id;
                      const custom_data =
                        item?.message?.pn_gcm?.data?.custom_data;
                      const metadata = item?.message?.metadata;

                      let showName = false;
                      let showPict = false;
                      const picAuthor =
                        custom_data?.author?.imageUrl ||
                        item?.message?.author?.imageUrl;
                      const idAuthor =
                        custom_data?.author?.id || item?.message?.author?.id;
                      const nameAuthor =
                        custom_data?.author?.firstName ||
                        item?.message?.author?.firstName;

                      const nameColor = generateRandomColor(nameAuthor);

                      const isLast =
                        index === array.length - 1 ||
                        (index + 1 < array.length &&
                          idAuthor !==
                            array[index + 1]?.message?.pn_gcm?.data?.custom_data
                              ?.author?.id &&
                          idAuthor !== array[index + 1]?.message?.author?.id);

                      const isFirstForUser =
                        index === 0 ||
                        (idAuthor !==
                          array[index - 1]?.message?.pn_gcm?.data?.custom_data
                            ?.author?.id &&
                          idAuthor !== array[index - 1]?.message?.author?.id);

                      if (isFirstForUser) {
                        showName = true;
                      }

                      if (isLast) {
                        showPict = true;
                      }
                      const text = custom_data?.text || metadata?.title;
                      const typeMsg = custom_data?.type;
                      const imgType = typeMsg === "image";
                      const textType = typeMsg === "text";
                      const fileName = custom_data?.name;
                      const fileSize = custom_data?.size;
                      const chatCategory = metadata?.category;

                      const isAdminSystem =
                        chatCategory === "created_zoom" ||
                        chatCategory === "removed_zoom" ||
                        chatCategory === "created_group" ||
                        chatCategory === "add_member" ||
                        chatCategory === "leave_member" ||
                        chatCategory === "removed_member";

                      const isClassSystem =
                        chatCategory === "assignment" ||
                        chatCategory === "quiz" ||
                        chatCategory === "uas" ||
                        chatCategory === "uts" ||
                        chatCategory === "material";
                      // const isSistem =
                      //   chatCategory === "uts" && item.message.type === "system";
                      const isSession = chatCategory === "session";
                      const isTrue =
                        custom_data?.author?.id === userChatAktif?.sender.id ||
                        item?.message?.author?.id === userChatAktif?.sender.id;

                      return (
                        <ToggleGroupItem
                          key={index}
                          className="group chat-message mb-0.5 w-full duration-500 ease-in"
                          asChild
                          // onClick={()=> setTextToCopy([...textToCopy, text])}
                          onContextMenu={(event) => {
                            setTempContext({
                              event,
                              text,
                              timetoken: item.timetoken,
                            });
                          }}
                          onClick={(event) => {
                            const dataStateValue =
                              event.currentTarget.getAttribute("data-state");
                            if (dataStateValue === "off") {
                              setChatHistory((prevChatHistory) => [
                                ...prevChatHistory,
                                text,
                              ]);
                            } else {
                              setChatHistory((prevChatHistory) =>
                                prevChatHistory.filter((chat) => chat !== text),
                              );
                            }
                          }}
                          value={item.timetoken}
                          aria-label={item.timetoken}
                        >
                          <div
                            className={` ${
                              isSession | isAdminSystem
                                ? "mb-2"
                                : isTrue
                                  ? "flex items-end justify-end"
                                  : "flex items-end"
                            } `}
                          >
                            <div
                              className={` text-sm  mx-2 ${
                                isSession || isAdminSystem
                                  ? "max-w-full"
                                  : "max-w-[95%]"
                              }   ${isTrue ? "items-end" : "items-start"} `}
                            >
                              {textType ? (
                                <div className="flex items-center gap-x-2.5">
                                  {!isTrue &&
                                    (chatMode === "class" ||
                                      chatMode === "group") && (
                                      <div className="flex-none relative">
                                        {!isTrue && showPict ? (
                                          <Image
                                            src={
                                              picAuthor
                                                ? picAuthor
                                                : `/static/images/logokedi.svg`
                                            }
                                            onError={(event) => {
                                              event.target.srcset =
                                                "/static/images/temp_default.webp";
                                            }}
                                            alt="profilePict"
                                            width={40}
                                            height={40}
                                            className="object-cover w-10 h-10 rounded-full ring-1 ring-white "
                                          />
                                        ) : (
                                          <div className="w-10 h-10"></div>
                                        )}
                                      </div>
                                    )}
                                  <div
                                    className={`px-4 py-2 rounded-lg flex flex-col  flex-1 ${
                                      isTrue
                                        ? " bg-[#9d21e6] text-white rounded-br-none"
                                        : "bg-gray-100 text-gray-700 rounded-bl-none group-hover:ring-1 ring-gray-400"
                                    }  font-medium`}
                                  >
                                    <span
                                      style={{ color: nameColor }}
                                      className="font-semibold"
                                    >
                                      {!isTrue && showName && nameAuthor}
                                    </span>
                                    <p className="relative flex wrap items-end justify-between hyphens-auto break-word whitespace-pre-wrap">
                                      <Linkify
                                        options={{
                                          target: "_blank",
                                          className:
                                            "text-blue-600 font-medium hover:text-blue-700 duration-300 break-all",
                                        }}
                                      >
                                        {text}
                                      </Linkify>
                                      <span className="flex-none opacity-70 translate-x-2  text-xs">
                                        {convertTime(
                                          item.timetoken,
                                          false,
                                          true,
                                        )}
                                      </span>
                                    </p>
                                  </div>
                                </div>
                              ) : imgType ? (
                                <Dialog>
                                  <div className="flex items-end gap-x-2.5">
                                    {!isTrue &&
                                      (chatMode === "class" ||
                                        chatMode === "group") && (
                                        <div className="flex-none relative">
                                          {!isTrue && showPict ? (
                                            <Image
                                              src={
                                                picAuthor
                                                  ? picAuthor
                                                  : `/static/images/logokedi.svg`
                                              }
                                              onError={(event) => {
                                                event.target.srcset =
                                                  "/static/images/temp_default.webp";
                                              }}
                                              alt="profilePict"
                                              width={40}
                                              height={40}
                                              className="w-10 h-10 rounded-full ring-1 ring-white "
                                            />
                                          ) : (
                                            <div className="w-10 h-10"></div>
                                          )}
                                        </div>
                                      )}
                                    <DialogTrigger
                                      className="cursor-pointer"
                                      asChild
                                    >
                                      <div
                                        className={` ${
                                          !isTrue ? "bg-gray-100 p-1.5" : ""
                                        } flex-none relative rounded-lg inline-block  font-medium`}
                                      >
                                        {!isTrue && showName && (
                                          <span
                                            style={{ color: nameColor }}
                                            className="font-semibold ml-2.5"
                                          >
                                            {nameAuthor}
                                          </span>
                                        )}
                                        <Image
                                          width={700}
                                          height={475}
                                          sizes="100vw"
                                          style={{
                                            width: "100%",
                                            height: "auto",
                                          }}
                                          blurDataURL={
                                            item?.message?.pn_gcm?.data
                                              ?.custom_data?.uri
                                          }
                                          src={
                                            item?.message?.pn_gcm?.data
                                              ?.custom_data?.uri
                                          }
                                          onError={(event) => {
                                            event.target.srcset =
                                              "/static/images/temp_default.webp";
                                          }}
                                          alt=""
                                          className={` ${
                                            !isTrue ? "mt-1" : ""
                                          }  rounded-xl object-contain max-w-[150px] max-h-[300px]`}
                                        />

                                        <p
                                          className={`  ${
                                            item?.message?.pn_gcm?.data
                                              .custom_data?.uri
                                              ? "absolute bottom-0 "
                                              : ""
                                          } ${
                                            isTrue
                                              ? "text-white  right-2"
                                              : " text-white left-2 bottom-2"
                                          } text-xs w-fit p-2 font-medium opacity-70 bg-black rounded-lg`}
                                        >
                                          {convertTime(item.timetoken, true)}
                                        </p>
                                      </div>
                                    </DialogTrigger>
                                  </div>
                                  <DialogContent className="p-3 pt-10 w-fit max-w-4xl max-h-[90vh]">
                                    <Image
                                      sizes="85vh"
                                      width={100}
                                      height={100}
                                      style={{
                                        width: "auto",
                                        height: "auto",
                                      }}
                                      className="rounded-lg border max-h-[85vh]"
                                      src={
                                        item?.message?.pn_gcm?.data.custom_data
                                          ?.uri
                                      }
                                      alt={
                                        item?.message?.pn_gcm?.data.custom_data
                                          ?.uri
                                      }
                                      blurDataURL="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAAEAAAABCAYAAAAfFcSJAAAADUlEQVR42mOcq/isHgAFLAIlijmUjAAAAABJRU5ErkJggg=="
                                      placeholder="blur"
                                    />
                                  </DialogContent>
                                </Dialog>
                              ) : isSession ? (
                                <>
                                  <div className="h-[35px] w-full bg-purple-400 font-semibold rounded-lg flex items-center justify-center text-white  mb-2">
                                    {item.message.metadata.title}
                                  </div>
                                  <div className="relative w-full bg-purple-100  rounded-lg flex items-center justify-between px-3.5 py-4 gap-x-5">
                                    <div className=" font-normal">
                                      {lang.absensi}{" "}
                                      {item.message.metadata.title}
                                    </div>
                                    {attendanceClass &&
                                    attendanceClass.length != 0 ? (
                                      attendanceClass?.map(
                                        (d: any, index: any) => {
                                          const isDisabled = isEndTimePassed(
                                            d.endTime,
                                          );
                                          const tempPresent = d.userList.some(
                                            (user: any) =>
                                              user.userId ===
                                              sessionData?.user.id,
                                          );
                                          const hasPassed =
                                            tempPresent &&
                                            isStartTimeBeforeNow(d.startTime);
                                          return (
                                            <div
                                              key={index}
                                              className={` ${
                                                d.id ===
                                                item.message.metadata.id
                                                  ? ""
                                                  : "hidden"
                                              }  `}
                                            >
                                              <button
                                                disabled={isDisabled}
                                                onClick={() => {
                                                  if (tempPresent) {
                                                    toast.success(
                                                      lang.alreadyAttend +
                                                        " " +
                                                        item.message.metadata
                                                          .title,
                                                    );
                                                  } else {
                                                    handleAttend.mutate(
                                                      item.message.metadata.id,
                                                    );
                                                  }
                                                }}
                                                className={`${
                                                  tempPresent
                                                    ? "bg-blue-500 text-white"
                                                    : !hasPassed && !isDisabled
                                                      ? "bg-red-600 text-white"
                                                      : "bg-gray-300"
                                                } px-4 py-2 rounded-[10px]`}
                                              >
                                                {isLoadAttend ? (
                                                  <Threedots
                                                    width={30}
                                                    height={15}
                                                    strokeWidth={2}
                                                  />
                                                ) : tempPresent ? (
                                                  lang.present
                                                ) : !hasPassed && isDisabled ? (
                                                  lang.absent
                                                ) : (
                                                  lang.notYetPresent
                                                )}
                                              </button>
                                            </div>
                                          );
                                        },
                                      )
                                    ) : (
                                      <button
                                        disabled={true}
                                        className={` bg-gray-300 px-4 py-2 rounded-[10px] opacity-70`}
                                      >
                                        {lang.notYetPresent}
                                      </button>
                                    )}
                                  </div>
                                </>
                              ) : isClassSystem ? (
                                <>
                                  {isTrue && (
                                    <div
                                      className={`px-6 pt-2 pb-4 rounded-lg inline-block ring-2 ring-[#9d21e6] text-gray-700 rounded-br-none font-medium mb-1.5`}
                                    >
                                      <p className="text-lg text-[#9d21e6]  font-semibold  line-clamp-1 text-ellipsis">
                                        {item.message.metadata.title}
                                      </p>
                                      <p className=" text-sm font-normal mb-6 line-clamp-2 text-ellipsis">
                                        {item.message.metadata.description}
                                      </p>
                                      <button
                                        onClick={() => {
                                          if (chatCategory === "assignment") {
                                            getAssignment(
                                              item.message.metadata.id,
                                            );
                                            settempQuizId(
                                              item.message.metadata.id,
                                            );
                                          } else if (chatCategory === "quiz") {
                                            getQuiz(item.message.metadata.id);
                                            settempQuizId(
                                              item.message.metadata.id,
                                            );
                                          } else if (
                                            chatCategory === "material"
                                          ) {
                                            getMaterial(
                                              item.message.metadata.id,
                                            );
                                          } else if (chatCategory === "uts") {
                                            getExam(
                                              userChatAktif?.classId,
                                              false,
                                              "uts",
                                            );
                                            setTabExam("uts");
                                          } else {
                                            getExam(
                                              userChatAktif?.classId,
                                              false,
                                              "uas",
                                            );
                                            setTabExam("uas");
                                          }
                                        }}
                                        className={`py-2 px-10 m-auto rounded-xl text-white transition-all h-[30px] text-sm hover:bg-white hover:text-[#9d21e6] hover:ring-2 ring-[#9d21e6] duration-300 bg-[#9d21e6] shadow-[0px_10px_15px_rgba(157,33,230,0.25)] flex items-center justify-center`}
                                      >
                                        {lang.open}{" "}
                                        {chatCategory === "assignment"
                                          ? lang.assignment
                                          : chatCategory === "quiz"
                                            ? lang.quiz
                                            : chatCategory === "material"
                                              ? lang.material
                                              : chatCategory === "uts"
                                                ? "UTS"
                                                : "UAS"}
                                      </button>
                                    </div>
                                  )}
                                  {!isTrue && (
                                    <div className="flex items-end gap-x-2 mb-1.5">
                                      <div className="relative">
                                        {!isTrue &&
                                          (chatMode === "class" ||
                                            chatMode === "group") && (
                                            <div className="flex-none relative">
                                              {!isTrue && showPict ? (
                                                <Image
                                                  src={
                                                    picAuthor
                                                      ? picAuthor
                                                      : `/static/images/logokedi.svg`
                                                  }
                                                  onError={(event) => {
                                                    event.target.srcset =
                                                      "/static/images/temp_default.webp";
                                                  }}
                                                  alt="profilePict"
                                                  width={40}
                                                  height={40}
                                                  className="w-10 h-10 rounded-full ring-1 ring-white "
                                                />
                                              ) : (
                                                <div className="w-10 h-10"></div>
                                              )}
                                            </div>
                                          )}
                                      </div>
                                      <div
                                        className={`relative px-6 pt-2 pb-4 rounded-lg inline-block ring-2 ring-[#9d21e6] text-gray-700 rounded-bl-none font-medium`}
                                      >
                                        <p className="text-[#9d21e6]  font-semibold  line-clamp-1 text-ellipsis">
                                          {item.message.author?.firstName}
                                        </p>

                                        <p className="text-lg text-[#9d21e6]  font-semibold  line-clamp-1 text-ellipsis">
                                          {item.message.metadata.title}
                                        </p>
                                        <p className=" text-sm font-normal mb-6 line-clamp-2 text-ellipsis">
                                          {item.message.metadata.description}
                                        </p>
                                        <button
                                          onClick={() => {
                                            if (chatCategory === "assignment") {
                                              getAssignment(
                                                item.message.metadata.id,
                                              );
                                              settempQuizId(
                                                item.message.metadata.id,
                                              );
                                            } else if (
                                              chatCategory === "quiz"
                                            ) {
                                              getQuiz(item.message.metadata.id);
                                              settempQuizId(
                                                item.message.metadata.id,
                                              );
                                            } else if (
                                              chatCategory === "material"
                                            ) {
                                              getMaterial(
                                                item.message.metadata.id,
                                              );
                                            } else if (chatCategory === "uts") {
                                              getExam(
                                                userChatAktif?.classId,
                                                true,
                                                "uts",
                                              );
                                              setTabExam("uts");
                                            } else {
                                              getExam(
                                                userChatAktif?.classId,
                                                true,
                                                "uas",
                                              );
                                              setTabExam("uas");
                                            }
                                          }}
                                          className="mb-1.5 py-2 px-10 m-auto rounded-xl text-white transition-all h-[30px] text-sm hover:bg-white hover:text-[#9d21e6] hover:ring-2 ring-[#9d21e6] duration-300 bg-[#9d21e6] shadow-[0px_10px_15px_rgba(157,33,230,0.25)] flex items-center justify-center"
                                        >
                                          {lang.open}{" "}
                                          {chatCategory === "assignment"
                                            ? lang.assignment
                                            : chatCategory === "quiz"
                                              ? lang.quiz
                                              : chatCategory === "material"
                                                ? lang.material
                                                : chatCategory === "uts"
                                                  ? "UTS"
                                                  : "UAS"}
                                        </button>
                                        <span className="absolute right-5 bottom-0.5 flex-none opacity-70 translate-x-2  text-xs">
                                          {convertTime(
                                            item.timetoken,
                                            false,
                                            true,
                                          )}
                                        </span>
                                      </div>
                                    </div>
                                  )}
                                </>
                              ) : !isSession && isAdminSystem ? (
                                <div
                                  className={`text-xs flex items-center justify-center`}
                                >
                                  <p className="bg-[#eae9e9] px-2 py-1 rounded-lg">
                                    {item.message.metadata?.category !==
                                      "leave_member" && (
                                      <span className="text-[#9d21e6]">
                                        {item.message.author?.firstName ===
                                        sessionData?.user.fullName
                                          ? lang.you
                                          : item.message.author?.firstName}
                                      </span>
                                    )}

                                    {item.message.metadata?.category ===
                                      "created_zoom" && (
                                      <>
                                        <span className="">
                                          {" "}
                                          {lang.changeVideoConf}
                                        </span>{" "}
                                        <span className="text-[#11794e]">
                                          {lang.videoActivated}{" "}
                                        </span>
                                        <span className="flex-none opacity-70 translate-x-2  text-xs">
                                          {convertTime(
                                            item.timetoken,
                                            false,
                                            true,
                                          )}
                                        </span>
                                      </>
                                    )}
                                    {item.message.metadata?.category ===
                                      "removed_zoom" && (
                                      <>
                                        <span className="">
                                          {" "}
                                          {lang.changeVideoConf}
                                        </span>{" "}
                                        <span className="text-red-600">
                                          {lang.videoDeactivated}{" "}
                                        </span>
                                        <span className="flex-none opacity-70 translate-x-2  text-xs">
                                          {convertTime(
                                            item.timetoken,
                                            false,
                                            true,
                                          )}
                                        </span>
                                      </>
                                    )}
                                    {item.message.metadata?.category ===
                                      "created_group" && (
                                      <>
                                        <span className="">
                                          {" "}
                                          {lang.createGroupChat}
                                        </span>{" "}
                                        <span className="text-[#11794e]">
                                          {userChatAktif?.receiver.name}
                                        </span>
                                      </>
                                    )}
                                    {item.message.metadata?.category ===
                                      "add_member" && (
                                      <>
                                        <span className="">
                                          {" "}
                                          {lang.addedGroupChat}
                                        </span>{" "}
                                        <span className="text-[#11794e]">
                                          {item.message.metadata?.name ===
                                          sessionData?.user.fullName
                                            ? lang.you
                                            : item.message.metadata?.name}
                                        </span>
                                      </>
                                    )}
                                    {item.message.metadata?.category ===
                                      "removed_member" && (
                                      <>
                                        <span className="">
                                          {lang.kickedGroupChat}
                                        </span>{" "}
                                        <span className="text-[#11794e]">
                                          {item.message.metadata?.name ===
                                          sessionData?.user.fullName
                                            ? lang.you
                                            : item.message.metadata?.name}
                                        </span>
                                      </>
                                    )}
                                    {item.message.metadata?.category ===
                                      "leave_member" && (
                                      <>
                                        <span className="text-[#11794e]">
                                          {item.message.metadata?.name ===
                                          sessionData?.user.fullName
                                            ? lang.you
                                            : item.message.metadata?.name}
                                        </span>
                                        <span className="">
                                          {" "}
                                          {lang.leftGroupChat}
                                        </span>{" "}
                                      </>
                                    )}
                                  </p>
                                </div>
                              ) : (
                                <div className="flex items-end gap-x-2.5">
                                  {!isTrue &&
                                    (chatMode === "class" ||
                                      chatMode === "group") && (
                                      <div className="flex-none relative">
                                        {!isTrue && showPict ? (
                                          <Image
                                            src={
                                              picAuthor
                                                ? picAuthor
                                                : `/static/images/logokedi.svg`
                                            }
                                            onError={(event) => {
                                              event.target.srcset =
                                                "/static/images/temp_default.webp";
                                            }}
                                            alt="profilePict"
                                            width={40}
                                            height={40}
                                            className="w-10 h-10 rounded-full ring-1 ring-white "
                                          />
                                        ) : (
                                          <div className="w-10 h-10"></div>
                                        )}
                                      </div>
                                    )}
                                  <div
                                    className={`p-1.5 rounded-lg inline-block  ${
                                      isTrue
                                        ? " bg-[#9d21e6] text-white rounded-br-none"
                                        : "bg-gray-100 text-gray-700 rounded-bl-none  group-hover:ring-1 ring-gray-400"
                                    }  font-medium`}
                                  >
                                    {!isTrue && showName && (
                                      <span
                                        style={{ color: nameColor }}
                                        className="font-semibold ml-1"
                                      >
                                        {nameAuthor}
                                      </span>
                                    )}
                                    <div
                                      className={` ${
                                        isTrue
                                          ? "bg-white/20"
                                          : "mt-1 bg-[#9d21e6]/[.1]"
                                      } flex items-center  py-1.5 px-2.5 rounded-lg`}
                                    >
                                      <Link
                                        href={
                                          item?.message?.pn_gcm?.data
                                            .custom_data?.uri ?? ""
                                        }
                                        className={`${
                                          isTrue
                                            ? "text-white bg-white/[.3]"
                                            : "text-[#9d21e6] bg-[#9d21e6]/[.1]"
                                        } flex items-center justify-center  rounded-full h-[34px] w-[34px] mr-2.5`}
                                      >
                                        <svg
                                          stroke="currentColor"
                                          fill="currentColor"
                                          strokeWidth={0}
                                          viewBox="0 0 16 16"
                                          height={18}
                                          width={18}
                                          xmlns="http://www.w3.org/2000/svg"
                                        >
                                          <path d="M9.293 0H4a2 2 0 0 0-2 2v12a2 2 0 0 0 2 2h8a2 2 0 0 0 2-2V4.707A1 1 0 0 0 13.707 4L10 .293A1 1 0 0 0 9.293 0zM9.5 3.5v-2l3 3h-2a1 1 0 0 1-1-1zM4.5 9a.5.5 0 0 1 0-1h7a.5.5 0 0 1 0 1h-7zM4 10.5a.5.5 0 0 1 .5-.5h7a.5.5 0 0 1 0 1h-7a.5.5 0 0 1-.5-.5zm.5 2.5a.5.5 0 0 1 0-1h4a.5.5 0 0 1 0 1h-4z" />
                                        </svg>
                                      </Link>
                                      <div className="flex flex-col">
                                        <span
                                          className={`text-sm break-words max-w-[200px]`}
                                        >
                                          {fileName}
                                        </span>
                                        <span className={`text-xs opacity-70`}>
                                          {formatBytes(fileSize)}
                                        </span>
                                      </div>
                                    </div>
                                    <p
                                      className={` mt-1 text-right  text-xs w-full font-medium opacity-70`}
                                    >
                                      {convertTime(item.timetoken, false, true)}
                                    </p>
                                  </div>
                                </div>
                              )}
                            </div>
                          </div>
                        </ToggleGroupItem>
                      );
                    })}
                  </React.Fragment>
                ))}
              <TypingMessage />
            </ToggleGroup>
          </ContextMenuTrigger>
          {roleDelete && (
            <ContextMenuContent>
              <ContextMenuItem
                onClick={() => {
                  setMultiMessage([tempContext?.timetoken]);
                  setalert2(true);
                }}
                className="flex items-center gap-x-2.5"
              >
                <svg
                  xmlns="http://www.w3.org/2000/svg"
                  width="24px"
                  height="24px"
                  viewBox="0 0 24 24"
                >
                  <path
                    fill="currentColor"
                    d="M7 21q-.825 0-1.412-.587T5 19V6H4V4h5V3h6v1h5v2h-1v13q0 .825-.587 1.413T17 21zM17 6H7v13h10zM9 17h2V8H9zm4 0h2V8h-2zM7 6v13z"
                  ></path>
                </svg>
                <span>Delete</span>
              </ContextMenuItem>
              <ContextMenuItem
                onClick={() => {
                  handleCopyClick([tempContext?.text]);
                }}
                className="flex items-center gap-x-2.5"
              >
                <svg
                  xmlns="http://www.w3.org/2000/svg"
                  fill="none"
                  viewBox="0 0 24 24"
                  strokeWidth={1.5}
                  stroke="currentColor"
                  className="w-6 h-6"
                >
                  <path
                    strokeLinecap="round"
                    strokeLinejoin="round"
                    d="M15.75 17.25v3.375c0 .621-.504 1.125-1.125 1.125h-9.75a1.125 1.125 0 01-1.125-1.125V7.875c0-.621.504-1.125 1.125-1.125H6.75a9.06 9.06 0 011.5.124m7.5 10.376h3.375c.621 0 1.125-.504 1.125-1.125V11.25c0-4.46-3.243-8.161-7.5-8.876a9.06 9.06 0 00-1.5-.124H9.375c-.621 0-1.125.504-1.125 1.125v3.5m7.5 10.375H9.375a1.125 1.125 0 01-1.125-1.125v-9.25m12 6.625v-1.875a3.375 3.375 0 00-3.375-3.375h-1.5a1.125 1.125 0 01-1.125-1.125v-1.5a3.375 3.375 0 00-3.375-3.375H9.75"
                  />
                </svg>
                <span>Copy</span>
              </ContextMenuItem>
            </ContextMenuContent>
          )}
        </ContextMenu>
      )}

      <div
        onDragOver={handleDragOver}
        onDragEnter={handleDragEnter}
        onDragLeave={handleDragLeave}
        onDrop={handleDrop}
        className="border-x  pb-3 pt-2.5 px-2 border-t"
      >
        <div className="relative flex items-end gap-x-1.5 rounded-xl ">
          <Popover open={xpop} onOpenChange={setXpop}>
            <PopoverTrigger
              style={{ transition: "height 0.3s ease-in-out" }}
              className=" items-center p-2   text-purple-800 inline-flex items-center justify-center rounded-full h-10 w-10 transition duration-500 ease-in-out hover:hover:bg-purple-200 focus:outline-none cursor-pointer"
            >
              <svg
                xmlns="http://www.w3.org/2000/svg"
                fill="none"
                viewBox="0 0 24 24"
                strokeWidth={1.5}
                stroke="currentColor"
                className="w-6 h-6"
              >
                <path
                  strokeLinecap="round"
                  strokeLinejoin="round"
                  d="M12 10.5v6m3-3H9m4.06-7.19l-2.12-2.12a1.5 1.5 0 00-1.061-.44H4.5A2.25 2.25 0 002.25 6v12a2.25 2.25 0 002.25 2.25h15A2.25 2.25 0 0021.75 18V9a2.25 2.25 0 00-2.25-2.25h-5.379a1.5 1.5 0 01-1.06-.44z"
                />
              </svg>
            </PopoverTrigger>
            <PopoverContent className="translate-x-24 -translate-y-4 w-fit">
              <div className="grid grid-cols-3 gap-2.5 place-items-center text-center max-w-[250px] gap-y-5">
                {chatMode === "class" &&
                  userChatAktif.sender.id === userChatAktif.receiver.id && (
                    <>
                      <div
                        onClick={() => {
                          setModalType("Buat Materi");
                          setModalClass(true);
                          //lgx
                        }}
                        className="mx-3 cursor-pointer text-center w-fit flex items-center flex-col justify-center"
                      >
                        <div className="mb-1 flex h-[40px] min-w-[40px] items-center justify-center rounded-full bg-[#f2bb2d]">
                          <svg
                            stroke="currentColor"
                            fill="currentColor"
                            strokeWidth={0}
                            viewBox="0 0 1024 1024"
                            className="text-white"
                            height={21}
                            width={21}
                            xmlns="http://www.w3.org/2000/svg"
                          >
                            <path d="M854.6 288.6L639.4 73.4c-6-6-14.1-9.4-22.6-9.4H192c-17.7 0-32 14.3-32 32v832c0 17.7 14.3 32 32 32h640c17.7 0 32-14.3 32-32V311.3c0-8.5-3.4-16.7-9.4-22.7zM790.2 326H602V137.8L790.2 326zm1.8 562H232V136h302v216a42 42 0 0 0 42 42h216v494z" />
                          </svg>
                        </div>
                        <label className="text-neutral-70 text-xs">
                          Materi
                        </label>
                      </div>
                      <div
                        onClick={() => {
                          setModalType("Buat Tugas");
                          setModalClass(true);
                        }}
                        className="mx-3 cursor-pointer text-center w-fit flex items-center flex-col justify-center"
                      >
                        <div className="mb-1 flex h-[40px] min-w-[40px] items-center justify-center rounded-full bg-[#f2bb2d]">
                          <svg
                            stroke="currentColor"
                            fill="currentColor"
                            strokeWidth={0}
                            viewBox="0 0 24 24"
                            className="text-white"
                            height={21}
                            width={21}
                            xmlns="http://www.w3.org/2000/svg"
                          >
                            <path d="M21 6.75736L19 8.75736V4H10V9H5V20H19V17.2426L21 15.2426V21.0082C21 21.556 20.5551 22 20.0066 22H3.9934C3.44476 22 3 21.5501 3 20.9932V8L9.00319 2H19.9978C20.5513 2 21 2.45531 21 2.9918V6.75736ZM21.7782 8.80761L23.1924 10.2218L15.4142 18L13.9979 17.9979L14 16.5858L21.7782 8.80761Z" />
                          </svg>
                        </div>
                        <label className="text-neutral-70 text-xs">
                          {lang.assignment}
                        </label>
                      </div>
                      <div
                        onClick={() => {
                          setModalType("Buat Ujian");
                          setModalClass(true);
                        }}
                        className="mx-3 cursor-pointer text-center w-fit flex items-center flex-col justify-center"
                      >
                        <div className="mb-1 flex h-[40px] min-w-[40px] items-center justify-center rounded-full bg-[#f2bb2d] text-white">
                          <svg
                            xmlns="http://www.w3.org/2000/svg"
                            fill="none"
                            viewBox="0 0 24 24"
                            strokeWidth="1.5"
                            stroke="currentColor"
                            className="w-6 h-6"
                          >
                            <path
                              strokeLinecap="round"
                              strokeLinejoin="round"
                              d="M10.125 2.25h-4.5c-.621 0-1.125.504-1.125 1.125v17.25c0 .621.504 1.125 1.125 1.125h12.75c.621 0 1.125-.504 1.125-1.125v-9M10.125 2.25h.375a9 9 0 019 9v.375M10.125 2.25A3.375 3.375 0 0113.5 5.625v1.5c0 .621.504 1.125 1.125 1.125h1.5a3.375 3.375 0 013.375 3.375M9 15l2.25 2.25L15 12"
                            />
                          </svg>
                        </div>
                        <label className="text-neutral-70 text-xs">
                          {lang.exam}
                        </label>
                      </div>
                      <div
                        onClick={() => {
                          setModalType("Buat Kuis");
                          setModalClass(true);
                        }}
                        className="mx-3 cursor-pointer text-center w-fit flex items-center flex-col justify-center"
                      >
                        <div className="mb-1 flex h-[40px] min-w-[40px] items-center justify-center rounded-full bg-[#f2bb2d] text-white">
                          <svg
                            stroke="currentColor"
                            fill="currentColor"
                            strokeWidth={0}
                            viewBox="0 0 1024 1024"
                            className="text-white"
                            height={21}
                            width={21}
                            xmlns="http://www.w3.org/2000/svg"
                          >
                            <path d="M512 64C264.6 64 64 264.6 64 512s200.6 448 448 448 448-200.6 448-448S759.4 64 512 64zm0 820c-205.4 0-372-166.6-372-372s166.6-372 372-372 372 166.6 372 372-166.6 372-372 372z" />
                            <path d="M623.6 316.7C593.6 290.4 554 276 512 276s-81.6 14.5-111.6 40.7C369.2 344 352 380.7 352 420v7.6c0 4.4 3.6 8 8 8h48c4.4 0 8-3.6 8-8V420c0-44.1 43.1-80 96-80s96 35.9 96 80c0 31.1-22 59.6-56.1 72.7-21.2 8.1-39.2 22.3-52.1 40.9-13.1 19-19.9 41.8-19.9 64.9V620c0 4.4 3.6 8 8 8h48c4.4 0 8-3.6 8-8v-22.7a48.3 48.3 0 0 1 30.9-44.8c59-22.7 97.1-74.7 97.1-132.5.1-39.3-17.1-76-48.3-103.3zM472 732a40 40 0 1 0 80 0 40 40 0 1 0-80 0z" />
                          </svg>
                        </div>
                        <label className="text-neutral-70 text-xs">
                          {lang.quiz}
                        </label>
                      </div>
                    </>
                  )}
                <label
                  htmlFor="fileInput"
                  className="mx-3 cursor-pointer text-center w-fit flex items-center flex-col justify-center"
                >
                  <div className="mb-1 flex h-[40px] min-w-[40px] items-center justify-center rounded-full bg-[#f2bb2d] text-white">
                    <svg
                      xmlns="http://www.w3.org/2000/svg"
                      fill="none"
                      viewBox="0 0 24 24"
                      stroke="currentColor"
                      className="h-6 w-6 "
                    >
                      <path
                        strokeLinecap="round"
                        strokeLinejoin="round"
                        strokeWidth={2}
                        d="M15.172 7l-6.586 6.586a2 2 0 102.828 2.828l6.414-6.586a4 4 0 00-5.656-5.656l-6.415 6.585a6 6 0 108.486 8.486L20.5 13"
                      />
                    </svg>
                  </div>
                  <span className="text-neutral-70 text-xs">File</span>
                  <input
                    id="fileInput"
                    ref={inputFileRef}
                    type="file"
                    className="hidden fileInput"
                    onChange={handleFileChange}
                    // multiple
                  />
                </label>
                <label
                  htmlFor="fileInput"
                  className="mx-3 cursor-pointer text-center w-fit flex items-center flex-col justify-center"
                >
                  <div className="mb-1 flex h-[40px] min-w-[40px] items-center justify-center rounded-full bg-[#f2bb2d] text-white">
                    <svg
                      xmlns="http://www.w3.org/2000/svg"
                      fill="none"
                      viewBox="0 0 24 24"
                      strokeWidth={1.5}
                      stroke="currentColor"
                      className="w-6 h-6"
                    >
                      <path
                        strokeLinecap="round"
                        strokeLinejoin="round"
                        d="M2.25 15.75l5.159-5.159a2.25 2.25 0 013.182 0l5.159 5.159m-1.5-1.5l1.409-1.409a2.25 2.25 0 013.182 0l2.909 2.909m-18 3.75h16.5a1.5 1.5 0 001.5-1.5V6a1.5 1.5 0 00-1.5-1.5H3.75A1.5 1.5 0 002.25 6v12a1.5 1.5 0 001.5 1.5zm10.5-11.25h.008v.008h-.008V8.25zm.375 0a.375.375 0 11-.75 0 .375.375 0 01.75 0z"
                      />
                    </svg>
                  </div>
                  <span className="text-neutral-70 text-xs">{lang.photo}</span>
                  <input
                    id="fileInput"
                    ref={inputFileRef}
                    type="image"
                    className="hidden fileInput"
                    onChange={handleFileChange}
                    // multiple
                  />
                </label>
              </div>
            </PopoverContent>
          </Popover>

          {selectedFile?.file ? (
            <ul
              id="gallery"
              className="relative border shadow-md rounded-xl relative justify-center flex items-center flex-1 flex-wrap  p-1 shadow"
            >
              <li
                className={`block flex gap-x-5 items-center justify-between w-full p-1 ${
                  selectedFile.file?.type.startsWith("image/") ? "h-20" : "h-20"
                }`}
                id=""
              >
                <article
                  className={`relative group  hasImage h-full w-full rounded-md focus:outline-none focus:shadow-outline cursor-pointer relative  shadow-md ${
                    selectedFile.file?.type.startsWith("image/")
                      ? "text-transparent  hover:text-white"
                      : "text-black hover:text-purple-500"
                  } `}
                >
                  {isLoadingFile && (
                    <div className="flex items-center justify-center w-full h-full z-40 absolute top-0 bg-white/50 ">
                      <LoadingSpinner size={50} strokeWidth={2} />
                    </div>
                  )}
                  {selectedFile.file?.type.startsWith("image/") && (
                    <img
                      className="img-preview w-full h-full sticky object-cover rounded-md bg-fixed"
                      src={selectedFile.preview}
                      alt="Preview"
                    />
                  )}
                  <Dialog>
                    <DialogTrigger asChild>
                      <section
                        className={`${
                          selectedFile.file.type.startsWith("image/")
                            ? "group-hover:bg-black/50"
                            : ""
                        }  flex flex-col rounded-md text-xs break-words w-full h-full z-20 absolute top-0 py-2 px-3`}
                      >
                        <h1 className="flex-1 truncate  text-sm">
                          {selectedFile.file.name}
                        </h1>
                        <div className="flex">
                          <span className="p-1">
                            {selectedFile.file.type.startsWith("image/") ? (
                              <svg
                                className="fill-current w-4 h-4 ml-auto pt-"
                                xmlns="http://www.w3.org/2000/svg"
                                width={24}
                                height={24}
                                viewBox="0 0 24 24"
                              >
                                <path d="M5 8.5c0-.828.672-1.5 1.5-1.5s1.5.672 1.5 1.5c0 .829-.672 1.5-1.5 1.5s-1.5-.671-1.5-1.5zm9 .5l-2.519 4-2.481-1.96-4 5.96h14l-5-8zm8-4v14h-20v-14h20zm2-2h-24v18h24v-18z" />
                              </svg>
                            ) : (
                              <svg
                                className="fill-current w-4 h-4 ml-auto pt-"
                                stroke="currentColor"
                                fill="currentColor"
                                strokeWidth={0}
                                viewBox="0 0 16 16"
                                height={20}
                                width={20}
                                xmlns="http://www.w3.org/2000/svg"
                              >
                                <path d="M9.293 0H4a2 2 0 0 0-2 2v12a2 2 0 0 0 2 2h8a2 2 0 0 0 2-2V4.707A1 1 0 0 0 13.707 4L10 .293A1 1 0 0 0 9.293 0zM9.5 3.5v-2l3 3h-2a1 1 0 0 1-1-1zM4.5 9a.5.5 0 0 1 0-1h7a.5.5 0 0 1 0 1h-7zM4 10.5a.5.5 0 0 1 .5-.5h7a.5.5 0 0 1 0 1h-7a.5.5 0 0 1-.5-.5zm.5 2.5a.5.5 0 0 1 0-1h4a.5.5 0 0 1 0 1h-4z" />
                              </svg>
                            )}
                          </span>
                          <p className="p-1 size text-xs">
                            {formatBytes(selectedFile.file?.size)}
                          </p>
                          <button
                            onClick={handleDelete}
                            className={`delete ml-auto focus:outline-none p-1 rounded-md ${
                              selectedFile.file?.type.startsWith("image/")
                                ? "hover:bg-white/50"
                                : "hover:text-red-700"
                            }`}
                          >
                            <svg
                              className="pointer-events-none fill-current w-4 h-4 ml-auto"
                              xmlns="http://www.w3.org/2000/svg"
                              width={24}
                              height={24}
                              viewBox="0 0 24 24"
                            >
                              <path
                                className="pointer-events-none"
                                d="M3 6l3 18h12l3-18h-18zm19-4v2h-20v-2h5.711c.9 0 1.631-1.099 1.631-2h5.316c0 .901.73 2 1.631 2h5.711z"
                              />
                            </svg>
                          </button>
                        </div>
                      </section>
                    </DialogTrigger>
                    <DialogContent
                      className={`p-3  pt-12 sm:pt-16 pb-5  ${
                        selectedFile.file?.type.startsWith("image/")
                          ? "w-fit max-w-4xl"
                          : ""
                      } `}
                    >
                      {selectedFile.file?.type.startsWith("image/") ? (
                        <>
                          <div className="relative w-fit  mx-auto">
                            <Image
                              sizes="100vw"
                              width={100}
                              height={100}
                              style={{
                                width: "auto",
                                height: "auto",
                              }}
                              className="rounded-lg border"
                              src={selectedFile.preview}
                              alt="Upload Image"
                            />
                          </div>
                          <div className="flex flex-wrap gap-2.5 items-center justify-center">
                            <div className="flex items-center gap-x-2.5">
                              <svg
                                className="flex-none fill-current w-4 h-4 ml-auto pt-"
                                xmlns="http://www.w3.org/2000/svg"
                                width={24}
                                height={24}
                                viewBox="0 0 24 24"
                              >
                                <path d="M5 8.5c0-.828.672-1.5 1.5-1.5s1.5.672 1.5 1.5c0 .829-.672 1.5-1.5 1.5s-1.5-.671-1.5-1.5zm9 .5l-2.519 4-2.481-1.96-4 5.96h14l-5-8zm8-4v14h-20v-14h20zm2-2h-24v18h24v-18z" />
                              </svg>
                              <h1 className="text-sm">
                                {selectedFile.file.name}
                              </h1>
                            </div>
                            <div className="flex items-center gap-x-2.5">
                              <p className="p-1 size text-xs text-gray-600">
                                - {formatBytes(selectedFile.file.size)}
                              </p>
                              <button
                                onClick={handleDelete}
                                className={`delete ml-auto focus:outline-none p-1 rounded-md ${
                                  selectedFile.file.type.startsWith("image/")
                                    ? "hover:bg-white/50"
                                    : "hover:text-red-700"
                                }`}
                              >
                                <svg
                                  className="pointer-events-none fill-current w-4 h-4 ml-auto"
                                  xmlns="http://www.w3.org/2000/svg"
                                  width={24}
                                  height={24}
                                  viewBox="0 0 24 24"
                                >
                                  <path
                                    className="pointer-events-none"
                                    d="M3 6l3 18h12l3-18h-18zm19-4v2h-20v-2h5.711c.9 0 1.631-1.099 1.631-2h5.316c0 .901.73 2 1.631 2h5.711z"
                                  />
                                </svg>
                              </button>
                            </div>
                          </div>
                        </>
                      ) : (
                        <div className="flex flex-wrap gap-2.5 items-center justify-center">
                          <div className="flex items-center gap-x-2.5">
                            <svg
                              className="flex-none fill-current w-4 h-4 ml-auto pt-"
                              xmlns="http://www.w3.org/2000/svg"
                              width={24}
                              height={24}
                              viewBox="0 0 24 24"
                            >
                              <path d="M5 8.5c0-.828.672-1.5 1.5-1.5s1.5.672 1.5 1.5c0 .829-.672 1.5-1.5 1.5s-1.5-.671-1.5-1.5zm9 .5l-2.519 4-2.481-1.96-4 5.96h14l-5-8zm8-4v14h-20v-14h20zm2-2h-24v18h24v-18z" />
                            </svg>
                            <h1 className="text-center">
                              {selectedFile.file.name}
                            </h1>
                          </div>
                          <div className="flex items-center gap-x-2.5">
                            <p className="p-1 size text-xs text-gray-600">
                              - {formatBytes(selectedFile.file.size)}
                            </p>
                            <button
                              onClick={handleDelete}
                              className={`delete ml-auto focus:outline-none p-1 rounded-md ${
                                selectedFile.file.type.startsWith("image/")
                                  ? "hover:bg-white/50"
                                  : "hover:text-red-700"
                              }`}
                            >
                              <svg
                                className="pointer-events-none fill-current w-4 h-4 ml-auto"
                                xmlns="http://www.w3.org/2000/svg"
                                width={24}
                                height={24}
                                viewBox="0 0 24 24"
                              >
                                <path
                                  className="pointer-events-none"
                                  d="M3 6l3 18h12l3-18h-18zm19-4v2h-20v-2h5.711c.9 0 1.631-1.099 1.631-2h5.316c0 .901.73 2 1.631 2h5.711z"
                                />
                              </svg>
                            </button>
                          </div>
                        </div>
                      )}
                    </DialogContent>
                  </Dialog>
                </article>
              </li>
            </ul>
          ) : (
            <textarea
              placeholder={lang.inputMessage} //lgx
              className="w-full  resize-none focus:outline-none focus:placeholder-gray-400 text-gray-600 placeholder-gray-600  px-4  rounded-xl bg-purple-50 placeholder-gray-500  py-2 border shadow-md"
              ref={inputTextRef}
              rows={1}
              value={inputText}
              disabled={isSentMessage}
              onChange={handleTyping}
              onBlur={handleStoppedTyping}
            ></textarea>
          )}
          {selectedFile.file ? (
            <button
              disabled={isLoadingFile}
              onClick={(e) => {
                e.preventDefault();
                sendFile();
              }}
              type="button"
              className="disabled:opacity-50 border border-purple-500 inline-flex items-center justify-center rounded-full p-2 transition duration-500 ease-in-out text-[#9D21E6] hover:hover:bg-purple-200 focus:outline-none"
            >
              <svg
                xmlns="http://www.w3.org/2000/svg"
                viewBox="0 0 20 20"
                fill="currentColor"
                className="h-6 w-6  transform rotate-90"
              >
                <path d="M10.894 2.553a1 1 0 00-1.788 0l-7 14a1 1 0 001.169 1.409l5-1.429A1 1 0 009 15.571V11a1 1 0 112 0v4.571a1 1 0 00.725.962l5 1.428a1 1 0 001.17-1.408l-7-14z" />
              </svg>
            </button>
          ) : isSentMessage ? (
            <div className="flex items-center justify-center bg-white/30 z-10 p-1.5">
              <LoadingSpinner stroke={`#9D21E6`} size={30} />
            </div>
          ) : (
            <button
              onClick={(e) => {
                e.preventDefault();
                sendMessage(inputText);
              }}
              style={{ transition: "height 0.3s ease-in-out" }}
              type="button"
              className="inline-flex items-center justify-center rounded-full p-2 transition duration-500 ease-in-out text-[#9D21E6] hover:hover:bg-purple-200 focus:outline-none"
            >
              <svg
                xmlns="http://www.w3.org/2000/svg"
                viewBox="0 0 20 20"
                fill="currentColor"
                className="h-6 w-6  transform rotate-90"
              >
                <path d="M10.894 2.553a1 1 0 00-1.788 0l-7 14a1 1 0 001.169 1.409l5-1.429A1 1 0 009 15.571V11a1 1 0 112 0v4.571a1 1 0 00.725.962l5 1.428a1 1 0 001.17-1.408l-7-14z" />
              </svg>
            </button>
          )}
        </div>
      </div>
    </>
  );
});
