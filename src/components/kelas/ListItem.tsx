import Image from "next/image";

function formatTime(timestamp: any) {
  const parse = parseInt(timestamp);
  const waktu = parse / 1e4;
  const currentTime = new Date();
  const notificationTime = new Date(waktu);

  const timeDifference: any =
    currentTime.getTime() - notificationTime.getTime();
  const hoursDifference = Math.floor(timeDifference / (1000 * 60 * 60));

  if (hoursDifference < 24) {
    // Jika kurang dari 24 jam, tampilkan waktu
    const formattedTime = notificationTime.toLocaleTimeString("id-ID", {
      hour: "2-digit",
      minute: "2-digit",
    });
    return formattedTime;
  } else {
    // Jika lebih dari 24 jam, tampilkan tanggal
    const formattedDate = notificationTime.toLocaleDateString("id-ID", {
      day: "numeric",
      month: "short",
    });
    return formattedDate;
  }
}

export const ListItem = ({
  onClick,
  name,
  lecturerName,
  profilePictureLink,
  text,
  timetoken,
  created,
  isLecturer,
  isAdmin,
  channel,
  unread,
  userChatAktif,
}: any) => {
  return (
    <div
      onClick={onClick}
      className={` ${userChatAktif?.channel === channel ? "bg-[#d7b9fd]/50" : "bg-white"} relative  w-full flex items-center px-2.5 py-1.5 hover:bg-[#d7b9fd]/50 duration-300 shadow-md border-b border-gray-300 rounded-xl-purple-200 cursor-pointer rounded-lg mb-1.5  border`}
    >
      <div className="flex items-center space-x-4">
        <div className="relative w-14 h-14">
          <Image
            width={60}
            height={60}
            src={profilePictureLink || `/static/images/logokedi.svg`}
            className="absolute inset-0 h-full w-full object-cover rounded-xl shadow-md border border-gray-300"
            alt=""
          />
        </div>
      </div>
      <div className="flex-grow py-3 pl-3 pr-1 flex-1">
        <div className="flex justify-between items-center mb-2">
          {name && (
            <div className="line-clamp-1 font-semibold text-gray-700 text-sm ">
              {name}
            </div>
          )}
          <div className="ml-1 whitespace-nowrap text-xs [@media(max-width:320px)]:hidden">
            {timetoken
              ? formatTime(timetoken)
              : new Date(created).toLocaleDateString("id-ID", {
                  month: "short",
                  day: "2-digit",
                })}
          </div>
        </div>
        {lecturerName ? (
          <div className="flex -mt-1 justify-between items-center">
            <div className="font-medium text-gray-600 text-sm">
              {lecturerName}
            </div>
                <div className="relative flex items-center gap-x-0.5">
                    {unread !== 0 && (
                        <div
                            className="absolute -translate-x-7  font-medium w-fit  bg-purple-600 rounded-full px-2 py-0.5 text-center object-right-top text-white text-[13px] mr-1"
                        >
                            {unread}
                        </div>
                    )}
                    {isLecturer && (
                    <span>
                        <svg
                        xmlns="http://www.w3.org/2000/svg"
                        fill="none"
                        viewBox="0 0 24 24"
                        strokeWidth={2}
                        stroke="currentColor"
                        className="w-6 h-6"
                        >
                        <path
                            strokeLinecap="round"
                            strokeLinejoin="round"
                            d="M17.982 18.725A7.488 7.488 0 0 0 12 15.75a7.488 7.488 0 0 0-5.982 2.975m11.963 0a9 9 0 1 0-11.963 0m11.963 0A8.966 8.966 0 0 1 12 21a8.966 8.966 0 0 1-5.982-2.275M15 9.75a3 3 0 1 1-6 0 3 3 0 0 1 6 0Z"
                        />
                        </svg>
                    </span>
                    )}
                </div>
          </div>
        ) : (
          <div className="flex -mt-1 justify-between items-center">
            <div className="font-medium text-gray-600 text-sm">
              {text && !lecturerName && (
                <p className="text-sm text-gray-500 -mt-1 line-clamp-1">
                  {text}
                </p>
              )}
            </div>

                <div className="relative flex items-center gap-x-0.5">
                    {unread !== 0 && (
                        <div
                            className="absolute -translate-x-7  font-medium w-fit  bg-purple-600 rounded-full px-2 py-0.5 text-center object-right-top text-white text-[13px] mr-1"
                        >
                            {unread}
                        </div>
                    )}
                    {isAdmin && (
                    <span>
                        <svg
                        xmlns="http://www.w3.org/2000/svg"
                        fill="none"
                        viewBox="0 0 24 24"
                        strokeWidth={2}
                        stroke="currentColor"
                        className="w-6 h-6"
                        >
                        <path
                            strokeLinecap="round"
                            strokeLinejoin="round"
                            d="M17.982 18.725A7.488 7.488 0 0 0 12 15.75a7.488 7.488 0 0 0-5.982 2.975m11.963 0a9 9 0 1 0-11.963 0m11.963 0A8.966 8.966 0 0 1 12 21a8.966 8.966 0 0 1-5.982-2.275M15 9.75a3 3 0 1 1-6 0 3 3 0 0 1 6 0Z"
                        />
                        </svg>
                    </span>
                    )}
                </div>
          </div>
        )}
      </div>
    </div>
  );
};
