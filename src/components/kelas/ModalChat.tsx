import {
  Select,
  SelectContent,
  SelectItem,
  SelectTrigger,
  SelectValue,
} from "@/components/ui/select";
import { FileInput } from "@/components/kelas/module/element/FileInput";
import { LoadingSpinner } from "@/components/ui/loading";
import { InfoClass } from "@/components/kelas/module/menu/InfoClass";
import React from "react";
import {
  Dialog,
  DialogContent,
  DialogHeader,
  DialogTitle,
} from "@/components/ui/dialog";
import { useSession } from "next-auth/react";
import Image from "next/image";
import {
  formatTimeAndDate,
  formatTanggal,
  formatDate,
  formatToTime,
  compareTimes,
  isWithinTimeRange,
} from "@/utils/convertTime";
import { Tabs, TabsContent, TabsList, TabsTrigger } from "@/components/ui/tabs";
import { BeriNilai } from "@/components/kelas/module/menu/BeriNilai";
import Link from "next/link";
import { ShowDataPartial } from "@/components/kelas/module/menu/InfoQA";
import {
  Popover,
  PopoverContent,
  PopoverTrigger,
} from "@/components/ui/popover";

import Linkify from "linkify-react";
import toast from "react-hot-toast";
import { Button } from "@/components/ui/button";
import { Calendar as CalendarIcon } from "lucide-react";
import { Calendar } from "@/components/ui/calendar";
import { cn } from "@/lib/utils";
import { format } from "date-fns";
import { getTaskStatus } from "@/utils/statusDate";

function isEndTimePassed(endTime) {
  const currentTime = new Date();
  const endTimeDate = new Date(endTime);

  return currentTime > endTimeDate;
}

const ModalChat = React.memo((props) => {
  const { data: sessionData } = useSession();
  const sessionToken = sessionData?.user?.accessToken;
  const {
    lang,
    userChatAktif,
    handleDragOver,
    handleDragEnter,
    handleButtonClick,
    handleDragLeave,
    modalType,
    setModalType,
    getExam,
    dataUjianUts,
    dataUjianUas,
    subExamUts,
    subExamUas,
    dataInfoKelas,
    dataMember,
    getMaterial,
    allMaterial,
    allAssignment,
    allQuiz,
    material,
    getQuiz,
    getAssignment,
    singleSubAssign,
    singleSubQuiz,
    dataKuis,
    dataTugas,
    subQuiz,
    subAssignment,
    addFriend,
    modalClass,
    isLoadingModal,
    inputFileRef,
    setModalClass,

    handleAttend,
    attendanceClass,
    tabExam,
    edit,
    isSubmittedUts,
    isSubmittedUas,
    setInputQuiz,
    open,
    setOpen,
    handleDropClass,
    handleFileChangeClass,
    fileClass,
    setFileClass,
    tempDocId,
    setTempDocId,
    setCreateMaterial,
    createMaterial,
    postSubmission,
    inputQuiz,
    postGradeExam,
    setCreateExam,
    createExam,
    isCalendarOpen,
    setIsCalendarOpen,
    date,
    postExam,
    postGradeAssignment,
    urlConf,
    setUrlConf,
    urlConfSucces,
    actVideoConf,
    setAlertVideo,
    postMaterial,
    postAssignment,
    postQuiz,
    setCreateAssignment,
    createAssignment,
    setDate,
    tempQuizId,
    settempQuizId,
    postGradeQuiz,
    setEdit,
    setdelmode,
  } = props;
  return (
    <Dialog
      open={modalClass}
      onOpenChange={setModalClass}
      onOpenAutoFocus={(e) => e.preventDefault()}
    >
      {/* prettier-ignore */}
      {isLoadingModal || handleAttend.isLoading ? (
        <div className="absolute inset-0 flex items-center justify-center backdrop-blur-sm bg-white/20 z-10">
          <LoadingSpinner size={70} strokeWidth={2} />
        </div>
      ) : (
        <DialogContent
          className={`p-4 sm:p-6  
                ${modalType === "Kuis" || modalType === "Tugas" ? "max-w-[98%] sm:max-w-3xl" : "max-w-xl"} min-h-[200px]`}
        >
          <DialogHeader>
            <DialogTitle className="text-xl text-center">
              {modalType === "Video Conf" && lang.videoConf}
              {modalType === "Info Kelas" && lang.classInfo}
              {modalType === "Ujian" && lang.exam}
              {modalType === "Absensi" && lang.attendance}
              {modalType === "Member" && lang.members}
              {modalType === "Material" && lang.materialDetail}
              {modalType === "All Material" && lang.material}
              {modalType === "All Quiz" && lang.quiz}
              {modalType === "All Assignment" && lang.assignments}
              {modalType === "Buat Ujian" && edit !== ""
                ? lang.editExam
                : modalType === "Buat Ujian"
                  ? lang.createExam
                  : ""}
              {modalType === "Buat Materi" && edit !== ""
                ? lang.editMaterial
                : modalType === "Buat Materi"
                  ? lang.createMaterial
                  : ""}
              {modalType === "Buat Tugas" && lang.createAssignment}
              {modalType === "Buat Kuis" && lang.createQuiz}
              {modalType === "Detail Tugas" && lang.assignmentDetail}
              {modalType === "Detail Kuis" && lang.quizDetail}
            </DialogTitle>
          </DialogHeader>
          {/* {modalType === "Buat Materi" && ( */}
          {modalType === "Detail Tugas" && subAssignment && (
            <div className="overflow-y-auto min-h-[35vh] max-h-[80vh] mt-5">
              {subAssignment?.map((d) => {
                if (d.student.id === sessionData.user.id) {
                  return <ShowDataPartial key={d.id} {...d} />;
                }
              })}
            </div>
          )}

          {modalType === "Detail Kuis" && subQuiz && (
            <div className="overflow-y-auto min-h-[35vh] max-h-[80vh] mt-5">
              {subQuiz?.map((d) => {
                if (d.student.id === sessionData.user.id) {
                  return <ShowDataPartial key={d.id} {...d} />;
                }
              })}
            </div>
          )}
          {modalType === "Ujian" && (
            <>
              <Tabs defaultValue={tabExam} className="mt-1.5">
                <TabsList className="grid grid-cols-2 mb-2.5">
                  <TabsTrigger value="uts">UTS</TabsTrigger>
                  <TabsTrigger value="uas">UAS</TabsTrigger>
                </TabsList>
                <TabsContent
                  value="uts"
                  className="min-h-[70vh] max-h-[75vh] overflow-y-auto pr-2.5"
                >
                  {dataUjianUts ? (
                    <div className="w-full mb-4">
                      <div className="mb-3 pb-1.5  border-b-2 border-dashed">
                        <label className="text-sm">Session</label>
                        <p className="my-1 font-bold">
                          {dataUjianUts?.session.name}
                        </p>
                      </div>
                      <div className="mb-3 pb-1.5  border-b-2 border-dashed">
                        <label className="text-sm">{lang.examTitle}</label>
                        <p className="my-1 font-bold">{dataUjianUts?.name}</p>
                      </div>
                      <div className="mb-3 pb-1.5  border-b-2 border-dashed">
                        <label className="text-sm">{lang.examDesc}</label>
                        <p className="my-1 font-bold">
                          {dataUjianUts?.description}
                        </p>
                      </div>
                      <div className="mb-3 pb-1.5  border-b-2 border-dashed">
                        <label className="text-sm">Batas Waktu</label>
                        <p className="my-1 font-bold">
                          {formatDate(dataUjianUts?.endDateTime)}
                          {"  "}
                          {formatToTime(dataUjianUts?.endDateTime)}
                        </p>
                      </div>
                      {subExamUts?.map((d) => {
                        if (
                          isSubmittedUts &&
                          d.studentId === sessionData.user.id
                        ) {
                          return (
                            <div key={d.id}>
                              <div className="mt-2">
                                {d.attachments?.map((z: any) => (
                                  <div
                                    key={d.id}
                                    className="mb-3 pb-1.5  border-b-2 border-dashed"
                                  >
                                    <label className="text-sm">
                                      File Jawaban
                                    </label>
                                    <Link
                                      href={z.documentStorage.url}
                                      className="flex items-center gap-x-2.5 mb-1 mt-2 font-bold  hover:text-blue-600 duration-500"
                                      target="_blank"
                                    >
                                      <svg
                                        xmlns="http://www.w3.org/2000/svg"
                                        viewBox="0 0 20 20"
                                        fill="currentColor"
                                        className="w-5 h-5"
                                      >
                                        <path d="M3 3.5A1.5 1.5 0 014.5 2h6.879a1.5 1.5 0 011.06.44l4.122 4.12A1.5 1.5 0 0117 7.622V16.5a1.5 1.5 0 01-1.5 1.5h-11A1.5 1.5 0 013 16.5v-13z" />
                                      </svg>
                                      <span>
                                        {z.documentStorage.originalName}
                                      </span>
                                    </Link>
                                  </div>
                                ))}
                              </div>
                              <div className="mb-3 pb-1.5  border-b-2 border-dashed">
                                <label className="text-sm">Penilaian</label>
                                <p className="my-1 font-bold text-3xl text-[#9d21e6]">
                                  {d.grade ?? "Belum ada Penilaian"}
                                </p>
                              </div>
                              <div className="mb-3 pb-1.5  border-b-2 border-dashed">
                                <label className="text-sm">Komentar</label>
                                <p className="my-1 font-bold">
                                  {d.gradeComment ?? "Belum ada Komentar"}
                                </p>
                              </div>
                            </div>
                          );
                        }
                      })}
                      {!isSubmittedUts &&
                        dataUjianUts?.class.lecturer?.id !==
                          sessionData?.user.id && (
                          <div className="mt-2">
                            {dataUjianUts?.attachments?.map((z: any) => (
                              <div
                                key={dataUjianUts.id}
                                className="mb-3 pb-1.5  border-b-2 border-dashed"
                              >
                                <label className="text-sm">
                                  {lang.attachments} UTS
                                </label>
                                <Link
                                  href={z.documentStorage.url}
                                  className="flex items-center gap-x-2.5 mb-1 mt-2 font-bold  hover:text-blue-600 duration-500"
                                  target="_blank"
                                >
                                  <svg
                                    xmlns="http://www.w3.org/2000/svg"
                                    viewBox="0 0 20 20"
                                    fill="currentColor"
                                    className="w-5 h-5"
                                  >
                                    <path d="M3 3.5A1.5 1.5 0 014.5 2h6.879a1.5 1.5 0 011.06.44l4.122 4.12A1.5 1.5 0 0117 7.622V16.5a1.5 1.5 0 01-1.5 1.5h-11A1.5 1.5 0 013 16.5v-13z" />
                                  </svg>
                                  <span>{z.documentStorage.originalName}</span>
                                </Link>
                              </div>
                            ))}
                          </div>
                        )}

                      {dataUjianUts?.class.lecturer?.id ===
                      sessionData?.user.id ? (
                        <>
                          <div className="flex items-center gap-x-2 justify-between">
                            <label className="sm:text-lg font-semibold">
                              {lang.submissionUts}
                            </label>
                            <div className="flex items-center">
                              <p className="text-neutral-90 text-base pr-6">
                                {dataUjianUts?.totalSubmissionSubmitted}/
                                {dataUjianUts?.totalStudent - 1}
                              </p>
                              <button
                                onClick={() => {
                                  const isStudent =
                                    userChatAktif?.receiver.id !==
                                    sessionData?.user.uuid;
                                  getExam(
                                    userChatAktif?.classId,
                                    isStudent,
                                    "uts",
                                  );
                                  {
                                    /*                              getExam(userChatAktif?.classId, isStudent, "uts");*/
                                  }
                                }}
                                className="text-white p-2 rounded-[10px] bg-[#f2bb2d]"
                              >
                                <svg
                                  xmlns="http://www.w3.org/2000/svg"
                                  fill="none"
                                  viewBox="0 0 24 24"
                                  strokeWidth={1.5}
                                  stroke="currentColor"
                                  className="w-6 h-6"
                                >
                                  <path
                                    strokeLinecap="round"
                                    strokeLinejoin="round"
                                    d="M16.023 9.348h4.992v-.001M2.985 19.644v-4.992m0 0h4.992m-4.993 0l3.181 3.183a8.25 8.25 0 0013.803-3.7M4.031 9.865a8.25 8.25 0 0113.803-3.7l3.181 3.182m0-4.991v4.99"
                                  />
                                </svg>
                              </button>
                            </div>
                          </div>
                          <div className="mt-4">
                            {subExamUts?.map((x: any) => (
                              <div
                                key={x.id}
                                className="p-4 border border-gray-900 rounded-lg flex gap-x-2.5 justify-between"
                              >
                                <div className="">
                                  <h1 className="text-lg font-semibold">
                                    {x.studentName}
                                  </h1>
                                  <div className="text-xs sm:text-sm text-gray-500 mt-2">
                                    {x.attachments?.map((z: any) => (
                                      <div key={z.id}>
                                        <p className="sm:text-normal text-[13px] hyphens-auto break-words max-w-[140px] sm:max-w-full truncate">
                                          {z.documentStorage.originalName}
                                        </p>
                                        <div className="flex items-center  mt-3 text-gray-500 mb-8">
                                          <svg
                                            xmlns="http://www.w3.org/2000/svg"
                                            fill="none"
                                            viewBox="0 0 24 24"
                                            strokeWidth={2.5}
                                            stroke="currentColor"
                                            className="w-[18px] h-[18px]"
                                          >
                                            <path
                                              strokeLinecap="round"
                                              strokeLinejoin="round"
                                              d="M3 16.5v2.25A2.25 2.25 0 005.25 21h13.5A2.25 2.25 0 0021 18.75V16.5m-13.5-9L12 3m0 0l4.5 4.5M12 3v13.5"
                                            />
                                          </svg>
                                          <label className="mx-2">
                                            {
                                              formatTimeAndDate(
                                                z.documentStorage.createdAt,
                                              ).date
                                            }
                                          </label>
                                          {" | "}
                                          <label className="mx-2">
                                            {
                                              formatTimeAndDate(
                                                z.documentStorage.createdAt,
                                              ).time
                                            }
                                          </label>
                                        </div>
                                        <div className="flex flex-col justify-between">
                                          <div className="flex  gap-x-3">
                                            <Link
                                              href={
                                                z.documentStorage.url ?? "#"
                                              }
                                              target="_blank"
                                              className=" text-[#9D21E6] bg-[#d7b9fb] p-2 rounded-md"
                                            >
                                              <svg
                                                xmlns="http://www.w3.org/2000/svg"
                                                fill="none"
                                                viewBox="0 0 24 24"
                                                strokeWidth={1.5}
                                                stroke="currentColor"
                                                className="w-6 h-6"
                                              >
                                                <path
                                                  strokeLinecap="round"
                                                  strokeLinejoin="round"
                                                  d="M2.036 12.322a1.012 1.012 0 010-.639C3.423 7.51 7.36 4.5 12 4.5c4.638 0 8.573 3.007 9.963 7.178.07.207.07.431 0 .639C20.577 16.49 16.64 19.5 12 19.5c-4.638 0-8.573-3.007-9.963-7.178z"
                                                />
                                                <path
                                                  strokeLinecap="round"
                                                  strokeLinejoin="round"
                                                  d="M15 12a3 3 0 11-6 0 3 3 0 016 0z"
                                                />
                                              </svg>
                                            </Link>
                                            <Link
                                              href={
                                                z.documentStorage.url ?? "#"
                                              }
                                              target="_blank"
                                              className=" text-[#9D21E6] bg-[#d7b9fb] p-2 rounded-md"
                                            >
                                              <svg
                                                xmlns="http://www.w3.org/2000/svg"
                                                fill="none"
                                                viewBox="0 0 24 24"
                                                strokeWidth={1.5}
                                                stroke="currentColor"
                                                className="w-6 h-6"
                                              >
                                                <path
                                                  strokeLinecap="round"
                                                  strokeLinejoin="round"
                                                  d="M3 16.5v2.25A2.25 2.25 0 005.25 21h13.5A2.25 2.25 0 0021 18.75V16.5M16.5 12L12 16.5m0 0L7.5 12m4.5 4.5V3"
                                                />
                                              </svg>
                                            </Link>
                                          </div>
                                        </div>
                                      </div>
                                    ))}
                                  </div>
                                </div>
                                <div className="flex flex-col justify-between">
                                  <div className="w-full h-full flex sm:flex-row flex-col items-center justify-between sm:mr-4 text-[#9D21E6] bg-[#d7b9fb] rounded-xl px-6 sm:px-4 py-2 mb-8">
                                    <p className="text-base">{lang.grade}</p>
                                    <p className="text-3xl sm:text-5xl text-right ">
                                      {x.grade || "-"}
                                    </p>
                                  </div>
                                  <button
                                    className="sm:text-normal text-sm bg-[#9D21E6] text-white rounded-md px-6 py-2"
                                    onClick={() => {
                                      setInputQuiz({
                                        id: x.studentId ?? "",
                                        grade: x.grade ?? 0,
                                        comment: x.gradeComment ?? "",
                                      });
                                      setOpen(true);
                                    }}
                                  >
                                    {lang.grade}
                                  </button>
                                </div>
                              </div>
                            ))}
                            {dataUjianUts?.totalSubmissionSubmitted === 0 && (
                              <div className="w-full h-[10vh]  flex flex-col justify-center items-center">
                                <h1 className="text-lg">{lang.dataNotFound}</h1>
                              </div>
                            )}
                          </div>
                        </>
                      ) : (
                        <div>
                          {!isSubmittedUts && (
                            <div id="formSubmitKuis" className="">
                              <FileInput
                                handleDragOver={handleDragOver}
                                handleDragEnter={handleDragEnter}
                                handleDragLeave={handleDragLeave}
                                handleDropClass={handleDropClass}
                                handleButtonClick={handleButtonClick}
                                inputFileRef={inputFileRef}
                                handleFileChangeClass={handleFileChangeClass}
                                fileClass={fileClass}
                                setFileClass={setFileClass}
                                tempDocId={tempDocId}
                                setTempDocId={setTempDocId}
                                setCreateMaterial={setCreateMaterial}
                                title={"UTS"}
                              />
                              <div className="">
                                <label
                                  htmlFor="komentarNilaiKuis"
                                  className="block  font-medium text-gray-700"
                                >
                                  {lang.link}
                                </label>
                                <input
                                  autoComplete="off"
                                  id="description"
                                  className="w-full resize-none mx-1 px-3 py-2 text-sm mt-1 rounded-[10px] border border-gray-400  outline-none  shadow-sm focus:ring-2 ring-[#9d21e6]"
                                  onChange={(e) =>
                                    setCreateMaterial({
                                      ...createMaterial,
                                      description: e.target.value,
                                    })
                                  }
                                  value={createMaterial?.description}
                                  placeholder="htttp://example.com"
                                />
                              </div>
                              <div className="w-fit mx-auto mt-4">
                                <button
                                  onClick={() =>
                                    postSubmission.mutate({
                                      id: dataUjianUts?.id,
                                      description: createMaterial.description,
                                      type: "class-exam",
                                    })
                                  }
                                  className="px-8  py-2.5   bg-[#9d21e6] text-white shadow-xl rounded-[10px] font-semibold"
                                  type="button"
                                >
                                  {lang.submit} UTS
                                </button>
                              </div>
                            </div>
                          )}
                        </div>
                      )}
                    </div>
                  ) : (
                    <div className="flex items-center h-full w-full justify-center min-h-[70vh] text-center opacity-80">
                      {lang.dataNotFound}
                    </div>
                  )}
                </TabsContent>
                <TabsContent
                  value="uas"
                  className="min-h-[70vh] max-h-[75vh] overflow-y-auto pr-2.5"
                >
                  {dataUjianUas ? (
                    <div className="w-full mb-4">
                      <div className="mb-3 pb-1.5  border-b-2 border-dashed">
                        <label className="text-sm">{lang.session}</label>
                        <p className="my-1 font-bold">
                          {dataUjianUas?.session.name}
                        </p>
                      </div>
                      <div className="mb-3 pb-1.5  border-b-2 border-dashed">
                        <label className="text-sm">{lang.examTitle}</label>
                        <p className="my-1 font-bold">{dataUjianUas?.name}</p>
                      </div>
                      <div className="mb-3 pb-1.5  border-b-2 border-dashed">
                        <label className="text-sm">{lang.examDesc}</label>
                        <p className="my-1 font-bold">
                          {dataUjianUas?.description}
                        </p>
                      </div>
                      <div className="mb-3 pb-1.5  border-b-2 border-dashed">
                        <label className="text-sm">{lang.deadline}</label>
                        <p className="my-1 font-bold">
                          {formatDate(dataUjianUas?.endDateTime)}
                          {"  "}
                          {formatToTime(dataUjianUas?.endDateTime)}
                        </p>
                      </div>
                      {subExamUas?.map((d) => {
                        if (
                          isSubmittedUas &&
                          d.studentId === sessionData.user.id
                        ) {
                          return (
                            <div key={d.id}>
                              <div className="mt-2">
                                {d.attachments?.map((z: any) => (
                                  <div
                                    key={d.id}
                                    className="mb-3 pb-1.5  border-b-2 border-dashed"
                                  >
                                    <label className="text-sm">
                                      {lang.submissionFile}
                                    </label>
                                    <Link
                                      href={z.documentStorage.url}
                                      className="flex items-center gap-x-2.5 mb-1 mt-2 font-bold  hover:text-blue-600 duration-500"
                                      target="_blank"
                                    >
                                      <svg
                                        xmlns="http://www.w3.org/2000/svg"
                                        viewBox="0 0 20 20"
                                        fill="currentColor"
                                        className="w-5 h-5"
                                      >
                                        <path d="M3 3.5A1.5 1.5 0 014.5 2h6.879a1.5 1.5 0 011.06.44l4.122 4.12A1.5 1.5 0 0117 7.622V16.5a1.5 1.5 0 01-1.5 1.5h-11A1.5 1.5 0 013 16.5v-13z" />
                                      </svg>
                                      <span>
                                        {z.documentStorage.originalName}
                                      </span>
                                    </Link>
                                  </div>
                                ))}
                              </div>
                              <div className="mb-3 pb-1.5  border-b-2 border-dashed">
                                <label className="text-sm">Penilaian</label>
                                <p className="my-1 font-bold text-3xl text-[#9d21e6]">
                                  {d.grade ?? lang.noGrade}
                                </p>
                              </div>
                              <div className="mb-3 pb-1.5  border-b-2 border-dashed">
                                <label className="text-sm">
                                  {lang.comment}
                                </label>
                                <p className="my-1 font-bold">
                                  {d.gradeComment ?? lang.noComment}
                                </p>
                              </div>
                            </div>
                          );
                        }
                      })}
                      {!isSubmittedUas &&
                        dataUjianUas?.class.lecturer?.id !==
                          sessionData?.user.id && (
                          <div className="mt-2">
                            {dataUjianUas?.attachments?.map((z: any) => (
                              <div
                                key={dataUjianUas.id}
                                className="mb-3 pb-1.5  border-b-2 border-dashed"
                              >
                                <label className="text-sm">
                                  {lang.attachments} UAS
                                </label>
                                <Link
                                  href={z.documentStorage.url}
                                  className="flex items-center gap-x-2.5 mb-1 mt-2 font-bold  hover:text-blue-600 duration-500"
                                  target="_blank"
                                >
                                  <svg
                                    xmlns="http://www.w3.org/2000/svg"
                                    viewBox="0 0 20 20"
                                    fill="currentColor"
                                    className="w-5 h-5"
                                  >
                                    <path d="M3 3.5A1.5 1.5 0 014.5 2h6.879a1.5 1.5 0 011.06.44l4.122 4.12A1.5 1.5 0 0117 7.622V16.5a1.5 1.5 0 01-1.5 1.5h-11A1.5 1.5 0 013 16.5v-13z" />
                                  </svg>
                                  <span>{z.documentStorage.originalName}</span>
                                </Link>
                              </div>
                            ))}
                          </div>
                        )}

                      {dataUjianUas?.class.lecturer?.id ===
                      sessionData?.user.id ? (
                        <>
                          <div className="flex items-center gap-x-2 justify-between">
                            <label className="sm:text-lg font-semibold">
                              {lang.submissionUas}
                            </label>
                            <div className="flex items-center">
                              <p className="text-neutral-90 text-base pr-6">
                                {dataUjianUas?.totalSubmissionSubmitted}/
                                {dataUjianUas?.totalStudent - 1}
                              </p>
                              <button
                                onClick={() => {
                                  const isStudent =
                                    userChatAktif?.receiver.id !==
                                    sessionData?.user.uuid;
                                  getExam(
                                    userChatAktif?.classId,
                                    isStudent,
                                    "uas",
                                  );
                                  {
                                    /*                              getExam(userChatAktif?.classId, isStudent, "uts");*/
                                  }
                                }}
                                className="text-white p-2 rounded-[10px] bg-[#f2bb2d]"
                              >
                                <svg
                                  xmlns="http://www.w3.org/2000/svg"
                                  fill="none"
                                  viewBox="0 0 24 24"
                                  strokeWidth={1.5}
                                  stroke="currentColor"
                                  className="w-6 h-6"
                                >
                                  <path
                                    strokeLinecap="round"
                                    strokeLinejoin="round"
                                    d="M16.023 9.348h4.992v-.001M2.985 19.644v-4.992m0 0h4.992m-4.993 0l3.181 3.183a8.25 8.25 0 0013.803-3.7M4.031 9.865a8.25 8.25 0 0113.803-3.7l3.181 3.182m0-4.991v4.99"
                                  />
                                </svg>
                              </button>
                            </div>
                          </div>
                          <div className="mt-4">
                            {subExamUas.map((x: any) => (
                              <div
                                key={x.id}
                                className="p-4 border border-gray-900 rounded-lg flex gap-x-2.5 justify-between"
                              >
                                <div className="">
                                  <h1 className="text-lg font-semibold">
                                    {x.studentName}
                                  </h1>
                                  <div className="text-xs sm:text-sm text-gray-500 mt-2">
                                    {x.attachments?.map((z: any) => (
                                      <div key={z.id}>
                                        <p className="sm:text-normal text-[13px] hyphens-auto break-words max-w-[140px] sm:max-w-full truncate">
                                          {z.documentStorage.originalName}
                                        </p>
                                        <div className="flex items-center  mt-3 text-gray-500 mb-8">
                                          <svg
                                            xmlns="http://www.w3.org/2000/svg"
                                            fill="none"
                                            viewBox="0 0 24 24"
                                            strokeWidth={2.5}
                                            stroke="currentColor"
                                            className="w-[18px] h-[18px]"
                                          >
                                            <path
                                              strokeLinecap="round"
                                              strokeLinejoin="round"
                                              d="M3 16.5v2.25A2.25 2.25 0 005.25 21h13.5A2.25 2.25 0 0021 18.75V16.5m-13.5-9L12 3m0 0l4.5 4.5M12 3v13.5"
                                            />
                                          </svg>
                                          <label className="mx-2">
                                            {
                                              formatTimeAndDate(
                                                z.documentStorage.createdAt,
                                              ).date
                                            }
                                          </label>
                                          {" | "}
                                          <label className="mx-2">
                                            {
                                              formatTimeAndDate(
                                                z.documentStorage.createdAt,
                                              ).time
                                            }
                                          </label>
                                        </div>
                                        <div className="flex flex-col justify-between">
                                          <div className="flex  gap-x-3">
                                            <Link
                                              href={
                                                z.documentStorage.url ?? "#"
                                              }
                                              target="_blank"
                                              className=" text-[#9D21E6] bg-[#d7b9fb] p-2 rounded-md"
                                            >
                                              <svg
                                                xmlns="http://www.w3.org/2000/svg"
                                                fill="none"
                                                viewBox="0 0 24 24"
                                                strokeWidth={1.5}
                                                stroke="currentColor"
                                                className="w-6 h-6"
                                              >
                                                <path
                                                  strokeLinecap="round"
                                                  strokeLinejoin="round"
                                                  d="M2.036 12.322a1.012 1.012 0 010-.639C3.423 7.51 7.36 4.5 12 4.5c4.638 0 8.573 3.007 9.963 7.178.07.207.07.431 0 .639C20.577 16.49 16.64 19.5 12 19.5c-4.638 0-8.573-3.007-9.963-7.178z"
                                                />
                                                <path
                                                  strokeLinecap="round"
                                                  strokeLinejoin="round"
                                                  d="M15 12a3 3 0 11-6 0 3 3 0 016 0z"
                                                />
                                              </svg>
                                            </Link>
                                            <Link
                                              href={
                                                z.documentStorage.url ?? "#"
                                              }
                                              target="_blank"
                                              className=" text-[#9D21E6] bg-[#d7b9fb] p-2 rounded-md"
                                            >
                                              <svg
                                                xmlns="http://www.w3.org/2000/svg"
                                                fill="none"
                                                viewBox="0 0 24 24"
                                                strokeWidth={1.5}
                                                stroke="currentColor"
                                                className="w-6 h-6"
                                              >
                                                <path
                                                  strokeLinecap="round"
                                                  strokeLinejoin="round"
                                                  d="M3 16.5v2.25A2.25 2.25 0 005.25 21h13.5A2.25 2.25 0 0021 18.75V16.5M16.5 12L12 16.5m0 0L7.5 12m4.5 4.5V3"
                                                />
                                              </svg>
                                            </Link>
                                          </div>
                                        </div>
                                      </div>
                                    ))}
                                  </div>
                                </div>
                                <div className="flex flex-col justify-between">
                                  <div className="w-full h-full flex sm:flex-row flex-col items-center justify-between sm:mr-4 text-[#9D21E6] bg-[#d7b9fb] rounded-xl px-6 sm:px-4 py-2 mb-8">
                                    <p className="text-base">{lang.grade}</p>
                                    <p className="text-3xl sm:text-5xl text-right ">
                                      {x.grade || "-"}
                                    </p>
                                  </div>
                                  <button
                                    className="sm:text-normal text-sm bg-[#9D21E6] text-white rounded-md px-6 py-2"
                                    onClick={() => {
                                      setInputQuiz({
                                        id: x.studentId ?? "",
                                        grade: x.grade ?? 0,
                                        comment: x.gradeComment ?? "",
                                      });
                                      setOpen(true);
                                    }}
                                  >
                                    {lang.grade}
                                  </button>
                                </div>
                              </div>
                            ))}
                            {dataUjianUas?.totalSubmissionSubmitted === 0 && (
                              <div className="w-full h-[10vh]  flex flex-col justify-center items-center">
                                <h1 className="text-lg">{lang.dataNotFound}</h1>
                              </div>
                            )}
                          </div>
                        </>
                      ) : (
                        <div>
                          {!isSubmittedUas && (
                            <div id="formSubmitKuis" className="">
                              <FileInput
                                handleDragOver={handleDragOver}
                                handleDragEnter={handleDragEnter}
                                handleDragLeave={handleDragLeave}
                                handleDropClass={handleDropClass}
                                handleButtonClick={handleButtonClick}
                                inputFileRef={inputFileRef}
                                handleFileChangeClass={handleFileChangeClass}
                                fileClass={fileClass}
                                setFileClass={setFileClass}
                                tempDocId={tempDocId}
                                setTempDocId={setTempDocId}
                                setCreateMaterial={setCreateMaterial}
                                title={"UAS"}
                              />
                              <div className="">
                                <label
                                  htmlFor="komentarNilaiKuis"
                                  className="block  font-medium text-gray-700"
                                >
                                  {lang.link}
                                </label>
                                <input
                                  autoComplete="off"
                                  id="description"
                                  className="w-full resize-none mx-1 px-3 py-2 text-sm mt-1 rounded-[10px] border border-gray-400  outline-none  shadow-sm focus:ring-2 ring-[#9d21e6]"
                                  onChange={(e) =>
                                    setCreateMaterial({
                                      ...createMaterial,
                                      description: e.target.value,
                                    })
                                  }
                                  value={createMaterial?.description}
                                  placeholder="htttp://example.com"
                                />
                              </div>
                              <div className="w-fit mx-auto mt-4">
                                <button
                                  onClick={() =>
                                    postSubmission.mutate({
                                      id: dataUjianUas?.id,
                                      description: createMaterial.description,
                                      type: "class-exam",
                                    })
                                  }
                                  className="px-8  py-2.5   bg-[#9d21e6] text-white shadow-xl rounded-[10px] font-semibold"
                                  type="button"
                                >
                                  {lang.submit} UAS
                                </button>
                              </div>
                            </div>
                          )}
                        </div>
                      )}
                    </div>
                  ) : (
                    <div className="flex items-center h-full w-full justify-center min-h-[70vh] text-center opacity-80">
                      {lang.dataNotFound}
                    </div>
                  )}
                </TabsContent>
              </Tabs>
              {open && (
                <BeriNilai
                  open={open}
                  setOpen={setOpen}
                  setInputQuiz={setInputQuiz}
                  inputQuiz={inputQuiz}
                  postGradeExam={postGradeExam}
                  dataUjianUts={dataUjianUts}
                  dataUjianUas={dataUjianUas}
                />
              )}
            </>
          )}
          {modalType === "Buat Ujian" && (
            <>
              <Tabs defaultValue="uts">
                <TabsList className="grid grid-cols-2 mb-2.5">
                  <TabsTrigger value="uts">UTS</TabsTrigger>
                  <TabsTrigger value="uas">UAS</TabsTrigger>
                </TabsList>
                <TabsContent
                  value="uts"
                  className="max-h-[75vh] overflow-y-auto px-4 pb-4"
                >
                  <div className="mb-3">
                    <label
                      htmlFor="title"
                      className="block  font-medium text-gray-700"
                    >
                      Session
                    </label>
                    <Select
                      onValueChange={(e) =>
                        setCreateExam({
                          ...createExam,
                          sessionId: e,
                        })
                      }
                    >
                      <SelectTrigger className=" p-3 mb-3 mt-1 w-full rounded-[10px] border border-gray-400  outline-none  shadow-sm focus:ring-2 ring-[#9d21e6]">
                        <SelectValue placeholder="Pilih Sesi" />
                      </SelectTrigger>
                      <SelectContent>
                        {attendanceClass?.map((d) => {
                          return (
                            <SelectItem key={d.id} value={d.id}>
                              {d.name}
                            </SelectItem>
                          );
                        })}
                      </SelectContent>
                    </Select>
                  </div>
                  <div className="mb-3">
                    <label
                      htmlFor="title"
                      className="block  font-medium text-gray-700"
                    >
                      {lang.examTitle}
                    </label>

                    <input
                      type="text"
                      id="title"
                      placeholder={lang.examTitle}
                      onChange={(e) =>
                        setCreateExam({
                          ...createExam,
                          name: e.target.value,
                        })
                      }
                      value={createExam?.name}
                      className="h-11 text-sm px-3 py-2 h-11 mt-1 w-full rounded-[10px] border border-gray-400  outline-none  shadow-sm focus:ring-2 ring-[#9d21e6]"
                    />
                  </div>
                  <div className="">
                    <label
                      htmlFor="komentarNilaiKuis"
                      className="block  font-medium text-gray-700"
                    >
                      {lang.examDesc}
                    </label>
                    <textarea
                      id="description"
                      className="w-full resize-none px-3 py-2 text-sm mt-1 rounded-[10px] border border-gray-400  outline-none  shadow-sm focus:ring-2 ring-[#9d21e6]"
                      rows={4}
                      onChange={(e) =>
                        setCreateExam({
                          ...createExam,
                          description: e.target.value,
                        })
                      }
                      value={createExam?.description}
                      placeholder={lang.examDesc}
                    ></textarea>
                    <div className="mt-3 mb-3">
                      <div className="mb-1 flex items-center w-full justify-between  gap-5">
                        <label
                          htmlFor="UserEmail"
                          className="block  font-medium text-gray-700"
                        >
                          {lang.deadline}
                        </label>
                      </div>
                      <Popover
                        open={isCalendarOpen.createUts}
                        onOpenChange={() =>
                          setIsCalendarOpen((prevState) => ({
                            ...prevState,
                            createUts: !prevState.createUts,
                          }))
                        }
                      >
                        <PopoverTrigger asChild>
                          <Button
                            variant={"outline"}
                            className={cn(
                              "w-full justify-start text-left font-normal rounded-[10px] h-[50px] border-gray-400 text-[16px]",
                              !date && "text-muted-foreground",
                            )}
                          >
                            <CalendarIcon className="mr-3 h-4 w-4" />
                            {createExam?.dueDate ? (
                              format(createExam?.dueDate, "dd MMMM yyyy")
                            ) : (
                              <span>{lang.pickDate}</span>
                            )}
                          </Button>
                        </PopoverTrigger>
                        <PopoverContent className="w-auto p-0">
                          <Calendar
                            mode="single"
                            selected={date}
                            onSelect={(selectedDate) => {
                              setCreateExam({
                                ...createExam,
                                dueDate: selectedDate,
                              });
                              setIsCalendarOpen((prevState) => ({
                                ...prevState,
                                createUts: !prevState.createUts,
                              }));
                            }}
                            initialFocus
                          />
                        </PopoverContent>
                      </Popover>
                    </div>
                  </div>
                  <FileInput
                    handleDragOver={handleDragOver}
                    handleDragEnter={handleDragEnter}
                    handleDragLeave={handleDragLeave}
                    handleDropClass={handleDropClass}
                    handleButtonClick={handleButtonClick}
                    inputFileRef={inputFileRef}
                    handleFileChangeClass={handleFileChangeClass}
                    fileClass={fileClass}
                    setFileClass={setFileClass}
                    tempDocId={tempDocId}
                    setTempDocId={setTempDocId}
                    setCreateMaterial={setCreateMaterial}
                    title={"Ujian"}
                  />
                  <div className="w-fit mx-auto">
                    <button
                      onClick={() =>
                        postExam.mutate({
                          classId: userChatAktif?.classId,
                          sessionId: parseInt(createExam?.sessionId),
                          name: createExam.name,
                          description: createExam.description,
                          dueDate: createExam?.dueDate
                            ? createExam?.dueDate?.toISOString()
                            : null,
                          type: "uts",
                        })
                      }
                      className="px-8  py-2.5   bg-[#9d21e6] text-white shadow-xl rounded-[10px] font-semibold"
                      type="button"
                    >
                      {lang.submit} UTS
                    </button>
                  </div>
                </TabsContent>
                <TabsContent
                  value="uas"
                  className="max-h-[75vh] overflow-y-auto px-4 pb-4"
                >
                  <div className="mb-3">
                    <label
                      htmlFor="title"
                      className="block  font-medium text-gray-700"
                    >
                      {lang.session}
                    </label>
                    <Select
                      onValueChange={(e) =>
                        setCreateExam({
                          ...createExam,
                          sessionId: e,
                        })
                      }
                    >
                      <SelectTrigger className=" p-3 mb-3 mt-1 w-full rounded-[10px] border border-gray-400  outline-none  shadow-sm focus:ring-2 ring-[#9d21e6]">
                        <SelectValue placeholder="Pilih Sesi" />
                      </SelectTrigger>
                      <SelectContent>
                        {attendanceClass?.map((d) => {
                          return (
                            <SelectItem key={d.id} value={d.id}>
                              {d.name}
                            </SelectItem>
                          );
                        })}
                      </SelectContent>
                    </Select>
                  </div>
                  <div className="mb-3">
                    <label
                      htmlFor="title"
                      className="block  font-medium text-gray-700"
                    >
                      {lang.examTitle}
                    </label>

                    <input
                      type="text"
                      id="title"
                      placeholder={lang.examTitle}
                      onChange={(e) =>
                        setCreateExam({
                          ...createExam,
                          name: e.target.value,
                        })
                      }
                      value={createExam?.name}
                      className="h-11 text-sm px-3 py-2 h-11 mt-1 w-full rounded-[10px] border border-gray-400  outline-none  shadow-sm focus:ring-2 ring-[#9d21e6]"
                    />
                  </div>
                  <div className="">
                    <label
                      htmlFor="komentarNilaiKuis"
                      className="block  font-medium text-gray-700"
                    >
                      {lang.examDesc}
                    </label>
                    <textarea
                      id="description"
                      className="w-full resize-none px-3 py-2 text-sm mt-1 rounded-[10px] border border-gray-400  outline-none  shadow-sm focus:ring-2 ring-[#9d21e6]"
                      rows={4}
                      onChange={(e) =>
                        setCreateExam({
                          ...createExam,
                          description: e.target.value,
                        })
                      }
                      value={createExam?.description}
                      placeholder={lang.examDesc}
                    ></textarea>
                    <div className="mt-3 mb-3">
                      <div className="mb-1 flex items-center w-full justify-between  gap-5">
                        <label
                          htmlFor="UserEmail"
                          className="block  font-medium text-gray-700"
                        >
                          {lang.deadline}
                        </label>
                      </div>
                      <Popover
                        open={isCalendarOpen.createUas}
                        onOpenChange={() =>
                          setIsCalendarOpen((prevState) => ({
                            ...prevState,
                            createUas: !prevState.createUas,
                          }))
                        }
                      >
                        <PopoverTrigger asChild>
                          <Button
                            variant={"outline"}
                            className={cn(
                              "w-full justify-start text-left font-normal rounded-[10px] h-[50px] border-gray-400 text-[16px]",
                              !date && "text-muted-foreground",
                            )}
                          >
                            <CalendarIcon className="mr-3 h-4 w-4" />
                            {createExam?.dueDate ? (
                              format(createExam?.dueDate, "dd MMMM yyyy")
                            ) : (
                              <span>{lang.pickDate}</span>
                            )}
                          </Button>
                        </PopoverTrigger>
                        <PopoverContent className="w-auto p-0">
                          <Calendar
                            mode="single"
                            selected={date}
                            onSelect={(selectedDate) => {
                              setCreateExam({
                                ...createExam,
                                dueDate: selectedDate,
                              });
                              setIsCalendarOpen((prevState) => ({
                                ...prevState,
                                createUas: !prevState.createUas,
                              }));
                            }}
                            initialFocus
                          />
                        </PopoverContent>
                      </Popover>
                    </div>
                  </div>
                  <FileInput
                    handleDragOver={handleDragOver}
                    handleDragEnter={handleDragEnter}
                    handleDragLeave={handleDragLeave}
                    handleDropClass={handleDropClass}
                    handleButtonClick={handleButtonClick}
                    inputFileRef={inputFileRef}
                    handleFileChangeClass={handleFileChangeClass}
                    fileClass={fileClass}
                    setFileClass={setFileClass}
                    tempDocId={tempDocId}
                    setTempDocId={setTempDocId}
                    setCreateMaterial={setCreateMaterial}
                    title={"Ujian"}
                  />
                  <div className="w-fit mx-auto">
                    <button
                      onClick={() =>
                        postExam.mutate({
                          classId: userChatAktif?.classId,
                          sessionId: parseInt(createExam?.sessionId),
                          name: createExam.name,
                          description: createExam.description,
                          dueDate: createExam?.dueDate
                            ? createExam?.dueDate?.toISOString()
                            : null,
                          type: "uas",
                        })
                      }
                      className="px-8  py-2.5   bg-[#9d21e6] text-white shadow-xl rounded-[10px] font-semibold"
                      type="button"
                    >
                      {lang.submit} UAS
                    </button>
                  </div>
                </TabsContent>
              </Tabs>
              <Popover open={open} onOpenChange={setOpen}>
                <PopoverTrigger></PopoverTrigger>
                <PopoverContent align="center" className=" sm:p-6 sm:w-96 ">
                  <h1 className="text-[#9D21E6] mb-3 text-center text-lg font-semibold">
                    {lang.giveagrade}
                  </h1>
                  <div className="mb-5">
                    <label
                      htmlFor="nilaiKuis"
                      className="block  font-medium text-gray-700"
                    >
                      {lang.grade}
                    </label>

                    <input
                      type="number"
                      id="nilaiKuis"
                      placeholder="3"
                      onChange={(e) =>
                        setInputQuiz({ ...inputQuiz, grade: e.target.value })
                      }
                      value={inputQuiz?.grade}
                      className="px-3 py-2 h-11 text-sm mt-3 w-full rounded-[10px] border border-gray-400  outline-none  shadow-sm focus:ring-2 ring-[#9d21e6]"
                    />
                  </div>
                  <div className="mb-5 ">
                    <label
                      htmlFor="komentarNilaiKuis"
                      className="block  font-medium text-gray-700"
                    >
                      {lang.comment}
                    </label>
                    <textarea
                      id="komentarNilaiKuis"
                      className="w-full resize-none px-3 py-2 text-sm mt-3 rounded-[10px] border border-gray-400  outline-none  shadow-sm focus:ring-2 ring-[#9d21e6]"
                      rows={4}
                      onChange={(e) =>
                        setInputQuiz({ ...inputQuiz, comment: e.target.value })
                      }
                      value={inputQuiz?.comment}
                      placeholder="Isi komentar terkait Kuis"
                    ></textarea>
                  </div>
                  <div className="w-fit mx-auto">
                    <button
                      onClick={() =>
                        postGradeAssignment.mutate({
                          id: inputQuiz.id,
                          grade: inputQuiz.grade,
                          comment: inputQuiz.comment,
                        })
                      }
                      className=" p-2.5 w-32  bg-[#9d21e6] text-white shadow-xl rounded-[10px] font-semibold"
                      type="button"
                    >
                      {lang.submit}
                    </button>
                  </div>
                </PopoverContent>
              </Popover>
            </>
          )}
          {modalType === "Video Conf" && (
            <div className="">
              <div className="">
                <label
                  htmlFor="komentarNilaiKuis"
                  className="block  font-medium text-gray-700"
                >
                  {lang.linkConf}
                </label>
                <textarea
                  id="description"
                  className="w-full resize-none px-3 py-2 text-sm mt-1 rounded-[10px] border border-gray-400  outline-none  shadow-sm focus:ring-2 ring-[#9d21e6]"
                  rows={4}
                  autoFocus
                  onChange={(e) => setUrlConf(e.target.value)}
                  value={urlConf}
                  placeholder={lang.descIputConf}
                ></textarea>
              </div>
              <div
                className={` ${
                  userChatAktif?.videoUrl !== undefined || urlConfSucces !== ""
                    ? "grid grid-cols-2 gap-2.5 sm:grid-cols-3 items-center"
                    : "gap-2.5"
                }  text-sm w-fit mx-auto mt-4  `}
              >
                <button
                  onClick={() => {
                    if (urlConf.length < 1)
                      return toast.error(`Error: Harap isi url dahulu`);
                    actVideoConf.mutate({
                      classId: userChatAktif?.classId,
                      urlConf: urlConf,
                      type: "put",
                    });
                  }}
                  className="px-8  py-2.5   bg-[#9d21e6] text-white shadow-xl rounded-[10px] font-semibold"
                  type="button"
                >
                  {lang.submit}
                </button>
                {(userChatAktif?.videoUrl !== undefined ||
                  urlConfSucces !== "") && (
                  <button
                    onClick={() => setAlertVideo(true)}
                    className="px-8 py-2.5  bg-red-600 text-white shadow-xl rounded-[10px] font-semibold"
                    type="button"
                  >
                    {lang.delete} Url
                  </button>
                )}
                {(userChatAktif?.videoUrl !== undefined ||
                  urlConfSucces !== "") && (
                  <Link
                    href={userChatAktif?.videoUrl ?? ""}
                    target="_blank"
                    className="col-span-2 sm:col-span-1 px-8 py-2.5  bg-blue-600 text-white shadow-xl rounded-[10px] font-semibold"
                    type="button"
                  >
                    {lang.openConf}
                  </Link>
                )}
              </div>
            </div>
          )}
          {modalType === "Buat Materi" && (
            <div className="">
              <div className="mb-3">
                <label
                  htmlFor="title"
                  className="block  font-medium text-gray-700"
                >
                  {lang.materialTitle}
                </label>

                <input
                  type="text"
                  autoFocus
                  id="title"
                  placeholder="Judul Materi"
                  onChange={(e) =>
                    setCreateMaterial({
                      ...createMaterial,
                      title: e.target.value,
                    })
                  }
                  value={createMaterial?.title}
                  className="px-3 py-2 h-11 text-sm mt-1 w-full rounded-[10px] border border-gray-400  outline-none  shadow-sm focus:ring-2 ring-[#9d21e6]"
                />
              </div>
              <div className="">
                <label
                  htmlFor="komentarNilaiKuis"
                  className="block  font-medium text-gray-700"
                >
                  {lang.description}
                </label>
                <textarea
                  id="description"
                  className="w-full resize-none px-3 py-2 text-sm mt-1 rounded-[10px] border border-gray-400  outline-none  shadow-sm focus:ring-2 ring-[#9d21e6]"
                  rows={4}
                  autoFocus
                  onChange={(e) =>
                    setCreateMaterial({
                      ...createMaterial,
                      description: e.target.value,
                    })
                  }
                  value={createMaterial?.description}
                  placeholder={lang.description}
                ></textarea>
              </div>
              <FileInput
                handleDragOver={handleDragOver}
                handleDragEnter={handleDragEnter}
                handleDragLeave={handleDragLeave}
                handleDropClass={handleDropClass}
                handleButtonClick={handleButtonClick}
                inputFileRef={inputFileRef}
                handleFileChangeClass={handleFileChangeClass}
                fileClass={fileClass}
                setFileClass={setFileClass}
                tempDocId={tempDocId}
                setTempDocId={setTempDocId}
                setCreateMaterial={setCreateMaterial}
                title={lang.material}
              />
              <div className="w-fit mx-auto mt-4">
                <button
                  onClick={() => postMaterial.mutate(createMaterial)}
                  className="px-8  py-2.5   bg-[#9d21e6] text-white shadow-xl rounded-[10px] font-semibold"
                  type="button"
                >
                  {edit !== "" ? lang.materialEdit : lang.materialUpload}
                </button>
              </div>
            </div>
          )}
          {modalType === "Buat Tugas" && (
            <div>
              <div className="mb-3">
                <label
                  htmlFor="title"
                  className="block  font-medium text-gray-700"
                >
                  {lang.assignmenTitle}
                </label>

                <input
                  type="text"
                  id="title"
                  placeholder={lang.assignmenTitle}
                  onChange={(e) =>
                    setCreateAssignment({
                      ...createAssignment,
                      name: e.target.value,
                    })
                  }
                  value={createAssignment?.name}
                  className="px-3 py-2 h-11 text-sm mt-1 w-full rounded-[10px] border border-gray-400  outline-none  shadow-sm focus:ring-2 ring-[#9d21e6]"
                />
              </div>
              <div className="">
                <label
                  htmlFor="komentarNilaiKuis"
                  className="block  font-medium text-gray-700"
                >
                  {lang.description}
                </label>
                <textarea
                  id={lang.description}
                  className="w-full resize-none px-3 py-2 text-sm mt-1 rounded-[10px] border border-gray-400  outline-none  shadow-sm focus:ring-2 ring-[#9d21e6]"
                  rows={4}
                  onChange={(e) =>
                    setCreateAssignment({
                      ...createAssignment,
                      instruction: e.target.value,
                    })
                  }
                  value={createAssignment?.instruction}
                  placeholder="Isi Deskripsi terkait materi"
                ></textarea>
                <div className="mt-3 mb-3">
                  <div className="mb-1 flex items-center w-full justify-between  gap-5">
                    <label
                      htmlFor="UserEmail"
                      className="block  font-medium text-gray-700"
                    >
                      {lang.deadline}
                    </label>
                  </div>
                  <Popover
                    open={isCalendarOpen.createAssign}
                    onOpenChange={() =>
                      setIsCalendarOpen((prevState) => ({
                        ...prevState,
                        createAssign: !prevState.createAssign,
                      }))
                    }
                  >
                    <PopoverTrigger asChild>
                      <Button
                        variant={"outline"}
                        className={cn(
                          "w-full justify-start text-left font-normal rounded-[10px] h-[50px] border-gray-400 text-[16px]",
                          !date && "text-muted-foreground",
                        )}
                      >
                        <CalendarIcon className="mr-3 h-4 w-4" />
                        {date ? (
                          format(date, "dd MMMM yyyy")
                        ) : (
                          <span>{lang.pickDate}</span>
                        )}
                      </Button>
                    </PopoverTrigger>
                    <PopoverContent className="w-auto p-0">
                      <Calendar
                        mode="single"
                        selected={date}
                        onSelect={(selectedDate) => {
                          setDate(selectedDate);
                          setIsCalendarOpen((prevState) => ({
                            ...prevState,
                            createAssign: !prevState.createAssign,
                          }));
                        }}
                        initialFocus
                      />
                    </PopoverContent>
                  </Popover>
                </div>
              </div>
              <FileInput
                handleDragOver={handleDragOver}
                handleDragEnter={handleDragEnter}
                handleDragLeave={handleDragLeave}
                handleDropClass={handleDropClass}
                handleButtonClick={handleButtonClick}
                inputFileRef={inputFileRef}
                handleFileChangeClass={handleFileChangeClass}
                fileClass={fileClass}
                setFileClass={setFileClass}
                tempDocId={tempDocId}
                setTempDocId={setTempDocId}
                setCreateMaterial={setCreateMaterial}
                title={lang.assignment}
              />
              <div className="w-fit mx-auto">
                <button
                  onClick={() =>
                    postAssignment.mutate({
                      classId: userChatAktif?.classId,
                      name: createAssignment.name,
                      instruction: createAssignment.instruction,
                      startDate: new Date().toISOString(),
                      dueDate: date?.toISOString(),
                    })
                  }
                  className="px-8  py-2.5   bg-[#9d21e6] text-white shadow-xl rounded-[10px] font-semibold"
                  type="button"
                >
                  {lang.assignmentUpload}
                </button>
              </div>
            </div>
          )}
          {modalType === "Buat Kuis" && (
            <div>
              <div className="mb-3">
                <label
                  htmlFor="title"
                  className="block  font-medium text-gray-700"
                >
                  {lang.quizTitle}
                </label>

                <input
                  type="text"
                  id="title"
                  placeholder={lang.quizTitle}
                  onChange={(e) =>
                    setCreateAssignment({
                      ...createAssignment,
                      name: e.target.value,
                    })
                  }
                  value={createAssignment?.name}
                  className="px-3 py-2 h-11 text-sm mt-1 w-full rounded-[10px] border border-gray-400  outline-none  shadow-sm focus:ring-2 ring-[#9d21e6]"
                />
              </div>
              <div className="">
                <label
                  htmlFor="komentarNilaiKuis"
                  className="block  font-medium text-gray-700"
                >
                  Deskripsi
                </label>
                <textarea
                  id={lang.description}
                  className="w-full resize-none px-3 py-2 text-sm mt-1 rounded-[10px] border border-gray-400  outline-none  shadow-sm focus:ring-2 ring-[#9d21e6]"
                  rows={4}
                  onChange={(e) =>
                    setCreateAssignment({
                      ...createAssignment,
                      instruction: e.target.value,
                    })
                  }
                  value={createAssignment?.instruction}
                  placeholder="Isi Deskripsi terkait materi"
                ></textarea>
                <div className="mt-3 mb-3">
                  <div className="mb-1 flex items-center w-full justify-between  gap-5">
                    <label
                      htmlFor="UserEmail"
                      className="block  font-medium text-gray-700"
                    >
                      {lang.deadline}
                    </label>
                  </div>
                  <Popover
                    open={isCalendarOpen.createQuiz}
                    onOpenChange={() =>
                      setIsCalendarOpen((prevState) => ({
                        ...prevState,
                        createQuiz: !prevState.createQuiz,
                      }))
                    }
                  >
                    <PopoverTrigger asChild>
                      <Button
                        variant={"outline"}
                        className={cn(
                          "w-full justify-start text-left font-normal rounded-[10px] h-[50px] border-gray-400 text-[16px]",
                          !date && "text-muted-foreground",
                        )}
                      >
                        <CalendarIcon className="mr-3 h-4 w-4" />
                        {date ? (
                          format(date, "dd MMMM yyyy")
                        ) : (
                          <span>{lang.pickDate}</span>
                        )}
                      </Button>
                    </PopoverTrigger>
                    <PopoverContent className="w-auto p-0">
                      <Calendar
                        mode="single"
                        selected={date}
                        onSelect={(selectedDate) => {
                          setDate(selectedDate);
                          setIsCalendarOpen((prevState) => ({
                            ...prevState,
                            createQuiz: !prevState.createQuiz,
                          }));
                        }}
                        initialFocus
                      />
                    </PopoverContent>
                  </Popover>
                </div>
              </div>
              <FileInput
                handleDragOver={handleDragOver}
                handleDragEnter={handleDragEnter}
                handleDragLeave={handleDragLeave}
                handleDropClass={handleDropClass}
                handleButtonClick={handleButtonClick}
                inputFileRef={inputFileRef}
                handleFileChangeClass={handleFileChangeClass}
                fileClass={fileClass}
                setFileClass={setFileClass}
                tempDocId={tempDocId}
                setTempDocId={setTempDocId}
                setCreateMaterial={setCreateMaterial}
                title={lang.quiz}
              />
              <div className="w-fit mx-auto">
                <button
                  onClick={() =>
                    postQuiz.mutate({
                      classId: userChatAktif?.classId,
                      name: createAssignment.name,
                      instruction: createAssignment.instruction,
                      startDate: new Date().toISOString(),
                      dueDate: date?.toISOString(),
                    })
                  }
                  className="px-8  py-2.5   bg-[#9d21e6] text-white shadow-xl rounded-[10px] font-semibold"
                  type="button"
                >
                  {lang.quizUpload}
                </button>
              </div>
            </div>
          )}
          {modalType === "Kuis" && dataKuis && (
            <>
              <div className="w-full ">
                <h1 className="font-bold text-2xl ">{dataKuis?.name}</h1>
                <p className="mt-2.5 mb-6 text-lg">{dataKuis?.instruction}</p>
                {dataKuis &&
                  Object.keys(dataKuis?.references).map((key: any) => {
                    return (
                      <Link
                        key={key}
                        href={dataKuis.references[key].url}
                        target="_blank"
                      >
                        <div className="w-fit py-2 px-4 flex gap-x-2 my-2 cursor-pointer border border-gray-900 rounded-[10px]">
                          <svg
                            xmlns="http://www.w3.org/2000/svg"
                            fill="none"
                            viewBox="0 0 24 24"
                            stroke="currentColor"
                            className="h-6 w-6 flex-none"
                          >
                            <path
                              strokeLinecap="round"
                              strokeLinejoin="round"
                              strokeWidth={2}
                              d="M15.172 7l-6.586 6.586a2 2 0 102.828 2.828l6.414-6.586a4 4 0 00-5.656-5.656l-6.415 6.585a6 6 0 108.486 8.486L20.5 13"
                            />
                          </svg>
                          <label className="ml-2 text-[#9D21E6]">
                            {dataKuis?.references[key].originalName}
                          </label>
                        </div>
                      </Link>
                    );
                  })}
              </div>
              {dataKuis && dataKuis?.lecturer?.id === sessionData?.user.id ? (
                <div className="w-full mb-4">
                  <div className="flex items-center gap-x-2 justify-between">
                    <label className="sm:text-lg font-semibold">
                      {lang.answerQuiz}
                    </label>
                    <div className="flex items-center">
                      <p className="text-neutral-90 text-base pr-6">
                        {
                          subQuiz?.filter(
                            (quiz: any) =>
                              quiz.grade !== null && quiz.grade !== undefined,
                          ).length
                        }
                        /{subQuiz?.length}
                      </p>
                      <button
                        onClick={() => getQuiz(tempQuizId)}
                        className="text-white p-2 rounded-[10px] bg-[#f2bb2d]"
                      >
                        <svg
                          xmlns="http://www.w3.org/2000/svg"
                          fill="none"
                          viewBox="0 0 24 24"
                          strokeWidth={1.5}
                          stroke="currentColor"
                          className="w-6 h-6"
                        >
                          <path
                            strokeLinecap="round"
                            strokeLinejoin="round"
                            d="M16.023 9.348h4.992v-.001M2.985 19.644v-4.992m0 0h4.992m-4.993 0l3.181 3.183a8.25 8.25 0 0013.803-3.7M4.031 9.865a8.25 8.25 0 0113.803-3.7l3.181 3.182m0-4.991v4.99"
                          />
                        </svg>
                      </button>
                    </div>
                  </div>
                  <div className="mt-4">
                    {subQuiz?.map((x: any) => (
                      <div
                        key={x.id}
                        className="p-4 border border-gray-900 rounded-lg flex gap-x-2.5 justify-between"
                      >
                        <div className="">
                          <h1 className="text-lg font-semibold">
                            {x.student.fullName}
                          </h1>
                          <div className="text-xs sm:text-sm text-gray-500 mt-2">
                            {x.references.map((z: any) => (
                              <div key={z.id}>
                                <p className="sm:text-normal text-[13px] hyphens-auto break-words max-w-[140px] sm:max-w-full truncate">
                                  {z.name}
                                </p>
                                <div className="flex items-center  mt-3 text-gray-500 mb-8">
                                  <svg
                                    xmlns="http://www.w3.org/2000/svg"
                                    fill="none"
                                    viewBox="0 0 24 24"
                                    strokeWidth={2.5}
                                    stroke="currentColor"
                                    className="w-[18px] h-[18px]"
                                  >
                                    <path
                                      strokeLinecap="round"
                                      strokeLinejoin="round"
                                      d="M3 16.5v2.25A2.25 2.25 0 005.25 21h13.5A2.25 2.25 0 0021 18.75V16.5m-13.5-9L12 3m0 0l4.5 4.5M12 3v13.5"
                                    />
                                  </svg>
                                  <label className="mx-2">
                                    {formatTimeAndDate(z.createdAt).date}
                                  </label>
                                  {" | "}
                                  <label className="mx-2">
                                    {formatTimeAndDate(z.createdAt).time}
                                  </label>
                                </div>
                                <div className="flex flex-col justify-between">
                                  <div className="flex  gap-x-3">
                                    <Link
                                      href={z.url}
                                      target="_blank"
                                      className=" text-[#9D21E6] bg-[#d7b9fb] p-2 rounded-md"
                                    >
                                      <svg
                                        xmlns="http://www.w3.org/2000/svg"
                                        fill="none"
                                        viewBox="0 0 24 24"
                                        strokeWidth={1.5}
                                        stroke="currentColor"
                                        className="w-6 h-6"
                                      >
                                        <path
                                          strokeLinecap="round"
                                          strokeLinejoin="round"
                                          d="M2.036 12.322a1.012 1.012 0 010-.639C3.423 7.51 7.36 4.5 12 4.5c4.638 0 8.573 3.007 9.963 7.178.07.207.07.431 0 .639C20.577 16.49 16.64 19.5 12 19.5c-4.638 0-8.573-3.007-9.963-7.178z"
                                        />
                                        <path
                                          strokeLinecap="round"
                                          strokeLinejoin="round"
                                          d="M15 12a3 3 0 11-6 0 3 3 0 016 0z"
                                        />
                                      </svg>
                                    </Link>
                                    <Link
                                      href={z.url}
                                      target="_blank"
                                      className=" text-[#9D21E6] bg-[#d7b9fb] p-2 rounded-md"
                                    >
                                      <svg
                                        xmlns="http://www.w3.org/2000/svg"
                                        fill="none"
                                        viewBox="0 0 24 24"
                                        strokeWidth={1.5}
                                        stroke="currentColor"
                                        className="w-6 h-6"
                                      >
                                        <path
                                          strokeLinecap="round"
                                          strokeLinejoin="round"
                                          d="M3 16.5v2.25A2.25 2.25 0 005.25 21h13.5A2.25 2.25 0 0021 18.75V16.5M16.5 12L12 16.5m0 0L7.5 12m4.5 4.5V3"
                                        />
                                      </svg>
                                    </Link>
                                  </div>
                                </div>
                              </div>
                            ))}
                          </div>
                        </div>
                        <div className="flex flex-col justify-between">
                          <div className="w-full h-full flex sm:flex-row flex-col items-center justify-between sm:mr-4 text-[#9D21E6] bg-[#d7b9fb] rounded-xl px-6 sm:px-4 py-2 mb-8">
                            <p className="text-base">{lang.grade}</p>
                            <p className="text-3xl sm:text-5xl text-right ">
                              {x.grade || "-"}
                            </p>
                          </div>
                          <button
                            className="sm:text-normal text-sm bg-[#9D21E6] text-white rounded-md px-6 py-2"
                            onClick={() => {
                              setInputQuiz({
                                id: x.id ?? "",
                                grade: x.grade ?? 0,
                                comment: x.lecturerComment ?? "",
                              });
                              setOpen(true);
                            }}
                          >
                            {lang.grade}
                          </button>
                        </div>
                      </div>
                    ))}
                    {subQuiz?.length <= 0 && (
                      <div className="w-full h-[10vh]  flex flex-col justify-center items-center">
                        <h1 className="text-2xl">{lang.dataNotFound}</h1>
                      </div>
                    )}
                  </div>
                </div>
              ) : new Date() > new Date(dataKuis?.dueDate).getTime() ? (
                <div className="mb-3 pb-1.5 border-b-2 border-dashed">
                  <label className="text-sm">Batas Waktu</label>
                  <div className="my-1 font-bold flex items-center gap-x-3">
                    <span>{formatDate(dataKuis?.dueDate)} </span>
                    <span className="inline-flex items-center justify-center rounded-full bg-emerald-100 px-2.5 py-0.5 text-emerald-700">
                      <svg
                        xmlns="http://www.w3.org/2000/svg"
                        fill="none"
                        viewBox="0 0 24 24"
                        strokeWidth="1.5"
                        stroke="currentColor"
                        className="-ms-1 me-1.5 h-4 w-4"
                      >
                        <path
                          strokeLinecap="round"
                          strokeLinejoin="round"
                          d="M9 12.75L11.25 15 15 9.75M21 12a9 9 0 11-18 0 9 9 0 0118 0z"
                        />
                      </svg>

                      <p className="whitespace-nowrap text-sm">
                        {lang.finished}
                      </p>
                    </span>
                  </div>
                  <p className="mt-2 text-sm">{lang.notSubmit}</p>
                </div>
              ) : (
                <div id="formSubmitKuis" className="">
                  <div className="mb-3 pb-1.5 border-b-2 border-dashed">
                    <label className="text-sm">{lang.deadline}</label>
                    <p className="my-1 font-bold">
                      {formatDate(dataKuis?.dueDate)}
                    </p>
                  </div>
                  <div className="">
                    <label
                      htmlFor="komentarNilaiKuis"
                      className="block  font-medium text-gray-700"
                    >
                      {lang.description}
                    </label>
                    <textarea
                      id="description"
                      className="w-full resize-none px-3 py-2 text-sm mt-1 rounded-[10px] border border-gray-400  outline-none  shadow-sm focus:ring-2 ring-[#9d21e6]"
                      rows={4}
                      onChange={(e) =>
                        setCreateMaterial({
                          ...createMaterial,
                          description: e.target.value,
                        })
                      }
                      value={createMaterial?.description}
                      placeholder={lang.description}
                    ></textarea>
                  </div>
                  <FileInput
                    handleDragOver={handleDragOver}
                    handleDragEnter={handleDragEnter}
                    handleDragLeave={handleDragLeave}
                    handleDropClass={handleDropClass}
                    handleButtonClick={handleButtonClick}
                    inputFileRef={inputFileRef}
                    handleFileChangeClass={handleFileChangeClass}
                    fileClass={fileClass}
                    setFileClass={setFileClass}
                    tempDocId={tempDocId}
                    setTempDocId={setTempDocId}
                    setCreateMaterial={setCreateMaterial}
                    title={lang.quiz}
                  />
                  <div className="w-fit mx-auto mt-4">
                    <button
                      onClick={() =>
                        postSubmission.mutate({
                          id: dataKuis?.id,
                          description: createMaterial.description,
                          type: "class-quiz",
                        })
                      }
                      className="px-8  py-2.5   bg-[#9d21e6] text-white shadow-xl rounded-[10px] font-semibold"
                      type="button"
                    >
                      {lang.submit}
                    </button>
                  </div>
                </div>
              )}

              {open && (
                <BeriNilai
                  open={open}
                  setOpen={setOpen}
                  setInputQuiz={setInputQuiz}
                  inputQuiz={inputQuiz}
                  postGradeQuiz={postGradeQuiz}
                />
              )}
            </>
          )}
          {modalType === "Tugas" && dataTugas && (
            <>
              <div className="w-full ">
                <h1 className="font-bold text-2xl ">{dataTugas?.name}</h1>
                <p className="mt-2.5 mb-6 text-lg">{dataTugas?.instruction}</p>
                {dataTugas &&
                  Object.keys(dataTugas?.references).map((key: any) => {
                    return (
                      <Link
                        key={key}
                        href={dataTugas.references[key].url}
                        target="_blank"
                      >
                        <div className="w-fit py-2 px-4 flex gap-x-2 my-2 cursor-pointer border border-gray-900 rounded-[10px]">
                          <svg
                            xmlns="http://www.w3.org/2000/svg"
                            fill="none"
                            viewBox="0 0 24 24"
                            stroke="currentColor"
                            className="h-6 w-6 flex-none"
                          >
                            <path
                              strokeLinecap="round"
                              strokeLinejoin="round"
                              strokeWidth={2}
                              d="M15.172 7l-6.586 6.586a2 2 0 102.828 2.828l6.414-6.586a4 4 0 00-5.656-5.656l-6.415 6.585a6 6 0 108.486 8.486L20.5 13"
                            />
                          </svg>
                          <label className="ml-2 text-[#9D21E6]">
                            {dataTugas?.references[key].originalName}
                          </label>
                        </div>
                      </Link>
                    );
                  })}
              </div>
              {dataTugas && dataTugas?.lecturer?.id === sessionData?.user.id ? (
                <div className="w-full mb-4">
                  <div className="flex items-center gap-x-2 justify-between">
                    <label className="sm:text-lg font-semibold">
                      {lang.answerAssign}
                    </label>
                    <div className="flex items-center">
                      <p className="text-neutral-90 text-base pr-6">
                        {
                          subAssignment?.filter(
                            (quiz: any) =>
                              quiz.grade !== null && quiz.grade !== undefined,
                          ).length
                        }
                        /{subAssignment?.length}
                      </p>
                      <button
                        onClick={() => getAssignment(tempQuizId)}
                        className="text-white p-2 rounded-[10px] bg-[#f2bb2d]"
                      >
                        <svg
                          xmlns="http://www.w3.org/2000/svg"
                          fill="none"
                          viewBox="0 0 24 24"
                          strokeWidth={1.5}
                          stroke="currentColor"
                          className="w-6 h-6"
                        >
                          <path
                            strokeLinecap="round"
                            strokeLinejoin="round"
                            d="M16.023 9.348h4.992v-.001M2.985 19.644v-4.992m0 0h4.992m-4.993 0l3.181 3.183a8.25 8.25 0 0013.803-3.7M4.031 9.865a8.25 8.25 0 0113.803-3.7l3.181 3.182m0-4.991v4.99"
                          />
                        </svg>
                      </button>
                    </div>
                  </div>
                  <div className="mt-4">
                    {subAssignment?.map((x: any) => (
                      <div
                        key={x.id}
                        className="mb-1.5 p-4 border border-gray-900 rounded-lg flex gap-x-2.5 justify-between"
                      >
                        <div className="">
                          <h1 className="text-lg font-semibold">
                            {x.student.fullName}
                          </h1>
                          <div className="text-xs sm:text-sm text-gray-500 mt-2">
                            {x.references.map((z: any) => (
                              <div key={z.id}>
                                <p className="sm:text-normal text-[13px] hyphens-auto break-words max-w-[140px] sm:max-w-full truncate">
                                  {z.originalName}
                                </p>
                                <div className="flex items-center  mt-3 text-gray-500 mb-8">
                                  <svg
                                    xmlns="http://www.w3.org/2000/svg"
                                    fill="none"
                                    viewBox="0 0 24 24"
                                    strokeWidth={2.5}
                                    stroke="currentColor"
                                    className="w-[18px] h-[18px]"
                                  >
                                    <path
                                      strokeLinecap="round"
                                      strokeLinejoin="round"
                                      d="M3 16.5v2.25A2.25 2.25 0 005.25 21h13.5A2.25 2.25 0 0021 18.75V16.5m-13.5-9L12 3m0 0l4.5 4.5M12 3v13.5"
                                    />
                                  </svg>
                                  <label className="mx-2">
                                    {formatTimeAndDate(z.createdAt).date}
                                  </label>
                                  {" | "}
                                  <label className="mx-2">
                                    {formatTimeAndDate(z.createdAt).time}
                                  </label>
                                </div>
                                <div className="flex flex-col justify-between">
                                  <div className="flex  gap-x-3">
                                    <Link
                                      href={z.url}
                                      target="_blank"
                                      className=" text-[#9D21E6] bg-[#d7b9fb] p-2 rounded-md"
                                    >
                                      <svg
                                        xmlns="http://www.w3.org/2000/svg"
                                        fill="none"
                                        viewBox="0 0 24 24"
                                        strokeWidth={1.5}
                                        stroke="currentColor"
                                        className="w-6 h-6"
                                      >
                                        <path
                                          strokeLinecap="round"
                                          strokeLinejoin="round"
                                          d="M2.036 12.322a1.012 1.012 0 010-.639C3.423 7.51 7.36 4.5 12 4.5c4.638 0 8.573 3.007 9.963 7.178.07.207.07.431 0 .639C20.577 16.49 16.64 19.5 12 19.5c-4.638 0-8.573-3.007-9.963-7.178z"
                                        />
                                        <path
                                          strokeLinecap="round"
                                          strokeLinejoin="round"
                                          d="M15 12a3 3 0 11-6 0 3 3 0 016 0z"
                                        />
                                      </svg>
                                    </Link>
                                    <Link
                                      href={z.url}
                                      target="_blank"
                                      className=" text-[#9D21E6] bg-[#d7b9fb] p-2 rounded-md"
                                    >
                                      <svg
                                        xmlns="http://www.w3.org/2000/svg"
                                        fill="none"
                                        viewBox="0 0 24 24"
                                        strokeWidth={1.5}
                                        stroke="currentColor"
                                        className="w-6 h-6"
                                      >
                                        <path
                                          strokeLinecap="round"
                                          strokeLinejoin="round"
                                          d="M3 16.5v2.25A2.25 2.25 0 005.25 21h13.5A2.25 2.25 0 0021 18.75V16.5M16.5 12L12 16.5m0 0L7.5 12m4.5 4.5V3"
                                        />
                                      </svg>
                                    </Link>
                                  </div>
                                </div>
                              </div>
                            ))}
                          </div>
                        </div>
                        <div className="flex flex-col justify-between">
                          <div className="w-full h-full flex sm:flex-row flex-col items-center justify-between sm:mr-4 text-[#9D21E6] bg-[#d7b9fb] rounded-xl px-6 sm:px-4 py-2 mb-8">
                            <p className="text-base">{lang.grade}</p>
                            <p className="text-3xl sm:text-5xl text-right ">
                              {x.grade || "-"}
                            </p>
                          </div>
                          <button
                            className="sm:text-normal text-sm bg-[#9D21E6] text-white rounded-md px-6 py-2"
                            onClick={() => {
                              setInputQuiz({
                                id: x.id ?? "",
                                grade: x.grade ?? 0,
                                comment: x.lecturerComment ?? "",
                              });
                              setOpen(true);
                            }}
                          >
                            {lang.grade}
                          </button>
                        </div>
                      </div>
                    ))}
                    {subAssignment?.length <= 0 && (
                      <div className="w-full h-[10vh]  flex flex-col justify-center items-center">
                        <h1 className="text-2xl">{lang.dataNotFound}</h1>
                      </div>
                    )}
                  </div>
                </div>
              ) : new Date() > new Date(dataTugas?.dueDate).getTime() ? (
                <div className="mb-3 pb-1.5 border-b-2 border-dashed">
                  <label className="text-sm">Batas Waktu</label>
                  <div className="my-1 font-bold flex items-center gap-x-3">
                    <span>{formatDate(dataTugas?.dueDate)} </span>
                    <span className="inline-flex items-center justify-center rounded-full bg-emerald-100 px-2.5 py-0.5 text-emerald-700">
                      <svg
                        xmlns="http://www.w3.org/2000/svg"
                        fill="none"
                        viewBox="0 0 24 24"
                        strokeWidth="1.5"
                        stroke="currentColor"
                        className="-ms-1 me-1.5 h-4 w-4"
                      >
                        <path
                          strokeLinecap="round"
                          strokeLinejoin="round"
                          d="M9 12.75L11.25 15 15 9.75M21 12a9 9 0 11-18 0 9 9 0 0118 0z"
                        />
                      </svg>

                      <p className="whitespace-nowrap text-sm">
                        {lang.finished}
                      </p>
                    </span>
                  </div>
                  <p className="mt-2 text-sm">{lang.notSubmit}</p>
                </div>
              ) : (
                <div id="formSubmitKuis" className="">
                  <div className="mb-3 pb-1.5 border-b-2 border-dashed">
                    <label className="text-sm">{lang.deadline}</label>
                    <p className="my-1 font-bold">
                      {formatDate(dataTugas?.dueDate)}
                    </p>
                  </div>
                  <div className="">
                    <label
                      htmlFor="komentarNilaiKuis"
                      className="block  font-medium text-gray-700"
                    >
                      {lang.description}
                    </label>
                    <textarea
                      id="description"
                      className="w-full resize-none px-3 py-2 text-sm mt-1 rounded-[10px] border border-gray-400  outline-none  shadow-sm focus:ring-2 ring-[#9d21e6]"
                      rows={4}
                      onChange={(e) =>
                        setCreateMaterial({
                          ...createMaterial,
                          description: e.target.value,
                        })
                      }
                      value={createMaterial?.description}
                      placeholder={lang.description}
                    ></textarea>
                  </div>
                  <FileInput
                    handleDragOver={handleDragOver}
                    handleDragEnter={handleDragEnter}
                    handleDragLeave={handleDragLeave}
                    handleDropClass={handleDropClass}
                    handleButtonClick={handleButtonClick}
                    inputFileRef={inputFileRef}
                    handleFileChangeClass={handleFileChangeClass}
                    fileClass={fileClass}
                    setFileClass={setFileClass}
                    tempDocId={tempDocId}
                    setTempDocId={setTempDocId}
                    setCreateMaterial={setCreateMaterial}
                    title={lang.assignment}
                  />
                  <div className="w-fit mx-auto mt-4">
                    <button
                      onClick={() =>
                        postSubmission.mutate({
                          id: dataTugas?.id,
                          description: createMaterial.description,
                          type: "class-assignment",
                        })
                      }
                      className="px-8  py-2.5   bg-[#9d21e6] text-white shadow-xl rounded-[10px] font-semibold"
                      type="button"
                    >
                      {lang.submit}
                    </button>
                  </div>
                </div>
              )}
              {open && (
                <BeriNilai
                  open={open}
                  setOpen={setOpen}
                  setInputQuiz={setInputQuiz}
                  inputQuiz={inputQuiz}
                  postGradeAssignment={postGradeAssignment}
                />
              )}
            </>
          )}
          {modalType === "Material" && material && (
            <>
              <div className="flex justify-between items-start">
                <div>
                  <p className="mb-2.5 font-bold text-lg text-neutral-90">
                    {material?.title}
                  </p>

                  <p className="mb-2 -mt-3 text-base text-neutral-70 relative hyphens-auto break-word whitespace-pre-wrap">
                    <Linkify
                      options={{
                        target: "_blank",
                        className: "text-blue-600 font-medium hover:text-blue-700 duration-300 break-all",
                      }}
                    >
                      {material?.description}
                    </Linkify>
                  </p>
                </div>
                {material?.lecturer?.uuid === sessionData?.user.uuid && (
                  <>
                    <Popover open={open} onOpenChange={setOpen}>
                      <PopoverTrigger></PopoverTrigger>
                      <PopoverContent
                        align="center"
                        className="p-0 w-[500px] -translate-y-28 -translate-x-12"
                      >
                        <div className="relative">
                          {/* ModalChat header */}
                          <div className="rounded-lg bg-white py-5 shadow">
                            {/* ModalChat header */}
                            <div className="mb-5 flex items-center justify-center gap-2 rounded-t px-6 pb-2 pt-6 dark:border-gray-600">
                              <div className="text-red-600">
                                <svg
                                  xmlns="http://www.w3.org/2000/svg"
                                  fill="none"
                                  viewBox="0 0 24 24"
                                  strokeWidth="1.5"
                                  stroke="currentColor"
                                  className="h-16 w-16"
                                >
                                  <path
                                    strokeLinecap="round"
                                    strokeLinejoin="round"
                                    d="M14.74 9l-.346 9m-4.788 0L9.26 9m9.968-3.21c.342.052.682.107 1.022.166m-1.022-.165L18.16 19.673a2.25 2.25 0 01-2.244 2.077H8.084a2.25 2.25 0 01-2.244-2.077L4.772 5.79m14.456 0a48.108 48.108 0 00-3.478-.397m-12 .562c.34-.059.68-.114 1.022-.165m0 0a48.11 48.11 0 013.478-.397m7.5 0v-.916c0-1.18-.91-2.164-2.09-2.201a51.964 51.964 0 00-3.32 0c-1.18.037-2.09 1.022-2.09 2.201v.916m7.5 0a48.667 48.667 0 00-7.5 0"
                                  />
                                </svg>
                              </div>
                              <p id="confirmMessage" className="font-medium">
                                {lang.promptDeleteData}
                              </p>
                            </div>
                            {/* ModalChat footer */}
                            <div className="mx-auto flex w-fit gap-x-6">
                              <button
                                onClick={() => setOpen(false)}
                                id="confirmNo"
                                type="button"
                                value="No"
                                className=" rounded-[10px] border border-[#265881] px-4 py-2 text-sm font-medium text-[#265881] hover:bg-slate-200"
                              >
                                {lang.cancel}
                              </button>
                              <button
                                onClick={() =>
                                  postMaterial.mutate(createMaterial)
                                }
                                id="confirmYes"
                                type="button"
                                value="Yes"
                                className="rounded-[10px] bg-red-600 px-4 py-2 text-sm font-medium text-white hover:bg-[#F15E5E]"
                              >
                                {lang.submit}
                              </button>
                            </div>
                          </div>
                        </div>
                      </PopoverContent>
                    </Popover>
                    <div className="flex items-center gap-x-2.5 mb-2">
                      <button
                        onClick={() => {
                          setModalType("Buat Materi");
                          setCreateMaterial({
                            classId: userChatAktif?.classId,
                            title: material?.title,
                            description: material?.description,
                            documentIdList: material?.references.map(
                              (i: any) => i.id,
                            ),
                          });
                          setEdit(material?.id);
                          setTempDocId(material?.references);
                        }}
                        type="button"
                        className="w-fit editOfVessel rounded-lg  border-blue-600 text-blue-600 p-1 duration-300 hover:bg-blue-600 hover:text-white duration-300"
                      >
                        <svg
                          xmlns="http://www.w3.org/2000/svg"
                          fill="none"
                          viewBox="0 0 24 24"
                          strokeWidth="1.5"
                          stroke="currentColor"
                          className="w-6 h-6"
                        >
                          <path
                            strokeLinecap="round"
                            strokeLinejoin="round"
                            d="M16.862 4.487l1.687-1.688a1.875 1.875 0 112.652 2.652L10.582 16.07a4.5 4.5 0 01-1.897 1.13L6 18l.8-2.685a4.5 4.5 0 011.13-1.897l8.932-8.931zm0 0L19.5 7.125M18 14v4.75A2.25 2.25 0 0115.75 21H5.25A2.25 2.25 0 013 18.75V8.25A2.25 2.25 0 015.25 6H10"
                          />
                        </svg>
                      </button>
                      <button
                        type="button"
                        onClick={() => {
                          setdelmode(material?.id);
                          setOpen(true);
                        }}
                        className="w-fit deleteOfVessel rounded-lg  border-[#F62121] text-[#F62121] p-1 duration-300 hover:bg-[#F62121] hover:text-white"
                      >
                        <svg
                          xmlns="http://www.w3.org/2000/svg"
                          fill="none"
                          viewBox="0 0 24 24"
                          strokeWidth="1.5"
                          stroke="currentColor"
                          className="w-6 h-6"
                        >
                          <path
                            strokeLinecap="round"
                            strokeLinejoin="round"
                            d="M14.74 9l-.346 9m-4.788 0L9.26 9m9.968-3.21c.342.052.682.107 1.022.166m-1.022-.165L18.16 19.673a2.25 2.25 0 01-2.244 2.077H8.084a2.25 2.25 0 01-2.244-2.077L4.772 5.79m14.456 0a48.108 48.108 0 00-3.478-.397m-12 .562c.34-.059.68-.114 1.022-.165m0 0a48.11 48.11 0 013.478-.397m7.5 0v-.916c0-1.18-.91-2.164-2.09-2.201a51.964 51.964 0 00-3.32 0c-1.18.037-2.09 1.022-2.09 2.201v.916m7.5 0a48.667 48.667 0 00-7.5 0"
                          />
                        </svg>
                      </button>
                    </div>
                  </>
                )}
              </div>
              {material?.references.map((reference: any) => (
                <div
                  key={reference.id}
                  className="flex items-center text-black gap-x-2"
                >
                  <svg
                    xmlns="http://www.w3.org/2000/svg"
                    fill="none"
                    viewBox="0 0 24 24"
                    stroke="currentColor"
                    className="h-6 w-6 "
                  >
                    <path
                      strokeLinecap="round"
                      strokeLinejoin="round"
                      strokeWidth={2}
                      d="M15.172 7l-6.586 6.586a2 2 0 102.828 2.828l6.414-6.586a4 4 0 00-5.656-5.656l-6.415 6.585a6 6 0 108.486 8.486L20.5 13"
                    />
                  </svg>
                  <Link
                    href={reference.url}
                    target="_blank"
                    className="text-[#9d21e6] hover:text-purple-800"
                  >
                    {reference?.originalName}
                  </Link>
                </div>
              ))}
            </>
          )}

          {modalType === "Info Kelas" && dataInfoKelas && (
            <InfoClass
              dataInfoKelas={dataInfoKelas}
              attendanceClass={attendanceClass}
              userChatAktif={userChatAktif}
              sessionToken={sessionToken}
              lang={lang}
            />
          )}

          {modalType === "Member" && dataMember && (
            <div className="max-h-[60vh] overflow-y-auto px-1.5 pt-5">
              {dataMember?.map((item: any) => (
                <div
                  key={item.id}
                  className="flex items-center justify-between items-center gap-x-2 hover:bg-[#9D21E6]/10 border p-2.5 rounded-lg mb-2.5 cursor-pointer"
                >
                  <div className="flex items-center gap-x-2">
                    <div className="flex-none relative h-12 w-12">
                      <Image
                        width={60}
                        height={60}
                        src={item?.profilePict ?? `/static/images/logokedi.svg`}
                        className="absolute inset-0 h-full w-full object-cover  rounded-lg"
                        alt=""
                      />
                    </div>
                    <div>
                      <p className="font-semibold text-ellipsis">
                        {sessionData?.user.id === item.id
                          ? item.fullName + " (Anda)"
                          : item.fullName}
                      </p>
                      {item.username && (
                        <p className="text-sm text-[#9D21E6]">
                          {"@" + item.username}
                        </p>
                      )}
                    </div>
                  </div>
                  {!item.isFriend && sessionData?.user.id !== item.id && (
                    <div
                      onClick={() => addFriend.mutate(item.id)}
                      className="text-[#9D21E6] cursor-pointer"
                    >
                      <svg
                        xmlns="http://www.w3.org/2000/svg"
                        width="24px"
                        height="24px"
                        viewBox="0 0 24 24"
                      >
                        <g
                          fill="none"
                          stroke="currentColor"
                          strokeLinecap="round"
                          strokeWidth="2.5"
                        >
                          <path
                            strokeDasharray="20"
                            strokeDashoffset="20"
                            d="M3 21V20C3 17.7909 4.79086 16 7 16H11C13.2091 16 15 17.7909 15 20V21"
                          >
                            <animate
                              fill="freeze"
                              attributeName="stroke-dashoffset"
                              dur="0.4s"
                              values="20;0"
                            ></animate>
                          </path>
                          <path
                            strokeDasharray="20"
                            strokeDashoffset="20"
                            d="M9 13C7.34315 13 6 11.6569 6 10C6 8.34315 7.34315 7 9 7C10.6569 7 12 8.34315 12 10C12 11.6569 10.6569 13 9 13Z"
                          >
                            <animate
                              fill="freeze"
                              attributeName="stroke-dashoffset"
                              begin="0.5s"
                              dur="0.4s"
                              values="20;0"
                            ></animate>
                          </path>
                          <path
                            strokeDasharray="8"
                            strokeDashoffset="8"
                            d="M15 6H21"
                          >
                            <animate
                              fill="freeze"
                              attributeName="stroke-dashoffset"
                              begin="1s"
                              dur="0.2s"
                              values="8;0"
                            ></animate>
                          </path>
                          <path
                            strokeDasharray="8"
                            strokeDashoffset="8"
                            d="M18 3V9"
                          >
                            <animate
                              fill="freeze"
                              attributeName="stroke-dashoffset"
                              begin="1.2s"
                              dur="0.2s"
                              values="8;0"
                            ></animate>
                          </path>
                        </g>
                      </svg>
                    </div>
                  )}
                </div>
              ))}
            </div>
          )}

          {modalType === "All Material" && allMaterial && (
            <div className="flex-1 overflow-y-auto">
              {allMaterial?.length === 0 ? (
                <div className="flex items-center justify-center h-full">
                  {lang.dataNotFound}
                </div>
              ) : (
                allMaterial?.map((x: any) => (
                  <div onClick={() => getMaterial(x.id)} key={x.id}>
                    <div className="w-full my-2  h-fit  border border-gray-200 py-2 px-4 bg-white rounded-xl">
                      <label>{x.title}</label>

                      <h1 className="text-black font-bold">
                        {lang.by}: {x.lecturer.fullName}
                      </h1>

                      <p className="text-gray-600 mt-2 text-[0.775rem]   ">
                        {formatTanggal(x.createdDate)}
                      </p>
                    </div>
                  </div>
                ))
              )}{" "}
            </div>
          )}
          {modalType === "All Assignment" && allAssignment && (
            <div className="flex-1 overflow-y-auto max-h-[80vh]">
              {allAssignment?.length === 0 ? (
                <div className="flex items-center justify-center h-full">
                  {lang.dataNotFound}
                </div>
              ) : (
                allAssignment?.map((x: any) => (
                  <div
                    onClick={() => {
                      const isAdmin =
                        userChatAktif?.receiver.id === userChatAktif?.sender.id;
                      if (isAdmin) {
                        getAssignment(x.id);
                        settempQuizId(x.id);
                      } else {
                        singleSubAssign(x.id);
                      }
                    }}
                    key={x.id}
                  >
                    <div className="w-full my-2  h-fit  border border-gray-200 py-2 px-4 bg-white rounded-xl">
                      <label>{x.name}</label>

                      <h1 className="text-black font-bold">{lang.deadline}</h1>
                      <div className="flex items-center text-sm gap-x-2.5">
                        <p className="text-gray-600 text-md">
                          {formatTanggal(x.dueDate)}
                        </p>
                        {getTaskStatus(x.dueDate)}
                      </div>
                      <div className="w-full mt-2 flex justify-center"></div>
                    </div>
                  </div>
                ))
              )}
            </div>
          )}
          {modalType === "All Quiz" && allQuiz && (
            <div className="flex-1 overflow-y-auto max-h-[80vh]">
              {allQuiz?.length === 0 ? (
                <div className="flex items-center justify-center h-full">
                  {lang.dataNotFound}
                </div>
              ) : (
                allQuiz?.map((x: any) => (
                  <div
                    onClick={() => {
                      const isAdmin =
                        userChatAktif?.receiver.id === userChatAktif?.sender.id;
                      if (isAdmin) {
                        getQuiz(x.id);
                        settempQuizId(x.id);
                      } else {
                        singleSubQuiz(x.id);
                      }
                    }}
                    key={x.id}
                  >
                    <div className="w-full my-2  h-fit  border border-gray-200 py-2 px-4 bg-white rounded-xl">
                      <label>{x.name}</label>
                      <h1 className="text-black font-bold">{lang.deadline}</h1>
                      <div className="flex items-center text-sm gap-x-2.5">
                        <p className="text-gray-600 text-md">
                          {formatTanggal(x.dueDate)}
                        </p>
                        {getTaskStatus(x.dueDate)}
                      </div>
                    </div>
                  </div>
                ))
              )}
            </div>
          )}
          {modalType === "Absensi" &&
          attendanceClass &&
          userChatAktif.receiver.id === userChatAktif.sender.id ? (
            <div className="flex-1 mt-6 px-4">
              <Tabs defaultValue="dosen" className="w-full">
                <TabsList className="grid grid-cols-2 mb-2.5">
                  <TabsTrigger value="dosen">{lang.lecturer}</TabsTrigger>
                  <TabsTrigger value="mahasiswa">{lang.student}</TabsTrigger>
                </TabsList>
                <TabsContent
                  value="dosen"
                  className="pb-5 overflow-y-auto max-h-[70vh] pr-2"
                >
                  {attendanceClass?.map((d, index) => {
                    const filterAttend =
                      attendanceClass &&
                      attendanceClass[index].userList?.map((d) => {
                        const endAttendTime =
                          attendanceClass[index]?.class?.endAttendanceTime;
                        const userAttendTime = d?.attendanceDate;
                        const checkLate = compareTimes(
                          userAttendTime,
                          endAttendTime,
                        );

                        let obj = {
                          ...d,
                          isLate: checkLate,
                        };
                        return obj;
                      });

                    const isShowBtn = isWithinTimeRange(d.startTime, d.endTime);

                    const filter = filterAttend?.filter(
                      (user) => user.userId === sessionData?.user.id,
                    );
                    {
                      /*   const filterNon = d.userList.filter(
                                (user) => user.userId !== sessionData?.user.id,
                                );
                                const tempPresent = d.userList.some( (user: any) => user.userId === sessionData?.user.id,);
                                const hasPassed = tempPresent && isStartTimeBeforeNow(d.startTime);
                                const isShow = isEndTimePassed(d.startTime);
                                const isDisabled = isEndTimePassed(d.endTime);*/
                    }
                    return (
                      <div key={d.id}>
                        <div className="space-y-2">
                          {filter && filter.length > 0 ? (
                            filter?.map((d: any) => {
                              const isLate = d.isLate == 2 ? true : false;
                              const checkLate = isLate
                                ? "text-yellow-600"
                                : "text-[#9D21E6]";
                              return (
                                <div
                                  key={d.id}
                                  className={` ${checkLate}  ${
                                    isLate
                                      ? "border-yellow-600"
                                      : "border-[#9D21E6]"
                                  } px-2.5 mb-3 mt-5  border-b-2 border-dashed pb-2.5 text-sm flex items-center justify-between `}
                                >
                                  <div>
                                    <p>
                                      {formatDate(d.attendanceDate)} {"  "}
                                    </p>
                                    <p className="font-semibold pt-2">
                                      {lang.session} {index + 1}
                                    </p>
                                  </div>
                                  <div className="space-y-1">
                                    <p>Jam</p>
                                    <p className="font-semibold">
                                      {formatToTime(d.attendanceDate)}
                                    </p>
                                  </div>
                                </div>
                              );
                            })
                          ) : (
                            <li
                              className={`  flex items-center justify-between border-b-2 border-dashed  border-gray-500 px-2.5 py-3.5 `}
                            >
                              <div className="flex flex-col text-sm gap-x-2.5 text-gray-600">
                                <p className="font-semibold mb-1">
                                  {lang.session} {index + 1}
                                </p>
                                <div className="font-semibold"></div>
                              </div>

                              <div className="space-y-1 flex flex-col items-center">
                                {!isShowBtn ? (
                                  <span className="text-sm font-bold">-</span>
                                ) : (
                                  <button
                                    onClick={() => handleAttend.mutate(d.id)}
                                    className={`text-sm bg-red-600 text-white px-4 py-2 rounded-[10px] text-sm`}
                                  >
                                    {lang.notYetPresent}
                                  </button>
                                )}
                              </div>
                            </li>
                          )}
                        </div>
                      </div>
                    );
                  })}
                </TabsContent>
                <TabsContent
                  value="mahasiswa"
                  className="overflow-y-auto max-h-[70vh] pr-2"
                >
                  {attendanceClass?.map((d, index) => {
                    // const tempPresent = d.userList.some(
                    //   (user: any) =>
                    //     user.userId === sessionData?.user.id,
                    // );
                    // const filter = d.userList.filter(
                    //   (user) => user.userId === sessionData?.user.id,
                    // );

                    const filterAttend =
                      attendanceClass &&
                      attendanceClass[index].userList?.map((d) => {
                        const endAttendTime =
                          attendanceClass[index]?.class?.endAttendanceTime;
                        const userAttendTime = d?.attendanceDate;
                        const checkLate = compareTimes(
                          userAttendTime,
                          endAttendTime,
                        );

                        let obj = {
                          ...d,
                          isLate: checkLate,
                        };
                        return obj;
                      });
                    const filterNon = filterAttend?.filter(
                      (user) => user.userId !== sessionData?.user.id,
                    );

                    const isNotEmpty = filterNon && filterNon.length > 0;
                    const isShow = isEndTimePassed(d.startTime);
                    // const hasPassed =
                    //   tempPresent && isStartTimeBeforeNow(d.startTime);
                    // const isDisabled = isEndTimePassed(d.endTime);
                    return (
                      <div
                        key={d.id}
                        className={` ${
                          isShow ? "" : ""
                        } flex items-center justify-between mb-3 mt-5`}
                      >
                        <ul className="space-y-1 w-full ">
                          <li>
                            <details className="group [&_summary::-webkit-details-marker]:hidden">
                              <summary className="border-b-2 border-dashed flex cursor-pointer items-center justify-between p-2">
                                <span
                                  className={` ${
                                    isNotEmpty
                                      ? "text-[#9D21E6]"
                                      : "text-gray-700"
                                  } text-sm font-medium`}
                                >
                                  {lang.session} {index + 1}{" "}
                                </span>

                                <span className="shrink-0 transition duration-300 group-open:-rotate-180">
                                  <svg
                                    xmlns="http://www.w3.org/2000/svg"
                                    className="h-5 w-5"
                                    viewBox="0 0 20 20"
                                    fill="currentColor"
                                  >
                                    <path
                                      fillRule="evenodd"
                                      d="M5.293 7.293a1 1 0 011.414 0L10 10.586l3.293-3.293a1 1 0 111.414 1.414l-4 4a1 1 0 01-1.414 0l-4-4a1 1 0 010-1.414z"
                                      clipRule="evenodd"
                                    />
                                  </svg>
                                </span>
                              </summary>

                              <ul className="space-y-1 p-3">
                                {filterNon && filterNon.length > 0 ? (
                                  filterNon?.map((d: any) => {
                                    const isLate = d.isLate == 2 ? true : false;
                                    return (
                                      <li
                                        key={d.id}
                                        className={` ${
                                          isLate
                                            ? "border-yellow-600"
                                            : "border-[#9D21E6]"
                                        } flex items-center justify-between border-b-2 border-dashed  px-2.5 py-2.5`}
                                      >
                                        <div
                                          className={` ${
                                            isLate
                                              ? "text-yellow-600"
                                              : "text-[#9D21E6]"
                                          } flex flex-col text-sm gap-x-2.5 `}
                                        >
                                          <div>{d.fullName}</div>
                                          <div className="font-semibold">
                                            {
                                              formatTimeAndDate(
                                                d.attendanceDate,
                                              ).date
                                            }
                                          </div>
                                        </div>

                                        <div
                                          className={` ${
                                            isLate
                                              ? "text-yellow-600"
                                              : "text-[#9D21E6]"
                                          } text-[#9D21E6] space-y-1 flex flex-col items-center `}
                                        >
                                          <p className="text-sm">Jam</p>
                                          <span className="text-sm font-bold">
                                            {formatToTime(d.attendanceDate)}
                                          </span>
                                        </div>
                                      </li>
                                    );
                                  })
                                ) : (
                                  <div className="opacity-70 h-full flex items-center justify-center">
                                    {lang.dataNotFound}
                                  </div>
                                )}
                              </ul>
                            </details>
                          </li>
                        </ul>
                      </div>
                    );
                  })}
                </TabsContent>
              </Tabs>
            </div>
          ) : (
            modalType === "Absensi" && (
              <div className="flex-1 px-4 overflow-y-auto">
                <ul className="space-y-1 w-full ">
                  <li>
                    <details
                      className="group [&_summary::-webkit-details-marker]:hidden"
                      open={true}
                    >
                      <summary
                        className={` group-open:border-b flex  group-open:border-gray-400 cursor-pointer items-center justify-between p-3`}
                      >
                        <div className="flex items-center gap-x-5">
                          <div className="flex-none relative w-14 h-14">
                            <Image
                              width={60}
                              height={60}
                              src={
                                attendanceClass[0]?.class?.lecturer
                                  ?.profilePicture?.url ||
                                `/static/images/logokedi.svg`
                              }
                              className="absolute inset-0 h-full w-full object-cover rounded-xl shadow-md"
                              alt=""
                            />
                          </div>
                          <div className="flex flex-col flex-1">
                            <div className="text-[14px] line-clamp-1">
                              {attendanceClass[0]?.class?.lecturer?.fullName}
                            </div>
                            <div className="text-[14px] font-semibold line-clamp-1">
                              {attendanceClass[0]?.class?.name}
                            </div>
                            <div className="text-[14px] opacity-70 line-clamp-1">
                              {attendanceClass[0]?.class?.description}{" "}
                              <span className="mx-1.5">|</span>
                              {formatToTime(
                                attendanceClass &&
                                  attendanceClass[0]?.class
                                    ?.startAttendanceTime,
                              )}{" "}
                              -{" "}
                              {formatToTime(
                                attendanceClass &&
                                  attendanceClass[0]?.class?.endAttendanceTime,
                              )}
                            </div>
                          </div>
                        </div>
                        <span className="shrink-0 transition duration-300 group-open:-rotate-180">
                          <svg
                            xmlns="http://www.w3.org/2000/svg"
                            className="h-5 w-5"
                            viewBox="0 0 20 20"
                            fill="currentColor"
                          >
                            <path
                              fillRule="evenodd"
                              d="M5.293 7.293a1 1 0 011.414 0L10 10.586l3.293-3.293a1 1 0 111.414 1.414l-4 4a1 1 0 01-1.414 0l-4-4a1 1 0 010-1.414z"
                              clipRule="evenodd"
                            />
                          </svg>
                        </span>
                      </summary>
                      <ul className="space-y-1">
                        {attendanceClass?.map((d, index) => {
                          const filterAttend =
                            attendanceClass &&
                            attendanceClass[index].userList?.map((d) => {
                              const endAttendTime =
                                attendanceClass[index]?.class
                                  ?.endAttendanceTime;
                              const userAttendTime = d?.attendanceDate;
                              const checkLate = compareTimes(
                                userAttendTime,
                                endAttendTime,
                              );

                              let obj = {
                                ...d,
                                isLate: checkLate,
                              };
                              return obj;
                            });

                          const isShowBtn = isWithinTimeRange(
                            d.startTime,
                            d.endTime,
                          );
                          const filter = filterAttend.filter(
                            (user) => user.userId === sessionData?.user.id,
                          );

                          return (
                            <div key={d.id} className="space-y-2">
                              {filter && filter.length > 0 ? (
                                filter?.map((item: any) => {
                                  const isLate =
                                    item.isLate == 2 ? true : false;
                                  const checkLate = isLate
                                    ? "text-yellow-600"
                                    : "text-[#9D21E6]";
                                  return (
                                    <li
                                      key={item.id}
                                      className={` ${checkLate} flex items-center justify-between border-b-2 border-dashed  border-gray-500 px-2.5 py-3.5 `}
                                    >
                                      <div className="flex flex-col text-sm gap-x-2.5">
                                        <p className="font-semibold mb-1">
                                          {lang.session} {index + 1}
                                        </p>
                                        <p>{formatDate(item.attendanceDate)}</p>
                                        <div className="font-semibold"></div>
                                      </div>
                                      <div className="space-y-1 flex flex-col items-center">
                                        <span className="text-sm font-bold">
                                          {formatToTime(item.attendanceDate)}
                                        </span>
                                      </div>
                                    </li>
                                  );
                                })
                              ) : (
                                <li
                                  className={`  flex items-center justify-between border-b-2 border-dashed  border-gray-500 px-2.5 py-3.5 `}
                                >
                                  <div className="flex flex-col text-sm gap-x-2.5 text-gray-600">
                                    <p className="font-semibold mb-1">
                                      {lang.session} {index + 1}
                                    </p>
                                    <div className="font-semibold"></div>
                                  </div>
                                  <div className="space-y-1 flex flex-col items-center">
                                    {!isShowBtn ? (
                                      <span className="text-sm font-bold">
                                        -
                                      </span>
                                    ) : (
                                      <button
                                        onClick={() =>
                                          handleAttend.mutate(d.id)
                                        }
                                        className={`text-sm bg-red-600 text-white px-4 py-2 rounded-[10px] text-sm`}
                                      >
                                        {lang.notYetPresent}
                                      </button>
                                    )}
                                  </div>
                                </li>
                              )}
                            </div>
                          );
                        })}
                      </ul>
                    </details>
                  </li>
                </ul>
              </div>
            )
          )}
        </DialogContent>
      )}
    </Dialog>
  );
});

export default ModalChat;
