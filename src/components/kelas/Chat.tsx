import React, { ChangeEvent, useEffect, useRef, useState } from "react";
import Image from "next/image";
import axios from "axios";
import toast from "react-hot-toast";
import { useRouter } from "next/router";
import { useMediaQuery } from "react-responsive";
import { useMutation, useQuery, useQueryClient } from "@tanstack/react-query";
import { usePubNub } from "@/context/PubnubContext";
import { v4 as uuidv4 } from "uuid";
import { ChatBox } from "./ChatBox";
import { getTaskStatus } from "@/utils/statusDate";
import { useAutoResizeInput } from "@/hook/useAutoResizeInput";
import { LoadingSpinner } from "@/components/ui/loading";
import { InfoClass } from "@/components/kelas/module/menu/InfoClass";
import { InfoGroup } from "@/components/kelas/module/menu/InfoGroup";
import { InfoMembers } from "@/components/kelas/module/menu/InfoMembers";
import { Tabs, TabsContent, TabsList, TabsTrigger } from "@/components/ui/tabs";
import { ReusableAlertDialog } from "@/components/kelas/module/element/Alert";
import { encryptData, decryptData } from "@/utils/helper";
import { getPubNub } from "@/utils/getPubNub";
import {
  Dialog,
  DialogContent,
  DialogHeader,
  DialogTitle,
} from "@/components/ui/dialog";
import {
  formatTanggal,
  formatDate,
  formatToTime,
  isEndTimePassed,
  compareTimes,
  formatTimeAndDate,
} from "@/utils/convertTime";
import useUserChat from "@/store/useUserChat";
import { getMessageUser } from "@/utils/getMessageUser";

interface SelectedFile {
  file: File | null;
  preview: string | ArrayBuffer | null;
}

export const Chat = (props: any) => {
  const router = useRouter();
  const { addData, addDataPrivate, lang, removeItem, user, viewportHeight } =
    props;

  const { pubnub } = usePubNub();

  const chatMode = useUserChat((state) => state.chatMode);
  const isMobileView = useUserChat((state) => state.isMobileView);
  const messageHistory = useUserChat((state) => state.messageHistory);
  const privateMessage = useUserChat((state) => state.privateMessage);
  const isLoadMoreMsg = useUserChat((state) => state.isLoadMoreMsg);
  const userChatAktif = useUserChat((state) => state.userChatAktif);

  const setMessageHistory = (value) => {
    useUserChat.setState({
      messageHistory: value,
    });
  };
  const setUserChatAktif = (value) => {
    useUserChat.setState({
      userChatAktif: value,
    });
  };
  const setIsMobileView = (value) => {
    useUserChat.setState({
      isMobileView: value,
    });
  };
  const { inputText, setInputText, isHeight, inputTextRef } =
    useAutoResizeInput("");
  // const [message, setMessage] = useState("");
  const sessionToken = user?.accessToken;
  const queryClient = useQueryClient();
  const [selectedFile, setSelectedFile] = useState<SelectedFile>({
    file: null,
    preview: null,
  });

  const lastMessageKey = `LastMessage_${user?.id}`;

  const [isLoadingFile, setIsLoadingFile] = useState(false);
  const [isSentMessage, setIsSentMessage] = useState(false);
  const [multiMessage, setMultiMessage] = useState([]);
  const [startTime, setStartTime] = useState("");
  const [endTime, setEndTime] = useState("");
  const [tempQuizId, settempQuizId] = useState(0);

  interface ApiResponse {
    data: {
      message: string;
    };
  }
  interface PostResponse extends ApiResponse {}
  type PostFile = {};

  const uploadFile: PostFile = useMutation(
    async (file): Promise<PostResponse> => {
      if (!sessionToken) throw new Error("Missing session token");
      try {
        const formData = new FormData();
        formData.append("file", file);
        formData.append("documentTypeId", "1");
        formData.append("isPrivate", "false");
        const response = await axios.post<PostResponse>(
          process.env.API_BE + `/api/storage`,
          formData,
          {
            headers: {
              "Content-Type": "multipart/form-data",
              Authorization: `Bearer ${sessionToken}`,
            },
          },
        );
        const res = response?.data;
        postPubnub(res);
        return response.data;
      } catch (error) {
        setIsLoadingFile(false);
        console.error(error);
        throw error;
      }
    },
    {
      onSuccess: () => {
        toast.success(lang.successUpload);
      },

      onError: (error) =>
        toast.error(`Error: ${error.response?.data?.meta?.message ?? error}`),
    },
  );

  function formatBytes(bytes: any, decimals = 2) {
    if (!+bytes) return "0 Bytes";

    const k = 1024;
    const dm = decimals < 0 ? 0 : decimals;
    const sizes = [
      "Bytes",
      "KiB",
      "MiB",
      "GiB",
      "TiB",
      "PiB",
      "EiB",
      "ZiB",
      "YiB",
    ];

    const i = Math.floor(Math.log(bytes) / Math.log(k));

    return `${parseFloat((bytes / Math.pow(k, i)).toFixed(dm))} ${sizes[i]}`;
  }

  const deleteMessage = async () => {
    try {
      pubnub.deleteMessages(
        {
          channel: userChatAktif?.channel,
          start: startTime,
          end: endTime,
        },
        async function (status) {
          if (status.statusCode === 200) {
            setMultiMessage([]);
            toast.success(lang.successDeleteMessage);
            getMessageUser({ channel: userChatAktif?.channel, isDelete: true });

            const messages = await pubnub.fetchMessages({
              channels: [userChatAktif?.channel],
              count: 1,
            });
            const msg_data = messages?.channels[userChatAktif?.channel]
              ? messages?.channels[userChatAktif?.channel]
              : [];

            if (msg_data.length === 0 && (chatMode === "private" || chatMode === "group")) {
              const lastAllMsg = localStorage.getItem(lastMessageKey);

              pubnub.signal(
                { message: "delete_all", channel: userChatAktif?.channel },
                (status, response) => {
                  console.log("response:", response);
                  console.log("status:", status);
                },
              );
              if (lastAllMsg && chatMode === "private") {
                const lastMessObj = JSON.parse(lastAllMsg);
                const decrypted = decryptData(lastMessObj, "KEDI");
                const newData = decrypted?.privateMessage?.filter(
                  (d) => d.channel !== userChatAktif?.channel,
                );
                const encryptedData = encryptData(
                  {
                    classMessage: decrypted?.classMessage,
                    privateMessage: newData,
                    groupChatMessage: decrypted?.groupChatMessage,
                  },
                  "KEDI",
                );
                localStorage.setItem(
                  lastMessageKey,
                  JSON.stringify(encryptedData),
                );
                router.push("/class");
                setUserChatAktif({});
              }

              if (chatMode === "private") {
                addDataPrivate(userChatAktif?.channel, true);
              } else if (chatMode === "group") {
                //addData();
              }
            }
          }
        },
      );
    } catch (error) {
      console.error("Error deleting messages:", error);
    }
  };

  async function publishWithTimeout(
    pubnub,
    obj,
    userChatAktif,
    setIsSentMessage,
    setInputText,
    setMessageHistory,
    addDataPrivate,
    chatMode,
  ) {
    const publishPromise = pubnub.publish({
      channel: userChatAktif?.channel,
      message: obj,
      sendByPost: true,
    });

    const timeoutPromise = new Promise((resolve, reject) => {
      setTimeout(() => {
        reject(new Error("Publish operation timed out"));
      }, 3000); // Adjust the timeout value as needed (5000 milliseconds = 5 seconds)
    });

    try {
      const result = await Promise.race([publishPromise, timeoutPromise]);

      if (result?.timetoken !== null) {
        const temp_obj = {
          channel: userChatAktif?.channel,
          message: obj,
          messageType: "text",
          timetoken: result?.timetoken,
          uuid: userChatAktif?.sender.id,
        };
        setIsSentMessage(false);
        setInputText("");
        const new_msg = [...messageHistory, temp_obj];
        if (new_msg?.length === 1) {
          getMessageUser({ channel: userChatAktif?.channel, isDelete: true });
        } else {
          setMessageHistory(new_msg);
        }

        const channelActivePrivate = JSON.parse(
          localStorage.getItem("channelActivePrivate") ?? "",
        );
        if (
          !channelActivePrivate.includes(userChatAktif?.channel) &&
          chatMode === "private"
        ) {
          channelActivePrivate.push(userChatAktif?.channel);
          addDataPrivate(userChatAktif?.channel);
          localStorage.setItem(
            "channelActivePrivate",
            JSON.stringify(channelActivePrivate),
          );
        }

        const channelExists = privateMessage?.some(
          (item) => item.channel === userChatAktif?.channel,
        );
        if (!channelExists && chatMode === "private") {
          addDataPrivate(userChatAktif?.channel);
        }
      }
    } catch (error) {
      console.error("Error:", error);
      // Handle timeout error here, such as showing a message to the user
    }
  }

  // Panggil fungsi ini untuk melakukan publish dengan timeout
  const sendMessage = async (message: string) => {
    const user = userChatAktif;
    const dateNow = +new Date() * 10000;

    if (message && user?.sender?.name) {
      let obj = {
        pn_gcm: {
          notification: {
            title: user?.sender?.name,
            body: message,
          },
          data: {
            type: "chat",
            sent_to: {
              id: user?.receiver?.id,
              channel: user?.channel,
              name: user?.receiver?.name,
              urlPhoto: user?.receiver?.photo,
            },
            custom_data: {
              status: "sent",
              author: {
                imageUrl: user?.sender?.photo,
                id: user?.sender?.id,
                firstName: user?.sender?.name,
              },
              text: message,
              type: "text",
              id: uuidv4(),
              createdAt: dateNow,
              metadata: {
                timetoken: dateNow,
              },
            },
          },
        },
        pn_apns: {
          aps: {
            alert: {
              title: user?.sender?.name,
              body: "Sent a message",
            },
          },
        },
      };

      setIsSentMessage(true);
      try {
        publishWithTimeout(
          pubnub,
          obj,
          userChatAktif,
          setIsSentMessage,
          setInputText,
          setMessageHistory,
          addDataPrivate,
          chatMode,
        );
      } catch (status) {
        setIsSentMessage(false);
        console.log(status);
      }
    }
  };

  const todoListRef = useRef<HTMLDivElement>(null);

  useEffect(() => {
    if (!isLoadMoreMsg) {
      scrollToLastTodo();
    } else {
      useUserChat.setState({
        isLoadMoreMsg: false,
      });
    }
   // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [messageHistory]);

  function scrollToLastTodo() {
    if (todoListRef?.current !== null) {
        todoListRef?.current?.scrollTo({
          top: todoListRef?.current?.scrollHeight,
          behavior: "instant",
        });
      // const newItem = todoListRef.current.lastElementChild;
      // if (newItem) {
      // newItem.scrollIntoView({ behavior: "auto" });
      // }
    }
  }

  useEffect(() => {
    setMultiMessage([]);
  }, [messageHistory]);

  useEffect(() => {
    setInputText("");
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [userChatAktif]);

  useEffect(() => {
    if (multiMessage.length === 1) {
      let result = BigInt(multiMessage[0]) + BigInt(1);
      let toString = result.toString();

      setStartTime(toString);
      setEndTime(multiMessage[0]);
    } else if (multiMessage.length > 1) {
      let min = null;
      let max = null;

      for (const message of multiMessage) {
        let a = BigInt(message);

        if (min === null || min > a) {
          min = a;
        }

        if (max === null || max < a) {
          max = a;
        }
      }

      let result = max + BigInt(1);
      let toString = result.toString();

      setStartTime(toString);
      setEndTime(min.toString());
    }
  }, [multiMessage]);

  const fileInputRef = useRef<HTMLInputElement>(null);

  const handleButtonClick = () => {
    if (fileInputRef.current) {
      fileInputRef.current.click();
    }
  };

  const sendFile = async () => {
    if (!selectedFile) return null;
    setIsLoadingFile(true);
    uploadFile.mutate(selectedFile.file);
  };

  async function postPubnub(result) {
    const documentStorage = result?.documentStorage;

    if (documentStorage && userChatAktif?.sender?.name) {
      const message = `Sent  a ${documentStorage.mime.includes("image") ? "photo" : "file"}`;
      let obj = {
        pn_gcm: {
          notification: {
            title: userChatAktif?.sender?.name,
            body: message,
          },
          data: {
            type: "chat",
            sent_to: {
              id: userChatAktif?.receiver?.id,
              channel: userChatAktif?.channel,
              name: userChatAktif?.receiver?.name,
              urlPhoto: userChatAktif?.receiver?.photo,
            },
            custom_data: {
              status: "sent",
              author: {
                imageUrl: userChatAktif?.sender?.photo,
                id: userChatAktif?.sender?.id,
                firstName: userChatAktif?.sender?.name,
              },
              createdAt: +new Date() * 10000,
              id: uuidv4(),
              documentId: documentStorage.id,
              mimeType: documentStorage.mime,
              name: documentStorage.originalName,
              size: selectedFile.file.size,
              type: documentStorage.mime.includes("image") ? "image" : "file",
              uri: result.url,
              metadata: {
                timetoken: +new Date() * 10000,
              },
            },
          },
        },
        pn_apns: {
          aps: {
            alert: {
              title: userChatAktif?.sender?.name,
              body: message,
            },
          },
        },
      };

      try {
        const result = await pubnub.publish({
          channel: userChatAktif?.channel,
          message: obj,
        });

        if (result?.token !== null) {
          const temp_obj = {
            channel: userChatAktif?.channel,
            message: obj,
            messageType: documentStorage.mime.includes("image")
              ? "image"
              : "file",
            timetoken: +new Date() * 10000,
            uuid: userChatAktif?.sender.id,
          };

          handleDelete();
          setInputText("");
          setIsLoadingFile(false);
          setMessageHistory([...messageHistory, temp_obj]);

          const channelActivePrivate = JSON.parse(
            localStorage.getItem("channelActivePrivate") ?? "",
          );
          if (
            !channelActivePrivate.includes(userChatAktif?.channel) &&
            chatMode === "private"
          ) {
            channelActivePrivate.push(userChatAktif?.channel);
            addDataPrivate(userChatAktif?.channel);
            localStorage.setItem(
              "channelActivePrivate",
              JSON.stringify(channelActivePrivate),
            );
          }
        }
      } catch (status) {
        setIsLoadingFile(false);
        console.log(status);
      }
    }
  }

  const handleFileChange = (event: ChangeEvent<HTMLInputElement>) => {
    const file = event.target.files?.[0] ?? null;
    if (file) {
      const reader = new FileReader();
      reader.onloadend = () => {
        setSelectedFile({ file, preview: reader.result });
      };

      if (file.type.startsWith("image/")) {
        reader.readAsDataURL(file);
      } else {
        reader.readAsText(file);
      }
    }
  };

  const inputFileRef = useRef(null);
  const handleDelete = () => {
    setSelectedFile({
      file: null,
      preview: null,
    });
  };

  const handleDragOver = (event: any) => {
    event.preventDefault();
  };

  const handleDragEnter = (event: any) => {
    event.preventDefault();
  };

  const handleDragLeave = (event: any) => {
    event.preventDefault();
  };

  const handleDrop = (event: any) => {
    event.preventDefault();
    const file = event.dataTransfer.files[0];
    if (file) {
      const reader = new FileReader();
      reader.onloadend = () => {
        setSelectedFile({ file, preview: reader.result });
      };

      if (file.type.startsWith("image/")) {
        reader.readAsDataURL(file);
      } else {
        reader.readAsText(file);
      }
    }
  };

  const isLG = useMediaQuery({ query: "(max-width: 1230px)" });

  const [modalClass, setModalClass] = useState(false);
  const [modalType, setModalType] = useState("");
  const [sideType, setSideType] = useState("");
  const [sidebar, setSidebar] = useState(false);
  const [heading, setHeading] = useState("");
  const [isLoadingModal, setIsloadingModal] = useState(false);
  const [isLoadingSidebar, setIsloadingSidebar] = useState(false);

  const getExam = async (classId: number, isStudent: boolean, type: string) => {
    setIsloadingModal(true);
    setModalType("Ujian");
    setHeading(lang.exam);
    setModalClass(true);
    if (sidebar) {
      setSidebar(false);
    }
    try {
      const response = await axios.get(
        process.env.API_BE +
          `/api/class-exam/${type}/${classId}/?isStudent=${isStudent}`,
        {
          headers: {
            Authorization: `Bearer ${sessionToken}`,
          },
        },
      );
      if (response.data.data) {
        const examId = response.data.data.id;
        const submission = await axios.get(
          process.env.API_BE + `/api/class-exam/${examId}/submission`,
          {
            headers: {
              Authorization: `Bearer ${sessionToken}`,
            },
          },
        );
        if (type === "uts") {
          queryClient.setQueryData(["subExamUts"], submission.data.data);
        } else {
          queryClient.setQueryData(["subExamUas"], submission.data.data);
        }
      }

      if (type === "uts") {
        queryClient.setQueryData(["examUts"], response.data.data);
      } else {
        queryClient.setQueryData(["examUas"], response.data.data);
      }
    } catch (error) {
      toast.error("Gagal mengambil data info Ujian .", error);
    } finally {
      setIsloadingModal(false);
    }
  };

  const getInfoClass = async (classId: number) => {
    setModalType("Info Kelas");
    setSideType("Info Kelas");
    setHeading(lang.classInfo);
    if (isLG) {
      setIsloadingModal(true);
      setModalClass(true);
    } else {
      setSidebar(true);
      setIsloadingSidebar(true);
    }
    try {
      const response = await axios.get(
        process.env.API_BE + `/api/class/${classId}`,
        {
          headers: {
            Authorization: `Bearer ${sessionToken}`,
          },
        },
      );
      queryClient.setQueryData(["infoClass"], response.data.data);
    } catch (error) {
      console.error("Gagal mengambil data info kelas.", error);
    } finally {
      setIsloadingModal(false);
      setIsloadingSidebar(false);
    }
  };

  const getInfoMembers = async (classId: number) => {
    setModalType("Member");
    setSideType("Member");
    setHeading(lang.members);

    if (isLG) {
      setIsloadingModal(true);
      setModalClass(true);
    } else {
      setSidebar(true);
      setIsloadingSidebar(true);
    }

    try {
      const response = await axios.get(
        process.env.API_BE + `/api/class/${classId}/members`,
        {
          headers: {
            Authorization: `Bearer ${sessionToken}`,
          },
        },
      );
      queryClient.setQueryData(["members"], response.data.data.items);
    } catch (error) {
      toast.error("Gagal mengambil data member.", error);
    } finally {
      setIsloadingModal(false);
      setIsloadingSidebar(false);
    }
  };

  const getMaterial = async (materialId: number) => {
    setIsloadingModal(true);
    setModalType("Material");
    setModalClass(true);

    try {
      const response = await axios.get(
        process.env.API_BE + `/api/class-material/${materialId}`,
        {
          headers: {
            Authorization: `Bearer ${sessionToken}`,
          },
        },
      );
      queryClient.setQueryData(["material"], response.data.data);
    } catch (error) {
      toast.error("Gagal mengambil data materi.", error);
    } finally {
      setIsloadingModal(false);
    }
  };

  const getAllMaterial = async (classId: number) => {
    if (isLG) {
      setModalType("All Material");
      setIsloadingModal(true);
      setModalClass(true);
    } else {
      setSideType("All Material");
      setHeading(lang.material);
      setSidebar(true);
      setIsloadingSidebar(true);
    }

    try {
      const response = await axios.get(
        process.env.API_BE + `/api/class/${classId}/material`,
        {
          headers: {
            Authorization: `Bearer ${sessionToken}`,
          },
        },
      );
      queryClient.setQueryData(["allMaterial"], response.data.data.items);
    } catch (error) {
      toast.error("Gagal mengambil data materi.", error);
    } finally {
      setIsloadingModal(false);
      setIsloadingSidebar(false);
    }
  };

  const getAllAssignment = async (classId: number) => {
    if (isLG) {
      setModalType("All Assignment");
      setIsloadingModal(true);
      setModalClass(true);
    } else {
      setSideType("All Assignment");
      setHeading(lang.assignments);
      setSidebar(true);
      setIsloadingSidebar(true);
    }

    try {
      const response = await axios.get(
        process.env.API_BE + `/api/class/${classId}/assignment`,
        {
          headers: {
            Authorization: `Bearer ${sessionToken}`,
          },
        },
      );
      queryClient.setQueryData(["allAssignment"], response.data.data.items);
    } catch (error) {
      toast.error("Gagal mengambil data tugas.", error);
    } finally {
      setIsloadingModal(false);
      setIsloadingSidebar(false);
    }
  };

  const singleSubAssign = async (assignmentId: number) => {
    setModalType("Detail Tugas");
    setIsloadingModal(true);
    setModalClass(true);
    try {
      const submission = await axios.get(
        process.env.API_BE + `/api/class-assignment/${assignmentId}/submission`,
        {
          headers: {
            Authorization: `Bearer ${sessionToken}`,
          },
        },
      );
      console.log(submission.data.data);
      queryClient.setQueryData(["subAssignment"], submission.data.data);
      if (submission.data.data.length === 0) {
        getAssignment(assignmentId);
      }
    } catch (error) {
      toast.error("Gagal mengambil data tugas.", error);
    } finally {
      setIsloadingModal(false);
    }
  };

  const singleSubQuiz = async (assignmentId: number) => {
    setModalType("Detail Kuis");
    setIsloadingModal(true);
    setModalClass(true);
    try {
      const submission = await axios.get(
        process.env.API_BE + `/api/class-quiz/${assignmentId}/submission`,
        {
          headers: {
            Authorization: `Bearer ${sessionToken}`,
          },
        },
      );
      queryClient.setQueryData(["subQuiz"], submission.data.data);
      if (submission.data.data.length === 0) {
        getQuiz(assignmentId);
      }
    } catch (error) {
      toast.error("Gagal mengambil data quiz.", error);
    } finally {
      setIsloadingModal(false);
    }
  };

  const getAssignment = async (assignmentId: number) => {
    setModalType("Tugas");
    setIsloadingModal(true);
    setModalClass(true);
    try {
      const response = await axios.get(
        process.env.API_BE + `/api/class-assignment/${assignmentId}`,
        {
          headers: {
            Authorization: `Bearer ${sessionToken}`,
          },
        },
      );
      const submission = await axios.get(
        process.env.API_BE + `/api/class-assignment/${assignmentId}/submission`,
        {
          headers: {
            Authorization: `Bearer ${sessionToken}`,
          },
        },
      );
      queryClient.setQueryData(["subAssignment"], submission.data.data);
      queryClient.setQueryData(["assignment"], response.data.data);
    } catch (error) {
      toast.error("Gagal mengambil data tugas.", error);
    } finally {
      setIsloadingModal(false);
    }
  };

  const getAllQuiz = async (classId: number) => {
    if (isLG) {
      setModalType("All Quiz");
      setIsloadingModal(true);
      setModalClass(true);
    } else {
      setSideType("All Quiz");
      setHeading(lang.quiz);
      setSidebar(true);
      setIsloadingSidebar(true);
    }

    try {
      const response = await axios.get(
        process.env.API_BE + `/api/class/${classId}/quiz`,
        {
          headers: {
            Authorization: `Bearer ${sessionToken}`,
          },
        },
      );
      queryClient.setQueryData(["allQuiz"], response.data.data.items);
    } catch (error) {
      toast.error("Gagal mengambil data kuis.", error);
    } finally {
      setIsloadingModal(false);
      setIsloadingSidebar(false);
    }
  };

  const getQuiz = async (quizId: number) => {
    setModalType("Kuis");
    setIsloadingModal(true);
    setModalClass(true);
    try {
      const response = await axios.get(
        process.env.API_BE + `/api/class-quiz/${quizId}`,
        {
          headers: {
            Authorization: `Bearer ${sessionToken}`,
          },
        },
      );
      const submission = await axios.get(
        process.env.API_BE + `/api/class-quiz/${quizId}/submission`,
        {
          headers: {
            Authorization: `Bearer ${sessionToken}`,
          },
        },
      );
      queryClient.setQueryData(["subQuiz"], submission.data.data);
      // console.log(response.data.data);
      queryClient.setQueryData(["quiz"], response.data.data);
    } catch (error) {
      toast.error("Gagal mengambil data quiz.", error);
    } finally {
      setIsloadingModal(false);
    }
  };

  const { data: dataUjianUts } = useQuery<any>(["examUts"], {
    enabled: false,
  });
  const { data: dataUjianUas } = useQuery<any>(["examUas"], {
    enabled: false,
  });
  const { data: subExamUts } = useQuery<any>(["subExamUts"], {
    enabled: false,
  });
  const { data: subExamUas } = useQuery<any>(["subExamUas"], {
    enabled: false,
  });
  const { data: dataInfoKelas } = useQuery<any>(["infoClass"], {
    enabled: false,
  });
  const { data: dataMember } = useQuery<any>(["members"], {
    enabled: false,
  });
  const { data: material } = useQuery<any>(["material"], {
    enabled: false,
  });
  const { data: allMaterial } = useQuery<any>(["allMaterial"], {
    enabled: false,
  });
  const { data: allAssignment } = useQuery<any>(["allAssignment"], {
    enabled: false,
  });
  const { data: allQuiz } = useQuery<any>(["allQuiz"], {
    enabled: false,
  });
  const { data: dataKuis } = useQuery<any>(["quiz"], {
    enabled: false,
  });
  const { data: subQuiz } = useQuery<any>(["subQuiz"], {
    enabled: false,
  });

  const { data: dataTugas } = useQuery<any>(["assignment"], {
    enabled: false,
  });
  const { data: subAssignment } = useQuery<any>(["subAssignment"], {
    enabled: false,
  });
  const { data: attendanceClass } = useQuery<any>(["attendanceClass"], {
    enabled: false,
    cacheTime: 0,
    retry: 2,
  });

  interface ToggleFavoriteResponse extends ApiResponse {}

  const addFriend = useMutation(
    async (userId: number): Promise<ToggleFavoriteResponse> => {
      if (!sessionToken) throw new Error("Missing session token");
      try {
        const response = await axios.post<ToggleFavoriteResponse>(
          process.env.API_BE + `/api/friend/add`,
          {
            id: Number(userId),
          },
          {
            headers: {
              Authorization: `Bearer ${sessionToken}`,
            },
          },
        );
        getInfoMembers(userChatAktif?.classId);
        toast.success(response.data.data.message);
        return response.data;
      } catch (error) {
        toast.error(error);
        throw error;
      }
    },
  );

  useEffect(() => {
    setSidebar(false);
  }, [messageHistory]);

  const handleAttend = useMutation(
    async (metadataId): Promise<PostResponse> => {
      if (!sessionToken) throw new Error("Missing session token");
      try {
        const response = await axios.post<PostResponse>(
          process.env.API_BE + `/api/class-attendance/${metadataId}/attend`,
          {},
          {
            headers: {
              Authorization: `Bearer ${sessionToken}`,
            },
          },
        );
        toast.success("Berhasil Hadir di sesi sekarang");
        return response.data;
      } catch (error) {
        toast.error(error);
        throw error;
      } finally {
        getAttendance();
      }
    },
    {
      onError: (error) =>
        toast.error(`Error: ${error.response.data.meta.message}`),
    },
  );

  const getAttendance = async () => {
    try {
      const classId = userChatAktif?.classId;
      const res = await axios.get(
        `${process.env.API_BE}/api/class/${classId}/attendance?withUserList=true`,
        {
          headers: {
            Authorization: `Bearer ${sessionToken}`,
          },
        },
      );
      return queryClient.setQueryData(["attendanceClass"], res.data.data.items);
    } catch (error) {
      toast.error("Gagal mengambil data Absen", error);
    }
  };

  function isWithinTimeRange(startTime, endTime) {
    const currentTime = new Date();
    const startDateTime = new Date(startTime);
    const endDateTime = new Date(endTime);

    return currentTime >= startDateTime && currentTime <= endDateTime;
  }

  // FOR GROUP
  const isAdmin = userChatAktif?.receiver?.id === userChatAktif?.sender?.id;

  const [_group, setGroup] = useState({
    profilePictureId: 0,
    name: "",
    description: "",
    userIdList: [],
  });

  const [modalGroup, setModalGroup] = useState(false);
  const [editGroup, setEditGroup] = useState(false);
  const [tempGroup, setTempGroup] = useState([]);
  const [displayGroup, setDisplayGroup] = useState([]);
  const [stepGroup, setStepGoup] = useState("2");
  const [isLoading, setIsloading] = useState(false);
  const [search, setSearch] = useState([]);
  const [isAdminGC, setIsAdminGC] = useState(false);

  const [alert3, setalert3] = useState(false);
  const [selectedImage, setSelectedImage] = useState(
    "/static/images/logokedi.svg",
  );

  const [selectedFileGroup, setSelectedFileGroup] = useState(null);
  const handleImageChange = (event) => {
    const file = event.target.files[0];

    if (file) {
      // Menggunakan URL.createObjectURL untuk membuat URL gambar dari file yang dipilih
      const imageUrl = URL.createObjectURL(file);
      setSelectedImage(imageUrl);
      setSelectedFileGroup(file);
    }
  };

  const fetchDataSearch = async () => {
    setIsloading(true);
    try {
      const response = await axios.get(
        process.env.API_BE + `/api/friend?limit=100&page=1`,
        {
          headers: {
            Authorization: `Bearer ${sessionToken}`,
          },
        },
      );

      setSearch(response.data.data.items);
      // queryClient.setQueryData(["queryResult"], response.data.data.items);
    } catch (error) {
      console.error("Gagal mengambil data pencarian.", error);
    } finally {
      setIsloading(false);
    }
  };

  const updateQuery = async (query: string) => {
    setIsloading(true);
    try {
      const response = await axios.get(
        process.env.API_BE + `/api/friend?query=${query}&limit=100&page=1`,
        {
          headers: {
            Authorization: `Bearer ${sessionToken}`,
          },
        },
      );

      setSearch(response.data.data.items);
      // return queryClient.setQueryData(
      //   ["queryResult"],
      //   response.data.data.items,
      // );
    } catch (error) {
      console.error("Gagal mengambil data pencarian.", error);
    } finally {
      setIsloading(false);
    }
  };

  const handleSubmit = async (e: React.FormEvent<HTMLFormElement>) => {
    e.preventDefault();
    const formData = new FormData(e.target as HTMLFormElement);
    const query = formData.get("query")?.toString().trim();
    console.log(query.length);
    if (query.length !== 0) {
      updateQuery(query);
    } else {
      setSearch([]);
      fetchDataSearch();
    }
  };

  const uploadFileClassG = (file: any, sessionToken: string) => {
    return new Promise((resolve, reject) => {
      const formData = new FormData();
      formData.append("file", file);
      formData.append("documentTypeId", "1");
      formData.append("isPrivate", "false");

      axios
        .post(process.env.API_BE + "/api/storage", formData, {
          headers: {
            "Content-Type": "multipart/form-data",
            Authorization: `Bearer ${sessionToken}`,
          },
        })
        .then((res) => {
          resolve(res.data.documentStorage.id);
        })
        .catch((err) => {
          reject(err.response.data.meta.message);
        });
    });
  };

  const handleUploadFilesG = async (file) => {
    if (!file) return 0;
    try {
      const responses = await uploadFileClassG(file, sessionToken ?? "");
      toast.success(lang.successUploadImg);
      console.log("Responses:", responses);
      return responses;
    } catch (error) {
      toast.error(lang.errorUpload);
      console.error("Error uploading files:", error);
    }
  };

  type PostNewGroup = {
    name: string;
    description: string;
    profilePictureId: number;
    userIdList: [];
  };

  const updateGroupChat = useMutation(
    async (data: PostNewGroup): Promise<PostResponse> => {
      const { name, description, userIdList } = data;
      if (!sessionToken) throw new Error("Missing session token");
      try {
        const uploading = await handleUploadFilesG(selectedFileGroup);
        const url = `${process.env.API_BE}/api/group-chat/${userChatAktif?.groupId}`;
        const response = await axios({
          method: "put",
          url: url,
          data: {
            name: String(name),
            profilePictureId: selectedFileGroup
              ? uploading
              : _group.profilePictureId,
            description: String(description),
            userIdList: userIdList,
          },
          headers: {
            Authorization: `Bearer ${sessionToken}`,
          },
        });
        return response.data;
      } catch (error) {
        console.error(error);
        throw error;
      }
    },
    {
      onSuccess: () => {
        toast.success(lang.successUpdatedGc);
        setGroup({
          profilePictureId: 0,
          name: "",
          description: "",
          userIdList: [],
        });
        setDisplayGroup([]);
        setStepGoup("2");
        setSelectedFileGroup(null);
        setSelectedImage("/static/images/logokedi.svg");
        setEditGroup(false);
        addData();
        setTimeout(() => {
          setUserChatAktif({});
        }, 500);
        if (isLG) {
          setModalGroup(false);
        } else {
          setSideType("");
          setIsloadingSidebar(false);
          setSidebar(false);
          setHeading("");
        }
      },
      onError: (error) =>
        toast.error(`Error: ${error.response?.data?.meta?.message ?? error}`),
    },
  );

  const inviteGroupChat = useMutation(
    async (data: PostNewGroup): Promise<PostResponse> => {
      const { userIdList, groupId } = data;
      if (!sessionToken) throw new Error("Missing session token");
      try {
        const url = `${process.env.API_BE}/api/group-chat/invite`;
        const response = await axios({
          method: "post",
          url: url,
          data: {
            userIdList: userIdList,
            groupId: groupId,
          },
          headers: {
            Authorization: `Bearer ${sessionToken}`,
          },
        });
        return response.data;
      } catch (error) {
        console.error(error);
        throw error;
      }
    },
    {
      onSuccess: () => {
        toast.success(lang.successAddedMem);
        getInfoGroup(userChatAktif?.groupId);
        setDisplayGroup([]);
        setTempGroup([]);
        setStepGoup("2");
        setTimeout(() => {
          getMessageUser({ channel: userChatAktif?.channel, isDelete: true });
        }, 500);
      },
      onError: (error) =>
        toast.error(`Error: ${error.response?.data?.meta?.message ?? error}`),
    },
  );

  const givePrevGroupChat = useMutation(
    async (data: PostNewGroup): Promise<PostResponse> => {
      const { groupId, userId, type } = data;
      if (!sessionToken) throw new Error("Missing session token");
      try {
        const url = `${process.env.API_BE}/api/group-chat/${groupId}/admin/${userId}`;
        const response = await axios({
          method: type,
          url: url,
          data: {},
          headers: {
            Authorization: `Bearer ${sessionToken}`,
          },
        });
        return response.data;
      } catch (error) {
        console.error(error);
        throw error;
      }
    },
    {
      onSuccess: (data) => {
        if (data) {
          const isTrue = data.data.isAdmin;
          if (isTrue) {
            toast.success(lang.successGrantPrivilege);
          } else {
            toast.success(lang.successDeletePrivilege);
          }
        }
        getInfoGroup(userChatAktif?.groupId);
      },
      onError: (error) =>
        toast.error(`Error: ${error.response?.data?.meta?.message ?? error}`),
    },
  );

  const deleteMemberGroupChat = useMutation(
    async (data: PostNewGroup): Promise<PostResponse> => {
      const { groupId, userId, type } = data;
      if (!sessionToken) throw new Error("Missing session token");
      try {
        const url = `${process.env.API_BE}/api/group-chat/${groupId}/member/${userId}`;
        const response = await axios({
          method: type,
          url: url,
          data: {},
          headers: {
            Authorization: `Bearer ${sessionToken}`,
          },
        });
        return response.data;
      } catch (error) {
        console.error(error);
        throw error;
      }
    },
    {
      onSuccess: (data) => {
        console.log(data);
        toast.success(lang.successDeleteMember);
        getInfoGroup(userChatAktif?.groupId);
        setTimeout(() => {
          getMessageUser({ channel: userChatAktif?.channel, isDelete: true });
        }, 500);
      },
      onError: (error) =>
        toast.error(`Error: ${error.response?.data?.meta?.message ?? error}`),
    },
  );

  const setallClassMessage = (value) => {
    useUserChat.setState({
      allClassMessage: value,
    });
  };
  const setPrivateMessage = (value) => {
    useUserChat.setState({
      privateMessage: value,
    });
  };
  const setGroupChatMessage = (value) => {
    useUserChat.setState({
      groupChatMessage: value,
    });
  };

  const leaveGroupChat = useMutation(
    async (data: PostNewGroup): Promise<PostResponse> => {
      const { groupId, type } = data;
      if (!sessionToken) throw new Error("Missing session token");
      try {
        const url = `${process.env.API_BE}/api/group-chat/${groupId}/leave`;
        const response = await axios({
          method: type,
          url: url,
          data: {},
          headers: {
            Authorization: `Bearer ${sessionToken}`,
          },
        });
        return response.data;
      } catch (error) {
        console.error(error);
        throw error;
      }
    },
    {
      onSuccess: (data) => {
        const path = data?.meta?.path;
        setAlertGroup(false);
        const match = path.match(/\/api\/group-chat\/(\d+)\/leave/);

        // Jika ekstraksi berhasil, ambil hasilnya
        if (match) {
          (async () => {
            toast.success(lang.successExitGroup);
            const msg = await getPubNub(user);

            //localStorage.setItem("lastTab", "channelActiveGroup");
            router.push("/class");
            useUserChat.setState({
              userChatAktif: {},
            });
          })();
        } else {
          console.log("Tidak ada data yang cocok");
        }
      },
      onError: (error) =>
        toast.error(`Error: ${error.response?.data?.meta?.message ?? error}`),
    },
  );

  interface DeleteResponse extends Response {}

  const deleteGroup = useMutation(
    async (id): Promise<DeleteResponse> => {
      if (!sessionToken) throw new Error("Missing session token");
      try {
        const response = await axios.delete<DeleteResponse>(
          process.env.API_BE + `/api/group-chat/${id}`,
          {
            headers: {
              Authorization: `Bearer ${sessionToken}`,
            },
          },
        );
        toast.success(response?.data?.data?.message);
        setalert3(false);
        return response.data;
      } catch (error) {
        console.error(error);
        throw error;
      }
    },

    {
      onSuccess: (data) => {
        console.log(data);
        const path = data?.meta?.path;
        setAlertGroup(false);
        const match = path.match(/\/api\/group-chat\/(\d+)/);

        // Jika ekstraksi berhasil, ambil hasilnya
        if (match) {
          const groupChatData = `chat-group.${match[1]}`;
          console.log("groupChatData:", groupChatData);
          removeItem(groupChatData);
          setUserChatAktif({});
          // setIsMobileView(false);
          router.push("/class");
        } else {
          console.log("Tidak ada data yang cocok");
        }
        toast.success(lang.successExitGroup);
      },
      onError: (error) =>
        toast.error(`Error: ${error.response?.data?.meta?.message ?? error}`),
    },
  );

  const getInfoGroup = async (id: number) => {
    if (isLG) {
      setModalGroup(true);
      setIsloading(true);
    } else {
      setSideType("Group");
      setIsloadingSidebar(true);
      setSidebar(true);
      setHeading("Group Info");
    }
    try {
      const response = await axios.get(
        process.env.API_BE + `/api/group-chat/${id}`,
        {
          headers: {
            Authorization: `Bearer ${sessionToken}`,
          },
        },
      );
      queryClient.setQueryData(["infoGroup"], response.data.data);

      const data = response.data.data;
      if (data?.groupChat?.profilePictureLink) {
        setSelectedImage(data?.groupChat?.profilePictureLink);
      }

      if (data?.groupChat) {
        const member = data?.groupChat?.groupMember?.map((d) => {
          return d.user.id;
        });

        data?.groupChat?.groupMember?.map((d) => {
          if (d?.user?.id === user?.id) {
            setIsAdminGC(d.isAdmin);
          }
        });

        const idPic = data?.groupChat?.profilePicture?.id;
        setGroup({
          name: data?.groupChat?.name,
          description: data?.groupChat?.description,
          userIdList: member,
          profilePictureId: idPic ? parseInt(idPic) : 0,
        });
      }
    } catch (error) {
      if (
        error?.response?.data?.meta?.message === "User bukan member dari grup"
      ) {
        toast.error(error.response.data.meta.message);
        setModalGroup(false);
      } else {
        console.error("Gagal mengambil data", error);
      }
    } finally {
      setIsloading(false);
      setIsloadingSidebar(false);
    }
  };

  const { data: infoGroup } = useQuery<any>(["infoGroup"], {
    enabled: false,
  });

  // console.log(isAdminGC &&!isAdmin)
  const isOwnerAndAdmin = isAdmin && isAdminGC;
  const isAdminOnly = !isAdmin && isAdminGC;
  const [alertGroup, setAlertGroup] = useState(false);
  // FOR GROUP

  return (
    <>
      {userChatAktif?.channel && (
        <>
          <div
            style={{ height: viewportHeight }}
            className={` relative flex-1  justify-between flex flex-col overflow-hidden  min-w-[350px]  lg:col-span-2 [@media(min-width:900px)]:mr-2.5 ${isMobileView ? "" : "pb-[3.7rem] md:pb-0 sm:pt-[4.8rem] pt-[4rem]"}  `}
          >
            <ChatBox
              getMessageUser={getMessageUser}
              viewportHeight={viewportHeight}
              inputText={inputText}
              setInputText={setInputText}
              isHeight={isHeight}
              inputTextRef={inputTextRef}
              setIsMobileView={setIsMobileView}
              lang={lang}
              userChatAktif={userChatAktif}
              messageHistory={messageHistory}
              todoListRef={todoListRef}
              // setMessage={setMessage}
              sendMessage={sendMessage}
              // message={message}
              chatMode={chatMode}
              removeItem={removeItem}
              addDataPrivate={addDataPrivate}
              formatBytes={formatBytes}
              handleButtonClick={handleButtonClick}
              handleFileChange={handleFileChange}
              sendFile={sendFile}
              handleDelete={handleDelete}
              handleDragOver={handleDragOver}
              handleDragEnter={handleDragEnter}
              handleDrop={handleDrop}
              handleDragLeave={handleDragLeave}
              selectedFile={selectedFile}
              isLoadingFile={isLoadingFile}
              isSentMessage={isSentMessage}
              modalType={modalType}
              setModalType={setModalType}
              getExam={getExam}
              dataUjianUts={dataUjianUts}
              dataUjianUas={dataUjianUas}
              subExamUts={subExamUts}
              subExamUas={subExamUas}
              getInfoClass={getInfoClass}
              dataInfoKelas={dataInfoKelas}
              getInfoMembers={getInfoMembers}
              dataMember={dataMember}
              getMaterial={getMaterial}
              getAllMaterial={getAllMaterial}
              getAllAssignment={getAllAssignment}
              getAllQuiz={getAllQuiz}
              allMaterial={allMaterial}
              allQuiz={allQuiz}
              allAssignment={allAssignment}
              material={material}
              getQuiz={getQuiz}
              getAssignment={getAssignment}
              singleSubAssign={singleSubAssign}
              singleSubQuiz={singleSubQuiz}
              dataKuis={dataKuis}
              subQuiz={subQuiz}
              dataTugas={dataTugas}
              subAssignment={subAssignment}
              addFriend={addFriend}
              modalClass={modalClass}
              isLoadingModal={isLoadingModal}
              setIsloadingModal={setIsloadingModal}
              setModalClass={setModalClass}
              setHeading={setHeading}
              setSideType={setSideType}
              setSidebar={setSidebar}
              inputFileRef={inputFileRef}
              multiMessage={multiMessage}
              setMultiMessage={setMultiMessage}
              deleteMessage={deleteMessage}
              tempQuizId={tempQuizId}
              settempQuizId={settempQuizId}
              getInfoGroup={getInfoGroup}
              fetchDataSearch={fetchDataSearch}
              setalert3={setalert3}
              setAlertGroup={setAlertGroup}
            />
          </div>
          {sidebar && (
            <div
              style={{ height: viewportHeight }}
              className="relative lg:flex w-full hidden max-w-[370px] lg:pb-0 pt-[5.2rem] className  flex-col justify-between shadow-md px-2.5"
            >
              {(isLoadingSidebar ||
                handleAttend.isLoading ||
                updateGroupChat.isLoading) && (
                <div className="absolute flex justify-center items-center w-full max-w-[360px] h-[80vh] bg-white/20 backdrop-blur-[1px] z-10">
                  <LoadingSpinner size={70} strokeWidth={2} />
                </div>
              )}

              <svg
                onClick={() => setSidebar(false)}
                width={32}
                height={32}
                viewBox="0 0 32 32"
                fill="none"
                xmlns="http://www.w3.org/2000/svg"
                className="absolute top-24 text-gray-500 right-4 h-[24px] w-[24px] cursor-pointer"
              >
                <g clipPath="url(#clip0_63_6312)">
                  <path
                    d="M19.4534 10.6667L16 14.1201L12.5467 10.6667L10.6667 12.5467L14.12 16.0001L10.6667 19.4534L12.5467 21.3334L16 17.8801L19.4534 21.3334L21.3334 19.4534L17.88 16.0001L21.3334 12.5467L19.4534 10.6667ZM16 2.66675C8.62669 2.66675 2.66669 8.62675 2.66669 16.0001C2.66669 23.3734 8.62669 29.3334 16 29.3334C23.3734 29.3334 29.3334 23.3734 29.3334 16.0001C29.3334 8.62675 23.3734 2.66675 16 2.66675ZM16 26.6667C10.12 26.6667 5.33335 21.8801 5.33335 16.0001C5.33335 10.1201 10.12 5.33341 16 5.33341C21.88 5.33341 26.6667 10.1201 26.6667 16.0001C26.6667 21.8801 21.88 26.6667 16 26.6667Z"
                    fill="currentColor"
                  />
                </g>
                <defs>
                  <clipPath id="clip0_63_6312">
                    <rect width={32} height={32} fill="white" />
                  </clipPath>
                </defs>
              </svg>
              {/*Content*/}
              <div
                className={`px-4 flex sm:items-center justify-between py-3   font-semibold  text-lg  `}
              >
                {heading}
              </div>
              {sideType === "Absensi" &&
              attendanceClass &&
              userChatAktif.receiver.id === userChatAktif.sender.id ? (
                <div className="flex-1 mt-6 px-4 overflow-y-auto">
                  <Tabs defaultValue="dosen" className="w-full">
                    <TabsList className="grid grid-cols-2 mb-2.5">
                      <TabsTrigger value="dosen">{lang.lecturer}</TabsTrigger>
                      <TabsTrigger value="mahasiswa">
                        {lang.student}
                      </TabsTrigger>
                    </TabsList>
                    <TabsContent value="dosen" className="pb-5">
                      {attendanceClass?.map((d, index) => {
                        const filterAttend =
                          attendanceClass &&
                          attendanceClass[index].userList?.map((d) => {
                            const endAttendTime =
                              attendanceClass[index]?.class?.endAttendanceTime;
                            const userAttendTime = d?.attendanceDate;
                            const checkLate = compareTimes(
                              userAttendTime,
                              endAttendTime,
                            );

                            let obj = {
                              ...d,
                              isLate: checkLate,
                            };
                            return obj;
                          });

                        const isShowBtn = isWithinTimeRange(
                          d.startTime,
                          d.endTime,
                        );

                        const filter = filterAttend?.filter(
                          (u) => u.userId === user.id,
                        );
                        return (
                          <div key={d.id}>
                            <div className="space-y-2">
                              {filter && filter.length > 0 ? (
                                filter?.map((d: any) => {
                                  const isLate = d.isLate == 2 ? true : false;
                                  const checkLate = isLate
                                    ? "text-yellow-600"
                                    : "text-[#9D21E6]";
                                  return (
                                    <div
                                      key={d.id}
                                      className={` ${checkLate}  ${
                                        isLate
                                          ? "border-yellow-600"
                                          : "border-[#9D21E6]"
                                      } px-2.5 mb-3 mt-5  border-b-2 border-dashed pb-2.5 text-sm flex items-center justify-between `}
                                    >
                                      <div>
                                        <p>
                                          {formatDate(d.attendanceDate)} {"  "}
                                        </p>
                                        <p className="font-semibold pt-2">
                                          {lang.session} {index + 1}
                                        </p>
                                      </div>
                                      <div className="space-y-1">
                                        <p>Jam</p>
                                        <p className="font-semibold">
                                          {formatToTime(d.attendanceDate)}
                                        </p>
                                      </div>
                                    </div>
                                  );
                                })
                              ) : (
                                <li
                                  className={`  flex items-center justify-between border-b-2 border-dashed  border-gray-500 px-2.5 py-3.5 `}
                                >
                                  <div className="flex flex-col text-sm gap-x-2.5 text-gray-600">
                                    <p className="font-semibold mb-1">
                                      {lang.session} {index + 1}
                                    </p>
                                    <div className="font-semibold"></div>
                                  </div>

                                  <div className="space-y-1 flex flex-col items-center">
                                    {!isShowBtn ? (
                                      <span className="text-sm font-bold">
                                        -
                                      </span>
                                    ) : (
                                      <button
                                        onClick={() =>
                                          handleAttend.mutate(d.id)
                                        }
                                        className={`text-sm bg-red-600 text-white px-4 py-2 rounded-[10px] text-sm`}
                                      >
                                        {lang.notYetPresent}
                                      </button>
                                    )}
                                  </div>
                                </li>
                              )}
                            </div>
                          </div>
                        );
                      })}
                    </TabsContent>
                    <TabsContent value="mahasiswa">
                      {attendanceClass?.map((d, index) => {
                        const filterAttend =
                          attendanceClass &&
                          attendanceClass[index].userList?.map((d) => {
                            const endAttendTime =
                              attendanceClass[index]?.class?.endAttendanceTime;
                            const userAttendTime = d?.attendanceDate;
                            const checkLate = compareTimes(
                              userAttendTime,
                              endAttendTime,
                            );

                            let obj = {
                              ...d,
                              isLate: checkLate,
                            };
                            return obj;
                          });
                        const filterNon = filterAttend?.filter(
                          (u) => u.userId !== user?.id,
                        );

                        const isNotEmpty = filterNon && filterNon.length > 0;
                        const isShow = isEndTimePassed(d.startTime);
                        // const hasPassed =
                        //   tempPresent && isStartTimeBeforeNow(d.startTime);
                        // const isDisabled = isEndTimePassed(d.endTime);
                        return (
                          <div
                            key={d.id}
                            className={` ${
                              isShow ? "" : ""
                            } flex items-center justify-between mb-3 mt-5`}
                          >
                            <ul className="space-y-1 w-full ">
                              <li>
                                <details className="group [&_summary::-webkit-details-marker]:hidden">
                                  <summary className="border-b-2 border-dashed flex cursor-pointer items-center justify-between p-2">
                                    <span
                                      className={` ${
                                        isNotEmpty
                                          ? "text-[#9D21E6]"
                                          : "text-gray-700"
                                      } text-sm font-medium`}
                                    >
                                      {lang.session} {index + 1}{" "}
                                    </span>

                                    <span className="shrink-0 transition duration-300 group-open:-rotate-180">
                                      <svg
                                        xmlns="http://www.w3.org/2000/svg"
                                        className="h-5 w-5"
                                        viewBox="0 0 20 20"
                                        fill="currentColor"
                                      >
                                        <path
                                          fillRule="evenodd"
                                          d="M5.293 7.293a1 1 0 011.414 0L10 10.586l3.293-3.293a1 1 0 111.414 1.414l-4 4a1 1 0 01-1.414 0l-4-4a1 1 0 010-1.414z"
                                          clipRule="evenodd"
                                        />
                                      </svg>
                                    </span>
                                  </summary>

                                  <ul className="space-y-1 p-3">
                                    {filterNon && filterNon.length > 0 ? (
                                      filterNon?.map((d: any) => {
                                        const isLate =
                                          d.isLate == 2 ? true : false;
                                        return (
                                          <li
                                            key={d.id}
                                            className={` ${
                                              isLate
                                                ? "border-yellow-600"
                                                : "border-[#9D21E6]"
                                            } flex items-center justify-between border-b-2 border-dashed  px-2.5 py-2.5`}
                                          >
                                            <div
                                              className={` ${
                                                isLate
                                                  ? "text-yellow-600"
                                                  : "text-[#9D21E6]"
                                              } flex flex-col text-sm gap-x-2.5 `}
                                            >
                                              <div>{d.fullName}</div>
                                              <div className="font-semibold">
                                                {
                                                  formatTimeAndDate(
                                                    d.attendanceDate,
                                                  ).date
                                                }
                                              </div>
                                            </div>

                                            <div
                                              className={` ${
                                                isLate
                                                  ? "text-yellow-600"
                                                  : "text-[#9D21E6]"
                                              } text-[#9D21E6] space-y-1 flex flex-col items-center `}
                                            >
                                              <p className="text-sm">Jam</p>
                                              <span className="text-sm font-bold">
                                                {formatToTime(d.attendanceDate)}
                                              </span>
                                            </div>
                                          </li>
                                        );
                                      })
                                    ) : (
                                      <div className="opacity-70 h-full flex items-center justify-center">
                                        {lang.dataNotFound}
                                      </div>
                                    )}
                                  </ul>
                                </details>
                              </li>
                            </ul>
                          </div>
                        );
                      })}
                    </TabsContent>
                  </Tabs>
                </div>
              ) : (
                sideType === "Absensi" && (
                  <div className="flex-1 px-4 overflow-y-auto pb-10">
                    <ul className="space-y-1 w-full ">
                      <li>
                        <details
                          className="group [&_summary::-webkit-details-marker]:hidden"
                          open={true}
                        >
                          <summary
                            className={` group-open:border-b flex  group-open:border-gray-400 cursor-pointer items-center justify-between p-3`}
                          >
                            <div className="flex items-start gap-x-5">
                              <div className="flex-none relative w-14 h-14">
                                <Image
                                  width={60}
                                  height={60}
                                  src={
                                    attendanceClass[0]?.class?.lecturer
                                      ?.profilePicture?.url ||
                                    `/static/images/logokedi.svg`
                                  }
                                  className="absolute inset-0 h-full w-full object-cover rounded-xl shadow-md"
                                  alt=""
                                />
                              </div>
                              <div className="flex flex-col flex-1">
                                <div className="text-[14px] line-clamp-1">
                                  {
                                    attendanceClass[0]?.class?.lecturer
                                      ?.fullName
                                  }
                                </div>
                                <div className="text-[14px] font-semibold line-clamp-1">
                                  {attendanceClass[0]?.class?.name}
                                </div>
                                <div className="text-[14px] opacity-70 ">
                                  <p>
                                    {attendanceClass[0]?.class?.description}{" "}
                                  </p>
                                  <p>
                                    {formatToTime(
                                      attendanceClass &&
                                        attendanceClass[0]?.class
                                          ?.startAttendanceTime,
                                    )}{" "}
                                    -{" "}
                                    {formatToTime(
                                      attendanceClass &&
                                        attendanceClass[0]?.class
                                          ?.endAttendanceTime,
                                    )}
                                  </p>
                                </div>
                              </div>
                            </div>
                            <span className="shrink-0 transition duration-300 group-open:-rotate-180">
                              <svg
                                xmlns="http://www.w3.org/2000/svg"
                                className="h-5 w-5"
                                viewBox="0 0 20 20"
                                fill="currentColor"
                              >
                                <path
                                  fillRule="evenodd"
                                  d="M5.293 7.293a1 1 0 011.414 0L10 10.586l3.293-3.293a1 1 0 111.414 1.414l-4 4a1 1 0 01-1.414 0l-4-4a1 1 0 010-1.414z"
                                  clipRule="evenodd"
                                />
                              </svg>
                            </span>
                          </summary>
                          <ul className="space-y-1">
                            {attendanceClass?.map((d, index) => {
                              const filterAttend =
                                attendanceClass &&
                                attendanceClass[index].userList?.map((d) => {
                                  const endAttendTime =
                                    attendanceClass[index]?.class
                                      ?.endAttendanceTime;
                                  const userAttendTime = d?.attendanceDate;
                                  const checkLate = compareTimes(
                                    userAttendTime,
                                    endAttendTime,
                                  );

                                  let obj = {
                                    ...d,
                                    isLate: checkLate,
                                  };
                                  return obj;
                                });

                              const isShowBtn = isWithinTimeRange(
                                d.startTime,
                                d.endTime,
                              );
                              const filter = filterAttend.filter(
                                (u) => u.userId === user?.id,
                              );

                              return (
                                <div key={d.id} className="space-y-2">
                                  {filter && filter.length > 0 ? (
                                    filter?.map((item: any) => {
                                      const isLate =
                                        item.isLate == 2 ? true : false;
                                      const checkLate = isLate
                                        ? "text-yellow-600"
                                        : "text-[#9D21E6]";
                                      return (
                                        <li
                                          key={item.id}
                                          className={` ${checkLate} flex items-center justify-between border-b-2 border-dashed  border-gray-500 px-2.5 py-3.5 `}
                                        >
                                          <div className="flex flex-col text-sm gap-x-2.5">
                                            <p className="font-semibold mb-1">
                                              {lang.session} {index + 1}
                                            </p>
                                            <p>
                                              {formatDate(item.attendanceDate)}
                                            </p>
                                            <div className="font-semibold"></div>
                                          </div>
                                          <div className="space-y-1 flex flex-col items-center">
                                            <span className="text-sm font-bold">
                                              {formatToTime(
                                                item.attendanceDate,
                                              )}
                                            </span>
                                          </div>
                                        </li>
                                      );
                                    })
                                  ) : (
                                    <li
                                      className={`  flex items-center justify-between border-b-2 border-dashed  border-gray-500 px-2.5 py-3.5 `}
                                    >
                                      <div className="flex flex-col text-sm gap-x-2.5 text-gray-600">
                                        <p className="font-semibold mb-1">
                                          {lang.session} {index + 1}
                                        </p>
                                        <div className="font-semibold"></div>
                                      </div>
                                      <div className="space-y-1 flex flex-col items-center">
                                        {!isShowBtn ? (
                                          <span className="text-sm font-bold">
                                            -
                                          </span>
                                        ) : (
                                          <button
                                            onClick={() =>
                                              handleAttend.mutate(d.id)
                                            }
                                            className={`text-sm bg-red-600 text-white px-4 py-2 rounded-[10px] text-sm`}
                                          >
                                            {lang.notYetPresent}
                                          </button>
                                        )}
                                      </div>
                                    </li>
                                  )}
                                </div>
                              );
                            })}
                          </ul>
                        </details>
                      </li>
                    </ul>
                  </div>
                )
              )}
              {sideType === "Info Kelas" && dataInfoKelas && (
                <InfoClass
                  lang={lang}
                  dataInfoKelas={dataInfoKelas}
                  attendanceClass={attendanceClass}
                  userChatAktif={userChatAktif}
                  sessionToken={sessionToken}
                />
              )}
              {sideType === "Member" && dataMember && (
                <InfoMembers
                  lang={lang}
                  dataMember={dataMember}
                  user={user}
                  userChatAktif={userChatAktif}
                  getInfoMembers={getInfoMembers}
                />
              )}
              {sideType === "All Material" && allMaterial && (
                <div className="flex-1 overflow-y-auto pt-2.5">
                  {allMaterial?.length === 0 ? (
                    <div className="flex items-center justify-center h-full">
                      {lang.noMaterial}
                    </div>
                  ) : (
                    allMaterial?.map((x: any) => (
                      <div onClick={() => getMaterial(x.id)} key={x.id}>
                        <div className="w-full my-2  h-fit  border border-gray-200 py-2 px-4 bg-white rounded-xl">
                          <label>{x.title}</label>

                          <h1 className="text-black font-bold">
                            {lang.by}: {x.lecturer.fullName}
                          </h1>

                          <p className="text-gray-600 mt-2 text-[0.775rem]   ">
                            {formatTanggal(x.createdDate)}
                          </p>
                        </div>
                      </div>
                    ))
                  )}{" "}
                </div>
              )}
              {sideType === "All Assignment" && allAssignment && (
                <div className="flex-1 overflow-y-auto">
                  {allAssignment?.length === 0 ? (
                    <div className="flex items-center justify-center h-full">
                      {lang.noAssignments}
                    </div>
                  ) : (
                    allAssignment?.map((x: any) => (
                      <div
                        onClick={() => {
                          const isAdmin =
                            userChatAktif?.receiver.id ===
                            userChatAktif?.sender.id;
                          if (isAdmin) {
                            getAssignment(x.id);
                            settempQuizId(x.id);
                          } else {
                            singleSubAssign(x.id);
                          }
                        }}
                        key={x.id}
                        className="cursor-pointer"
                      >
                        <div className="w-full my-2  h-fit  border border-gray-200 py-2 px-4 bg-white rounded-xl">
                          <label>{x.name}</label>

                          <h1 className="text-black font-bold">
                            {lang.deadline}
                          </h1>
                          <div className="flex items-center text-sm gap-x-2.5">
                            <p className="text-gray-600 text-md">
                              {formatTanggal(x.dueDate)}
                            </p>
                            {getTaskStatus(x.dueDate)}
                          </div>
                          <div className="w-full mt-2 flex justify-center"></div>
                        </div>
                      </div>
                    ))
                  )}
                </div>
              )}
              {sideType === "All Quiz" && allQuiz && (
                <div className="flex-1 overflow-y-auto">
                  {allQuiz?.length === 0 ? (
                    <div className="flex items-center justify-center h-full">
                      {lang.noQuizzes}
                    </div>
                  ) : (
                    allQuiz?.map((x: any) => (
                      <div
                        onClick={() => {
                          const isAdmin =
                            userChatAktif?.receiver.id ===
                            userChatAktif?.sender.id;
                          if (isAdmin) {
                            getQuiz(x.id);
                            settempQuizId(x.id);
                          } else {
                            singleSubQuiz(x.id);
                          }
                        }}
                        key={x.id}
                        className="cursor-pointer"
                      >
                        <div className="w-full my-2  h-fit  border border-gray-200 py-2 px-4 bg-white rounded-xl">
                          <label>{x.name}</label>
                          <h1 className="text-black font-bold">
                            {lang.deadline}
                          </h1>
                          <div className="flex items-center text-sm gap-x-2.5">
                            <p className="text-gray-600 text-md">
                              {formatTanggal(x.dueDate)}
                            </p>
                            {getTaskStatus(x.dueDate)}
                          </div>
                        </div>
                      </div>
                    ))
                  )}
                </div>
              )}
              {sideType === "Group" && infoGroup && (
                <div className="flex-1 overflow-y-auto">
                  <InfoGroup
                    infoGroup={infoGroup}
                    stepGroup={stepGroup}
                    lang={lang}
                    displayGroup={displayGroup}
                    tempGroup={tempGroup}
                    setTempGroup={setTempGroup}
                    search={search}
                    _group={_group}
                    setDisplayGroup={setDisplayGroup}
                    inviteGroupChat={inviteGroupChat}
                    setStepGoup={setStepGoup}
                    handleSubmit={handleSubmit}
                    updateGroupChat={updateGroupChat}
                    userChatAktif={userChatAktif}
                    editGroup={editGroup}
                    selectedImage={selectedImage}
                    handleImageChange={handleImageChange}
                    setGroup={setGroup}
                    isAdmin={isAdmin}
                    setEditGroup={setEditGroup}
                    isAdminGC={isAdminGC}
                    user={user}
                    isAdminOnly={isAdminOnly}
                    isOwnerAndAdmin={isOwnerAndAdmin}
                    givePrevGroupChat={givePrevGroupChat}
                    deleteMemberGroupChat={deleteMemberGroupChat}
                  />
                </div>
              )}
            </div>
          )}
        </>
      )}

      <Dialog
        open={modalGroup}
        onOpenChange={(open) => {
          setModalGroup(open);
          setGroup({ name: "", description: "" });
          setDisplayGroup([]);
          setTempGroup([]);
        }}
      >
        <DialogContent className="sm:max-w-[425px]">
          <DialogHeader>
            <DialogTitle>{lang.groupInfo}</DialogTitle>
          </DialogHeader>
          {isLoading && (
            <div className="absolute h-full w-[98%] flex items-center justify-center bg-white/30 backdrop-blur-sm z-10">
              <LoadingSpinner stroke={`#9D21E6`} size={60} />
            </div>
          )}
          <InfoGroup
            isLoading={isLoading}
            infoGroup={infoGroup}
            stepGroup={stepGroup}
            lang={lang}
            displayGroup={displayGroup}
            tempGroup={tempGroup}
            setTempGroup={setTempGroup}
            search={search}
            _group={_group}
            setDisplayGroup={setDisplayGroup}
            inviteGroupChat={inviteGroupChat}
            setStepGoup={setStepGoup}
            handleSubmit={handleSubmit}
            updateGroupChat={updateGroupChat}
            userChatAktif={userChatAktif}
            editGroup={editGroup}
            selectedImage={selectedImage}
            handleImageChange={handleImageChange}
            setGroup={setGroup}
            isAdmin={isAdmin}
            setEditGroup={setEditGroup}
            isAdminGC={isAdminGC}
            user={user}
            isAdminOnly={isAdminOnly}
            isOwnerAndAdmin={isOwnerAndAdmin}
            givePrevGroupChat={givePrevGroupChat}
            deleteMemberGroupChat={deleteMemberGroupChat}
          />
        </DialogContent>
      </Dialog>

      <ReusableAlertDialog
        open={alertGroup}
        onOpenChange={setAlertGroup}
        title={lang.promptExitGroup}
        description={lang.promptDesc}
        textDelete={lang.btnExitGroup}
        isLoading={leaveGroupChat?.isLoading}
        onDelete={() =>
          leaveGroupChat.mutate({
            groupId: userChatAktif?.groupId,
            type: "delete",
          })
        }
      />
      <ReusableAlertDialog
        open={alert3}
        onOpenChange={setalert3}
        title="Are you sure to delete group?"
        description={lang.promptDesc}
        onDelete={() => deleteGroup.mutate(userChatAktif?.groupId)}
        isLoading={deleteGroup?.isLoading}
      />
    </>
  );
};
