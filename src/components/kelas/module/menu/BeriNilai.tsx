import {
  Popover,
  PopoverContent,
  PopoverTrigger,
} from "@/components/ui/popover";

export const BeriNilai = (props) => {
  const {
    open,
    setOpen,
    setInputQuiz,
    inputQuiz,
    postGradeAssignment,
    postGradeQuiz,
    postGradeExam,
    dataUjianUts,
    dataUjianUas,
  } = props;

  const handleClick = () => {
    if (postGradeAssignment) {
      postGradeAssignment.mutate({
        id: inputQuiz.id,
        grade: inputQuiz.grade,
        comment: inputQuiz.comment,
      });
    } else if (postGradeQuiz) {
      postGradeQuiz.mutate({
        id: inputQuiz.id,
        grade: inputQuiz.grade,
        comment: inputQuiz.comment,
      });
    } else if (postGradeExam) {
      postGradeExam.mutate({
        studentId: parseInt(inputQuiz.id),
        grade: inputQuiz.grade,
        comment: inputQuiz.comment,
        examId: dataUjianUts?.id ?? dataUjianUas?.id,
      });
    }
  };
  return (
    <Popover open={open} onOpenChange={setOpen}>
      <PopoverTrigger></PopoverTrigger>
      <PopoverContent align="center" className=" sm:p-6 sm:w-96 ">
        <h1 className="text-[#9D21E6] mb-3 text-center text-lg font-semibold">
          Beri Nilai
        </h1>
        <div className="mb-5">
          <label
            htmlFor="nilaiKuis"
            className="block  font-medium text-gray-700"
          >
            Nilai
          </label>

          <input
            type="number"
            id="nilaiKuis"
            placeholder="3"
            onChange={(e) =>
              setInputQuiz({ ...inputQuiz, grade: e.target.value })
            }
            value={inputQuiz?.grade}
            className="px-3 py-2 h-11 text-sm mt-3 w-full rounded-[10px] border border-gray-400  outline-none  shadow-sm focus:ring-2 ring-[#9d21e6]"
          />
        </div>
        <div className="mb-5 ">
          <label
            htmlFor="komentarNilaiKuis"
            className="block  font-medium text-gray-700"
          >
            Komentar
          </label>
          <textarea
            id="komentarNilaiKuis"
            className="w-full resize-none px-3 py-2 text-sm mt-3 rounded-[10px] border border-gray-400  outline-none  shadow-sm focus:ring-2 ring-[#9d21e6]"
            rows={4}
            onChange={(e) =>
              setInputQuiz({ ...inputQuiz, comment: e.target.value })
            }
            value={inputQuiz?.comment}
            placeholder="Isi komentar terkait Kuis"
          ></textarea>
        </div>
        <div className="w-fit mx-auto">
          <button
            onClick={handleClick}
            className=" p-2.5 w-32  bg-[#9d21e6] text-white shadow-xl rounded-[10px] font-semibold"
            type="button"
          >
            Submit
          </button>
        </div>
      </PopoverContent>
    </Popover>
  );
};
