import React from "react";
import Image from "next/image";
import {
  DropdownMenu,
  DropdownMenuContent,
  DropdownMenuItem,
  DropdownMenuLabel,
  DropdownMenuSeparator,
  DropdownMenuTrigger,
} from "@/components/ui/dropdown-menu";
import { ToggleGroup, ToggleGroupItem } from "@/components/ui/toggle-group";

export const InfoGroup = (props) => {
  const {
    infoGroup,
    stepGroup,
    lang,
    displayGroup,
    tempGroup,
    setTempGroup,
    search,
    _group,
    setDisplayGroup,
    inviteGroupChat,
    setStepGoup,
    handleSubmit,
    updateGroupChat,
    userChatAktif,
    editGroup,
    selectedImage,
    handleImageChange,
    setGroup,
    isAdmin,
    setEditGroup,
    isAdminGC,
    user,
    isAdminOnly,
    isOwnerAndAdmin,
    givePrevGroupChat,
    deleteMemberGroupChat,
    isLoading,
  } = props;
  return (
    <>
      {infoGroup && stepGroup === "1" ? (
        <div className="">
          <div className="mb-3">
            <label htmlFor="title" className="block  font-medium text-gray-700">
              {lang.whoAdded}
            </label>
            {displayGroup.length > 0 && (
              <div className="flex items-center gap-x-2.5 overflow-x-auto mt-3 max-w-sm">
                {displayGroup.map((item) => (
                  <div
                    key={item.id}
                    className="flex flex-col items-center gap-x-2.5"
                  >
                    <div className="relative h-10 w-10 border p-0.5 rounded-lg border-gray-300">
                      <Image
                        width={50}
                        height={50}
                        src={item?.profilePict ?? `/static/images/logokedi.svg`}
                        className="absolute inset-0 h-full w-full object-cover  rounded-lg"
                        alt=""
                      />
                    </div>
                    <div>
                      <p className="text-xs truncate w-10">{item.fullName}</p>
                    </div>
                  </div>
                ))}
              </div>
            )}
          </div>
          <ToggleGroup
            type="multiple"
            value={tempGroup}
            onValueChange={(value) => {
              if (value) setTempGroup(value);
            }}
            className="h-[55vh] pt-2 relative overflow-y-auto px-1.5"
          >
            {search?.length > 0 ? (
              search?.map((item: any) => {
                const isItemHidden = (itemId) =>
                  _group?.userIdList?.includes(itemId);
                if (!isItemHidden(item.id)) {
                  return (
                    <ToggleGroupItem
                      value={item.id}
                      key={item.id}
                      onClick={() => {
                        if (
                          !displayGroup.some(
                            (existingItem) => existingItem.id === item.id,
                          )
                        ) {
                          if (!tempGroup.includes(item.id)) {
                            setDisplayGroup([...displayGroup, item]);
                          } else {
                            console.log("Item sudah ada di dalam tempGroup");
                          }
                        } else {
                          const updatedDisplayGroup = displayGroup.filter(
                            (existingItem) => existingItem.id !== item.id,
                          );
                          setDisplayGroup(updatedDisplayGroup);
                        }
                      }}
                      className="w-full flex items-center justify-between items-center gap-x-2 hover:bg-[#9D21E6]/10 border p-2.5 rounded-lg mb-1.5 cursor-pointer"
                    >
                      <div className="flex items-center gap-x-2.5">
                        <div className="flex-none relative h-12 w-12 border p-0.5 rounded-lg border-gray-300">
                          <Image
                            width={60}
                            height={60}
                            src={
                              item?.profilePict ?? `/static/images/logokedi.svg`
                            }
                            className="absolute inset-0 h-full w-full object-cover  rounded-lg"
                            alt=""
                          />
                        </div>
                        <div>
                          <p className="font-semibold text-ellipsis">
                            {item.fullName}
                          </p>
                        </div>
                      </div>
                    </ToggleGroupItem>
                  );
                }
              })
            ) : (
              <div className="h-full flex items-center justify-center">
                {lang.dataNotFound}
              </div>
            )}
          </ToggleGroup>
          <div className="text-sm w-fit mx-auto mt-4 space-x-4">
            <button
              className="px-8  py-2   bg-[#9d21e6] text-white shadow-xl rounded-[10px] font-medium"
              type="button"
              onClick={() =>
                inviteGroupChat.mutate({
                  userIdList: tempGroup,
                  groupId: userChatAktif?.groupId
                    ? parseInt(userChatAktif?.groupId)
                    : 0,
                })
              }
            >
              {lang.submit}
            </button>
            <button
              onClick={() => setStepGoup("2")}
              className="px-8  py-2  bg-[#d7b9fd] text-[#9d21e6]  shadow-xl rounded-[10px] font-medium"
              type="button"
            >
              {lang.cancel}
            </button>
          </div>
        </div>
      ) : (
        <div className="pr-3">
          <div className="my-3">
            <form onSubmit={handleSubmit} className="flex justify-end  w-full">
              <div className="flex  gap-x-5 w-full">
                <div className="mx-auto h-24 w-24 text-center mb-5">
                  <div className="relative w-64">
                    <label className="cursor-pointer">
                      <input
                        disabled={updateGroupChat.isLoading || !editGroup}
                        type="file"
                        accept="image/*"
                        className="hidden"
                        onChange={handleImageChange}
                      />
                      <Image
                        className="h-24 w-24 rounded-xl absolute object-cover"
                        width={500}
                        height={500}
                        src={selectedImage}
                        alt="profile image"
                      />
                      {editGroup && (
                        <div className="w-24 h-24 group hover:bg-gray-200 opacity-40 rounded-full absolute flex justify-center items-center cursor-pointer transition duration-500">
                          <img
                            className="hidden group-hover:block w-12"
                            src="https://www.svgrepo.com/show/33565/upload.svg"
                            alt=""
                          />
                        </div>
                      )}
                    </label>
                  </div>
                </div>
                <div className="flex-1">
                  <label
                    htmlFor="title"
                    className="block text-sm font-medium text-gray-700"
                  >
                    {lang.titleGroupChat}
                  </label>
                  <input
                    type="text"
                    name="name"
                    value={_group.name}
                    onChange={(e) =>
                      setGroup({ ..._group, name: e.target.value })
                    }
                    className={`disabled:bg-white w-full resize-none py-2 text-sm mt-1 disabled:border-b  border-black  outline-none ${
                      editGroup
                        ? "border px-2.5 rounded-lg focus:ring-1 ring-[#9d21e6]"
                        : "border-b"
                    }`}
                    placeholder={lang.titleGroupChat}
                    disabled={updateGroupChat.isLoading || !editGroup}
                    autoComplete="off"
                  />
                  <label
                    htmlFor="title"
                    className=" mt-3 block text-sm font-medium text-gray-700"
                  >
                    {lang.descGroupChat}
                  </label>
                  <textarea
                    id="description"
                    value={_group.description}
                    rows={_group.description?.length > 25 ? 2 : 1}
                    maxLength="50"
                    onChange={(e) =>
                      setGroup({ ..._group, description: e.target.value })
                    }
                    className={`disabled:bg-white w-full resize-none py-2 text-sm mt-1 disabled:border-b  border-black  outline-none ${
                      editGroup
                        ? "border px-2.5 rounded-lg focus:ring-1 ring-[#9d21e6]"
                        : "border-b"
                    }`}
                    disabled={updateGroupChat.isLoading || !editGroup}
                    placeholder={lang.descGroupChat}
                  ></textarea>
                </div>
              </div>
            </form>
            {isAdminGC && (
              <div className="text-sm w-fit mx-auto mt-4 space-x-4">
                <button
                  onClick={() => setEditGroup(!editGroup)}
                  className="px-8  py-2  bg-[#d7b9fd] text-[#9d21e6]  shadow-xl rounded-[10px] font-medium"
                  type="button"
                >
                  {editGroup ? lang.cancelEdit : lang.edit}
                </button>
                <button
                  className="px-8  py-2  bg-[#9d21e6] text-white shadow-xl rounded-[10px] font-medium"
                  onClick={() =>
                    editGroup
                      ? updateGroupChat.mutate({
                          name: _group.name,
                          description: _group.description,
                          userIdList: _group.userIdList,
                        })
                      : setStepGoup("1")
                  }
                  type="button"
                >
                  {editGroup ? lang.submit : lang.addMemberGc}
                </button>
              </div>
            )}
          </div>
          <div className="px-1.5 text-sm font-medium">
            {infoGroup?.groupChat?.groupMember?.length}
            {infoGroup?.groupChat?.groupMember?.length > 1
              ? " " + lang.members
              : " " + lang.member}
          </div>
          <div className="h-[45vh] sm:h-full pt-2 relative overflow-y-auto px-1.5">
            {infoGroup
              ? infoGroup?.groupChat?.groupMember?.map((item: any) => {
                  const name = item?.user?.fullName;
                  const userId = item?.user?.id;
                  const img = item?.user?.profilePictureLink;
                  const isOwner =
                    infoGroup?.groupChat?.createdBy?.id === userId;
                  const isAdminGroup = item.isAdmin;
                  return (
                    <div
                      onClick={() => console.log(isAdminGC)}
                      value={item.id}
                      key={item.id}
                      className="w-full flex items-center justify-between items-center gap-x-2 hover:bg-[#9D21E6]/10 border p-2.5 rounded-lg mb-2.5 cursor-pointer"
                    >
                      <div className="flex items-center gap-x-2.5">
                        <div className="flex-none relative h-12 w-12 border p-0.5 rounded-lg border-gray-300">
                          <Image
                            width={60}
                            height={60}
                            src={img ?? `/static/images/logokedi.svg`}
                            className="absolute inset-0 h-full w-full object-cover  rounded-lg"
                            alt=""
                          />
                        </div>
                        <div>
                          <p className="font-semibold text-ellipsis text-sm">
                            {name} {user?.id === userId && "(Anda)"}
                          </p>
                        </div>
                      </div>
                      <div className="flex items-center gap-x-2">
                        {isOwner && (
                          <span className="px-2 py-1 rounded-full bg-[#d7b9fd] text-black font-semibold text-xs">
                            {lang.owner}
                          </span>
                        )}
                        {isAdminGroup && !isOwner && (
                          <span className="px-2 py-1 rounded-full bg-[#d7b9fd] text-black font-semibold text-xs">
                            Admin
                          </span>
                        )}

                        {(isOwnerAndAdmin || isAdminOnly) &&
                          user?.id !== userId && (
                            <>
                              {!isOwner && (
                                <DropdownMenu>
                                  {((!isOwner && isAdminOnly) || isAdmin) && (
                                    <DropdownMenuTrigger className="inline-flex items-center justify-center  h-10 w-10 rounded-lg transition duration-500 ease-in-out focus:outline-none">
                                      <svg
                                        xmlns="http://www.w3.org/2000/svg"
                                        width={20}
                                        height={20}
                                        fill="currentColor"
                                        className="bi bi-three-dots-vertical"
                                        viewBox="0 0 16 16"
                                      >
                                        <path d="M9.5 13a1.5 1.5 0 1 1-3 0 1.5 1.5 0 0 1 3 0zm0-5a1.5 1.5 0 1 1-3 0 1.5 1.5 0 0 1 3 0zm0-5a1.5 1.5 0 1 1-3 0 1.5 1.5 0 0 1 3 0z" />
                                      </svg>
                                    </DropdownMenuTrigger>
                                  )}
                                  <DropdownMenuContent className="mr-8 z-[100]">
                                    <DropdownMenuLabel>
                                      {lang.groupMenu}
                                    </DropdownMenuLabel>
                                    <DropdownMenuSeparator />
                                    {isOwnerAndAdmin && (
                                      <DropdownMenuItem
                                        onClick={() =>
                                          givePrevGroupChat.mutate({
                                            groupId: userChatAktif?.groupId,
                                            userId: userId,
                                            type: isAdminGroup
                                              ? "delete"
                                              : "post",
                                          })
                                        }
                                      >
                                        {isAdminGroup
                                          ? lang.delete
                                          : lang.grant}{" "}
                                        {lang.privileged}
                                      </DropdownMenuItem>
                                    )}
                                    {!isOwner && (
                                      <DropdownMenuItem
                                        onClick={() =>
                                          deleteMemberGroupChat.mutate({
                                            groupId: userChatAktif?.groupId,
                                            userId: userId,
                                            type: "delete",
                                          })
                                        }
                                      >
                                        {lang.deleteMemberGc}
                                      </DropdownMenuItem>
                                    )}
                                  </DropdownMenuContent>
                                </DropdownMenu>
                              )}
                            </>
                          )}
                      </div>
                    </div>
                  );
                })
              : !isLoading && (
                  <div className="h-full flex items-center justify-center">
                    {lang.dataNotFound}
                  </div>
                )}
          </div>
        </div>
      )}
    </>
  );
};
