import { formatDate, formatToTime } from "@/utils/convertTime";
import Link from "next/link";

export const ShowDataPartial = (props) => {
  return (
    <>
      {props?.classQuiz ? (
        <div key={props.id} className="flow-root px-4">
          <dl className="-my-3 divide-y-2 divide-dashed divide-gray-200 text-sm">
            <div className="grid grid-cols-1 gap-1 py-3 sm:grid-cols-3 sm:gap-4">
              <dt className=" text-gray-700">Judul Kuis</dt>
              <dd className="font-medium  sm:col-span-2">
                {props?.classQuiz?.name}
              </dd>
            </div>

            <div className="grid grid-cols-1 gap-1 py-3 sm:grid-cols-3 sm:gap-4">
              <dt className="text-gray-700">Instruksi Kuis</dt>
              <dd className="font-medium sm:col-span-2">
                {props?.classQuiz?.instruction}
              </dd>
            </div>

            <div className="grid grid-cols-1 gap-1 py-3 sm:grid-cols-3 sm:gap-4">
              <dt className="text-gray-700">Batas Waktu</dt>
              <dd className="font-medium sm:col-span-2">
                {formatDate(props?.classQuiz?.dueDate)}
              </dd>
            </div>

            <div className="grid grid-cols-1 gap-1 py-3 sm:grid-cols-3 sm:gap-4 items-center">
              <dt className="text-gray-700">File Jawaban</dt>
              <dd className="font-medium sm:col-span-2">
                <div className="text-xs sm:text-sm  mt-2">
                  {props.references?.map((z: any) => (
                    <div key={props.id} className="">
                      <Link
                        href={z?.url ?? "#"}
                        target="_blank"
                        rel="noopener noreferrer"
                        className="flex items-center gap-x-2.5 mb-1 mt-2 font-bold"
                      >
                        <svg
                          xmlns="http://www.w3.org/2000/svg"
                          fill="none"
                          viewBox="0 0 24 24"
                          strokeWidth={1.5}
                          stroke="currentColor"
                          className="w-6 h-6"
                        >
                          <path
                            strokeLinecap="round"
                            strokeLinejoin="round"
                            d="M18.375 12.739l-7.693 7.693a4.5 4.5 0 01-6.364-6.364l10.94-10.94A3 3 0 1119.5 7.372L8.552 18.32m.009-.01l-.01.01m5.699-9.941l-7.81 7.81a1.5 1.5 0 002.112 2.13"
                          />
                        </svg>

                        <span>{z.originalName}</span>
                      </Link>
                    </div>
                  ))}
                </div>{" "}
              </dd>
            </div>

            <div className="grid grid-cols-1 gap-1 py-3 sm:grid-cols-3 sm:gap-4">
              <dt className="text-gray-700">Deksripsi Jawaban</dt>
              <dd className="font-medium sm:col-span-1">
                {props?.description ?? "Tidak ada deskrispsi"}
              </dd>
            </div>
            <div
              key={props.id}
              className="grid grid-cols-1 gap-1 py-3 sm:grid-cols-3 sm:gap-4"
            >
              <dt className="text-gray-700">Penilaian</dt>
              <dd
                className={` ${
                  props.grade ? "text-xl text-[#9d21e6] font-semibold" : ""
                }   sm:col-span-2 font-medium`}
              >
                {props.grade ?? "Belum ada Penilaian"}
              </dd>
            </div>
            <div className="grid grid-cols-1 gap-1 py-3 sm:grid-cols-3 sm:gap-4 items-center">
              <dt className="text-gray-700">Komentar</dt>
              <dd className={`font-medium text-gray-700 sm:col-span-2`}>
                {props.lecturerComment ?? "Belum ada Komentar"}
              </dd>
            </div>
          </dl>
        </div>
      ) : (
        <div key={props.id} className="flow-root px-4">
          <dl className="-my-3 divide-y-2 divide-dashed divide-gray-200 text-sm">
            <div className="grid grid-cols-1 gap-1 py-3 sm:grid-cols-3 sm:gap-4">
              <dt className=" text-gray-700">Judul Tugas</dt>
              <dd className="font-medium  sm:col-span-2">
                {props?.classAssignment?.name}
              </dd>
            </div>

            <div className="grid grid-cols-1 gap-1 py-3 sm:grid-cols-3 sm:gap-4">
              <dt className="text-gray-700">Instruksi Tugas</dt>
              <dd className="font-medium sm:col-span-2">
                {props?.classAssignment?.instruction}
              </dd>
            </div>

            <div className="grid grid-cols-1 gap-1 py-3 sm:grid-cols-3 sm:gap-4">
              <dt className="text-gray-700">Batas Waktu</dt>
              <dd className="font-medium sm:col-span-2">
                {formatDate(props?.classAssignment?.dueDate)}
                {"  "}
                {formatToTime(props?.classAssignment?.dueDate)}
              </dd>
            </div>

            <div className="grid grid-cols-1 gap-1 py-3 sm:grid-cols-3 sm:gap-4 items-center">
              <dt className="text-gray-700">File Jawaban</dt>
              <dd className="font-medium sm:col-span-2">
                <div className="text-xs sm:text-sm  mt-2">
                  {props.references?.map((z: any) => (
                    <div key={props.id} className="">
                      <Link
                        href={z?.url ?? "#"}
                        target="_blank"
                        rel="noopener noreferrer"
                      className="flex items-center gap-x-2.5 mb-1 mt-2 font-bold">
                        <svg
                          xmlns="http://www.w3.org/2000/svg"
                          fill="none"
                          viewBox="0 0 24 24"
                          strokeWidth={1.5}
                          stroke="#000"
                          className="w-6 h-6 text-red-700"
                        >
                          <path
                            strokeLinecap="round"
                            strokeLinejoin="round"
                            d="M18.375 12.739l-7.693 7.693a4.5 4.5 0 01-6.364-6.364l10.94-10.94A3 3 0 1119.5 7.372L8.552 18.32m.009-.01l-.01.01m5.699-9.941l-7.81 7.81a1.5 1.5 0 002.112 2.13"
                          />
                        </svg>

                        <span>{z.originalName}</span>
                      </Link>
                    </div>
                  ))}
                </div>{" "}
              </dd>
            </div>

            <div className="grid grid-cols-1 gap-1 py-3 sm:grid-cols-3 sm:gap-4">
              <dt className="text-gray-700">Deksripsi Jawaban</dt>
              <dd className="font-medium sm:col-span-1">
                {props?.description ?? "Tidak ada deskrispsi"}
              </dd>
            </div>
            <div
              key={props.id}
              className="grid grid-cols-1 gap-1 py-3 sm:grid-cols-3 sm:gap-4"
            >
              <dt className="text-gray-700">Penilaian</dt>
              <dd
                className={` ${
                  props.grade ? "text-xl text-[#9d21e6] font-semibold" : ""
                }   sm:col-span-2 font-medium`}
              >
                {props.grade ?? "Belum ada Penilaian"}
              </dd>
            </div>
            <div className="grid grid-cols-1 gap-1 py-3 sm:grid-cols-3 sm:gap-4 items-center">
              <dt className="text-gray-700">Komentar</dt>
              <dd className={`font-medium text-gray-700 sm:col-span-2`}>
                {props.lecturerComment ?? "Belum ada Komentar"}
              </dd>
            </div>
          </dl>
        </div>
      )}
    </>
  );
};
