import React from "react";
import { Progress } from "@/components/ui/progress";
import { calculateProgress } from "@/utils/helper";
import { converDateNoDay, formatDate, formatToTime } from "@/utils/convertTime";
import { LoadingSpinner } from "@/components/ui/loading";
import { useMutation } from "@tanstack/react-query";
import toast from "react-hot-toast";
import axios from "axios";

interface ApiResponse {
  data: {
    message: string;
  };
}
interface PostResponse extends ApiResponse {}
export const InfoClass = ({
  lang,
  dataInfoKelas,
  attendanceClass,
  userChatAktif,
  sessionToken,
}: any) => {
  const isAdmin = userChatAktif?.receiver?.id === userChatAktif?.sender?.id;

  const nextSession = useMutation(
    async (classId): Promise<PostResponse> => {
      if (!sessionToken) throw new Error("Missing session token");
      try {
        const response = await axios.post<PostResponse>(
          process.env.API_BE + `/api/class/${classId}/activate-next-session`,
          {},
          {
            headers: {
              Authorization: `Bearer ${sessionToken}`,
            },
          },
        );
        toast.success(lang.successNextSession);
        return response.data;
      } catch (error) {
        console.error(error);
        throw error;
      }
    },
    {
      onError: (error) =>
        toast.error(`Error: ${error?.response?.data?.meta?.message}`),
    },
  );

  function formatCurrency(amount) {
    if (!amount) return 0;
    const formattedAmount = new Intl.NumberFormat("id-ID", {
      style: "currency",
      currency: "IDR",
      minimumFractionDigits: 2,
    }).format(amount);

    return formattedAmount;
  }
  return (
    <div className="text-sm pb-5 flex flex-col justify-between  h-full max-h-[85vh] overflow-y-auto">
      <div className=" px-3">
        <div className=" mb-3 pb-1.5 pt-2.5  border-y-2 border-dashed">
          <Progress
            className="col-span-2 max-w-[95%] mx-auto h-2"
            value={calculateProgress(40, 50).toFixed(0)}
          />
          <div className="flex justify-between items-center mt-3 text-sm">
            <div>
              <p className="font-semibold mb-1">{lang.startTime}</p>
              <p>{converDateNoDay(dataInfoKelas?.startClassDate)}</p>
            </div>
            <div className="items-end text-right">
              <p className="font-semibold mb-1">{lang.endTime}</p>
              <p>{converDateNoDay(dataInfoKelas?.endClassDate)}</p>
            </div>
          </div>
        </div>
        <div className="mb-3 pb-1.5  border-b-2 border-dashed">
          <label>{lang.startRegisterDate}</label>
          <p className="my-1 font-bold">
            {formatDate(dataInfoKelas?.startRegisterDate)}
          </p>
        </div>
        <div className="mb-3 pb-1.5  border-b-2 border-dashed">
          <label>{lang.endRegisterDate}</label>
          <p className="my-1 font-bold">
            {formatDate(dataInfoKelas?.endRegisterDate)}
          </p>
        </div>

        <div className="mb-3 pb-1.5  border-b-2 border-dashed">
          <label>{lang.maximumCapacity}</label>
          <p className="my-1 font-bold">{dataInfoKelas?.maximumCapacity}</p>
        </div>

        <div className="mb-3 pb-1.5  border-b-2 border-dashed">
          <label>{lang.numOfSession}</label>
          <p className="my-1 font-bold">{attendanceClass?.length}</p>
        </div>

        <div className="mb-3 pb-1.5  border-b-2 border-dashed">
          <label>{lang.class}</label>
          <p className="my-1 font-bold">
            {dataInfoKelas?.isPaidClass === false ? lang.free : lang.paid}
          </p>
        </div>
        {dataInfoKelas?.isPaidClass === true && (
          <div className="mb-3 pb-1.5  border-b-2 border-dashed">
            <label>{lang.cost}</label>
            <p className="my-1 font-bold">
              {formatCurrency(dataInfoKelas?.registrationFee)}
            </p>
          </div>
        )}
        <div className="mb-3 pb-1.5  border-b-2 border-dashed">
          <label>Tipe</label>
          <p className="my-1 font-bold">
            {dataInfoKelas?.isPrivateClass === false
              ? lang.public
              : lang.private}
          </p>
        </div>

        <div className="mb-3 pb-1.5  border-b-2 border-dashed">
          <label>{lang.timeAttendance}</label>
          <p className="my-1 font-bold">
            {formatToTime(dataInfoKelas?.startAttendanceTime)}
            {" - "}
            {formatToTime(dataInfoKelas?.endAttendanceTime)}
          </p>
        </div>
      </div>

      {isAdmin && (
        <div className="w-full flex items-center justify-center">
          <button
            onClick={() => nextSession.mutate(userChatAktif.classId)}
            className="py-2.5 px-10 mt-4 rounded-xl text-white transition-all text-sm bg-[#9d21e6] w-fit shadow-[0px_10px_15px_rgba(157,33,230,0.25)] flex items-center justify-center"
          >
            {nextSession.isLoading ? (
              <LoadingSpinner size="25" stroke="#fff" />
            ) : (
              lang.btnNextSession
            )}
          </button>
        </div>
      )}
    </div>
  );
};

{
  /*



    class: {
      titleGroupChat: "Nama Grup Chat",
      descGroupChat: "Deskripsi Grup Chat",
      submit: "Kirim",
      dataNotFound: "Data tidak ditemukan",
      chats: "Chat",
      group: "Grup",
      activeCLass: "Kelas Aktif",
      noMessage: "Tidak ada pesan",
      all: "Semua",
      public: "Umum",
      academic: "Akademik",
      btnChatContact: "Chat kontak",
      btnCreateGroupChat: "Buat Grup Chat",
      titleCreateGroupChat: "Buat Grup Chat",
      adtMenu: "Menu chat",
      class: "Kelas",
      member: "Anggota",
      members: "Anggota",
      back: "Kembali",
      cancel: "Batal",
      next: "Lanjut",
      search: "Cari",
      whoAdded: "Siapa yang ingin anda tambahkan ?",
      successUploadImg: "Foto berhasil diunggah",
      successUpload: "File berhasil diunggah",
      errorUpload: "Terjadi kesalahan saat mengunggah file",
      yourMessage: "Pesan Anda",
      descYourMessage: "Kirim foto dan pesan pribadi ke teman atau grup",
      successDeleteMessage: "Pesan berhasil dihapus",
      lecturer: "Dosen",
      student: "Mahasiswa",
      session: "Sesi",
      present: "Hadir",
      absent: "Tidak hadir",
      notYetPresent: "Belum Hadir",
      by: "Oleh",
      noMaterial: "Tidak ada materi",
      noAssignments: "Tidak ada tugas",
      noQuizzes: "Tidak ada kuis",
      deadline: "Batas Waktu",
      classInfo: "Info Kelas",
      materials: "Materials",
      attendance: "Attendance",
      assignments: "Assignments",
      quizzes: "Quizzes",
      exam: "Exam",
      successNextSession: "Berhasil mengaktifkan sesi selanjutnya",
      startRegisterDate: "Tanggal Mulai Pendaftaran",
      endRegisterDate: "Tanggal Selesai Pendaftaran",
      maximumCapacity: "Jumlah Peserta Maximal",
      startTime: "Waktu Mulai",
      endTime: "Waktu Selesai",
      numOfSession: "Jumah Sesi",
      cost: "Biaya",
      free: "Gratis",
      paid: "Berbayar",
      type: "Jenis",
      private: "Privat",
      timeAttendance: "Waktu Absensi",
      btnNextSession: "Aktifkan Sesi Selanjutnya",

    },
*/
}
