import React from "react";
import Image from "next/image";
import { useMutation } from "@tanstack/react-query";
import toast from "react-hot-toast";
import axios from "axios";

export const InfoMembers = ({ dataMember, user, userChatAktif,getInfoMembers }) => {
  const sessionToken = user?.accessToken;
  interface ApiResponse {
    data: {
      message: string;
    };
  }
  interface ToggleFavoriteResponse extends ApiResponse {}
  const addFriend = useMutation(
    async (userId: number): Promise<ToggleFavoriteResponse> => {
      if (!sessionToken) throw new Error("Missing session token");
      try {
        const response = await axios.post<ToggleFavoriteResponse>(
          process.env.API_BE + `/api/friend/add`,
          {
            id: Number(userId),
          },
          {
            headers: {
              Authorization: `Bearer ${sessionToken}`,
            },
          },
        );
        getInfoMembers(userChatAktif?.classId);
        toast.success(response.data.data.message);
        return response.data;
      } catch (error) {
        toast.error(error);
        throw error;
      }
    },
  );
  return (
    <div className="overflow-y-auto px-1.5 pb-4 flex-1">
      {dataMember?.map((item: any) => (
        <div
          key={item.id}
          className="flex items-center justify-between items-center gap-x-2 hover:bg-[#9D21E6]/10 border p-2.5 rounded-lg mb-2.5 cursor-pointer"
        >
          <div className="flex items-center gap-x-2">
            <div className="relative h-12 w-12">
              <Image
                width={60}
                height={60}
                src={item?.profilePict ?? `/static/images/logokedi.svg`}
                className="absolute inset-0 h-full w-full object-cover  rounded-lg"
                alt=""
              />
            </div>
            <div>
              <p className="font-semibold text-ellipsis">{item.fullName}</p>
              {item.username && (
                <p className="text-sm text-[#9D21E6]">{"@" + item.username}</p>
              )}
            </div>
          </div>
          {!item.isFriend && user.id !== item.id && (
            <div
              onClick={() => addFriend.mutate(item.id)}
              className={`text-[#9D21E6] cursor-pointer `}
            >
              <svg
                xmlns="http://www.w3.org/2000/svg"
                width="24px"
                height="24px"
                viewBox="0 0 24 24"
              >
                <g
                  fill="none"
                  stroke="currentColor"
                  strokeLinecap="round"
                  strokeWidth="2.5"
                >
                  <path
                    strokeDasharray="20"
                    strokeDashoffset="20"
                    d="M3 21V20C3 17.7909 4.79086 16 7 16H11C13.2091 16 15 17.7909 15 20V21"
                  >
                    <animate
                      fill="freeze"
                      attributeName="stroke-dashoffset"
                      dur="0.4s"
                      values="20;0"
                    ></animate>
                  </path>
                  <path
                    strokeDasharray="20"
                    strokeDashoffset="20"
                    d="M9 13C7.34315 13 6 11.6569 6 10C6 8.34315 7.34315 7 9 7C10.6569 7 12 8.34315 12 10C12 11.6569 10.6569 13 9 13Z"
                  >
                    <animate
                      fill="freeze"
                      attributeName="stroke-dashoffset"
                      begin="0.5s"
                      dur="0.4s"
                      values="20;0"
                    ></animate>
                  </path>
                  <path strokeDasharray="8" strokeDashoffset="8" d="M15 6H21">
                    <animate
                      fill="freeze"
                      attributeName="stroke-dashoffset"
                      begin="1s"
                      dur="0.2s"
                      values="8;0"
                    ></animate>
                  </path>
                  <path strokeDasharray="8" strokeDashoffset="8" d="M18 3V9">
                    <animate
                      fill="freeze"
                      attributeName="stroke-dashoffset"
                      begin="1.2s"
                      dur="0.2s"
                      values="8;0"
                    ></animate>
                  </path>
                </g>
              </svg>
            </div>
          )}
        </div>
      ))}
    </div>
  );
};
