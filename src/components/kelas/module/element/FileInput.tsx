import { SetStateAction } from "react";
export const FileInput = (props) => {
  const {
    handleDragOver,
    handleDragEnter,
    handleDragLeave,
    handleDropClass,
    handleButtonClick,
    inputFileRef,
    handleFileChangeClass,
    fileClass,
    setFileClass,
    tempDocId,
    setTempDocId,
    setCreateMaterial,
    createMaterial,
    title,
  } = props;
  return (
    <>
      <label htmlFor="title" className="block my-1.5 font-medium text-gray-700">{`File ${title}`}</label>

      <div
        onDragOver={handleDragOver}
        onDragEnter={handleDragEnter}
        onDragLeave={handleDragLeave}
        onDrop={handleDropClass}
        className="p-0.5 bg-[#cfb6ff]/30 rounded-[10px] border-2 border-dashed border-[#9d21e6] mb-3"
      >
        <div className="relative flex  w-full gap-x-1.5 rounded-xl ">
          <button
            type="button"
            className="h-24 w-full text-center items-center text-[#9d21e6] font-semibold inline-flex items-center justify-center rounded-full transition duration-500 ease-in-out focus:outline-none"
            onClick={handleButtonClick}
          >
            <label
              htmlFor="fileInput"
              className=" h-full w-full flex items-center justify-center gap-x-2.5  cursor-pointer"
            >
              <svg
                xmlns="http://www.w3.org/2000/svg"
                fill="none"
                viewBox="0 0 24 24"
                strokeWidth={1.5}
                stroke="currentColor"
                className="w-6 h-6"
              >
                <path
                  strokeLinecap="round"
                  strokeLinejoin="round"
                  d="M12 10.5v6m3-3H9m4.06-7.19l-2.12-2.12a1.5 1.5 0 00-1.061-.44H4.5A2.25 2.25 0 002.25 6v12a2.25 2.25 0 002.25 2.25h15A2.25 2.25 0 0021.75 18V9a2.25 2.25 0 00-2.25-2.25h-5.379a1.5 1.5 0 01-1.06-.44z"
                />
              </svg>
              <span>Choose File / Drag and Drop Here</span>
            </label>
            <input
              ref={inputFileRef}
              id="fileInput"
              type="file"
              multiple
              className="hidden"
              onChange={handleFileChangeClass}
            />
          </button>
        </div>
      </div>
      {(fileClass?.length > 0 || tempDocId?.length > 0) && (
        <div className="px-4 py-1 overflow-y-auto max-h-[22vh] mb-4 bg-[#cfb6ff]/50 rounded-[10px] border border-gray-400">
          {tempDocId?.map((file: any, index: number) => (
            <div key={index}>
              <div className="flex items-center gap-x-2.5 mb-1.5 text-[#9d21e6]">
                <svg
                  xmlns="http://www.w3.org/2000/svg"
                  fill="none"
                  viewBox="0 0 24 24"
                  stroke="currentColor"
                  className="h-6 w-6 "
                >
                  <path
                    strokeLinecap="round"
                    strokeLinejoin="round"
                    strokeWidth={2}
                    d="M15.172 7l-6.586 6.586a2 2 0 102.828 2.828l6.414-6.586a4 4 0 00-5.656-5.656l-6.415 6.585a6 6 0 108.486 8.486L20.5 13"
                  />
                </svg>

                <span className="text-[#9d21e6]">{file.name}</span>
                <button
                  className="cursor-pointer text-red-600 duration-300"
                  onClick={() =>
                    setTempDocId((prevFiles: SetStateAction<never>[]) => {
                      const updatedFiles = [...prevFiles];
                      updatedFiles.splice(index, 1);
                      setCreateMaterial({
                        ...createMaterial,
                        documentIdList: updatedFiles.map((i: any) => i.id),
                      });
                      return updatedFiles;
                    })
                  }
                >
                  <svg
                    className="pointer-events-none fill-current w-4 h-4 ml-auto"
                    xmlns="http://www.w3.org/2000/svg"
                    width={24}
                    height={24}
                    viewBox="0 0 24 24"
                  >
                    <path
                      className="pointer-events-none"
                      d="M3 6l3 18h12l3-18h-18zm19-4v2h-20v-2h5.711c.9 0 1.631-1.099 1.631-2h5.316c0 .901.73 2 1.631 2h5.711z"
                    />
                  </svg>
                </button>
              </div>
            </div>
          ))}
          {fileClass?.map((file: any, index: number) => (
            <div key={index}>
              <div className="flex items-center gap-x-2.5 mb-1.5 text-[#9d21e6]">
                <svg
                  xmlns="http://www.w3.org/2000/svg"
                  fill="none"
                  viewBox="0 0 24 24"
                  stroke="currentColor"
                  className="h-6 w-6 "
                >
                  <path
                    strokeLinecap="round"
                    strokeLinejoin="round"
                    strokeWidth={2}
                    d="M15.172 7l-6.586 6.586a2 2 0 102.828 2.828l6.414-6.586a4 4 0 00-5.656-5.656l-6.415 6.585a6 6 0 108.486 8.486L20.5 13"
                  />
                </svg>

                <span className="text-[#9d21e6]">{file.name}</span>
                <button
                  className="cursor-pointer text-red-600 duration-300"
                  onClick={() =>
                    setFileClass((prevFiles: any) => {
                      const updatedFiles = [...prevFiles];
                      updatedFiles.splice(index, 1);
                      return updatedFiles;
                    })
                  }
                >
                  <svg
                    className="pointer-events-none fill-current w-4 h-4 ml-auto"
                    xmlns="http://www.w3.org/2000/svg"
                    width={24}
                    height={24}
                    viewBox="0 0 24 24"
                  >
                    <path
                      className="pointer-events-none"
                      d="M3 6l3 18h12l3-18h-18zm19-4v2h-20v-2h5.711c.9 0 1.631-1.099 1.631-2h5.316c0 .901.73 2 1.631 2h5.711z"
                    />
                  </svg>
                </button>
              </div>
            </div>
          ))}
        </div>
      )}
    </>
  );
};
