import { LoadingSpinner } from "@/components/ui/loading";
import {
  AlertDialog,
  AlertDialogCancel,
  AlertDialogContent,
  AlertDialogDescription,
  AlertDialogFooter,
  AlertDialogHeader,
  AlertDialogTitle,
} from "@/components/ui/alert-dialog";

export const ReusableAlertDialog = ({
  open,
  onOpenChange,
  title,
  description,
  onDelete,
  isLoading,
  textDelete,
}) => {
  return (
    <>
      <AlertDialog open={open} onOpenChange={onOpenChange}>
        <AlertDialogContent>
          {isLoading && (
            <div className="absolute h-full w-[98%] flex items-center justify-center bg-white/30 z-20">
              <LoadingSpinner stroke={`#9D21E6`} size={40} />
            </div>
          )}
          <AlertDialogHeader>
            <AlertDialogTitle>{title}</AlertDialogTitle>
            <AlertDialogDescription>{description}</AlertDialogDescription>
          </AlertDialogHeader>
          <AlertDialogFooter>
            <AlertDialogCancel>Batal</AlertDialogCancel>
            <button
              onClick={onDelete}
              disabled={isLoading ?? false}
              className="rounded-md text-sm font-semibold text-white py-2.5 px-4 bg-[#9d21e6] hover:bg-purple-600"
            >
              {textDelete ?? "Hapus"}
            </button>
          </AlertDialogFooter>
        </AlertDialogContent>
      </AlertDialog>
    </>
  );
};
