import Image from "next/image";
import Link from "next/link";
import axios from "axios";
import { useQuery } from "@tanstack/react-query";
import Slider, { LazyLoadTypes } from "react-slick";
import "slick-carousel/slick/slick.css";
import "slick-carousel/slick/slick-theme.css";

export const StoryFeed = ({ user }: any) => {
  const sessionToken = user?.accessToken;

  const { data, error, isLoading } = useQuery(["storyData"], async () => {
    if (!sessionToken) return null;

    const response = await axios.get(
      process.env.API_BE + "/api/feeds/story?limit=50&page=1",
      {
        headers: {
          Authorization: `Bearer ${sessionToken}`,
        },
      },
    );
    if (response.status !== 200) {
      throw new Error("Gagal mengambil data");
    }

    return response.data;
  });

  const settings = {
    dots: false,
    infinite: false,
    speed: 500,
    slidesToShow: 7,
    slidesToScroll: 1,
    initialSlide: 0,
    swipeToSlide: true,
    lazyLoad: "ondemand" as LazyLoadTypes,
    responsive: [
      {
        breakpoint: 1500,
        settings: {
          slidesToShow: 7,
          slidesToScroll: 1,
        },
      },
      {
        breakpoint: 790,
        settings: {
          arrows: false,
          slidesToShow: 6,
          slidesToScroll: 1,
        },
      },
      {
        breakpoint: 670,
        settings: {
          arrows: false,
          slidesToShow: 5,
          slidesToScroll: 1,
        },
      },
      {
        breakpoint: 550,
        settings: {
          arrows: false,
          slidesToShow: 4,
          slidesToScroll: 1,
        },
      },
      {
        breakpoint: 385,
        settings: {
          arrows: false,
          slidesToShow: 4,
          slidesToScroll: 1,
        },
      },
    ],
  };

  if (isLoading)
    return (
      <>
        <div className="h-[4.2rem] sm:h-[6rem]" />
        <div className="flex justify-between max-w-3xl my-5 px-2 sm:mx-auto">
          <div className="animate-pulse flex flex-col justify-center w-20">
            <div className="w-12 h-12 mx-auto lg:w-20 lg:h-20 rounded-xl bg-slate-200"></div>
            <div className="h-2 bg-slate-200 rounded mt-2"></div>
          </div>
          <div className="animate-pulse flex flex-col justify-center w-20">
            <div className="w-12 h-12 mx-auto lg:w-20 lg:h-20 rounded-xl bg-slate-200"></div>
            <div className="h-2 bg-slate-200 rounded mt-2"></div>
          </div>
          <div className="animate-pulse flex flex-col justify-center w-20">
            <div className="w-12 h-12 mx-auto lg:w-20 lg:h-20 rounded-xl bg-slate-200"></div>
            <div className="h-2 bg-slate-200 rounded mt-2"></div>
          </div>
          <div className="animate-pulse flex flex-col justify-center w-20">
            <div className="w-12 h-12 mx-auto lg:w-20 lg:h-20 rounded-xl bg-slate-200"></div>
            <div className="h-2 bg-slate-200 rounded mt-2"></div>
          </div>
          <div className="animate-pulse hidden sm:flex flex-col justify-center w-20">
            <div className="w-12 h-12 mx-auto lg:w-20 lg:h-20 rounded-xl bg-slate-200"></div>
            <div className="h-2 bg-slate-200 rounded mt-2"></div>
          </div>
          <div className="animate-pulse hidden md:flex flex-col justify-center w-20">
            <div className="w-12 h-12 mx-auto lg:w-20 lg:h-20 rounded-xl bg-slate-200"></div>
            <div className="h-2 bg-slate-200 rounded mt-2"></div>
          </div>
          <div className="animate-pulse hidden lg:flex flex-col justify-center  w-20">
            <div className="w-12 h-12 mx-auto lg:w-20 lg:h-20 rounded-xl bg-slate-200"></div>
            <div className="h-2 bg-slate-200 rounded mt-2"></div>
          </div>
        </div>
      </>
    );

  if (error) return <div />;

  return (
    <>
      <div className="h-[4.2rem] sm:h-[6rem]" />
      <div className="bg-white border shadow-sm max-w-3xl mx-auto py-3.5  mb-2.5">
        <Slider {...settings}>
          {data?.data?.items.map((item: any, index: number) => {
            return (
              <Link href={`/profile/${item?.id}` ?? `/profile`} key={index}>
                <div className="flex flex-col justify-center items-center mx-auto  w-20">
                  <div className="relative  h-16 w-16 sm:w-20 sm:h-20">
                    <Image
                      fill
                      blurDataURL="/static/images/default/placeholder.png"
                      placeholder="blur"
                      onErrorCapture={() =>
                        console.log("Error Load Image Feed " + item?.fullName)
                      }
                      sizes="(max-width: 768px) 100vw, (max-width: 1200px) 50vw, 33vw"
                      className="rounded-xl border border-gray-300 absolute inset-0 h-full w-full object-cover  "
                      alt={`${item?.fullName} Profile Picture`}
                      loading="lazy"
                      src={
                        item.profilePictureLink ??
                        "/static/images/default/placeholder.png"
                      }
                    />
                  </div>
                  <p className="text-center mt-2 overflow-hidden truncate text-sm sm:text-base max-w-[80px]">
                    {item.fullName}
                  </p>
                </div>
              </Link>
            );
          })}
        </Slider>
      </div>
    </>
  );
};
