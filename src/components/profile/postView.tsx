import Link from "next/link";
import { EmptyState } from "@/components/ui/empty-state";
import Image from "next/image";
import React, { useState } from "react";
import axios from "axios";
import {
  useQuery,
  useInfiniteQuery,
  useQueryClient,
  useMutation,
} from "@tanstack/react-query";
import { useAutoResizeInput } from "@/hook/useAutoResizeInput";
import { LoadingSpinner } from "@/components/ui/loading";
import { useInView } from "react-intersection-observer";
import Slider, { LazyLoadTypes } from "react-slick";
import "slick-carousel/slick/slick.css";
import "slick-carousel/slick/slick-theme.css";
import dayjs from "dayjs";
import "dayjs/locale/id";
import relativeTime from "dayjs/plugin/relativeTime";
import { toast } from "react-hot-toast";
import {
  Dialog,
  DialogContent,
  DialogHeader,
  DialogTitle,
  DialogTrigger,
} from "@/components/ui/dialog";
import { downloadapp, appstore, appgalery, appgoogle } from "@/assets";
import { useRouter } from "next/router";

interface ApiResponse {
  data: {
    message: string;
  };
}

interface ToggleLikeResponse extends ApiResponse {}

interface ToggleFavoriteResponse extends ApiResponse {}

interface PostCommentResponse extends ApiResponse {}

dayjs.extend(relativeTime);

export const PostsView = ({ user, user_id }: any) => {
  const router = useRouter();
  const locale: "en" | "id" = router.locale as "en" | "id";
  dayjs.locale(locale);

  {
    /*  const locale: any = { 
    id: {
      relativeTime: {
        future: "dalam %s",
        past: "%s yang lalu",
        s: "beberapa detik",
        m: "semenit",
        mm: "%d menit",
        h: "sejam",
        hh: "%d jam",
        d: "sehari",
        dd: "%d hari",
        M: "sebulan",
        MM: "%d bulan",
        y: "setahun",
        yy: "%d tahun",
      },
    },
    en: {
      relativeTime: {
        future: "in %s",
        past: "%s ago",
        s: "a few seconds",
        m: "a minute",
        mm: "%d minutes",
        h: "an hour",
        hh: "%d hours",
        d: "a day",
        dd: "%d days",
        M: "a month",
        MM: "%d months",
        y: "a year",
        yy: "%d years",
      },
    },
  };

  dayjs.locale(locale, undefined, true);
    */
  }

  const queryClient = useQueryClient();
  const profileData: any = queryClient.getQueryData(["profileData"]);

  const { ref, inView } = useInView();

  const [expandedPosts, setExpandedPosts] = useState<number[]>([]);

  const sessionToken = user?.accessToken;

  // const [isloadComment, setIsloadComment] = useState(false);
  const [isloadComment, setIsloadComment] = useState({});

  const {
    inputText,
    setInputText,
    inputTextSub,
    setInputTextSub,
    inputTextRef,
    toggle,
    setToggle,
    toggleSub,
    setToggleSub,
  } = useAutoResizeInput("");

  const { status, data, error, isFetchingNextPage, fetchNextPage } =
    useInfiniteQuery(
      ["postsDataProfile"],
      async ({ pageParam = 1 }) => {
        const res = await axios.get(
          process.env.API_BE +
            `/api/feeds?userId=${user_id}&?limit=10&page=${pageParam}`,
          {
            headers: {
              Authorization: `Bearer ${sessionToken}`,
            },
          },
        );
        return res.data;
      },
      {
        getNextPageParam: (lastPage) => {
          const { data } = lastPage;

          const { currentPage, totalPages } = data.meta;
          return currentPage < totalPages ? currentPage + 1 : undefined;
        },
      },
    );

  React.useEffect(() => {
    if (inView) {
      fetchNextPage();
    }
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [inView]);

  React.useEffect(() => {
    const handleRouteChange = (url) => {
      if (url !== "/profile/[id]") {
        queryClient.clear();
      }
    };

    router.events.on("routeChangeStart", handleRouteChange);

    return () => {
      router.events.off("routeChangeStart", handleRouteChange);
    };
  }, [router]);

  type LikePost = {
    postId: number;
    commentId: number | undefined;
  };

  const toggleLike = useMutation(
    async (data: LikePost): Promise<ToggleLikeResponse> => {
      const { postId, commentId } = data;
      if (!sessionToken) throw new Error("Missing session token");
      try {
        let apiUrl = process.env.API_BE + `/api/feeds/${postId}/like`;
        if (commentId !== undefined) {
          apiUrl =
            process.env.API_BE +
            `/api/feeds/${postId}/comment/${commentId}/like`;
        }
        const response = await axios.post<ToggleLikeResponse>(
          apiUrl,
          {},
          {
            headers: {
              Authorization: `Bearer ${sessionToken}`,
            },
          },
        );
        if (commentId !== undefined) {
          updateComments(postId);
        }
        toast.success(response.data.data.message);
        queryClient.invalidateQueries(["postsDataProfile"]);
        return response.data;
      } catch (error) {
        console.error(error);
        throw error;
      }
    },
  );
  // console.log(toggleLike);

  const fetchDataComments = async (postId: number) => {
    const response = await axios.get(
      process.env.API_BE + `/api/feeds/${postId}/comment?limit=10&page=1`,
      {
        headers: {
          Authorization: `Bearer ${sessionToken}`,
        },
      },
    );

    return {
      postId,
      items: response.data.data.items,
    };
  };

  const updateComments = async (postId: number) => {
    // setIsloadComment(true);
    setIsloadComment((prev) => ({
      ...prev,
      [postId]: postId,
    }));
    try {
      const response = await fetchDataComments(postId);

      queryClient.setQueryData(["postComments"], response);
    } catch (error) {
      console.error("Gagal mengambil data komentar.", error);
    } finally {
      setIsloadComment({});
    }
  };

  const { data: getComments } = useQuery<any>(["postComments"], {
    enabled: false,
  });

  type PostComment = {
    postId: number;
    content: string;
    parentId?: number;
  };

  const postComments = useMutation(
    async (data: PostComment): Promise<PostCommentResponse> => {
      const { postId, content, parentId } = data;
      if (!sessionToken) throw new Error("Missing session token");
      try {
        const response = await axios.post<PostCommentResponse>(
          process.env.API_BE + `/api/feeds/${postId}/comment`,
          {
            comment: String(content),
            parentCommentId: parentId !== 0 ? Number(parentId) : 0,
          },
          {
            headers: {
              Authorization: `Bearer ${sessionToken}`,
            },
          },
        );
        updateComments(postId);
        toast.success(response.data.data.message);
        setInputText("");
        setToggle(false);
        queryClient.invalidateQueries(["postsDataProfile"]);
        if (parentId !== 0) {
          setInputTextSub("");
          setToggleSub(0);
        }
        return response.data;
      } catch (error) {
        console.error(error);
        throw error;
      }
    },
  );
  type DeleteComment = {
    postId: number;
    commentId: number;
  };
  const deleteComments = useMutation(
    async (data: DeleteComment): Promise<ApiResponse> => {
      const { postId, commentId } = data;
      if (!sessionToken) throw new Error("Missing session token");
      try {
        const response = await axios.delete<ApiResponse>(
          process.env.API_BE + `/api/feeds/${postId}/comment/${commentId}`,
          {
            headers: {
              Authorization: `Bearer ${sessionToken}`,
            },
          },
        );
        updateComments(postId);
        toast.success(response.data.data.message);

        return response.data;
      } catch (error) {
        console.error(error);
        throw error;
      }
    },
  );

  const toggleFavorite = useMutation(
    async (postId: number): Promise<ToggleFavoriteResponse> => {
      if (!sessionToken) throw new Error("Missing session token");
      try {
        const response = await axios.post<ToggleFavoriteResponse>(
          process.env.API_BE + `/api/feeds/favorites/${postId}`,
          {},
          {
            headers: {
              Authorization: `Bearer ${sessionToken}`,
            },
          },
        );
        toast.success(response.data.data.message);
        queryClient.invalidateQueries(["postsDataProfile"]);
        return response.data;
      } catch (error) {
        console.error(error);
        throw error;
      }
    },
  );

  const removeFavorite = useMutation(
    async (postId: number): Promise<ApiResponse> => {
      try {
        const response = await axios.delete<ApiResponse>(
          process.env.API_BE + `/api/feeds/favorites/${postId}`,
          {
            headers: {
              Authorization: `Bearer ${sessionToken}`,
            },
          },
        );
        toast.success(response.data.data.message);
        queryClient.invalidateQueries(["postsDataProfile"]);
        return response.data;
      } catch (error) {
        console.error(error);
        throw error;
      }
    },
  );

  const toggleReadMore = (postId: number): void => {
    setExpandedPosts((prevExpandedPosts: number[]): number[] => {
      if (prevExpandedPosts.includes(postId)) {
        return prevExpandedPosts.filter((id: number) => id !== postId);
      } else {
        return [...prevExpandedPosts, postId];
      }
    });
  };

  const addFriend = useMutation(
    async (userId: number): Promise<ToggleFavoriteResponse> => {
      if (!sessionToken) throw new Error("Missing session token");
      try {
        const response = await axios.post<ToggleFavoriteResponse>(
          process.env.API_BE + `/api/friend/add`,
          {
            id: Number(userId),
          },
          {
            headers: {
              Authorization: `Bearer ${sessionToken}`,
            },
          },
        );
        toast.success(response.data.data.message);
        queryClient.invalidateQueries(["postsDataProfile"]);
        return response.data;
      } catch (error) {
        console.error(error);
        throw error;
      }
    },
  );

  const settings = {
    dots: true,
    infinite: false,
    speed: 500,
    slidesToShow: 1,
    slidesToScroll: 1,
    arrows: false,
    adaptiveHeight: true,
    lazyLoad: "ondemand" as LazyLoadTypes,
    appendDots: (dots: any) => (
      <div
        style={{
          backgroundColor: "transparent",
          borderRadius: "10px",
        }}
      >
        <ul style={{ marginBottom: "30px" }}> {dots} </ul>
      </div>
    ),
  };

  // const [result_search, setresult_search] = useState([]);

  // if (error) return <h1>Error : Request Data</h1>;

  {
    /*  const allItems = data?.pages?.flatMap((page) => page.data.items);
  const searchItems = (query, items) => {
    query = query.toLowerCase();
    return items.filter(
      (item) =>
        item.message.toLowerCase().includes(query) ||
        item.user.fullName.toLowerCase().includes(query) ||
        item.class.name.toLowerCase().includes(query) ||
        item.periods.toLowerCase().includes(query),
    );
  };
*/
  }
  // Contoh penggunaan fungsi pencarian
  // if (data) {
  //   const searchResult = searchItems(searchTerm, allItems);
  //   console.log(searchResult);
  // }

  return (
    <>
      {/*      <div className="max-w-2xl mx-auto">
        <div className="relative mb-4">
          <label htmlFor="Search" className="sr-only">
            {" "}
            Search{" "}
          </label>

          <input
            type="text"
            id="Search"
            placeholder="Search for..."
            className="w-full rounded-md border-gray-200 p-2.5 pe-10 shadow-md sm:text-sm  outline-none"
            onChange={(e) => {
              if (data) {
                let test = searchItems(e.target.value, allItems);
                setresult_search(test);
              }
            }}
          />

          <span className="absolute inset-y-0 end-0 grid w-10 place-content-center">
            <button type="button" className="text-gray-600 hover:text-gray-700">
              <span className="sr-only">Search</span>

              <svg
                xmlns="http://www.w3.org/2000/svg"
                fill="none"
                viewBox="0 0 24 24"
                strokeWidth="1.5"
                stroke="currentColor"
                className="h-4 w-4"
              >
                <path
                  strokeLinecap="round"
                  strokeLinejoin="round"
                  d="M21 21l-5.197-5.197m0 0A7.5 7.5 0 105.196 5.196a7.5 7.5 0 0010.607 10.607z"
                />
              </svg>
            </button>
          </span>
        </div>
        <div className="max-h-[60vh] overflow-y-auto mb-10 p-2.5 shadow-xl rounded-lg">
        {result_search?.map((d, index) => {
          return (
            <div
              key={index}
              className="flow-root bg-white border border-gray-400 py-3 shadow-sm my-2"
            >
              <dl className="-my-3 divide-y divide-gray-100 text-sm">
                <div className="grid grid-cols-1 gap-1 p-3 even:bg-gray-50 sm:grid-cols-3 sm:gap-4">
                  <dt className="font-medium text-gray-900">Name</dt>
                  <dd className="text-gray-700 sm:col-span-2">
                    {d?.user?.fullName}
                  </dd>
                </div>

                <div className="grid grid-cols-1 gap-1 p-3 even:bg-gray-50 sm:grid-cols-3 sm:gap-4">
                  <dt className="font-medium text-gray-900">Class</dt>
                  <dd className="text-gray-700 sm:col-span-2">
                    {d?.class?.name}
                  </dd>
                </div>

                <div className="grid grid-cols-1 gap-1 p-3 even:bg-gray-50 sm:grid-cols-3 sm:gap-4">
                  <dt className="font-medium text-gray-900">Description</dt>
                  <dd className="text-gray-700 sm:col-span-2">{d?.message}</dd>
                </div>

                <div className="grid grid-cols-1 gap-1 p-3 even:bg-gray-50 sm:grid-cols-3 sm:gap-4">
                  <dt className="font-medium text-gray-900">Periode</dt>
                  <dd className="text-gray-700 sm:col-span-2">{d?.periods}</dd>
                </div>
              </dl>
            </div>
          );
        })}
          <div className="h-[70vh]  max-w-3xl  flex justify-center items-center">
            <LoadingSpinner size={80} strokeWidth={2} />
          </div>
        {status === "loading" ? (
        </div>
      </div>*/}
      <div className="max-w-3xl mx-auto">
        {status === "loading" ? (
          [...Array(5)].map((_, index) => (
            <div
              key={index}
              role="status"
              className=" mb-5 p-4 border border-gray-300 rounded-lg shadow animate-pulse md:p-6 dark:border-gray-700"
            >
              <div className="flex items-center mb-4">
                <svg
                  className="w-10 h-10 me-3 text-gray-200 dark:text-gray-700"
                  aria-hidden="true"
                  xmlns="http://www.w3.org/2000/svg"
                  fill="currentColor"
                  viewBox="0 0 20 20"
                >
                  <path d="M10 0a10 10 0 1 0 10 10A10.011 10.011 0 0 0 10 0Zm0 5a3 3 0 1 1 0 6 3 3 0 0 1 0-6Zm0 13a8.949 8.949 0 0 1-4.951-1.488A3.987 3.987 0 0 1 9 13h2a3.987 3.987 0 0 1 3.951 3.512A8.949 8.949 0 0 1 10 18Z" />
                </svg>
                <div>
                  <div className="h-2.5 bg-gray-200 rounded-full dark:bg-gray-700 w-32 mb-2" />
                  <div className="w-48 h-2 bg-gray-200 rounded-full dark:bg-gray-700" />
                </div>
              </div>
              <div className="flex items-center justify-center h-60 md:h-80 mb-4 bg-gray-300 rounded dark:bg-gray-700">
                <svg
                  className="w-10 h-10 text-gray-200 dark:text-gray-600"
                  aria-hidden="true"
                  xmlns="http://www.w3.org/2000/svg"
                  fill="currentColor"
                  viewBox="0 0 16 20"
                >
                  <path d="M14.066 0H7v5a2 2 0 0 1-2 2H0v11a1.97 1.97 0 0 0 1.934 2h12.132A1.97 1.97 0 0 0 16 18V2a1.97 1.97 0 0 0-1.934-2ZM10.5 6a1.5 1.5 0 1 1 0 2.999A1.5 1.5 0 0 1 10.5 6Zm2.221 10.515a1 1 0 0 1-.858.485h-8a1 1 0 0 1-.9-1.43L5.6 10.039a.978.978 0 0 1 .936-.57 1 1 0 0 1 .9.632l1.181 2.981.541-1a.945.945 0 0 1 .883-.522 1 1 0 0 1 .879.529l1.832 3.438a1 1 0 0 1-.031.988Z" />
                  <path d="M5 5V.13a2.96 2.96 0 0 0-1.293.749L.879 3.707A2.98 2.98 0 0 0 .13 5H5Z" />
                </svg>
              </div>
              <div className="h-2.5 bg-gray-200 rounded-full dark:bg-gray-700 w-48 mb-4" />
              <div className="h-2 bg-gray-200 rounded-full dark:bg-gray-700 mb-2.5" />
              <div className="h-2 bg-gray-200 rounded-full dark:bg-gray-700 mb-2.5" />
              <div className="h-2 bg-gray-200 rounded-full dark:bg-gray-700" />
              <span className="sr-only">Loading...</span>
            </div>
          ))
        ) : (
          <>
            {data?.pages.map((data, index) => (
              <div key={index}>
                {data.data.items.length > 0 ? (
                  data.data.items.map((data: any) => {
                    return (
                      <div
                        ref={ref}
                        key={data.id}
                        className={`bg-white pb-2 border-y md:border mb-2.5 shadow-md`}
                      >
                        {/* header title */}
                        <div className="w-full flex px-2 md:px-6 py-3 justify-between">
                          <div className="flex w-4/5">
                            <Link
                              href={`/profile/${data?.user?.id}` ?? `/profile`}
                            >
                              <div className="relative w-14 lg:w-16 h-14 lg:h-16">
                                <Image
                                  blurDataURL="/static/images/default/placeholder.png"
                                  placeholder="blur"
                                  width={60}
                                  height={60}
                                  src={
                                    data.user?.profilePictureLink ??
                                    `/static/images/logokedi.svg`
                                  }
                                  className="absolute inset-0 h-full w-full object-cover  rounded-lg shadow-md border border-gray-100"
                                  alt=""
                                />
                              </div>
                            </Link>

                            <div className="ml-2.5 sm:ml-4 self-center">
                              <div className="flex items-center">
                                <Link
                                  href={
                                    `/profile/${data?.user?.id}` ?? `/profile`
                                  }
                                  className="font-semibold text-[#302B2B] hover:text-blue-600 duration-300 sm:text-lg"
                                >
                                  {data.user?.fullName ?? ""}
                                </Link>
                                {data.user?.isVerifiedAccount === true ? (
                                  <div className=" text-[#9D21E6]">
                                    <svg
                                      stroke="currentColor"
                                      fill="currentColor"
                                      strokeWidth={0}
                                      viewBox="0 0 24 24"
                                      className="ml-2 text-[#9D21E6]"
                                      height={17}
                                      width={17}
                                      xmlns="http://www.w3.org/2000/svg"
                                    >
                                      <path fill="none" d="M0 0h24v24H0z" />
                                      <path d="M23 12l-2.44-2.79.34-3.69-3.61-.82-1.89-3.2L12 2.96 8.6 1.5 6.71 4.69 3.1 5.5l.34 3.7L1 12l2.44 2.79-.34 3.7 3.61.82L8.6 22.5l3.4-1.47 3.4 1.46 1.89-3.19 3.61-.82-.34-3.69L23 12zm-12.91 4.72l-3.8-3.81 1.48-1.48 2.32 2.33 5.85-5.87 1.48 1.48-7.33 7.35z" />
                                    </svg>
                                  </div>
                                ) : null}
                              </div>

                              <p className="-mt-1 text-[#302B2B] text-sm hidden">
                                {data.class?.name}
                              </p>
                            </div>
                          </div>
                          <div className="w-1/5 flex justify-end items-center gap-x-1.5 sm:gap-x-3 mr-2">
                            {!data.user?.isFriend ? (
                              <div
                                onClick={() => addFriend.mutate(data.user.id)}
                                className="text-[#9D21E6] cursor-pointer"
                              >
                                {addFriend.isLoading ? (
                                  <LoadingSpinner size={24} />
                                ) : (
                                  <svg
                                    xmlns="http://www.w3.org/2000/svg"
                                    width="24px"
                                    height="24px"
                                    viewBox="0 0 24 24"
                                  >
                                    <g
                                      fill="none"
                                      stroke="currentColor"
                                      strokeLinecap="round"
                                      strokeWidth="2.5"
                                    >
                                      <path
                                        strokeDasharray="20"
                                        strokeDashoffset="20"
                                        d="M3 21V20C3 17.7909 4.79086 16 7 16H11C13.2091 16 15 17.7909 15 20V21"
                                      >
                                        <animate
                                          fill="freeze"
                                          attributeName="stroke-dashoffset"
                                          dur="0.4s"
                                          values="20;0"
                                        ></animate>
                                      </path>
                                      <path
                                        strokeDasharray="20"
                                        strokeDashoffset="20"
                                        d="M9 13C7.34315 13 6 11.6569 6 10C6 8.34315 7.34315 7 9 7C10.6569 7 12 8.34315 12 10C12 11.6569 10.6569 13 9 13Z"
                                      >
                                        <animate
                                          fill="freeze"
                                          attributeName="stroke-dashoffset"
                                          begin="0.5s"
                                          dur="0.4s"
                                          values="20;0"
                                        ></animate>
                                      </path>
                                      <path
                                        strokeDasharray="8"
                                        strokeDashoffset="8"
                                        d="M15 6H21"
                                      >
                                        <animate
                                          fill="freeze"
                                          attributeName="stroke-dashoffset"
                                          begin="1s"
                                          dur="0.2s"
                                          values="8;0"
                                        ></animate>
                                      </path>
                                      <path
                                        strokeDasharray="8"
                                        strokeDashoffset="8"
                                        d="M18 3V9"
                                      >
                                        <animate
                                          fill="freeze"
                                          attributeName="stroke-dashoffset"
                                          begin="1.2s"
                                          dur="0.2s"
                                          values="8;0"
                                        ></animate>
                                      </path>
                                    </g>
                                  </svg>
                                )}
                              </div>
                            ) : null}
                            <div className="relative hidden">
                              <button className="pt-2 peer text-gray-500 transition-all duration-200 hover:text-sky-500 ">
                                <svg
                                  xmlns="http://www.w3.org/2000/svg"
                                  fill="none"
                                  viewBox="0 0 24 24"
                                  strokeWidth={2.5}
                                  stroke="currentColor"
                                  className="h-[24px] w-[24px] rotate-90"
                                >
                                  <path
                                    strokeLinecap="round"
                                    strokeLinejoin="round"
                                    d="M6.75 12a.75.75 0 11-1.5 0 .75.75 0 011.5 0zM12.75 12a.75.75 0 11-1.5 0 .75.75 0 011.5 0zM18.75 12a.75.75 0 11-1.5 0 .75.75 0 011.5 0z"
                                  />
                                </svg>
                              </button>
                              <div className='invisible absolute -right-2 -top-5 z-10 w-[200px] rounded-md border border-slate-200 opacity-0 transition-all duration-300 after:absolute after:top-0 after:-z-20 after:inline-block after:h-full after:w-full after:rounded-md after:bg-white  after:content-[""] peer-focus:visible peer-focus:top-0 peer-focus:opacity-100'>
                                <div className="p-1">
                                  <button
                                    className="flex w-full items-center gap-2 rounded-md p-2 text-sm hover:bg-purple-100"
                                    role="menuitem"
                                  >
                                    <svg
                                      xmlns="http://www.w3.org/2000/svg"
                                      className="h-4 w-4"
                                      fill="none"
                                      viewBox="0 0 24 24"
                                      stroke="currentColor"
                                      strokeWidth={2}
                                    >
                                      <path
                                        strokeLinecap="round"
                                        strokeLinejoin="round"
                                        d="M16.862 4.487l1.687-1.688a1.875 1.875 0 112.652 2.652L10.582 16.07a4.5 4.5 0 01-1.897 1.13L6 18l.8-2.685a4.5 4.5 0 011.13-1.897l8.932-8.931zm0 0L19.5 7.125M18 14v4.75A2.25 2.25 0 0115.75 21H5.25A2.25 2.25 0 013 18.75V8.25A2.25 2.25 0 015.25 6H10"
                                      />
                                    </svg>
                                    Menu 1
                                  </button>
                                  <button
                                    type="submit"
                                    className="flex w-full items-center gap-2 rounded-md p-2 text-sm hover:bg-purple-100"
                                    role="menuitem"
                                  >
                                    <svg
                                      xmlns="http://www.w3.org/2000/svg"
                                      className="h-4 w-4"
                                      fill="none"
                                      viewBox="0 0 24 24"
                                      stroke="currentColor"
                                      strokeWidth={2}
                                    >
                                      <path
                                        strokeLinecap="round"
                                        strokeLinejoin="round"
                                        d="M19 7l-.867 12.142A2 2 0 0116.138 21H7.862a2 2 0 01-1.995-1.858L5 7m5 4v6m4-6v6m1-10V4a1 1 0 00-1-1h-4a1 1 0 00-1 1v3M4 7h16"
                                      />
                                    </svg>
                                    Menu 2
                                  </button>
                                </div>
                              </div>
                            </div>
                          </div>
                        </div>

                        {/* image card */}

                        <Slider {...settings}>
                          {data?.media?.length > 0
                            ? data?.media?.map((x: any, index: number) => (
                                <Image
                                  blurDataURL="/static/images/default/placeholder.png"
                                  placeholder="blur"
                                  key={index}
                                  src={x}
                                  onErrorCapture={() => console.log("ERROR")}
                                  onError={(event) => {
                                    event.target.srcset =
                                      "/static/images/temp_default.webp";
                                  }}
                                  alt={x}
                                  width={500}
                                  height={500}
                                  loading="lazy"
                                />
                              ))
                            : ["/static/images/temp_default.webp"].map(
                                (x: any, index: number) => (
                                  <Image
                                    key={index}
                                    blurDataURL="/static/images/default/placeholder.png"
                                    placeholder="blur"
                                    src={x}
                                    onErrorCapture={() => console.log("ERROR")}
                                    onError={(event) => {
                                      event.target.srcset =
                                        "/static/images/temp_default.webp";
                                    }}
                                    alt={x}
                                    width={500}
                                    height={500}
                                    loading="lazy"
                                  />
                                ),
                              )}
                        </Slider>

                        {/* label gambar */}
                        {data.hyperlinkLink && (
                          <>
                            <Dialog>
                              <Link
                                href={"/class/" + data?.class?.id}
                                className="flex cursor-pointer relative text-white px-3 lg:px-5 w-full h-10 font-bold items-center justify-between bg-[#1DA26A]"
                              >
                                <p className="text-white">
                                  {data.hyperlinkName}
                                </p>
                                <svg
                                  xmlns="http://www.w3.org/2000/svg"
                                  fill="none"
                                  viewBox="0 0 24 24"
                                  strokeWidth={3}
                                  stroke="currentColor"
                                  className="w-5 h-5"
                                >
                                  <path
                                    strokeLinecap="round"
                                    strokeLinejoin="round"
                                    d="M8.25 4.5l7.5 7.5-7.5 7.5"
                                  />
                                </svg>
                              </Link>
                              {/*<DialogTrigger className="flex cursor-pointer relative text-white px-3 lg:px-5 w-full h-10 font-bold items-center justify-between bg-[#1DA26A]">
                              <p className="text-white">{data.hyperlinkName}</p>
                              <svg
                                xmlns="http://www.w3.org/2000/svg"
                                fill="none"
                                viewBox="0 0 24 24"
                                strokeWidth={3}
                                stroke="currentColor"
                                className="w-5 h-5"
                              >
                                <path
                                  strokeLinecap="round"
                                  strokeLinejoin="round"
                                  d="M8.25 4.5l7.5 7.5-7.5 7.5"
                                />
                              </svg>
                            </DialogTrigger>*/}
                              <DialogContent>
                                <DialogHeader>
                                  <DialogTitle className="text-center  text-[22px] sm:text-2xl font-semibold text-[#9D21E6] mx-auto max-w-[320px]">
                                    Silahkan Join Melalui Aplikasi KELAS DIGITAL
                                  </DialogTitle>
                                </DialogHeader>
                                <div>
                                  <Image
                                    src={downloadapp}
                                    alt="barcode download"
                                    className="w-auto h-auto max-w-[250px] sm:max-w-[300px] max-h-[250px] sm:max-h-[300px] mx-auto"
                                  />
                                  <div className="mt-7 [@media(max-width:400px)]:flex-wrap flex gap-3.5 items-center justify-center">
                                    {[
                                      {
                                        src: appgoogle,
                                        alt: "Google Play",
                                        href: "https://play.google.com/store/apps/details?id=id.kelasdigital.lite",
                                      },
                                      {
                                        src: appstore,
                                        alt: "App Store",
                                        href: "https://apps.apple.com/id/app/kelas-digital-lite/id1611032222",
                                      },
                                      {
                                        src: appgalery,
                                        alt: "App Gallery",
                                        href: "https://appgallery.huawei.com/app/C106887823",
                                      },
                                    ].map((image, index) => (
                                      <Link
                                        key={index}
                                        href={image.href}
                                        target="_blank"
                                        rel="noopener noreferrer"
                                      >
                                        <Image
                                          src={image.src}
                                          className={`w-auto h-[50px]`}
                                          alt={image.alt}
                                        />
                                      </Link>
                                    ))}
                                  </div>
                                </div>
                              </DialogContent>
                            </Dialog>
                          </>
                        )}
                        <article className="text-start px-3 lg:px-5 pt-4">
                          <div className="flex items-center mb-3 justify-between">
                            {/* share dan simpan */}
                            <div className="w-1/2 flex gap-x-3">
                              {toggleLike.isLoading &&
                              toggleLike.variables?.commentId === undefined ? (
                                <div className="w-[40px]">
                                  <LoadingSpinner size={24} />
                                </div>
                              ) : (
                                <button
                                  onClick={() =>
                                    toggleLike.mutate({
                                      postId: data.id,
                                      commentId: undefined,
                                    })
                                  }
                                  className="flex items-center cursor-pointer min-w-[40px] gap-x-1"
                                >
                                  <svg
                                    xmlns="http://www.w3.org/2000/svg"
                                    width="24px"
                                    height="24px"
                                    viewBox="0 0 32 32"
                                  >
                                    {data.isLikes ? (
                                      <path
                                        fill="#9D21E6"
                                        d="M2 16h5v14H2zm21 14H9V15.197l3.042-4.563l.845-5.917A2.01 2.01 0 0 1 14.867 3H15a3.003 3.003 0 0 1 3 3v6h8a4.005 4.005 0 0 1 4 4v7a7.008 7.008 0 0 1-7 7z"
                                      ></path>
                                    ) : (
                                      <path
                                        fill="#444"
                                        d="M26 12h-6V6a3.003 3.003 0 0 0-3-3h-2.133a2.01 2.01 0 0 0-1.98 1.717l-.845 5.917L8.465 16H2v14h21a7.008 7.008 0 0 0 7-7v-7a4.005 4.005 0 0 0-4-4ZM8 28H4V18h4Zm20-5a5.006 5.006 0 0 1-5 5H10V17.303l3.958-5.937l.91-6.366H17a1 1 0 0 1 1 1v8h8a2.002 2.002 0 0 1 2 2Z"
                                      ></path>
                                    )}
                                  </svg>

                                  <span
                                    className={`ml-1 text-sm font-medium ${
                                      data.isLikes ? "text-[#9D21E6]" : null
                                    }`}
                                  >
                                    {data.totalLikes ?? 0}
                                  </span>
                                </button>
                              )}
                              {data.class && (
                                <div className="flex items-center cursor-pointer min-w-[40px] gap-x-1">
                                  <svg
                                    xmlns="http://www.w3.org/2000/svg"
                                    width="24px"
                                    height="24px"
                                    viewBox="0 0 32 32"
                                  >
                                    <path
                                      fill="#444"
                                      d="M30 30h-2v-5a5.006 5.006 0 0 0-5-5v-2a7.008 7.008 0 0 1 7 7zm-8 0h-2v-5a5.006 5.006 0 0 0-5-5H9a5.006 5.006 0 0 0-5 5v5H2v-5a7.008 7.008 0 0 1 7-7h6a7.008 7.008 0 0 1 7 7zM20 2v2a5 5 0 0 1 0 10v2a7 7 0 0 0 0-14zm-8 2a5 5 0 1 1-5 5a5 5 0 0 1 5-5m0-2a7 7 0 1 0 7 7a7 7 0 0 0-7-7z"
                                    ></path>
                                  </svg>

                                  <span className="ml-1 text-sm">
                                    {data.totalMember}
                                  </span>
                                </div>
                              )}
                              {isloadComment[data.id] ? (
                                <div className="w-[40px]">
                                  <LoadingSpinner size={24} />
                                </div>
                              ) : (
                                <button
                                  onClick={() => updateComments(data.id)}
                                  className="flex items-center cursor-pointer min-w-[40px] gap-x-1"
                                >
                                  <svg
                                    xmlns="http://www.w3.org/2000/svg"
                                    width="24px"
                                    height="24px"
                                    viewBox="0 0 32 32"
                                  >
                                    <path
                                      fill="#444"
                                      d="M17.74 30L16 29l4-7h6a2 2 0 0 0 2-2V8a2 2 0 0 0-2-2H6a2 2 0 0 0-2 2v12a2 2 0 0 0 2 2h9v2H6a4 4 0 0 1-4-4V8a4 4 0 0 1 4-4h20a4 4 0 0 1 4 4v12a4 4 0 0 1-4 4h-4.84Z"
                                    ></path>
                                    <path
                                      fill="#444"
                                      d="M8 10h16v2H8zm0 6h10v2H8z"
                                    ></path>
                                  </svg>

                                  <span className="ml-1 text-sm">
                                    {data.totalComments ?? 0}
                                  </span>
                                </button>
                              )}
                            </div>

                            {/* Simpan */}
                            <div className=" flex gap-x-3">
                              <Dialog>
                                {data?.class?.id && (
                                  <DialogTrigger className="flex cursor-pointer relative text-white px-3 lg:px-5 w-full h-10 font-bold items-center justify-between">
                                    <svg
                                      xmlns="http://www.w3.org/2000/svg"
                                      width="24px"
                                      height="24px"
                                      viewBox="0 0 32 32"
                                    >
                                      <path
                                        fill="#444"
                                        d="M23 20a5 5 0 0 0-3.89 1.89l-7.31-4.57a4.46 4.46 0 0 0 0-2.64l7.31-4.57A5 5 0 1 0 18 7a4.79 4.79 0 0 0 .2 1.32l-7.31 4.57a5 5 0 1 0 0 6.22l7.31 4.57A4.79 4.79 0 0 0 18 25a5 5 0 1 0 5-5Zm0-16a3 3 0 1 1-3 3a3 3 0 0 1 3-3ZM7 19a3 3 0 1 1 3-3a3 3 0 0 1-3 3Zm16 9a3 3 0 1 1 3-3a3 3 0 0 1-3 3Z"
                                      ></path>
                                    </svg>
                                  </DialogTrigger>
                                )}
                                <DialogContent>
                                  <DialogHeader>
                                    <DialogTitle className="text-center  text-[22px] sm:text-2xl font-semibold text-[#9D21E6] mx-auto max-w-[320px]">
                                      Share this link
                                    </DialogTitle>
                                  </DialogHeader>
                                  <div className="my-4 hidden">
                                    <div className="flex justify-around my-4">
                                      {/*FACEBOOK ICON*/}
                                      <Link
                                        className="border hover:bg-[#1877f2] w-12 h-12 fill-[#1877f2] hover:fill-white border-blue-200 rounded-full flex items-center justify-center shadow-xl hover:shadow-blue-500/50 cursor-pointer"
                                        href={`https://web.facebook.com/sharer.php?u=${
                                          data?.hyperlinkLink ?? ""
                                        } ?? ""`}
                                        target="_blank"
                                        rel="noopener noreferrer"
                                      >
                                        <svg
                                          xmlns="http://www.w3.org/2000/svg"
                                          width={24}
                                          height={24}
                                          viewBox="0 0 24 24"
                                        >
                                          <path d="M13.397 20.997v-8.196h2.765l.411-3.209h-3.176V7.548c0-.926.258-1.56 1.587-1.56h1.684V3.127A22.336 22.336 0 0 0 14.201 3c-2.444 0-4.122 1.492-4.122 4.231v2.355H7.332v3.209h2.753v8.202h3.312z" />
                                        </svg>
                                      </Link>
                                      {/*TWITTER ICON*/}
                                      <Link
                                        href={`https://twitter.com/intent/tweet?text= Kelas Digital Share&url=${
                                          data?.hyperlinkLink ?? ""
                                        }`}
                                        target="_blank"
                                        rel="noopener noreferrer"
                                        className="border hover:bg-[#1d9bf0] w-12 h-12 fill-[#1d9bf0] hover:fill-white border-blue-200 rounded-full flex items-center justify-center shadow-xl hover:shadow-sky-500/50 cursor-pointer"
                                      >
                                        <svg
                                          xmlns="http://www.w3.org/2000/svg"
                                          width={24}
                                          height={24}
                                          viewBox="0 0 24 24"
                                        >
                                          <path d="M19.633 7.997c.013.175.013.349.013.523 0 5.325-4.053 11.461-11.46 11.461-2.282 0-4.402-.661-6.186-1.809.324.037.636.05.973.05a8.07 8.07 0 0 0 5.001-1.721 4.036 4.036 0 0 1-3.767-2.793c.249.037.499.062.761.062.361 0 .724-.05 1.061-.137a4.027 4.027 0 0 1-3.23-3.953v-.05c.537.299 1.16.486 1.82.511a4.022 4.022 0 0 1-1.796-3.354c0-.748.199-1.434.548-2.032a11.457 11.457 0 0 0 8.306 4.215c-.062-.3-.1-.611-.1-.923a4.026 4.026 0 0 1 4.028-4.028c1.16 0 2.207.486 2.943 1.272a7.957 7.957 0 0 0 2.556-.973 4.02 4.02 0 0 1-1.771 2.22 8.073 8.073 0 0 0 2.319-.624 8.645 8.645 0 0 1-2.019 2.083z" />
                                        </svg>
                                      </Link>
                                      {/*INSTAGRAM ICON*/}
                                      <Link
                                        href="#"
                                        // href={`whatsapp://send?text=${
                                        //   data?.hyperlinkLink ?? ""
                                        // }`}
                                        // target="_blank"
                                        // rel="noopener noreferrer"
                                        onClick={() =>
                                          alert(
                                            "Fitur masih dalam pengembangan",
                                          )
                                        }
                                        className="border hover:bg-[#bc2a8d] w-12 h-12 fill-[#bc2a8d] hover:fill-white border-pink-200 rounded-full flex items-center justify-center shadow-xl hover:shadow-pink-500/50 cursor-pointer"
                                      >
                                        <svg
                                          xmlns="http://www.w3.org/2000/svg"
                                          width={24}
                                          height={24}
                                          viewBox="0 0 24 24"
                                        >
                                          <path d="M11.999 7.377a4.623 4.623 0 1 0 0 9.248 4.623 4.623 0 0 0 0-9.248zm0 7.627a3.004 3.004 0 1 1 0-6.008 3.004 3.004 0 0 1 0 6.008z" />
                                          <circle
                                            cx="16.806"
                                            cy="7.207"
                                            r="1.078"
                                          />
                                          <path d="M20.533 6.111A4.605 4.605 0 0 0 17.9 3.479a6.606 6.606 0 0 0-2.186-.42c-.963-.042-1.268-.054-3.71-.054s-2.755 0-3.71.054a6.554 6.554 0 0 0-2.184.42 4.6 4.6 0 0 0-2.633 2.632 6.585 6.585 0 0 0-.419 2.186c-.043.962-.056 1.267-.056 3.71 0 2.442 0 2.753.056 3.71.015.748.156 1.486.419 2.187a4.61 4.61 0 0 0 2.634 2.632 6.584 6.584 0 0 0 2.185.45c.963.042 1.268.055 3.71.055s2.755 0 3.71-.055a6.615 6.615 0 0 0 2.186-.419 4.613 4.613 0 0 0 2.633-2.633c.263-.7.404-1.438.419-2.186.043-.962.056-1.267.056-3.71s0-2.753-.056-3.71a6.581 6.581 0 0 0-.421-2.217zm-1.218 9.532a5.043 5.043 0 0 1-.311 1.688 2.987 2.987 0 0 1-1.712 1.711 4.985 4.985 0 0 1-1.67.311c-.95.044-1.218.055-3.654.055-2.438 0-2.687 0-3.655-.055a4.96 4.96 0 0 1-1.669-.311 2.985 2.985 0 0 1-1.719-1.711 5.08 5.08 0 0 1-.311-1.669c-.043-.95-.053-1.218-.053-3.654 0-2.437 0-2.686.053-3.655a5.038 5.038 0 0 1 .311-1.687c.305-.789.93-1.41 1.719-1.712a5.01 5.01 0 0 1 1.669-.311c.951-.043 1.218-.055 3.655-.055s2.687 0 3.654.055a4.96 4.96 0 0 1 1.67.311 2.991 2.991 0 0 1 1.712 1.712 5.08 5.08 0 0 1 .311 1.669c.043.951.054 1.218.054 3.655 0 2.436 0 2.698-.043 3.654h-.011z" />
                                        </svg>
                                      </Link>
                                      {/*WHATSAPP ICON*/}
                                      <Link
                                        href={`whatsapp://send?text=${
                                          data?.hyperlinkLink ?? ""
                                        }`}
                                        target="_blank"
                                        rel="noopener noreferrer"
                                        className="border hover:bg-[#25D366] w-12 h-12 fill-[#25D366] hover:fill-white border-green-200 rounded-full flex items-center justify-center shadow-xl hover:shadow-green-500/50 cursor-pointer"
                                      >
                                        <svg
                                          xmlns="http://www.w3.org/2000/svg"
                                          width={24}
                                          height={24}
                                          viewBox="0 0 24 24"
                                        >
                                          <path
                                            fillRule="evenodd"
                                            clipRule="evenodd"
                                            d="M18.403 5.633A8.919 8.919 0 0 0 12.053 3c-4.948 0-8.976 4.027-8.978 8.977 0 1.582.413 3.126 1.198 4.488L3 21.116l4.759-1.249a8.981 8.981 0 0 0 4.29 1.093h.004c4.947 0 8.975-4.027 8.977-8.977a8.926 8.926 0 0 0-2.627-6.35m-6.35 13.812h-.003a7.446 7.446 0 0 1-3.798-1.041l-.272-.162-2.824.741.753-2.753-.177-.282a7.448 7.448 0 0 1-1.141-3.971c.002-4.114 3.349-7.461 7.465-7.461a7.413 7.413 0 0 1 5.275 2.188 7.42 7.42 0 0 1 2.183 5.279c-.002 4.114-3.349 7.462-7.461 7.462m4.093-5.589c-.225-.113-1.327-.655-1.533-.73-.205-.075-.354-.112-.504.112s-.58.729-.711.879-.262.168-.486.056-.947-.349-1.804-1.113c-.667-.595-1.117-1.329-1.248-1.554s-.014-.346.099-.458c.101-.1.224-.262.336-.393.112-.131.149-.224.224-.374s.038-.281-.019-.393c-.056-.113-.505-1.217-.692-1.666-.181-.435-.366-.377-.504-.383a9.65 9.65 0 0 0-.429-.008.826.826 0 0 0-.599.28c-.206.225-.785.767-.785 1.871s.804 2.171.916 2.321c.112.15 1.582 2.415 3.832 3.387.536.231.954.369 1.279.473.537.171 1.026.146 1.413.089.431-.064 1.327-.542 1.514-1.066.187-.524.187-.973.131-1.067-.056-.094-.207-.151-.43-.263"
                                          />
                                        </svg>
                                      </Link>
                                      {/*TELEGRAM ICON*/}
                                      <Link
                                        href={`https://t.me/share/url?url=${
                                          data?.hyperlinkLink ?? ""
                                        }`}
                                        target="_blank"
                                        rel="noopener noreferrer"
                                        className="border hover:bg-[#229ED9] w-12 h-12 fill-[#229ED9] hover:fill-white border-sky-200 rounded-full flex items-center justify-center shadow-xl hover:shadow-sky-500/50 cursor-pointer"
                                      >
                                        <svg
                                          xmlns="http://www.w3.org/2000/svg"
                                          width={24}
                                          height={24}
                                          viewBox="0 0 24 24"
                                        >
                                          <path d="m20.665 3.717-17.73 6.837c-1.21.486-1.203 1.161-.222 1.462l4.552 1.42 10.532-6.645c.498-.303.953-.14.579.192l-8.533 7.701h-.002l.002.001-.314 4.692c.46 0 .663-.211.921-.46l2.211-2.15 4.599 3.397c.848.467 1.457.227 1.668-.785l3.019-14.228c.309-1.239-.473-1.8-1.282-1.434z" />
                                        </svg>
                                      </Link>
                                    </div>
                                    <p className="text-sm italic">
                                      Or copy link
                                    </p>
                                  </div>
                                  {/*BOX LINK*/}
                                  <div className="border-2 border-gray-200 rounded-lg flex gap-x-2 justify-between items-center mt-4 py-2">
                                    <svg
                                      xmlns="http://www.w3.org/2000/svg"
                                      width={24}
                                      height={24}
                                      viewBox="0 0 24 24"
                                      className="fill-gray-500 ml-2"
                                    >
                                      <path d="M8.465 11.293c1.133-1.133 3.109-1.133 4.242 0l.707.707 1.414-1.414-.707-.707c-.943-.944-2.199-1.465-3.535-1.465s-2.592.521-3.535 1.465L4.929 12a5.008 5.008 0 0 0 0 7.071 4.983 4.983 0 0 0 3.535 1.462A4.982 4.982 0 0 0 12 19.071l.707-.707-1.414-1.414-.707.707a3.007 3.007 0 0 1-4.243 0 3.005 3.005 0 0 1 0-4.243l2.122-2.121z" />
                                      <path d="m12 4.929-.707.707 1.414 1.414.707-.707a3.007 3.007 0 0 1 4.243 0 3.005 3.005 0 0 1 0 4.243l-2.122 2.121c-1.133 1.133-3.109 1.133-4.242 0L10.586 12l-1.414 1.414.707.707c.943.944 2.199 1.465 3.535 1.465s2.592-.521 3.535-1.465L19.071 12a5.008 5.008 0 0 0 0-7.071 5.006 5.006 0 0 0-7.071 0z" />
                                    </svg>
                                    <input
                                      className="w-full outline-none bg-transparent"
                                      type="text"
                                      placeholder="link"
                                      defaultValue={
                                        window.location.host +
                                        "/class/" +
                                        data?.class?.id
                                      }
                                    />
                                    <button
                                      onClick={() => {
                                        navigator.clipboard.writeText(
                                          window.location.host +
                                            "/class/" +
                                            data?.class?.id,
                                        );
                                        toast.success("Sukses menyalin text");
                                      }}
                                      className="bg-purple-500 text-white font-medium rounded-lg text-sm py-2 px-5 mr-2 hover:bg-indigo-600"
                                    >
                                      Copy
                                    </button>
                                  </div>
                                </DialogContent>
                              </Dialog>
                              {removeFavorite.isLoading ||
                              toggleFavorite.isLoading ? (
                                <div className="">
                                  <LoadingSpinner size={24} />
                                </div>
                              ) : (
                                <button
                                  onClick={
                                    data.isFavorites
                                      ? () => removeFavorite.mutate(data.id)
                                      : () => toggleFavorite.mutate(data.id)
                                  }
                                  className="flex items-center cursor-pointer gap-x-1"
                                >
                                  <svg
                                    xmlns="http://www.w3.org/2000/svg"
                                    width="24px"
                                    height="24px"
                                    viewBox="0 0 32 32"
                                  >
                                    {data.isFavorites ? (
                                      <path
                                        fill="#9D21E6"
                                        d="M24 2H8a2 2 0 0 0-2 2v26l10-5.054L26 30V4a2 2 0 0 0-2-2Z"
                                      ></path>
                                    ) : (
                                      <path
                                        fill="#444"
                                        d="M24 4v22.75l-7.1-3.59l-.9-.45l-.9.45L8 26.75V4h16m0-2H8a2 2 0 0 0-2 2v26l10-5l10 5V4a2 2 0 0 0-2-2Z"
                                      ></path>
                                    )}
                                  </svg>
                                </button>
                              )}
                            </div>
                          </div>

                          <h1 className="text-lg lg:text-xl font-medium mb-1.5">
                            {data.class?.name ? data.class.name : null}
                          </h1>
                          <div className="text-gray-500 font-medium mb-1.5">
                            {data.periods ? "Periode : " + data.periods : null}
                          </div>
                          <div className="whitespace-pre-wrap break-word">
                            {expandedPosts.includes(data.id)
                              ? data.message
                              : data.message?.substr(0, 100)}
                            {data.message?.length >= 100 && (
                              <button
                                className="bg-transparent text-[#9D21E6] ml-2"
                                onClick={() => toggleReadMore(data.id)}
                              >
                                {expandedPosts.includes(data.id)
                                  ? "less"
                                  : "...more"}
                              </button>
                            )}
                          </div>

                          <div className="mt-4 mb-1.5 capitalize text-gray-500 sm:text-normal text-[15px]">
                            {dayjs(data.createdDate).fromNow()}
                          </div>
                        </article>
                        <div>
                          {getComments !== undefined &&
                            getComments?.postId === data.id && (
                              <div className="">
                                {data.totalComments > 0 && (
                                  <h2 className="text-lg lg:text-2xl font-bold text-gray-900  p-3 ">
                                    Komentar ({data.totalComments})
                                  </h2>
                                )}
                                <div
                                  className={`rounded-b-lg ${
                                    toggle ? "pt-2.5 pb-4" : "py-2.5"
                                  }   border-y border-y-300`}
                                >
                                  <div className="grid grid-cols-[auto,1fr] gap-x-3 gap-y-1">
                                    {toggle && (
                                      <p className="hidden ml-[78px] sm:ml-[84px] mt-1 text-[#71767b] focus-within:block">
                                        Membalas{" "}
                                        <Link
                                          href={
                                            `/profile/${user?.id}` ?? `/profile`
                                          }
                                          className="font-medium text-blue-500"
                                        >
                                          {data.user?.fullName ?? ""}
                                        </Link>
                                      </p>
                                    )}
                                  </div>
                                  <div
                                    className={`${
                                      toggle
                                        ? "place-items-start"
                                        : "place-items-center"
                                    } grid grid-cols-[auto,1fr]  gap-x-3 gap-y-1  py-2.5 px-4 sm:px-6`}
                                  >
                                    <Link
                                      href={
                                        `/profile/${user?.id}` ?? `/profile`
                                      }
                                    >
                                      <div className="relative h-12 w-12">
                                        <Image
                                          blurDataURL="/static/images/default/placeholder.png"
                                          placeholder="blur"
                                          width={60}
                                          height={60}
                                          src={
                                            user?.photo ??
                                            `/static/images/logokedi.svg`
                                          }
                                          className="absolute inset-0 h-full w-full object-cover  rounded-lg"
                                          alt={`${
                                            user?.fullName ?? ""
                                          } "Profile Picture"`}
                                        />
                                      </div>
                                    </Link>
                                    <div
                                      onClick={() => setToggle(true)}
                                      className={` ${
                                        toggle ? "flex-col" : ""
                                      } flex h-fit w-full items-center`}
                                    >
                                      <textarea
                                        ref={inputTextRef}
                                        className="h-auto w-full resize-none overflow-hidden border-none bg-transparent pt-1 text-base text-lg outline-none placeholder:text-gray-500 focus:ring-0 disabled:opacity-60"
                                        rows={1}
                                        value={inputText}
                                        disabled={postComments.isLoading}
                                        onChange={(e) =>
                                          setInputText(e.target.value)
                                        }
                                        placeholder="Post your Comment!"
                                      />
                                      {toggle ? (
                                        <div className="mb-3 block w-full border-b border-gray-300 pt-1" />
                                      ) : (
                                        <button className="flex h-fit justify-end rounded-full text-white bg-[#9D21E6] px-3 py-1.5 text-sm font-semibold opacity-60">
                                          Kirim
                                        </button>
                                      )}
                                    </div>
                                  </div>
                                  {toggle && (
                                    <div className="flex items-center justify-end gap-x-2 px-4">
                                      {postComments.isLoading ? (
                                        <LoadingSpinner
                                          size={20}
                                          strokeWidth={4.5}
                                        />
                                      ) : (
                                        <>
                                          <button
                                            className="flex justify-end rounded-full bg-gray-400 px-3 py-1.5 text-sm font-medium duration-300 hover:bg-gray-500 disabled:opacity-60 text-white"
                                            onClick={() => {
                                              setToggle(false);
                                              setInputText("");
                                            }}
                                          >
                                            Batal
                                          </button>
                                          <button
                                            onClick={() =>
                                              postComments.mutate({
                                                postId: data.id,
                                                content: inputText,
                                              })
                                            }
                                            className="flex h-fit justify-end rounded-full text-white bg-[#9D21E6] px-3 py-1.5 text-sm font-semibold disabled:opacity-60"
                                            disabled={inputText === ""}
                                          >
                                            Kirim
                                          </button>
                                        </>
                                      )}
                                    </div>
                                  )}
                                </div>
                                <div className=" divide-y">
                                  {getComments.items.map((item: any) => (
                                    <div key={item.id}>
                                      <div className=" grid grid-cols-[auto,1fr] gap-x-3 gap-y-1 px-4 sm:px-6 py-3.5 hover:bg-slate-100 ">
                                        <div className="flex min-w-[45px]">
                                          <Link
                                            href={
                                              `/profile/${item?.user?.id}` ??
                                              `/profile`
                                            }
                                          >
                                            <div className="relative h-12 w-12">
                                              <Image
                                                blurDataURL="/static/images/default/placeholder.png"
                                                placeholder="blur"
                                                width={60}
                                                height={60}
                                                src={
                                                  item?.user
                                                    ?.profilePictureLink ??
                                                  `/static/images/logokedi.svg`
                                                }
                                                className="absolute inset-0 h-full w-full object-cover  rounded-lg"
                                                alt=""
                                              />
                                            </div>
                                          </Link>
                                        </div>
                                        <div className="min-w-0">
                                          <div className="flex justify-between gap-x-2.5 text-[15px]">
                                            <div className="xs:overflow-visible xs:whitespace-normal mb-1 flex items-center gap-x-1.5 truncate leading-5 [@media(max-width:360px)]:flex-wrap">
                                              <Link
                                                className="truncate font-semibold capitalize text-base"
                                                href={
                                                  `/profile/${item?.user?.id}` ??
                                                  `/profile`
                                                }
                                              >
                                                {item.user?.fullName ?? ""}
                                              </Link>
                                              <span className="text-[#71767b]">
                                                ·
                                              </span>
                                              <a className="text-[#71767b] sm:text-base text-sm">
                                                {dayjs(
                                                  item.createdDate,
                                                ).fromNow()}
                                              </a>
                                            </div>
                                            {item.user.id === user.id && (
                                              <div className="relative h-[22px]">
                                                <button className="peer text-gray-400 transition-all duration-200 hover:text-sky-500 ">
                                                  <svg
                                                    xmlns="http://www.w3.org/2000/svg"
                                                    fill="none"
                                                    viewBox="0 0 24 24"
                                                    strokeWidth={2}
                                                    stroke="currentColor"
                                                    className="h-[22px] w-[22px]"
                                                  >
                                                    <path
                                                      strokeLinecap="round"
                                                      strokeLinejoin="round"
                                                      d="M6.75 12a.75.75 0 11-1.5 0 .75.75 0 011.5 0zM12.75 12a.75.75 0 11-1.5 0 .75.75 0 011.5 0zM18.75 12a.75.75 0 11-1.5 0 .75.75 0 011.5 0z"
                                                    />
                                                  </svg>
                                                </button>

                                                <div className='invisible absolute -right-2 -top-5 z-10 w-[200px] rounded-md border border-slate-200 opacity-0 transition-all duration-300 after:absolute after:top-0 after:-z-20 after:inline-block after:h-full after:w-full after:rounded-md after:bg-white  after:content-[""] peer-focus:visible peer-focus:top-0 peer-focus:opacity-100'>
                                                  <div className="p-1">
                                                    <button
                                                      className="flex w-full items-center gap-2 rounded-md p-2 text-sm hover:bg-purple-100"
                                                      role="menuitem"
                                                    >
                                                      <svg
                                                        xmlns="http://www.w3.org/2000/svg"
                                                        className="h-4 w-4"
                                                        fill="none"
                                                        viewBox="0 0 24 24"
                                                        stroke="currentColor"
                                                        strokeWidth={2}
                                                      >
                                                        <path
                                                          strokeLinecap="round"
                                                          strokeLinejoin="round"
                                                          d="M16.862 4.487l1.687-1.688a1.875 1.875 0 112.652 2.652L10.582 16.07a4.5 4.5 0 01-1.897 1.13L6 18l.8-2.685a4.5 4.5 0 011.13-1.897l8.932-8.931zm0 0L19.5 7.125M18 14v4.75A2.25 2.25 0 0115.75 21H5.25A2.25 2.25 0 013 18.75V8.25A2.25 2.25 0 015.25 6H10"
                                                        />
                                                      </svg>
                                                      Ubah Komen
                                                    </button>
                                                    <button
                                                      onClick={() =>
                                                        deleteComments.mutate({
                                                          postId: data.id,
                                                          commentId: item.id,
                                                        })
                                                      }
                                                      type="submit"
                                                      className="flex w-full items-center gap-2 rounded-md p-2 text-sm hover:bg-purple-100"
                                                      role="menuitem"
                                                    >
                                                      <svg
                                                        xmlns="http://www.w3.org/2000/svg"
                                                        className="h-4 w-4"
                                                        fill="none"
                                                        viewBox="0 0 24 24"
                                                        stroke="currentColor"
                                                        strokeWidth={2}
                                                      >
                                                        <path
                                                          strokeLinecap="round"
                                                          strokeLinejoin="round"
                                                          d="M19 7l-.867 12.142A2 2 0 0116.138 21H7.862a2 2 0 01-1.995-1.858L5 7m5 4v6m4-6v6m1-10V4a1 1 0 00-1-1h-4a1 1 0 00-1 1v3M4 7h16"
                                                        />
                                                      </svg>
                                                      Hapus Komen
                                                    </button>
                                                  </div>
                                                </div>
                                              </div>
                                            )}
                                          </div>
                                          <a href="">
                                            <span className="whitespace-pre-wrap break-word leading-snug">
                                              {item.comment}
                                            </span>
                                          </a>
                                          <div className="mt-2 flex   gap-x-3">
                                            {toggleLike.isLoading &&
                                            toggleLike.variables?.commentId ===
                                              item.id ? (
                                              <div className="w-[40px]">
                                                <LoadingSpinner size={24} />
                                              </div>
                                            ) : (
                                              <button
                                                onClick={() =>
                                                  toggleLike.mutate({
                                                    postId: data.id,
                                                    commentId: item.id,
                                                  })
                                                }
                                                className="flex items-center gap-x-1.5 text-gray-800 hover:text-sky-500"
                                              >
                                                <svg
                                                  xmlns="http://www.w3.org/2000/svg"
                                                  width="24px"
                                                  height="24px"
                                                  viewBox="0 0 32 32"
                                                >
                                                  {item.isLikes ? (
                                                    <path
                                                      fill="#9D21E6"
                                                      d="M2 16h5v14H2zm21 14H9V15.197l3.042-4.563l.845-5.917A2.01 2.01 0 0 1 14.867 3H15a3.003 3.003 0 0 1 3 3v6h8a4.005 4.005 0 0 1 4 4v7a7.008 7.008 0 0 1-7 7z"
                                                    ></path>
                                                  ) : (
                                                    <path
                                                      fill="#444"
                                                      d="M26 12h-6V6a3.003 3.003 0 0 0-3-3h-2.133a2.01 2.01 0 0 0-1.98 1.717l-.845 5.917L8.465 16H2v14h21a7.008 7.008 0 0 0 7-7v-7a4.005 4.005 0 0 0-4-4ZM8 28H4V18h4Zm20-5a5.006 5.006 0 0 1-5 5H10V17.303l3.958-5.937l.91-6.366H17a1 1 0 0 1 1 1v8h8a2.002 2.002 0 0 1 2 2Z"
                                                    ></path>
                                                  )}
                                                </svg>

                                                <span
                                                  className={`ml-1 text-sm font-medium ${
                                                    item.isLikes
                                                      ? "text-[#9D21E6]"
                                                      : null
                                                  }`}
                                                >
                                                  {item.totalLikes ?? 0}
                                                </span>
                                              </button>
                                            )}
                                            <button
                                              onClick={() =>
                                                setToggleSub(item.id)
                                              }
                                              className="flex items-center gap-x-1 text-gray-800"
                                            >
                                              <svg
                                                xmlns="http://www.w3.org/2000/svg"
                                                width="24px"
                                                height="24px"
                                                viewBox="0 0 32 32"
                                              >
                                                <path
                                                  fill="#444"
                                                  d="M17.74 30L16 29l4-7h6a2 2 0 0 0 2-2V8a2 2 0 0 0-2-2H6a2 2 0 0 0-2 2v12a2 2 0 0 0 2 2h9v2H6a4 4 0 0 1-4-4V8a4 4 0 0 1 4-4h20a4 4 0 0 1 4 4v12a4 4 0 0 1-4 4h-4.84Z"
                                                ></path>
                                                <path
                                                  fill="#444"
                                                  d="M8 10h16v2H8zm0 6h10v2H8z"
                                                ></path>
                                              </svg>
                                              <span className=" text-sm font-medium">
                                                {item.totalComments}
                                              </span>
                                            </button>
                                          </div>
                                        </div>
                                      </div>
                                      {/* Comment */}
                                      {toggleSub === item.id && (
                                        <div
                                          className={`rounded-b-lg pt-2.5 pb-4   border-y border-y-300`}
                                        >
                                          <div className="grid grid-cols-[auto,1fr] gap-x-3 gap-y-1">
                                            <p className="ml-[78px] sm:ml-[84px] mt-1 text-[#71767b] focus-within:block">
                                              Membalas{" "}
                                              <Link
                                                href={
                                                  `/profile/${item?.user?.id}` ??
                                                  `/profile`
                                                }
                                                className="font-medium text-blue-500"
                                              >
                                                {item?.user?.fullName ?? ""}
                                              </Link>
                                            </p>
                                          </div>
                                          <div
                                            className={`${
                                              toggle
                                                ? "place-items-start"
                                                : "place-items-center"
                                            } grid grid-cols-[auto,1fr]  gap-x-3 gap-y-1  py-2.5 px-4 sm:px-6`}
                                          >
                                            <Link
                                              href={
                                                `/profile/${item?.user?.id}` ??
                                                `/profile`
                                              }
                                            >
                                              <div className="relative h-12 w-12">
                                                <Image
                                                  blurDataURL="/static/images/default/placeholder.png"
                                                  placeholder="blur"
                                                  width={60}
                                                  height={60}
                                                  src={
                                                    user?.photo ??
                                                    `/static/images/logokedi.svg`
                                                  }
                                                  className="absolute inset-0 h-full w-full object-cover  rounded-lg"
                                                  alt=""
                                                />
                                              </div>
                                            </Link>
                                            <div
                                              className={` flex flex-col h-fit w-full items-center`}
                                            >
                                              <textarea
                                                ref={inputTextRef}
                                                className="h-auto w-full resize-none overflow-hidden border-none bg-transparent pt-1 text-base text-lg outline-none placeholder:text-gray-500 focus:ring-0 disabled:opacity-60"
                                                rows={1}
                                                value={inputTextSub}
                                                disabled={
                                                  postComments.isLoading
                                                }
                                                onChange={(e) =>
                                                  setInputTextSub(
                                                    e.target.value,
                                                  )
                                                }
                                                placeholder="Post your reply!"
                                              />
                                              <div className="mb-3 block w-full border-b border-gray-300 pt-1" />
                                            </div>
                                          </div>

                                          <div className="flex items-center justify-end gap-x-2 px-4">
                                            {postComments.isLoading ? (
                                              <LoadingSpinner
                                                size={20}
                                                strokeWidth={4.5}
                                              />
                                            ) : (
                                              <>
                                                <button
                                                  className="flex justify-end rounded-full bg-gray-400 px-3 py-1.5 text-sm font-medium duration-300 hover:bg-gray-500 disabled:opacity-60 text-white"
                                                  onClick={() => {
                                                    setToggleSub(0);
                                                    setInputTextSub("");
                                                  }}
                                                >
                                                  Batal
                                                </button>
                                                <button
                                                  onClick={() =>
                                                    postComments.mutate({
                                                      postId: data.id,
                                                      content: inputTextSub,
                                                      parentId: item.id,
                                                    })
                                                  }
                                                  className="flex h-fit justify-end rounded-full text-white bg-[#9D21E6] px-3 py-1.5 text-sm font-semibold disabled:opacity-60"
                                                  disabled={inputTextSub === ""}
                                                >
                                                  Balas
                                                </button>
                                              </>
                                            )}
                                          </div>
                                        </div>
                                      )}

                                      {item?.comments?.map(
                                        (d: any, index: number) => (
                                          <div key={index}>
                                            <div className=" grid grid-cols-[auto,1fr] gap-x-3 gap-y-1 pl-12 sm:pl-16 pr-4 sm:pr-6 py-3.5 hover:bg-slate-100 ">
                                              <div className="flex min-w-[45px]">
                                                <Link
                                                  href={
                                                    `/profile/${d?.user?.id}` ??
                                                    `/profile`
                                                  }
                                                >
                                                  <div className="relative h-12 w-12">
                                                    <Image
                                                      blurDataURL="/static/images/default/placeholder.png"
                                                      placeholder="blur"
                                                      width={60}
                                                      height={60}
                                                      src={
                                                        d.user
                                                          ?.profilePictureLink ??
                                                        `/static/images/logokedi.svg`
                                                      }
                                                      className="absolute inset-0 h-full w-full object-cover  rounded-lg"
                                                      alt=""
                                                    />
                                                  </div>
                                                </Link>
                                              </div>
                                              <div className="min-w-0">
                                                <div className="flex justify-between gap-x-2.5 text-[15px]">
                                                  <div className="xs:overflow-visible xs:whitespace-normal mb-1 flex items-center gap-x-1.5 truncate leading-5 [@media(max-width:360px)]:flex-wrap">
                                                    <Link
                                                      className="truncate font-semibold capitalize "
                                                      href={
                                                        `/profile/${d?.user?.id}` ??
                                                        `/profile`
                                                      }
                                                    >
                                                      {d?.user?.fullName ?? ""}
                                                    </Link>
                                                    <span className="text-[#71767b]">
                                                      ·
                                                    </span>
                                                    <div className="text-[#71767b]">
                                                      {dayjs(
                                                        d.createdDate,
                                                      ).fromNow()}
                                                    </div>
                                                  </div>
                                                  {d.user.id === user.id && (
                                                    <div className="relative h-[22px]">
                                                      <button className="peer text-gray-400 transition-all duration-200 hover:text-sky-500 ">
                                                        <svg
                                                          xmlns="http://www.w3.org/2000/svg"
                                                          fill="none"
                                                          viewBox="0 0 24 24"
                                                          strokeWidth={2}
                                                          stroke="currentColor"
                                                          className="h-[22px] w-[22px]"
                                                        >
                                                          <path
                                                            strokeLinecap="round"
                                                            strokeLinejoin="round"
                                                            d="M6.75 12a.75.75 0 11-1.5 0 .75.75 0 011.5 0zM12.75 12a.75.75 0 11-1.5 0 .75.75 0 011.5 0zM18.75 12a.75.75 0 11-1.5 0 .75.75 0 011.5 0z"
                                                          />
                                                        </svg>
                                                      </button>
                                                      <div className='invisible absolute -right-2 -top-5 z-10 w-[200px] rounded-md border border-slate-200 opacity-0 transition-all duration-300 after:absolute after:top-0 after:-z-20 after:inline-block after:h-full after:w-full after:rounded-md after:bg-white  after:content-[""] peer-focus:visible peer-focus:top-0 peer-focus:opacity-100'>
                                                        <div className="p-1">
                                                          <button
                                                            className="flex w-full items-center gap-2 rounded-md p-2 text-sm hover:bg-purple-100"
                                                            role="menuitem"
                                                          >
                                                            <svg
                                                              xmlns="http://www.w3.org/2000/svg"
                                                              className="h-4 w-4"
                                                              fill="none"
                                                              viewBox="0 0 24 24"
                                                              stroke="currentColor"
                                                              strokeWidth={2}
                                                            >
                                                              <path
                                                                strokeLinecap="round"
                                                                strokeLinejoin="round"
                                                                d="M16.862 4.487l1.687-1.688a1.875 1.875 0 112.652 2.652L10.582 16.07a4.5 4.5 0 01-1.897 1.13L6 18l.8-2.685a4.5 4.5 0 011.13-1.897l8.932-8.931zm0 0L19.5 7.125M18 14v4.75A2.25 2.25 0 0115.75 21H5.25A2.25 2.25 0 013 18.75V8.25A2.25 2.25 0 015.25 6H10"
                                                              />
                                                            </svg>
                                                            Ubah Komen
                                                          </button>
                                                          <button
                                                            type="submit"
                                                            className="flex w-full items-center gap-2 rounded-md p-2 text-sm hover:bg-purple-100"
                                                            role="menuitem"
                                                          >
                                                            <svg
                                                              xmlns="http://www.w3.org/2000/svg"
                                                              className="h-4 w-4"
                                                              fill="none"
                                                              viewBox="0 0 24 24"
                                                              stroke="currentColor"
                                                              strokeWidth={2}
                                                            >
                                                              <path
                                                                strokeLinecap="round"
                                                                strokeLinejoin="round"
                                                                d="M19 7l-.867 12.142A2 2 0 0116.138 21H7.862a2 2 0 01-1.995-1.858L5 7m5 4v6m4-6v6m1-10V4a1 1 0 00-1-1h-4a1 1 0 00-1 1v3M4 7h16"
                                                              />
                                                            </svg>
                                                            Hapus Komen
                                                          </button>
                                                        </div>
                                                      </div>
                                                    </div>
                                                  )}
                                                </div>
                                                <a href="#">
                                                  <span className="whitespace-pre-wrap break-word leading-snug">
                                                    {d.comment}
                                                  </span>
                                                </a>
                                                <div className="mt-2 flex   gap-x-3">
                                                  {toggleLike.isLoading &&
                                                  toggleLike.variables
                                                    ?.commentId === d.id ? (
                                                    <div className="w-[40px]">
                                                      <LoadingSpinner
                                                        size={24}
                                                      />
                                                    </div>
                                                  ) : (
                                                    <button
                                                      onClick={() =>
                                                        toggleLike.mutate({
                                                          postId: data.id,
                                                          commentId: d.id,
                                                        })
                                                      }
                                                      className="flex items-center gap-x-1 text-gray-800 hover:text-sky-500"
                                                    >
                                                      <svg
                                                        xmlns="http://www.w3.org/2000/svg"
                                                        width="24px"
                                                        height="24px"
                                                        viewBox="0 0 32 32"
                                                      >
                                                        {d.isLikes ? (
                                                          <path
                                                            fill="#9D21E6"
                                                            d="M2 16h5v14H2zm21 14H9V15.197l3.042-4.563l.845-5.917A2.01 2.01 0 0 1 14.867 3H15a3.003 3.003 0 0 1 3 3v6h8a4.005 4.005 0 0 1 4 4v7a7.008 7.008 0 0 1-7 7z"
                                                          ></path>
                                                        ) : (
                                                          <path
                                                            fill="#444"
                                                            d="M26 12h-6V6a3.003 3.003 0 0 0-3-3h-2.133a2.01 2.01 0 0 0-1.98 1.717l-.845 5.917L8.465 16H2v14h21a7.008 7.008 0 0 0 7-7v-7a4.005 4.005 0 0 0-4-4ZM8 28H4V18h4Zm20-5a5.006 5.006 0 0 1-5 5H10V17.303l3.958-5.937l.91-6.366H17a1 1 0 0 1 1 1v8h8a2.002 2.002 0 0 1 2 2Z"
                                                          ></path>
                                                        )}
                                                      </svg>
                                                      <span
                                                        className={`ml-1 text-sm font-medium ${
                                                          d.isLikes
                                                            ? "text-[#9D21E6]"
                                                            : null
                                                        }`}
                                                      >
                                                        {d.totalLikes ?? 0}
                                                      </span>{" "}
                                                    </button>
                                                  )}
                                                </div>
                                              </div>
                                            </div>
                                          </div>
                                        ),
                                      )}
                                    </div>
                                  ))}
                                </div>
                              </div>
                            )}
                        </div>
                      </div>
                    );
                  })
                ) : (
                  <div className="flex items-center justify-center text-gray-600 mt-5 h-[45vh] md:h-[50vh]">
                    <EmptyState />
                  </div>
                )}
              </div>
            ))}
            <div>
              {isFetchingNextPage
                ? [...Array(1)].map((_, index) => (
                    <div
                      key={index}
                      role="status"
                      className=" mb-5 p-4 border border-gray-300 rounded-lg shadow animate-pulse md:p-6 dark:border-gray-700"
                    >
                      <div className="flex items-center mb-4">
                        <svg
                          className="w-10 h-10 me-3 text-gray-200 dark:text-gray-700"
                          aria-hidden="true"
                          xmlns="http://www.w3.org/2000/svg"
                          fill="currentColor"
                          viewBox="0 0 20 20"
                        >
                          <path d="M10 0a10 10 0 1 0 10 10A10.011 10.011 0 0 0 10 0Zm0 5a3 3 0 1 1 0 6 3 3 0 0 1 0-6Zm0 13a8.949 8.949 0 0 1-4.951-1.488A3.987 3.987 0 0 1 9 13h2a3.987 3.987 0 0 1 3.951 3.512A8.949 8.949 0 0 1 10 18Z" />
                        </svg>
                        <div>
                          <div className="h-2.5 bg-gray-200 rounded-full dark:bg-gray-700 w-32 mb-2" />
                          <div className="w-48 h-2 bg-gray-200 rounded-full dark:bg-gray-700" />
                        </div>
                      </div>
                      <div className="flex items-center justify-center h-60 md:h-80 mb-4 bg-gray-300 rounded dark:bg-gray-700">
                        <svg
                          className="w-10 h-10 text-gray-200 dark:text-gray-600"
                          aria-hidden="true"
                          xmlns="http://www.w3.org/2000/svg"
                          fill="currentColor"
                          viewBox="0 0 16 20"
                        >
                          <path d="M14.066 0H7v5a2 2 0 0 1-2 2H0v11a1.97 1.97 0 0 0 1.934 2h12.132A1.97 1.97 0 0 0 16 18V2a1.97 1.97 0 0 0-1.934-2ZM10.5 6a1.5 1.5 0 1 1 0 2.999A1.5 1.5 0 0 1 10.5 6Zm2.221 10.515a1 1 0 0 1-.858.485h-8a1 1 0 0 1-.9-1.43L5.6 10.039a.978.978 0 0 1 .936-.57 1 1 0 0 1 .9.632l1.181 2.981.541-1a.945.945 0 0 1 .883-.522 1 1 0 0 1 .879.529l1.832 3.438a1 1 0 0 1-.031.988Z" />
                          <path d="M5 5V.13a2.96 2.96 0 0 0-1.293.749L.879 3.707A2.98 2.98 0 0 0 .13 5H5Z" />
                        </svg>
                      </div>
                      <div className="h-2.5 bg-gray-200 rounded-full dark:bg-gray-700 w-48 mb-4" />
                      <div className="h-2 bg-gray-200 rounded-full dark:bg-gray-700 mb-2.5" />
                      <div className="h-2 bg-gray-200 rounded-full dark:bg-gray-700 mb-2.5" />
                      <div className="h-2 bg-gray-200 rounded-full dark:bg-gray-700" />
                      <span className="sr-only">Loading...</span>
                    </div>
                  ))
                : null}
            </div>
          </>
        )}
      </div>
    </>
  );
};
