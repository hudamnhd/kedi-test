import type { PropsWithChildren } from "react";
import { useSession } from "next-auth/react";
import { SignIn } from "../auth/sign";

export const Wrapper = (props: PropsWithChildren) => {
  const { data: sessionData, status } = useSession();

  return (
    <>
      {sessionData && status === "authenticated" && <>{props.children}</>}

      {!sessionData && status === "unauthenticated" && <SignIn />}
    </>
  );
};
