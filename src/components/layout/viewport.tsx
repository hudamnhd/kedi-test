import React, { useState, useEffect } from "react";

export const LayoutViewport = ({ children }) => {
  const [viewportHeight, setViewportHeight] = useState(0);

  useEffect(() => {
    const adjustHeight = () => {
      const vh = window.innerHeight;
      setViewportHeight(vh);
    };

    adjustHeight();
    window.addEventListener("resize", adjustHeight);
    return () => {
      window.removeEventListener("resize", adjustHeight);
    };
  }, []);

  // Menambahkan viewportHeight sebagai prop untuk children
  const childrenWithProps = React.Children.map(children, (child) =>
    React.cloneElement(child, { viewportHeight }),
  );

  return (
    <main style={{ maxHeight: `${viewportHeight}px`, overflow: "none" }}>
      {viewportHeight !== 0 && childrenWithProps}
    </main>
  );
};
