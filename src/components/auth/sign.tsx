import { NextPage } from "next";
import { LoadingSpinner } from "@/components/ui/loading";
import toast from "react-hot-toast";
import { ReactEventHandler, useState } from "react";
import { signIn } from "next-auth/react";
import Link from "next/link";
import { useRouter } from "next/router";

export const SignIn: NextPage = (): JSX.Element => {
  const [userInfo, setUserInfo] = useState({ email: "", password: "" });
  const [loading, setLoading] = useState(false);
  const router = useRouter();

  const handleSubmit = async (e: any) => {
    e.preventDefault();

    try {
      setLoading(true);
      const res = await signIn("credentials", {
        email: userInfo.email,
        password: userInfo.password,
        redirect: false,
      });
      console.log(res);
      if (res?.error) {
        toast.error(res.error);
      } else {
        toast.success("Successfully Login");
        router.push("/");
      }
    } catch (error) {
      console.error("Error during form submission:", error);
    } finally {
      setLoading(false);
    }
  };

  return (
    <div className="h-screen flex  items-center justify-center bg-[#9D21E6]">
      <div className="flex flex-col items-center h-[95%] bg-white rounded-xl my-4  mx-4 px-6 py-10 max-w-lg w-full">
        <img
          className="w-32"
          alt="logo kedi"
          src="https://web.kelas.digital/static/media/Logo KeDi-04.d265b50ebe18a8d44ccbd38d81c3095f.svg"
        />
        <form className="mt-10 w-full" onSubmit={handleSubmit}>
          <div className="mb-3 sm:mb-5">
            <label
              htmlFor="UserEmail"
              className="block text-xs sm:text-sm  font-medium text-gray-700"
            >
              Email
            </label>

            <input
              value={userInfo.email}
              onChange={({ target }) =>
                setUserInfo({ ...userInfo, email: target.value })
              }
              placeholder="john@rhcp.com"
              type="email"
              disabled={loading}
              className="disabled:bg-gray-200 mt-1.5 sm:mt-3 px-4 py-1.5 sm:py-2.5 w-full rounded-[10px] border border-gray-400 outline-none focus:ring-2 focus:border-[#9D21E6] ring-[#9D21E6] shadow-sm sm:text-sm"
            />
          </div>
          <div className="mb-3 sm:mb-5">
            <label
              htmlFor="Userpassword"
              className="block text-xs sm:text-sm  font-medium text-gray-700"
            >
              Password
            </label>

            <input
              value={userInfo.password}
              onChange={({ target }) =>
                setUserInfo({ ...userInfo, password: target.value })
              }
              placeholder="john@rhcp.com"
              type="password"
              disabled={loading}
              className="disabled:bg-gray-200 mt-1.5 sm:mt-3 px-4 py-1.5 sm:py-2.5 w-full rounded-[10px] border border-gray-400 outline-none focus:ring-2 focus:border-[#9D21E6] ring-[#9D21E6] shadow-sm sm:text-sm"
            />
          </div>
          <div className="flex flex-wrap gap-3 justify-between">
            <label
              data-testid="comp-checkbox"
              className="sc-idiyUo gOwXoN comp-checkbox"
            >
              <div className="flex items-center gap-x-2">
                <input
                  type="checkbox"
                  className="accent-[#9D21E6] rounded-md scale-125"
                />
                <span className="text-sm text-gray-700 font-medium">
                  Ingat saya
                </span>
              </div>
            </label>
            <Link
              href="/lupa-katasandi"
              className="text-[#9D21E6] cursor-pointer text-sm"
            >
              Lupa Kata Sandi ?
            </Link>
          </div>
          <div className="sm:text-base text-sm flex flex-col gap-y-2.5 justify-center mt-4 sm:mt-6 max-w-[300px] mx-auto">
            <button
              className="flex justify-center cursor-pointer bg-[#9D21E6] text-white rounded-full px-4 py-2.5 font-semibold w-full border border-gray-400 shadow-lg"
              type="submit"
              value="Login"
            >
              {loading ? <LoadingSpinner stroke={`#FFF`} size={24} /> : "Login"}
            </button>
            <button
              type="button"
              disabled={loading}
              onClick={() => signIn("google")}
              className="px-2 w-full border border-gray-400 shadow-lg font-semibold rounded-full py-2.5 flex justify-center items-center"
            >
              <svg
                stroke="currentColor"
                fill="currentColor"
                strokeWidth={0}
                version="1.1"
                x="0px"
                y="0px"
                viewBox="0 0 48 48"
                enableBackground="new 0 0 48 48"
                className="inline-block mr-2 h-5 lg:h-6 w-5 lg:w-6"
                height="1em"
                width="1em"
                xmlns="http://www.w3.org/2000/svg"
              >
                <path
                  fill="#FFC107"
                  d="M43.611,20.083H42V20H24v8h11.303c-1.649,4.657-6.08,8-11.303,8c-6.627,0-12-5.373-12-12
	c0-6.627,5.373-12,12-12c3.059,0,5.842,1.154,7.961,3.039l5.657-5.657C34.046,6.053,29.268,4,24,4C12.955,4,4,12.955,4,24
	c0,11.045,8.955,20,20,20c11.045,0,20-8.955,20-20C44,22.659,43.862,21.35,43.611,20.083z"
                />
                <path
                  fill="#FF3D00"
                  d="M6.306,14.691l6.571,4.819C14.655,15.108,18.961,12,24,12c3.059,0,5.842,1.154,7.961,3.039l5.657-5.657
	C34.046,6.053,29.268,4,24,4C16.318,4,9.656,8.337,6.306,14.691z"
                />
                <path
                  fill="#4CAF50"
                  d="M24,44c5.166,0,9.86-1.977,13.409-5.192l-6.19-5.238C29.211,35.091,26.715,36,24,36
	c-5.202,0-9.619-3.317-11.283-7.946l-6.522,5.025C9.505,39.556,16.227,44,24,44z"
                />
                <path
                  fill="#1976D2"
                  d="M43.611,20.083H42V20H24v8h11.303c-0.792,2.237-2.231,4.166-4.087,5.571
	c0.001-0.001,0.002-0.001,0.003-0.002l6.19,5.238C36.971,39.205,44,34,44,24C44,22.659,43.862,21.35,43.611,20.083z"
                />
              </svg>
              Login dengan Google
            </button>
          </div>
        </form>
      </div>
    </div>
  );
};
