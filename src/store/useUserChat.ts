import { shallow } from "zustand/shallow";
import { createWithEqualityFn } from "zustand/traditional";

const useUserChat = createWithEqualityFn(() => ({
  isLoadMetadata: false,
  isLoadMoreMsg: false,
  isLoadMoreList: false,
  isFetchMessage: false,
  isFetchFirst: false,
  isTyping: {},
  isMobileView: false,
  initLoad: false,
  initPubNub: false,
  notifDelete: null,
  notifMessage: [],
  seenMessages: {},
  chatMode: "",
  responses: { channel: [] },
  userChatAktif: {},
  messageHistory: [],
  tabChat: "",
  privateMessage: [],
  groupChatMessage: [],
  classMessage: [],
  allClassMessage: [],
  groupItems: [],
  privateItems: [],
  classItems: [],
  newItemsGroup: [],
  newItemsPrivate: [],
  newItemsClass: [],
  channelStore: {
      storedGroup: [],
      storedPrivate: [],
      storedClass: [],
  },
}), shallow);

export default useUserChat;
