import NextAuth from "next-auth";

declare module "next-auth" {
  /**
   * Returned by `useSession`, `getSession` and received as a prop on the `SessionProvider` React Context
   */
  interface Session {
    user: {
      /** The user's postal address. */
      name?: string | null | undefined;
      fullName?: string | null | undefined;
      email?: string | null | undefined;
      image?: string | null | undefined;
      accessToken?: string | null | undefined;
      uuid?: string | null | undefined;
      id?: string | null | undefined;
      photo?: string | null | undefined;
    };
  }
}
