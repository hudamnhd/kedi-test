export const dataTerm = {
  en: {
    termAndCond: [
      {
        title: "Terms and Conditions",
        content: `By downloading or using the app, these terms will automatically apply to you – you should therefore ensure that you read them carefully before using the app. You may not copy, or modify the application, any part of the application, or our trademarks in any way. You should not attempt to extract the application's source code, nor should you attempt to translate the application into another language, or create a derivative version. The application itself, and all trademarks, copyright, database rights and other intellectual property rights related to it, belong to PT. Ekasakti World Class.

PT. Ekasakti World Class is committed to ensuring that this application is as useful and efficient as possible. For that reason, we reserve the right to make changes to the application or charge fees for its services, at any time and for any reason. We will not charge you for the app or its services without clearly explaining to you what you are actually paying for.

The Digital Class Application processes personal data that you have provided to us, in order to provide our Services. You are responsible for maintaining the security of your phone and access to the application. Therefore, we recommend that you do not jailbreak or root your phone, which is the process of removing software restrictions and restrictions imposed by your device's official operating system. This may make your phone vulnerable to malware/viruses/malicious programs, compromise your phone's security features and may cause the Digital Class app to function poorly or not at all.

This application uses third party services that state their own Terms and Conditions.

Links to Terms and Conditions of third party service providers used by the application
          • Service Google Play
          • Google Analytics for Firebase

You need to know that there are several things that PT. Kelas Dunia Ekasakti is not responsible. Certain app functions will require the app to have an active internet connection. The connection can be Wi-Fi, or provided by your mobile network provider, but  PT. Kelas Dunia Ekasakti is not responsible for applications that do not function optimally if you do not have Wi-Fi access, and do not have remaining data quota.

If you use the app outside of an area with Wi-Fi, you should remember that the terms of your agreement with your mobile network provider will still apply. As a result, you may be charged by your mobile provider for data charges during the connection when accessing the application, or other third party fees. In using the app, you accept responsibility for such charges, including roaming data charges if you use the app outside your home region (i.e. region or country) without disabling data roaming. If you are not the bill payer for the device on which you use the app, please note that we assume that you have received permission from the bill payer to use the app.

In line with that,  PT. Kelas Dunia Ekasakti is not always responsible for how you use the application i.e. you must ensure that your device remains charged – if the battery runs out and you cannot turn it on to utilize the Services,  PT. Kelas Dunia Ekasakti cannot accept responsibility.

In connection with PT. Kelas Dunia Ekasakti for your use of the application, when you use the application, it is important to remember that although we strive to ensure that it is updated and correct at all times, we rely on third parties to provide us with information so that we can provide it to you. PT. Kelas Dunia Ekasakti is not responsible for any losses, direct or indirect, that you experience as a result of relying entirely on the functionality of this application.

At some point, we may want to update the app. The app is currently available on Android & iOS – requirements for both systems (and for any additional systems we decide to extend the app's availability to) may change, and you'll need to download updates if you want to keep using the app. PT. Kelas Dunia Ekasakti does not promise to always update the application to be relevant to you and/or work with the version of the device you are using the application on, please note that we assume that you have received permission from the bill payer to use the application. In line with that, PT. Kelas Dunia Ekasakti is not always responsible for how you use the application i.e. you must ensure that your device remains charged – if the battery runs out and you cannot turn it on to utilize the Services, PT. Kelas Dunia Ekasakti cannot accept responsibility. In connection with PT. Kelas Dunia Ekasakti Responsibility for your use of the application, when you use the application, it is important to remember that although we strive to ensure that it is updated and correct at all times, we rely on third parties to provide us with information so that we can provide it to you.PT. Kelas Dunia Ekasakti is not responsible for any losses, direct or indirect, that you experience as a result of relying entirely on the functionality of this application. At some point, we may want to update the app. The app is currently available on Android & iOS – requirements for both systems (and for any additional systems we decide to extend the app's availability to) may change, and you'll need to download updates if you want to keep using the app. PT. Kelas Dunia Ekasakti does not promise to always update the application so that it is relevant to you and/or works with the version of Android & iOS that you have installed on your device. However, you promise to always accept application updates when offered to you, We may also wish to stop providing the application, and may discontinue use of it at any time without providing you with notice of termination. Unless we notify you otherwise, upon any termination, (a) the rights and licenses granted to you in these terms will terminate; (b) You must stop using the application, and (if necessary) delete it from your device.`,
      },
      {
        title: "Changes to These Terms and Conditions",
        content: `We may update our Terms and Conditions from time to time. Therefore, you are advised to periodically review this page for any changes. We will notify you of any changes by posting the new Terms and Conditions on this page.

These terms and conditions are effective from 07-01-2021`,
      },
      {
        title: "Contact us",
        content: `If you have any questions or suggestions about our Terms and Conditions, please feel free to contact us at`,
        mail: "info@kelas-digital.id",
      },
    ],
  },
  id: {
    termAndCond: [
      {
        title: "Syarat & Ketentuan",
        content: `Dengan mengunduh atau menggunakan aplikasi, persyaratan ini akan berlaku secara otomatis untuk Anda – karena itu Anda harus memastikan bahwa Anda membacanya dengan cermat sebelum menggunakan aplikasi. Anda tidak boleh menyalin, atau memodifikasi aplikasi, bagian mana pun dari aplikasi, atau merek dagang kami dengan cara apa pun. Anda tidak boleh mencoba mengekstrak kode sumber aplikasi, dan Anda juga tidak boleh mencoba menerjemahkan aplikasi ke bahasa lain, atau membuat versi turunan. Aplikasi itu sendiri, dan semua merek dagang, hak cipta, hak basis data, dan hak kekayaan intelektual lainnya yang terkait dengannya, milik PT. Kelas Dunia Ekasakti.

PT. Kelas Dunia Ekasakti berkomitmen untuk memastikan bahwa aplikasi ini berguna dan seefisien mungkin. Untuk alasan itu, kami berhak untuk membuat perubahan pada aplikasi atau membebankan biaya untuk layanannya, kapan saja dan untuk alasan apa pun. Kami tidak akan menagih Anda untuk aplikasi atau layanannya tanpa menjelaskan dengan jelas kepada Anda apa yang sebenarnya Anda bayar.

Aplikasi Kelas Digital memproses data pribadi yang telah Anda berikan kepada kami, dalam rangka memberikan Layanan kami. Anda bertanggung jawab untuk menjaga keamanan ponsel dan akses ke aplikasi. Oleh karena itu, kami menyarankan Anda untuk tidak melakukan jailbreak atau root pada ponsel Anda, yang merupakan proses menghilangkan batasan dan batasan perangkat lunak yang diberlakukan oleh sistem operasi resmi perangkat Anda. Hal ini dapat membuat ponsel Anda rentan terhadap malware/virus/program jahat, mengganggu fitur keamanan ponsel Anda dan dapat menyebabkan aplikasi Kelas Digital tidak berfungsi dengan baik atau tidak berfungsi sama sekali.

Aplikasi ini menggunakan layanan pihak ketiga yang menyatakan Syarat dan Ketentuan mereka sendiri.

Tautan ke Syarat dan Ketentuan penyedia layanan pihak ketiga yang digunakan oleh aplikasi
          • Layanan Google Play
          • Google Analytics untuk Firebase

Perlu Anda ketahui bahwa ada beberapa hal yang PT. Kelas Dunia Ekasakti tidak bertanggung jawab. Fungsi aplikasi tertentu akan mengharuskan aplikasi memiliki koneksi internet aktif. Koneksi dapat berupa Wi-Fi, atau disediakan oleh penyedia jaringan seluler Anda, tetapi PT. Kelas Dunia Ekasakti tidak bertanggung jawab atas aplikasi yang tidak berfungsi secara maksimal jika Anda tidak memiliki akses Wi-Fi, dan tidak memiliki sisa kuota data.

Jika Anda menggunakan aplikasi di luar area dengan Wi-Fi, Anda harus ingat bahwa persyaratan perjanjian Anda dengan penyedia jaringan seluler Anda akan tetap berlaku. Akibatnya, Anda mungkin dikenakan biaya oleh penyedia seluler Anda untuk biaya data selama koneksi saat mengakses aplikasi, atau biaya pihak ketiga lainnya. Dalam menggunakan aplikasi, Anda menerima tanggung jawab atas biaya tersebut, termasuk biaya data roaming jika Anda menggunakan aplikasi di luar wilayah asal Anda (yaitu wilayah atau negara) tanpa menonaktifkan roaming data. Jika Anda bukan pembayar tagihan untuk perangkat tempat Anda menggunakan aplikasi, perlu diketahui bahwa kami berasumsi bahwa Anda telah menerima izin dari pembayar tagihan untuk menggunakan aplikasi.

Sejalan dengan itu, PT. Kelas Dunia Ekasakti tidak selalu bertanggung jawab atas cara Anda menggunakan aplikasi yaitu Anda harus memastikan bahwa perangkat Anda tetap terisi daya – jika baterai habis dan Anda tidak dapat menyalakannya untuk memanfaatkan Layanan, PT. Kelas Dunia Ekasakti tidak bisa menerima tanggung jawab.

Sehubungan dengan PT. Tanggung jawab Kelas Dunia Ekasakti atas penggunaan Anda atas aplikasi, ketika Anda menggunakan aplikasi, penting untuk diingat bahwa meskipun kami berusaha untuk memastikan bahwa itu diperbarui dan benar setiap saat, kami mengandalkan pihak ketiga untuk memberikan informasi kepada kami sehingga kami dapat menyediakannya untuk Anda. PT. Kelas Dunia Ekasakti tidak bertanggung jawab atas kerugian, langsung atau tidak langsung, yang Anda alami sebagai akibat dari mengandalkan sepenuhnya fungsi aplikasi ini.

Pada titik tertentu, kami mungkin ingin memperbarui aplikasi. Aplikasi saat ini tersedia di Android & iOS – persyaratan untuk kedua sistem (dan untuk sistem tambahan apa pun yang kami putuskan untuk memperpanjang ketersediaan aplikasi) dapat berubah, dan Anda harus mengunduh pembaruan jika ingin tetap menggunakan aplikasi. PT. Kelas Dunia Ekasakti tidak berjanji akan selalu mengupdate aplikasi agar relevan dengan Anda dan/atau bekerja dengan versi perangkat tempat Anda menggunakan aplikasi, perlu diketahui bahwa kami berasumsi bahwa Anda telah menerima izin dari pembayar tagihan untuk menggunakan aplikasi. Sejalan dengan itu, PT. Kelas Dunia Ekasakti tidak selalu bertanggung jawab atas cara Anda menggunakan aplikasi yaitu Anda harus memastikan bahwa perangkat Anda tetap terisi daya – jika baterai habis dan Anda tidak dapat menyalakannya untuk memanfaatkan Layanan, PT. Kelas Dunia Ekasakti tidak bisa menerima tanggung jawab. Sehubungan dengan PT. Tanggung jawab Kelas Dunia Ekasakti atas penggunaan Anda atas aplikasi, ketika Anda menggunakan aplikasi, penting untuk diingat bahwa meskipun kami berusaha untuk memastikan bahwa itu diperbarui dan benar setiap saat, kami mengandalkan pihak ketiga untuk memberikan informasi kepada kami sehingga kami dapat menyediakannya untuk Anda. PT. Kelas Dunia Ekasakti tidak bertanggung jawab atas kerugian, langsung atau tidak langsung, yang Anda alami sebagai akibat dari mengandalkan sepenuhnya fungsi aplikasi ini. Pada titik tertentu, kami mungkin ingin memperbarui aplikasi. Aplikasi saat ini tersedia di Android & iOS – persyaratan untuk kedua sistem (dan untuk sistem tambahan apa pun yang kami putuskan untuk memperpanjang ketersediaan aplikasi) dapat berubah, dan Anda harus mengunduh pembaruan jika ingin tetap menggunakan aplikasi. PT. Kelas Dunia Ekasakti tidak berjanji akan selalu mengupdate aplikasi agar relevan dengan Anda dan/atau bekerja dengan versi Android & iOS yang telah Anda instal di perangkat Anda. Namun, Anda berjanji untuk selalu menerima pembaruan aplikasi saat ditawarkan kepada Anda, Kami mungkin juga ingin berhenti menyediakan aplikasi, dan dapat menghentikan penggunaannya kapan saja tanpa memberikan pemberitahuan penghentian kepada Anda. Kecuali jika kami memberi tahu Anda sebaliknya, setelah penghentian apa pun, (a) hak dan lisensi yang diberikan kepada Anda dalam persyaratan ini akan berakhir; (b) Anda harus berhenti menggunakan aplikasi, dan (jika perlu) menghapusnya dari perangkat Anda.`,
      },
      {
        title: "Perubahan Syarat dan Ketentuan Ini",
        content: `Kami dapat memperbarui Syarat dan Ketentuan kami dari waktu ke waktu. Oleh karena itu, Anda disarankan untuk meninjau halaman ini secara berkala untuk setiap perubahan. Kami akan memberi tahu Anda tentang perubahan apa pun dengan memposting Syarat dan Ketentuan baru di halaman ini.

Syarat dan ketentuan ini berlaku mulai 01-07-2021`,
      },
      {
        title: "Hubungi Kami",
        content: `Jika Anda memiliki pertanyaan atau saran tentang Kebijakan Privasi kami, jangan ragu untuk menghubungi kami di`,
        mail: "info@kelas.digital",
      },
    ],
  },
};
