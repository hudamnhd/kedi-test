export const dataPrivacyPolicy = {
  en: {
    privacyPolice: [
      {
        title: "Privacy Policy",
        content: `PT. Kelas Dunia Ekasakti built the Kelas Digital application as a free application. This SERVICE is provided by PT. Kelas Dunia Ekasakti is free of charge and is intended to be used as is.

This page is used to inform visitors regarding our policies with the collection, use, and disclosure of Personal Information if anyone decides to use our Services.

If you choose to use our Services, then you consent to the collection and use of information in connection with this policy. The Personal Information we collect is used to provide and improve the Services. We will not use or share your information with anyone except as described in this Privacy Policy.

The terms used in this Privacy Policy have the same meanings as in our Terms and Conditions, which are accessible on Digital Class unless otherwise defined in this Privacy Policy.`,
      },
      {
        title: "Information Collection and Use",
        content: ` For a better experience, when using our Services, we may ask you to provide us with certain personally identifiable information, including but not limited to Users' full names, email addresses, and telephone numbers. The information we request will be stored by us and used as described in this privacy policy.

Links to the privacy policies of third party service providers used by the app
       • Google Play Services
       • Google Analytics for Firebase
       • Log Data

We would like to inform you that whenever you use our Services, if an error occurs in the application, we collect data and information (through third party products) on your phone called Log Data. This Log Data may include information such as your device's Internet Protocol (“IP”) address, device name, operating system version, the configuration of the application when using our Services, the time and date of your use of the Services, and other statistics.`,
      },
      {
        title: "Cookies",
        content: `A cookie is a file with a small amount of data that is usually used as an anonymous unique identifier. These are sent to your browser from websites you visit and stored in your device's internal memory.

This service does not explicitly use these “cookies”. However, applications may use third-party code and libraries that use "cookies" to collect information and improve their services. You have the choice to accept or reject these cookies and know when they are sent to your device. If you choose to reject our cookies, you may not be able to use some parts of the Service.`,
      },
      {
        title: "Service Provider",
        content: `We may employ third party companies and individuals for the following reasons:

To facilitate our Services;
To provide Services on our behalf;
To perform services related to the Services; or
To help us analyze how our Services are used.
We would like to inform users of this Service that these third parties have access to your Personal Information.
The reason is to carry out the tasks assigned to them on our behalf. However, they are obliged not to disclose or use such information for other purposes.`,
      },
      {
        title: "Security",
        content: ` We value your trust in providing us with your Personal Information, therefore we strive to use commercially acceptable means to protect it. But remember that no method of transmission over the internet, or method of electronic storage is 100% secure and reliable, and we cannot guarantee its absolute security.`,
      },
      {
        title: "Links to Other Sites",
        content: `The Service may contain links to other sites. If you click on a third-party link, you will be directed to that site. Please note that these external sites are not operated by us. Therefore, we strongly advise you to review this website's Privacy Policy. We have no control over and assume no responsibility for the content, privacy policies, or practices of any third party sites or services.`,
      },
      {
        title: "Children's Privacy",
        content: `The Services are not directed to anyone under 13 years of age. We do not knowingly collect personally identifiable information from children under 13 years of age. If we discover that a child under 13 has provided us with personal information, we immediately delete it from our servers. If you are a parent or guardian and you are aware that your child has provided us with personal information, please contact us so that we can take appropriate action.`,
      },
      {
        title: "Changes to This Privacy Policy",
        content: `We may update our Privacy Policy from time to time. Therefore, you are advised to periodically review this page for any changes. We will notify you of any changes by posting the new Privacy Policy on this page.

This policy is effective from 07-01-2021`,
      },
      {
        title: "Contact us",
        content: `If you have any questions or suggestions about our Privacy Policy, please feel free to contact us at`,
        mail: "info@kelas-digital.id",
      },
    ],
  },
  id: {
    privacyPolice: [
      {
        title: "Kebijakan Privasi",
        content: `PT. Kelas Dunia Ekasakti membangun aplikasi Kelas Digital sebagai aplikasi Gratis. LAYANAN ini disediakan oleh PT. Kelas Dunia Ekasakti tanpa biaya dan dimaksudkan untuk digunakan apa adanya.

Halaman ini digunakan untuk memberi tahu pengunjung mengenai kebijakan kami dengan pengumpulan, penggunaan, dan pengungkapan Informasi Pribadi jika ada yang memutuskan untuk menggunakan Layanan kami.

Jika Anda memilih untuk menggunakan Layanan kami, maka Anda menyetujui pengumpulan dan penggunaan informasi sehubungan dengan kebijakan ini. Informasi Pribadi yang kami kumpulkan digunakan untuk menyediakan dan meningkatkan Layanan. Kami tidak akan menggunakan atau membagikan informasi Anda dengan siapa pun kecuali sebagaimana dijelaskan dalam Kebijakan Privasi ini.

Istilah yang digunakan dalam Kebijakan Privasi ini memiliki arti yang sama seperti dalam Syarat dan Ketentuan kami, yang dapat diakses di Kelas Digital kecuali ditentukan lain dalam Kebijakan Privasi ini.`,
      },
      {
        title: "Pengumpulan dan Penggunaan Informasi",
        content: `Untuk pengalaman yang lebih baik, saat menggunakan Layanan kami, kami mungkin meminta Anda untuk memberikan kami informasi pengenal pribadi tertentu, termasuk namun tidak terbatas pada nama lengkap Pengguna, alamat email, dan nomor telepon. Informasi yang kami minta akan disimpan oleh kami dan digunakan seperti yang dijelaskan dalam kebijakan privasi ini.

Tautan ke kebijakan privasi penyedia layanan pihak ketiga yang digunakan oleh aplikasi
       • Google Play Services
       • Google Analytics for Firebase
       • Log Data

Kami ingin memberi tahu Anda bahwa setiap kali Anda menggunakan Layanan kami, jika terjadi kesalahan dalam aplikasi, kami mengumpulkan data dan informasi (melalui produk pihak ketiga) di ponsel Anda yang disebut Data Log. Data Log ini dapat mencakup informasi seperti alamat Protokol Internet (“IP”) perangkat Anda, nama perangkat, versi sistem operasi, konfigurasi aplikasi saat menggunakan Layanan kami, waktu dan tanggal penggunaan Layanan oleh Anda, dan statistik lainnya.`,
      },
      {
        title: "Cookies",
        content: `Cookie adalah file dengan sejumlah kecil data yang biasanya digunakan sebagai pengidentifikasi unik anonim. Ini dikirim ke browser Anda dari situs web yang Anda kunjungi dan disimpan di memori internal perangkat Anda.

Layanan ini tidak menggunakan “cookies” ini secara eksplisit. Namun, aplikasi dapat menggunakan kode dan perpustakaan pihak ketiga yang menggunakan "cookies" untuk mengumpulkan informasi dan meningkatkan layanan mereka. Anda memiliki pilihan untuk menerima atau menolak cookie ini dan mengetahui kapan cookie dikirim ke perangkat Anda. Jika Anda memilih untuk menolak cookie kami, Anda mungkin tidak dapat menggunakan beberapa bagian dari Layanan ini.`,
      },
      {
        title: "Penyedia Jasa",
        content: `Kami dapat mempekerjakan perusahaan dan individu pihak ketiga karena alasan berikut:

Untuk memfasilitasi Layanan kami;
Untuk menyediakan Layanan atas nama kami;
Untuk melakukan layanan terkait Layanan; atau
Untuk membantu kami dalam menganalisis bagaimana Layanan kami digunakan.
Kami ingin memberi tahu pengguna Layanan ini bahwa pihak ketiga ini memiliki akses ke Informasi Pribadi Anda. Alasannya adalah untuk melakukan tugas yang diberikan kepada mereka atas nama kita. Namun, mereka berkewajiban untuk tidak mengungkapkan atau menggunakan informasi tersebut untuk tujuan lain.`,
      },
      {
        title: "Keamanan",
        content: `Kami menghargai kepercayaan Anda dalam memberikan Informasi Pribadi Anda kepada kami, oleh karena itu kami berusaha untuk menggunakan cara yang dapat diterima secara komersial untuk melindunginya. Tetapi ingat bahwa tidak ada metode transmisi melalui internet, atau metode penyimpanan elektronik yang 100% aman dan andal, dan kami tidak dapat menjamin keamanan mutlaknya.`,
      },
      {
        title: "Tautan ke Situs Lain",
        content: `Layanan ini mungkin berisi tautan ke situs lain. Jika Anda mengklik tautan pihak ketiga, Anda akan diarahkan ke situs itu. Perhatikan bahwa situs eksternal ini tidak dioperasikan oleh kami. Oleh karena itu, kami sangat menyarankan Anda untuk meninjau Kebijakan Privasi situs web ini. Kami tidak memiliki kendali atas dan tidak bertanggung jawab atas konten, kebijakan privasi, atau praktik situs atau layanan pihak ketiga mana pun.`,
      },
      {
        title: "Privasi Anak-Anak",
        content: `Layanan ini tidak ditujukan kepada siapa pun yang berusia di bawah 13 tahun. Kami tidak dengan sengaja mengumpulkan informasi pengenal pribadi dari anak-anak di bawah 13 tahun. Jika kami menemukan bahwa seorang anak di bawah 13 tahun telah memberi kami informasi pribadi, kami segera menghapusnya dari server kami. Jika Anda adalah orang tua atau wali dan Anda mengetahui bahwa anak Anda telah memberikan informasi pribadi kepada kami, silakan hubungi kami sehingga kami dapat melakukan tindakan yang diperlukan.`,
      },
      {
        title: "Perubahan pada Kebijakan Privasi Ini",
        content: `Kami dapat memperbarui Kebijakan Privasi kami dari waktu ke waktu. Oleh karena itu, Anda disarankan untuk meninjau halaman ini secara berkala untuk setiap perubahan. Kami akan memberi tahu Anda tentang perubahan apa pun dengan memposting Kebijakan Privasi baru di halaman ini.

Kebijakan ini berlaku mulai 01-07-2021`,
      },
      {
        title: "Hubungi Kami",
        content: `Jika Anda memiliki pertanyaan atau saran tentang Kebijakan Privasi kami, jangan ragu untuk menghubungi kami di`,
        mail: "info@kelas.digital",
      },
    ],
  },
};
