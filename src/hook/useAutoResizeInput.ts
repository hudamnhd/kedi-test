import { useEffect, useRef, useState } from "react";

export function useAutoResizeInput(
  initialInputText = "",
  initialToggle = false,
  initialToggleSub = 0
) {
  const [inputText, setInputText] = useState(initialInputText);
  const [inputTextSub, setInputTextSub] = useState(initialInputText);
  const [toggle, setToggle] = useState(initialToggle);
  const [isHeight, setIsHeight] = useState(0);
  const [toggleSub, setToggleSub] = useState(initialToggleSub);
  const inputTextRef = useRef<HTMLTextAreaElement>(null);

const autoResizeInput = () => {

  if (inputTextRef.current) {
    const maxHeight = 180; 
    inputTextRef.current.style.height = "auto";
    inputTextRef.current.style.height = `${Math.min(inputTextRef.current.scrollHeight, maxHeight)}px`;
    inputTextRef.current.style.overflow = inputTextRef.current.scrollHeight > maxHeight ? 'auto' : 'hidden';
    const currentHeight = inputTextRef.current.scrollHeight;
    const newHeight = Math.min(currentHeight, 180);
    setIsHeight(newHeight - 40);
    inputTextRef.current.style.transition = 'height 0.3s ease-in-out'; 
  }
};

  useEffect(() => {
    autoResizeInput();
  }, [inputText]);

  return {
    inputText,
    setInputText,
    inputTextSub,
    setInputTextSub,
    toggle,
    setToggle,
    inputTextRef,
    toggleSub,
    setToggleSub,
    isHeight,
    setIsHeight,
  };
}
