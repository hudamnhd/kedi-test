import useUserChat from "@/store/useUserChat";

import Pubnub from "pubnub";
import { encryptData, decryptData } from "@/utils/helper";

function updateMessagesByChannel(channelName, newMessages, decrypted) {
  let channelFound = false;
  const updatedMessagesArray = decrypted?.channels?.map((channel) => {
    const key = Object.keys(channel)[0];
    if (key === channelName) {
      channelFound = true;
      return { [channelName]: newMessages };
    }
    return channel;
  });

  if (!channelFound) {
    updatedMessagesArray.push({ [channelName]: newMessages });
  }

  return updatedMessagesArray;
}

function getMessagesByChannel(channel, decrypted) {
  const channelData = decrypted?.channels?.find((dt) => dt[channel]);
  return channelData ? channelData[channel] : [];
}

export const getMessageUser = async ({
  channel,
  init = false,
  isDelete = false,
  isMore = false,
}) => {
  useUserChat.setState({
    isFetchMessage: true,
  });
  if (init) {
    useUserChat.setState({
      isFetchFirst: true,
    });
  } else {
    useUserChat.setState({
      isFetchFirst: false,
    });
  }

  try {
    const storedData = localStorage.getItem("sessionPubnub");
    const storagePubnub = JSON.parse(storedData);

    const messagesKey = `Pb-msg-${storagePubnub?.uuid}`;
    const lastAllMsg = localStorage.getItem(messagesKey);

    const pubnub = new Pubnub({
      publishKey: storagePubnub.publishKey,
      subscribeKey: storagePubnub.subscribeKey,
      uuid: storagePubnub.uuid,
    });

    if (lastAllMsg) {
      const lastMessObj = JSON.parse(lastAllMsg);
      const decrypted = decryptData(lastMessObj, "KEDI");

      const messagesForChannel = getMessagesByChannel(channel, decrypted);
      if (init && !isDelete && !isMore) {
        const totalMessages = messagesForChannel.length;
        const startIndex = Math.max(totalMessages - 25, 0);
        const endIndex = totalMessages;

        useUserChat.setState({
          messageHistory: messagesForChannel.slice(startIndex, endIndex),
        });

        setTimeout(() => {
          useUserChat.setState({
            isFetchFirst: false,
          });
        }, 200);
      } else if (isMore) {
        useUserChat.setState({
          messageHistory: messagesForChannel,
        });
      }

      if (!isDelete) {
        useUserChat.setState({
          isFetchMessage: false,
        });
      }

      const messages = await pubnub.fetchMessages({
        channels: [channel],
        count: 100,
      });

      const data = messages.channels[channel] ? messages.channels[channel] : [];

      if (isDelete) {
        useUserChat.setState({
          messageHistory: data,
        });
        useUserChat.setState({
          isFetchMessage: false,
        });
      }

      const updatedMessagesArray = updateMessagesByChannel(
        channel,
        data,
        decrypted,
      );

      const encryptedData = encryptData(
        { channels: updatedMessagesArray },
        "KEDI",
      );

      localStorage.setItem(messagesKey, JSON.stringify(encryptedData));

      //return queryClient.setQueryData(["messageHistory"], data);
    } else {
      const messages = await pubnub.fetchMessages({
        channels: [channel],
        count: 100,
      });

      const data = messages.channels[channel] ? messages.channels[channel] : [];

      useUserChat.setState({
        messageHistory: data,
      });

      useUserChat.setState({
        isFetchFirst: false,
      });

      //return queryClient.setQueryData(["messageHistory"], data);
    }
  } catch (error) {
    console.error("Failed to fetch messages.", error);
  } finally {
    useUserChat.setState({
      isFetchMessage: false,
    });
  }
};
