type DateTimeFormatOptions = {
  localeMatcher?: "best fit" | "lookup";
  weekday?: "long" | "short" | "narrow";
  era?: "long" | "short" | "narrow";
  year?: "numeric" | "2-digit";
  month?: "numeric" | "2-digit" | "long" | "short" | "narrow";
  day?: "numeric" | "2-digit";
  hour?: "numeric" | "2-digit";
  minute?: "numeric" | "2-digit";
  second?: "numeric" | "2-digit";
  timeZoneName?: "long" | "short";
  formatMatcher?: "best fit" | "basic";
  hour12?: boolean;
};

export const converDateNoDay = (dateString) => {
  if (!dateString) return "Data tidak valid";
  // Validate date (optional, adjust as needed)
  const year = parseInt(dateString?.substring(0, 4));
  const month = parseInt(dateString?.substring(5, 7));
  const day = parseInt(dateString?.substring(8, 10));

  // Validate date (optional, adjust as needed)
  const bulanIndonesia = [
    "Januari",
    "Februari",
    "Maret",
    "April",
    "Mei",
    "Juni",
    "Juli",
    "Agustus",
    "September",
    "Oktober",
    "November",
    "Desember",
  ];

  const newDateString = `${day.toString().padStart(2, "0")} ${
    bulanIndonesia[month - 1]
  } ${year}`;
  return newDateString;
};

export const formatToTime = (inputDate: string) => {
  if (!inputDate) return "Data tidak valid";

  const date = new Date(inputDate);

  // Set zona waktu ke Waktu Indonesia Barat (WIB)
  const options = { timeZone: "Asia/Jakarta", hour12: false };
  const formattedTime = date.toLocaleTimeString("id-ID", options).slice(0, 5);

  return formattedTime;
};

export const formatDate = (isoDate: any) => {
  if (!isoDate) return "Data tidak valid";
  const options: DateTimeFormatOptions = {
    weekday: "long",
    year: "numeric",
    month: "long",
    day: "numeric",
  };
  const formattedDate = new Date(isoDate).toLocaleDateString("id-ID", options);
  return formattedDate;
};

export const formatTanggal = (tanggalISO: string) => {
  if (!tanggalISO) return "Data tidak valid";
  const inputDate = new Date(tanggalISO);

  const options: DateTimeFormatOptions = {
    weekday: "long",
    year: "numeric",
    month: "long",
    day: "numeric",
  };

  return inputDate.toLocaleDateString("id-ID", options);
};

// parse time from timetoken
export function formatTime(timestamp: any) {
  if (!timestamp) return "Data tidak valid";
  const parse = parseInt(timestamp);
  const waktu = parse / 1e4;
  const notificationTime = new Date(waktu);
  return notificationTime.toISOString();
}
// for startTime class
export function isStartTimeBeforeNow(startTime) {
  if (!startTime) return "Data tidak valid";
  const itemStartTime = new Date(startTime);
  const currentTime = new Date();

  return itemStartTime < currentTime;
}

// for endTime class absen
export function isEndTimePassed(endTime) {
  if (!endTime) return "Data tidak valid";
  const currentTime = new Date();
  const endTimeDate = new Date(endTime);

  return currentTime > endTimeDate;
}

export function formatTimeAndDate(isoDate: any) {
  if (!isoDate) return "Data tidak valid";
  var date = new Date(isoDate);

  const formatDate = date.toLocaleDateString("id-ID", {
    day: "numeric",
    month: "short",
    year: "numeric",
  });
  const formatHour = date.toLocaleTimeString("id-Id", {
    hour: "2-digit",
    minute: "2-digit",
  });
  const result = {
    date: formatDate,
    time: formatHour,
  };
  return result;
}

export function convertTime(
  timetoken: string,
  includeMonthName: boolean = false,
  onlyTime: boolean = false,
) {
  const parse = parseInt(timetoken, 10);
  const waktu = new Date(parse / 1e4);
  const tahun = waktu.getFullYear();
  const tanggal = String(waktu.getDate()).padStart(2, "0");
  const jam = String(waktu.getHours()).padStart(2, "0");
  const menit = String(waktu.getMinutes()).padStart(2, "0");
  const namaBulan = [
    "Januari",
    "Februari",
    "Maret",
    "April",
    "Mei",
    "Juni",
    "Juli",
    "Agustus",
    "September",
    "Oktober",
    "November",
    "Desember",
  ][waktu.getMonth()];

  if (includeMonthName) {
    return `${tanggal} ${namaBulan} ${tahun} ${jam}:${menit}`;
  } else if (onlyTime) {
    return `${jam}:${menit}`;
  } else {
    return `${tanggal} ${namaBulan} ${tahun}`;
  }
}

export function formatTimeHourOrDate(timestamp: any) {
  if (!timestamp) return "Data tidak valid";
  const parse = parseInt(timestamp);
  const waktu = parse / 1e4;
  const currentTime = new Date();
  const notificationTime = new Date(waktu);

  const timeDifference: any =
    currentTime.getTime() - notificationTime.getTime();
  const hoursDifference = Math.floor(timeDifference / (1000 * 60 * 60));

  if (hoursDifference < 24) {
    const formattedTime = notificationTime.toLocaleTimeString("id-ID", {
      hour: "2-digit",
      minute: "2-digit",
    });
    return formattedTime;
  } else {
    const formattedDate = notificationTime.toLocaleDateString("id-ID", {
      day: "numeric",
      month: "short",
    });
    return formattedDate;
  }
}

export const compareTimes = (timeStr1, timeStr2) => {
  const time1 = new Date(timeStr1);
  const time2 = new Date(timeStr2);

  const hour1 = time1.getHours();
  const minute1 = time1.getMinutes();

  const hour2 = time2.getHours();
  const minute2 = time2.getMinutes();

  const totalMinutes1 = hour1 * 60 + minute1;
  const totalMinutes2 = hour2 * 60 + minute2;

  if (totalMinutes1 < totalMinutes2) {
    return 1; // Tidak telat
  } else if (totalMinutes1 === totalMinutes2) {
    return 1; // Sama
  } else {
    return 2; // Telat
  }
};

export function isWithinTimeRange(startTime, endTime) {
  const currentTime = new Date();
  const startDateTime = new Date(startTime);
  const endDateTime = new Date(endTime);

  return currentTime >= startDateTime && currentTime <= endDateTime;
}
