import CryptoJS from 'crypto-js';

export const calculateProgress = (start: string, end: string) => {
  const startDate = new Date(start).getTime();
  const endDate = new Date(end).getTime();
  const currentDate = new Date().getTime();

  const totalDuration = endDate - startDate;
  const elapsedDuration = currentDate - startDate;
  const currentProgress = (elapsedDuration / totalDuration) * 100;

  const finalProgress = currentProgress > 100 ? 100 : currentProgress;

  return finalProgress;
};

export const formatBytes = (bytes: any, decimals = 2) => {
  if (!+bytes) return "0 Bytes";

  const k = 1024;
  const dm = decimals < 0 ? 0 : decimals;
  const sizes = [
    "Bytes",
    "KiB",
    "MiB",
    "GiB",
    "TiB",
    "PiB",
    "EiB",
    "ZiB",
    "YiB",
  ];

  const i = Math.floor(Math.log(bytes) / Math.log(k));

  return `${parseFloat((bytes / Math.pow(k, i)).toFixed(dm))} ${sizes[i]}`;
};

export const isValidUrl = (s: string) => {
  var expression =
    /[-a-zA-Z0-9@:%._\+~#=]{1,256}\.[a-zA-Z0-9()]{1,6}\b([-a-zA-Z0-9()@:%_\+.~#?&//=]*)?/gi;
  var regex = new RegExp(expression);

  return s?.match(regex);
};

export const rendertext = (text: string) => {
  const reg = /(http:\/\/|https:\/\/)((\w|=|\?|\.|\/|&|-)+)/g;
  return text.replace(
    reg,
    `<a class="hover:text-blue-600 duration-300 underline hyphens-auto break-words" href='$1$2' target='_blank'>$1$2</a>`,
  );
};


import { type_doc, type_jpg, type_pdf, type_png, type_ppt } from "@/assets";

export function getFileIcon(fileType) {
  const iconMapping = {
    doc: type_doc,
    jpg: type_jpg,
    pdf: type_pdf,
    png: type_png,
    ppt: type_ppt,
  };

  return iconMapping[fileType];
}

export function getFileType(fileName) {
  const [, extension] = fileName.split(".");
  const fileType = extension.toLowerCase();

  const fileTypeMapping = {
    png: "png",
    jpg: "jpg",
    jpeg: "jpg",
    gif: "gif",
    doc: "doc",
    docx: "doc",
    pdf: "pdf",
  };

  return fileTypeMapping[fileType] || "unknown";
}

let APIURL = process.env.API_LINK;

let baseUrl;
if (APIURL === "Development") {
  baseUrl = "http://localhost:3000/";
} else if (APIURL === "Staging") {
  baseUrl = "https://web-staging.kelas.digital/";
} else {
  baseUrl = "https://web.kelas.digital/";
}

export async function dynamicBlurDataUrl(url) {

  const base64str = await fetch(
    `${baseUrl}/_next/image?url=${url}&w=16&q=75`
  ).then(async (res) =>
    Buffer.from(await res.arrayBuffer()).toString('base64')
  );

  const blurSvg = `
    <svg xmlns='http://www.w3.org/2000/svg' viewBox='0 0 8 5'>
      <filter id='b' color-interpolation-filters='sRGB'>
        <feGaussianBlur stdDeviation='1' />
      </filter>

      <image preserveAspectRatio='none' filter='url(#b)' x='0' y='0' height='100%' width='100%' 
      href='data:image/avif;base64,${base64str}' />
    </svg>
  `;

  const toBase64 = (str) =>
    typeof window === 'undefined'
      ? Buffer.from(str).toString('base64')
      : window.btoa(str);

  return `data:image/svg+xml;base64,${toBase64(blurSvg)}`;
}

export const encryptData = (data, secretKey) => {
    const encryptedObject = {};
    for (const key in data) {
        if (Object.hasOwnProperty.call(data, key)) {
            const encryptedArray = data[key].map(item => CryptoJS.AES.encrypt(JSON.stringify(item), secretKey).toString());
            encryptedObject[key] = encryptedArray;
        }
    }
    return encryptedObject;
};

export const decryptData = (encryptedData, secretKey) => {
    const decryptedObject = {};
    for (const key in encryptedData) {
        if (Object.hasOwnProperty.call(encryptedData, key)) {
            const decryptedArray = encryptedData[key].map(encryptedItem => JSON.parse(CryptoJS.AES.decrypt(encryptedItem, secretKey).toString(CryptoJS.enc.Utf8)));
            decryptedObject[key] = decryptedArray;
        }
    }
    return decryptedObject;
};
