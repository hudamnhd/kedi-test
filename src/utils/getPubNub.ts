import axios from "axios";
import PubNub from "pubnub";
import { encryptData } from "@/utils/helper";
import { formatTime } from "@/utils/convertTime";

const compareDates = (a, b) => {
  return new Date(b.sortTime) - new Date(a.sortTime);
};

const formatSortTime = (dataItem) => {
  return dataItem.timetoken
    ? formatTime(dataItem.timetoken)
    : dataItem.createdDate;
};

const processAndSortData = (dataArray) => {
  if (dataArray.length === 0 || dataArray === null || dataArray === undefined)
    return [];
  const newData = dataArray.map((item) => {
    const sortTime = formatSortTime(item);
    return {
      ...item,
      sortTime: sortTime,
    };
  });

  newData.sort(compareDates);
  return newData;
};

export const getPubNub = async (user) => {
  let obj = null;
  const sessionToken = user?.accessToken;

  async function fetchData() {
    try {
      if (!sessionToken) return;

      const response = await axios.get(
        process.env.API_BE + "/api/auth/pubnub-token",
        {
          headers: {
            Authorization: `Bearer ${sessionToken}`,
          },
        },
      );

      if (response.status !== 200) {
        throw new Error("Gagal mengambil data");
      }

      localStorage.setItem("sessionPubnub", JSON.stringify(response.data.data));
      const storagePubnub = localStorage.getItem("sessionPubnub");

      if (storagePubnub) {
        await initializePubnub();
      }

      // setIsReady(true);
    } catch (error) {
      console.error("Error:", error);
      // Handle error
    }
  }

  await fetchData();

  async function initializePubnub() {
    const storagePubnub = JSON.parse(
      localStorage.getItem("sessionPubnub") ?? "",
    );

    const pubnub = new PubNub({
      publishKey: user?.publishKey,
      subscribeKey: user?.subscribeKey,
      uuid: user?.uuid,
    });

    const batchSize = 500;
    const MAX_ITEMS_PER_TAB = 30;
    let channelActive: any[] = [];
    let channelActivePrivate: any[] = [];
    let channelActiveGroup: any[] = [];
    let channelActiveClass: any[] = [];

    const groupChannels = storagePubnub?.channels?.filter((channelName) =>
      channelName.startsWith("chat-group."),
    );
    const privateChannels = storagePubnub?.channels?.filter((channelName) =>
      channelName.startsWith("private."),
    );
    const classChannels = storagePubnub?.channels?.filter((channelName) =>
      channelName.startsWith("group."),
    );

    channelActive = [...storagePubnub.channels];
    channelActiveClass = [...classChannels];
    channelActiveGroup = [...groupChannels];

    for (
      let startIndex = 0;
      startIndex < privateChannels.length;
      startIndex += batchSize
    ) {
      const endIndex = Math.min(startIndex + batchSize, privateChannels.length);
      const channelsBatch = privateChannels.slice(startIndex, endIndex);

      try {
        const messages = await pubnub.fetchMessages({
          channels: channelsBatch,
          count: 1,
        });
        const privateFilter = Object.keys(messages?.channels);
        channelActivePrivate = [
          ...new Set([...channelActivePrivate, ...privateFilter]),
        ];
      } catch (error) {
        console.error("Error fetching messages:", error);
      }
    }
    // setDisabled(false);
    localStorage.setItem("channelActive", JSON.stringify(channelActive));
    localStorage.setItem(
      "channelActivePrivate",
      JSON.stringify(channelActivePrivate),
    );
    localStorage.setItem(
      "channelActiveGroup",
      JSON.stringify(channelActiveGroup),
    );
    localStorage.setItem(
      "channelActiveClass",
      JSON.stringify(channelActiveClass),
    );

    const allChannels = [
      ...channelActiveClass.slice(-MAX_ITEMS_PER_TAB),
      ...channelActiveGroup.slice(-MAX_ITEMS_PER_TAB),
      ...channelActivePrivate.slice(-MAX_ITEMS_PER_TAB),
    ];

    const lastMessageKey = `LastMessage_${user?.id}`;
    let cachedDataArray;

    const getMetadata = async (dataArray) => {
      if (
        cachedDataArray &&
        JSON.stringify(dataArray) === JSON.stringify(cachedDataArray)
      ) {
        return cachedDataArray;
      }

      cachedDataArray = dataArray;

      const pubnub = new PubNub({
        publishKey: user?.publishKey,
        subscribeKey: user?.subscribeKey,
        uuid: user?.uuid,
      });

      if (dataArray.length > 0) {
        try {
          const response = await axios.post(
            process.env.API_BE + "/api/pubnub/metadata-detail",
            {
              channelList: dataArray,
            },
            {
              headers: {
                Authorization: `Bearer ${sessionToken}`,
              },
            },
          );
          const messages = await pubnub.fetchMessages({
            channels: dataArray,
            count: 1,
          });

          const metadata = response.data.data;
          const msgs = messages.channels;

          getListChat(metadata, msgs, undefined);
        } catch (error) {
          console.error("Gagal mengambil data metadataUsers.", error);
        }
      }
    };

    function getListChat(metadataUsers, messages) {
      if (metadataUsers !== null) {
        let filteredAndMergedData = [];
        let filteredAndMergedDataGroupChat = [];
        let filteredAndMergedDataClass = [];
        let allClassMessage = [];
        let privateMessage = [];
        let groupChatMessage = [];

        // Filter dan gabungkan data
        if (messages) {
          const filteredData = Object.values(messages)
            .map((channelArray) => channelArray[0])
            .filter((channel) => channel?.message !== undefined);

          filteredAndMergedDataClass = filteredData
            .sort((a, b) => b.timetoken - a.timetoken)
            .filter((item1) => {
              return metadataUsers?.class?.some(
                (item2) => item1.channel === item2.channel,
              );
            })
            .map((item1) => {
              const matchingItem2 = metadataUsers?.class.find(
                (item2) => item1.channel === item2.channel,
              );
              if (matchingItem2) {
                return {
                  ...item1,
                  ...matchingItem2,
                };
              }
              return null;
            })
            .filter((mergedItem) => mergedItem !== null);

          filteredAndMergedDataGroupChat = filteredData
            .filter((item1) => {
              return metadataUsers?.groupChat?.some(
                (item2) => item1.channel === item2.channel,
              );
            })
            .map((item1) => {
              const matchingItem2 = metadataUsers?.groupChat.find(
                (item2) => item1.channel === item2.channel,
              );
              if (matchingItem2) {
                return {
                  ...item1,
                  ...matchingItem2,
                };
              }
              return null;
            })
            .filter((mergedItem) => mergedItem !== null);

          filteredAndMergedData = filteredData
            .filter((item1) => {
              return metadataUsers?.private?.some(
                (item2) => item1.channel === item2.channel,
              );
            })
            .map((item1) => {
              const matchingItem2 = metadataUsers.private.find(
                (item2) => item1.channel === item2.channel,
              );
              if (matchingItem2) {
                return {
                  ...item1,
                  ...matchingItem2,
                };
              }
              return null;
            })
            .filter((mergedItem) => mergedItem !== null);
        }

        // Gabungkan data tanpa pesan dengan data yang memiliki pesan
        const classNoMessage = metadataUsers?.class.filter(
          (item1) =>
            !filteredAndMergedDataClass.some(
              (item2) => item1.channel === item2.channel,
            ),
        );

        const groupNoMessage = metadataUsers?.groupChat.filter(
          (item1) =>
            !filteredAndMergedDataGroupChat.some(
              (item2) => item1.channel === item2.channel,
            ),
        );

        let dataClassNoMsg = classNoMessage ?? [];
        let dataGroupNoMsg = groupNoMessage ?? [];

        let dataClass = [...filteredAndMergedDataClass, ...dataClassNoMsg];
        let dataGroup = [...filteredAndMergedDataGroupChat, ...dataGroupNoMsg];

        const _dataPrivate = filteredAndMergedData.map((item) => ({
          ...item,
          unread: item.unread || 0,
        }));
        privateMessage = processAndSortData(_dataPrivate);
        const _dataClass = dataClass.map((item) => ({
          ...item,
          unread: item.unread || 0,
        }));
        allClassMessage = processAndSortData(_dataClass);
        const _dataGroup = dataGroup.map((item) => ({
          ...item,
          unread: item.unread || 0,
        }));
        groupChatMessage = processAndSortData(_dataGroup);

        // Simpan data ke local storage setelah semua proses selesai
        const encryptedData = encryptData(
          { classMessage: allClassMessage, privateMessage, groupChatMessage },
          "KEDI",
        );

        localStorage.setItem(lastMessageKey, JSON.stringify(encryptedData));

        const allMsg = {
          classMessage: allClassMessage,
          privateMessage,
          groupChatMessage,
        };

        obj = allMsg;
      }
    }

    await getMetadata(allChannels); // run hanya sekali
  }
  return obj;
};
