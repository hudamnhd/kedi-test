import { NextPage } from "next";
import Link from "next/link";
import Image from "next/image";
import toast from "react-hot-toast";
import { LoadingSpinner } from "@/components/ui/loading";
import { signIn, getSession } from "next-auth/react";
import { useState, JSX } from "react";
import { useRouter } from "next/router";
import { logoLogin } from "@/assets";
import { trans } from "@/lang";

export const SignIn: NextPage = (): JSX.Element => {
  const [userInfo, setUserInfo] = useState({ email: "", password: "" });
  const [loading, setLoading] = useState(false);
  const [showPassword, setShowPassword] = useState(false);
  const router = useRouter();

  const locale: "en" | "id" = router.locale as "en" | "id";
  const lang = trans[locale].login;
  const { query } = router;
  const classId = query.classId as string;

  const handleSubmit = async (e: any) => {
    e.preventDefault();

    if (userInfo.email.trim() === "" || userInfo.password.trim() === "") {
      toast.error("Email dan password tidak boleh kosong");
      return;
    }
    try {
      setLoading(true);
      const res = await signIn("credentials", {
        email: userInfo.email,
        password: userInfo.password,
        redirect: false,
      });
      if (res?.error) {
        toast.error(res.error);
      } else {
        toast.success("Successfully Login");
        localStorage.clear()
        if (classId) {
          router.push("/class/" + classId);
        } else {
          router.push("/");
        }
      }
    } catch (error) {
      console.error("Error during form submission:", error);
    } finally {
      setLoading(false);
    }
  };

  const handleTogglePassword = () => {
    setShowPassword(!showPassword);
  };

  const directClass = "/register" + "?classId=" + classId;
  const linkLogin = classId ? directClass : "/register";

  return (
    <div className="h-screen flex  items-center justify-center bg-[#9D21E6]">
      <div className="flex flex-col items-center max-h-[95vh] bg-white rounded-xl my-4  mx-4 px-6 py-10 max-w-lg w-full">
        <Image
          src={logoLogin}
          alt="Logo Kelas Digital"
          className="max-w-[145px]   mx-auto"
        />
        <form className="mt-7 w-full" onSubmit={handleSubmit}>
          <div className="mb-4">
            <label
              htmlFor="UserEmail"
              className="block text-xs sm:text-sm  font-medium text-gray-700"
            >
              {lang.email}
            </label>

            <input
              value={userInfo.email}
              onChange={({ target }) =>
                setUserInfo({ ...userInfo, email: target.value })
              }
              placeholder={lang.plemail}
              type="email"
              disabled={loading}
              className="disabled:bg-gray-200 mt-1.5  px-4 py-1.5 sm:py-2.5 w-full rounded-[10px] border border-gray-400 outline-none focus:ring-2 focus:border-[#9D21E6] ring-[#9D21E6] shadow-sm sm:text-sm"
            />
          </div>
          <div className="mb-3">
            <label
              htmlFor="Userpassword"
              className="block text-xs sm:text-sm  font-medium text-gray-700"
            >
              {lang.password}
            </label>
            <div className="relative">
              <input
                value={userInfo.password}
                onChange={({ target }) =>
                  setUserInfo({ ...userInfo, password: target.value })
                }
                placeholder="****"
                type={showPassword ? "text" : "password"}
                disabled={loading}
                className="disabled:bg-gray-200 mt-1.5  px-4 py-1.5 sm:py-2.5 w-full rounded-[10px] border border-gray-400 outline-none focus:ring-2 focus:border-[#9D21E6] ring-[#9D21E6] shadow-sm sm:text-sm"
              />
              <div
                onClick={handleTogglePassword}
                className="absolute inset-y-0 top-1.5 right-0 pr-3 flex items-center text-sm leading-5"
              >
                {showPassword ? (
                  <svg
                    className="h-5 text-gray-700"
                    fill="none"
                    xmlns="http://www.w3.org/2000/svg"
                    viewBox="0 0 576 512"
                  >
                    <path
                      fill="currentColor"
                      d="M572.52 241.4C518.29 135.59 410.93 64 288 64S57.68 135.64 3.48 241.41a32.35 32.35 0 0 0 0 29.19C57.71 376.41 165.07 448 288 448s230.32-71.64 284.52-177.41a32.35 32.35 0 0 0 0-29.19zM288 400a144 144 0 1 1 144-144 143.93 143.93 0 0 1-144 144zm0-240a95.31 95.31 0 0 0-25.31 3.79 47.85 47.85 0 0 1-66.9 66.9A95.78 95.78 0 1 0 288 160z"
                    ></path>
                  </svg>
                ) : (
                  <svg
                    className="h-5 text-gray-700"
                    fill="none"
                    xmlns="http://www.w3.org/2000/svg"
                    viewBox="0 0 640 512"
                  >
                    <path
                      fill="currentColor"
                      d="M320 400c-75.85 0-137.25-58.71-142.9-133.11L72.2 185.82c-13.79 17.3-26.48 35.59-36.72 55.59a32.35 32.35 0 0 0 0 29.19C89.71 376.41 197.07 448 320 448c26.91 0 52.87-4 77.89-10.46L346 397.39a144.13 144.13 0 0 1-26 2.61zm313.82 58.1l-110.55-85.44a331.25 331.25 0 0 0 81.25-102.07 32.35 32.35 0 0 0 0-29.19C550.29 135.59 442.93 64 320 64a308.15 308.15 0 0 0-147.32 37.7L45.46 3.37A16 16 0 0 0 23 6.18L3.37 31.45A16 16 0 0 0 6.18 53.9l588.36 454.73a16 16 0 0 0 22.46-2.81l19.64-25.27a16 16 0 0 0-2.82-22.45zm-183.72-142l-39.3-30.38A94.75 94.75 0 0 0 416 256a94.76 94.76 0 0 0-121.31-92.21A47.65 47.65 0 0 1 304 192a46.64 46.64 0 0 1-1.54 10l-73.61-56.89A142.31 142.31 0 0 1 320 112a143.92 143.92 0 0 1 144 144c0 21.63-5.29 41.79-13.9 60.11z"
                    ></path>
                  </svg>
                )}
              </div>
            </div>
          </div>
          <div className="flex flex-wrap gap-3 justify-end">
            <Link
              href="/lupa-katasandi"
              className="text-[#9D21E6] cursor-pointer text-sm"
            >
              {lang.forgotPassword}
            </Link>
          </div>
          <div className="sm:text-base text-sm flex flex-col gap-y-2.5 justify-center mt-4 sm:mt-6 max-w-[300px] mx-auto">
            <button
              className="flex justify-center cursor-pointer bg-[#9D21E6] text-white rounded-full px-4 py-2.5 font-semibold w-full border border-gray-400 shadow-lg"
              type="submit"
              value="Login"
            >
              {loading ? (
                <LoadingSpinner stroke={`#FFF`} size={24} />
              ) : (
                lang.login
              )}
            </button>
            <button
              type="button"
              disabled={loading}
              onClick={() =>
                signIn("google", { callbackUrl: "http://localhost:3000/" })
              }
              className="hidden px-2 w-full border border-gray-400 shadow-lg font-semibold rounded-full py-2.5 flex justify-center items-center"
            >
              <svg
                stroke="currentColor"
                fill="currentColor"
                strokeWidth={0}
                version="1.1"
                x="0px"
                y="0px"
                viewBox="0 0 48 48"
                enableBackground="new 0 0 48 48"
                className="inline-block mr-2 h-5 lg:h-6 w-5 lg:w-6"
                height="1em"
                width="1em"
                xmlns="http://www.w3.org/2000/svg"
              >
                <path
                  fill="#FFC107"
                  d="M43.611,20.083H42V20H24v8h11.303c-1.649,4.657-6.08,8-11.303,8c-6.627,0-12-5.373-12-12
	c0-6.627,5.373-12,12-12c3.059,0,5.842,1.154,7.961,3.039l5.657-5.657C34.046,6.053,29.268,4,24,4C12.955,4,4,12.955,4,24
	c0,11.045,8.955,20,20,20c11.045,0,20-8.955,20-20C44,22.659,43.862,21.35,43.611,20.083z"
                />
                <path
                  fill="#FF3D00"
                  d="M6.306,14.691l6.571,4.819C14.655,15.108,18.961,12,24,12c3.059,0,5.842,1.154,7.961,3.039l5.657-5.657
	C34.046,6.053,29.268,4,24,4C16.318,4,9.656,8.337,6.306,14.691z"
                />
                <path
                  fill="#4CAF50"
                  d="M24,44c5.166,0,9.86-1.977,13.409-5.192l-6.19-5.238C29.211,35.091,26.715,36,24,36
	c-5.202,0-9.619-3.317-11.283-7.946l-6.522,5.025C9.505,39.556,16.227,44,24,44z"
                />
                <path
                  fill="#1976D2"
                  d="M43.611,20.083H42V20H24v8h11.303c-0.792,2.237-2.231,4.166-4.087,5.571
	c0.001-0.001,0.002-0.001,0.003-0.002l6.19,5.238C36.971,39.205,44,34,44,24C44,22.659,43.862,21.35,43.611,20.083z"
                />
              </svg>
              {lang.loginWithGoogle}
            </button>
          </div>

          <div className="flex flex-wrap gap-3 justify-center mt-2.5">
            <Link
              href={linkLogin}
              className="text-[#9D21E6] cursor-pointer text-sm"
            >
              <span className="text-gray-900">belum memiliki akun ?</span>{" "}
              <span className="font-bold uppercase">Register</span>
            </Link>
          </div>
          <div className="text-center font-medium text-sm mt-10">
            <p className="text-[#9D21E6]">Dibuat oleh</p>
            <p className="text-[#686464]">PT. KELAS DUNIA EKASAKTI</p>
          </div>
        </form>
      </div>
    </div>
  );
};

export default SignIn;

const Modal = () => {
  return (
    <>
      <div
        className="min-w-screen h-screen animated fadeIn faster  fixed  left-0 top-0 flex flex-col justify-center items-center inset-0 z-50 outline-none focus:outline-none"
        id="modal-id"
      >
        <div className="absolute bg-black/50 opacity-80 inset-0 z-0" />

        <div className="w-full  max-w-lg p-5 relative mx-4 sm:mx-auto my-auto rounded-xl shadow-lg  bg-white ">
          <p className="text-xs text-red-500 italic p-2 rounded-md mx-auto mb-4 text-center bg-purple-100">
            Login google hanya bisa ketika sudah mendaftarkan akun di Aplikasi
            Mobile
            <span className="text-gray-500 block">
              *Abaikan pesan ini jika anda tidak mencoba login google dengan
              akun baru
            </span>
          </p>
          <div className="">
            <div className="text-center pb-2.5 flex flex-col items-center justify-center">
              <svg
                xmlns="http://www.w3.org/2000/svg"
                fill="none"
                viewBox="0 0 24 24"
                strokeWidth={1.7}
                stroke="currentColor"
                className="w-16 h-16 text-[#9D21E6]"
              >
                <path
                  strokeLinecap="round"
                  strokeLinejoin="round"
                  d="M9.75 9.75l4.5 4.5m0-4.5l-4.5 4.5M21 12a9 9 0 11-18 0 9 9 0 0118 0z"
                />
              </svg>
              <h2 className="text-xl font-bold pt-4 pb-2 text-[#9D21E6]">
                Harap Login Ulang
              </h2>
              <p className="text-sm text-gray-500 px-8 text-[#686464]">
                Silakan klik tombol di bawah untuk ke halaman login
              </p>
            </div>
            {/*footer*/}
            <div className="max-w-[150px] sm:text-base text-sm flex flex-col gap-y-2.5 justify-center mt-2  mx-auto">
              <button
                className="flex justify-center cursor-pointer bg-[#9D21E6] text-white rounded-[10px] px-4 py-2.5 font-semibold w-full border border-gray-400 shadow-lg"
                type="button"
              >
                OK
              </button>
            </div>
          </div>
        </div>
      </div>
    </>
  );
};
export async function getServerSideProps(context: any) {
  try {
    const session = await getSession(context);
    if (session) {
      return {
        redirect: {
          destination: `/`,
          permanent: false,
        },
      };
    } else {
      return {
        props: {}, // Mengembalikan objek kosong jika tidak ada sesi yang tersedia
      };
    }
  } catch (error) {
    console.error("Kesalahan:", error);
    return {
      props: {
        error: "Terjadi kesalahan saat memuat sesi pengguna",
      },
    };
  }
}
