import { NextPage } from "next";
import Link from "next/link";
import Image from "next/image";
import toast from "react-hot-toast";
import cryptoJS from "crypto-js";
import { LoadingSpinner } from "@/components/ui/loading";
import { useState, JSX, ControlProps, useId } from "react";
import { signIn, getSession } from "next-auth/react";
import { useRouter } from "next/router";
import { logo_kedi_white } from "@/assets";
import { trans } from "@/lang";
import { useMutation } from "@tanstack/react-query";
import axios from "axios";
import { AsyncPaginate } from 'react-select-async-paginate';

export const SignIn: NextPage = (): JSX.Element => {
  const [phoneNumber, setPhoneNumber] = useState("");
  const [userInfo, setUserInfo] = useState({
    username: "",
    fullName: "",
    phoneNumber: "",
    email: "",
    password: "",
    refCode: getDateTime(),
    institution: "",
  });
console.log('userInfo:',  userInfo);
  const [passwordrep, setPasswordrep] = useState("");
  const [loading, setLoading] = useState(false);
  const [showPassword, setShowPassword] = useState(false);
  const [showPasswordR, setShowPasswordR] = useState(false);
  const router = useRouter();

  const locale: "en" | "id" = router.locale as "en" | "id";
  const lang = trans[locale].login;

  const { query } = router;
  const classId = query.classId as string;

const handleSubmit = async (e: any) => {
  e.preventDefault();
  
  const errors = validateUserInfo();

  if (Object.keys(errors).length === 0) {
    handleRegister.mutate(userInfo);
  } else {
    Object.values(errors).forEach(errorMsg => {
        toast.error(errorMsg);
        });
  }
};

  interface ApiResponse {
    data: {
      message: string;
    };
  }

const validateUserInfo = () => {
  const errors = {};

  if (!userInfo.username.trim()) {
    errors.username = "Username tidak boleh kosong";
  }

  if (!userInfo.fullName.trim()) {
    errors.fullName = "Nama lengkap tidak boleh kosong";
  }

  if (!userInfo.phoneNumber.trim()) {
    errors.phoneNumber = "Nomor telepon tidak boleh kosong";
  }

  if (!userInfo.email.trim()) {
    errors.email = "Email tidak boleh kosong";
  } else if (!/\S+@\S+\.\S+/.test(userInfo.email.trim())) {
    errors.email = "Email tidak valid";
  }

  if (!userInfo.password.trim()) {
    errors.password = "Password tidak boleh kosong";
  }

  if (!userInfo.institution.trim()) {
    errors.institution = "Institusi tidak boleh kosong";
  }

  return errors;
};
  interface PostResponse extends ApiResponse {}

  const handleRegister = useMutation(
    async (data): Promise<PostResponse> => {
      setLoading(true);
      try {
        const response = await axios.post<PostResponse>(
          process.env.API_BE + `/api/auth/public/student`,
          data,
          {},
        );
        setLoading(false);
        const status = response?.data?.meta?.status_code;
        const OK = status === 201;
        if (OK) {
          handleLogin();
        }
        toast.success(`Berhasil Register Akun `);

        return response.data;
      } catch (error) {
        setLoading(false);
        console.error(error);
        const status = error?.response?.data?.meta?.status_code;
        const OK = status === 400;
        if (OK) {
          handleLogin();
        }
        throw error;
      }
    },
    {
      onError: (error) =>
        toast.error(`Error: ${error.response.data.meta.message}`),
    },
  );

  async function handleLogin() {
    try {
      const res = await signIn("credentials", {
        email: userInfo.email,
        password: userInfo.password,
        redirect: false,
      });
      if (res?.error) {
        // toast.error(res.error);
      } else {
        toast.success("Successfully Login");
        if (classId) {
          router.push("/class/" + classId);
        } else {
          router.push("/");
        }
      }
    } catch (error) {
      console.error("Error during form submission:", error);
    } finally {
      setLoading(false);
    }
  }

  function getDateTime() {
    const now = new Date();

    const year = now.getFullYear();
    const month = (now.getMonth() + 1).toString().padStart(2, "0"); // tambah 1 karena indeks bulan dimulai dari 0
    const day = now.getDate().toString().padStart(2, "0");
    const hours = now.getHours().toString().padStart(2, "0");
    const minutes = now.getMinutes().toString().padStart(2, "0");
    const seconds = now.getSeconds().toString().padStart(2, "0");
    const result = `${year}${month}${day}${hours}${minutes}${seconds}_KELAS_DIGITAL`;
    const refCode = cryptoJS.AES.encrypt(result, "KELAS_DIGITAL").toString();

    return refCode;
  }
  const directClass = "/login" + "?classId=" + classId;
  const linkLogin = classId ? directClass : "/login";

  const loadOptions = async (searchQuery, loadedOptions, { page }) => {
      const response = await fetch(process.env.API_BE + `/api/university/search/univ?query=${searchQuery}&limit=50&page=${page}`);
      const data = await response.json();

      const options = [
          {
              value: 'Umum',
              label: 'Umum'
          },
          ...data.data.items.map(item => ({
              value: item.name,
              label: item.name
          }))
      ];

      return {
          options: options,
          hasMore: data.data.meta.currentPage < data.data.meta.totalPages,
          additional: {
              page: page + 1,
          },
      };
  };

  const customStyles = {
      // provide correct types here
      control: (provided: ControlProps, state: { isFocused: boolean }) => ({
          ...provided,
          padding: '2px',
          borderRadius: '8px',
          border: '1px solid #bdbdbd',
          boxShadow: state.isFocused ? '0 0 0 2px #9D21E6' : null,
      }),
      option: (provided: ControlProps, state: { isFocused: boolean }) => ({
          ...provided,
          backgroundColor: state.isFocused ? '#9D21E6' : null,
          color: state.isFocused ? 'white' : null,
      }),
  }

  return (
    <div className="min-h-screen flex flex-col items-center justify-center bg-[#9D21E6] px-2 py-10">
      <div>
        <Image
          src={logo_kedi_white}
          className="w-auto h-auto"
          alt="logo_kedi_white"
        />
      </div>
      <div className="flex flex-col items-center bg-white rounded-lg my-4  mx-2 px-6 pt-6 pb-16 max-w-xl w-full mb-32">
        <h2 className="text-[#9D21E6]  text-3xl font-bold sm:text-4xl">
          Daftar
        </h2>
        <form className="mt-8 w-full" onSubmit={handleSubmit}>
          <div className="mb-3">
            <label
              htmlFor="UserUsername"
              className="block text-xs sm:text-sm  font-medium text-gray-700"
            >
              Username
            </label>

            <input
              value={userInfo.username}
              onChange={({ target }) =>
                setUserInfo({ ...userInfo, username: target.value })
              }
              placeholder="username"
              type="text"
              disabled={loading}
              className="disabled:bg-gray-200 mt-1.5 px-4 py-1.5 sm:py-2.5 w-full rounded-[8px] border border-gray-400 outline-none focus:ring-2 focus:border-[#9D21E6] ring-[#9D21E6] shadow-sm sm:text-sm"
            />
          </div>
          <div className="mb-3">
            <label
              htmlFor="UserName"
              className="block text-xs sm:text-sm  font-medium text-gray-700"
            >
              Nama Lengkap
            </label>

            <input
              value={userInfo.fullName}
              onChange={({ target }) =>
                setUserInfo({ ...userInfo, fullName: target.value })
              }
              placeholder="Masukan nama lengkap"
              type="text"
              disabled={loading}
              className="disabled:bg-gray-200 mt-1.5 px-4 py-1.5 sm:py-2.5 w-full rounded-[8px] border border-gray-400 outline-none focus:ring-2 focus:border-[#9D21E6] ring-[#9D21E6] shadow-sm sm:text-sm"
            />
          </div>

          <div className="mb-3">
            <label
              htmlFor="UserNoHp"
              className="block text-xs sm:text-sm  font-medium text-gray-700"
            >
              No. Hp
            </label>

            <div className="flex items-center focus:ring-2 ring-[#9D21E6] rounded-[10px] border border-gray-400 mt-1">
              <button
                id="dropdown-phone-button"
                data-dropdown-toggle="dropdown-phone"
                className="flex-shrink-0 z-10 inline-flex items-center py-2.5 px-4 text-sm font-medium text-center text-gray-900 bg-gray-100  border-r border-gray-400 rounded-s-[10px] hover:bg-gray-200 focus:outline-none"
                type="button"
              >
                <svg
                  width={20}
                  height={22}
                  viewBox="0 0 32 22"
                  fill="none"
                  xmlns="http://www.w3.org/2000/svg"
                  xmlnsXlink="http://www.w3.org/1999/xlink"
                >
                  <rect width={32} height={22} fill="url(#pattern0)" />
                  <defs>
                    <pattern
                      id="pattern0"
                      patternContentUnits="objectBoundingBox"
                      width={1}
                      height={1}
                    >
                      <use
                        xlinkHref="#image0_2498_1068"
                        transform="matrix(0.00195312 0 0 0.00277444 0 -0.210256)"
                      />
                    </pattern>
                    <image
                      id="image0_2498_1068"
                      width={512}
                      height={512}
                      xlinkHref="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAgAAAAIACAMAAADDpiTIAAAAA3NCSVQICAjb4U/gAAAACXBIWXMAAA3XAAAN1wFCKJt4AAAAGXRFWHRTb2Z0d2FyZQB3d3cuaW5rc2NhcGUub3Jnm+48GgAAAnNQTFRF////EhshEhshEhshEhshEhshEhshEhshEhshEhshEhshEhshEhshEhshEhshEhshEhshEhshEhshEhshEhshEhshEhshEhshEhshEhshEhshEhshEhshEhshEhshEhshEhshEhshEhshEhshEhshEhshEhshEhshEhshEhshEhshEhshEhshEhshEhshEhshEhshEhshEhshEhshEhshEhshEhshEhshEhshEhshEhshEhshEhshEhshEhshEhshEhshEhshEhshEhshEhshEhshEhshEhshEhshExshExwiFBshFB0jFRshFR4kFhshFh8kFx8lGCAmGRshGxshHCUrHRshHxshHygtIhsiIiowJRsiJi4zKjI4LBoiLjY7LxoiLzc8MhoiNhoiNxoiOUFGOxojPURJQRojQUlNQhojRhojRxojR05TSVBUShojSxojTRojTlVaURkjVBkjVhkkWF5iWV9jXBkkXRkkXhkkX2VpYGZqYRkkZWtuZxkkZ2xwaBkkaRkkaW9zbhkkbxkkcHV5chklcxkldHl8d3yAehglfxglgIWIgYWJghglgoeKgxglh4yPiBgliRglihgljBgmjxgmkBgmkJWXkZWYkpaZkxgmmBgmmhgmmp6gmxgmnJ+inRgmnhgmnxcmoKSmohcmoqaooxcmpRcnqRcnqhcnq6+xrBcnrhcnrxcnsBcnsRcnshcntLe4t7q8uby+wcPFw8XGxMbHxsjKzM7Pzc/Q0dLT0dPU2Nna29zd3d7f3+Hh4eLj4+Tl5+jo6erq6+zt8fLy8/P09vb2+Pn5+fr6+/v8/Pz8/f39/v7+////Uyls4QAAAEh0Uk5TAAECBAUJDRMWGRobJygpKzAyODk6PEFCSUpLTVJTYWhpa215goOGiZGXmJmfoKawsbm/xsnKy9LT2d3h5Ofq7O3y9Pj7/P3+FU3bEwAACC1JREFUeNrt2umflXUdx+EvMCzDsO/KIjDs+74MOwxwIC0sSw0hQckwRUnNMNdIRTNcSkRDU9OkxYXUEskto7LU5k/qAVb4u+8zzKAPPL9zXQ/vM/rg832/4MzhRAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAnde114ARY86jJowZMaBX18/t9E1jp85ZuLy1Qk1pXb5wztSxTZ/1+v2aF2w0Zu3auKC531kfv8vgyUtNWPuWTh7c5WzuP8r182lgVKfPP3S+2XIyf2inzt93tslyM7tvh8/fOH29vfKzfnpjx+4/co2x8rRmZEfu37zBUrna0HzG83efZaaczep+ho/9Ftkob4va/XBw0AoL5W7FoOr3P2edffK37txq9+/v7X9dWNO//P49W2xTH1p6lt2/YZ5l6sW8hpIAprXzH2zde8vdDzz23FFqwnOPPXD3LXu3tnPQacX7j6v6w5ffduQP1KAjt11e9ajj0vsPr/KVnwtufNaStevZGy8ov2vr8OQNQJU3gNc8bcTa9vQ1Vd4IfvptwPjSH7rysAFr3+ErS487/vT791hZ9iM3vWy9HLx8U9l1V/Y4LYCJZX/7HzBdLg6UvROYeNo3QNYWX774kN3yceji4oXXNrb3EcBFT1otJ09e1M6HAX2KvwJuethmeXl4U/FXwT6fBDCjGMcdFsvNHcUrzzh1/26rC69cb6/8XF848+puERExrPDCpS+aKz8vXlo49LCIiJhSeO4XwCwdKBx6SkRELEsfb3/FWDl6ZXt66WUREQMLXTxoqzw9WDj1wIiYUPgXAEvlqvCvAhMiYnH68F5D5ere9NaLI3oXPgM6aqhcHS18GtQ7hqSPrrBTvq5Irz0kRqeP7jJTvu5Krz26+FWQp8yUr6eKXwuZlH4N4JiZ8nUs/WLApJiZfgxspZylHwfPjLnJk51GytnO5NxzY0ny5Goj5ezq5NxLYlXy5AYj5eyG5NyrIn1buM9IOduX3lsAAhCAAAQgAAEIQAACEIAAjCQABIAAEAACQAAIAAEgAASAABAAAkAACAABIAAEgAAQAAJAAAgAASAABIAAEAACQAAIAAEgAASAABAAAkAACAABIAAEgAAQAAJAAAgAASAABIAAEAACQAAIAAEgAASAABAAAkAACAABIAAEgAAQAAJAAAgAASAABCAAAQhAAAIQgAAEIAABCEAAAhCAAAQgAAEIQAACEIAAjCQABIAAEAACQAAIAAEgAASAABAAAkAACAABIAAEgAAQAAJAAAgAASAABIAAqKkArnu0iiMvtf+//s2hR/nCu+6MAVT3lZ0/+F216x/+7tYKtSg69dNbf1Z6/pf2fcmSdRFAZdNPSu5/7Nt2rJcAKt/4dTGAH5uxfgKofKdw/199zYx1FMCm3/oDoK4DqPw8DWCvFesqgB+lAWy3Yl0F8Iu2xC4r1lUAf0oD2G/Fegpg89/TAH5pxXoK4Pvp/dv+8nUz1k8A3/xrIQB/BNRRAJt/X7x/27+/Z8c6CWDHH9vK/PPgly2ZfwBf3f3Tf7RVceKH37JlHgHc/kYVf/5XW7v+dvwNvvBuP2MAB9vI2EEBCEAAAhCAAAQgAAEIQAACEIAABCAAAQhAAAIQgAAEIAAEgAAQAAJAAAgAASAABIAAEAACQAAIAAEgAASAABAAAkAACAABIAAEgAAQAAJAAAgAASAABIAAEAACQAAIAAEgAASAABAAAkAACAABIAAEgAAQAAJAAAgAASAABIAAEAACQAAIAAEgAASAABAAAkAACAABIAAEIAABCEAAAhCAAAQgAAEIQAACEIAABCAAAQhAAAIQgAAEIAAEgAAQAAJAAAgAASAABIAAEAACQAAIAAEgAASAABAAAkAA1FQAq5IH9xgpZ/ck514VS5InNxspZzcn514Sc5Mnu42Us93JuefGzOTJNiPlbFty7pkxKXmy5WMr5evjLcm5J8X49G3hO2bK1zvptcfH6PTRE2bK1xPptUfHkPTRtWbK17XptYdE7/TR5pN2ytXJzem1e0csTp89b6hcPZ/eenFETEgf7jFUrvakt54QEQPTh5XXLJWn1wqnHhgRsSx9uusjW+Xoo13ppZdFRMSUQhcvGCtHLxQOPSUiIoYVnm/7wFr5+WBb4dDDIiKi2+rCC3eaKz93Fs68ultERMSMwiuVx+2Vm8eLV55x6v7Rp7Xw0vmvWywvr59fOHJrn08CiGnFOC5522Y5efuS4o2n/ff+0bi2+Oplx62Wj+OXFS+8tvF/AcTE4suVC/0ymI0XLiw58MT/3z96rCz5gcr9H5ouBx/eX3bdlT1OC6D4tZBT/ypwwnq178Se0uOOP/3+0dBS+kOVW981YG1799byy7Y0fCqAGN5a/nNb7nvPiLXrvfu2lN+1dXgkxlWqueqRtyxZi9565KqqRx0XBdMq1e3Y/9Azr775/klqwvtvvvrMQ/t3tHPQacX7R8O8CnViXkNJANGzxTL1oaVnlOq/xjb1YE3/qOKcddbJ37pzo6pBK+yTuxWDoh1NiyyUt0VN0a7us2yUs1nd40yaN5gpVxuaowNG+mUgU2tGRoc0Tl9vrPysn94YHdV3tr1yM7tvdMbQ+SbLyfyh0VmjlpotF0tHxVnoMniyBnK4/uTBXeJs9WtesNGEtWvjguZ+8Rk1jZ06Z+HyVmPWltblC+dMHdsUn5euvQaMGHMeNWHMiAG9ugYAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA0Hn/ATVcyxVLzgCuAAAAAElFTkSuQmCC"
                    />
                  </defs>
                </svg>
                <span className="ml-1.5">+62</span>
              </button>
              <div className="relative w-full">
                <input
                  value={phoneNumber}
                  onChange={({ target }) => {
                    setPhoneNumber(target.value);
                    setUserInfo({
                      ...userInfo,
                      phoneNumber: "+62-" + target.value,
                    });
                  }}
                  placeholder="1234"
                  disabled={loading}
                  type="number"
                  autoComplete="off"
                  id="phone-input"
                  className="block p-2.5 w-full z-20 text-sm disabled:bg-gray-200  px-4 py-1.5 sm:py-2.5 w-full rounded-r-[10px]  outline-none shadow-sm sm:text-sm"
                />
              </div>
            </div>
          </div>
          <div className="mb-3">
            <label
              htmlFor="UserEmail"
              className="block text-xs sm:text-sm  font-medium text-gray-700"
            >
              {lang.email}
            </label>

            <input
              value={userInfo.email}
              onChange={({ target }) =>
                setUserInfo({ ...userInfo, email: target.value })
              }
              placeholder="email@kelas.digital"
              type="email"
              disabled={loading}
              className="disabled:bg-gray-200 mt-1.5 px-4 py-1.5 sm:py-2.5 w-full rounded-[8px] border border-gray-400 outline-none focus:ring-2 focus:border-[#9D21E6] ring-[#9D21E6] shadow-sm sm:text-sm"
            />
          </div>
          <div className="mb-3">
            <label
              htmlFor="Userpassword"
              className="block text-xs sm:text-sm  font-medium text-gray-700"
            >
              {lang.password}
            </label>
            <div className="relative">
              <input
                value={userInfo.password}
                onChange={({ target }) =>
                  setUserInfo({ ...userInfo, password: target.value })
                }
                placeholder="****"
                type={showPassword ? "text" : "password"}
                disabled={loading}
                className="disabled:bg-gray-200 mt-1.5 px-4 py-1.5 sm:py-2.5 w-full rounded-[8px] border border-gray-400 outline-none focus:ring-2 focus:border-[#9D21E6] ring-[#9D21E6] shadow-sm sm:text-sm"
              />
              <div
                onClick={() => setShowPassword(!showPassword)}
                className="absolute inset-y-0 top-1 right-0 pr-3 flex items-center text-sm leading-5"
              >
                {showPassword ? (
                  <svg
                    className="h-5 text-gray-700"
                    fill="none"
                    xmlns="http://www.w3.org/2000/svg"
                    viewBox="0 0 576 512"
                  >
                    <path
                      fill="currentColor"
                      d="M572.52 241.4C518.29 135.59 410.93 64 288 64S57.68 135.64 3.48 241.41a32.35 32.35 0 0 0 0 29.19C57.71 376.41 165.07 448 288 448s230.32-71.64 284.52-177.41a32.35 32.35 0 0 0 0-29.19zM288 400a144 144 0 1 1 144-144 143.93 143.93 0 0 1-144 144zm0-240a95.31 95.31 0 0 0-25.31 3.79 47.85 47.85 0 0 1-66.9 66.9A95.78 95.78 0 1 0 288 160z"
                    ></path>
                  </svg>
                ) : (
                  <svg
                    className="h-5 text-gray-700"
                    fill="none"
                    xmlns="http://www.w3.org/2000/svg"
                    viewBox="0 0 640 512"
                  >
                    <path
                      fill="currentColor"
                      d="M320 400c-75.85 0-137.25-58.71-142.9-133.11L72.2 185.82c-13.79 17.3-26.48 35.59-36.72 55.59a32.35 32.35 0 0 0 0 29.19C89.71 376.41 197.07 448 320 448c26.91 0 52.87-4 77.89-10.46L346 397.39a144.13 144.13 0 0 1-26 2.61zm313.82 58.1l-110.55-85.44a331.25 331.25 0 0 0 81.25-102.07 32.35 32.35 0 0 0 0-29.19C550.29 135.59 442.93 64 320 64a308.15 308.15 0 0 0-147.32 37.7L45.46 3.37A16 16 0 0 0 23 6.18L3.37 31.45A16 16 0 0 0 6.18 53.9l588.36 454.73a16 16 0 0 0 22.46-2.81l19.64-25.27a16 16 0 0 0-2.82-22.45zm-183.72-142l-39.3-30.38A94.75 94.75 0 0 0 416 256a94.76 94.76 0 0 0-121.31-92.21A47.65 47.65 0 0 1 304 192a46.64 46.64 0 0 1-1.54 10l-73.61-56.89A142.31 142.31 0 0 1 320 112a143.92 143.92 0 0 1 144 144c0 21.63-5.29 41.79-13.9 60.11z"
                    ></path>
                  </svg>
                )}
              </div>
            </div>
          </div>
          <div className="mb-3">
            <label
              htmlFor="Userpassword"
              className="block text-xs sm:text-sm  font-medium text-gray-700"
            >
              Ulangi {lang.password}
            </label>
            <div className="relative">
              <input
                value={passwordrep}
                onChange={({ target }) => setPasswordrep(target.value)}
                placeholder="****"
                type={showPasswordR ? "text" : "password"}
                disabled={loading}
                className="disabled:bg-gray-200 mt-1.5 px-4 py-1.5 sm:py-2.5 w-full rounded-[8px] border border-gray-400 outline-none focus:ring-2 focus:border-[#9D21E6] ring-[#9D21E6] shadow-sm sm:text-sm"
              />
              <div
                onClick={() => setShowPasswordR(!showPasswordR)}
                className="absolute inset-y-0 top-1 right-0 pr-3 flex items-center text-sm leading-5"
              >
                {showPasswordR ? (
                  <svg
                    className="h-5 text-gray-700"
                    fill="none"
                    xmlns="http://www.w3.org/2000/svg"
                    viewBox="0 0 576 512"
                  >
                    <path
                      fill="currentColor"
                      d="M572.52 241.4C518.29 135.59 410.93 64 288 64S57.68 135.64 3.48 241.41a32.35 32.35 0 0 0 0 29.19C57.71 376.41 165.07 448 288 448s230.32-71.64 284.52-177.41a32.35 32.35 0 0 0 0-29.19zM288 400a144 144 0 1 1 144-144 143.93 143.93 0 0 1-144 144zm0-240a95.31 95.31 0 0 0-25.31 3.79 47.85 47.85 0 0 1-66.9 66.9A95.78 95.78 0 1 0 288 160z"
                    ></path>
                  </svg>
                ) : (
                  <svg
                    className="h-5 text-gray-700"
                    fill="none"
                    xmlns="http://www.w3.org/2000/svg"
                    viewBox="0 0 640 512"
                  >
                    <path
                      fill="currentColor"
                      d="M320 400c-75.85 0-137.25-58.71-142.9-133.11L72.2 185.82c-13.79 17.3-26.48 35.59-36.72 55.59a32.35 32.35 0 0 0 0 29.19C89.71 376.41 197.07 448 320 448c26.91 0 52.87-4 77.89-10.46L346 397.39a144.13 144.13 0 0 1-26 2.61zm313.82 58.1l-110.55-85.44a331.25 331.25 0 0 0 81.25-102.07 32.35 32.35 0 0 0 0-29.19C550.29 135.59 442.93 64 320 64a308.15 308.15 0 0 0-147.32 37.7L45.46 3.37A16 16 0 0 0 23 6.18L3.37 31.45A16 16 0 0 0 6.18 53.9l588.36 454.73a16 16 0 0 0 22.46-2.81l19.64-25.27a16 16 0 0 0-2.82-22.45zm-183.72-142l-39.3-30.38A94.75 94.75 0 0 0 416 256a94.76 94.76 0 0 0-121.31-92.21A47.65 47.65 0 0 1 304 192a46.64 46.64 0 0 1-1.54 10l-73.61-56.89A142.31 142.31 0 0 1 320 112a143.92 143.92 0 0 1 144 144c0 21.63-5.29 41.79-13.9 60.11z"
                    ></path>
                  </svg>
                )}
              </div>
            </div>
          </div>

          <div className="mb-3">
            <label
              htmlFor="UserUsername"
              className="mb-2 block text-xs sm:text-sm  font-medium text-gray-700"
            >
              Institusi
            </label>
            <AsyncPaginate
                menuPosition='fixed'
                defaultValue={userInfo.institution} 
                onChange={(param) => setUserInfo({ ...userInfo, institution: param?.value ?? null })}
                loadOptions={loadOptions}
                isClearable
                styles={customStyles}
                instanceId={useId()}
                placeholder="Select institution"
                disabled={loading}
                additional={{
                    page: 1,
                }}
            />
          </div>
          <div className="sm:text-base text-sm flex flex-col gap-y-2.5 justify-center mt-4 sm:mt-6 max-w-[300px] mx-auto">
            <button
              className="flex justify-center cursor-pointer bg-[#9D21E6] text-white rounded-lg px-4 py-2.5 font-semibold w-full border border-gray-400 shadow-lg"
              type="submit"
              value="Login"
            >
              {loading ? (
                <LoadingSpinner stroke={`#FFF`} size={24} />
              ) : (
                "Sign Up"
              )}
            </button>
          </div>
          <div className="flex flex-wrap gap-3 justify-center mt-2.5">
            <Link
              href={linkLogin}
              className="text-[#9D21E6] cursor-pointer text-sm"
            >
              <span className="text-gray-900">
                Anda sudah memiliki akun KELAS DIGITAL?
              </span>{" "}
              <span className="font-bold uppercase italic">Login</span>
            </Link>
          </div>
        </form>
      </div>
    </div>
  );
};

export default SignIn;

export async function getServerSideProps(context: any) {
  const classId = context.query.classId;
  try {
    const session = await getSession(context);
    if (session) {
      return {
        redirect: {
          destination: `/class/${classId}`,
          permanent: false,
        },
      };
    } else {
      return {
        props: {}, // Mengembalikan objek kosong jika tidak ada sesi yang tersedia
      };
    }
  } catch (error) {
    console.error("Kesalahan:", error);
    return {
      props: {
        error: "Terjadi kesalahan saat memuat sesi pengguna",
      },
    };
  }
}
