import Link from "next/link";
import React from "react";
import { useState, useRef, useEffect, ChangeEvent } from "react";
import { Navbar } from "@/components/navbar/navbar";
import { getSession } from "next-auth/react";
import { LoadingSpinner } from "@/components/ui/loading";

import { useQuery, useMutation } from "@tanstack/react-query";
import axios from "axios";
import toast from "react-hot-toast";
import { trans } from "@/lang";
import { useRouter } from "next/router";
import { defaultImgDevelopment, defaultImgStaging, defaultImgProduction } from "@/assets/defaultImg";

let APIURL = process.env.API_LINK;

let defaultImg;
if (APIURL === 'Development') {
    defaultImg = defaultImgDevelopment;
} else if (APIURL === 'Staging') {
    defaultImg = defaultImgStaging;
} else {
    defaultImg = defaultImgProduction;
}

const Home = ({ user }: any) => {
  return (
    <div className="bg-[#FAFAFA]">
      <Navbar user={user} />
      <BuatKelas user={user} />
    </div>
  );
};

const initState = {
  message: "",
  feedDocumentStorageIds: [],
};

const BuatKelas = ({ user }: any) => {
  const router = useRouter();

  const locale: "en" | "id" = router.locale as "en" | "id";
  const lang = trans[locale].createPost;

  const sessionToken = user?.accessToken;
  const [fileClass, setFileClass] = React.useState<any>([]);
  const [data, setdata] = useState(initState);

  const [checkedItems, setCheckedItems] = useState([]);

  const handleChange = (
    e: React.ChangeEvent<HTMLInputElement | HTMLTextAreaElement>,
  ) => {
    const name = e.target.name;

    setdata({
      ...data,
      [name]: e.target.value,
    });
  };

  const isChecked = (itemId) => checkedItems.includes(itemId);

  const handleCheckboxChange = (itemId) => {
    setCheckedItems((prevCheckedItems) => {
      const updatedCheckedItems = isChecked(itemId)
        ? prevCheckedItems.filter((item) => item !== itemId)
        : [...prevCheckedItems, itemId];

      setdata({
        ...data,
        feedDocumentStorageIds: updatedCheckedItems,
      });

      return updatedCheckedItems;
    });
  };

  const handleFileChangeClass = (event: ChangeEvent<HTMLInputElement>) => {
    const file = event.target.files ?? null;
    if (file) {
      const filesArray = Array.from(file);
      setFileClass((prevFiles: any) => [...prevFiles, ...filesArray]);
    }
  };
  const handleDropClass = (event: any) => {
    event.preventDefault();

    const file = event.dataTransfer.files ?? null;
    if (file) {
      const filesArray = Array.from(file);
      setFileClass((prevFiles: any) => [...prevFiles, ...filesArray]);
    }
  };

  const uploadFileClass = (file: any, sessionToken: string) => {
    return new Promise((resolve, reject) => {
      const formData = new FormData();

      console.log(file)
      formData.append("file", file);
      formData.append("documentTypeId", "1");
      formData.append("isPrivate", "true");

      axios
        .post(process.env.API_BE + "/api/storage", formData, {
          headers: {
            "Content-Type": "multipart/form-data",
            Authorization: `Bearer ${sessionToken}`,
          },
        })
        .then((res) => {
          resolve(res.data.documentStorage.id);
        })
        .catch((err) => {
          reject(err.response.data.meta.message);
        });
    });
  };

  const handleUploadFiles = async () => {
    const uploadPromises = fileClass.map((file: any) =>
      uploadFileClass(file, sessionToken ?? ""),
    );
    try {
      const responses = await Promise.all(uploadPromises);
      if (fileClass.length > 0) {
        toast.success("Semua file berhasil diunggah");
      }
      return responses;
    } catch (error) {
      toast.error("Terjadi kesalahan saat mengunggah file");
      console.error("Error uploading files:", error);
      throw error;
    }
  };

  const fileInputRef = useRef<HTMLInputElement>(null);
  const inputFileRef = useRef(null);

  const handleButtonClick = () => {
    if (fileInputRef.current) {
      fileInputRef.current.click();
    }
  };

  const handleDragOver = (event: any) => {
    event.preventDefault();
  };

  const handleDragEnter = (event: any) => {
    event.preventDefault();
  };

  const handleDragLeave = (event: any) => {
    event.preventDefault();
  };

  useEffect(() => {
    if (fileClass.length > 0) {
      setCheckedItems([]);
    }
  }, [fileClass]);
  interface PostResponse extends Response {}

  type PostClass = {
    classId: number;
    name: string;
    instruction: string;
    startDate: string;
    dueDate: string | undefined;
  };

  const postClass: PostClass = useMutation(
    async (): Promise<PostResponse> => {
      if (!sessionToken) throw new Error("Missing session token");
      try {
        const uploading = await handleUploadFiles();
        const newData = {
          ...data,
          feedDocumentStorageIds: uploading.length > 0 ? uploading : data.feedDocumentStorageIds,
        };

        const response = await axios.post<PostResponse>(
          process.env.API_BE + `/api/feeds/post/simple`,
          newData,
          {
            headers: {
              Authorization: `Bearer ${sessionToken}`,
            },
          },
        );
        return response.data;
      } catch (error) {
        console.error(error);
        throw error;
      } finally {
        setdata(initState);
        setFileClass([]);
      }
    },
    {
      onSuccess: () => {
        toast.success("Postingan Berhasil Di Buat");
        router.push("/");
      },

      onError: (error) =>
        toast.error(`Error: ${error.response?.data?.meta?.message ?? error}`),
    },
  );

  return (
    <>
      {postClass.isLoading && (
        <div className="fixed flex items-center justify-center w-full h-full bg-white/30 z-10">
          <LoadingSpinner size={70} strokeWidth={2} />
        </div>
      )}
      <div className="h-[3.6rem]" />
      <div className="pb-[2rem] md:pb-10 max-w-4xl mx-auto px-2">
        <div className="mt-6 sm:mt-10 mb-6 sm:mb-8 flex items-center px-2">
          <Link
            href="/"
            className=" bg-purple-100 text-[#9d21e6] flex justify-center items-center rounded-full w-10 h-10  "
          >
            <svg
              stroke="currentColor"
              fill="currentColor"
              strokeWidth={0}
              viewBox="0 0 24 24"
              height={32}
              width={32}
              xmlns="http://www.w3.org/2000/svg"
            >
              <path d="M13.293 6.293 7.586 12l5.707 5.707 1.414-1.414L10.414 12l4.293-4.293z" />
            </svg>
          </Link>
          <h1 className="ml-4 sm:ml-6 font-semibold text-[#9d21e6] text-xl sm:text-2xl md:text-3xl font-montserrat-semibold">
            {lang.title}
          </h1>
        </div>
        <div className="w-full bg-white p-6 rounded-xl border shadow-md">
          <div className="sc-jXbUNg keBVCS">
            <form className="sc-fqkvVR kUnBHo comp-form">
              <section>
                <h1 className="font-bold text-lg text-neutral-90">
                  {lang.uploadImage}
                </h1>
                <div className="flex justify-between my-4">
                  <label className="text-lg font-normal text-[#9d21e6]">
                    Default Kelas Digital
                  </label>
                  <label className="sm:inline hidden text-lg font-normal text-gray-400 text-right">
                    Powered by Unsplash
                  </label>
                </div>
                <div className="flex flex-wrap mb-4 mt-6 sm:mt-11">
                  {defaultImg?.map((d) => (
                    <label
                      key={d.documentStorage.id}
                      className={`border-4 relative ${
                        isChecked(d.documentStorage.id)
                          ? "border-purple-500"
                          : "border-transparent"
                      } mr-2 rounded-xl cursor-pointer`}
                    >
                      <img
                        className="w-28 h-28 object-cover rounded-xl shadow-md"
                        src={d.url}
                        alt="Default image class"
                      />
                      <input
                        type="checkbox"
                        checked={isChecked(d.documentStorage.id)}
                        onChange={() =>
                          handleCheckboxChange(d.documentStorage.id)
                        }
                        className="hidden"
                      />
                    </label>
                  ))}
                </div>
                <>
                  <div
                    onDragOver={handleDragOver}
                    onDragEnter={handleDragEnter}
                    onDragLeave={handleDragLeave}
                    onDrop={handleDropClass}
                    className="p-0.5 bg-[#cfb6ff]/30 rounded-[10px] border-2 border-dashed border-[#9d21e6] mb-3"
                  >
                    <div className="relative flex  w-full gap-x-1.5 rounded-xl ">
                      <button
                        type="button"
                        className="h-24 w-full text-center p-2.5 items-center text-[#9d21e6] font-semibold inline-flex items-center justify-center rounded-full transition duration-500 ease-in-out focus:outline-none"
                        onClick={handleButtonClick}
                      >
                        <label
                          htmlFor="fileInput"
                          className="flex items-center gap-x-2.5  cursor-pointer"
                        >
                          <svg
                            xmlns="http://www.w3.org/2000/svg"
                            fill="none"
                            viewBox="0 0 24 24"
                            strokeWidth={1.5}
                            stroke="currentColor"
                            className="w-6 h-6"
                          >
                            <path
                              strokeLinecap="round"
                              strokeLinejoin="round"
                              d="M12 10.5v6m3-3H9m4.06-7.19l-2.12-2.12a1.5 1.5 0 00-1.061-.44H4.5A2.25 2.25 0 002.25 6v12a2.25 2.25 0 002.25 2.25h15A2.25 2.25 0 0021.75 18V9a2.25 2.25 0 00-2.25-2.25h-5.379a1.5 1.5 0 01-1.06-.44z"
                            />
                          </svg>
                          <span>{lang.fileInput}</span>
                        </label>
                        <input
                          ref={inputFileRef}
                          id="fileInput"
                          type="file"
                          multiple
                          accept="image/*"
                          className="hidden"
                          onChange={handleFileChangeClass}
                        />
                      </button>
                    </div>
                  </div>
                  {fileClass?.length > 0 && (
                    <div className="px-4 py-2.5 flex gap-x-4 overflow-y-auto max-h-[22vh] mb-4  rounded-[10px] border border-gray-400">
                      {fileClass?.map((file: any, index: number) => (
                        <div className="relative" key={index}>
                          <img
                            src={URL.createObjectURL(file)}
                            className="rounded-xl shadow-md"
                            alt={`File ${index + 1}`}
                            style={{
                              width: "100px",
                              height: "100px",
                              objectFit: "cover",
                            }}
                          />
                          <button
                            className="absolute top-1 right-1 cursor-pointer text-red-600 hover:bg-white rounded-full p-1.5 hover:ring-1 ring-red-600  duration-300"
                            onClick={() =>
                              setFileClass((prevFiles: any) => {
                                const updatedFiles = [...prevFiles];
                                updatedFiles.splice(index, 1);
                                return updatedFiles;
                              })
                            }
                          >
                            <svg
                              className="pointer-events-none fill-current w-4 h-4 ml-auto"
                              xmlns="http://www.w3.org/2000/svg"
                              width={24}
                              height={24}
                              viewBox="0 0 24 24"
                            >
                              <path
                                className="pointer-events-none"
                                d="M3 6l3 18h12l3-18h-18zm19-4v2h-20v-2h5.711c.9 0 1.631-1.099 1.631-2h5.316c0 .901.73 2 1.631 2h5.711z"
                              />
                            </svg>
                          </button>
                        </div>
                      ))}
                    </div>
                  )}
                </>

                <div className="grid mt-14 gap-x-10">
                  <div>
                    <div className="mb-5">
                      <label
                        htmlFor="message"
                        className="block  font-medium text-gray-700"
                      >
                        {lang.description}
                      </label>
                      <textarea
                        id="message"
                        name="message"
                        className="w-full resize-none px-3 py-2 text-sm mt-3 rounded-[10px] border border-gray-400  outline-none  shadow-sm focus:ring-2 ring-[#9d21e6]"
                        value={data?.message}
                        onChange={handleChange}
                        rows={4}
                        placeholder={lang.description}
                      ></textarea>
                    </div>
                  </div>
                </div>
              </section>

              <div className="w-full flex justify-center items-center my-4 mt-6">
                <div className="w-full">
                  <button
                    type="button"
                    onClick={() => postClass.mutate()}
                    className="p-2.5 w-32  bg-[#9d21e6] text-white shadow-xl rounded-[10px] font-semibold"
                  >
                    {lang.submit}
                  </button>
                </div>
              </div>
            </form>
          </div>
        </div>
      </div>
    </>
  );
};

export default Home;

export async function getServerSideProps(context: any) {
  try {
    const session = await getSession(context);
    if (session) {
      return {
        props: { user: session.user },
      };
    } else {
      return {
        redirect: {
          destination: `/login`,
          permanent: false,
        },
      };
    }
  } catch (error) {
    console.error("Kesalahan:", error);
    return {
      props: {
        error: "Terjadi kesalahan saat memuat sesi pengguna",
      },
    };
  }
}
