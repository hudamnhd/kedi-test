import { getSession } from "next-auth/react";
import React, { useEffect, useState } from "react";
import Link from "next/link";
import Image from "next/image";
import { trans } from "@/lang";
import { useRouter } from "next/router";
import { useMediaQuery } from "react-responsive";
import { Navbar } from "@/components/navbar/navbar";
import { converDateNoDay } from "@/utils/convertTime";
 
// import { useInView } from "react-intersection-observer";
import { Select, SelectContent, SelectItem, SelectTrigger, SelectValue, } from "@/components/ui/select";

  function formatCurrency(amount) {
    if (!amount) return 0;
    const formattedAmount = new Intl.NumberFormat("id-ID", {
      style: "currency",
      currency: "IDR",
      minimumFractionDigits: 2,
    }).format(amount);

    return formattedAmount;
  }

function getStatusLabel(status) {
    switch (status) {
        case "SETTLEMENT":
            return "Sudah Dibayar";
        case "CANCEL":
        case "EXPIRE":
        case "EXPIRED":
        case "ERROR":
            return "Transaksi Dibatalkan";
        case "awaiting-approval":
            return "Menunggu Disetujui";
        case "PENDING":
            return "Menunggu Pembayaran";
        case "NEW":
        case "BARU":
            return "Belum Dibayar";
        default:
            return "Status tidak diketahui";
    }
}

const ClassMaterial = ({ user, payment }: any) => {

const notifFinance = payment?.data;
  return (
        <div className="bg-[#FAFAFA]">
            <Navbar user={user} />
            <Kelasz user={user} notifFinance={notifFinance} />
        </div>
  );
};

export default ClassMaterial;

const Kelasz = ({ user, notifFinance }: any) => {
    const [filterNotifFinance, setFilterNotifFinance] = useState<any[]>([]);
    const [valueFilterNotif, setValueFilterNotif] = useState("Semua");
    const [dataNotifFinance, setDataNotifFinance] = useState([]);
    const [value, setValue] = useState("nonAcademic");
    const [searchTerm, setSearchTerm] = useState("");
    const [searchResults, setSearchResults] = useState<[]>(dataNotifFinance);

    const handleSearch = () => {
        const filteredMessages = dataNotifFinance.filter(message =>
            message?.invoiceItems[0]?.name.toLowerCase().includes(searchTerm.toLowerCase()) || (message?.totalAmount && message.totalAmount.toString().includes(searchTerm.toLowerCase()))
        );
        setSearchResults(filteredMessages);
};


  useEffect(() => {
    handleSearch();
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [searchTerm]);

  useEffect(() => {
    setSearchResults(dataNotifFinance);
  }, [dataNotifFinance]);


    useEffect(() => {

        const filteredData = notifFinance?.filter((d) => {
            const status        = d.status;
            const isCancel      = status === "CANCEL" ||  status === "EXPIRE" || status === "EXPIRED" || status === "ERROR" ;
            const isSuccess     = status === "SETTLEMENT";
            const isPending     = status === "PENDING";
            const isWaitApprove = status === "awaiting-approval";
            const isWaitPay     = status === "NEW" || status === "BARU";
            
            if (valueFilterNotif === "Semua") {
                return true;
            } else if (valueFilterNotif === "Belum Dibayar") {
                return isWaitPay;
            } else if (valueFilterNotif === "Transaksi Dibatalkan") {
                return isCancel;
            } else if (valueFilterNotif === "Menunggu Disetujui") {
                return isWaitApprove
            } else if (valueFilterNotif === "Sudah Dibayar") {
                return isSuccess;
            } else if (valueFilterNotif === "Menunggu Pembayaran") {
                return isPending;
            }
        })

        setDataNotifFinance(filteredData)
    }, [valueFilterNotif, notifFinance]);
    useEffect(() => {

        let uniqueStatus = new Set();

        uniqueStatus.add("Semua");
        notifFinance?.forEach(item => {
            const status = getStatusLabel(item.status)
            uniqueStatus.add(status);
        });

        let uniqueStatusArray = Array.from(uniqueStatus);

        setFilterNotifFinance(uniqueStatusArray); 
    }, [notifFinance]);

  return (
    <>
      <section className="bg-white shadow-xl  w-full flex items-center gap-x-3">
        <ListClass
          searchResults={searchResults}
          value={value}
          user={user}
          setValue={setValue}
          searchTerm={searchTerm}
          setSearchTerm={setSearchTerm}
          dataNotifFinance={dataNotifFinance}
          valueFilterNotif={valueFilterNotif} setValueFilterNotif={setValueFilterNotif} filterNotifFinance={filterNotifFinance}
        />
      </section>
    </>
  );
};

const ListClass = (props: any) => {
  const { searchResults,  searchTerm, setSearchTerm, valueFilterNotif, setValueFilterNotif, filterNotifFinance } = props;

  const router = useRouter();
  const locale: "en" | "id" = router.locale as "en" | "id";
  const lang = trans[locale].dashboard;

  const isMD = useMediaQuery({
    query: "(max-width: 769px)",
  });

  const heightContainer = isMD
    ? { height: "calc(100vh - 270px)" }
    : { height: "calc(100vh - 220px)" };

    const [viewportHeight, setViewportHeight] = useState(0);

    useEffect(() => {
        const adjustHeight = () => {
        const vh = window.innerHeight ;
        setViewportHeight(vh);
        };
        adjustHeight();

        window.addEventListener('resize', adjustHeight);
        return () => {
        window.removeEventListener('resize', adjustHeight);
        };
    }, []);

  return (
    <>
      <div style={{ height: viewportHeight, overflow: 'auto' }} className="pt-[4.4rem] sm:pt-[5.2rem] pb-[3.8rem] lg:pb-0 pb-2.5 className flex flex-col w-full max-w-2xl mx-auto shadow-md px-3 sm:px-5">
        <TitleMenu
          lang={lang}
          setSearchTerm={setSearchTerm}
          searchTerm={searchTerm}
          
          title={`Kedi Pay`}
        />
        <div className="w-full">
          <SelectAndSearch lang={lang} valueFilterNotif={valueFilterNotif} setValueFilterNotif={setValueFilterNotif} filterNotifFinance={filterNotifFinance}/>
          <div className="overflow-y-auto pr-2">
            {searchResults?.length > 0 ? 
                searchResults?.map((d, index) => {
                const status = getStatusLabel(d.status);
                const textStatus =
                    d?.status === "SETTLEMENT" ? "text-[#1DA26A]"
                    : d?.status === "CANCEL" || d?.status === "EXPIRE" || d?.status === "EXPIRED" || d?.status === "ERROR" ? "text-red-500"
                        : d?.status === "PENDING" ? "text-yellow-500"
                        : d?.status === "awaiting-approval" ? "text-yellow-600"
                        : d?.status === "NEW" || d?.status === "BARU" ? "text-amber-600" : "text-gray-700";

                const isType = d?.notif_type === "payment" ? "Total Bayar : " : d?.notif_type === "payout" ? "Total Penarikan : " : "";
                return (
                    <div
                    key={index}
                    className="text-sm justify-between py-2 px-4 border-2 rounded-lg mb-1.5 hover:bg-purple-100"
                    >
                    <Link href={d?.invoiceIdentifier ? "/payment/" + d?.invoiceIdentifier : "#"}>
                        <div className="flex items-center justify-between space-x-4 mb-2">
                            <div className="flex-none px-1  text-stone-600 text-xs">
                            {converDateNoDay(d?.createdAt)}
                            </div>
                            <div
                            className={` ${textStatus} font-bold  text-sm`}
                            >
                            {status}
                            </div>
                        </div>
                        <div className="flex items-center space-x-4">
                            <div className="flex-none relative w-14 h-14">
                            <Image
                                width={60}
                                height={60}
                                src={`/static/images/logokedi.svg`}
                                className="absolute inset-0 h-full w-full object-cover rounded-xl shadow-md border border-gray-300"
                                alt=""
                            />
                            </div>
                            <div className="flex flex-col space-y-1">
                            <span className="font-bold">
                                {d?.invoiceItems?.length > 0 ? "( " + d?.invoiceItems[0]?.name + " )" : d?.extra_data?.class_name ? "( " + d?.extra_data?.class_name + " )" : d?.invoiceIdentifier ? "( " + d?.invoiceIdentifier + " )" : "Tidak diketahui"}
                            </span>
                            <span className="text-sm">
                                {isType}
                                <span className="text-[#1DA26A] font-semibold">
                                {d?.totalAmount ? formatCurrency( d?.totalAmount) : formatCurrency(0) }
                                </span>
                            </span>
                            </div>
                        </div>
                    </Link>
                    </div>
                );
                }) : (
                <div className="flex items-center justify-center h-full text-sm opacity-70">
                No data
                </div>
                )} 
          </div>
        </div>
      </div>
    </>
  );
};

const TitleMenu = ({ title, lang, searchTerm, setSearchTerm }) => {
  const [focus, setFocus] = useState(false);
  const focusSearch = () => {
    const searchInput = document.getElementById("searchInput");
    if (searchInput) {
      searchInput.focus();
    }
  };
  return (
    <div className="relative mb-5 flex items-center justify-between pt-3 w-full h-[40px]">
      <Link href="/dashboard" className="flex items-center flex-none">
        <div className="flex-none bg-[#9D21E6] flex justify-center items-center rounded-full w-8 h-8 lg:w-10 lg:h-10 p-1">
          <svg
            stroke="currentColor"
            fill="currentColor"
            strokeWidth={0}
            viewBox="0 0 1024 1024"
            className="text-white "
            height={20}
            width={20}
            xmlns="http://www.w3.org/2000/svg"
          >
            <path d="M872 474H286.9l350.2-304c5.6-4.9 2.2-14-5.2-14h-88.5c-3.9 0-7.6 1.4-10.5 3.9L155 487.8a31.96 31.96 0 0 0 0 48.3L535.1 866c1.5 1.3 3.3 2 5.2 2h91.5c7.4 0 10.8-9.2 5.2-14L286.9 550H872c4.4 0 8-3.6 8-8v-60c0-4.4-3.6-8-8-8z" />
          </svg>
        </div>
        <h1 className="ml-2 lg:ml-4 text-lg sm:text-xl md:text-2xl font-bold text-[#9D21E6] self-center ">
          {title}
        </h1>
      </Link>
      <div
        className={` ${
          focus ? "w-full absolute" : "relative"
        } duration-500 z-10 text-gray-600 `}
      >
        <input
          className="sm:w-[135px] w-[0px] peer focus:w-full focus:duration-500  focus:border  focus:shadow-md border-gray-200 bg-white h-11 focus:pl-10 pr-4 rounded-lg text-sm focus:outline-none"
          type="search"
          name="search"
          id="searchInput"
          onFocus={() => setFocus(true)}
          onBlur={() => setFocus(false)}
          autoComplete="off"
          value={searchTerm}
          onChange={(e) => setSearchTerm(e.target.value)}
          placeholder={lang.search}
        />
        <button
          onClick={focusSearch}
          className="absolute  peer-focus:duration-500 -left-6 peer-focus:left-3 top-0.5 mt-3"
        >
          <svg
            className="text-gray-600 h-4 w-4 fill-current"
            xmlns="http://www.w3.org/2000/svg"
            xmlnsXlink="http://www.w3.org/1999/xlink"
            version="1.1"
            id="Capa_1"
            x="0px"
            y="0px"
            viewBox="0 0 56.966 56.966"
            style={{ enableBackground: "new 0 0 56.966 56.966" }}
            xmlSpace="preserve"
            width="512px"
            height="512px"
          >
            <path d="M55.146,51.887L41.588,37.786c3.486-4.144,5.396-9.358,5.396-14.786c0-12.682-10.318-23-23-23s-23,10.318-23,23  s10.318,23,23,23c4.761,0,9.298-1.436,13.177-4.162l13.661,14.208c0.571,0.593,1.339,0.92,2.162,0.92  c0.779,0,1.518-0.297,2.079-0.837C56.255,54.982,56.293,53.08,55.146,51.887z M23.984,6c9.374,0,17,7.626,17,17s-7.626,17-17,17  s-17-7.626-17-17S14.61,6,23.984,6z" />
          </svg>
        </button>
      </div>
      {/*      <div className="hidden hover:bg-[#CFB6FF] relative flex justify-center cursor-pointer items-center rounded-full w-6 h-6 lg:w-10 lg:h-10">
        <svg
          stroke="currentColor"
          fill="currentColor"
          strokeWidth={0}
          viewBox="0 0 16 16"
          className="text-[#9D21E6] cursor-pointer w-5 h-5"
          xmlns="http://www.w3.org/2000/svg"
        >
          <path d="M9.5 13a1.5 1.5 0 1 1-3 0 1.5 1.5 0 0 1 3 0zm0-5a1.5 1.5 0 1 1-3 0 1.5 1.5 0 0 1 3 0zm0-5a1.5 1.5 0 1 1-3 0 1.5 1.5 0 0 1 3 0z" />
        </svg>
      </div>*/}
    </div>
  );
};

const SelectAndSearch = ({ lang, valueFilterNotif, setValueFilterNotif, filterNotifFinance }: any) => {
  return (
    <div className="flex items-center mb-2.5 mt-1.5 gap-x-3">
      <Select defaultValue="Semua" value={valueFilterNotif} onValueChange={setValueFilterNotif}>
        <SelectTrigger className="shadow-md rounded-lg">
          <SelectValue placeholder={lang.class} />
        </SelectTrigger>
              <SelectContent ref={(ref) => ref?.addEventListener('touchend', (e) => e.preventDefault())} >
            {filterNotifFinance.length > 0 && filterNotifFinance.map((status) =>  {  
                return  (
                <SelectItem key={status} value={status} className="py-2.5">{status}</SelectItem>
            )})}
        </SelectContent>
      </Select>
    </div>
  );
};

export async function getServerSideProps(context: any) {
  try {
    const session = await getSession(context);
    if (session) {
      const session = await getSession(context);
      const response = await fetch(
        process.env.API_URL + `/api/finance/pay/history`,
        {
          method: "GET",
          headers: {
            "Content-Type": "application/json",
            Authorization: `Bearer ${session?.user?.accessToken}`,
          },
        },
      );

      const payment = await response.json(); // atau response.text();
      const { user } = session;
      return {
        props: { user, payment },
      };

    } else {
      return {
        redirect: {
          destination: `/login`,
          permanent: false,
        },
      };
    }
  } catch (error) {
    console.error("Kesalahan:", error);
    return {
      props: {
        error: "Terjadi kesalahan saat memuat sesi pengguna",
      },
    };
  }
}
