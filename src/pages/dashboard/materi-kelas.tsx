import { getSession } from "next-auth/react";
import React, { useEffect, useState, useRef, ChangeEvent } from "react";
import Link from "next/link";
import axios from "axios";
import Image from "next/image";
import toast from "react-hot-toast";
import { trans } from "@/lang";
import { useRouter } from "next/router";
import { useMediaQuery } from "react-responsive";
import { useQuery, useQueryClient, useMutation } from "@tanstack/react-query";
// import { Select, SelectContent, SelectItem, SelectTrigger, SelectValue, } from "@/components/ui/select";
import { Popover, PopoverContent, PopoverTrigger, } from "@/components/ui/popover";
import { Dialog, DialogContent, DialogHeader, DialogTitle, } from "@/components/ui/dialog";
import { Tabs, TabsContent, TabsList, TabsTrigger } from "@/components/ui/tabs";
import { LoadingSpinner } from "@/components/ui/loading";
import { Navbar } from "@/components/navbar/navbar";
import { FileInput } from "@/components/kelas/module/element/FileInput";
import { formatTanggal, formatTimeHourOrDate, formatToTime, } from "@/utils/convertTime";

interface ApiResponse {
  data: {
    message: string;
  };
}

interface PostResponse extends ApiResponse {}

function LoadingStatus() {
  return (
    <div className="my-2 mx-2 p-5 border border-gray-200 rounded-lg shadow animate-pulse relative">
      <div className="flex items-center">
        <svg
          className="w-10 h-10 me-3 text-gray-200 dark:text-gray-700"
          aria-hidden="true"
          xmlns="http://www.w3.org/2000/svg"
          fill="currentColor"
          viewBox="0 0 20 20"
        >
          <path d="M10 0a10 10 0 1 0 10 10A10.011 10.011 0 0 0 10 0Zm0 5a3 3 0 1 1 0 6 3 3 0 0 1 0-6Zm0 13a8.949 8.949 0 0 1-4.951-1.488A3.987 3.987 0 0 1 9 13h2a3.987 3.987 0 0 1 3.951 3.512A8.949 8.949 0 0 1 10 18Z" />
        </svg>
        <div>
          <div className="h-2.5 bg-gray-200 rounded-full dark:bg-gray-700 w-32 mb-2" />
          <div className="w-48 h-2 bg-gray-200 rounded-full dark:bg-gray-700" />
        </div>
      </div>
      <span className="sr-only">Loading...</span>
    </div>
  );
}

function SkeltonList() {
  const loadingStatuses = Array.from({ length: 8 }, (_, index) => (
    <LoadingStatus key={index} />
  ));

  return loadingStatuses;
}

const ClassMaterial = ({ user }: any) => {
  return (
        <div className="bg-[#FAFAFA]">
            <Navbar user={user} />
            <Kelasz user={user}/>
        </div>
  );
};

export default ClassMaterial;

const Kelasz = ({ user }: any) => {
  const sessionToken = user?.accessToken;

  const [classMessage, setClassMessage] = useState<any[]>([]);
  const [AllCassMessage, setAllClassMessage] = useState<any[]>([]);
  const [value, setValue] = useState("nonAcademic");
  const [searchTerm, setSearchTerm] = useState("");
  const [searchResults, setSearchResults] = useState<[]>(classMessage);

  const handleSearch = () => {
    const filteredMessages = classMessage.filter(
      (message) =>
        message.name.toLowerCase().includes(searchTerm.toLowerCase()) ||
        message.username.toLowerCase().includes(searchTerm.toLowerCase()) ||
        message.description.toLowerCase().includes(searchTerm.toLowerCase()),
    );
    setSearchResults(filteredMessages);
  };

  const { data: materialClassFilter, isLoading } = useQuery(
    ["materialClassFilter"],
    async () => {
      const response = await axios.get(
        process.env.API_BE +
          "/api/user/class-pagination?by=material&limit=200&page=1",
        {
          headers: {
            Authorization: `Bearer ${sessionToken}`,
          },
        },
      );

      return response.data.data.items;
    },
    {
      refetchOnWindowFocus: false,
    },
  );

  useEffect(() => {
    handleSearch();
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [searchTerm]);

  useEffect(() => {
    setSearchResults(classMessage);
  }, [classMessage]);

  useEffect(() => {
    const trueArray = materialClassFilter?.filter((item) => item.isAcademic === true);
    const falseArray = materialClassFilter?.filter(
      (item) => item.isAcademic === false,
    );
    const allArray = materialClassFilter ?? [];

    const valueToFilterMap = {
      semua: allArray,
      academic: trueArray,
      nonAcademic: falseArray,
    };

    const filteredArray = valueToFilterMap[value] || allArray;

{/*    function formatTime(timestamp: any) {
      const parse = parseInt(timestamp);
      const waktu = parse / 1e4;
      const notificationTime = new Date(waktu);
      return notificationTime.toISOString();
    }
    const new_d = filteredArray.map((d) => {
      const _sort = d.timetoken ? formatTime(d.timetoken) : d.createdDate;
      let obj = {
        ...d,
        sortTime: _sort,
      };
      return obj;
    });
    function compareDates(a, b) {
      return new Date(b.sortTime) - new Date(a.sortTime);
    }

    new_d.sort(compareDates);*/}

    setClassMessage(filteredArray);
  }, [value, materialClassFilter]);


  return (
    <>
      <section className="shadow-xl  w-full flex items-center gap-x-3">
        <ListClass
          classMessage={classMessage}
          searchResults={searchResults}
          value={value}
          user={user}
          setValue={setValue}
          searchTerm={searchTerm}
          setSearchTerm={setSearchTerm}
          isLoading={isLoading}
        />
      </section>
    </>
  );
};

const ListClass = (props: any) => {
  const { searchResults, value, setValue, searchTerm, setSearchTerm, user, isLoading } = props;

  const router = useRouter();
  const locale: "en" | "id" = router.locale as "en" | "id";
  const lang = trans[locale].dashboard;

  const sessionToken = user?.accessToken;

  const queryClient = useQueryClient();

  const [modalClass, setModalClass] = useState(false);
  const [modalType, setModalType] = useState("");
  const [classId, setClassId] = useState(null);
  const [isLoadingModal, setIsloadingModal] = useState(false);
  const [_isLecturer, setIsLecturer] = useState(null);
  const [_tempClass, setTempClass] = useState(null);

  const isMD = useMediaQuery({
    query: "(max-width: 769px)",
  });

  const heightContainer = isMD
    ? { height: "calc(100vh - 270px)" }
    : { height: "calc(100vh - 220px)" };


  const getAllMaterial = async (classId: number) => {
    setModalType("All Material");
    setIsloadingModal(true);
    setModalClass(true);

    try {
      const response = await axios.get(
        process.env.API_BE + `/api/class/${classId}/material`,
        {
          headers: {
            Authorization: `Bearer ${sessionToken}`,
          },
        },
      );
      queryClient.setQueryData(["allMaterial"], response.data.data.items);
    } catch (error) {
      toast.error("Gagal mengambil data materi.", error);
    } finally {
      setIsloadingModal(false);
    }
  };

    const [viewportHeight, setViewportHeight] = useState(0);

    useEffect(() => {
        const adjustHeight = () => {
        const vh = window.innerHeight ;
        setViewportHeight(vh);
        };
        adjustHeight();

        window.addEventListener('resize', adjustHeight);
        return () => {
        window.removeEventListener('resize', adjustHeight);
        };
    }, []);

  return (
    <>
    {modalClass && (
      <ModalMateri
        lang={lang}
        user={user}
        classId={classId}
        isLecturer={_isLecturer}
        tempClass={_tempClass}
        modalType={modalType}
        setModalType={setModalType}
        modalClass={modalClass}
        setModalClass={setModalClass}
        isLoadingModal={isLoadingModal}
        setIsloadingModal={setIsloadingModal}
        getAllMaterial={getAllMaterial}
      />
    )}

      <div style={{ height: viewportHeight, overflow: 'auto' }} className="pt-[4.4rem] sm:pt-[5.2rem] pb-[3.8rem] lg:pb-0 pb-2.5 className flex flex-col w-full max-w-2xl mx-auto shadow-md px-3 sm:px-5">
        <TitleMenu
          lang={lang}
          setSearchTerm={setSearchTerm}
          searchTerm={searchTerm}
          title={` ${lang.materials} ${lang.class} `}
        />
        <div className="w-full">
          <SelectAndSearch value={value} setValue={setValue} lang={lang} />
          <div className="overflow-y-auto pr-2">
            {isLoading ? (
            <SkeltonList />
            ): searchResults?.length < 1 ? (
              <div className="h-[35vh] w-full text-center flex items-end justify-center">
                <div className="">{lang.dataNotFound}</div>
              </div>
            ) : (
              searchResults?.map((d: any, index: number) => {
                const isLecturer = d?.userId == user?.id; // isLecturer scope x  _isLecturer global for modal

                return (
                  <ListItem
                    key={index}
                    lang={lang}
                    channel={d.channel}
                    name={d.name}
                    lecturerName={d.username}
                    profilePictureLink={d.profilePictureLink}
                    timetoken={d.timetoken}
                    created={d.createdDate}
                    isLecturer={isLecturer}
                    isAcademic={d.isAcademic}
                    onClick={() => {
                      getAllMaterial(d.classId);
                      setClassId(d.classId);
                      setIsLecturer(isLecturer);
                      setTempClass(d);
                    }}
                  />
                );
              })
            )}
          </div>
        </div>
      </div>
    </>
  );
};

const TitleMenu = ({ title, lang, searchTerm, setSearchTerm }) => {
  const [focus, setFocus] = useState(false);
  const focusSearch = () => {
    const searchInput = document.getElementById("searchInput");
    if (searchInput) {
      searchInput.focus();
    }
  };
  return (
    <div className="relative mb-5 flex items-center justify-between pt-3 w-full h-[40px]">
      <Link href="/dashboard" className="flex items-center flex-none">
        <div className="flex-none bg-[#9D21E6] flex justify-center items-center rounded-full w-8 h-8 lg:w-10 lg:h-10 p-1">
          <svg
            stroke="currentColor"
            fill="currentColor"
            strokeWidth={0}
            viewBox="0 0 1024 1024"
            className="text-white "
            height={20}
            width={20}
            xmlns="http://www.w3.org/2000/svg"
          >
            <path d="M872 474H286.9l350.2-304c5.6-4.9 2.2-14-5.2-14h-88.5c-3.9 0-7.6 1.4-10.5 3.9L155 487.8a31.96 31.96 0 0 0 0 48.3L535.1 866c1.5 1.3 3.3 2 5.2 2h91.5c7.4 0 10.8-9.2 5.2-14L286.9 550H872c4.4 0 8-3.6 8-8v-60c0-4.4-3.6-8-8-8z" />
          </svg>
        </div>
        <h1 className="ml-2 lg:ml-4 text-lg sm:text-xl md:text-2xl font-bold text-[#9D21E6] self-center ">
          {title}
        </h1>
      </Link>
      <div
        className={` ${
          focus ? "w-full absolute" : "relative"
        } duration-500 z-10 text-gray-600 `}
      >
        <input
          className="sm:w-[135px] w-[0px] peer focus:w-full focus:duration-500  focus:border  focus:shadow-md border-gray-200 bg-white h-11 focus:pl-10 pr-4 rounded-lg text-sm focus:outline-none"
          type="search"
          name="search"
          id="searchInput"
          onFocus={() => setFocus(true)}
          onBlur={() => setFocus(false)}
          autoComplete="off"
          value={searchTerm}
          onChange={(e) => setSearchTerm(e.target.value)}
          placeholder={lang.search}
        />
        <button
          onClick={focusSearch}
          className="absolute  peer-focus:duration-500 -left-6 peer-focus:left-3 top-0.5 mt-3"
        >
          <svg
            className="text-gray-600 h-4 w-4 fill-current"
            xmlns="http://www.w3.org/2000/svg"
            xmlnsXlink="http://www.w3.org/1999/xlink"
            version="1.1"
            id="Capa_1"
            x="0px"
            y="0px"
            viewBox="0 0 56.966 56.966"
            style={{ enableBackground: "new 0 0 56.966 56.966" }}
            xmlSpace="preserve"
            width="512px"
            height="512px"
          >
            <path d="M55.146,51.887L41.588,37.786c3.486-4.144,5.396-9.358,5.396-14.786c0-12.682-10.318-23-23-23s-23,10.318-23,23  s10.318,23,23,23c4.761,0,9.298-1.436,13.177-4.162l13.661,14.208c0.571,0.593,1.339,0.92,2.162,0.92  c0.779,0,1.518-0.297,2.079-0.837C56.255,54.982,56.293,53.08,55.146,51.887z M23.984,6c9.374,0,17,7.626,17,17s-7.626,17-17,17  s-17-7.626-17-17S14.61,6,23.984,6z" />
          </svg>
        </button>
      </div>
      {/*      <div className="hidden hover:bg-[#CFB6FF] relative flex justify-center cursor-pointer items-center rounded-full w-6 h-6 lg:w-10 lg:h-10">
        <svg
          stroke="currentColor"
          fill="currentColor"
          strokeWidth={0}
          viewBox="0 0 16 16"
          className="text-[#9D21E6] cursor-pointer w-5 h-5"
          xmlns="http://www.w3.org/2000/svg"
        >
          <path d="M9.5 13a1.5 1.5 0 1 1-3 0 1.5 1.5 0 0 1 3 0zm0-5a1.5 1.5 0 1 1-3 0 1.5 1.5 0 0 1 3 0zm0-5a1.5 1.5 0 1 1-3 0 1.5 1.5 0 0 1 3 0z" />
        </svg>
      </div>*/}
    </div>
  );
};

const SelectAndSearch = ({ lang, value, setValue }: any) => {
  return (
    <div className="flex items-center mb-2.5 mt-1.5 gap-x-3">
      <Tabs
        value={value}
        onValueChange={setValue}
        defaultValue="nonAcademic"
        className="w-full"
      >
        <TabsList className="grid grid-cols-2 mb-2.5">
          <TabsTrigger value="nonAcademic">{lang.public}</TabsTrigger>
          <TabsTrigger value="academic">{lang.academic}</TabsTrigger>
        </TabsList>
      </Tabs>
      {/*<Select value={value} onValueChange={setValue}>
        <SelectTrigger className="shadow-md rounded-lg">
          <SelectValue placeholder={lang.class} />
        </SelectTrigger>
        <SelectContent>
          <SelectItem value="academic">{lang.academic}</SelectItem>
          <SelectItem value="umum">{lang.public}</SelectItem>
          <SelectItem value="semua">{lang.all}</SelectItem>
        </SelectContent>
      </Select>*/}
    </div>
  );
};

const ListItem = ({
  name,
  onClick,
  lecturerName,
  profilePictureLink,
  isLecturer,
  timetoken,
  lang,
  created,
  isAcademic,
}: any) => {
  return (
    <div
      onClick={onClick}
      className="relative  overflow-hidden  bg-white w-full flex items-center px-2.5 py-1.5 hover:bg-[#d7b9fd]/50 duration-300 shadow-md border-b border-gray-300 rounded-xl-purple-200 cursor-pointer rounded-lg mb-1.5  border"
    >
      <div className="flex items-center space-x-4">
        <div className="flex-none relative w-14 h-14">
          <Image
            width={60}
            height={60}
            src={profilePictureLink || `/static/images/logokedi.svg`}
            className="absolute inset-0 h-full w-full object-cover rounded-full shadow-md"
            alt=""
          />
        </div>
      </div>
      <div className="flex-grow py-3 pl-3 pr-1 flex-1">
        <div className="flex items-center justify-between">
          <div className="space-y-2">
            {name && (
              <div className="line-clamp-1 font-semibold text-gray-700 text-sm">
                <span>{name}</span>
              </div>
            )}

            {lecturerName && (
              <div className="flex -mt-1 justify-between items-center">
                <div className="font-medium text-gray-600 text-sm">
                  {lecturerName}
                </div>
                <div className="flex items-center gap-x-2.5">
                  <span className=" hidden inline-flex items-center justify-center rounded-full bg-purple-100 px-2.5 py-0.5 text-purple-700">
                    <svg
                      xmlns="http://www.w3.org/2000/svg"
                      fill="none"
                      viewBox="0 0 24 24"
                      strokeWidth={1.5}
                      stroke="currentColor"
                      className="-ms-1 me-1.5 h-4 w-4"
                    >
                      <path
                        strokeLinecap="round"
                        strokeLinejoin="round"
                        d="M12 6.042A8.967 8.967 0 0 0 6 3.75c-1.052 0-2.062.18-3 .512v14.25A8.987 8.987 0 0 1 6 18c2.305 0 4.408.867 6 2.292m0-14.25a8.966 8.966 0 0 1 6-2.292c1.052 0 2.062.18 3 .512v14.25A8.987 8.987 0 0 0 18 18a8.967 8.967 0 0 0-6 2.292m0-14.25v14.25"
                      />
                    </svg>

                    <p className="whitespace-nowrap text-sm">Gratis</p>
                  </span>
                </div>
              </div>
            )}
          </div>
          <div className="flex-none flex flex-col items-end gap-y-1">
            <div className=" ml-1 mt-1 flex items-center gap-x-2  text-xs [@media(max-width:320px)]:hidden">

              {isLecturer && (
                <svg xmlns="http://www.w3.org/2000/svg" className="text-purple-700" width="2em" height="2em" viewBox="0 0 24 24"><path fill="currentColor" d="M5.85 17.1q1.275-.975 2.85-1.537T12 15t3.3.563t2.85 1.537q.875-1.025 1.363-2.325T20 12q0-3.325-2.337-5.663T12 4T6.337 6.338T4 12q0 1.475.488 2.775T5.85 17.1M12 13q-1.475 0-2.488-1.012T8.5 9.5t1.013-2.488T12 6t2.488 1.013T15.5 9.5t-1.012 2.488T12 13m0 9q-2.075 0-3.9-.788t-3.175-2.137T2.788 15.9T2 12t.788-3.9t2.137-3.175T8.1 2.788T12 2t3.9.788t3.175 2.137T21.213 8.1T22 12t-.788 3.9t-2.137 3.175t-3.175 2.138T12 22m0-2q1.325 0 2.5-.387t2.15-1.113q-.975-.725-2.15-1.112T12 17t-2.5.388T7.35 18.5q.975.725 2.15 1.113T12 20m0-9q.65 0 1.075-.425T13.5 9.5t-.425-1.075T12 8t-1.075.425T10.5 9.5t.425 1.075T12 11m0 7.5"></path></svg>
              )}
{/*              <span className="hidden">
                <span className="sm:inline hidden"> {lang.lastActivity} </span>{" "}
                {timetoken
                  ? formatTimeHourOrDate(timetoken)
                  : new Date(created).toLocaleDateString("id-ID", {
                      month: "short",
                      day: "2-digit",
                    })}
              </span>


            {isAcademic ? (
              <span className="hidden sm:inline-flex items-center justify-center rounded-md bg-amber-100 px-2.5 py-0.5 text-amber-700">
                <svg
                  xmlns="http://www.w3.org/2000/svg"
                  fill="none"
                  viewBox="0 0 24 24"
                  strokeWidth={1.5}
                  stroke="currentColor"
                  className="-ms-1 me-1.5 h-4 w-4"
                >
                  <path
                    strokeLinecap="round"
                    strokeLinejoin="round"
                    d="M4.26 10.147a60.438 60.438 0 0 0-.491 6.347A48.62 48.62 0 0 1 12 20.904a48.62 48.62 0 0 1 8.232-4.41 60.46 60.46 0 0 0-.491-6.347m-15.482 0a50.636 50.636 0 0 0-2.658-.813A59.906 59.906 0 0 1 12 3.493a59.903 59.903 0 0 1 10.399 5.84c-.896.248-1.783.52-2.658.814m-15.482 0A50.717 50.717 0 0 1 12 13.489a50.702 50.702 0 0 1 7.74-3.342M6.75 15a.75.75 0 1 0 0-1.5.75.75 0 0 0 0 1.5Zm0 0v-3.675A55.378 55.378 0 0 1 12 8.443m-7.007 11.55A5.981 5.981 0 0 0 6.75 15.75v-1.5"
                  />
                </svg>

                <p className="whitespace-nowrap text-[13px]">{lang.academic}</p>
              </span>
            ) : (
              <span className="hidden sm:inline-flex items-center justify-center rounded-md bg-emerald-100 px-2.5 py-0.5 text-emerald-700">
                <svg
                  xmlns="http://www.w3.org/2000/svg"
                  fill="none"
                  viewBox="0 0 24 24"
                  strokeWidth={1.5}
                  stroke="currentColor"
                  className="-ms-1 me-1.5 h-4 w-4"
                >
                  <path
                    strokeLinecap="round"
                    strokeLinejoin="round"
                    d="M12 6.042A8.967 8.967 0 0 0 6 3.75c-1.052 0-2.062.18-3 .512v14.25A8.987 8.987 0 0 1 6 18c2.305 0 4.408.867 6 2.292m0-14.25a8.966 8.966 0 0 1 6-2.292c1.052 0 2.062.18 3 .512v14.25A8.987 8.987 0 0 0 18 18a8.967 8.967 0 0 0-6 2.292m0-14.25v14.25"
                  />
                </svg>

                <p className="whitespace-nowrap text-[13px]">{lang.public}</p>
              </span>
            )}*/}
            </div>
          </div>
        </div>
      </div>
    </div>
  );
};

const ModalMateri = (props: any) => {
  const {
    lang,
    isLecturer,
    user,
    tempClass,
    classId,
    modalClass,
    setModalClass,
    modalType,
    setModalType,
    isLoadingModal,
    setIsloadingModal,
    getAllMaterial,
  } = props;

  const sessionToken = user?.accessToken;

  const queryClient = useQueryClient();

  const [delmode, setdelmode] = useState("");
  const [edit, setEdit] = useState("");
  const [tempDocId, setTempDocId] = useState([]);

  const [fileClass, setFileClass] = React.useState<any>([]);
  const [open, setOpen] = useState(false);

  const [createMaterial, setCreateMaterial] = useState<any>({
    classId: classId,
    title: "",
    description: "",
    documentIdList: [],
  });

  const { data: allMaterial } = useQuery<any>(["allMaterial"], {
    enabled: false,
  });

  const { data: material } = useQuery<any>(["material"], {
    enabled: false,
  });

  type PostMaterial = {
    classId: number;
    title: string;
    description: string;
    documentIdList: [];
  };

  const postMaterial = useMutation(
    async (data: PostMaterial): Promise<PostResponse> => {
      const { classId, title, description, documentIdList } = data;
      if (!sessionToken) throw new Error("Missing session token");
      setIsloadingModal(true);
      try {
        const url =
          edit !== ""
            ? `${process.env.API_BE}/api/class-material/${edit}`
            : delmode !== ""
              ? `${process.env.API_BE}/api/class-material/${delmode}`
              : `${process.env.API_BE}/api/class-material`;
        const uploading = await handleUploadFiles();
        const response = await axios({
          method: edit !== "" ? "put" : delmode !== "" ? "delete" : "post",
          url: url,
          data: {
            classId: Number(classId),
            title: String(title),
            description: String(description),
            documentIdList:
              documentIdList.length !== 0 && uploading
                ? [...documentIdList, ...uploading]
                : uploading,
          },
          headers: {
            Authorization: `Bearer ${sessionToken}`,
          },
        });
        if (edit !== "") {
          toast.success("Berhasil Update Materi");
        } else if (delmode !== "") {
          toast.success("Berhasil Delete Materi");
        } else {
          toast.success("Berhasil Upload Materi");
        }

        return response.data;
      } catch (error) {
        console.error(error);
        setIsloadingModal(false);
        throw error;
      } finally {
        getAllMaterial(classId);
        setIsloadingModal(false);
        setFileClass([]);
        setCreateMaterial({
          classId: classId,
          title: "",
          description: "",
        });
      }
    },
  );

  const getMaterial = async (materialId: number) => {
    setIsloadingModal(true);
    setModalType("Material");
    setModalClass(true);

    try {
      const response = await axios.get(
        process.env.API_BE + `/api/class-material/${materialId}`,
        {
          headers: {
            Authorization: `Bearer ${sessionToken}`,
          },
        },
      );
      queryClient.setQueryData(["material"], response.data.data);
    } catch (error) {
      toast.error("Gagal mengambil data materi.", error);
    } finally {
      setIsloadingModal(false);
    }
  };

  const uploadFileClass = (file: any, sessionToken: string) => {
    return new Promise((resolve, reject) => {
      const formData = new FormData();
      formData.append("file", file);
      formData.append("documentTypeId", "1");
      formData.append("isPrivate", "true");

      axios
        .post(process.env.API_BE + "/api/storage", formData, {
          headers: {
            "Content-Type": "multipart/form-data",
            Authorization: `Bearer ${sessionToken}`,
          },
        })
        .then((res) => {
          resolve(res.data.documentStorage.id);
        })
        .catch((err) => {
          reject(err.response.data.meta.message);
        });
    });
  };

  const handleUploadFiles = async () => {
    const uploadPromises = fileClass.map((file: any) =>
      uploadFileClass(file, sessionToken ?? ""),
    );
    try {
      const responses = await Promise.all(uploadPromises);
      if (fileClass.length > 0) {
        toast.success("Semua file berhasil diunggah");
        console.log("Responses:", responses);
      }
      return responses;
    } catch (error) {
      toast.error("Terjadi kesalahan saat mengunggah file");
      console.error("Error uploading files:", error);
    }
  };

  const handleFileChangeClass = (event: ChangeEvent<HTMLInputElement>) => {
    const file = event.target.files ?? null;
    if (file) {
      const filesArray = Array.from(file);
      setFileClass((prevFiles: any) => [...prevFiles, ...filesArray]);
    }
  };

  const fileInputRef = useRef<HTMLInputElement>(null);
  const inputFileRef = useRef(null);

  const handleButtonClick = () => {
    if (fileInputRef.current) {
      fileInputRef.current.click();
    }
  };

  const handleDragOver = (event: any) => {
    event.preventDefault();
  };

  const handleDragEnter = (event: any) => {
    event.preventDefault();
  };

  const handleDragLeave = (event: any) => {
    event.preventDefault();
  };

  const handleDropClass = (event: any) => {
    event.preventDefault();

    const file = event.dataTransfer.files ?? null;
    if (file) {
      const filesArray = Array.from(file);
      setFileClass((prevFiles: any) => [...prevFiles, ...filesArray]);
    }
  };

  const sortedMaterials = allMaterial?.slice().sort((a, b) => {
    const dateA = new Date(a.updatedAt);
    const dateB = new Date(b.updatedAt);

    return dateB - dateA;
  });

  return (
    <Dialog
      open={modalClass}
      onOpenChange={() => {
        if (modalType === "Edit Materi") {
          setModalType("Material");
        } else if (modalType === "Material") {
          setModalType("All Material");
        } else {
          setModalClass((prevState) => !prevState);
        }
      }}
      onOpenAutoFocus={(e) => e.preventDefault()}
    >
        {isLoadingModal ? (
          <div className="absolute flex items-center justify-center w-full h-full bg-white/20 backdrop-blur-sm z-10">
            <LoadingSpinner size={70} strokeWidth={2} />
          </div>
        ) : (
        
            <DialogContent className={`p-4 sm:p-6 max-w-xl min-h-[200px]`}>
                <DialogHeader>
                <DialogTitle className="mb-2 text-xl text-center">
                    {modalType === "All Material" && lang.materials}
                    {modalType === "Material" && lang.materialDetail}
                    {modalType === "Edit Material" && lang.materialEdit}
                </DialogTitle>
                {false && (
                    <div className="border border-gray-100 py-3 w-full max-w-[95%] mx-auto flex flex-col items-center rounded-lg justify-center bg-purple-50 px-2.5 py-0.5 text-sm text-purple-700">
                    <p className="font-semibold text-normal line-clamp-1">
                        {tempClass?.name}
                    </p>
                    <p className=" line-clamp-1">
                        {tempClass?.description} <span className="mx-1.5">|</span>{" "}
                        {formatToTime(
                        allMaterial && allMaterial[0]?.class?.startAttendanceTime,
                        )}{" "}
                        -{" "}
                        {formatToTime(
                        allMaterial && allMaterial[0]?.class?.endAttendanceTime,
                        )}
                    </p>
                    </div>
                )}
                </DialogHeader>

                {modalType === "Edit Materi" && (
                <div className="">
                    <div className="mb-3">
                    <label
                        htmlFor="title"
                        className="block  font-medium text-gray-700"
                    >
                        {lang.materialTitle}
                    </label>

                    <input
                        type="text"
                        autoFocus
                        id="title"
                        placeholder="Judul Materi"
                        onChange={(e) =>
                        setCreateMaterial({
                            ...createMaterial,
                            title: e.target.value,
                        })
                        }
                        value={createMaterial?.title}
                        className="px-3 py-2 h-11 text-sm mt-1 w-full rounded-[10px] border border-gray-400  outline-none  shadow-sm focus:ring-2 ring-[#9d21e6]"
                    />
                    </div>
                    <div className="">
                    <label
                        htmlFor="komentarNilaiKuis"
                        className="block  font-medium text-gray-700"
                    >
                        {lang.description}
                    </label>
                    <textarea
                        id="description"
                        className="w-full resize-none px-3 py-2 text-sm mt-1 rounded-[10px] border border-gray-400  outline-none  shadow-sm focus:ring-2 ring-[#9d21e6]"
                        rows={4}
                        autoFocus
                        onChange={(e) =>
                        setCreateMaterial({
                            ...createMaterial,
                            description: e.target.value,
                        })
                        }
                        value={createMaterial?.description}
                        placeholder="Isi Deskripsi terkait materi"
                    ></textarea>
                    </div>
                    <FileInput
                    handleDragOver={handleDragOver}
                    handleDragEnter={handleDragEnter}
                    handleDragLeave={handleDragLeave}
                    handleDropClass={handleDropClass}
                    handleButtonClick={handleButtonClick}
                    inputFileRef={inputFileRef}
                    handleFileChangeClass={handleFileChangeClass}
                    fileClass={fileClass}
                    setFileClass={setFileClass}
                    tempDocId={tempDocId}
                    setTempDocId={setTempDocId}
                    setCreateMaterial={setCreateMaterial}
                    title={lang.materials}
                    />
                    <div className="w-fit mx-auto mt-4">
                    <button
                        onClick={() => postMaterial.mutate(createMaterial)}
                        className="px-8  py-2.5   bg-[#9d21e6] text-white shadow-xl rounded-[10px] font-semibold"
                        type="button"
                    >
                        {lang.materialSave}
                    </button>
                    </div>
                </div>
                )}

                {modalType === "All Material" && sortedMaterials && (
                <div className="flex-1 overflow-y-auto max-h-[80vh] pr-1.5">
                    {allMaterial?.length === 0 ? (
                    <div className="flex items-center justify-center h-full">
                        {lang.dataNotFound}
                    </div>
                    ) : (
                    <>
                        <div className="w-full mb-4">
                        <div className="mt-4">
                            <ul className="space-y-1 w-full ">
                            <li>
                                <details className="group [&_summary::-webkit-details-marker]:hidden p-3 border shadow-sm rounded-lg">
                                <summary
                                    className={`flex  group-open:border-gray-400 cursor-pointer items-center justify-between group-open:p-3`}
                                >
                                    <div className="flex items-center gap-x-5">
                                    <div className="flex-none relative w-14 h-14">
                                        <Image
                                        width={60}
                                        height={60}
                                        src={
                                            sortedMaterials[0]?.class?.lecturer
                                            ?.profilePicture.url ||
                                            `/static/images/logokedi.svg`
                                        }
                                        className="absolute inset-0 h-full w-full object-cover rounded-xl shadow-md"
                                        alt=""
                                        />
                                    </div>
                                    <div className="flex flex-col">
                                        <div className="text-[14px] line-clamp-1">
                                        {sortedMaterials[0]?.lecturer?.fullName}
                                        </div>
                                        <div className="text-[14px] font-semibold line-clamp-1">
                                        {sortedMaterials[0]?.class?.name}
                                        </div>
                                        <div className="text-[14px] opacity-70 line-clamp-1">
                                        {sortedMaterials[0]?.class?.description}{" "}
                                        <span className="mx-1.5">|</span>
                                        {formatToTime(
                                            sortedMaterials &&
                                            sortedMaterials[0]?.class
                                                ?.startAttendanceTime,
                                        )}{" "}
                                        -{" "}
                                        {formatToTime(
                                            sortedMaterials &&
                                            sortedMaterials[0]?.class
                                                ?.endAttendanceTime,
                                        )}
                                        </div>
                                    </div>
                                    </div>
                                    <span className="shrink-0 transition duration-300 group-open:-rotate-180">
                                    <svg
                                        xmlns="http://www.w3.org/2000/svg"
                                        className="h-5 w-5"
                                        viewBox="0 0 20 20"
                                        fill="currentColor"
                                    >
                                        <path
                                        fillRule="evenodd"
                                        d="M5.293 7.293a1 1 0 011.414 0L10 10.586l3.293-3.293a1 1 0 111.414 1.414l-4 4a1 1 0 01-1.414 0l-4-4a1 1 0 010-1.414z"
                                        clipRule="evenodd"
                                        />
                                    </svg>
                                    </span>
                                </summary>
                                <ul className="space-y-3 p-3">
                                    {sortedMaterials?.map((x: any) => {
                                    return (
                                        <div
                                        onClick={() => getMaterial(x.id)}
                                        key={x.id}
                                        >
                                        <div className="w-full my-2  h-fit  border border-gray-300 py-2.5 px-4 bg-white rounded-xl">
                                            <div className="font-bold">{x.title}</div>
                                            <div className="text-gray-700 text-sm">
                                            {x.description}
                                            </div>

                                            <h1 className="text-black font-bold my-3"></h1>
                                            {x.references.map(
                                            (reference: any) => (
                                                <div
                                                key={reference.id}
                                                className="flex items-center text-black gap-x-2"
                                                >
                                                <svg
                                                    xmlns="http://www.w3.org/2000/svg"
                                                    fill="none"
                                                    viewBox="0 0 24 24"
                                                    stroke="currentColor"
                                                    className="h-5 w-5 "
                                                >
                                                    <path
                                                    strokeLinecap="round"
                                                    strokeLinejoin="round"
                                                    strokeWidth={2}
                                                    d="M15.172 7l-6.586 6.586a2 2 0 102.828 2.828l6.414-6.586a4 4 0 00-5.656-5.656l-6.415 6.585a6 6 0 108.486 8.486L20.5 13"
                                                    />
                                                </svg>
                                                <Link
                                                    href={reference.url}
                                                    target="_blank"
                                                    className="text-[#9d21e6] font-medium text-sm"
                                                >
                                                    {reference?.originalName}
                                                </Link>
                                                </div>
                                            ),
                                            )}

                                            <div className="flex items-center gap-2 w-full mt-1.5">
                                            {x.createdDate !== x.updatedAt ? (
                                                <div className="w-full flex items-center justify-between gap-2 text-gray-600 mt-2 text-[0.775rem]">
                                                <p>
                                                    {formatTanggal(x.updatedAt)}
                                                </p>
                                                <p className="inline-flex items-center justify-center rounded-full bg-emerald-100 px-2.5 py-0.5 text-emerald-700">
                                                    <span className="whitespace-nowrap">
                                                    Updated
                                                    </span>
                                                </p>
                                                </div>
                                            ) : (
                                                <p className="text-gray-600 mt-2 text-[0.775rem]">
                                                {formatTanggal(x.createdDate)}
                                                </p>
                                            )}
                                            </div>
                                        </div>
                                        </div>
                                    );
                                    })}
                                </ul>
                                </details>
                            </li>
                            </ul>
                            {sortedMaterials?.length <= 0 && (
                            <div className="w-full h-[10vh]  flex flex-col justify-center items-center">
                                <h1 className="text-2xl">{lang.dataNotFound}</h1>
                            </div>
                            )}
                        </div>
                        </div>
                    </>
                    )}{" "}
                </div>
                )}

                {modalType === "Material" && material && (
                <>
                    <div className="flex justify-between">
                    <div>
                        <p className="mb-2.5 font-bold text-lg text-neutral-90">
                        {material?.title}
                        </p>
                        <p className="mb-2 -mt-3 text-base text-neutral-70">
                        {material?.description}
                        </p>
                    </div>

                    {material?.lecturer?.uuid === user.uuid && (
                        <>
                        <Popover open={open} onOpenChange={setOpen}>
                            <PopoverTrigger></PopoverTrigger>
                            <PopoverContent
                            align="center"
                            className="p-0 w-[500px] -translate-y-28 -translate-x-12"
                            >
                            <div className="relative">
                                {/* Modal header */}
                                <div className="rounded-lg bg-white py-5 shadow">
                                {/* Modal header */}
                                <div className="mb-5 flex items-center justify-center gap-2 rounded-t px-6 pb-2 pt-6 dark:border-gray-600">
                                    <div className="text-red-600">
                                    <svg
                                        xmlns="http://www.w3.org/2000/svg"
                                        fill="none"
                                        viewBox="0 0 24 24"
                                        strokeWidth="1.5"
                                        stroke="currentColor"
                                        className="h-16 w-16"
                                    >
                                        <path
                                        strokeLinecap="round"
                                        strokeLinejoin="round"
                                        d="M14.74 9l-.346 9m-4.788 0L9.26 9m9.968-3.21c.342.052.682.107 1.022.166m-1.022-.165L18.16 19.673a2.25 2.25 0 01-2.244 2.077H8.084a2.25 2.25 0 01-2.244-2.077L4.772 5.79m14.456 0a48.108 48.108 0 00-3.478-.397m-12 .562c.34-.059.68-.114 1.022-.165m0 0a48.11 48.11 0 013.478-.397m7.5 0v-.916c0-1.18-.91-2.164-2.09-2.201a51.964 51.964 0 00-3.32 0c-1.18.037-2.09 1.022-2.09 2.201v.916m7.5 0a48.667 48.667 0 00-7.5 0"
                                        />
                                    </svg>
                                    </div>
                                    <p id="confirmMessage" className="font-medium">
                                    {lang.promptDelete}
                                    </p>
                                </div>
                                {/* Modal footer */}
                                <div className="mx-auto flex w-fit gap-x-6">
                                    <button
                                    onClick={() => setOpen(false)}
                                    id="confirmNo"
                                    type="button"
                                    value="No"
                                    className=" rounded-[10px] border border-[#265881] px-4 py-2 text-sm font-medium text-[#265881] hover:bg-slate-200"
                                    >
                                    {lang.cancel}
                                    </button>
                                    <button
                                    onClick={() =>
                                        postMaterial.mutate(createMaterial)
                                    }
                                    id="confirmYes"
                                    type="button"
                                    value="Yes"
                                    className="rounded-[10px] bg-red-600 px-4 py-2 text-sm font-medium text-white hover:bg-[#F15E5E]"
                                    >
                                    {lang.submit}
                                    </button>
                                </div>
                                </div>
                            </div>
                            </PopoverContent>
                        </Popover>
                        <div className="flex items-center gap-x-2.5 mb-2">
                            <button
                            onClick={() => {
                                setModalType("Edit Materi");
                                setCreateMaterial({
                                classId: classId,
                                title: material?.title,
                                description: material?.description,
                                documentIdList: material?.references.map(
                                    (i: any) => i.id,
                                ),
                                });
                                setEdit(material?.id);
                                setTempDocId(material?.references);
                            }}
                            type="button"
                            className="w-fit editOfVessel rounded-lg  border-blue-600 text-blue-600 p-1 duration-300 hover:bg-blue-600 hover:text-white duration-300"
                            >
                            <svg
                                xmlns="http://www.w3.org/2000/svg"
                                fill="none"
                                viewBox="0 0 24 24"
                                strokeWidth="1.5"
                                stroke="currentColor"
                                className="w-6 h-6"
                            >
                                <path
                                strokeLinecap="round"
                                strokeLinejoin="round"
                                d="M16.862 4.487l1.687-1.688a1.875 1.875 0 112.652 2.652L10.582 16.07a4.5 4.5 0 01-1.897 1.13L6 18l.8-2.685a4.5 4.5 0 011.13-1.897l8.932-8.931zm0 0L19.5 7.125M18 14v4.75A2.25 2.25 0 0115.75 21H5.25A2.25 2.25 0 013 18.75V8.25A2.25 2.25 0 015.25 6H10"
                                />
                            </svg>
                            </button>
                            <button
                            type="button"
                            onClick={() => {
                                setdelmode(material?.id);
                                setOpen(true);
                            }}
                            className="w-fit deleteOfVessel rounded-lg  border-[#F62121] text-[#F62121] p-1 duration-300 hover:bg-[#F62121] hover:text-white"
                            >
                            <svg
                                xmlns="http://www.w3.org/2000/svg"
                                fill="none"
                                viewBox="0 0 24 24"
                                strokeWidth="1.5"
                                stroke="currentColor"
                                className="w-6 h-6"
                            >
                                <path
                                strokeLinecap="round"
                                strokeLinejoin="round"
                                d="M14.74 9l-.346 9m-4.788 0L9.26 9m9.968-3.21c.342.052.682.107 1.022.166m-1.022-.165L18.16 19.673a2.25 2.25 0 01-2.244 2.077H8.084a2.25 2.25 0 01-2.244-2.077L4.772 5.79m14.456 0a48.108 48.108 0 00-3.478-.397m-12 .562c.34-.059.68-.114 1.022-.165m0 0a48.11 48.11 0 013.478-.397m7.5 0v-.916c0-1.18-.91-2.164-2.09-2.201a51.964 51.964 0 00-3.32 0c-1.18.037-2.09 1.022-2.09 2.201v.916m7.5 0a48.667 48.667 0 00-7.5 0"
                                />
                            </svg>
                            </button>
                        </div>
                        </>
                    )}
                    </div>
                    {material?.references.map((reference: any) => (
                    <div
                        key={reference.id}
                        className="flex items-center text-black gap-x-2"
                    >
                        <svg
                        xmlns="http://www.w3.org/2000/svg"
                        fill="none"
                        viewBox="0 0 24 24"
                        stroke="currentColor"
                        className="h-6 w-6 "
                        >
                        <path
                            strokeLinecap="round"
                            strokeLinejoin="round"
                            strokeWidth={2}
                            d="M15.172 7l-6.586 6.586a2 2 0 102.828 2.828l6.414-6.586a4 4 0 00-5.656-5.656l-6.415 6.585a6 6 0 108.486 8.486L20.5 13"
                        />
                        </svg>
                        <Link
                        href={reference.url}
                        target="_blank"
                        className="text-[#9d21e6]"
                        >
                        {reference?.originalName}
                        </Link>
                    </div>
                    ))}
                </>
                )}
            </DialogContent>
        )}
    </Dialog>
  );
};

export async function getServerSideProps(context: any) {
  try {
    const session = await getSession(context);
    if (session) {
      return {
        props: { user: session.user },
      };
    } else {
      return {
        redirect: {
          destination: `/login`,
          permanent: false,
        },
      };
    }
  } catch (error) {
    console.error("Kesalahan:", error);
    return {
      props: {
        error: "Terjadi kesalahan saat memuat sesi pengguna",
      },
    };
  }
}
