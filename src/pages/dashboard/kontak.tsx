import React, { useState, useEffect } from "react";
import Link from "next/link";
import { Navbar } from "@/components/navbar/navbar";
import { getSession } from "next-auth/react";
import { LoadingSpinner } from "@/components/ui/loading";
import { useQuery, useQueryClient, useMutation } from "@tanstack/react-query";
import Image from "next/image";
import axios from "axios";
import { ReusableAlertDialog } from "@/components/kelas/module/element/Alert";
import toast from "react-hot-toast";
import { useMediaQuery } from "react-responsive";
import {
  DropdownMenu,
  DropdownMenuContent,
  DropdownMenuItem,
  DropdownMenuLabel,
  DropdownMenuSeparator,
  DropdownMenuTrigger,
} from "@/components/ui/dropdown-menu";

import {
  Dialog,
  DialogContent,
  DialogHeader,
  DialogTitle,
} from "@/components/ui/dialog";
import { trans } from "@/lang";
import { useRouter } from "next/router";

const Home = ({ user }: any) => {
  const sessionToken = user?.accessToken;

  const { data, isLoading: isLoadingList } = useQuery(
    ["listContact"],
    async () => {
      const response = await axios.get(
        process.env.API_BE + "/api/friend?limit=10&page=1",
        {
          headers: {
            Authorization: `Bearer ${sessionToken}`,
          },
        },
      );
      if (response.status !== 200) {
        throw new Error("Gagal mengambil data");
      }
      return response.data.data.items;
    },
    {
      refetchOnWindowFocus: false,
    },
  );
  const { data: dataBlock } = useQuery(
    ["listContactBlocked"],
    async () => {
      const response = await axios.get(
        process.env.API_BE + "/api/friend/block?limit=10&page=1",
        {
          headers: {
            Authorization: `Bearer ${sessionToken}`,
          },
        },
      );
      if (response.status !== 200) {
        throw new Error("Gagal mengambil data");
      }
      return response.data.data.items;
    },
    {
      refetchOnWindowFocus: false,
    },
  );

  const filteredData = data?.filter((d) => d.id !== user.id);
  const filteredDataBlock = dataBlock?.filter((d) => d.id !== user.id);
  return (
    <>
      <Navbar user={user} />
      <BuatKelas
        user={user}
        filteredData={filteredData}
        filteredDataBlock={filteredDataBlock}
        isLoadingList={isLoadingList}
      />
    </>
  );
};
const BuatKelas = ({
  user,
  isLoadingList,
  filteredData,
  filteredDataBlock,
}: any) => {
  const queryClient = useQueryClient();
  const sessionToken = user?.accessToken;

  const router = useRouter();
  const locale: "en" | "id" = router.locale as "en" | "id";
  const lang = trans[locale].dashboard;

  const [modalMenuProfile, setModalMenuProfile] = useState(false);
  const [modalType, setModalType] = useState("");
  const [alertRemove, setAlertRemove] = useState(false);
  const [alertBlock, setAlertBlock] = useState(false);
  const [alertUnblock, setAlertUnblock] = useState(false);
  const [tempData, setTempData] = useState({
    id: 0,
    fullName: "",
  });

  const [searchTerm, setSearchTerm] = useState("");
  const [searchResults, setSearchResults] = useState<[]>(filteredData);

  const handleSearch = () => {
    const filteredMessages = filteredData?.filter(
      (user) =>
        user?.fullName?.toLowerCase().includes(searchTerm.toLowerCase()) ||
        user?.username?.toLowerCase().includes(searchTerm.toLowerCase()),
    );
    setSearchResults(filteredMessages);
  };

  interface ApiResponse {
    data: {
      message: string;
    };
  }
  interface PostResponse extends ApiResponse {}

  const handleBlock = useMutation(
    async (userId): Promise<PostResponse> => {
      if (!sessionToken) throw new Error("Missing session token");
      try {
        const response = await axios.post<PostResponse>(
          process.env.API_BE + `/api/friend/block`,
          { id: userId },
          {
            headers: {
              Authorization: `Bearer ${sessionToken}`,
            },
          },
        );
        toast.success(`Berhasil Bloked Friend ${tempData.fullName}`);
        setAlertBlock(false);

        setTempData({
          id: 0,
          fullName: "",
        });

        return response.data;
      } catch (error) {
        toast.error("Error Request");
        throw error;
      } finally {
        queryClient.invalidateQueries(["listContact"]);
        queryClient.invalidateQueries(["listContactBlocked"]);
      }
    },
    {
      onError: (error) =>
        toast.error(`Error: ${error.response.data.meta.message}`),
    },
  );

  const handleUnBlock = useMutation(
    async (userId): Promise<PostResponse> => {
      if (!sessionToken) throw new Error("Missing session token");
      try {
        const response = await axios.post<PostResponse>(
          process.env.API_BE + `/api/friend/unblock`,
          { id: userId },
          {
            headers: {
              Authorization: `Bearer ${sessionToken}`,
            },
          },
        );
        toast.success(`Berhasil Unblock Friend ${tempData.fullName}`);

        setAlertUnblock(false);

        setTempData({
          id: 0,
          fullName: "",
        });

        return response.data;
      } catch (error) {
        toast.error("Error Request");
        throw error;
      } finally {
        queryClient.invalidateQueries(["listContact"]);
        queryClient.invalidateQueries(["listContactBlocked"]);
      }
    },
    {
      onError: (error) =>
        toast.error(`Error: ${error.response.data.meta.message}`),
    },
  );

  const handleRemove = useMutation(
    async (userId): Promise<PostResponse> => {
      if (!sessionToken) throw new Error("Missing session token");
      try {
        const response = await axios.post<PostResponse>(
          process.env.API_BE + `/api/friend/remove`,
          { id: userId },
          {
            headers: {
              Authorization: `Bearer ${sessionToken}`,
            },
          },
        );
        toast.success(`Berhasil Remove Friend ${tempData.fullName}`);

        setAlertRemove(false);

        setTempData({
          id: 0,
          fullName: "",
        });

        return response.data;
      } catch (error) {
        toast.error("Error Request");
        throw error;
      } finally {
        queryClient.invalidateQueries(["listContact"]);
      }
    },
    {
      onError: (error) =>
        toast.error(`Error: ${error.response.data.meta.message}`),
    },
  );

  const titleBlock = `${lang.promptBlock} ${tempData.fullName}?`;
  const titleUnblock = `${lang.promptUnblock} ${tempData.fullName}?`;
  const titleRemove = `${lang.promptUnfriend} ${tempData.fullName}?`;

  useEffect(() => {
    handleSearch();
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [searchTerm]);

  useEffect(() => {
    setSearchResults(filteredData);
  }, [filteredData]);

  const isMD = useMediaQuery({
    query: "(max-width: 769px)",
  });

  const heightContainer = isMD
    ? { height: "calc(100vh - 210px)" }
    : { height: "calc(100vh - 160px)" };

    const [viewportHeight, setViewportHeight] = useState(0);

    useEffect(() => {
        const adjustHeight = () => {
        const vh = window.innerHeight ;
        setViewportHeight(vh);
        };
        adjustHeight();

        window.addEventListener('resize', adjustHeight);
        return () => {
        window.removeEventListener('resize', adjustHeight);
        };
    }, []);

  return (
    <>
    {alertBlock && (
      <ReusableAlertDialog
        open={alertBlock}
        onOpenChange={setAlertBlock}
        title={titleBlock}
        description={lang.promptDesc}
        textDelete={lang.btnBlock}
        onDelete={() => handleBlock.mutate(tempData.id)}
        isLoading={handleBlock?.isLoading}
      />
    )}
    {alertRemove && (
      <ReusableAlertDialog
        open={alertRemove}
        onOpenChange={setAlertRemove}
        title={titleRemove}
        description={lang.promptDesc}
        textDelete={lang.btnUnfriend}
        onDelete={() => handleRemove.mutate(tempData.id)}
        isLoading={handleRemove?.isLoading}
      />
    )}
    {alertUnblock && (
      <ReusableAlertDialog
        open={alertUnblock}
        onOpenChange={setAlertUnblock}
        title={titleUnblock}
        description={lang.promptDesc}
        textDelete={lang.btnUnblock}
        onDelete={() => handleUnBlock.mutate(tempData.id)}
        isLoading={handleUnBlock?.isLoading}
      />
    )}

      <div style={{ height: viewportHeight, overflow: 'auto' }} className="relative pt-[4.4rem] sm:pt-[5.2rem] pb-[3.8rem] lg:pb-0 pb-2.5 className flex flex-col w-full max-w-2xl mx-auto shadow-md px-3 sm:px-5">
        {isLoadingList && (
          <div style={{ height: viewportHeight, overflow: '' }} className="absolute inset-0 flex items-center justify-center bg-white/30 backdrop-blur-sm z-10">
            <LoadingSpinner size={70} strokeWidth={2} />
          </div>
        )}
        <div className=" pr-2 ">
          <TitleMenu
            title={` ${lang.contacts}`}
            lang={lang}
            setModalMenuProfile={setModalMenuProfile}
            setModalType={setModalType}
            searchTerm={searchTerm}
            setSearchTerm={setSearchTerm}
          />
          <div>
            <div className="w-full relative">
              <div className="overflow-y-auto px-1.5 pt-2">
                {searchResults?.length > 0 ? (
                  searchResults?.map((item: any) => (
                    <div
                      key={item.id}
                      className="w-full flex items-center justify-between items-center gap-x-2 hover:bg-[#9D21E6]/10 border p-2.5 pr-4 rounded-lg mb-2.5 cursor-pointer"
                    >
                      <div className="flex items-center gap-x-2.5 ">
                        <div className="relative h-12 w-12 border p-0.5 rounded-lg border-gray-300">
                          <Image
                            width={60}
                            height={60}
                            src={
                              item?.profilePict ?? `/static/images/logokedi.svg`
                            }
                            className=" absolute inset-0 h-full w-full object-cover  rounded-lg"
                            alt=""
                          />
                        </div>
                        <div>
                          <p className="font-semibold text-ellipsis">
                            {item.fullName}
                          </p>
                          {item.username && (
                            <p className="text-sm text-[#9D21E6]">
                              {"@" + item.username}
                            </p>
                          )}
                        </div>
                      </div>
                      <div className="flex items-center gap-x-2.5">
                        <button
                          onClick={() => {
                            setAlertBlock(true);

                            setTempData({
                              id: parseInt(item.id),
                              fullName: item.fullName ?? item.username,
                            });
                          }}
                          className="w-6 h-6 text-red-600 cursor-pointer"
                        >
                          <svg
                            xmlns="http://www.w3.org/2000/svg"
                            viewBox="0 0 24 24"
                          >
                            <path
                              fill="currentColor"
                              d="M12 2C6.48 2 2 6.48 2 12s4.48 10 10 10s10-4.48 10-10S17.52 2 12 2zM4 12c0-4.42 3.58-8 8-8c1.85 0 3.55.63 4.9 1.69L5.69 16.9A7.902 7.902 0 0 1 4 12zm8 8c-1.85 0-3.55-.63-4.9-1.69L18.31 7.1A7.902 7.902 0 0 1 20 12c0 4.42-3.58 8-8 8z"
                            ></path>
                          </svg>
                        </button>
                        <button
                          onClick={() => {
                            setAlertRemove(true);

                            setTempData({
                              id: parseInt(item.id),
                              fullName: item.fullName ?? item.username,
                            });
                          }}
                          className="w-6 h-6 text-red-600 cursor-pointer"
                        >
                          <svg
                            xmlns="http://www.w3.org/2000/svg"
                            viewBox="0 0 24 24"
                          >
                            <path
                              fill="currentColor"
                              d="m8.4 17l3.6-3.6l3.6 3.6l1.4-1.4l-3.6-3.6L17 8.4L15.6 7L12 10.6L8.4 7L7 8.4l3.6 3.6L7 15.6zm3.6 5q-2.075 0-3.9-.788t-3.175-2.137q-1.35-1.35-2.137-3.175T2 12q0-2.075.788-3.9t2.137-3.175q1.35-1.35 3.175-2.137T12 2q2.075 0 3.9.788t3.175 2.137q1.35 1.35 2.138 3.175T22 12q0 2.075-.788 3.9t-2.137 3.175q-1.35 1.35-3.175 2.138T12 22m0-2q3.35 0 5.675-2.325T20 12q0-3.35-2.325-5.675T12 4Q8.65 4 6.325 6.325T4 12q0 3.35 2.325 5.675T12 20m0-8"
                            ></path>
                          </svg>
                        </button>
                      </div>
                    </div>
                  ))
                ) : searchResults && (
                  <div className="min-h-[60vh] h-full flex items-center justify-center">
                    {!isLoadingList && lang.dataNotFound}
                  </div>
                )}
              </div>
            </div>
          </div>
        </div>
      </div>

    {modalMenuProfile && (
      <Dialog
        open={modalMenuProfile}
        onOpenChange={setModalMenuProfile}
        onOpenAutoFocus={(e) => e.preventDefault()}
      >
        <DialogContent>
          <DialogHeader>
            <DialogTitle className="text-center">
              {modalType === "Kontak Blocked" && "Kontak yang Diblokir"}
            </DialogTitle>
          </DialogHeader>
          {modalType === "Kontak Blocked" && (
            <div
              value="general"
              className="max-h-[65vh] overflow-y-auto px-1.5 pt-2"
            >
              {filteredDataBlock?.length > 0 ? (
                filteredDataBlock?.map((item: any) => (
                  <div
                    key={item.id}
                    className="w-full flex items-center justify-between items-center gap-x-2 hover:bg-[#9D21E6]/10 border p-2.5 pr-4 rounded-lg mb-2.5 cursor-pointer"
                  >
                    <div className="flex items-center gap-x-2.5 ">
                      <div className="relative h-12 w-12 border p-0.5 rounded-lg border-gray-300">
                        <Image
                          width={60}
                          height={60}
                          src={
                            item?.profilePictureLink ??
                            `/static/images/logokedi.svg`
                          }
                          className=" absolute inset-0 h-full w-full object-cover  rounded-lg"
                          alt=""
                        />
                      </div>
                      <div>
                        <p className="font-semibold text-ellipsis">
                          {item.fullName}
                        </p>
                        {item.username && (
                          <p className="text-sm text-[#9D21E6]">
                            {"@" + item.username}
                          </p>
                        )}
                      </div>
                    </div>
                    <button
                      onClick={() => {
                        setAlertUnblock(true);

                        setTempData({
                          id: parseInt(item.id),
                          fullName: item.fullName ?? item.username,
                        });
                      }}
                      className="flex items-center text-sm gap-x-2 text-red-600 cursor-pointer font-semibold"
                    >
                      <svg
                        className="w-6 h-6"
                        xmlns="http://www.w3.org/2000/svg"
                        viewBox="0 0 24 24"
                      >
                        <path
                          fill="currentColor"
                          d="M12 2C6.48 2 2 6.48 2 12s4.48 10 10 10s10-4.48 10-10S17.52 2 12 2zM4 12c0-4.42 3.58-8 8-8c1.85 0 3.55.63 4.9 1.69L5.69 16.9A7.902 7.902 0 0 1 4 12zm8 8c-1.85 0-3.55-.63-4.9-1.69L18.31 7.1A7.902 7.902 0 0 1 20 12c0 4.42-3.58 8-8 8z"
                        ></path>
                      </svg>
                      <span>Unblock</span>
                    </button>
                  </div>
                ))
              ) : (
                <div className="h-full flex items-center justify-center">
                  {!filteredDataBlock.isLoading && lang.dataNotFound}
                </div>
              )}
            </div>
          )}
        </DialogContent>
      </Dialog>
    )}
    </>
  );
};

export default Home;

const TitleMenu = ({
  lang,
  title,
  setModalMenuProfile,
  setModalType,
  searchTerm,
  setSearchTerm,
}) => {
  const [focus, setFocus] = useState(false);
  const focusSearch = () => {
    const searchInput = document.getElementById("searchInput");
    if (searchInput) {
      searchInput.focus();
    }
  };
  return (

    <div className="relative mb-5 flex items-center justify-between pt-3 w-full h-[40px]">
      <Link href="/dashboard" className="flex items-center flex-none">
        <div className="flex-none bg-[#9D21E6] flex justify-center items-center rounded-full w-8 h-8 lg:w-10 lg:h-10 p-1">
          <svg
            stroke="currentColor"
            fill="currentColor"
            strokeWidth={0}
            viewBox="0 0 1024 1024"
            className="text-white "
            height={20}
            width={20}
            xmlns="http://www.w3.org/2000/svg"
          >
            <path d="M872 474H286.9l350.2-304c5.6-4.9 2.2-14-5.2-14h-88.5c-3.9 0-7.6 1.4-10.5 3.9L155 487.8a31.96 31.96 0 0 0 0 48.3L535.1 866c1.5 1.3 3.3 2 5.2 2h91.5c7.4 0 10.8-9.2 5.2-14L286.9 550H872c4.4 0 8-3.6 8-8v-60c0-4.4-3.6-8-8-8z" />
          </svg>
        </div>
        <h1 className="ml-2 lg:ml-4 text-lg sm:text-xl md:text-2xl font-bold text-[#9D21E6] self-center ">
          {title}
        </h1>
      </Link>
      <div
        className={` ${
          focus ? "w-full absolute" : "relative"
        } duration-500 z-10 text-gray-600 flex items-center`}
      >
        <input
          className="sm:w-[135px] w-[10px] peer focus:w-full focus:duration-500  focus:border  focus:shadow-md border-gray-200 bg-white h-11 focus:pl-10 pr-4 rounded-lg text-sm focus:outline-none"
          type="search"
          name="search"
          id="searchInput"
          onFocus={() => setFocus(true)}
          onBlur={() => setFocus(false)}
          autoComplete="off"
          value={searchTerm}
          onChange={(e) => setSearchTerm(e.target.value)}
          placeholder={lang.search}
        />
        <button
          onClick={focusSearch}
          className="absolute  peer-focus:duration-500 -left-6 peer-focus:left-3 top-0.5 mt-3"
        >
          <svg
            className="text-gray-600 h-4 w-4 fill-current"
            xmlns="http://www.w3.org/2000/svg"
            xmlnsXlink="http://www.w3.org/1999/xlink"
            version="1.1"
            id="Capa_1"
            x="0px"
            y="0px"
            viewBox="0 0 56.966 56.966"
            style={{ enableBackground: "new 0 0 56.966 56.966" }}
            xmlSpace="preserve"
            width="512px"
            height="512px"
          >
            <path d="M55.146,51.887L41.588,37.786c3.486-4.144,5.396-9.358,5.396-14.786c0-12.682-10.318-23-23-23s-23,10.318-23,23  s10.318,23,23,23c4.761,0,9.298-1.436,13.177-4.162l13.661,14.208c0.571,0.593,1.339,0.92,2.162,0.92  c0.779,0,1.518-0.297,2.079-0.837C56.255,54.982,56.293,53.08,55.146,51.887z M23.984,6c9.374,0,17,7.626,17,17s-7.626,17-17,17  s-17-7.626-17-17S14.61,6,23.984,6z" />
          </svg>
        </button>
        {!focus && (
        <DropdownMenu>
          <DropdownMenuTrigger className="">
            <div className="hover:bg-[#CFB6FF] relative flex justify-center cursor-pointer items-center rounded-full w-6 h-6 lg:w-10 lg:h-10">
              <svg
                stroke="currentColor"
                fill="currentColor"
                strokeWidth={0}
                viewBox="0 0 16 16"
                className="text-[#9D21E6] cursor-pointer w-5 h-5"
                xmlns="http://www.w3.org/2000/svg"
              >
                <path d="M9.5 13a1.5 1.5 0 1 1-3 0 1.5 1.5 0 0 1 3 0zm0-5a1.5 1.5 0 1 1-3 0 1.5 1.5 0 0 1 3 0zm0-5a1.5 1.5 0 1 1-3 0 1.5 1.5 0 0 1 3 0z" />
              </svg>
            </div>
          </DropdownMenuTrigger>
          <DropdownMenuContent className="mr-8">
            <DropdownMenuLabel>{lang.contactMenu}</DropdownMenuLabel>
            <DropdownMenuSeparator />
            <DropdownMenuItem
              onClick={() => {
                setModalMenuProfile(true);
                setModalType("Kontak Blocked");
              }}
            >
              {lang.blockedContacts}
            </DropdownMenuItem>
          </DropdownMenuContent>
        </DropdownMenu>    
        )}
        
      </div>
      {/*      <div className="hidden hover:bg-[#CFB6FF] relative flex justify-center cursor-pointer items-center rounded-full w-6 h-6 lg:w-10 lg:h-10">
        <svg
          stroke="currentColor"
          fill="currentColor"
          strokeWidth={0}
          viewBox="0 0 16 16"
          className="text-[#9D21E6] cursor-pointer w-5 h-5"
          xmlns="http://www.w3.org/2000/svg"
        >
          <path d="M9.5 13a1.5 1.5 0 1 1-3 0 1.5 1.5 0 0 1 3 0zm0-5a1.5 1.5 0 1 1-3 0 1.5 1.5 0 0 1 3 0zm0-5a1.5 1.5 0 1 1-3 0 1.5 1.5 0 0 1 3 0z" />
        </svg>
      </div>*/}
    </div>
  );
};

export async function getServerSideProps(context: any) {
  try {
    const session = await getSession(context);
    if (session) {
      return {
        props: { user: session.user },
      };
    } else {
      return {
        redirect: {
          destination: `/login`,
          permanent: false,
        },
      };
    }
  } catch (error) {
    console.error("Kesalahan:", error);
    return {
      props: {
        error: "Terjadi kesalahan saat memuat sesi pengguna",
      },
    };
  }
}
