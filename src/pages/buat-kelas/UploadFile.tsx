
import { useState } from 'react';
import axios from 'axios';

const UploadFilePage = ({ sessionToken }) => {
  const [file, setFile] = useState(null);
  const [response, setResponse] = useState(null);
  const [error, setError] = useState(null);

  const handleFileChange = (e) => {
    setFile(e.target.files[0]);
  };

  const handleSubmit = async () => {
    const formData = new FormData();
    formData.append('file', file);
    formData.append('documentTypeId', '1');
    formData.append('isPrivate', 'false');

    try {
      const res = await axios.post(`${process.env.API_BE}/api/storage`, formData, {
        headers: {
          'Content-Type': 'multipart/form-data',
          Authorization: `Bearer ${sessionToken}`,
        },
      });
      setResponse(res.data);
      setError(null);
    } catch (err) {
      setResponse(null);
      setError(err.response?.data?.meta?.message || 'An error occurred during file upload.');
    }
  };

  return (
    <div>
      <h1>Upload File</h1>
      <input type="file" onChange={handleFileChange} />
      <button onClick={handleSubmit}>Upload</button>
      {response && (
        <div>
          <h2>Response:</h2>
          <pre>{JSON.stringify(response, null, 2)}</pre>
        </div>
      )}
      {error && (
        <div style={{ color: 'red' }}>
          <p>Error: {error}</p>
        </div>
      )}
    </div>
  );
};

export default UploadFilePage;
