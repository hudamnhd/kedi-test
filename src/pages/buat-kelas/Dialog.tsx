import { useState, useRef, useEffect, ChangeEvent } from "react";
import {
  AlertDialog,
  AlertDialogAction,
  AlertDialogCancel,
  AlertDialogContent,
  AlertDialogDescription,
  AlertDialogFooter,
  AlertDialogHeader,
  AlertDialogTitle,
} from "@/components/ui/alert-dialog";

const DialogCreateClass = () => {
  const [isOpen, setIsOpen] = useState(true);
  return (
    <AlertDialog open={isOpen}>
      <AlertDialogContent className="max-w-4xl">
        <AlertDialogHeader>
          <AlertDialogTitle className="text-center text-xl mb-3">
            Syarat dan Ketentuan Pembuatan Kelas di Kelas Digital
          </AlertDialogTitle>
          <AlertDialogDescription asChild>
            <ul className="list-decimal px-5 space-y-1 text-gray-600 text-left max-h-[60vh] overflow-y-auto">
              <li>
                Nama, topik dan materi kelas tidak boleh mengandung unsur SARA,
                pornografi, kekerasan dan hal-hal lain yang melanggar hukum.{" "}
              </li>
              <li>
                Tanggal terakhir pendaftaran kelas minimal tiga hari sebelum
                tanggal pelaksanaan kelas.{" "}
              </li>
              <li>
                Kelas dengan topik yang sama bisa dibuka kembali pendaftaran
                setelah selesai pelaksanaan kelas yang awal.{" "}
              </li>
              <li>
                Kelas tidak boleh dibatalkan pelaksanaannya jika pendaftar lebih
                dari satu. Anda diperkenankan untuk memperpanjang pendaftaran
                dan mengubah jadwal pelaksanaan kelas jika pendaftar kurang dari
                lima orang.{" "}
              </li>
              <li>
                Kelas gratis tidak boleh dibatalkan pelaksanaan jika pendaftar
                lebih dari lima orang.{" "}
              </li>
              <li>
                Jika Anda membatalkan pelaksanaan kelas tanpa alasan yang
                diperbolehkan, maka Anda tidak boleh membuat kelas baru dalam
                waktu tiga bulan.{" "}
              </li>
              <li>
                Kelas Berbayar Publik mengikuti tarif yang ditetapkan oleh Kelas
                Digital sebesar Rp.30.000,- per sesi per peserta. Kelas akan
                dipublikasikan di Beranda Kelas Digital dan dipromosikan lewat
                email kepada seluruh pengguna Kelas Digital.{" "}
              </li>
              <li>
                Kelas Berbayar Privat boleh menentukan tarif sesuai yang
                diinginkan oleh pembuat kelas. Kelas tidak akan muncul dalam
                halaman beranda, dan pembuat kelas bisa mempromosikan lewat
                media sosial yang dimiliki.
              </li>
            </ul>
          </AlertDialogDescription>
        </AlertDialogHeader>
        <AlertDialogFooter className="mx-auto gap-3 grid grid-cols-2 space-y-0 mt-2">
          <AlertDialogAction
            onClick={() => setIsOpen(false)}
            className="bg-[#9D21E6] hover:bg-purple-600 text-white rounded-lg"
          >
            Setuju
          </AlertDialogAction>
          <AlertDialogCancel
            onClick={() => {
              setIsOpen(false);
              setTimeout(() => {
              }, 500);
            }}
            className="disabled:opacity-60 bg-[#D7B9FD] text-[#9D21E6]"
          >
            Tidak
          </AlertDialogCancel>
        </AlertDialogFooter>
      </AlertDialogContent>
    </AlertDialog>
  );
};
export default DialogCreateClass
