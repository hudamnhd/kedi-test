import dynamic from "next/dynamic";
import React from "react";
import axios from "axios";
import toast from "react-hot-toast";
import moment from "moment";
import { useState, useRef, useEffect, ChangeEvent } from "react";
import { getSession } from "next-auth/react";
import { format } from "date-fns";
import { cn } from "@/lib/utils";
import { useMutation, useQueryClient } from "@tanstack/react-query";
import { useRouter } from "next/router";
import { trans } from "@/lang";
import { LoadingSpinner } from "@/components/ui/loading";
import { Navbar } from "@/components/navbar/navbar";
import { Label } from "@/components/ui/label";
import { RadioGroup, RadioGroupItem } from "@/components/ui/radio-group";
import { Calendar as CalendarIcon } from "lucide-react";
import { Switch } from "@/components/ui/switch";
import { ToggleGroup, ToggleGroupItem } from "@/components/ui/toggle-group";
import { Calendar } from "@/components/ui/calendar";
import { Button } from "@/components/ui/button";
import {
  defaultImgDevelopment,
  defaultImgStaging,
  defaultImgProduction,
} from "@/assets/defaultImg";
import {
  Popover,
  PopoverContent,
  PopoverTrigger,
} from "@/components/ui/popover";
import Link from "next/link";
import {
  Select,
  SelectContent,
  SelectGroup,
  SelectItem,
  SelectTrigger,
  SelectValue,
} from "@/components/ui/select";
import { getPubNub } from "@/utils/getPubNub";
import useUserChat from "@/store/useUserChat";

// import UploadFilePage from "./UploadFile";
const DialogCreateClass = dynamic(() => import("./Dialog"), { ssr: false });

let APIURL = process.env.API_LINK;

let defaultImg;

if (APIURL === "Development") {
  defaultImg = defaultImgDevelopment;
} else if (APIURL === "Staging") {
  defaultImg = defaultImgStaging;
} else {
  defaultImg = defaultImgProduction;
}

const formatRupiah = (
  angka: number,
  prefix: string = "Rp.",
): { formatted: string; original: number } => {
  const numberString = angka.toString().replace(/[^,\d]/g, ""),
    split = numberString.split(","),
    sisa = split[0].length % 3,
    rupiah = split[0].substr(0, sisa),
    ribuan = split[0].substr(sisa).match(/\d{3}/gi);
  let formattedRupiah = "";

  if (ribuan) {
    const separator = sisa ? "." : "";
    formattedRupiah = rupiah + separator + ribuan.join(".");
  } else {
    formattedRupiah = rupiah;
  }

  formattedRupiah =
    split[1] !== undefined ? formattedRupiah + "," + split[1] : formattedRupiah;

  return {
    formatted: `${prefix} ${formattedRupiah}`,
    original: parseFloat(numberString),
  };
};

const Home = ({ user }: any) => {
  return (
    <div className="bg-[#FAFAFA]">
      <Navbar user={user} />
      <BuatKelas user={user} />
    </div>
  );
};

const today = new Date();
const initialDate = `${today.getFullYear()}-${(today.getMonth() + 1)
  .toString()
  .padStart(2, "0")}-${today.getDate().toString().padStart(2, "0")}`;

const initState = {
  name: "",
  description: "",
  repetition: {
    repeatJump: 1,
    repeatEvery: "day",
    repeatDays: [],
    sessionCount: 1,
  },
  startRegisterDate: "",
  endRegisterDate: "",
  startClassDate: "",
  endClassDate: "",
  startLearningTime: "",
  endLearningTime: "",
  maximumCapacity: 10,
  isPaidClass: false,
  registrationFee: null,
  isPrivateClass: false,
  pin: null,
  isAttendanceRequired: false,
  startAttendanceTime: null,
  endAttendanceTime: null,
  feedDocumentStorageIds: [],
};

const BuatKelas = ({ user }: any) => {
  const router = useRouter();

  const locale: "en" | "id" = router.locale as "en" | "id";
  const lang = trans[locale].createClass;

  const [isCalendarOpen, setIsCalendarOpen] = useState({
    startReg: false,
    endReg: false,
    startClass: false,
  });

  const [isErrDate, setIsErrDate] = useState({
    startReg: "",
    endReg: "",
    startClass: "",
    endLearn: "",
    endAttend: "",
  });
  const queryClient = useQueryClient();

  const sessionToken = user?.accessToken;
  const [fileClass, setFileClass] = React.useState<any>([]);
  const [isSesi, setSesi] = useState(false);
  const [data, setdata] = useState(initState);
  const [dataPin, setDataPin] = useState("");

  const [checkedItems, setCheckedItems] = useState([]);
  {
    /*
  const handleChangeNum = (event) => {
    let result = event.target.value.replace(/\D/g, "");
    result = Math.max(1, parseInt(result, 10));
    result = isNaN(result) ? 0 : result;
    setdata({
      ...data,
      maximumCapacity: result,
    });
  };
  */
  }

  const handleValueChange = (value: string) => {
    if (value === "day" || value === "month") {
      setdata((prevData) => {
        const { repeatDays, ...newRepetition } = prevData.repetition;

        return {
          ...prevData,
          repetition: {
            ...newRepetition,
            repeatJump: 1,
            repeatEvery: value,
            // repeatDays is removed
          },
        };
      });
    } else {
      setdata({
        ...data,
        repetition: {
          ...data.repetition,
          repeatJump: 1,
          repeatEvery: value,
          repeatDays: [],
        },
      });
    }
  };

  const handleWeekRepeat = (value: string) => {
    if (value) {
      setdata((prevdata: any) => ({
        ...prevdata,
        repetition: {
          ...data.repetition,
          repeatJump: 1,
          repeatDays: value,
        },
      }));
    }
  };

  const handleChange = (
    e: React.ChangeEvent<HTMLInputElement | HTMLTextAreaElement>,
  ) => {
    const name = e.target.name;

    if (name === "endLearningTime") {
      if (e.target.value <= data.startLearningTime) {
        setIsErrDate({
          ...isErrDate,
          endLearn: "Waktu Selesai harus lebih dari waktu mulai",
        });
      } else {
        setIsErrDate({
          ...isErrDate,
          endLearn: "",
        });
      }
    }

    if (name === "endAttendanceTime") {
      if (e.target.value <= data.startAttendanceTime) {
        console.log(e.target.value);
        setIsErrDate({
          ...isErrDate,
          endAttend: "Waktu Selesai harus lebih dari waktu mulai",
        });
      } else {
        setIsErrDate({
          ...isErrDate,
          endAttend: "",
        });
      }
    }

    if (name === "maximumCapacity") {
      let result = event.target.value.replace(/\D/g, "");
      result = Math.max(1, parseInt(result, 10));
      result = isNaN(result) ? 0 : result;
      setdata({
        ...data,
        maximumCapacity: result,
      });
    } else {
      setdata({
        ...data,
        [name]: e.target.value,
      });
    }
  };

  const handleSessionChange = (inputValue: any) => {
    const value = parseInt(inputValue, 10);
    if (value > 14)
      return setdata((prevdata: any) => ({
        ...prevdata,
        repetition: {
          sessionCount: 14,
        },
      }));

    if (!isNaN(value) || inputValue === "") {
      setdata((prevdata: any) => ({
        ...prevdata,
        repetition: {
          sessionCount: inputValue === "" ? "" : value,
        },
      }));
    } else {
      console.error("Invalid input. Please enter a numeric value.");
    }
  };

  function parseDateValue(date: any, time: any, isAddDays: any) {
    if (date instanceof Date) {
      return isAddDays ? moment(date).add(2, "days").toDate() : date;
    }

    if (date && time && typeof time === "string") {
      return new Date(`${date}T${time}`);
    }

    return null;
  }

  function parseISODate(date) {
    return date instanceof Date ? date.toISOString() : null;
  }

  function parseData(data) {
    const endClassDate = parseDateValue(data?.startClassDate, null, true);
    const startLearningTime = parseDateValue(
      initialDate,
      data?.startLearningTime,
      false,
    );
    const endLearningTime = parseDateValue(
      initialDate,
      data?.endLearningTime,
      false,
    );
    const startAttendanceTime = parseDateValue(
      initialDate,
      data?.startAttendanceTime,
      false,
    );
    const endAttendanceTime = parseDateValue(
      initialDate,
      data?.endAttendanceTime,
      false,
    );

    const payload = {
      ...data,
      name: data?.name,
      description: data?.description,
      startRegisterDate: parseISODate(data?.startRegisterDate),
      endRegisterDate: parseISODate(data?.endRegisterDate),
      startClassDate: parseISODate(data?.startClassDate),
      endClassDate: parseISODate(endClassDate),
      startLearningTime: parseISODate(startLearningTime),
      endLearningTime: parseISODate(endLearningTime),
      startAttendanceTime: parseISODate(startAttendanceTime),
      endAttendanceTime: parseISODate(endAttendanceTime),
      maximumCapacity: parseInt(data?.maximumCapacity),
    };

    const isErrDateEmpty = Object.values(isErrDate).every(
      (value) => !value.trim(),
    );

    if (!isErrDateEmpty) {
      throw new Error("Periksa Kembali data yang di isi");
    }
    if (!payload.name.trim() || !payload.description.trim()) {
      throw new Error("Nama dan deskripsi tidak boleh kosong");
    } else if (payload.feedDocumentStorageIds.length === 0) {
      throw new Error("Pilih gambar kelas");
    } else if (
      !payload.startRegisterDate ||
      !payload.endRegisterDate ||
      !payload.startClassDate
    ) {
      throw new Error("lengkapi Form");
    } else if (payload.isAttendanceRequired) {
      if (!payload.startAttendanceTime || !payload.endAttendanceTime) {
        throw new Error("Harap lengkapi Waktu Absen");
      }
    }
    return payload;
  }

  const [formattedValue, setFormattedValue] = useState<string>("");

  const handleInputChange = (event: React.ChangeEvent<HTMLInputElement>) => {
    const rawValue = event.target.value;

    const parsedValue = parseFloat(rawValue.replace(/[^\d]/g, ""));
    setdata({ ...data, registrationFee: parsedValue });

    if (!isNaN(parsedValue)) {
      const formattedResult = formatRupiah(parsedValue);
      setFormattedValue(formattedResult.formatted);
    } else {
      setFormattedValue("");
    }
  };
  const isChecked = (itemId) => checkedItems.includes(itemId);

  const handleCheckboxChange = (itemId) => {
    setCheckedItems((prevCheckedItems) => {
      const updatedCheckedItems = isChecked(itemId)
        ? prevCheckedItems.filter((item) => item !== itemId)
        : [...prevCheckedItems, itemId];

      // Update data dengan feedDocumentStorageIds yang baru
      setdata({
        ...data,
        feedDocumentStorageIds: updatedCheckedItems,
      });

      return updatedCheckedItems;
    });
  };

  const handleFileChangeClass = (event: ChangeEvent<HTMLInputElement>) => {
    const file = event.target.files ?? null;
    if (file) {
      const filesArray = Array.from(file);
      setFileClass((prevFiles: any) => [...prevFiles, ...filesArray]);
    }
  };
  const handleDropClass = (event: any) => {
    event.preventDefault();

    const file = event.dataTransfer.files ?? null;
    if (file) {
      const filesArray = Array.from(file);
      setFileClass((prevFiles: any) => [...prevFiles, ...filesArray]);
    }
  };

  const uploadFileClass = (file: any, sessionToken: string) => {
    return new Promise((resolve, reject) => {
      const formData = new FormData();
      formData.append("file", file);
      formData.append("documentTypeId", "1");
      formData.append("isPrivate", "true");

      axios
        .post(process.env.API_BE + "/api/storage", formData, {
          headers: {
            "Content-Type": "multipart/form-data",
            Authorization: `Bearer ${sessionToken}`,
          },
        })
        .then((res) => {
          resolve(res.data.documentStorage.id);
        })
        .catch((err) => {
          reject(err.response.data.meta.message);
        });
    });
  };

  const handleUploadFiles = async () => {
    const uploadPromises = fileClass.map((file: any) =>
      uploadFileClass(file, sessionToken ?? ""),
    );
    try {
      const responses = await Promise.all(uploadPromises);
      if (fileClass.length > 0) {
        toast.success("Semua file berhasil diunggah");
        console.log("Responses:", responses);
      }
      return responses;
    } catch (error) {
      toast.error("Terjadi kesalahan saat mengunggah file");
      console.error("Error uploading files:", error);
    }
  };

  const fileInputRef = useRef<HTMLInputElement>(null);
  const inputFileRef = useRef(null);

  const handleButtonClick = () => {
    if (fileInputRef.current) {
      fileInputRef.current.click();
    }
  };

  const handleDragOver = (event: any) => {
    event.preventDefault();
  };

  const handleDragEnter = (event: any) => {
    event.preventDefault();
  };

  const handleDragLeave = (event: any) => {
    event.preventDefault();
  };

  useEffect(() => {
    if (fileClass.length > 0) {
      setCheckedItems([]);
    }
  }, [fileClass]);

  interface ApiResponse {
    data: {
      message: string;
    };
  }

  interface PostResponse extends ApiResponse {}
  type PostClass = {};

  const setallClassMessage = (value) => {
    useUserChat.setState({
      allClassMessage: value,
    });
  };
  const setPrivateMessage = (value) => {
    useUserChat.setState({
      privateMessage: value,
    });
  };
  const setGroupChatMessage = (value) => {
    useUserChat.setState({
      groupChatMessage: value,
    });
  };

  const setChannelStore = (value) => {
    useUserChat.setState({
      channelStore: value,
    });
  };

  const postClass: PostClass = useMutation(
    async (): Promise<PostResponse> => {
      if (!sessionToken) throw new Error("Missing session token");
      try {
        const uploading = await handleUploadFiles();
        const newData = {
          ...data,
          feedDocumentStorageIds:
            uploading.length > 0 ? uploading : data.feedDocumentStorageIds,
        };

        const processData = parseData(newData);
        const response = await axios.post<PostResponse>(
          process.env.API_BE + `/api/class`,
          processData,
          {
            headers: {
              Authorization: `Bearer ${sessionToken}`,
            },
          },
        );
        console.log(response.data);
        return response.data;
      } catch (error) {
        console.error(error);
        throw error;
      }
    },
    {
      onSuccess: () => {
        toast.success("Kelas Berhasil Di Buat");
        setdata(initState);
        setFileClass([]);
        setSesi(false);
        setCheckedItems([]);

      (async () => {
        const msg = await getPubNub(user);

        setPrivateMessage(msg?.privateMessage);
        setallClassMessage(msg?.classMessage);
        setGroupChatMessage(msg?.groupChatMessage);

        const storagePubnub = JSON.parse(
          localStorage.getItem("sessionPubnub") ?? "",
        );

        const groupChannels = storagePubnub?.channels?.filter((channelName) =>
          channelName.startsWith("chat-group."),
        );

        const classChannels = storagePubnub?.channels?.filter((channelName) =>
          channelName.startsWith("group."),
        );

        let initialClass = [...classChannels];
        let initialGroup = [...groupChannels];

        let dataPrivate = localStorage.getItem("channelActivePrivate");
        let initialPrivate = dataPrivate ? JSON.parse(dataPrivate) : [];

        // Set semua data yang tersedia dari sumber eksternal

        setChannelStore({
          storedGroup: initialGroup,
          storedPrivate: initialPrivate,
          storedClass: initialClass,
        });

        router.push("/");
      })();
      },

      onError: (error) =>
        toast.error(`Error: ${error.response?.data?.meta?.message ?? error}`),
    },
  );
  function formatTanggalIndonesia(inputTanggal) {
    const namaBulanIndonesia = [
      "Januari",
      "Februari",
      "Maret",
      "April",
      "Mei",
      "Juni",
      "Juli",
      "Agustus",
      "September",
      "Oktober",
      "November",
      "Desember",
    ];

    const tanggalObjek = new Date(inputTanggal);

    const tanggal = tanggalObjek.getDate();
    const bulanIndex = tanggalObjek.getMonth();
    const tahun = tanggalObjek.getFullYear();

    const tanggalDalamFormat = `${tanggal} ${namaBulanIndonesia[bulanIndex]} ${tahun}`;

    return tanggalDalamFormat;
  }
  // console.log(isErrDate.startReg !== "" || data.startRegisterDate === "");

  const [viewportHeight, setViewportHeight] = useState(0);

  useEffect(() => {
    const adjustHeight = () => {
      const vh = window.innerHeight;
      setViewportHeight(vh);
    };
    adjustHeight();

    window.addEventListener("resize", adjustHeight);
    return () => {
      window.removeEventListener("resize", adjustHeight);
    };
  }, []);
  return (
    <section className="h-full relative">
      <DialogCreateClass />
      {postClass.isLoading && (
        <div className="absolute flex items-center justify-center w-full h-full bg-white/30 z-10">
          <LoadingSpinner size={70} strokeWidth={2} />
        </div>
      )}
      <div
        style={{ height: viewportHeight, overflow: "auto" }}
        className="relative pt-[4.4rem] sm:pt-[5.2rem] pb-[3.8rem] lg:pb-20 pb-2.5 className flex flex-col w-full max-w-4xl mx-auto shadow-md px-3 sm:px-5"
      >
        <div className="mt-6 sm:mt-10 mb-6 sm:mb-8 flex items-center px-2">
          <Link
            href="/"
            className=" bg-purple-100 text-[#9d21e6] flex justify-center items-center rounded-full w-10 h-10  "
          >
            <svg
              stroke="currentColor"
              fill="currentColor"
              strokeWidth={0}
              viewBox="0 0 24 24"
              height={32}
              width={32}
              xmlns="http://www.w3.org/2000/svg"
            >
              <path d="M13.293 6.293 7.586 12l5.707 5.707 1.414-1.414L10.414 12l4.293-4.293z" />
            </svg>
          </Link>
          <h1 className="ml-4 sm:ml-8 font-semibold text-[#9d21e6] text-xl sm:text-2xl md:text-3xl font-montserrat-semibold">
            {lang.title}
          </h1>
        </div>
        {/*        <UploadFilePage sessionToken={sessionToken} />*/}
            <form
              onSubmit={(e) => {
                e.preventDefault();
                postClass.mutate();
              }}
              className="w-full bg-white p-6 rounded-xl border shadow-md"
            >
              <section>
                <h1 className="font-bold text-lg text-neutral-90">
                  {lang.uploadImage}
                </h1>
                <div className="flex justify-between my-4">
                  <label className="text-lg font-normal text-[#9d21e6]">
                    Default Kelas Digital
                  </label>
                  <label className="sm:inline hidden text-lg font-normal text-gray-400 text-right">
                    Powered by Unsplash
                  </label>
                </div>
                <div className="w-fit mx-auto flex mb-4 overflow-x-auto mt-6 sm:mt-11 pb-2.5">
                  {defaultImg?.map((d) => (
                    <label
                      key={d.documentStorage.id}
                      className={`border-4 flex-none relative ${
                        isChecked(d.documentStorage.id)
                          ? "border-purple-500"
                          : "border-transparent"
                      } mr-2 rounded-xl cursor-pointer`}
                    >
                      <img
                        className="w-28 h-28 object-cover rounded-xl shadow-md"
                        src={d.url}
                        alt="Default image class"
                      />
                      <input
                        type="checkbox"
                        checked={isChecked(d.documentStorage.id)}
                        onChange={() =>
                          handleCheckboxChange(d.documentStorage.id)
                        }
                        className="hidden"
                      />
                    </label>
                  ))}
                </div>
                <>
                  <div
                    onDragOver={handleDragOver}
                    onDragEnter={handleDragEnter}
                    onDragLeave={handleDragLeave}
                    onDrop={handleDropClass}
                    className="p-0.5 bg-[#cfb6ff]/30 rounded-[10px] border-2 border-dashed border-[#9d21e6] mb-3"
                  >
                    <div className="relative flex  w-full gap-x-1.5 rounded-xl ">
                      <button
                        type="button"
                        className="h-24 w-full text-center p-2.5 items-center text-[#9d21e6] font-semibold inline-flex items-center justify-center rounded-full transition duration-500 ease-in-out focus:outline-none"
                        onClick={handleButtonClick}
                      >
                        <label
                          htmlFor="fileInput"
                          className="flex items-center gap-x-2.5  cursor-pointer"
                        >
                          <svg
                            xmlns="http://www.w3.org/2000/svg"
                            fill="none"
                            viewBox="0 0 24 24"
                            strokeWidth={1.5}
                            stroke="currentColor"
                            className="w-6 h-6"
                          >
                            <path
                              strokeLinecap="round"
                              strokeLinejoin="round"
                              d="M12 10.5v6m3-3H9m4.06-7.19l-2.12-2.12a1.5 1.5 0 00-1.061-.44H4.5A2.25 2.25 0 002.25 6v12a2.25 2.25 0 002.25 2.25h15A2.25 2.25 0 0021.75 18V9a2.25 2.25 0 00-2.25-2.25h-5.379a1.5 1.5 0 01-1.06-.44z"
                            />
                          </svg>
                          <span>{lang.fileInput}</span>
                        </label>
                        <input
                          ref={inputFileRef}
                          id="fileInput"
                          type="file"
                          multiple
                          accept="image/*"
                          className="hidden"
                          onChange={handleFileChangeClass}
                        />
                      </button>
                    </div>
                  </div>
                  {fileClass?.length > 0 && (
                    <div className="px-4 py-2.5 flex gap-x-4 overflow-y-auto max-h-[22vh] mb-4  rounded-[10px] border border-gray-400">
                      {fileClass?.map((file: any, index: number) => (
                        <div className="relative" key={index}>
                          <img
                            src={URL.createObjectURL(file)}
                            className="rounded-xl shadow-md"
                            alt={`File ${index + 1}`}
                            style={{
                              width: "100px",
                              height: "100px",
                              objectFit: "cover",
                            }}
                          />
                          <button
                            className="absolute top-1 right-1 cursor-pointer text-red-600 hover:bg-white rounded-full p-1.5 hover:ring-1 ring-red-600  duration-300"
                            onClick={() =>
                              setFileClass((prevFiles: any) => {
                                const updatedFiles = [...prevFiles];
                                updatedFiles.splice(index, 1);
                                return updatedFiles;
                              })
                            }
                          >
                            <svg
                              className="pointer-events-none fill-current w-4 h-4 ml-auto"
                              xmlns="http://www.w3.org/2000/svg"
                              width={24}
                              height={24}
                              viewBox="0 0 24 24"
                            >
                              <path
                                className="pointer-events-none"
                                d="M3 6l3 18h12l3-18h-18zm19-4v2h-20v-2h5.711c.9 0 1.631-1.099 1.631-2h5.316c0 .901.73 2 1.631 2h5.711z"
                              />
                            </svg>
                          </button>
                        </div>
                      ))}
                    </div>
                  )}
                </>

                <div className="grid md:grid-cols-2 mt-14 gap-x-10">
                  <div className="flex flex-col  sm:gap-y-2.5">
                    <div className="mb-5">
                      <label
                        htmlFor="name"
                        className="block  font-medium text-gray-700"
                      >
                        {lang.className}
                      </label>

                      <input
                        required
                        type="text"
                        name="name"
                        autoComplete="off"
                        value={data?.name}
                        onChange={handleChange}
                        placeholder={lang.placeholderClassName}
                        className="p-3 mt-3 w-full rounded-[10px] border border-gray-400  outline-none  shadow-sm focus:ring-2 ring-[#9d21e6]"
                      />
                    </div>
                    <div className="mb-5">
                      <label
                        htmlFor="description"
                        className="block  font-medium text-gray-700"
                      >
                        {lang.description}
                      </label>

                      <input
                        required
                        autoComplete="off"
                        type="text"
                        id="description"
                        name="description"
                        value={data?.description}
                        onChange={handleChange}
                        placeholder={lang.description}
                        className="p-3 mt-3 w-full rounded-[10px] border border-gray-400  outline-none  shadow-sm focus:ring-2 ring-[#9d21e6]"
                      />
                    </div>

                    <div className="mb-5">
                      <div className="mb-3 flex items-center w-full justify-between  gap-5">
                        <label
                          htmlFor="startRegisterDate"
                          className="block  font-medium text-gray-700"
                        >
                          {lang.startRegisterDate}
                        </label>
                      </div>
                      <Popover
                        open={isCalendarOpen.startReg}
                        onOpenChange={() =>
                          setIsCalendarOpen((prevState) => ({
                            ...prevState,
                            startReg: !prevState.startReg,
                          }))
                        }
                      >
                        <PopoverTrigger asChild>
                          <Button
                            variant={"outline"}
                            className={cn(
                              `${
                                isErrDate.startReg !== ""
                                  ? "border-red-500"
                                  : "border-gray-400"
                              }  w-full  justify-start text-left font-normal rounded-[10px] h-[50px]  text-[16px]`,
                              !data?.startRegisterDate &&
                                "text-muted-foreground",
                            )}
                          >
                            <CalendarIcon className="mr-3 h-4 w-4" />
                            {data?.startRegisterDate ? (
                              format(data?.startRegisterDate, "dd MMMM yyyy")
                            ) : (
                              <span>{lang.pickDate}</span>
                            )}
                          </Button>
                        </PopoverTrigger>
                        <PopoverContent className="w-auto p-0">
                          <Calendar
                            mode="single"
                            selected={data?.startRegisterDate}
                            onSelect={(selectedDate) => {
                              const today = new Date();
                              today.setHours(0, 0, 0, 0);
                              const tanggalMulai = new Date(selectedDate);

                              if (
                                tanggalMulai.getTime() === today.getTime() ||
                                tanggalMulai > today
                              ) {
                                setIsErrDate({
                                  ...isErrDate,
                                  startReg: "",
                                  endReg: "",
                                });
                              } else {
                                setIsErrDate({
                                  ...isErrDate,
                                  startReg: `Tanggal tidak boleh kurang dari Hari Sekarang`,
                                });
                              }
                              setdata({
                                ...data,
                                startRegisterDate: selectedDate,
                                endRegisterDate: "",
                              });
                              setIsCalendarOpen((prevState) => ({
                                ...prevState,
                                startReg: !prevState.startReg,
                              }));
                            }}
                            initialFocus
                          />
                        </PopoverContent>
                      </Popover>
                      {isErrDate.startReg !== "" && (
                        <div className="md:absolute text-red-600 mt-1 text-sm">
                          {isErrDate.startReg}
                        </div>
                      )}
                    </div>
                    <div className="mb-5 relative">
                      <div className="mb-3 flex items-center w-full justify-between  gap-5">
                        <label
                          htmlFor="endRegisterDate"
                          className="block  font-medium text-gray-700"
                        >
                          {lang.endRegisterDate}
                        </label>
                      </div>

                      <Popover
                        open={isCalendarOpen.endReg}
                        onOpenChange={() =>
                          setIsCalendarOpen((prevState) => ({
                            ...prevState,
                            endReg: !prevState.endReg,
                          }))
                        }
                      >
                        <PopoverTrigger asChild>
                          <Button
                            disabled={
                              data.startRegisterDate === ""
                                ? true
                                : isErrDate.startReg !== ""
                                  ? true
                                  : false
                            }
                            variant={"outline"}
                            className={cn(
                              `${
                                isErrDate.endReg !== ""
                                  ? "border-red-500"
                                  : "border-gray-400"
                              }  w-full  justify-start text-left font-normal rounded-[10px] h-[50px]  text-[16px]`,
                              !data?.endRegisterDate && "text-muted-foreground",
                            )}
                          >
                            <CalendarIcon className="mr-3 h-4 w-4" />
                            {data?.endRegisterDate ? (
                              format(data?.endRegisterDate, "dd MMMM yyyy")
                            ) : (
                              <span>{lang.pickDate}</span>
                            )}
                          </Button>
                        </PopoverTrigger>
                        <PopoverContent className="w-auto p-0">
                          <Calendar
                            mode="single"
                            selected={data?.endRegisterDate}
                            onSelect={(selectedDate) => {
                              const tanggalRegister = new Date(
                                data.startRegisterDate,
                              );

                              const tanggalMulai = new Date(selectedDate);
                              const today = new Date();

                              if (
                                (tanggalMulai === today ||
                                  tanggalMulai > today) &&
                                (tanggalMulai > tanggalRegister ||
                                  tanggalMulai.getTime() ===
                                    tanggalRegister.getTime())
                              ) {
                                setIsErrDate({
                                  ...isErrDate,
                                  endReg: "",
                                  startClass: "",
                                });
                              } else {
                                setIsErrDate({
                                  ...isErrDate,
                                  endReg: `Tanggal tidak boleh kurang dari ${formatTanggalIndonesia(
                                    today,
                                  )}`,
                                });
                              }
                              setdata({
                                ...data,
                                endRegisterDate: selectedDate,
                                startClassDate: "",
                              });
                              setIsCalendarOpen((prevState) => ({
                                ...prevState,
                                endReg: !prevState.endReg,
                              }));
                            }}
                            initialFocus
                          />
                        </PopoverContent>
                      </Popover>
                      {isErrDate.endReg !== "" && (
                        <div className="md:absolute text-red-600 mt-1 text-sm">
                          {isErrDate.endReg}
                        </div>
                      )}
                    </div>
                  </div>
                  <div className="flex flex-col gap-y-2.5">
                    <div className="mb-5 relative">
                      <div className="mb-3 flex items-center w-full justify-between  gap-5">
                        <label
                          htmlFor="startClassDate"
                          className="block  font-medium text-gray-700"
                        >
                          {lang.startClassDate}
                        </label>
                      </div>
                      <Popover
                        open={isCalendarOpen.startClass}
                        onOpenChange={() => {
                          setIsCalendarOpen((prevState) => ({
                            ...prevState,
                            startClass: !prevState.startClass,
                          }));
                        }}
                      >
                        <PopoverTrigger asChild>
                          <Button
                            disabled={
                              data.endRegisterDate === ""
                                ? true
                                : isErrDate.endReg !== ""
                                  ? true
                                  : false
                            }
                            variant={"outline"}
                            className={cn(
                              `${
                                isErrDate.startClass !== ""
                                  ? "border-red-500"
                                  : "border-gray-400"
                              } w-full  justify-start text-left font-normal rounded-[10px] h-[50px] text-[16px]`,
                              !data?.startClassDate && "text-muted-foreground",
                            )}
                          >
                            <CalendarIcon className="mr-3 h-4 w-4" />
                            {data?.startClassDate ? (
                              format(data?.startClassDate, "dd MMMM yyyy")
                            ) : (
                              <span>{lang.pickDate}</span>
                            )}
                          </Button>
                        </PopoverTrigger>
                        <PopoverContent className="w-auto p-0">
                          <Calendar
                            mode="single"
                            selected={data?.startClassDate}
                            onSelect={(selectedDate) => {
                              const tanggalRegister = new Date(
                                data.endRegisterDate,
                              );

                              // Tanggal mulai yang akan divalidasi
                              const tanggalMulai = new Date(selectedDate); // Gantilah YOUR_TANGGAL_MULAI_HERE dengan tanggal mulai yang ingin divalidasi

                              // Lakukan validasi
                              if (tanggalMulai > tanggalRegister) {
                                setIsErrDate({
                                  ...isErrDate,
                                  startClass: "",
                                });
                              } else if (
                                tanggalMulai.getTime() ===
                                tanggalRegister.getTime()
                              ) {
                                setIsErrDate({
                                  ...isErrDate,
                                  startClass: "",
                                });
                              } else {
                                setIsErrDate({
                                  ...isErrDate,
                                  startClass: `Tanggal tidak boleh kurang dari ${formatTanggalIndonesia(
                                    data.startRegisterDate,
                                  )}`,
                                });
                              }
                              setdata({
                                ...data,
                                startClassDate: selectedDate,
                              });

                              setIsCalendarOpen((prevState) => ({
                                ...prevState,
                                startClass: !prevState.startClass,
                              }));
                            }}
                            initialFocus
                          />
                        </PopoverContent>
                      </Popover>
                      {isErrDate.startClass !== "" && (
                        <div className="md:absolute text-red-600 mt-1 text-sm">
                          {isErrDate.startClass}
                        </div>
                      )}
                    </div>

                    <div className="mb-5 relative">
                      <label
                        htmlFor="timeLearning"
                        className="block  font-medium text-gray-700"
                      >
                        {lang.learningTime}
                      </label>
                      <div className="mt-2 flex gap-x-3 items-center justify-center">
                        <input
                          required
                          type="time"
                          id="startLearningTime"
                          name="startLearningTime"
                          value={data?.startLearningTime}
                          onChange={handleChange}
                          placeholder="3"
                          className="col-span-2 p-3 w-full rounded-[10px] border border-gray-400  outline-none  shadow-sm focus:ring-2 ring-[#9d21e6]"
                        />
                        <span className="text-center font-bold">-</span>
                        <input
                          required
                          disabled={data?.startLearningTime === ""}
                          type="time"
                          id="endLearningTime"
                          name="endLearningTime"
                          value={data?.endLearningTime}
                          onChange={handleChange}
                          placeholder=""
                          className={` ${
                            isErrDate.endLearn !== ""
                              ? "border-red-500"
                              : "border-gray-400"
                          } col-span-2 p-3 w-full rounded-[10px] border border-gray-400  outline-none  shadow-sm focus:ring-2 ring-[#9d21e6]`}
                        />
                      </div>
                      {isErrDate.endLearn !== "" && (
                        <div className="md:absolute text-red-600 mt-1 text-sm">
                          {isErrDate.endLearn}
                        </div>
                      )}
                    </div>
                    <div className="mb-5">
                      <div className="mb-3 flex items-center w-full justify-between  gap-5">
                        <Label htmlFor="sesi-mode">{lang.session}</Label>
                        <Switch
                          id="sesi-mode"
                          checked={isSesi}
                          onCheckedChange={setSesi}
                        />
                      </div>
                      <input
                        disabled={!isSesi}
                        type="text"
                        id="sesi"
                        min="1"
                        max="14"
                        placeholder="1"
                        value={data?.repetition?.sessionCount} // Bisa juga menggunakan sessions.uasSession tergantung kebutuhan
                        onChange={(e) => handleSessionChange(e.target.value)}
                        className="disabled:bg-slate-100 p-3 w-full rounded-[10px] border border-gray-400  outline-none  shadow-sm focus:ring-2 ring-[#9d21e6]"
                      />
                      <p className="hidden text-sm text-red-500 mt-3.5">
                        {lang.notifSesi}
                      </p>
                    </div>
                    {isSesi && (
                      <>
                        <div className="mb-5">
                          <label
                            htmlFor="maximumCapacity"
                            className="block  font-medium text-gray-700"
                          >
                            {lang.repeatEvery}
                          </label>
                          <Select
                            value={data?.repetition?.repeatEvery}
                            onValueChange={handleValueChange}
                          >
                            <SelectTrigger className="mt-3 w-full p-3 h-[50px] rounded-[10px] border border-gray-400">
                              <SelectValue placeholder={lang.repeatEvery} />
                            </SelectTrigger>
                            <SelectContent>
                              <SelectGroup>
                                <SelectItem value="day">{lang.day}</SelectItem>
                                <SelectItem value="week">
                                  {lang.week}
                                </SelectItem>
                                <SelectItem value="month">
                                  {lang.month}
                                </SelectItem>
                              </SelectGroup>
                            </SelectContent>
                          </Select>
                        </div>
                        {data?.repetition?.repeatEvery === "week" && (
                          <div className="mb-5">
                            <label
                              htmlFor="maximumCapacity"
                              className="block  font-medium text-gray-700"
                            >
                              {lang.day}
                            </label>
                            <ToggleGroup
                              value={data?.repetition?.repeatDays}
                              onValueChange={handleWeekRepeat}
                              className="grid grid-cols-7 mt-3 gap-x-2.5"
                              type="multiple"
                            >
                              <ToggleGroupItem
                                value={1}
                                className="w-8 h-8 bg-slate-200"
                              >
                                M
                              </ToggleGroupItem>
                              <ToggleGroupItem
                                value={2}
                                className="w-8 h-8 bg-slate-200"
                              >
                                S
                              </ToggleGroupItem>
                              <ToggleGroupItem
                                value={3}
                                className="w-8 h-8 bg-slate-200"
                              >
                                S
                              </ToggleGroupItem>
                              <ToggleGroupItem
                                value={4}
                                className="w-8 h-8 bg-slate-200"
                              >
                                R
                              </ToggleGroupItem>
                              <ToggleGroupItem
                                value={5}
                                className="w-8 h-8 bg-slate-200"
                              >
                                K
                              </ToggleGroupItem>
                              <ToggleGroupItem
                                value={6}
                                className="w-8 h-8 bg-slate-200"
                              >
                                J
                              </ToggleGroupItem>
                              <ToggleGroupItem
                                value={7}
                                className="w-8 h-8 bg-slate-200"
                              >
                                S
                              </ToggleGroupItem>
                            </ToggleGroup>
                          </div>
                        )}
                      </>
                    )}
                    <div className="mb-5">
                      <label
                        htmlFor="maximumCapacity"
                        className="block  font-medium text-gray-700"
                      >
                        {lang.maximumCapacity}
                      </label>

                      <input
                        type="text"
                        name="maximumCapacity"
                        value={data?.maximumCapacity}
                        min="1"
                        onChange={handleChange}
                        id="maximumCapacity"
                        placeholder="10"
                        className="p-3 mt-3 w-full rounded-[10px] border border-gray-400  outline-none  shadow-sm focus:ring-2 ring-[#9d21e6]"
                      />
                    </div>
                  </div>
                </div>
              </section>

              <div className="my-5">
                <label
                  htmlFor="UserEmail"
                  className="mb-3 block  font-medium text-gray-700"
                >
                  {lang.cost}
                </label>
                <RadioGroup defaultValue="free">
                  <div>
                    <RadioGroupItem
                      value="free"
                      id="free"
                      onClick={() => {
                        setdata({
                          ...data,
                          isPaidClass: false,
                          registrationFee: null,
                          isPrivateClass: false,
                          pin: null,
                        });
                      }}
                      className="peer sr-only"
                    />
                    <Label
                      htmlFor="free"
                      className="flex flex-col  justify-between rounded-[10px] border-2 border-muted cursor-pointer p-4 hover:bg-purple-50  peer-data-[state=checked]:border-[#9d21e6] [&:has([data-state=checked])]:border-[#9d21e6]"
                    >
                      {lang.free}
                    </Label>
                  </div>
                  <div>
                    <RadioGroupItem
                      value="paid"
                      id="paid"
                      onClick={() => {
                        setdata({
                          ...data,
                          isPaidClass: true,
                          registrationFee: 0,
                        });
                      }}
                      className="peer sr-only"
                    />
                    <Label
                      htmlFor="paid"
                      className="flex flex-col  justify-between rounded-[10px] border-2 border-muted cursor-pointer p-4 hover:bg-purple-50  peer-data-[state=checked]:border-[#9d21e6] [&:has([data-state=checked])]:border-[#9d21e6]"
                    >
                      {lang.paid}
                    </Label>
                  </div>
                </RadioGroup>
              </div>
              {data?.isPaidClass && (
                <div className="mb-5">
                  <label
                    htmlFor="maximumCapacity"
                    className="block  font-medium text-gray-700"
                  >
                    {lang.nominal_fee}
                  </label>

                  <input
                    type="text"
                    value={formattedValue}
                    onChange={handleInputChange}
                    placeholder="200000"
                    className="p-3 mt-3 w-full rounded-[10px] border border-gray-400  outline-none  shadow-sm focus:ring-2 ring-[#9d21e6]"
                  />
                </div>
              )}
              <div className="mb-5">
                <label
                  htmlFor="UserEmail"
                  className="mb-3 block  font-medium text-gray-700"
                >
                  {lang.type}
                </label>
                <RadioGroup defaultValue="general">
                  <div>
                    <RadioGroupItem
                      value="general"
                      id="general"
                      onClick={() => {
                        setdata({
                          ...data,
                          isPrivateClass: false,
                          pin: null,
                        });
                      }}
                      className="peer sr-only"
                    />
                    <Label
                      htmlFor="general"
                      className="flex flex-col  justify-between rounded-[10px] border-2 border-muted cursor-pointer p-4 hover:bg-purple-50  peer-data-[state=checked]:border-[#9d21e6] [&:has([data-state=checked])]:border-[#9d21e6]"
                    >
                      {lang.public}
                    </Label>
                  </div>
                  {!data?.isPaidClass && (
                    <div>
                      <RadioGroupItem
                        value="private"
                        id="privat"
                        onClick={() => {
                          setDataPin(12345678);
                          setdata({
                            ...data,
                            isPrivateClass: true,
                            pin: "12345678",
                          });
                        }}
                        className="peer sr-only"
                      />
                      <Label
                        htmlFor="privat"
                        className="flex flex-col  justify-between rounded-[10px] border-2 border-muted cursor-pointer p-4 hover:bg-purple-50  peer-data-[state=checked]:border-[#9d21e6] [&:has([data-state=checked])]:border-[#9d21e6]"
                      >
                        {lang.private}
                      </Label>
                    </div>
                  )}
                </RadioGroup>
              </div>
              {data?.isPrivateClass && (
                <div className="mb-5">
                  <label
                    htmlFor="pin"
                    className="block  font-medium text-gray-700"
                  >
                    PIN
                  </label>

                  <input
                    type="number"
                    id="pin"
                    name="pin"
                    value={dataPin}
                    onChange={(e) => {
                      setDataPin(e.target.value.slice(0, 8));
                      setdata({
                        ...data,
                        pin: e.target.value.toString().slice(0, 8),
                      });
                    }}
                    placeholder="1234567"
                    className="p-3 mt-3 w-full rounded-[10px] border border-gray-400  outline-none  shadow-sm focus:ring-2 ring-[#9d21e6]"
                  />
                </div>
              )}

              <div className="mb-5">
                <div className="mb-3 flex items-center w-full justify-between  gap-5">
                  <Label htmlFor="attend-mode">{lang.attendance}</Label>
                  <Switch
                    id="attend-mode"
                    checked={data?.isAttendanceRequired}
                    onCheckedChange={(selected) => {
                      if (selected) {
                        setdata({
                          ...data,
                          isAttendanceRequired: selected,
                          startAttendanceTime: "",
                          endAttendanceTime: "",
                        });
                      } else {
                        setdata({
                          ...data,
                          isAttendanceRequired: selected,
                          startAttendanceTime: null,
                          endAttendanceTime: null,
                        });
                      }
                    }}
                  />
                </div>
                {data?.isAttendanceRequired && (
                  <div className="relative">
                    <div className="mt-2 flex gap-x-3 items-center justify-center">
                      <input
                        type="time"
                        id="startAttendanceTime"
                        name="startAttendanceTime"
                        value={data?.startAttendanceTime}
                        onChange={handleChange}
                        placeholder="3"
                        className="col-span-2 p-3 w-full rounded-[10px] border border-gray-400  outline-none  shadow-sm focus:ring-2 ring-[#9d21e6]"
                      />
                      <span className="text-center font-bold">-</span>
                      <input
                        type="time"
                        id="endAttendanceTime"
                        name="endAttendanceTime"
                        value={data?.endAttendanceTime}
                        onChange={handleChange}
                        placeholder=""
                        className={` ${
                          isErrDate.endAttend !== ""
                            ? "border-red-500"
                            : "border-gray-400"
                        } col-span-2 p-3 w-full rounded-[10px] border border-gray-400  outline-none  shadow-sm focus:ring-2 ring-[#9d21e6]`}
                      />
                    </div>

                    {isErrDate.endAttend !== "" && (
                      <div className="md:absolute text-red-600 mt-1 text-sm">
                        {isErrDate.endAttend}
                      </div>
                    )}
                  </div>
                )}
              </div>
              <div className="w-full flex justify-center items-center my-4 mt-6">
                <div className="w-full">
                  <button
                    type="submit"
                    className="p-2.5 w-32  bg-[#9d21e6] text-white shadow-xl rounded-[10px] font-semibold"
                  >
                    {lang.submit}
                  </button>
                </div>
              </div>
            </form>
      </div>
    </section>
  );
};

export default Home;

export async function getServerSideProps(context: any) {
  try {
    const session = await getSession(context);
    if (session) {
      return {
        props: { user: session.user },
      };
    } else {
      return {
        redirect: {
          destination: `/login`,
          permanent: false,
        },
      };
    }
  } catch (error) {
    console.error("Kesalahan:", error);
    return {
      props: {
        error: "Terjadi kesalahan saat memuat sesi pengguna",
      },
    };
  }
}
