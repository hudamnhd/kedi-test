import NextAuth from "next-auth";
import CredentialsProvider from "next-auth/providers/credentials";
import axios from "axios";
import GoogleProvider from "next-auth/providers/google";
import jwt from "jsonwebtoken";

const fetchDataFromAPI = async (access_token: string) => {
  try {
    const response = await axios.post(
      process.env.API_URL + "/api/auth/google/verify",
      {
        accessToken: access_token,
      },
    );
    return response.data;
  } catch (error) {
    console.error("Gagal mengambil data dari API:");
    return null;
  }
};

const checkTokenNotExp = async (access_token: string) => {
  const token = access_token;

  const decodedToken = jwt.decode(token);

  if (decodedToken) {
    const expirationTime = decodedToken.exp;

    const currentTime = Math.floor(Date.now() / 1000);
    // const expirationDate = new Date(expirationTime * 1000);

    if (expirationTime < currentTime) {
      // console.log("Token is expired");
      return false;
    } else {
      // console.log("Token is not expired");
      // console.log("Token will expire on:", expirationDate.toLocaleString());
      return true;
    }
  } else {
    console.error("Failed to decode the token");
    return false;
  }
};

export default NextAuth({
  pages: {
    signIn: "/login",
  },
  providers: [
    CredentialsProvider({
      id: "credentials",
      name: "Credentials",
      //@ts-ignore
      async authorize(credentials: any) {
        const data = {
          email: credentials.email,
          password: credentials.password,
        };

        const config = {
          headers: {
            "Content-Type": "application/json",
          },
        };

        try {
          const response = await axios.post(
            // process.env.API_URL + "/api/auth/login",
             process.env.API_URL + "/api/auth/login/v2",
            JSON.stringify(data),
            config,
          );

          if (response.status >= 200 && response.status < 300) {
                const data = response.data.data;

                let user = {
                // response ini dari get /api/auth/login
                    id:           data.id,
                    uuid:         data.uuid,
                    username:     data.username,
                    fullName:     data.fullName,
                    email:        data.email,
                    photo:        data.photo,
                    accessToken:  data.access_token,
                    publishKey:   data.publishKey,
                    subscribeKey: data.subscribeKey,

                    // for api v1
                    // uuid:         data.pubnub.uuid,
                    // publishKey:   data.pubnub.publishKey,
                    // subscribeKey: data.pubnub.subscribeKey,


                };

            return user;
          }
        } catch (error) {
          if (axios.isAxiosError(error)) {
            const axiosError: any = error;
            if (axiosError.response) {
              throw Error(axiosError?.response?.data?.meta?.message);
            }
          }
        }
      },
    }),
    GoogleProvider({
      clientId: process.env.GOOGLE_CLIENT_ID ?? "",
      clientSecret: process.env.GOOGLE_CLIENT_SECRET ?? "",
      httpOptions: {
        timeout: 10000,
      },
    }),
  ],
  callbacks: {
    async jwt({ token, user, account, trigger, session }) {
      if (account?.access_token) {
        const additionalData = await fetchDataFromAPI(account.access_token);
        // if (additionalData.data.access_token === null) return null;
        if (additionalData) {
          token.accessToken = additionalData.data.access_token;
          token.publishKey = additionalData?.data?.pubnub?.publishKey;
          token.subscribeKey = additionalData?.data?.pubnub?.subscribeKey;
          // token.additionalData = additionalData;
        }
      }

    if (trigger === "update" && session?.photo) {
        // Note, that `session` can be any arbitrary object, remember to validate it!
         token.email    = session.email ?? "";
         token.fullName = session.fullName ?? "";
         token.id       = session.id ?? "";
         token.uuid     = session.uuid ?? "";
         token.photo    = session.photo ?? "";
      }

      return { ...token, ...user };
    },
    async session({ session, token }) {
      const isNotExpired = await checkTokenNotExp(token.accessToken);
      if (!isNotExpired) return null;
      session.user = token as any;
      return session;
    },
  },
});
