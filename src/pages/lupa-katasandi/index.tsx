import { NextPage } from "next";
import { LoadingSpinner } from "@/components/ui/loading";
import Image from "next/image";
import toast from "react-hot-toast";
import { useState } from "react";
import { signIn } from "next-auth/react";
import { logoLogin } from "@/assets";
import { useMutation } from "@tanstack/react-query";
import axios from "axios";

const SignIn: NextPage = (): JSX.Element => {
  const [userInfo, setUserInfo] = useState({ email: "" });
  const [toggle, setToggle] = useState(false);

  const response = useMutation(async (email: string) => {
    if (email.length === 0) return toast.error("Email field empty");
    try {
      const res = await axios.post(
        process.env.API_BE + `/api/auth/request-reset-password`,
        {
          email: String(email),
        },
      );
      toast.success(res.data.data.message);
      setToggle(true);
      setUserInfo({ ...userInfo, email: "" })
      return res.data.data;
    } catch (error) {
      console.error(error);
      toast.error(error.response.data.data[0]);
      throw error;
    }
  });

  const Modal = () => {
    return (
      <>
        {/* component */}
        <div
          className="min-w-screen h-screen animated fadeIn faster  fixed  left-0 top-0 flex justify-center items-center inset-0 z-50 outline-none focus:outline-none"
          id="modal-id"
        >
          <div className="absolute bg-black/50 opacity-80 inset-0 z-0" />
          <div className="w-full  max-w-lg p-5 relative mx-auto my-auto rounded-xl shadow-lg  bg-white ">
            {/*content*/}
            <div className="">
              {/*body*/}
              <div className="text-center p-5 flex flex-col items-center justify-center">
                <svg
                  xmlns="http://www.w3.org/2000/svg"
                  fill="none"
                  viewBox="0 0 24 24"
                  strokeWidth={1.7}
                  stroke="currentColor"
                  className="w-16 h-16 text-[#9D21E6]"
                >
                  <path
                    strokeLinecap="round"
                    strokeLinejoin="round"
                    d="M9 12.75L11.25 15 15 9.75M21 12a9 9 0 11-18 0 9 9 0 0118 0z"
                  />
                </svg>
                <h2 className="text-xl font-bold pt-4 pb-2 text-[#9D21E6]">
                  {response?.data?.message}
                </h2>
              </div>
              {/*footer*/}
              <div className="max-w-[150px] sm:text-base text-sm flex flex-col gap-y-2.5 justify-center mt-2  mx-auto">
                <button
                  className="flex justify-center cursor-pointer bg-[#9D21E6] text-white rounded-[10px] px-4 py-2.5 font-semibold w-full border border-gray-400 shadow-lg"
                  type="button"
                  onClick={() => setToggle(false)}
                >
                  OK
                </button>
              </div>
            </div>
          </div>
        </div>
      </>
    );
  };

  return (
    <div className="relative">
      {toggle && <Modal />}
      <div className="h-screen flex flex-col items-center px-4 bg-[#9D21E6]">
        <h1 className="text-[#9D21E6] font-medium text-3xl mb-4 mt-12 ">
          Lupa Kata Sandi
        </h1>
        <div className="mx-10 flex flex-col items-center min-h-[70%] bg-white shadow-xl rounded-xl mt-4  px-6 py-10 max-w-lg w-full">
          <Image
            src={logoLogin}
            alt="Logo Kelas Digital"
            className="max-w-[130px]  min-h-[100px] mx-auto"
          />
          <h1 className="text-xl md:text-[24px] text-center mb-4 mt-5 font-medium">
            Silakan isi kolom dibawah dengan Email Anda yang terdaftar di Kelas
            Digital
          </h1>
          <p className="text-[#686464] text-center">
            Kita akan kirimkan Anda link untuk atur ulang kata sandi
          </p>
          <div className="mt-6 w-full">
            <div className="mb-3 sm:mb-5">
              <label
                htmlFor="UserEmail"
                className="block text-xs sm:text-sm  font-medium text-gray-700"
              >
                Email
              </label>

              <input
                value={userInfo.email}
                onChange={({ target }) =>
                  setUserInfo({ ...userInfo, email: target.value })
                }
                placeholder="Masukan email anda"
                type="email"
                disabled={response.isLoading}
                className="disabled:bg-gray-200 mt-1.5 sm:mt-3 px-4 py-1.5 sm:py-2.5 w-full rounded-[10px] border border-gray-400 outline-none focus:ring-2 focus:border-[#9D21E6] ring-[#9D21E6] shadow-sm sm:text-sm"
              />
            </div>

            <div className="sm:text-base text-sm flex flex-col gap-y-2.5 justify-center mt-4 sm:mt-6  mx-auto">
              <button
                className="flex justify-center cursor-pointer bg-[#9D21E6] text-white rounded-[10px] px-4 py-2.5 font-semibold w-full border border-gray-400 shadow-lg"
                type="button"
                onClick={() => response.mutate(userInfo.email)}
              >
                {response.isLoading ? (
                  <LoadingSpinner stroke={`#FFF`} size={24} />
                ) : (
                  "Kirim"
                )}
              </button>
            </div>
          </div>
        </div>
      </div>
    </div>
  );
};

export default SignIn;
