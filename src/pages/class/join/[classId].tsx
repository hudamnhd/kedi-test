import { toast } from "react-hot-toast";
import axios from "axios";
import Image from "next/image";
import Link from "next/link";
import React, { useState, useEffect  } from "react";
import { Navbar } from "@/components/navbar/navbar";
import { getSession } from "next-auth/react";
import { trans } from "@/lang";
import { useRouter } from "next/router";
import { useMutation, useQuery, useQueryClient } from "@tanstack/react-query";
import { LoadingSpinner } from "@/components/ui/loading";
import { downloadapp, appstore, appgalery, appgoogle } from "@/assets";
import { Threedots } from "@/assets/icon";

import {
  Dialog,
  DialogContent,
  DialogHeader,
  DialogTitle,
  DialogTrigger,
} from "@/components/ui/dialog";
import {
  AlertDialog,
  AlertDialogAction,
  AlertDialogCancel,
  AlertDialogContent,
  AlertDialogDescription,
  AlertDialogFooter,
  AlertDialogHeader,
  AlertDialogTitle,
  AlertDialogTrigger,
} from "@/components/ui/alert-dialog";

const ClassPage = ({ user, res, join, notifFinance : notif, classId }: any) => {
  const router = useRouter();
  const locale: "en" | "id" = router.locale as "en" | "id";
  const lang = trans[locale].class;
  const dataInfoKelas = res?.data;
  const sessionToken = user?.accessToken;
  const notifFinance = notif?.data?.filter((d) => {
      const invoiceItemsFiltered = d?.data?.invoiceItems?.filter((dt) => dt?.productReference?.relId == classId);
      const statusCondition      = d?.data?.status === "PENDING";
      const paidsFiltered        = d?.data?.paids?.filter((dx) => dx?.status === "PENDING");
      return invoiceItemsFiltered.length > 0 && statusCondition && paidsFiltered.length > 0;

  });

  const { data: listPayment, isLoading: isLoadingListPay } = useQuery(
    ["paymentMethod"],
    async () => {
      if (!sessionToken) return null;

      const response = await axios.get(
        process.env.API_BE + "/api/finance/payment-method?limit=8&page=1",
        {
          headers: {
            Authorization: `Bearer ${sessionToken}`,
          },
        },
      );
      if (response.status !== 200) {
        throw new Error("Gagal mengambil data");
      }

      return response.data.data;
    },
    {
      onError: (error) =>
        toast.error(`Error: ${error.response.data.meta.message}`),
    },
  );

  return (
    <div className="bg-[#FAFAFA]">
      <Navbar user={user} />
      <InfoClass
        user={user}
        dataInfoKelas={dataInfoKelas}
        listPayment={listPayment}
        lang={lang}
        join={join}
        notifFinance={notifFinance}
        notif={notif}
      />
    </div>
  );
};

const TitleMenu = ({ title, classId }) => {
  return (
    <div className="relative sm:mt-3 mb-5 sm:mb-8 flex items-center justify-between pt-3 w-full h-[40px]">
      <Link href={`/class/${classId}`} className="flex items-center flex-none">
        <div className="flex-none text-[#9D21E6] bg-[#D7B9FD]   flex justify-center items-center rounded-full w-8 h-8 lg:w-10 lg:h-10 p-1">
          <svg
            xmlns="http://www.w3.org/2000/svg"
            fill="none"
            viewBox="0 0 24 24"
            strokeWidth={2.5}
            stroke="currentColor"
            height={18}
            width={18}
          >
            <path
              strokeLinecap="round"
              strokeLinejoin="round"
              d="M15.75 19.5 8.25 12l7.5-7.5"
            />
          </svg>
        </div>
        <h1 className="ml-4 lg:ml-4 text-xl md:text-2xl font-bold text-[#9D21E6] self-center ">
          {title}
        </h1>
      </Link>
    </div>
  );
};

function formatCurrency(data) {
  if (
    isNaN(data) &&
    data !== null &&
    data !== undefined &&
    data !== "" &&
    data !== "0"
  ) {
    data = parseFloat(data);
  }
  const amount =
    data === null || data === undefined || data === 0 || data === "0"
      ? 0
      : data;

  const formattedAmount = new Intl.NumberFormat("id-ID", {
    style: "currency",
    currency: "IDR",
    minimumFractionDigits: 2,
  }).format(amount);

  return formattedAmount;
}

const InfoClass = ({ user, dataInfoKelas, listPayment, isLoadingListPay, lang, notifFinance, notif }) => {
  const sessionToken                              = user?.accessToken;
  const router                                    = useRouter();
  const [isAlertOpen, setIsAlertOpen]             = useState(false);
  const [succesReqPay, setSuccesReqPay]           = useState(false);
  const [detailTransaction, setDetailTransaction] = useState(null);
  const [succesCancelPay, setSuccesCancelPay]     = useState(null);
  const [modalPayment, setModalPayment]           = useState(false);
  const [payloadReview, setPayloadReview]         = useState({
    invoiceIdentifier: "",
    paymentMethodId: 0,
  });
  const [isLoading, setIsLoading] = useState({
    cekStatus: false,
    cancelTransaction: false,
    previewPrice: false,
  });

  interface ApiResponse {
    data: {
      message: string;
    };
  }

  const queryClient = useQueryClient();

  interface PostResponse extends ApiResponse {}

  const payPaidClass = useMutation(
    async (data): Promise<PostResponse> => {
      const { invoiceIdentifier, paymentMethodId } = data;

      if (!sessionToken) throw new Error("Missing session token");

      try {
        const response = await axios.post<PostResponse>(
          process.env.API_BE + `/api/finance/pay`,
          {
            invoiceIdentifier,
            installment: null,
            paymentMethodId,
          },
          {
            headers: {
              Authorization: `Bearer ${sessionToken}`,
            },
          },
        );
        toast.success(`Request Payment`);

        if (response?.data?.data?.url) {
          setSuccesReqPay(true);
          window.open(response?.data?.data?.url, "_blank");
        }
        console.log("response:", response.data);
        return response.data;
      } catch (error) {
        console.error(error);
        throw error;
      }
    },
    {
      onError: (error) =>
        toast.error(`Error: ${error.response.data.meta.message}`),
    },
  );

  const cekStatusTransaksi = async (id: number) => {
    if (!id) return toast.error("Gagal mengambil data Transaksi.");
    setIsLoading((prevState) => ({ ...prevState, cekStatus: true }));
    try {
      const response = await axios.get(
        process.env.API_BE + `/api/finance/invoice/detail/${id}`,
        {
          headers: {
            Authorization: `Bearer ${sessionToken}`,
          },
        },
      );
      setDetailTransaction(response.data.data);
      console.log(response.data.data);
      queryClient.invalidateQueries(["notifFinance"]);
      setTimeout(() => {
        if ( response.data.data.status === "SETTLEMENT" ) return router.push("/")
      }, 1000);
    } catch (error) {
      toast.error("Gagal mengambil data member.", error);
    } finally {
      setIsLoading((prevState) => ({ ...prevState, cekStatus: false }));
    }
  };

  const cancelStatusTransaction = async () => {

      if (detailTransaction && detailTransaction?.financePayList) {
        const filteredNotifStatus = detailTransaction?.financePayList?.filter((item) => {
          if ( item.status === "PENDING") {
            return item
          }
          return false;
        });

        if (filteredNotifStatus && filteredNotifStatus.length > 0) {
            // Buat array untuk menyimpan hasil panggilan API
                const transactionIdentifier = filteredNotifStatus[0]?.transactionIdentifier;
                setIsLoading((prevState) => ({ ...prevState, cancelTransaction: true }));
                try {
                    // Lakukan panggilan API untuk setiap transactionIdentifier
                    const response = await axios.post(
                        process.env.API_BE + `/api/finance/pay/cancel/${transactionIdentifier}`,
                        {},
                        {
                            headers: {
                                Authorization: `Bearer ${sessionToken}`,
                            },
                        }
                    );
                    // Jika panggilan API berhasil, kembalikan hasilnya

                    const successful200 = response?.data?.data?.data?.transaction_status;
                    setSuccesCancelPay(successful200)
                    const message_success = response?.data?.data?.data?.status_message;
                    toast.success(message_success ?? "Succes cancel transaction")
                    setSuccesReqPay(false)
                    setDetailTransaction(null)
                    queryClient.invalidateQueries(["notifFinance"]);
                    router.push("/")
                } catch (error) {
                    // Jika terjadi kesalahan, tangani di sini
                    console.error(`Error cancelling transaction ${transactionIdentifier}:`, error);
                    // Kembalikan objek yang menandakan bahwa panggilan API gagal
                } finally {
                    setIsLoading((prevState) => ({ ...prevState, cancelTransaction: false }));
                }

        }
      } else if (notifFinance) {

        const filteredNotifFinance = notifFinance.filter((item) => {
          if ( item && item.data && item.data.invoiceItems && item.data.invoiceItems.length > 0) {
            const classId = dataInfoKelas.id;
            const relId = item?.data?.invoiceItems[0]?.productReference?.relId;
            return classId == relId;
          }
          return false;
        });

        const filteredNotifStatus = filteredNotifFinance[0]?.data?.paids.filter((item) => {
          if ( item.status === "PENDING") {
            return item
          }
          return false;
        });

        if (filteredNotifStatus && filteredNotifStatus.length > 0) {
            // Buat array untuk menyimpan hasil panggilan API
                const transactionIdentifier = filteredNotifStatus[0]?.transactionIdentifier;
                setIsLoading((prevState) => ({ ...prevState, cancelTransaction: true }));
                try {
                    // Lakukan panggilan API untuk setiap transactionIdentifier
                    const response = await axios.post(
                        process.env.API_BE + `/api/finance/pay/cancel/${transactionIdentifier}`,
                        {},
                        {
                            headers: {
                                Authorization: `Bearer ${sessionToken}`,
                            },
                        }
                    );
                    // Jika panggilan API berhasil, kembalikan hasilnya

                    const successful200 = response?.data?.data?.data?.transaction_status;
                    setSuccesCancelPay(successful200)
                    const message_success = response?.data?.data?.data?.status_message;
                    toast.success(message_success ?? "Succes cancel transaction")
                    setSuccesReqPay(false)
                    setDetailTransaction(null)
                    queryClient.invalidateQueries(["notifFinance"]);
                    router.push("/")
                } catch (error) {
                    // Jika terjadi kesalahan, tangani di sini
                    console.error(`Error cancelling transaction ${transactionIdentifier}:`, error);
                    // Kembalikan objek yang menandakan bahwa panggilan API gagal
                } finally {
                    setIsLoading((prevState) => ({ ...prevState, cancelTransaction: false }));
                }


            // const successfulResults = results.filter(result => result.success);
            // const successful200 = successfulResults.filter((d) => d.data.data.data.transaction_status);

        }
      }

  };

  const [checkedItems, setCheckedItems] = useState([]);
  const [dataTemp, setDataTemp] = useState({
    platformFee: 0,
    serviceFee: 0,
    total: 0,
  });
  const [isLoadPrice, setIsLoadPrice] = useState(false);
  const [errorLoadPrice, setErrorIsLoadPrice] = useState(null);

  const isChecked = (itemId) => checkedItems.includes(itemId);

  const handleCheckboxChange = (itemId) => {
    setCheckedItems((prevCheckedItems) => {
      let updatedCheckedItems = [];

      if (!prevCheckedItems.includes(itemId)) {
        updatedCheckedItems = [itemId];
      } else {
        updatedCheckedItems = prevCheckedItems.filter(
          (item) => item !== itemId,
        );
      }

      if (notif) {
        const filteredNotifFinance = notif?.data?.filter((item) => {
          if (
            item &&
            item.data &&
            item.data.invoiceItems &&
            item.data.invoiceItems.length > 0
          ) {
            const classId = dataInfoKelas.id;
            const relId = item?.data?.invoiceItems[0]?.productReference?.relId;
            return classId == relId && item?.status === "NEW" || item?.status === "BARU" || item?.status === "PENDING";
          }
          return false;
        });

        const headers = {
          Authorization: `Bearer ${sessionToken}`,
        };

        // Definisikan payload
        const payload_review = {
          invoiceIdentifier: filteredNotifFinance[0]?.data?.invoiceIdentifier,
          paymentMethodId: parseInt(updatedCheckedItems[0]?.id),
        };

        if (payload_review?.invoiceIdentifier === undefined) router.push("/class/" + dataInfoKelas?.id )
        setPayloadReview(payload_review);
        setIsLoadPrice(true);
        axios
          .post(
            process.env.API_BE + "/api/finance/pay/preview",
            payload_review,
            { headers },
          )
          .then((response) => {
            const data = response?.data?.data;
            setDataTemp({
              platformFee: data?.detail[2]?.price ?? 0,
              serviceFee: data?.detail[1]?.price ?? 0,
              total: data?.total ?? 0,
            });
            setIsLoadPrice(false);
          })
          .catch((error) => {
            setDataTemp({
              platformFee: 0,
              serviceFee: 0,
              total: 0,
            });
            setModalPayment(false);
            setCheckedItems([]);
            setErrorIsLoadPrice(error);
            setIsLoadPrice(false);
            return null;
          });
      } else {
        console.log("notifFinance is null or undefined.");
      }

      setModalPayment(false);
      return updatedCheckedItems;
    });
  };

  useEffect(() => {
if (notifFinance && listPayment?.items) {
        const payload_review = {
          invoiceIdentifier: notifFinance[0]?.data?.invoiceIdentifier,
          paymentMethodId: parseInt(notifFinance[0]?.data?.paids[0]?.paymentMethodId),
        };
        const filterPaymentMethod = listPayment?.items?.filter((d) => d.id == notifFinance[0]?.data?.paids[0]?.paymentMethodId)
        if ( filterPaymentMethod?.length > 0 ) {
            handleCheckboxChange(filterPaymentMethod[0])
            setSuccesReqPay(true);
            setDetailTransaction(notifFinance[0]);

        }
    }
  }, [notifFinance])
  

  return (
    <>
      <Dialog
        open={isAlertOpen}
        onOpenChange={setIsAlertOpen}
        onOpenAutoFocus={(e) => e.preventDefault()}
      >
        <DialogContent>
          <DialogHeader>
            <DialogTitle className="text-center  text-[22px] sm:text-2xl font-semibold text-[#9D21E6] mx-auto max-w-[320px]">
              Silahkan Join Melalui Aplikasi KELAS DIGITAL
            </DialogTitle>
          </DialogHeader>
          <div>
            <Image
              src={downloadapp}
              alt="barcode download"
              className="w-auto h-auto max-w-[250px] sm:max-w-[300px] max-h-[250px] sm:max-h-[300px] mx-auto"
            />
            <div className="mt-7 [@media(max-width:400px)]:flex-wrap flex gap-3.5 items-center justify-center">
              {[
                {
                  src: appgoogle,
                  alt: "Google Play",
                  href: "https://play.google.com/store/apps/details?id=id.kelasdigital.lite",
                },
                {
                  src: appstore,
                  alt: "App Store",
                  href: "https://apps.apple.com/id/app/kelas-digital-lite/id1611032222",
                },
                {
                  src: appgalery,
                  alt: "App Gallery",
                  href: "https://appgallery.huawei.com/app/C106887823",
                },
              ].map((image, index) => (
                <Link
                  key={index}
                  href={image.href}
                  target="_blank"
                  rel="noopener noreferrer"
                >
                  <Image
                    src={image.src}
                    className={`w-auto h-[50px]`}
                    alt={image.alt}
                  />
                </Link>
              ))}
            </div>
          </div>
        </DialogContent>
      </Dialog>

      <div className="pt-[4.4rem] sm:pt-[5.2rem] pb-[5rem] lg:pb-0 pb-[6rem] sm:pb-[7rem] className flex flex-col min-h-screen w-fit w-full md:max-w-screen-md  mx-auto px-3 sm:px-5">
        <div className=" pr-2">
          <TitleMenu title={`Ikut Kelas Berbayar`} classId={dataInfoKelas.id} />
          <div className="text-sm px-3 py-5 sm:p-5 bg-white rounded-xl border shadow-md">
            <div className="mb-4">
              <div className="mb-1 text-[16px] font-semibold text-gray-600">
                Kelas
              </div>
              <div className="flex items-start gap-x-5 border border-gray-300 p-4 rounded-lg">
                <div className="relative w-16 h-16 shadow-md border rounded-lg">
                  <Image
                    width={60}
                    height={60}
                    src={`/static/images/logokedi.svg`}
                    className="absolute inset-0 h-full w-full object-cover rounded-xl shadow-md p-1"
                    alt=""
                  />
                </div>
                <div className="flex flex-col flex-1 gap-0.5">
                  <div className="text-[14px] line-clamp-1">
                    {dataInfoKelas?.lecturer?.fullName}
                  </div>
                  <div className="text-[14px] font-semibold line-clamp-1">
                    {dataInfoKelas?.name}
                  </div>
                  <div className="text-[14px] font-semibold line-clamp-1 text-[#9D21E6]">
                    {formatCurrency(dataInfoKelas?.registrationFee)}
                  </div>
                </div>
              </div>
            </div>
            <div className="mb-4">
              <div className="mb-1 text-[16px] font-semibold text-gray-600">
                Metode Pembayaran
              </div>
              <Dialog
                open={modalPayment}
                onOpenChange={() => {
                    if (succesReqPay) return;
                    setModalPayment((prevState) => (!prevState))}
                } 
                onOpenAutoFocus={(e) => e.preventDefault()}
              >
                <DialogTrigger  asChild>
                  <div className={` flex items-start gap-x-5 border border-gray-300 px-4 py-2.5 rounded-lg cursor-pointer ${succesReqPay ? "bg-slate-100" : "bg-white"} `}>
                    {isLoadPrice ? (
                            <Threedots
                              width={28}
                              height={20}
                              strokeWidth={2}
                              fill="#9D21E6"
                            />
                    ) : checkedItems.length === 0 ? (
                      "Pilih Metode Pembayaran"
                    ) : (
                      <div
                        className={` flex items-center gap-x-2.5 flex-none relative bg-white  mr-2 rounded-xl cursor-pointer`}
                      >
                        {checkedItems[0]?.logo ? (
                          <Image
                            src={checkedItems[0]?.logo ?? `/static/images/logokedi.svg`}
                            className="w-[40px] sm:w-[60px] max-h-[21px] sm:max-h-[25px]"
                            width={40}
                            height={40}
                            alt="Default image class"
                          />
                        ) : (
                        <Kedipay className="h-7 w-7"/>
                        )}
                        <div>
                          <div className="font-medium">
                            {" "}
                            {checkedItems[0]?.name}{" "}
                          </div>
                        </div>
                      </div>
                    )}
                  </div>
                </DialogTrigger>
                <DialogContent className="max-w-4xl">
                  <DialogHeader>
                    <DialogTitle className="text-center text-[22px] sm:text-2xl font-semibold text-[#9D21E6] mx-auto max-w-[320px]">
                      Pilih pembayaran
                    </DialogTitle>
                  </DialogHeader>
                  <div className="w-fit mx-auto grid md:grid-cols-2 gap-5 mb-4 mt-6 pb-2.5 max-h-[80vh] overflow-y-auto">
                  {isLoadingListPay && ( <div className="flex items-center justify-center h-[30vh]"> <LoadingSpinner stroke={`#9D21E6`} size={50} />  </div>)}
                    {listPayment &&
                      listPayment.items.map((d) => (
                        <label
                          key={d?.id}
                          className={` border-4 flex items-center gap-x-2 flex-none relative p-3 bg-white ${isChecked(d) ? "border-purple-500 " : "border-transparent shadow-md "} mr-2 rounded-xl cursor-pointer`}
                        >
                        {d?.logo ? (
                          <Image
                            src={d?.logo ?? `/static/images/logokedi.svg`}
                            className="w-[90px] sm:w-[120px] sm:h-auto"
                            width={120}
                            height={60}
                            alt="Default image class"
                          />
                        ) : (
                        <Kedipay />
                        )}
                          <div>
                            <div className="font-semibold"> {d?.name} </div>
                            <div className="text-[#9D9797] text-xs">
                              {" "}
                              {d?.description}{" "}
                            </div>
                          </div>
                          <input
                            type="checkbox"
                            checked={isChecked(d)}
                            onChange={() => handleCheckboxChange(d)}
                            className="hidden"
                          />
                        </label>
                      ))}
                  </div>
                </DialogContent>
              </Dialog>
            </div>
            <div className="mb-4">
              <div className="mb-1 text-[16px] font-semibold text-gray-600">
                Kelas
              </div>
              <div className="flex items-start gap-x-5 border border-gray-300 px-4 py-2.5 rounded-lg">
                <table className="w-full">
                  <tbody>
                    <tr className="h-8">
                      <td className="text-left text-gray-700">Subtotal</td>
                      <td className="text-right text-gray-700">
                        {formatCurrency(dataInfoKelas?.registrationFee)}
                      </td>
                    </tr>
                    <tr className="h-8">
                      <td className="text-left text-gray-700">Biaya Layanan</td>
                      <td className="text-right text-gray-700">
                        {isLoadPrice ? (
                          <div className="flex justify-end">
                            {" "}
                            <Threedots
                              width={28}
                              height={28}
                              strokeWidth={2}
                              fill="#9D21E6"
                            />{" "}
                          </div>
                        ) : (
                          formatCurrency(dataTemp?.platformFee)
                        )}
                      </td>
                    </tr>
                    <tr className="h-8">
                      <td className="text-left text-gray-700">Jasa Aplikasi</td>
                      <td className="text-right text-gray-700">
                        {isLoadPrice ? (
                          <div className="flex justify-end">
                            {" "}
                            <Threedots
                              width={28}
                              height={28}
                              strokeWidth={2}
                              fill="#9D21E6"
                            />{" "}
                          </div>
                        ) : (
                          formatCurrency(dataTemp?.serviceFee)
                        )}
                      </td>
                    </tr>
                    <tr className="h-2"></tr>
                  </tbody>
                  <tfoot className="border-t-2 border-dashed">
                    <tr className="mb-10 h-10 text-[14px] text-[#9D21E6]">
                      <td className="text-left font-bold">Total Pembayaran</td>
                      <td className="text-right font-bold">
                        {isLoadPrice ? (
                          <div className="flex justify-end">
                            {" "}
                            <Threedots
                              width={28}
                              height={28}
                              strokeWidth={2}
                              fill="#9D21E6"
                            />{" "}
                          </div>
                        ) : (
                          formatCurrency(
                            dataTemp?.total === 0
                              ? dataInfoKelas?.registrationFee
                              : dataTemp?.total,
                          )
                        )}
                      </td>
                    </tr>
                  </tfoot>
                </table>
              </div>
            </div>
          </div>

          <AlertDialog>
            {succesReqPay ? (
              <div className="flex items-center justify-center gap-5">
                  <button
                    onClick={() =>
                      cekStatusTransaksi(payloadReview?.invoiceIdentifier)
                    }
                    className="mt-5 flex justify-center cursor-pointer bg-[#9D21E6] disabled:bg-[#EAE9E9] disabled:text-gray-700 text-white rounded-[10px] px-4 py-2.5 font-semibold w-full max-w-[200px]   shadow-xl"
                  >
                    {isLoading.cekStatus ? (
                    <LoadingSpinner stroke={`#FFF`} size={24} />
                    ) : (
                        "Cek Status"
                    )}
                  </button>
                  {detailTransaction && detailTransaction.status !== "SETTLEMENT" && (
                    <button 
                        onClick={() => cancelStatusTransaction() }
                        disabled={detailTransaction === null}
                        className="mt-5 flex justify-center cursor-pointer bg-red-600 disabled:bg-[#EAE9E9] disabled:text-gray-700 text-white rounded-[10px] px-4 py-2.5 font-semibold w-full max-w-[200px]   shadow-xl">
                        {isLoading.cancelTransaction ? (
                        <LoadingSpinner stroke={`#FFF`} size={24} />
                        ) : (
                            "Batalkan Transaksi"
                        )}
                    </button>
                  )}
              </div>
            ) : (
              <AlertDialogTrigger
                disabled={checkedItems.length === 0}
                className="mt-5 flex justify-center cursor-pointer bg-[#9D21E6] disabled:bg-[#EAE9E9] disabled:text-gray-700 text-white rounded-[10px] px-4 py-2.5 font-semibold w-full max-w-[250px] mx-auto  shadow-xl"
              >
                {payPaidClass.isLoading ? (
                  <LoadingSpinner stroke={`#FFF`} size={24} />
                ) : (
                  "Bayar"
                )}
              </AlertDialogTrigger>
            )}
            <AlertDialogContent className="max-w-4xl">
              <AlertDialogHeader>
                <AlertDialogTitle className="text-center mb-2">
                  Syarat dan Ketentuan Mengikuti Kelas
                </AlertDialogTitle>
                <AlertDialogDescription asChild>
                  <ul className="list-decimal px-5 space-y-1 text-gray-600 text-left">
                    <li>
                      Anda bisa mengikuti kelas dengan melakukan pendaftaran.{" "}
                    </li>
                    <li>
                      Untuk kelas gratis, maka Anda akan langsung bergabung di
                      dalam kelas.{" "}
                    </li>
                    <li>
                      Untuk kelas berbayar, Anda baru bisa bergabung setelah
                      melakukan pembayaran dan terverifikasi.{" "}
                    </li>
                    <li>
                      Anda tidak bisa membatalkan mengikuti sebuah kelas setelah
                      kelas dimulai.{" "}
                    </li>
                    <li>
                      Anda bisa membatalkan mengikuti kelas sebelum kelas
                      dimulai. Dana yang telah Anda bayarkan bisa digunakan
                      untuk mengikuti kelas yang lain.
                    </li>
                  </ul>
                </AlertDialogDescription>
              </AlertDialogHeader>
              <AlertDialogFooter className="mx-auto gap-3 grid grid-cols-2 space-y-0 mt-2">
                <AlertDialogAction
                  onClick={() => payPaidClass.mutate(payloadReview)}
                  className="bg-[#9D21E6] hover:bg-purple-600 text-white rounded-lg"
                >
                  {payPaidClass.isLoading ? (
                    <LoadingSpinner stroke={`#FFF`} size={24} />
                  ) : (
                    "Setuju"
                  )}
                </AlertDialogAction>
                <AlertDialogCancel
                  disabled={payPaidClass.isLoading}
                  className="disabled:opacity-60 bg-[#D7B9FD] text-[#9D21E6]"
                >
                  Tidak
                </AlertDialogCancel>
              </AlertDialogFooter>
            </AlertDialogContent>
          </AlertDialog>

          {succesReqPay && (
            <div
              role="alert"
              className="rounded border-s-4 border-green-500 bg-green-50 p-4 mt-5"
            >
              <div className="flex items-center gap-2 text-green-800">
                <svg
                  xmlns="http://www.w3.org/2000/svg"
                  viewBox="0 0 24 24"
                  fill="currentColor"
                  className="h-5 w-5"
                >
                  <path
                    fillRule="evenodd"
                    d="M9.401 3.003c1.155-2 4.043-2 5.197 0l7.355 12.748c1.154 2-.29 4.5-2.599 4.5H4.645c-2.309 0-3.752-2.5-2.598-4.5L9.4 3.003zM12 8.25a.75.75 0 01.75.75v3.75a.75.75 0 01-1.5 0V9a.75.75 0 01.75-.75zm0 8.25a.75.75 0 100-1.5.75.75 0 000 1.5z"
                    clipRule="evenodd"
                  />
                </svg>

                <strong className="block font-medium"> Informasi</strong>
              </div>

                <ul className="space-y-1 text-sm text-green-700 list-disc  mt-3 ml-5">
                    {detailTransaction && ( <li className="">Status Transaksi : <strong className="font-bold">{detailTransaction?.status}</strong></li>)}
                    <li> Jika kamu ingin mengubah metode pembayaran harap batalkan transaksi dahulu sebelum mengubah metode pembayaran yang baru.  </li>
                    <li> Sebelum membatalkan transaksi harap cek status terlebih dahulu</li>
                </ul>

            </div>
          )}
        </div>
      </div>
    </>
  );
};

const Kedipay = ({ className }) => {
    const style = className ?? "h-12 sm:h-16 w-12 sm:w-16"
    return(
        <svg
        xmlns="http://www.w3.org/2000/svg"
        viewBox="0 0 118.26 100.155"
        className={style}
        >
            <g transform="translate(0.751 0.777)">
                <g transform="translate(0.001 9.86)">
                <path
                    d="M110.576,64.573a6.412,6.412,0,0,1-6.393,6.393h-86.9a6.412,6.412,0,0,1-6.393-6.393V35.832a7.366,7.366,0,0,1,6.351-7.124L104.223,18.7a5.581,5.581,0,0,1,6.351,5.662V64.573Z"
                    transform="translate(-10.892 -18.656)"
                    fill="none"
                    stroke="#231f20"
                    strokeMiterlimit={10}
                    strokeWidth="1.5"
                />
                </g>
                <path
                d="M97.871,58.793,37.758,83.143A8.461,8.461,0,0,1,26.74,78.478L15.214,50.02A8.461,8.461,0,0,1,19.878,39L79.991,14.651a8.461,8.461,0,0,1,11.018,4.665l11.527,28.458A8.462,8.462,0,0,1,97.871,58.793Z"
                transform="translate(-6.705 -14.03)"
                fill="#9d21e6"
                stroke="#000"
                strokeMiterlimit={10}
                strokeWidth="1.5"
                />
                <path
                d="M115.8,98.991H19.351a8.46,8.46,0,0,1-8.46-8.46V34.573a8.46,8.46,0,0,1,8.46-8.46H115.8a8.46,8.46,0,0,1,8.46,8.46V90.53A8.463,8.463,0,0,1,115.8,98.991Z"
                transform="translate(-10.892 -0.363)"
                fill="#fff"
                stroke="#231f20"
                strokeMiterlimit={10}
                strokeWidth="1.5"
                />
                <path
                d="M86.8,58.8H56.665a9.652,9.652,0,0,1-9.652-9.652h0A10.709,10.709,0,0,1,57.722,38.435H86.8V58.8Z"
                transform="translate(29.964 13.574)"
                fill="#fff"
                stroke="#231f20"
                strokeMiterlimit={10}
                strokeWidth="1.5"
                />
                <ellipse
                cx="2.749"
                cy="2.749"
                rx="2.749"
                ry="2.749"
                transform="translate(88.12 59.768)"
                fill="#231f20"
                />
            </g>
        </svg>
    )
}

export async function getServerSideProps(context: any) {
  const { classId } = context.params;
  try {
    const session = await getSession(context);
    if (session) {
      const session = await getSession(context);
      const response = await fetch(
        process.env.API_URL + `/api/class/${classId}`,
        {
          method: "GET",
          headers: {
            "Content-Type": "application/json",
            Authorization: `Bearer ${session?.user?.accessToken}`,
          },
        },
      );

      const isJoined = await fetch(
        process.env.API_URL + `/api/user/class-pagination?classId=${classId}`,
        {
          method: "GET",
          headers: {
            "Content-Type": "application/json",
            Authorization: `Bearer ${session?.user?.accessToken}`,
          },
        },
      );

      const notif = await fetch(
        process.env.API_URL + `/api/finance/notification`,
        {
          method: "GET",
          headers: {
            "Content-Type": "application/json",
            Authorization: `Bearer ${session?.user?.accessToken}`,
          },
        },
      );

      const res          = await response.json(); // atau response.text();
      const join         = await isJoined.json(); // atau response.text();
      const notifFinance = await notif.json(); // atau response.text();

      const { user } = session;
      return {
        props: { user, res, join, notifFinance, classId },
      };
    } else {
      return {
        redirect: {
          destination: `/login?classId=${classId}`,
          permanent: false,
        },
      };
    }
  } catch (error) {
    console.error("Kesalahan:", error);
    return {
      props: {
        error: "Terjadi kesalahan saat memuat sesi pengguna",
      },
    };
  }
}

export default ClassPage;
