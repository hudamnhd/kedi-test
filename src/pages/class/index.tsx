import { getSession } from "next-auth/react";
import { Navbar } from "@/components/navbar/navbar";
import React from "react";
import dynamic from "next/dynamic";
import { LoadingSpinner } from "@/components/ui/loading";
import { Threedots } from "@/assets/icon";
import Image from "next/image";

const ClassRoom = dynamic(() => import("./ClassRoom"), {
  ssr: false,
  loading: () => (
    <div className="fixed h-full w-full flex flex-col gap-1 items-center justify-center bottom-0 left-1/2 transform -translate-x-1/2  z-50">
      <Image
        src="/static/images/logomain.svg"
        width={500}
        height={500}
        priority
        className="w-auto h-auto translate-y-6"
        alt="Logo Kedi"
      />
      <Threedots width={55} height={55} strokeWidth={4} fill="#9D21E6" />
    </div>
  ),
});

const Home = ({ user, viewportHeight }: any) => {
  return (
    <>
      <Navbar user={user} />
      <ClassRoom user={user} viewportHeight={viewportHeight} />
    </>
  );
};

export default Home;

export async function getServerSideProps(context: any) {
  try {
    const session = await getSession(context);
    if (session) {
      return {
        props: { user: session.user },
      };
    } else {
      return {
        redirect: {
          destination: `/login`,
          permanent: false,
        },
      };
    }
  } catch (error) {
    console.error("Kesalahan:", error);
    return {
      props: {
        error: "Terjadi kesalahan saat memuat sesi pengguna",
      },
    };
  }
}
