import React, { useEffect, useState, useRef } from "react";
import PubNub from "pubnub";
import axios from "axios";
import { Chat } from "@/components/kelas/Chat";
import { ListChat } from "@/components/kelas/ListChat";
import { PubNubContextProvider } from "@/context/PubnubContext";
import { trans } from "@/lang";
import { useRouter } from "next/router";
import { encryptData, decryptData } from "@/utils/helper";
import { formatTime } from "@/utils/convertTime";
import useUserChat from "@/store/useUserChat";

const mergeAndRemoveDuplicates = (existing, incoming) => {
  const map = new Map();
  const channel = "channel";
  (existing || []).forEach((item) => map.set(item[channel], item));
  (incoming || []).forEach((item) => map.set(item[channel], item));
  return Array.from(map.values());
};

function compareDates(a, b) {
  return new Date(b.sortTime) - new Date(a.sortTime);
}

const mergeAndReplace = (existing, incoming) => {
  const map = new Map();

  existing.forEach((item) => map.set(item.channel, item));

  incoming.forEach((item) => map.set(item.channel, item));

  return Array.from(map.values());
};

const formatSortTime = (dataItem) => {
  return dataItem.timetoken
    ? formatTime(dataItem.timetoken)
    : dataItem.createdDate;
};

const processAndSortData = (dataArray) => {
  const newData = dataArray.map((item) => {
    const sortTime = formatSortTime(item);
    return {
      ...item,
      sortTime: sortTime,
    };
  });

  newData.sort(compareDates);
  return newData;
};

function mergeMessagesArrays(arr1, arr2) {
  const mergedMessages = new Map();

  function addMessages(array) {
    array.forEach((item) => {
      const channelName = Object.keys(item)[0];
      const messages = item[channelName];

      mergedMessages.set(channelName, messages);
    });
  }

  addMessages(arr1);
  addMessages(arr2);

  const resultArray = [];
  mergedMessages.forEach((messages, channelName) => {
    resultArray.push({ [channelName]: messages });
  });

  return resultArray;
}

let cachedDataArray;

export const getMetadata = async (
  dataArray,
  user,
  tabChat,
  responses,
  allClassMessage,
  privateMessage,
  groupChatMessage,
) => {
  const sessionToken = user?.accessToken;
  const lastMessageKey = `LastMessage_${user?.id}`;

  const setIsLoadMetadata = (value) => {
    useUserChat.setState({
      isLoadMetadata: value,
    });
  };

  const setallClassMessage = (value) => {
    useUserChat.setState({
      allClassMessage: value,
    });
  };
  const setPrivateMessage = (value) => {
    useUserChat.setState({
      privateMessage: value,
    });
  };
  const setGroupChatMessage = (value) => {
    useUserChat.setState({
      groupChatMessage: value,
    });
  };

  //if (
  //  cachedDataArray &&
  //  JSON.stringify(dataArray) === JSON.stringify(cachedDataArray)
  //) {
  //  return cachedDataArray;
  //}

  async function combineData(metadataUsers, messages) {
    let filteredAndMergedData = [];
    let filteredAndMergedDataGroupChat = [];
    let filteredAndMergedDataClass = [];

    if (metadataUsers !== null) {
      if (messages) {
        const filteredData = Object.values(messages)
          .map((channelArray: any) => {
            const lastObject = channelArray[0];
            return lastObject;
          })
          .filter((channel) => channel?.message !== undefined);

        filteredAndMergedDataClass = filteredData
          .sort((a, b) => b.timetoken - a.timetoken)
          .filter((item1) => {
            return metadataUsers?.class?.some(
              (item2: any) => item1.channel === item2.channel,
            );
          })
          .map((item1) => {
            const matchingItem2 = metadataUsers?.class.find(
              (item2: any) => item1.channel === item2.channel,
            );

            if (matchingItem2) {
              return {
                ...item1,
                ...matchingItem2,
              };
            }
            return null;
          })
          .filter((mergedItem) => mergedItem !== null);

        filteredAndMergedDataGroupChat = filteredData
          .filter((item1) => {
            return metadataUsers?.groupChat?.some(
              (item2: any) => item1.channel === item2.channel,
            );
          })
          .map((item1) => {
            const matchingItem2 = metadataUsers?.groupChat.find(
              (item2: any) => item1.channel === item2.channel,
            );
            if (matchingItem2) {
              return {
                ...item1,
                ...matchingItem2,
              };
            }
            return null;
          })
          .filter((mergedItem) => mergedItem !== null); // Menghapus objek yang tidak cocok (jika ada)

        filteredAndMergedData = filteredData
          .filter((item1) => {
            return metadataUsers?.private?.some(
              (item2: any) => item1.channel === item2.channel,
            );
          })
          .map((item1) => {
            const matchingItem2 = metadataUsers.private.find(
              (item2: any) => item1.channel === item2.channel,
            );
            if (matchingItem2) {
              return {
                ...item1,
                ...matchingItem2,
              };
            }
            return null;
          })
          .filter((mergedItem) => mergedItem !== null);
      }
      const classNoMessage = metadataUsers?.class.filter(
        (item1: any) =>
          !filteredAndMergedDataClass.some(
            (item2) => item1.channel === item2.channel,
          ),
      );

      const groupNoMessage = metadataUsers?.groupChat.filter(
        (item1: any) =>
          !filteredAndMergedDataGroupChat.some(
            (item2) => item1.channel === item2.channel,
          ),
      );
      let dataClassNoMsg = classNoMessage ?? [];
      let dataGroupNoMsg = groupNoMessage ?? [];

      let dataClass = [...filteredAndMergedDataClass, ...dataClassNoMsg];
      let dataGroup = [...filteredAndMergedDataGroupChat, ...dataGroupNoMsg];

      const sorted_group_chat = dataGroup?.map((d) => {
        const _sort = d.timetoken ? formatTime(d.timetoken) : d.createdDate;
        let obj = {
          ...d,
          sortTime: _sort,
        };
        return obj;
      });

      sorted_group_chat.sort(compareDates);

      if (
        tabChat === "channelActivePrivate" &&
        filteredAndMergedData?.length !== 0
      ) {
        (async () => {
          const originalData = mergeAndReplace(
            privateMessage.map((item) => ({
              ...item,
              unread: item?.unread ?? 0,
            })),
            filteredAndMergedData.map((item) => ({
              ...item,
              unread: item?.unread ?? 0,
            })),
          );

          const mergedData = processAndSortData(originalData);

          if (responses?.channels) {
            const newMergedData = mergedData.map((objek) => {
              const unread = responses?.channels[objek?.channel] ?? 0;
              return { ...objek, unread };
            });

            setPrivateMessage(newMergedData); // Set state di sini
            setIsLoadMetadata(false);
            const encryptedDataString = localStorage.getItem(lastMessageKey);

            if (encryptedDataString) {
              const decryptedData = decryptData(
                JSON.parse(encryptedDataString),
                "KEDI",
              );

              const updatedData = {
                classMessage: mergeAndRemoveDuplicates(
                  decryptedData.classMessage || [],
                  allClassMessage,
                ),
                privateMessage: mergeAndRemoveDuplicates(
                  decryptedData.privateMessage || [],
                  newMergedData,
                ),
                groupChatMessage: mergeAndRemoveDuplicates(
                  decryptedData.groupChatMessage || [],
                  groupChatMessage,
                ),
              };

              const encryptedMergedData = encryptData(updatedData, "KEDI");

              localStorage.setItem(
                lastMessageKey,
                JSON.stringify(encryptedMergedData),
              );
            } else {
              const encryptedData = encryptData(
                {
                  classMessage: allClassMessage,
                  privateMessage: newMergedData,
                  groupChatMessage,
                },
                "KEDI",
              );

              localStorage.setItem(
                lastMessageKey,
                JSON.stringify(encryptedData),
              );
            }
          } else {
            console.log("Tidak ada data yang tersimpan.");
            setPrivateMessage(mergedData); // Set state di sini jika lastMessStr tidak ada
            setIsLoadMetadata(false);
            const encryptedDataString = localStorage.getItem(lastMessageKey);

            if (encryptedDataString) {
              const decryptedData = decryptData(
                JSON.parse(encryptedDataString),
                "KEDI",
              );

              const updatedData = {
                classMessage: mergeAndRemoveDuplicates(
                  decryptedData.classMessage || [],
                  allClassMessage,
                ),
                privateMessage: mergeAndRemoveDuplicates(
                  decryptedData.privateMessage || [],
                  mergedData,
                ),
                groupChatMessage: mergeAndRemoveDuplicates(
                  decryptedData.groupChatMessage || [],
                  groupChatMessage,
                ),
              };

              const encryptedMergedData = encryptData(updatedData, "KEDI");

              localStorage.setItem(
                lastMessageKey,
                JSON.stringify(encryptedMergedData),
              );
            } else {
              const encryptedData = encryptData(
                {
                  classMessage: allClassMessage,
                  privateMessage: mergedData,
                  groupChatMessage,
                },
                "KEDI",
              );

              localStorage.setItem(
                lastMessageKey,
                JSON.stringify(encryptedData),
              );
            }
          }

          useUserChat.setState({
            isLoadMoreList: false,
          });
        })();
      } else if (tabChat === "channelActiveClass" && dataClass.length !== 0) {
        (async () => {
          const originalData = mergeAndReplace(
            allClassMessage.map((item) => ({
              ...item,
              unread: item?.unread ?? 0,
            })),
            dataClass.map((item) => ({ ...item, unread: item?.unread ?? 0 })),
          );

          const mergedData = processAndSortData(originalData);

          if (responses?.channels) {
            const newMergedData = mergedData.map((objek) => {
              const unread = responses?.channels[objek?.channel] ?? 0;
              return { ...objek, unread };
            });

            setallClassMessage(newMergedData); // Set state di sini
            setIsLoadMetadata(false);

            const encryptedDataString = localStorage.getItem(lastMessageKey);

            if (encryptedDataString) {
              const decryptedData = decryptData(
                JSON.parse(encryptedDataString),
                "KEDI",
              );

              const updatedData = {
                classMessage: mergeAndRemoveDuplicates(
                  decryptedData.classMessage || [],
                  newMergedData,
                ),
                privateMessage: mergeAndRemoveDuplicates(
                  decryptedData.privateMessage || [],
                  privateMessage,
                ),
                groupChatMessage: mergeAndRemoveDuplicates(
                  decryptedData.groupChatMessage || [],
                  groupChatMessage,
                ),
              };

              const encryptedMergedData = encryptData(updatedData, "KEDI");

              localStorage.setItem(
                lastMessageKey,
                JSON.stringify(encryptedMergedData),
              );
            } else {
              const encryptedData = encryptData(
                {
                  classMessage: newMergedData,
                  privateMessage,
                  groupChatMessage,
                },
                "KEDI",
              );

              localStorage.setItem(
                lastMessageKey,
                JSON.stringify(encryptedData),
              );
            }
          } else {
            console.log("Tidak ada data yang tersimpan.");
            setallClassMessage(mergedData); // Set state di sini jika lastMessStr tidak ada
            setIsLoadMetadata(false);

            const encryptedDataString = localStorage.getItem(lastMessageKey);

            if (encryptedDataString) {
              const decryptedData = decryptData(
                JSON.parse(encryptedDataString),
                "KEDI",
              );

              const updatedData = {
                classMessage: mergeAndRemoveDuplicates(
                  decryptedData.classMessage || [],
                  mergedData,
                ),
                privateMessage: mergeAndRemoveDuplicates(
                  decryptedData.privateMessage || [],
                  privateMessage,
                ),
                groupChatMessage: mergeAndRemoveDuplicates(
                  decryptedData.groupChatMessage || [],
                  groupChatMessage,
                ),
              };

              const encryptedMergedData = encryptData(updatedData, "KEDI");

              localStorage.setItem(
                lastMessageKey,
                JSON.stringify(encryptedMergedData),
              );
            } else {
              const encryptedData = encryptData(
                { classMessage: mergedData, privateMessage, groupChatMessage },
                "KEDI",
              );

              localStorage.setItem(
                lastMessageKey,
                JSON.stringify(encryptedData),
              );
            }
          }

          useUserChat.setState({
            isLoadMoreList: false,
          });
        })();
      } else if (
        tabChat === "channelActiveGroup" &&
        sorted_group_chat?.length !== 0
      ) {
        (async () => {
          const originalData = mergeAndReplace(
            groupChatMessage.map((item) => ({
              ...item,
              unread: item?.unread ?? 0,
            })),
            sorted_group_chat.map((item) => ({
              ...item,
              unread: item?.unread ?? 0,
            })),
          );

          const mergedData = processAndSortData(originalData);

          if (responses?.channels) {
            const newMergedData = mergedData.map((objek) => {
              const unread = responses?.channels[objek?.channel] ?? 0;
              return { ...objek, unread };
            });

            setGroupChatMessage(newMergedData); // Set state di sini
            setIsLoadMetadata(false);

            const encryptedDataString = localStorage.getItem(lastMessageKey);

            if (encryptedDataString) {
              const decryptedData = decryptData(
                JSON.parse(encryptedDataString),
                "KEDI",
              );

              const updatedData = {
                classMessage: mergeAndRemoveDuplicates(
                  decryptedData.classMessage || [],
                  allClassMessage,
                ),
                privateMessage: mergeAndRemoveDuplicates(
                  decryptedData.privateMessage || [],
                  privateMessage,
                ),
                groupChatMessage: mergeAndRemoveDuplicates(
                  decryptedData.groupChatMessage || [],
                  newMergedData,
                ),
              };

              const encryptedMergedData = encryptData(updatedData, "KEDI");

              localStorage.setItem(
                lastMessageKey,
                JSON.stringify(encryptedMergedData),
              );
            } else {
              const encryptedData = encryptData(
                {
                  classMessage: allClassMessage,
                  privateMessage,
                  groupChatMessage: newMergedData,
                },
                "KEDI",
              );

              localStorage.setItem(
                lastMessageKey,
                JSON.stringify(encryptedData),
              );
            }
          } else {
            console.log("Tidak ada data yang tersimpan.");
            setGroupChatMessage(mergedData); // Set state di sini jika lastMessStr tidak ada
            setIsLoadMetadata(false);

            const encryptedDataString = localStorage.getItem(lastMessageKey);

            if (encryptedDataString) {
              const decryptedData = decryptData(
                JSON.parse(encryptedDataString),
                "KEDI",
              );

              const updatedData = {
                classMessage: mergeAndRemoveDuplicates(
                  decryptedData.classMessage || [],
                  allClassMessage,
                ),
                privateMessage: mergeAndRemoveDuplicates(
                  decryptedData.privateMessage || [],
                  privateMessage,
                ),
                groupChatMessage: mergeAndRemoveDuplicates(
                  decryptedData.groupChatMessage || [],
                  mergedData,
                ),
              };

              const encryptedMergedData = encryptData(updatedData, "KEDI");

              localStorage.setItem(
                lastMessageKey,
                JSON.stringify(encryptedMergedData),
              );
            } else {
              const encryptedData = encryptData(
                {
                  classMessage: allClassMessage,
                  privateMessage,
                  groupChatMessage: mergedData,
                },
                "KEDI",
              );

              localStorage.setItem(
                lastMessageKey,
                JSON.stringify(encryptedData),
              );
            }
          }

          useUserChat.setState({
            isLoadMoreList: false,
          });
        })();
      }
    }
    useUserChat.setState({
      isLoadMoreList: false,
    });
  }

  //cachedDataArray = dataArray;

  const pubnub = new PubNub({
    publishKey: user?.publishKey,
    subscribeKey: user?.subscribeKey,
    uuid: user?.uuid,
  });

  setIsLoadMetadata(true);
  setTimeout(() => {
    //setIsLoadMetadata(false);
  }, 500);

  if (dataArray.length > 0) {
    try {
      const response = await axios.post(
        process.env.API_BE + "/api/pubnub/metadata-detail",
        {
          channelList: dataArray,
        },
        {
          headers: {
            Authorization: `Bearer ${sessionToken}`,
          },
        },
      );
      const messages = await pubnub.fetchMessages({
        channels: dataArray,
        count: 1,
      });

      if (messages.channels && response.data.data) {
        const _metadataUsers = response.data.data;
        const _messages = messages.channels;

        await combineData(_metadataUsers, _messages);
      }

      //setMessages(messages.channels);
      //setMetadataUsers(response.data.data);
      setTimeout(() => {
        //setIsLoadMetadata(false);
      }, 500);
    } catch (error) {
      setIsLoadMetadata(false);
      console.error("Gagal mengambil data metadataUsers.", error);
    }
  } else {
    setTimeout(() => {
      setIsLoadMetadata(false);
    }, 1000);
  }
};

const ClassRoom = ({ user, viewportHeight }: any) => {
  const router = useRouter();
  const locale: "en" | "id" = router.locale as "en" | "id";
  const lang = trans[locale].class;

  const sessionToken = user?.accessToken;
  const lastTimetokenKey = `LastTimetoken_${user?.id}`;
  const lastMessageKey = `LastMessage_${user?.id}`;
  const messagesKey = `Pb-msg-${user?.uuid}`;
  const MAX_ITEMS_PER_TAB = 30;

  const [groupItems, setGroupItems] = useState([]);
  const [privateItems, setPrivateItems] = useState([]);
  const [classItems, setClassItems] = useState([]);

  const [newItemsGroup, setNewItemsGroup] = useState([]);
  const [newItemsPrivate, setNewItemsPrivate] = useState([]);
  const [newItemsClass, setNewItemsClass] = useState([]);


  const tabChat = useUserChat((state) => state.tabChat);
  const isMobileView = useUserChat((state) => state.isMobileView);
  const initLoad = useUserChat((state) => state.initLoad);
  const privateMessage = useUserChat((state) => state.privateMessage);
  const groupChatMessage = useUserChat((state) => state.groupChatMessage);
  const allClassMessage = useUserChat((state) => state.allClassMessage);
  const responses = useUserChat((state) => state.responses);
  const channelStore = useUserChat((state) => state.channelStore);

  const setChannelStore = (value) => {
    useUserChat.setState({
      channelStore: value,
    });
  };
  const setResponses = (value) => {
    useUserChat.setState({
      responses: value,
    });
  };
  const setallClassMessage = (value) => {
    useUserChat.setState({
      allClassMessage: value,
    });
  };
  const setPrivateMessage = (value) => {
    useUserChat.setState({
      privateMessage: value,
    });
  };
  const setGroupChatMessage = (value) => {
    useUserChat.setState({
      groupChatMessage: value,
    });
  };

  useEffect(() => {
    const storagePubnub = JSON.parse(
      localStorage.getItem("sessionPubnub") ?? "",
    );

    const groupChannels = storagePubnub?.channels?.filter((channelName) =>
      channelName.startsWith("chat-group."),
    );

    const classChannels = storagePubnub?.channels?.filter((channelName) =>
      channelName.startsWith("group."),
    );

    let initialClass = [...classChannels];
    let initialGroup = [...groupChannels];

    let dataPrivate = localStorage.getItem("channelActivePrivate");
    let initialPrivate = dataPrivate ? JSON.parse(dataPrivate) : [];

    // Set semua data yang tersedia dari sumber eksternal

    //setChannelStore({
    //  storedGroup: initialGroup,
    //  storedPrivate: initialPrivate,
    //  storedClass: initialClass,
    //});
    //setStoredGroup(initialGroup);
    //setStoredPrivate(initialPrivate);
    //setStoredClass(initialClass);

    // setClassItems(initialClass.slice(-MAX_ITEMS_PER_TAB));

    const lastMessStr = localStorage.getItem(lastTimetokenKey);
    const allMessages = [
      ...initialClass.slice(-MAX_ITEMS_PER_TAB),
      ...initialPrivate.slice(-MAX_ITEMS_PER_TAB),
      ...initialGroup.slice(-MAX_ITEMS_PER_TAB),
    ];
    const _date = +new Date() * 10000;
    const dateNow = _date.toString();

    if (!lastMessStr) {
      const mappedMessages = allMessages.map((channel) => ({
        channel,
        timetoken: dateNow,
      }));
      const channels = mappedMessages.map((message) => message.channel);
      const timetokens = mappedMessages.map((message) => message.timetoken);
      const lastMess = { channels, timetokens };
      localStorage.setItem(lastTimetokenKey, JSON.stringify(lastMess));
    }

    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, []);

  // Load 20 items from storedClass initially
  async function initializePubnub(decrypted, pubnub) {
    const channelClassMessage =
      decrypted?.classMessage?.map((d) => d.channel) || [];
    const channelPrivateMessage =
      decrypted?.privateMessage?.map((d) => d.channel) || [];
    const channelGroupChatMessage =
      decrypted?.groupChatMessage?.map((d) => d.channel) || [];

    const allChannels = [
      ...channelClassMessage,
      ...channelPrivateMessage,
      ...channelGroupChatMessage,
    ];
    const batchSize = 500;
    let channelActivePrivate: any[] = [];

    for (
      let startIndex = 0;
      startIndex < allChannels.length;
      startIndex += batchSize
    ) {
      const endIndex = Math.min(startIndex + batchSize, allChannels.length);
      const channelsBatch = allChannels.slice(startIndex, endIndex);

      try {
        const messages = await pubnub.fetchMessages({
          channels: channelsBatch,
          count: 25,
        });
        const privateFilter = Object.keys(messages.channels).map((channel) => ({
          [channel]: messages.channels[channel],
        }));
        channelActivePrivate = [
          ...new Set([...channelActivePrivate, ...privateFilter]),
        ];
      } catch (error) {
        console.error("Error fetching messages:", error);
      }
    }

    const lasAllMsgData = localStorage.getItem(messagesKey);

    if (lasAllMsgData) {
      const lastMessObj = JSON.parse(lasAllMsgData);
      const decrypted = decryptData(lastMessObj, "KEDI");

      const mergedMessagesArray = mergeMessagesArrays(
        channelActivePrivate,
        decrypted?.channels,
      );

      const encryptedData = encryptData(
        { channels: mergedMessagesArray },
        "KEDI",
      );

      localStorage.setItem(messagesKey, JSON.stringify(encryptedData));
    } else {
      const encryptedData = encryptData(
        { channels: channelActivePrivate },
        "KEDI",
      );

      localStorage.setItem(messagesKey, JSON.stringify(encryptedData));
    }
  }

  useEffect(() => {
    if (!initLoad) {
      (async () => {
        const lastAllMsg = localStorage.getItem(lastMessageKey);
        const lastMessStr = localStorage.getItem(lastTimetokenKey);
        const pubnub = new PubNub({
          publishKey: user?.publishKey,
          subscribeKey: user?.subscribeKey,
          uuid: user?.uuid,
        });

        if (lastAllMsg) {
          const lastMessObj = JSON.parse(lastAllMsg);
          const decrypted = decryptData(lastMessObj, "KEDI");
          initializePubnub(decrypted, pubnub);
        }

        if (lastMessStr) {
          try {
            const lastMessObj = JSON.parse(lastMessStr);
            const channels = lastMessObj.channels.slice(0, 100);
            const timetokens = lastMessObj.timetokens.slice(0, 100);

            const res = await pubnub.messageCounts({
              channels: channels,
              channelTimetokens: timetokens,
            });

            setResponses(res);
          } catch (error) {
            console.error(error);
          }
        }
      })();
    } else {
      (async () => {
        const lastMessStr = localStorage.getItem(lastTimetokenKey);
        const pubnub = new PubNub({
          publishKey: user?.publishKey,
          subscribeKey: user?.subscribeKey,
          uuid: user?.uuid,
        });

        if (lastMessStr) {
          try {
            const lastMessObj = JSON.parse(lastMessStr);
            const channels = lastMessObj.channels.slice(0, 100);
            const timetokens = lastMessObj.timetokens.slice(0, 100);

            const res = await pubnub.messageCounts({
              channels: channels,
              channelTimetokens: timetokens,
            });

            setResponses(res);
          } catch (error) {
            console.error(error);
          }
        }
      })();
    }

    if (initLoad) {
      if (tabChat === "channelActivePrivate") {
        getMetadata(
          channelStore.storedPrivate.slice(-MAX_ITEMS_PER_TAB),
          user,
          tabChat,
          responses,
          allClassMessage,
          privateMessage,
          groupChatMessage,
        );
      } else if (tabChat === "channelActiveClass") {
        getMetadata(
          channelStore.storedClass.slice(-MAX_ITEMS_PER_TAB),
          user,
          tabChat,
          responses,
          allClassMessage,
          privateMessage,
          groupChatMessage,
        );
      } else if (tabChat === "channelActiveGroup") {
        getMetadata(
          channelStore.storedGroup.slice(-MAX_ITEMS_PER_TAB),
          user,
          tabChat,
          responses,
          allClassMessage,
          privateMessage,
          groupChatMessage,
        );
      }
    }

    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [tabChat]);

  // Fungsi untuk menghapus item spesifik dari array
  const removeItem = (itemToRemove) => {
    const updatedArray = channelStore.storedGroup.filter(
      (item) => item !== itemToRemove,
    );
    setChannelStore({
      ...channelStore,
      storedGroup: updatedArray,
    });
    //setStoredGroup(updatedArray.slice(-MAX_ITEMS_PER_TAB));
    setGroupChatMessage([]);
    getMetadata(
      updatedArray.slice(-MAX_ITEMS_PER_TAB),
      user,
      tabChat,
      responses,
      allClassMessage,
      privateMessage,
      groupChatMessage,
    );
  };

  const addData = (newData) => {
    const groupData = [
      ...channelStore.storedGroup.slice(-MAX_ITEMS_PER_TAB),
      ...(newData ? [newData] : []),
    ];
    getMetadata(
      groupData,
      user,
      tabChat,
      responses,
      allClassMessage,
      privateMessage,
      groupChatMessage,
    );
    setChannelStore({
      ...channelStore,
      storedGroup: groupData,
    });
    //setStoredGroup(groupData);
    // setGroupChatMessage([]);
  };

  const addDataClass = (newData) => {
    const groupData = [
      ...channelStore.storedClass.slice(-MAX_ITEMS_PER_TAB),
      ...(newData ? [newData] : []),
    ];
    // getMetadata(groupData);
    // setStoredClass(groupData);
    // setGroupChatMessage([]);
  };

  const addDataPrivate = async (newData, isDelete) => {
    if (isDelete) {
      const updatedArray = channelStore.storedClass.filter(
        (item) => item !== newData,
      );
      setChannelStore({
        ...channelStore,
        storedGroup: updatedArray,
      });

      const dataPrivate = privateMessage.filter(
        (item) => item.channel !== newData,
      );

      setPrivateMessage(dataPrivate);
      getMetadata(
        updatedArray.slice(-MAX_ITEMS_PER_TAB),
        user,
        tabChat,
        responses,
        allClassMessage,
        privateMessage,
        groupChatMessage,
      );
    } else {
      const privateData = [
        ...channelStore.storedPrivate.slice(-MAX_ITEMS_PER_TAB),
        ...(newData ? [newData] : []),
      ];

      getMetadata(
        privateData,
        user,
        tabChat,
        responses,
        allClassMessage,
        privateMessage,
        groupChatMessage,
      );
      setChannelStore({
        ...channelStore,
        storedPrivate: privateData,
      });
      //setStoredPrivate(privateData);
    }
    // setPrivateMessage([]);
  };

  useEffect(() => {
    if (newItemsClass.length > 0 && initLoad) {
      getMetadata(
        newItemsClass,
        user,
        tabChat,
        responses,
        allClassMessage,
        privateMessage,
        groupChatMessage,
      );

      const lastMessStr = localStorage.getItem(lastTimetokenKey);
      const allMessages = [
        ...newItemsClass,
        ...channelStore.storedPrivate.slice(-MAX_ITEMS_PER_TAB),
        ...channelStore.storedGroup.slice(-MAX_ITEMS_PER_TAB),
      ];
      const _date = +new Date() * 10000;
      const dateNow = _date.toString();

      if (!lastMessStr) {
        const mappedMessages = allMessages.map((channel) => ({
          channel,
          timetoken: dateNow,
        }));
        const channels = mappedMessages.map((message) => message.channel);
        const timetokens = mappedMessages.map((message) => message.timetoken);
        const lastMess = { channels, timetokens };
        localStorage.setItem(lastTimetokenKey, JSON.stringify(lastMess));
      }
    }
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [newItemsClass]);

  const loadMoreItemsClass = (sourceData, setItems) => {
    setItems((prevItems) => {
      const currentLength = prevItems.length;
      let startIndex = Math.max(
        -currentLength - MAX_ITEMS_PER_TAB,
        -sourceData.length,
      );
      let endIndex = -currentLength;
      let new_items;

      if (currentLength === 0) {
        new_items = sourceData.slice(-MAX_ITEMS_PER_TAB);
      } else {
        new_items = sourceData.slice(startIndex, endIndex);
      }

      const updatedItems = [...prevItems, ...new_items];
      setNewItemsClass(new_items);

      // Return array of unique items
      return updatedItems;
    });
  };

  useEffect(() => {
    if (newItemsGroup.length > 0 && initLoad) {
      getMetadata(
        newItemsGroup,
        user,
        tabChat,
        responses,
        allClassMessage,
        privateMessage,
        groupChatMessage,
      );
    }
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [newItemsGroup]);

  const loadMoreItemsGroup = (sourceData, setItems) => {
    setItems((prevItems) => {
      const currentLength = prevItems.length;
      let startIndex = Math.max(
        -currentLength - MAX_ITEMS_PER_TAB,
        -sourceData.length,
      );
      let endIndex = -currentLength;
      let new_items;

      if (currentLength === 0) {
        new_items = sourceData.slice(-MAX_ITEMS_PER_TAB);
      } else {
        new_items = sourceData.slice(startIndex, endIndex);
      }

      const updatedItems = [...prevItems, ...new_items];
      if (currentLength !== 0) {
        setNewItemsGroup(new_items);
      }
      // Return array of unique items
      return updatedItems;
    });
  };

  useEffect(() => {
    if (newItemsPrivate.length > 0 && initLoad) {
      getMetadata(
        newItemsPrivate,
        user,
        tabChat,
        responses,
        allClassMessage,
        privateMessage,
        groupChatMessage,
      );
    }
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [newItemsPrivate]);

  const loadMoreItemsPrivate = (sourceData, setItems) => {
    setItems((prevItems) => {
      const currentLength = prevItems.length;
      let startIndex = Math.max(
        -currentLength - MAX_ITEMS_PER_TAB,
        -sourceData.length,
      );
      let endIndex = -currentLength;
      let new_items;

      if (currentLength === 0) {
        new_items = sourceData.slice(-MAX_ITEMS_PER_TAB);
      } else {
        new_items = sourceData.slice(startIndex, endIndex);
      }

      const updatedItems = [...prevItems, ...new_items];
      if (currentLength !== 0) {
        setNewItemsPrivate(new_items);
      }
      // Return array of unique items
      return updatedItems;
    });
  };

  useEffect(() => {
    if (tabChat === "") {
      let dataClass = localStorage.getItem("channelActiveClass");
      let initialClass = dataClass ? JSON.parse(dataClass) : [];
      getMetadata(
        initialClass.slice(-MAX_ITEMS_PER_TAB),
        user,
        tabChat,
        responses,
        allClassMessage,
        privateMessage,
        groupChatMessage,
      );
      if (initialClass.length <= MAX_ITEMS_PER_TAB) {
        //getMetadata(initialClass.slice(-MAX_ITEMS_PER_TAB));
        // console.log(initialClass.slice(-MAX_ITEMS_PER_TAB));
      }

      setTimeout(() => {
        useUserChat.setState({
          tabChat: "channelActiveClass",
        });
      }, 500);
    }
    setTimeout(() => {
      useUserChat.setState({
        initLoad: true,
      });
    }, 100);
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, []); // Tambahkan array kosong di sini

  return (
    <>
      <div className="flex flex-col [@media(min-width:900px)]:flex-row gap-x-3 shadow-xl items-center">
        <div
          style={{ height: viewportHeight, overflow: "" }}
          className={`relative [@media(min-width:900px)]:max-w-[368px] w-full pt-[4.4rem] sm:pt-[5.2rem] pb-[3.8rem][@media(min-width:900px)]:pb-0 pb-2.5 className flex flex-col shadow-md px-2.5 ${isMobileView ? "hidden [@media(min-width:900px)]:block" : ""}`}
        >
          <ListChat
            addData={addData}
            classItems={classItems}
            groupItems={groupItems}
            lang={lang}
            loadMoreItemsClass={loadMoreItemsClass}
            loadMoreItemsGroup={loadMoreItemsGroup}
            loadMoreItemsPrivate={loadMoreItemsPrivate}
            privateItems={privateItems}
            privateMessage={privateMessage}
            setClassItems={setClassItems}
            setGroupItems={setGroupItems}
            setPrivateItems={setPrivateItems}
            storedClass={channelStore.storedClass}
            storedGroup={channelStore.storedGroup}
            storedPrivate={channelStore.storedPrivate}
            user={user}
            viewportHeight={viewportHeight}
          />
        </div>
        <ViewChat
          addData={addData}
          addDataPrivate={addDataPrivate}
          lang={lang}
          removeItem={removeItem}
          user={user}
          viewportHeight={viewportHeight}
        />
      </div>
    </>
  );
};

const ViewChat = React.memo((props) => {
  const isMobileView = useUserChat((state) => state.isMobileView);
  const userChatAktif = useUserChat((state) => state.userChatAktif);
  const { addData, addDataPrivate, lang, removeItem, user, viewportHeight } =
    props;
  return (
    <>
      {userChatAktif?.channel ? (
        <div
          className={`w-full ${
            isMobileView ? "flex" : "hidden [@media(min-width:900px)]:flex"
          }`}
        >
          <PubNubContextProvider>
            <Chat
              addData={addData}
              addDataPrivate={addDataPrivate}
              lang={lang}
              removeItem={removeItem}
              user={user}
              viewportHeight={viewportHeight}
            />
          </PubNubContextProvider>
        </div>
      ) : (
        <div
          style={{ height: viewportHeight, overflow: "" }}
          className="relative flex-1 pb-[3.2rem] md:pb-0 sm:pt-[5rem] pt-[4rem] justify-between hidden [@media(min-width:900px)]:flex flex-col  min-w-[350px]  lg:col-span-2 md:mr-2.5"
        >
          <div className="w-full h-[90%] flex flex-col justify-center items-center">
            <svg
              stroke="currentColor"
              fill="currentColor"
              strokeWidth={0}
              viewBox="0 0 24 24"
              color="grey"
              height={150}
              width={150}
              xmlns="http://www.w3.org/2000/svg"
              style={{ color: "grey" }}
            >
              <path d="M16 2H8C4.691 2 2 4.691 2 8v12a1 1 0 0 0 1 1h13c3.309 0 6-2.691 6-6V8c0-3.309-2.691-6-6-6zm4 13c0 2.206-1.794 4-4 4H4V8c0-2.206 1.794-4 4-4h8c2.206 0 4 1.794 4 4v7z" />
              <circle cx="9.5" cy="11.5" r="1.5" />
              <circle cx="14.5" cy="11.5" r="1.5" />
            </svg>
            <h1 className="text-2xl">{lang.yourMessage}</h1>
            <h4 className="text-md text-gray-500 text-center">
              {lang.descYourMessage}
            </h4>
          </div>
        </div>
      )}
    </>
  );
});

export default ClassRoom;
