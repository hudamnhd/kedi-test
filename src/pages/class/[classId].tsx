import toast from "react-hot-toast";
import axios from "axios";
import Link from "next/link";
import useUserChat from "@/store/useUserChat";
import React, { useState, useEffect, useId } from "react";
import { Navbar } from "@/components/navbar/navbar";
import { getSession } from "next-auth/react";
import { formatDate, formatToTime } from "@/utils/convertTime";
import { trans } from "@/lang";
import { useRouter } from "next/router";
import { useMutation, useQueryClient } from "@tanstack/react-query";
import { LoadingSpinner } from "@/components/ui/loading";
import { getMessageUser } from "@/utils/getMessageUser";
import { AsyncPaginate } from "react-select-async-paginate";
import {
  AlertDialog,
  AlertDialogAction,
  AlertDialogCancel,
  AlertDialogContent,
  AlertDialogDescription,
  AlertDialogFooter,
  AlertDialogHeader,
  AlertDialogTitle,
} from "@/components/ui/alert-dialog";

const ClassPage = ({ user, res, profile, join }: any) => {
  const router = useRouter();
  const locale: "en" | "id" = router.locale as "en" | "id";
  const lang = trans[locale].class;
  const dataInfoKelas = res.data;

  return (
    <div className="bg-[#FAFAFA]">
      <Navbar user={user} />
      <InfoClass
        user={user}
        dataInfoKelas={dataInfoKelas}
        lang={lang}
        profile={profile}
        join={join}
      />
    </div>
  );
};

const TitleMenu = ({ title }) => {
  return (
    <div className="relative sm:mt-3 mb-5 sm:mb-8 flex items-center justify-between pt-3 w-full h-[40px]">
      <Link href="/" className="flex items-center flex-none">
        <div className="flex-none text-[#9D21E6] bg-[#D7B9FD]   flex justify-center items-center rounded-full w-8 h-8 lg:w-10 lg:h-10 p-1">
          <svg
            xmlns="http://www.w3.org/2000/svg"
            fill="none"
            viewBox="0 0 24 24"
            strokeWidth={2.5}
            stroke="currentColor"
            height={18}
            width={18}
          >
            <path
              strokeLinecap="round"
              strokeLinejoin="round"
              d="M15.75 19.5 8.25 12l7.5-7.5"
            />
          </svg>
        </div>
        <h1 className="ml-4 lg:ml-4 text-xl md:text-2xl font-bold text-[#9D21E6] self-center ">
          {title}
        </h1>
      </Link>
    </div>
  );
};

const InfoClass = ({ user, dataInfoKelas, lang, profile, join }) => {
  console.log("join:", join);
  const setChatMode = (value) => {
    useUserChat.setState({
      chatMode: value,
    });
  };

  const setUserChatAktif = (value) => {
    useUserChat.setState({
      userChatAktif: value,
    });
  };

  const sessionToken = user?.accessToken;
  const router = useRouter();
  const [isAlertOpen, setIsAlertOpen] = useState(false);
  const [userInfo, setUserInfo] = useState({
    institution: profile?.data?.user?.fromInstitution ?? "",
    fromCity: profile?.data?.user?.fromCity ?? "",
    informationSource: profile?.data?.user?.informationSource ?? "",
  });

  const [value] = useState({
    value: profile?.data?.user?.fromInstitution,
    label: profile?.data?.user?.fromInstitution,
  });
  const [valueCity] = useState({
    value: profile?.data?.user?.fromCity,
    label: profile?.data?.user?.fromCity,
  });

  function formatCurrency(amount) {
    if (!amount) return 0;
    const formattedAmount = new Intl.NumberFormat("id-ID", {
      style: "currency",
      currency: "IDR",
      minimumFractionDigits: 2,
    }).format(amount);

    return formattedAmount;
  }

  interface ApiResponse {
    data: {
      message: string;
    };
  }

  const queryClient = useQueryClient();

  interface PostResponse extends ApiResponse {}

  const joinClass = useMutation(
    async (data): Promise<PostResponse> => {
      const { classId, item } = data;
      if (!sessionToken) throw new Error("Missing session token");
      if (!classId) throw new Error("Missing Class id");
      if (
        !userInfo.institution ||
        !userInfo.fromCity ||
        !userInfo.informationSource
      ) {
        toast.error("All fields are required");
        return;
      }

      if (item.isPaidClass) {
        try {
          const response = await axios.post<PostResponse>(
            process.env.API_BE + `/api/class/join-paid-class`,
            {
              classId: parseInt(classId),
              pin: "12345678",
              ...userInfo,
            },
            {
              headers: {
                Authorization: `Bearer ${sessionToken}`,
              },
            },
          );
          router.push("/class/join/" + classId);
          return response.data;
        } catch (error) {
          console.error(error);
          router.push("/class/join/" + classId);
        }
      } else {
        const storedData = localStorage.getItem("channelActiveClass");

        const rooms = JSON.parse(storedData ?? "[]");

        const newRoom = "group." + item.chatRoom.id;

        try {
          const response = await axios.post<PostResponse>(
            process.env.API_BE + `/api/class/join`,
            {
              classId: parseInt(classId),
              pin: "12345678",
              ...userInfo,
            },
            {
              headers: {
                Authorization: `Bearer ${sessionToken}`,
              },
            },
          );

          const opt = {
            channel: "group." + item.chatRoom.id,
          };

          toast.success(`Berhasil Join Class`);
          queryClient.invalidateQueries(["pubnubData"]);
          setTimeout(() => {
            if (!rooms.includes(newRoom)) {
              rooms.push(newRoom);
              localStorage.setItem("channelActiveClass", JSON.stringify(rooms));
            }
            router.push("/class");
            getMessageUser(opt);
            setChatMode("class");
            setUserChatAktif({
              channel: "group." + item.chatRoom.id,
              classId: classId,
              receiver: {
                id: item.id,
                name: item.name,
                photo: null,
              },
              sender: {
                id: user.uuid,
                name: user.fullName,
                photo: user.photo,
              },
            });
          }, 2000);

          return response.data;
        } catch (error) {
          console.error(error);
          throw error;
        }
      }
    },
    {
      onError: (error) =>
        toast.error(`Error: ${error.response.data.meta.message}`),
    },
  );

  const goClass = useMutation(
    async (data): Promise<PostResponse> => {
      const { classId, item } = data;
      const storedData = localStorage.getItem("channelActiveClass");

      const rooms = JSON.parse(storedData ?? "[]");

      const newRoom = "group." + item.chatRoom.id;

      if (!sessionToken) throw new Error("Missing session token");
      if (!classId) throw new Error("Missing Class id");

      const opt = {
        channel: "group." + item.chatRoom.id,
        isDelete: true,
      };

      try {
        if (!rooms.includes(newRoom)) {
          rooms.push(newRoom);
          localStorage.setItem("channelActiveClass", JSON.stringify(rooms));
        }
        router.push("/class");
        getMessageUser(opt);
        setChatMode("class");
        setUserChatAktif({
          channel: "group." + item.chatRoom.id,
          classId: classId,
          receiver: {
            id: item?.lecturer?.uuid,
            name: item.name,
            photo: null,
          },
          sender: {
            id: user.uuid,
            name: user.fullName,
            photo: user.photo,
          },
        });
        console.log({
          channel: "group." + item.chatRoom.id,
          classId: classId,
          receiver: {
            id: item.id,
            name: item.name,
            photo: null,
          },
          sender: {
            id: user.uuid,
            name: user.fullName,
            photo: user.photo,
          },
        });
      } catch (error) {
        console.error(error);
        throw error;
      }
    },
    {
      onError: (error) =>
        toast.error(`Error: ${error.response.data.meta.message}`),
    },
  );

  const today = new Date();
  today.setHours(0);
  today.setMinutes(0);
  today.setSeconds(0);

  const todayEnd = new Date();
  todayEnd.setHours(23);
  todayEnd.setMinutes(59);
  todayEnd.setSeconds(59);

  const endRegisterDate = new Date(dataInfoKelas?.endRegisterDate);
  const startRegisterDate = new Date(dataInfoKelas?.startRegisterDate);

  const isValid = today >= startRegisterDate;
  const isExpired = endRegisterDate > todayEnd;

  const [selectedOption, setSelectedOption] = useState("");
  const [otherValue, setOtherValue] = useState("");

  useEffect(() => {
    if (join?.data?.items?.length > 0) {
      goClass.mutate({
        classId: dataInfoKelas.id,
        item: dataInfoKelas,
      });
    }
  }, [dataInfoKelas, join]);

  const loadOptions = async (searchQuery, loadedOptions, { page }) => {
    try {
      const response = await fetch(
        process.env.API_BE +
          `/api/university/search/univ?query=${searchQuery}&limit=50&page=${page}`,
      );

      if (!response.ok) {
        throw new Error("Failed to fetch data from the server");
      }

      const data = await response.json();

      const options = [
        {
          value: "Umum",
          label: "Umum",
        },
        ...data.data.items.map((item) => ({
          value: item.name,
          label: item.name,
        })),
      ];

      return {
        options: options,
        hasMore: data.data.meta.currentPage < data.data.meta.totalPages,
        additional: {
          page: page + 1,
        },
      };
    } catch (error) {
      // Handle error
      return {
        options: [
          {
            value: "error",
            label: "Failed to fetch data. Please try again later.",
            disabled: true, // Men-disable opsi
          },
        ],
        hasMore: false,
        additional: {
          page: page + 1,
        },
      };
    }
  };

  const loadOptionsCity = async (searchQuery, loadedOptions, { page }) => {
    try {
      const response = await fetch(
        process.env.API_BE +
          `/api/city?query=${searchQuery}&limit=50&page=${page}`,
      );

      if (!response.ok) {
        throw new Error("Failed to fetch data from the server");
      }

      const data = await response.json();

      const options = data.data.items.map((item) => {
        let label = item["Kota/Kab"];
        if (item["Provinsi"].trim() !== "") {
          label += ", " + item["Provinsi"];
        }
        return {
          value: item["id_Kota/Kab"],
          label: label,
        };
      });

      return {
        options: options,
        hasMore: data.data.meta.currentPage < data.data.meta.totalPages,
        additional: {
          page: page + 1,
        },
      };
    } catch (error) {
      // Handle error
      return {
        options: [
          {
            value: "error",
            label: "Failed to fetch data. Please try again later.",
            disabled: true, // Men-disable opsi
          },
        ],
        hasMore: false,
        additional: {
          page: page + 1,
        },
      };
    }
  };

  const customStyles = {
    // provide correct types here
    control: (provided: ControlProps, state: { isFocused: boolean }) => ({
      ...provided,
      padding: "2px",
      borderRadius: "8px",
      border: "1px solid #bdbdbd",
      boxShadow: state.isFocused ? "0 0 0 2px #9D21E6" : null,
    }),
    option: (provided: ControlProps, state: { isFocused: boolean }) => ({
      ...provided,
      backgroundColor: state.isFocused
        ? "#9D21E6"
        : state.isSelected
          ? "#d7a6f5"
          : null,
      color: state.isFocused ? "white" : state.isSelected ? "#000" : null,
    }),
  };

  const options = [
    { label: "LinkedIn", value: "linkedin" },
    { label: "Sosial Media", value: "social-media" },
    { label: "Universitas", value: "universitas" },
    { label: "SMA/SMK", value: "sma-smk" },
    { label: "Lainnya", value: "other" },
  ];

  const handleOptionChange = (event) => {
    setSelectedOption(event.target.value);
    if (event.target.value !== "other") {
      setOtherValue("");
      setUserInfo({ ...userInfo, informationSource: event.target.value });
    } else {
      setUserInfo({ ...userInfo, informationSource: "" });
    }
  };

  const handleOtherInputChange = (event) => {
    setOtherValue(event.target.value);
    setUserInfo({ ...userInfo, informationSource: event.target.value });
  };

  useEffect(() => {
    if (profile?.data?.user?.informationSource) {
      const data = options.filter(
        (d) => d.value === userInfo?.informationSource,
      );
      if (data.length === 0) {
        setSelectedOption("other");
        setOtherValue(userInfo?.informationSource);
      } else {
        setSelectedOption(data[0].value);
      }
    }
  }, [profile]);

  function LineMdAlertCircleLoop() {
    return (
      <svg
        className="h-10 w-10 text-amber-500"
        xmlns="http://www.w3.org/2000/svg"
        viewBox="0 0 24 24"
      >
        <g stroke="currentColor" strokeLinecap="round" strokeWidth="2">
          <path
            fill="currentColor"
            fillOpacity="0"
            strokeDasharray="60"
            strokeDashoffset="60"
            d="M12 3C16.9706 3 21 7.02944 21 12C21 16.9706 16.9706 21 12 21C7.02944 21 3 16.9706 3 12C3 7.02944 7.02944 3 12 3Z"
          >
            <animate
              fill="freeze"
              attributeName="stroke-dashoffset"
              dur="0.5s"
              values="60;0"
            ></animate>
            <animate
              fill="freeze"
              attributeName="fill-opacity"
              begin="1.2s"
              dur="0.15s"
              values="0;0.3"
            ></animate>
          </path>
          <path
            fill="none"
            strokeDasharray="8"
            strokeDashoffset="8"
            d="M12 7V13"
          >
            <animate
              fill="freeze"
              attributeName="stroke-dashoffset"
              begin="0.6s"
              dur="0.2s"
              values="8;0"
            ></animate>
            <animate
              attributeName="stroke-width"
              begin="1s"
              dur="3s"
              keyTimes="0;0.1;0.2;0.3;1"
              repeatCount="indefinite"
              values="2;3;3;2;2"
            ></animate>
          </path>
        </g>
        <circle cx="12" cy="17" r="1" fill="currentColor" fillOpacity="0">
          <animate
            fill="freeze"
            attributeName="fill-opacity"
            begin="0.8s"
            dur="0.4s"
            values="0;1"
          ></animate>
          <animate
            attributeName="r"
            begin="1.3s"
            dur="3s"
            keyTimes="0;0.1;0.2;0.3;1"
            repeatCount="indefinite"
            values="1;2;2;1;1"
          ></animate>
        </circle>
      </svg>
    );
  }

  useEffect(() => {
    if (join?.data?.items?.length === 0) {
      if (isExpired || !isValid) {
        toast.custom(
          (t) => (
            <div
              className={`${
                t.visible ? "animate-enter" : "animate-leave"
              } max-w-md w-full bg-white shadow-lg rounded-lg pointer-events-auto flex ring-1 ring-black ring-opacity-5`}
            >
              <div className="flex-1 w-0 p-4">
                <div className="flex items-start">
                  <div className="flex-shrink-0 pt-0.5">
                    <LineMdAlertCircleLoop />
                  </div>
                  <div className="ml-3 flex-1">
                    <p className="text-sm font-medium text-gray-900">
                      Information
                    </p>
                    <p className="mt-1 text-sm text-gray-700">
                      {!isValid ? (
                        <span className="mx-1">Pendaftaran belum mulai</span>
                      ) : isExpired ? (
                        <span className="mx-1">Pendaftaran telah selesai</span>
                      ) : (
                        ""
                      )}
                    </p>
                  </div>
                </div>
              </div>
              <div className="flex border-l border-gray-200">
                <button
                  onClick={() => toast.dismiss(t.id)}
                  className="w-full border border-transparent rounded-none rounded-r-lg p-4 flex items-center justify-center text-sm font-medium text-gray-700 hover:text-red-500 focus:outline-none focus:ring-2 focus:ring-indigo-500 duration-300"
                >
                  <svg
                    xmlns="http://www.w3.org/2000/svg"
                    width="24px"
                    height="24px"
                    viewBox="0 0 24 24"
                  >
                    <path
                      fill="none"
                      stroke="currentColor"
                      strokeDasharray="12"
                      strokeDashoffset="12"
                      strokeLinecap="round"
                      strokeWidth="2.5"
                      d="M12 12L19 19M12 12L5 5M12 12L5 19M12 12L19 5"
                    >
                      <animate
                        fill="freeze"
                        attributeName="stroke-dashoffset"
                        dur="0.4s"
                        values="12;0"
                      ></animate>
                    </path>
                  </svg>
                </button>
              </div>
            </div>
          ),
          {
            position: "top-center",
            duration: 20000,
          },
        );
      }
    }
  }, [isValid, isExpired, join]);

  if (join?.data?.items?.length > 0)
    return (
      <div className="absolute bg-white/10 backdrop-blur-[1px] h-full w-full flex items-center justify-center bottom-0 left-1/2 transform -translate-x-1/2  z-50">
        <LoadingSpinner stroke={`#9D21E6`} size={50} />
      </div>
    );

  return (
    <>
      <div className="pt-[4.4rem] sm:pt-[5.2rem] pb-[5rem] lg:pb-0 pb-[6rem] sm:pb-[7rem] className flex flex-col min-h-screen w-fit w-full md:max-w-screen-md lg:max-w-screen-xl mx-auto px-3 sm:px-5">
        <div className=" pr-2 mb-24">
          <TitleMenu title={`Ikut Kelas`} />
          <div className="text-sm px-3 py-5 sm:p-5 bg-white rounded-xl border shadow-md mb-5">
            <div className="grid lg:grid-cols-3 lg:gap-3 px-3">
              <div>
                <div className="mb-3 pb-1.5 ">
                  <label className="font-semibold text-[#302B2B]">
                    Pengajar
                  </label>
                  <p className="my-1 text-gray-500">
                    {dataInfoKelas?.lecturer?.fullName}
                  </p>
                </div>
                <div className="mb-3 pb-1.5 ">
                  <label className="font-semibold text-[#302B2B]">
                    Nama Kelas
                  </label>
                  <p className="my-1 text-gray-500">{dataInfoKelas?.name}</p>
                </div>
                <div className="mb-3 pb-1.5 ">
                  <label className="font-semibold text-[#302B2B]">
                    Deskripsi
                  </label>
                  <p className="my-1 text-gray-500 whitespace-pre-wrap break-word">
                    {dataInfoKelas?.description}
                  </p>
                </div>
              </div>
              <div>
                <div className="mb-3 pb-1.5 ">
                  <label className="font-semibold text-[#302B2B]">
                    Tanggal Pendaftaran
                  </label>
                  {!isValid ? (
                    <p className="my-1 text-gray-500">
                      <span className="mx-1">Pendaftaran belum mulai</span>
                    </p>
                  ) : isExpired ? (
                    <p className="my-1 text-gray-500">
                      <span className="mx-1">Pendaftaran telah selesai</span>
                    </p>
                  ) : (
                    <p className="my-1 text-gray-500">
                      {formatDate(dataInfoKelas?.startRegisterDate)}
                      <span className="mx-1">-</span>
                      {formatDate(dataInfoKelas?.endRegisterDate)}
                    </p>
                  )}
                </div>
                <div className="mb-3 pb-1.5 ">
                  <label className="font-semibold text-[#302B2B]">
                    Tanggal Belajar
                  </label>
                  <p className="my-1 text-gray-500">
                    {formatDate(dataInfoKelas?.startClassDate)}
                    <span className="mx-1">-</span>
                    {formatDate(dataInfoKelas?.endClassDate)}
                  </p>
                </div>

                <div className="mb-3 pb-1.5 ">
                  <label className="font-semibold text-[#302B2B]">
                    {lang.numOfSession}
                  </label>
                  <p className="my-1 text-gray-500">
                    {dataInfoKelas?.classAttendanceList?.length}
                  </p>
                </div>
                <div className="mb-3 pb-1.5 ">
                  <label className="font-semibold text-[#302B2B]">
                    Jumlah Peserta
                  </label>
                  <p className="my-1 text-gray-500">
                    {dataInfoKelas?.maximumCapacity}
                  </p>
                </div>
              </div>

              <div>
                <div className="mb-3 pb-1.5 ">
                  <label className="font-semibold text-[#302B2B]">
                    Waktu Belajar/Hari
                  </label>
                  <p className="my-1 text-gray-500">
                    {formatToTime(dataInfoKelas?.startLearningTime)}
                    {" - "}
                    {formatToTime(dataInfoKelas?.endLearningTime)}
                  </p>
                </div>

                <div className="mb-3 pb-1.5 ">
                  <label className="font-semibold text-[#302B2B]">
                    {lang.timeAttendance}
                  </label>
                  <p className="my-1 text-gray-500">
                    {formatToTime(dataInfoKelas?.startAttendanceTime)}
                    {" - "}
                    {formatToTime(dataInfoKelas?.endAttendanceTime)}
                  </p>
                </div>

                <div className="mb-3 pb-1.5 ">
                  <label className="font-semibold text-[#302B2B]">
                    {lang.cost}
                  </label>
                  <p className="my-1 text-gray-500">
                    {dataInfoKelas?.isPaidClass === false
                      ? lang.free
                      : lang.paid}
                  </p>
                </div>
                <div className="mb-3 pb-1.5 ">
                  <label className="font-semibold text-[#302B2B]">Tipe</label>
                  <p className="my-1 text-gray-500">
                    {dataInfoKelas?.isPrivateClass === false
                      ? lang.public
                      : lang.private}
                  </p>
                </div>
              </div>
            </div>
          </div>

          <div className="text-sm px-3 py-5 my-5 sm:p-5 bg-white rounded-xl border shadow-md">
            <div className="grid lg:grid-cols-3 gap-x-5 px-3">
              <div className="mb-2.5">
                <label
                  htmlFor="UserUsername"
                  className="mb-2 block text-xs sm:text-sm  font-medium text-gray-700"
                >
                  Asal Kota/Kabupaten Domisili
                </label>
                <AsyncPaginate
                  defaultValue={valueCity?.value ? valueCity : null}
                  onChange={(param) =>
                    setUserInfo({
                      ...userInfo,
                      fromCity: param?.label ?? null,
                    })
                  }
                  loadOptions={loadOptionsCity}
                  isClearable
                  instanceId={useId()}
                  styles={customStyles}
                  placeholder="Pilih Asal Kota/Kabupaten Domisili"
                  isDisabled={joinClass.isLoading}
                  isOptionDisabled={(option) => option.disabled}
                  additional={{
                    page: 1,
                  }}
                />
              </div>
              <div className="mb-2.5">
                <label
                  htmlFor="UserUsername"
                  className="mb-2 block text-xs sm:text-sm  font-medium text-gray-700"
                >
                  Institusi
                </label>
                <AsyncPaginate
                  defaultValue={value?.value ? value : null}
                  onChange={(param) =>
                    setUserInfo({
                      ...userInfo,
                      institution: param?.value ?? null,
                    })
                  }
                  loadOptions={loadOptions}
                  isClearable
                  instanceId={useId()}
                  styles={customStyles}
                  placeholder="Pilih Institusi"
                  isDisabled={joinClass.isLoading}
                  isOptionDisabled={(option) => option.disabled}
                  additional={{
                    page: 1,
                  }}
                />
              </div>
              <div></div>
              <div className="mb-5">
                <label
                  htmlFor="description"
                  className="block  font-medium text-gray-700"
                >
                  Mengetahui Event dari?
                </label>

                <>
                  {options.map((option) => (
                    <div
                      key={option.value}
                      className="flex items-center mt-2 mb-2"
                    >
                      <input
                        id={option.value}
                        type="radio"
                        name="radio-group"
                        value={option.value}
                        className="w-4 h-4 text-blue-600 bg-gray-100 border-gray-300 focus:ring-blue-500 dark:focus:ring-blue-600 dark:ring-offset-gray-800 focus:ring-2 dark:bg-gray-700 dark:border-gray-600"
                        checked={selectedOption === option.value}
                        onChange={handleOptionChange}
                      />
                      <label
                        htmlFor={option.value}
                        className="ms-2 text-sm font-medium text-gray-600 "
                      >
                        {option.label}
                      </label>
                    </div>
                  ))}
                  {selectedOption === "other" && (
                    <div className="mt-2 pl-5">
                      <input
                        type="text"
                        placeholder="Masukkan Jawaban Anda"
                        value={otherValue}
                        onChange={handleOtherInputChange}
                        className="border border-gray-300 rounded-md p-2 focus:outline-none focus:ring focus:ring-blue-500"
                      />
                    </div>
                  )}
                </>
              </div>
            </div>
          </div>

          {dataInfoKelas?.isPaidClass === true && (
            <div className="flex items-center justify-between bg-[#F4EBFE] p-5 mt-5 rounded-lg">
              <label className="font-semibold text-[#45008E]">
                {lang.cost}
              </label>
              <p className="my-1 md:text-2xl font-bold text-[#45008E]">
                {formatCurrency(dataInfoKelas?.registrationFee)}
              </p>
            </div>
          )}
          <button
            onClick={() => setIsAlertOpen(true)}
            disabled={!isValid || isExpired}
            className="mt-5 flex justify-center cursor-pointer disabled:cursor-default disabled:opacity-70 bg-[#9D21E6] text-white rounded-lg px-4 py-2.5 font-semibold w-full max-w-[250px] mx-auto border border-gray-400 shadow-xl"
          >
            Ikut Kelas
          </button>
          {isAlertOpen && (
            <AlertDialog open={isAlertOpen} onOpenChange={setIsAlertOpen}>
              <AlertDialogContent className="max-w-4xl">
                <AlertDialogHeader>
                  <AlertDialogTitle className="text-center mb-2">
                    Syarat dan Ketentuan Mengikuti Kelas
                  </AlertDialogTitle>
                  <AlertDialogDescription asChild>
                    <ul className="list-decimal px-5 space-y-1 text-gray-600 text-left">
                      <li>
                        Anda bisa mengikuti kelas dengan melakukan pendaftaran.{" "}
                      </li>
                      <li>
                        Untuk kelas gratis, maka Anda akan langsung bergabung di
                        dalam kelas.{" "}
                      </li>
                      <li>
                        Untuk kelas berbayar, Anda baru bisa bergabung setelah
                        melakukan pembayaran dan terverifikasi.{" "}
                      </li>
                      <li>
                        Anda tidak bisa membatalkan mengikuti sebuah kelas
                        setelah kelas dimulai.{" "}
                      </li>
                      <li>
                        Anda bisa membatalkan mengikuti kelas sebelum kelas
                        dimulai. Dana yang telah Anda bayarkan bisa digunakan
                        untuk mengikuti kelas yang lain.
                      </li>
                    </ul>
                  </AlertDialogDescription>
                </AlertDialogHeader>
                <AlertDialogFooter className="mx-auto gap-3 grid grid-cols-2 space-y-0 mt-2">
                  <AlertDialogAction
                    onClick={() =>
                      joinClass.mutate({
                        classId: dataInfoKelas.id,
                        item: dataInfoKelas,
                      })
                    }
                    className="bg-[#9D21E6] hover:bg-purple-600 text-white rounded-lg"
                  >
                    {joinClass.isLoading ? (
                      <LoadingSpinner stroke={`#FFF`} size={24} />
                    ) : (
                      "Setuju"
                    )}
                  </AlertDialogAction>
                  <AlertDialogCancel
                    disabled={joinClass.isLoading}
                    className="disabled:opacity-60 bg-[#D7B9FD] text-[#9D21E6]"
                  >
                    Tidak
                  </AlertDialogCancel>
                </AlertDialogFooter>
              </AlertDialogContent>
            </AlertDialog>
          )}
        </div>
      </div>
    </>
  );
};

export async function getServerSideProps(context: any) {
  const { classId } = context.params;
  try {
    const session = await getSession(context);
    if (session) {
      const session = await getSession(context);
      const response = await fetch(
        process.env.API_URL + `/api/class/${classId}`,
        {
          method: "GET",
          headers: {
            "Content-Type": "application/json",
            Authorization: `Bearer ${session?.user?.accessToken}`,
          },
        },
      );

      const isProfile = await fetch(process.env.API_URL + `/api/auth/profile`, {
        method: "GET",
        headers: {
          "Content-Type": "application/json",
          Authorization: `Bearer ${session?.user?.accessToken}`,
        },
      });

      const isJoined = await fetch(
        process.env.API_URL + `/api/user/class-pagination?classId=${classId}`,
        {
          method: "GET",
          headers: {
            "Content-Type": "application/json",
            Authorization: `Bearer ${session?.user?.accessToken}`,
          },
        },
      );

      const res = await response.json(); // atau response.text();
      const profile = await isProfile.json(); // atau response.text();
      const join = await isJoined.json(); // atau response.text();

      const { user } = session;
      return {
        props: { user, res, profile, join },
      };
    } else {
      return {
        redirect: {
          destination: `/login?classId=${classId}`,
          permanent: false,
        },
      };
    }
  } catch (error) {
    console.error("Kesalahan:", error);
    return {
      props: {
        error: "Terjadi kesalahan saat memuat sesi pengguna",
      },
    };
  }
}

export default ClassPage;
