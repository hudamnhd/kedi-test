import "@/styles/globals.css";
import { useState } from "react";
import {
  Hydrate,
  QueryClient,
  QueryClientProvider,
} from "@tanstack/react-query";
import Head from "next/head";
import { SessionProvider } from "next-auth/react";
import { Toaster } from "react-hot-toast";
import { AppProps } from "next/app";
import NextNProgress from "nextjs-progressbar";
import { Toaster as Soner } from "sonner";
import { LayoutViewport } from "@/components/layout/viewport";

export default function App({
  Component,
  pageProps: { session, ...pageProps },
}: AppProps) {
  const [queryClient] = useState(() => new QueryClient());
  return (
    <>
      <Head>
        <title>Kelas Digital</title>
        <meta name="description" content="Kelas Digital Website" />
      </Head>
      <SessionProvider session={session}>
        <QueryClientProvider client={queryClient}>
          <Hydrate state={pageProps.dehydratedState}>
            <NextNProgress
              options={{ showSpinner: false, easing: "ease", speed: 500 }}
              color="#9D21E6"
              stopDelayMs={200}
              height={5}
              showOnShallow={true}
            />
            <LayoutViewport>
              <Component {...pageProps} />
            </LayoutViewport>
            <Toaster position="bottom-center" />
            <Soner expand={true} />
          </Hydrate>
        </QueryClientProvider>
      </SessionProvider>
    </>
  );
}
