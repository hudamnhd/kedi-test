import Link from "next/link";
import { getSession } from "next-auth/react";
import { Navbar } from "@/components/navbar/navbar";
import { useRouter } from "next/router";
import { trans } from "@/lang";

const Dashboard = ({ user }: any) => {
  return (
    <div className="bg-[#FAFAFA]">
      <Navbar user={user} />
      <ListMenu />
    </div>
  );
};

const ListMenu = () => {
  const router = useRouter();
  const locale: "en" | "id" = router.locale as "en" | "id";
  const lang = trans[locale].dashboard;

  const heightContainer = { height: "calc(100vh)" };
  const disabled = false;
  const isDisabled = disabled ? "#EAE9E9" : "#9d21e6";

  return (
    <div className="pt-[4.4rem] sm:pt-[5.2rem] pb-[3.8rem] lg:pb-0 pb-2.5 className flex flex-col w-full max-w-xl mx-auto sm:px-5">
      <div className="overflow-y-auto pr-2 " style={heightContainer}>
        <div className="my-4 grid grid-cols-3 gap-2 sm:gap-5 mx-3 place-items-center sm:mx-auto">
          <div className="w-full py-4 sm:p-4 bg-white rounded-lg shadow-md hover:bg-purple-100 duration-300">
            <Link className="text-black" href="/dashboard/absensi">
              <div className="sc-jXbUNg cPOoWY">
                <div className="w-auto cursor-pointer  rounded-lg p-2 ">
                  <div className="  items-center  my-2 lg:my-0 mx-2 text-md ">
                    <div className="block lg:flex justify-center items-center">
                      <div className="flex items-center justify-center">
                        <svg
                          xmlns="http://www.w3.org/2000/svg"
                          width="81.371"
                          height="111.663"
                          viewBox="0 0 81.371 111.663"
                          className="h-12 sm:h-16 w-12 sm:w-16 "
                        >
                          <g transform="translate(-17.475 -10)">
                            <path
                              d="M18.226,32.852V90.906c0,3.332,4.491,6.06,9.984,6.06h59.9c5.493,0,9.984-2.728,9.984-6.06V32.852Z"
                              transform="translate(0.001 23.947)"
                              fill="#fff"
                              stroke="#000"
                              strokeMiterlimit={10}
                              strokeWidth="1.5"
                            />
                            <path
                              d="M29.48,56.722l8.067,14.122L61.5,41.289"
                              transform="translate(11.794 32.788)"
                              fill="none"
                              stroke="#9d21e6"
                              strokeLinecap="round"
                              strokeMiterlimit={10}
                              strokeWidth={2}
                            />
                            <path
                              d="M88.112,18.522H83.328v.33a7.049,7.049,0,1,1-14.1,0v-.33H47.09v.33a7.049,7.049,0,1,1-14.1,0v-.33H28.209a10.012,10.012,0,0,0-9.984,9.984V40.779H98.094V28.506A10.011,10.011,0,0,0,88.112,18.522Z"
                              transform="translate(0 8.93)"
                              fill="#9d21e6"
                              stroke="#000"
                              strokeMiterlimit={10}
                              strokeWidth="1.5"
                            />
                            <path
                              d="M31.1,31.483h0a4.255,4.255,0,0,1-4.256-4.256V14.256A4.255,4.255,0,0,1,31.1,10h0a4.255,4.255,0,0,1,4.256,4.256V27.227A4.255,4.255,0,0,1,31.1,31.483Z"
                              transform="translate(9.032 0)"
                              fill="#231f20"
                            />
                            <path
                              d="M48.794,31.483h0a4.255,4.255,0,0,1-4.256-4.256V14.256A4.255,4.255,0,0,1,48.794,10h0a4.255,4.255,0,0,1,4.256,4.256V27.227A4.255,4.255,0,0,1,48.794,31.483Z"
                              transform="translate(27.574 0)"
                              fill="#231f20"
                            />
                          </g>
                        </svg>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
              <h1 className="text-center font-medium mt-2 text-xs sm:text-sm">
                {lang.attendance}
              </h1>
            </Link>
          </div>

          <div className="w-full py-4 sm:p-4 bg-white rounded-lg shadow-md hover:bg-purple-100 duration-300">
            <Link className="text-black" href="/dashboard/tugas">
              <div className="sc-jXbUNg cPOoWY">
                <div className="w-auto cursor-pointer  rounded-lg p-2 ">
                  <div className="  items-center  my-2 lg:my-0 mx-2 text-md ">
                    <div className="block lg:flex justify-center items-center">
                      <div className="flex items-center justify-center">
                        <svg
                          xmlns="http://www.w3.org/2000/svg"
                          width="98.395"
                          height="114.757"
                          viewBox="0 0 98.395 114.757"
                          className="h-12 sm:h-16 w-12 sm:w-16 "
                        >
                          <g transform="translate(-14.5 -10.513)">
                            <g transform="translate(15.25 11.263)">
                              <path
                                d="M95.194,105.125a4.013,4.013,0,0,1-4,4H19.252a4.013,4.013,0,0,1-4-4V15.265a4.013,4.013,0,0,1,4-4H91.193a4.013,4.013,0,0,1,4,4Z"
                                transform="translate(-15.25 -11.263)"
                                fill="#fff"
                              />
                              <path
                                d="M95.194,105.125a4.013,4.013,0,0,1-4,4H19.252a4.013,4.013,0,0,1-4-4V15.265a4.013,4.013,0,0,1,4-4H91.193a4.013,4.013,0,0,1,4,4Z"
                                transform="translate(-15.25 -11.263)"
                                fill="none"
                                stroke="#000"
                                strokeMiterlimit={10}
                                strokeWidth="1.5"
                              />
                            </g>
                            <g transform="translate(25.68 24.834)">
                              <path
                                d="M79.109,20.941h-57.2a1.448,1.448,0,1,1,0-2.9h57.2a1.448,1.448,0,1,1,0,2.9Z"
                                transform="translate(-20.463 -18.046)"
                              />
                            </g>
                            <g transform="translate(25.68 40.704)">
                              <path
                                d="M79.109,28.873h-57.2a1.448,1.448,0,1,1,0-2.9h57.2a1.448,1.448,0,1,1,0,2.9Z"
                                transform="translate(-20.463 -25.978)"
                              />
                            </g>
                            <g transform="translate(25.68 56.574)">
                              <path
                                d="M79.109,36.8h-57.2a1.447,1.447,0,1,1,0-2.893h57.2a1.447,1.447,0,1,1,0,2.893Z"
                                transform="translate(-20.463 -33.91)"
                              />
                            </g>
                            <g transform="translate(25.68 72.16)">
                              <path
                                d="M53.776,44.6H21.912a1.449,1.449,0,1,1,0-2.9H53.778a1.449,1.449,0,0,1,0,2.9Z"
                                transform="translate(-20.463 -41.7)"
                                fill="#9d21e6"
                              />
                            </g>
                            <path
                              d="M20.422,0A20.422,20.422,0,1,1,0,20.422,20.422,20.422,0,0,1,20.422,0Z"
                              transform="translate(54.073 95.329) rotate(-45)"
                              fill="#9d21e6"
                              stroke="#000"
                              strokeWidth="1.5"
                            />
                            <path
                              d="M43.673,57.188l8.085,9.292,14.992-17.1"
                              transform="translate(28.445 38.141)"
                              fill="none"
                              stroke="#231f20"
                              strokeLinecap="round"
                              strokeMiterlimit={10}
                              strokeWidth={2}
                            />
                          </g>
                        </svg>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
              <h1 className="text-center font-medium mt-2 text-xs sm:text-sm">
                {lang.assignments}
              </h1>
            </Link>
          </div>

          <div className="w-full py-4 sm:p-4 bg-white rounded-lg shadow-md hover:bg-purple-100 duration-300">
            <Link className="text-black" href="/dashboard/quiz">
              <div className="sc-jXbUNg cPOoWY">
                <div className="w-auto cursor-pointer  rounded-lg p-2 ">
                  <div className="  items-center  my-2 lg:my-0 mx-2 text-md ">
                    <div className="block lg:flex justify-center items-center">
                      <div className="flex items-center justify-center">
                        <svg
                          xmlns="http://www.w3.org/2000/svg"
                          width="100.039"
                          height="121.241"
                          viewBox="0 0 100.039 121.241"
                          className="h-12 sm:h-16 w-12 sm:w-16 "
                        >
                          <g transform="translate(-15.917 -9.613)">
                            <path
                              d="M88.226,106.185l-7.171,7.789H16.667V17.341H95.6l.2,80.615Z"
                              transform="translate(0 8.61)"
                              fill="#fff"
                              stroke="#000"
                              strokeMiterlimit={10}
                              strokeWidth="1.5"
                            />
                            <g transform="translate(76.395 91.293)">
                              <circle
                                cx="19.405"
                                cy="19.405"
                                r="19.405"
                                transform="translate(0 0)"
                                fill="#9d21e6"
                                stroke="#000"
                                strokeMiterlimit={10}
                                strokeWidth="1.5"
                              />
                              <g transform="translate(12.74 9.315)">
                                <path
                                  d="M49.108,56.338a4.8,4.8,0,0,1,.813-2.567,6.262,6.262,0,0,1,2.372-2.153,7.462,7.462,0,0,1,3.639-.856,7.763,7.763,0,0,1,3.413.715,5.529,5.529,0,0,1,2.285,1.937,4.764,4.764,0,0,1,.806,2.667,4.115,4.115,0,0,1-.46,1.988,6.142,6.142,0,0,1-1.092,1.47q-.633.623-2.276,2.089a9.594,9.594,0,0,0-.726.726,3.045,3.045,0,0,0-.407.576,2.92,2.92,0,0,0-.206.516c-.051.172-.123.478-.221.914a1.488,1.488,0,0,1-1.586,1.387,1.631,1.631,0,0,1-1.168-.453,1.786,1.786,0,0,1-.471-1.349,4.749,4.749,0,0,1,1.264-3.38q.576-.62,1.546-1.472c.57-.5.983-.873,1.235-1.126a3.868,3.868,0,0,0,.639-.847,2.068,2.068,0,0,0,.259-1.012,2.367,2.367,0,0,0-.791-1.8,2.9,2.9,0,0,0-2.048-.733,2.828,2.828,0,0,0-2.158.742,5.871,5.871,0,0,0-1.173,2.18q-.452,1.5-1.72,1.5a1.7,1.7,0,0,1-1.26-.523A1.621,1.621,0,0,1,49.108,56.338Zm6.5,14.612a2.093,2.093,0,0,1-1.418-.527,1.85,1.85,0,0,1-.605-1.474,1.888,1.888,0,0,1,.587-1.414,1.971,1.971,0,0,1,1.439-.572A1.953,1.953,0,0,1,57.6,68.948a1.849,1.849,0,0,1-.6,1.465A2.028,2.028,0,0,1,55.613,70.949Z"
                                  transform="translate(-49.108 -50.762)"
                                />
                              </g>
                            </g>
                            <path
                              d="M62.186,17.69H52.933a7.328,7.328,0,0,0-14.656,0H29.029a3.753,3.753,0,0,0-3.746,3.742V26.8a3.752,3.752,0,0,0,3.746,3.737H62.184A3.747,3.747,0,0,0,65.921,26.8V21.432A3.745,3.745,0,0,0,62.186,17.69Z"
                              transform="translate(10.631)"
                              fill="#9d21e6"
                              stroke="#000"
                              strokeMiterlimit={10}
                              strokeWidth="1.5"
                            />
                            <g transform="translate(48.334 43.627)">
                              <path
                                d="M70.027,28.486H32.46a1.616,1.616,0,1,1,0-3.232H70.027a1.616,1.616,0,1,1,0,3.232Z"
                                transform="translate(-30.843 -25.254)"
                              />
                            </g>
                            <g transform="translate(22.234 43.737)">
                              <path
                                d="M37.943,28.535H20.776a1.616,1.616,0,1,1,0-3.232H37.943a1.616,1.616,0,1,1,0,3.232Z"
                                transform="translate(-19.159 -25.303)"
                              />
                            </g>
                            <g transform="translate(48.334 56.659)">
                              <path
                                d="M70.027,34.32H32.46a1.616,1.616,0,1,1,0-3.232H70.027a1.616,1.616,0,1,1,0,3.232Z"
                                transform="translate(-30.843 -31.088)"
                              />
                            </g>
                            <g transform="translate(22.234 56.767)">
                              <path
                                d="M37.943,34.368H20.776a1.616,1.616,0,1,1,0-3.232H37.943a1.616,1.616,0,1,1,0,3.232Z"
                                transform="translate(-19.159 -31.136)"
                              />
                            </g>
                            <g transform="translate(48.334 69.689)">
                              <path
                                d="M70.027,40.153H32.46a1.616,1.616,0,1,1,0-3.232H70.027a1.616,1.616,0,1,1,0,3.232Z"
                                transform="translate(-30.843 -36.921)"
                              />
                            </g>
                            <g transform="translate(22.234 69.799)">
                              <path
                                d="M37.943,40.2H20.776a1.616,1.616,0,1,1,0-3.232H37.943a1.616,1.616,0,1,1,0,3.232Z"
                                transform="translate(-19.159 -36.97)"
                              />
                            </g>
                          </g>
                        </svg>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
              <h1 className="text-center font-medium mt-2 text-xs sm:text-sm">
                {lang.quizzes}
              </h1>
            </Link>
          </div>

          <div className="w-full py-4 sm:p-4 bg-white rounded-lg shadow-md hover:bg-purple-100 duration-300">
            <Link className="text-black" href="/dashboard/materi-kelas">
              <div className="sc-jXbUNg cPOoWY">
                <div className="w-auto cursor-pointer  rounded-lg p-2 ">
                  <div className="  items-center  my-2 lg:my-0 mx-2 text-md ">
                    <div className="block lg:flex justify-center items-center">
                      <div className="flex items-center justify-center">
                        <svg
                          xmlns="http://www.w3.org/2000/svg"
                          width="95.043"
                          height="114.464"
                          viewBox="0 0 95.043 114.464"
                          className="h-12 sm:h-16 w-12 sm:w-16 "
                        >
                          <g transform="translate(0)">
                            <path
                              d="M106.533,107.3,38.078,122.744a1.449,1.449,0,0,1-1.735-1.1L15.421,28.288a1.453,1.453,0,0,1,1.1-1.735L84.974,11.106a1.451,1.451,0,0,1,1.735,1.1l20.925,93.358A1.453,1.453,0,0,1,106.533,107.3Z"
                              transform="translate(-14.008 -9.693)"
                              fill="#9d21e6"
                            />
                            <path
                              d="M108.414,109.013a2.817,2.817,0,0,1-.855.349L39.1,124.807a2.821,2.821,0,0,1-3.381-2.134L14.8,29.314a2.839,2.839,0,0,1,2.146-3.383L85.4,10.485a2.828,2.828,0,0,1,3.383,2.136l20.925,93.358A2.845,2.845,0,0,1,108.414,109.013ZM17.528,28.625a.086.086,0,0,0-.04.082l20.923,93.358a.082.082,0,0,0,.088.055l68.455-15.445h0a.085.085,0,0,0,.053-.088L86.082,13.23a.079.079,0,0,0-.088-.057L17.545,28.618A.021.021,0,0,0,17.528,28.625Z"
                              transform="translate(-14.729 -10.414)"
                            />
                          </g>
                          <g transform="translate(5.875 3.938)">
                            <path
                              d="M89.491,116.778l-69.969-5.591a1.45,1.45,0,0,1-1.334-1.563L25.843,14.28a1.455,1.455,0,0,1,1.565-1.332l69.969,5.593a1.454,1.454,0,0,1,1.336,1.56l-7.657,95.347A1.451,1.451,0,0,1,89.491,116.778Z"
                              transform="translate(-16.803 -11.57)"
                              fill="#fff"
                            />
                            <path
                              d="M90.333,118.88c-.08,0-.155,0-.231-.008l-69.969-5.593a2.83,2.83,0,0,1-2.6-3.043L25.188,14.89A2.838,2.838,0,0,1,28.24,12.3L98.211,17.89a2.835,2.835,0,0,1,2.6,3.043l-7.655,95.347a2.837,2.837,0,0,1-2.821,2.6ZM28.015,15.04a.079.079,0,0,0-.076.069l-7.653,95.347a.083.083,0,0,0,.067.08l69.967,5.591h0c.044-.011.076-.034.08-.067l7.655-95.347a.072.072,0,0,0-.065-.08L28.021,15.04Z"
                              transform="translate(-17.526 -12.29)"
                            />
                            <g transform="translate(13.28 21.694)">
                              <g transform="translate(0 34.64)">
                                <line
                                  x2="17.726"
                                  y2="1.416"
                                  transform="translate(0.921 0.917)"
                                  fill="#fff"
                                />
                                <path
                                  d="M42.5,42.363c-.025,0-.05,0-.076,0L24.694,40.945a.918.918,0,1,1,.149-1.829l17.724,1.416a.917.917,0,0,1-.071,1.831Z"
                                  transform="translate(-23.848 -39.112)"
                                  fill="#9d21e6"
                                />
                              </g>
                              <g transform="translate(3.346)">
                                <line
                                  x2="53.065"
                                  y2="4.238"
                                  transform="translate(0.919 0.922)"
                                  fill="#fff"
                                />
                                <path
                                  d="M79.425,28.7c-.027,0-.05,0-.076,0L26.287,24.453a.917.917,0,0,1-.842-.989.9.9,0,0,1,.991-.84L79.5,26.866a.917.917,0,0,1-.074,1.831Z"
                                  transform="translate(-25.442 -22.619)"
                                />
                              </g>
                              <g transform="translate(2.428 7.267)">
                                <line
                                  x2="53.062"
                                  y2="4.238"
                                  transform="translate(0.921 0.922)"
                                  fill="#fff"
                                />
                                <path
                                  d="M78.988,32.157c-.025,0-.05,0-.076,0L25.85,27.913a.917.917,0,0,1-.842-.989.9.9,0,0,1,.991-.84l53.062,4.242a.917.917,0,0,1-.074,1.831Z"
                                  transform="translate(-25.005 -26.079)"
                                />
                              </g>
                              <g transform="translate(1.51 15.311)">
                                <line
                                  x2="53.065"
                                  y2="4.24"
                                  transform="translate(0.921 0.92)"
                                  fill="#fff"
                                />
                                <path
                                  d="M78.553,35.987c-.027,0-.05,0-.076,0L25.413,31.743a.917.917,0,0,1-.842-.989.9.9,0,0,1,.991-.84l53.065,4.242a.917.917,0,0,1-.074,1.831Z"
                                  transform="translate(-24.568 -29.909)"
                                />
                              </g>
                            </g>
                            <g transform="translate(31.694 8.45)">
                              <path
                                d="M32.616,16.313"
                                transform="translate(-32.616 -16.313)"
                                fill="none"
                                stroke="#233863"
                                strokeLinecap="round"
                                strokeLinejoin="round"
                                strokeMiterlimit={10}
                                strokeWidth={3}
                              />
                            </g>
                          </g>
                        </svg>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
              <h1 className="text-center font-medium mt-2 text-xs sm:text-sm">
                {lang.materials}
              </h1>
            </Link>
          </div>

          <div className="w-full py-4 sm:p-4 bg-white rounded-lg shadow-md hover:bg-purple-100 duration-300">
            <Link className="text-black" href="/dashboard/kontak">
              <div className="sc-jXbUNg cPOoWY">
                <div className="w-auto cursor-pointer  rounded-lg p-2 ">
                  <div className="  items-center  my-2 lg:my-0 mx-2 text-md ">
                    <div className="block lg:flex justify-center items-center">
                      <div className="flex items-center justify-center">
                        <svg
                          className="h-12 sm:h-16 w-12 sm:w-16 "
                          viewBox="0 0 75 75"
                          fill="none"
                          xmlns="http://www.w3.org/2000/svg"
                        >
                          <path
                            d="M42.52 38.44C46.6953 38.44 50.08 35.0553 50.08 30.88C50.08 26.7047 46.6953 23.32 42.52 23.32C38.3447 23.32 34.96 26.7047 34.96 30.88C34.96 35.0553 38.3447 38.44 42.52 38.44Z"
                            fill="#9D21E6"
                          />
                          <path
                            d="M18.35 19.6H13.89C12.548 19.6 11.46 20.6857 11.46 22.025C11.46 23.3643 12.548 24.45 13.89 24.45H18.35C19.6921 24.45 20.78 23.3643 20.78 22.025C20.78 20.6857 19.6921 19.6 18.35 19.6Z"
                            fill="#9D21E6"
                          />
                          <path
                            d="M18.35 50.54H13.89C12.548 50.54 11.46 51.6257 11.46 52.965C11.46 54.3043 12.548 55.39 13.89 55.39H18.35C19.6921 55.39 20.78 54.3043 20.78 52.965C20.78 51.6257 19.6921 50.54 18.35 50.54Z"
                            fill="#9D21E6"
                          />
                          <path
                            d="M17.65 10C17.43 10 17.22 10 17 10.04C15.39 10.32 14.26 11.73 14.26 13.46C14.26 15.34 14.26 17.23 14.26 19.1C14.26 19.37 14.2 19.48 13.94 19.45C13.74 19.43 13.55 19.45 13.35 19.45C12.28 19.51 11.41 20.22 11.1 21.28C10.81 22.31 11.15 23.42 11.96 24.08C12.54 24.55 13.21 24.65 13.9 24.6C14.17 24.58 14.25 24.67 14.25 24.96C14.25 33.2 14.25 41.43 14.25 49.66C14.25 50.1467 14.0266 50.39 13.58 50.39C13.5 50.39 13.43 50.39 13.35 50.39C12.08 50.44 11.06 51.5 11 52.82C10.94 54.19 11.88 55.36 13.16 55.51C13.41 55.54 13.66 55.55 13.91 55.53C14.19 55.51 14.25 55.62 14.25 55.9C14.25 57.74 14.25 59.57 14.25 61.41C14.25 63.48 15.65 64.98 17.61 64.99C19.75 64.99 21.89 64.99 24.03 64.99C31.41 64.99 38.8 64.99 46.19 64.99C50.74 64.99 55.29 64.99 59.84 64.99C60.62 64.99 61.32 64.73 61.91 64.2C62.75 63.45 63.1 62.47 63.1 61.31C63.09 53.37 63.1 45.44 63.1 37.5C63.1 29.56 63.1 21.55 63.1 13.59C63.1 11.45 61.72 10 59.71 10C45.7 10 31.69 10 17.67 10H17.65ZM13.48 22.88C12.98 22.88 12.64 22.52 12.64 22.04C12.64 21.55 12.98 21.18 13.47 21.17C14.41 21.16 15.36 21.17 16.3 21.17C17.24 21.17 18.16 21.17 19.1 21.17C19.49 21.17 19.8 21.39 19.9 21.75C20 22.1 19.93 22.41 19.68 22.66C19.51 22.83 19.31 22.89 19.08 22.89C17.21 22.89 15.34 22.89 13.48 22.89V22.88ZM13.55 53.81C13.01 53.81 12.64 53.46 12.64 52.96C12.64 52.46 12.99 52.1 13.53 52.1C14.46 52.1 15.38 52.1 16.3 52.1C17.22 52.1 18.1 52.1 19 52.1C19.59 52.1 19.94 52.42 19.94 52.95C19.94 53.48 19.59 53.82 19.02 53.82C17.19 53.82 15.38 53.82 13.55 53.82V53.81ZM22.72 63.26C20.99 63.26 19.27 63.26 17.54 63.26C16.61 63.26 15.9 62.53 15.9 61.54C15.89 59.64 15.9 57.73 15.89 55.83C15.89 55.58 15.97 55.53 16.19 55.53C17.16 55.54 18.12 55.55 19.09 55.53C20.99 55.5 22.17 53.52 21.32 51.78C20.88 50.9 20.17 50.41 19.22 50.39C18.22 50.37 17.22 50.37 16.22 50.39C15.98 50.39 15.89 50.34 15.89 50.05C15.89 45.43 15.89 40.81 15.89 36.18C15.89 32.44 15.89 28.7 15.89 24.97C15.89 24.66 15.97 24.59 16.25 24.59C17.25 24.61 18.25 24.61 19.25 24.59C20.44 24.56 21.45 23.57 21.57 22.36C21.7 21.03 20.93 19.84 19.73 19.53C19.49 19.47 19.24 19.46 18.99 19.46C18.06 19.46 17.12 19.46 16.19 19.46C15.96 19.46 15.9 19.39 15.9 19.15C15.9 17.25 15.9 15.34 15.9 13.44C15.9 12.48 16.58 11.75 17.49 11.74C19.31 11.72 21.14 11.74 22.97 11.72C23.25 11.72 23.22 11.89 23.22 12.07V37.54V62.76C23.22 63.37 23.25 63.28 22.73 63.28L22.72 63.26ZM59.69 11.72C60.59 11.72 61.22 12.21 61.41 13.04C61.45 13.21 61.46 13.4 61.46 13.59C61.46 22.97 61.46 32.35 61.46 41.73V61.41C61.46 62.38 60.99 63.05 60.18 63.23C60.03 63.26 59.86 63.27 59.7 63.27C51.23 63.27 42.77 63.27 34.31 63.27C31.28 63.27 28.25 63.27 25.22 63.28C24.9 63.28 24.83 63.19 24.83 62.86C24.83 54.4 24.83 45.95 24.83 37.49V12.18C24.83 11.72 24.83 11.72 25.27 11.72C34.41 11.72 43.56 11.72 52.71 11.72C55.03 11.72 57.36 11.72 59.68 11.72H59.69Z"
                            fill="black"
                          />
                          <path
                            d="M46.44 38.19C49.02 36.77 50.64 34.06 50.64 31.08C50.64 26.6 46.99 22.95 42.51 22.95C38.03 22.95 34.38 26.6 34.38 31.08C34.38 34.06 36.01 36.77 38.58 38.19C33.76 39.85 30.45 44.43 30.45 49.57C30.45 50.03 30.83 50.41 31.29 50.41C31.75 50.41 32.13 50.03 32.13 49.57C32.13 43.85 36.78 39.2 42.5 39.2C48.22 39.2 52.87 43.85 52.87 49.57C52.87 50.03 53.25 50.41 53.71 50.41C54.17 50.41 54.55 50.03 54.55 49.57C54.55 44.42 51.24 39.85 46.42 38.19H46.44ZM48.96 31.08C48.96 34.63 46.07 37.53 42.51 37.53C38.95 37.53 36.06 34.64 36.06 31.08C36.06 27.52 38.95 24.63 42.51 24.63C46.07 24.63 48.96 27.52 48.96 31.08Z"
                            fill="black"
                          />
                        </svg>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
              <h1 className="text-center font-medium mt-2 text-xs sm:text-sm">
                {lang.contacts}
              </h1>
            </Link>
          </div>

          <div className="w-full py-4 sm:p-4 bg-white rounded-lg shadow-md hover:bg-purple-100 duration-300">
            <Link className="text-black" href="/dashboard/kedipay">
              <div className="sc-jXbUNg cPOoWY">
                <div className="w-auto cursor-pointer  rounded-lg p-2 ">
                  <div className="  items-center  my-2 lg:my-0 mx-2 text-md ">
                    <div className="block lg:flex justify-center items-center">
                      <div className="flex items-center justify-center">
                        <svg
                          xmlns="http://www.w3.org/2000/svg"
                          width="118.26"
                          height="100.155"
                          viewBox="0 0 118.26 100.155"
                          className="h-12 sm:h-16 w-12 sm:w-16"
                        >
                          <g transform="translate(0.751 0.777)">
                            <g transform="translate(0.001 9.86)">
                              <path
                                d="M110.576,64.573a6.412,6.412,0,0,1-6.393,6.393h-86.9a6.412,6.412,0,0,1-6.393-6.393V35.832a7.366,7.366,0,0,1,6.351-7.124L104.223,18.7a5.581,5.581,0,0,1,6.351,5.662V64.573Z"
                                transform="translate(-10.892 -18.656)"
                                fill="none"
                                stroke="#231f20"
                                strokeMiterlimit={10}
                                strokeWidth="1.5"
                              />
                            </g>
                            <path
                              d="M97.871,58.793,37.758,83.143A8.461,8.461,0,0,1,26.74,78.478L15.214,50.02A8.461,8.461,0,0,1,19.878,39L79.991,14.651a8.461,8.461,0,0,1,11.018,4.665l11.527,28.458A8.462,8.462,0,0,1,97.871,58.793Z"
                              transform="translate(-6.705 -14.03)"
                              fill={isDisabled}
                              stroke="#000"
                              strokeMiterlimit={10}
                              strokeWidth="1.5"
                            />
                            <path
                              d="M115.8,98.991H19.351a8.46,8.46,0,0,1-8.46-8.46V34.573a8.46,8.46,0,0,1,8.46-8.46H115.8a8.46,8.46,0,0,1,8.46,8.46V90.53A8.463,8.463,0,0,1,115.8,98.991Z"
                              transform="translate(-10.892 -0.363)"
                              fill="#fff"
                              stroke="#231f20"
                              strokeMiterlimit={10}
                              strokeWidth="1.5"
                            />
                            <path
                              d="M86.8,58.8H56.665a9.652,9.652,0,0,1-9.652-9.652h0A10.709,10.709,0,0,1,57.722,38.435H86.8V58.8Z"
                              transform="translate(29.964 13.574)"
                              fill="#fff"
                              stroke="#231f20"
                              strokeMiterlimit={10}
                              strokeWidth="1.5"
                            />
                            <ellipse
                              cx="2.749"
                              cy="2.749"
                              rx="2.749"
                              ry="2.749"
                              transform="translate(88.12 59.768)"
                              fill="#231f20"
                            />
                          </g>
                        </svg>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
              <h1 className="text-center font-medium mt-2 text-xs sm:text-sm">Keuangan</h1>
            </Link>
          </div>
          <div className="w-full py-4 sm:p-4 bg-[#EAE9E9] rounded-lg shadow-md  duration-300">
            <Link className="text-black" href="#" style={{pointerEvents: 'none'}}>
              <div className="sc-jXbUNg cPOoWY">
                <div className="w-auto cursor-pointer  rounded-lg p-2 ">
                  <div className="  items-center  my-2 lg:my-0 mx-2 text-md ">
                    <div className="block lg:flex justify-center items-center">
                      <div className="flex items-center justify-center">
                        <svg
                          xmlns="http://www.w3.org/2000/svg"
                          width="92.151"
                          height="126.259"
                          viewBox="0 0 92.151 126.259"
                          className="h-12 sm:h-16 w-12 sm:w-16 "
                        >
                          <g transform="translate(0.75 0.75)">
                            <g transform="translate(0 21.825)">
                              <path
                                d="M60.779,108.118H29.046a2.131,2.131,0,0,1-2.091-2.155v-72.9A2.135,2.135,0,0,1,29.046,30.9H60.779a2.035,2.035,0,0,1,1.529.711v-2.7a5.323,5.323,0,0,0-5.214-5.394H26.054a5.322,5.322,0,0,0-5.216,5.394v78.4a5.323,5.323,0,0,0,5.216,5.389h31.04a5.311,5.311,0,0,0,5.2-5.281A2.018,2.018,0,0,1,60.779,108.118Z"
                                transform="translate(-20.838 -16.528)"
                                fill={isDisabled}
                                stroke="#000"
                                strokeMiterlimit={10}
                                strokeWidth="1.5"
                              />
                              <path
                                d="M57.5,27.823h7.132s6.684-.711,6.684,8.02v72.189s-.386,8.823-6.91,8.823c-.162.008-6.928,0-6.928,0l-21.262,6.277s-7.781,3.419-7.778-6.277c-.01-5,0-42.776,0-42.776V27.786s-.713-8.1,5.767-6.856C37.876,22.022,57.5,27.823,57.5,27.823Z"
                                transform="translate(-8.908 -20.801)"
                                fill="#fff"
                                stroke="#000"
                                strokeMiterlimit={10}
                                strokeWidth="1.5"
                              />
                            </g>
                            <path
                              d="M26.231,0A26.231,26.231,0,1,1,0,26.231,26.231,26.231,0,0,1,26.231,0Z"
                              transform="translate(38.19 0)"
                                fill={isDisabled}
                              stroke="#000"
                              strokeWidth="1.5"
                            />
                            <path
                              d="M45.859,14.537V35.07H58.9"
                              transform="translate(18.562 -8.839)"
                              fill="none"
                              stroke="#000"
                              strokeLinecap="round"
                              strokeMiterlimit={10}
                              strokeWidth={2}
                            />
                          </g>
                        </svg>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
              <h1 className="text-center font-medium mt-2 text-xs sm:text-sm">Jadwal Kelas</h1>
            </Link>
          </div>

          <div className="w-full py-4 sm:p-4 bg-[#EAE9E9] rounded-lg shadow-md  duration-300">
            <Link className="text-black" href="#" style={{pointerEvents: 'none'}}>
              <div className="sc-jXbUNg cPOoWY">
                <div className="w-auto cursor-pointer  rounded-lg p-2 ">
                  <div className="  items-center  my-2 lg:my-0 mx-2 text-md ">
                    <div className="block lg:flex justify-center items-center">
                      <div className="flex items-center justify-center">
                        <svg
                          xmlns="http://www.w3.org/2000/svg"
                          width="90.838"
                          height="120.518"
                          viewBox="0 0 90.838 120.518"
                          className="h-12 sm:h-16 w-12 sm:w-16 "
                        >
                          <g transform="translate(0.75 0.75)">
                            <g transform="translate(0 13.376)">
                              <path
                                d="M94.831,106.748a4.4,4.4,0,0,1-4.385,4.385H21.571a4.4,4.4,0,0,1-4.385-4.385V20.47a4.4,4.4,0,0,1,4.385-4.385H90.446a4.4,4.4,0,0,1,4.385,4.385Z"
                                transform="translate(-17.186 -16.085)"
                                fill="#fff"
                              />
                              <path
                                d="M94.831,106.748a4.4,4.4,0,0,1-4.385,4.385H21.571a4.4,4.4,0,0,1-4.385-4.385V20.47a4.4,4.4,0,0,1,4.385-4.385H90.446a4.4,4.4,0,0,1,4.385,4.385Z"
                                transform="translate(-17.186 -16.085)"
                                fill="none"
                                stroke="#000"
                                strokeMiterlimit={10}
                                strokeWidth="1.5"
                              />
                            </g>
                            <path
                              d="M62.146,17.21H53.021a7.227,7.227,0,0,0-14.455,0h-9.12A3.7,3.7,0,0,0,25.752,20.9v5.3a3.7,3.7,0,0,0,3.694,3.688h32.7A3.7,3.7,0,0,0,65.832,26.2V20.9A3.7,3.7,0,0,0,62.146,17.21Z"
                              transform="translate(-6.972 -9.984)"
                              fill={isDisabled}
                              stroke="#000"
                              strokeMiterlimit={10}
                              strokeWidth="1.5"
                            />
                            <g transform="translate(9.011 27.475)">
                              <g transform="translate(19.741 6.759)">
                                <path
                                  d="M70.621,27.965H31.486a1.183,1.183,0,1,1,0-2.366H70.623a1.183,1.183,0,1,1,0,2.366Z"
                                  transform="translate(-30.3 -25.599)"
                                />
                                <g transform="translate(0 6.461)">
                                  <path
                                    d="M53.092,30.912H31.486a1.183,1.183,0,1,1,0-2.366H53.092a1.183,1.183,0,1,1,0,2.366Z"
                                    transform="translate(-30.3 -28.546)"
                                    fill={isDisabled}
                                  />
                                </g>
                              </g>
                              <rect
                                width="13.532"
                                height="13.532"
                                transform="translate(0 3.907)"
                                fill="#fff"
                                stroke="#000"
                                strokeMiterlimit={10}
                                strokeWidth="0.5"
                              />
                              <path
                                d="M22.419,29.576l3.69,6.459L37.064,22.516"
                                transform="translate(-19.957 -22.516)"
                                fill="none"
                                stroke={isDisabled}
                                strokeLinecap="round"
                                strokeMiterlimit={10}
                                strokeWidth="0.75"
                              />
                              <g transform="translate(19.741 29.183)">
                                <path
                                  d="M70.621,38.2H31.486a1.185,1.185,0,1,1,0-2.37H70.623a1.185,1.185,0,1,1,0,2.37Z"
                                  transform="translate(-30.3 -35.827)"
                                />
                                <g transform="translate(0 6.459)">
                                  <path
                                    d="M53.092,41.141H31.486a1.184,1.184,0,1,1,0-2.368H53.092a1.184,1.184,0,1,1,0,2.368Z"
                                    transform="translate(-30.3 -38.773)"
                                    fill={isDisabled}
                                  />
                                </g>
                              </g>
                              <g transform="translate(0 22.424)">
                                <rect
                                  width="13.532"
                                  height="13.532"
                                  transform="translate(0 3.911)"
                                  fill="#fff"
                                  stroke="#000"
                                  strokeMiterlimit={10}
                                  strokeWidth="0.5"
                                />
                                <path
                                  d="M22.419,39.8l3.69,6.461L37.064,32.744"
                                  transform="translate(-19.957 -32.744)"
                                  fill="none"
                                  stroke={isDisabled}
                                  strokeLinecap="round"
                                  strokeMiterlimit={10}
                                  strokeWidth="0.75"
                                />
                              </g>
                            </g>
                            <g transform="translate(43.727 71.293)">
                              <path
                                d="M50.1,57.5a1.388,1.388,0,0,1-.25.772.887.887,0,0,1-.546.36.952.952,0,0,1-.647-.112,1.386,1.386,0,0,1-.548-.594,20.66,20.66,0,0,0-.423-2.3q-.27-1.131-.647-2.249l-6.588.048a.52.52,0,0,1-.112-.011.568.568,0,0,0-.114-.013Q39.6,55.509,39.1,57.772a1.182,1.182,0,0,1-.572.695.981.981,0,0,1-.708.075.944.944,0,0,1-.548-.447,1.264,1.264,0,0,1-.112-.9,73.154,73.154,0,0,1,1.975-7.049,44.89,44.89,0,0,1,2.9-6.726.875.875,0,0,1,.7-.6.891.891,0,0,1,.52-.039.94.94,0,0,1,.423.237c.015,0,.024,0,.024.013s.009.011.026.011a11.831,11.831,0,0,1,2.164,2.811q.871,1.565,1.592,3.183c.182.381.351.763.511,1.142s.309.765.458,1.144l1.318-.024a1.324,1.324,0,0,1,.7.447.944.944,0,0,1,.213.609.968.968,0,0,1-.263.6,1.257,1.257,0,0,1-.721.386h-.52q.326,1.019.559,2.052A19.108,19.108,0,0,1,50.1,57.5Zm-6.864-11.96q-.7,1.417-1.28,2.883t-1.083,2.96l5.422-.05q-.322-.747-.658-1.467t-.686-1.443c-.263-.48-.531-.969-.794-1.467A9.98,9.98,0,0,0,43.231,45.536Z"
                                transform="translate(-37.131 -42.189)"
                              />
                              <path
                                d="M43.271,47.152a.7.7,0,0,1-.193-.476.72.72,0,0,1,.21-.5.859.859,0,0,1,.634-.246h2.035V43.232a1.026,1.026,0,0,1,.292-.548.675.675,0,0,1,.458-.182.72.72,0,0,1,.449.164.934.934,0,0,1,.3.513v2.749H49.9a.907.907,0,0,1,.511.3.7.7,0,0,1,.164.449.655.655,0,0,1-.184.456,1.058,1.058,0,0,1-.548.294H47.456V50.18a.889.889,0,0,1-.246.634.734.734,0,0,1-.5.21.719.719,0,0,1-.476-.193.918.918,0,0,1-.274-.594v-2.8H43.867A.967.967,0,0,1,43.271,47.152Z"
                                transform="translate(-30.039 -42.502)"
                              />
                            </g>
                            <g transform="translate(33.807 60.756)">
                              <ellipse
                                cx="18.761"
                                cy="18.761"
                                rx="18.761"
                                ry="18.761"
                                fill="none"
                                stroke="#000"
                                strokeMiterlimit={10}
                                strokeWidth="1.3"
                              />
                              <rect
                                width="10.087"
                                height="1.725"
                                transform="translate(32.126 31.95) rotate(47.249)"
                                fill="#fff"
                                stroke="#000"
                                strokeMiterlimit={10}
                                strokeWidth={1}
                              />
                              <rect
                                width="27.581"
                                height="4.714"
                                transform="matrix(0.679, 0.734, -0.734, 0.679, 36.643, 34.638)"
                                fill="#fff"
                                stroke="#000"
                                strokeMiterlimit={10}
                                strokeWidth="1.3"
                              />
                            </g>
                          </g>
                        </svg>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
              <h1 className="text-center font-medium mt-2 text-xs sm:text-sm">Monitor Nilai</h1>
            </Link>
          </div>

          <div className="w-full py-4 sm:p-4 bg-[#EAE9E9] rounded-lg shadow-md  duration-300">
            <Link className="text-black" href="#" style={{pointerEvents: 'none'}}>
              <div className="sc-jXbUNg cPOoWY">
                <div className="w-auto cursor-pointer  rounded-lg p-2 ">
                  <div className="  items-center  my-2 lg:my-0 mx-2 text-md ">
                    <div className="block lg:flex justify-center items-center">
                      <div className="flex items-center justify-center">
                        <svg
                          xmlns="http://www.w3.org/2000/svg"
                          width="120.487"
                          height="102.695"
                          viewBox="0 0 120.487 102.695"
                          className="h-12 sm:h-16 w-12 sm:w-16 "
                        >
                          <g transform="translate(0.751 0.75)">
                            <path
                              d="M73.01,46.157H32.342a22.5,22.5,0,0,0-22.5,22.5V78.04H95.51V68.653A22.5,22.5,0,0,0,73.01,46.157Z"
                              transform="translate(-9.847 23.155)"
                              fill="#fff"
                              stroke="#000"
                              strokeMiterlimit={10}
                              strokeWidth="1.5"
                            />
                            <ellipse
                              cx="24.656"
                              cy="24.656"
                              rx="24.656"
                              ry="24.656"
                              transform="translate(18.173 12.461)"
                              fill={isDisabled}
                              stroke="#000"
                              strokeMiterlimit={10}
                              strokeWidth="1.5"
                            />
                            <path
                              d="M66.652,14.095a23.6,23.6,0,0,0-12.1,43.859l-7.287,9.655,21.1-6.386a23.595,23.595,0,0,0-1.712-47.128ZM79.137,37.283,75.5,35.27,68.084,43.6l-5.977-4.564-6.317,7.5-3.158-2.707,9.363-10.489,5.64,4.512,4.514-4.737L68.8,31.083l10.336-2.774v8.974Z"
                              transform="translate(28.735 -14.095)"
                              fill={isDisabled}
                              stroke="#000"
                              strokeMiterlimit={10}
                              strokeWidth="1.5"
                            />
                          </g>
                        </svg>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
              <h1 className="text-center font-medium mt-2 text-xs sm:text-sm">Bimbingan</h1>
            </Link>
          </div>


        </div>
      </div>
    </div>
  );
};

export default Dashboard;

export async function getServerSideProps(context: any) {
  try {
    const session = await getSession(context);
    if (session) {
      return {
        props: { user: session.user },
      };
    } else {
      return {
        redirect: {
          destination: `/login`,
          permanent: false,
        },
      };
    }
  } catch (error) {
    console.error("Kesalahan:", error);
    return {
      props: {
        error: "Terjadi kesalahan saat memuat sesi pengguna",
      },
    };
  }
}
