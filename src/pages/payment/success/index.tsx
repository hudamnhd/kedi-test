import { useState } from "react";
import { Threedots } from "@/assets/icon";
import { useQuery  } from "@tanstack/react-query";
import axios from "axios";
import { getSession } from "next-auth/react";
import Image from "next/image";
import { useRouter } from "next/router";
import React from "react";

function formatCurrency(data) {
  if (
    isNaN(data) &&
    data !== null &&
    data !== undefined &&
    data !== "" &&
    data !== "0"
  ) {
    data = parseFloat(data);
  }
  const amount =
    data === null || data === undefined || data === 0 || data === "0"
      ? 0
      : data;

  const formattedAmount = new Intl.NumberFormat("id-ID", {
    style: "currency",
    currency: "IDR",
    minimumFractionDigits: 2,
  }).format(amount);

  return formattedAmount;
}

const SuccessPayment = ({ user, res, all_data, invoice }: any) => {
console.log('invoice:',  invoice);
    const[statusPay, setStatusPay ] = useState("PENDING");

  const sessionToken = user?.accessToken;
  const { data } = useQuery( ["transaction_status"], async () => {
      if (!sessionToken) return null;

      const response = await axios.get(
        process.env.API_BE + `/api/finance/invoice/detail/${invoice}`,
        {
          headers: {
            Authorization: `Bearer ${sessionToken}`,
          },
        },
      );
      if (response.status !== 200) {
        throw new Error("Gagal mengambil data");
      }

        const financePayStatus = response?.data?.data?.financePayList?.map((d) => d.status);

        if (financePayStatus && financePayStatus.length > 0) {
            setStatusPay(financePayStatus[0])
        }
      return response.data.data;
    },
    { refetchInterval: statusPay === "PENDING" ? 10000 : false },
  );


  return (
    <div className="flex h-screen w-full items-center justify-center bg-gray-600">

    <div className="relative w-full max-w-md rounded bg-gray-50 px-6 pt-8 shadow-lg">
        {statusPay === "PENDING" &&  ( <div className="absolute inset-0 flex items-center justify-center bg-white/30"> <Threedots width={60} height={60} strokeWidth={2} fill="#9D21E6" /> </div>)}
        <img
          src="/static/images/logomain.svg"
          alt="chippz"
          className="mx-auto w-auto py-4"
        />
        <div className="flex flex-col  gap-1.5 mb-2">
          <div className="text-sm"> Hello  <span className="font-bold">{user?.fullName}</span> </div>
          <div className="font-medium">
            {" "}
            Pembayaran untuk kelas berbayar {statusPay === "PENDING" ? "masih dalam proses" : "telah berhasil"}
          </div>
          <div className="text-sm">
           {statusPay === "PENDING" ? "Mohon ditunggu" : "Terima kasih"} , ya! Berikut detail pembayaranmu:
          </div>
        </div>
        <div className="flex flex-col gap-3 border border-gray-300 rounded-[10px] p-4 text-xs">
          <p className="flex justify-between">
            <span className="text-gray-600">Invoice No.:</span>
            <span>{all_data?.invoiceIdentifier}</span>
          </p>
          <p className="flex justify-between">
            <span className="text-gray-600">Jenis:</span>
            <span>Access Paid Class</span>
          </p>
          <p className="flex justify-between">
            <span className="text-gray-600">Nama Kelas:</span>
            <span className="font-bold">{all_data?.name}</span>
          </p>
          <p className="flex justify-between">
            <span className="text-gray-600">Metode Pembayaran:</span>
            <span>{all_data?.paymentMethod?.description}</span>
          </p>
        </div>
        <div className="flex flex-col gap-3 pb-6 pt-4 text-xs">
          <h3 className="text-lg font-medium">Ringkasan Pembayaran</h3>
          <table className="w-full">
            <tbody>
              <tr className="h-8">
                <td className="text-left text-gray-700">Subtotal</td>
                <td className="text-right text-gray-700">
                  {formatCurrency(all_data?.productReference?.price)}
                </td>
              </tr>
              <tr className="h-8">
                <td className="text-left text-gray-700">Biaya Layanan</td>
                <td className="text-right text-gray-700">
                  {formatCurrency(all_data?.paymentFee)}
                </td>
              </tr>
              <tr className="h-8">
                <td className="text-left text-gray-700">Jasa Aplikasi</td>
                <td className="text-right text-gray-700">
                  {formatCurrency(0)}
                </td>
              </tr>
              <tr className="h-2"></tr>
            </tbody>
            <tfoot className="border-t-2 border-dashed">
              <tr className="mb-10 h-10 text-[14px] text-[#9D21E6]">
                <td className="text-left font-bold">Total Pembayaran</td>
                <td className="text-right font-bold">
                  {formatCurrency(all_data?.totalAmount)}
                </td>
              </tr>
            </tfoot>
          </table>
        </div>
      </div>
    </div>
  );
};

export default SuccessPayment;

export async function getServerSideProps(context: any) {
  const { success, invoice, order_id, status_code, transaction_status } =
    context.query;
  try {
    const session = await getSession(context);
    if (session) {
      const session = await getSession(context);
      const response = await fetch(
        process.env.API_URL + `/api/finance/invoice/detail/${invoice}`,
        {
          method: "GET",
          headers: {
            "Content-Type": "application/json",
            Authorization: `Bearer ${session?.user?.accessToken}`,
          },
        },
      );

      const res = await response.json(); // atau response.text();

      const financePayList = res?.data?.financePayList?.filter(
        (d) => d.status === "PENDING" || d.status === "SETTLEMENT",
      );
      const history = res?.data?.history?.filter(
        (d) => d.status === "PENDING" || d.status === "SETTLEMENT",
      );
      const items = res?.data?.items?.filter(
        (d) => d.invoiceIdentifier === financePayList[0]?.invoiceIdentifier,
      );
      const all_data = { ...financePayList[0], ...history[0], ...items[0] };

      const { user } = session;
      return {
        props: { user, res, all_data, invoice },
      };
    } else {
      return {
        redirect: {
          destination: `/login`,
          permanent: false,
        },
      };
    }
  } catch (error) {
    console.error("Kesalahan:", error);
    return {
      props: {
        error: "Terjadi kesalahan saat memuat sesi pengguna",
      },
    };
  }
}
