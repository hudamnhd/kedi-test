import React, { useState, useEffect } from "react";
import { Threedots } from "@/assets/icon";
import { useQuery, useQueryClient } from "@tanstack/react-query";
import axios from "axios";
import { getSession } from "next-auth/react";
import Image from "next/image";
import { useRouter } from "next/router";
import { Navbar } from "@/components/navbar/navbar";
import Link from "next/link";

function formatCurrency(data) {
  if (
    isNaN(data) &&
    data !== null &&
    data !== undefined &&
    data !== "" &&
    data !== "0"
  ) {
    data = parseFloat(data);
  }
  const amount =
    data === null || data === undefined || data === 0 || data === "0"
      ? 0
      : data;

  const formattedAmount = new Intl.NumberFormat("id-ID", {
    style: "currency",
    currency: "IDR",
    minimumFractionDigits: 2,
  }).format(amount);

  return formattedAmount;
}

const SuccessPayment = ({ user, res, all_data, invoiceIdentifier }: any) => {
  const [statusPay, setStatusPay] = useState("");
  const [linkPay, setLinkPay] = useState({});
  const router = useRouter();
  const queryClient = useQueryClient();
  const sessionToken = user?.accessToken;

  const { data, isLoading } = useQuery(
    ["transaction_status", invoiceIdentifier],
    async () => {
      if (!sessionToken) return null;

      const response = await axios.get(
        process.env.API_BE + `/api/finance/invoice/detail/${invoiceIdentifier}`,
        {
          headers: {
            Authorization: `Bearer ${sessionToken}`,
          },
        },
      );
      if (response.status !== 200) {
        throw new Error("Gagal mengambil data");
      }

      const financePayStatus = response?.data?.data?.financePayList?.map(
        (d) => d.status,
      );
      const financePayPending = response?.data?.data?.financePayList?.filter(
        (d) => d.status === "PENDING",
      );
      if (financePayPending.length > 0) {
        setLinkPay({
          url: financePayPending[0].data?.url,
          redirect_url: financePayPending[0].data?.data?.redirect_url,
        });
      }

      if (response?.data?.data?.status) {
        setStatusPay(response?.data?.data?.status);
      } else if (financePayStatus && financePayStatus.length > 0) {
        setStatusPay(financePayStatus[0]);
      }

      return response.data.data;
    },
    {},
  );

  useEffect(() => {}, [router]);

  const status =
    statusPay === "SETTLEMENT"
      ? "Sudah Dibayar"
      : statusPay === "CANCEL" ||
          statusPay === "EXPIRE" ||
          statusPay === "EXPIRED"
        ? "Dibatalkan"
        : statusPay === "awaiting-approval"
          ? "Menunggu Disetujui"
          : statusPay === "PENDING"
            ? "Menunggu Pembayaran"
            : statusPay === "NEW" || statusPay === "BARU"
              ? "Belum dibayar"
              : "Dibatalkan";

  const textStatus =
    statusPay === "SETTLEMENT"
      ? "text-[#1DA26A]"
      : statusPay === "CANCEL" ||
          statusPay === "EXPIRE" ||
          statusPay === "EXPIRED"
        ? "text-red-700"
        : statusPay === "PENDING"
          ? "text-yellow-500"
          : statusPay === "awaiting-approval"
            ? "text-yellow-600"
            : statusPay === "NEW" || statusPay === "BARU"
              ? "text-amber-700"
              : "text-red-700";

  const payMethodShow = statusPay === "SETTLEMENT" || statusPay === "PENDING";
  const isNewTrans = statusPay === "NEW" || statusPay === "BARU";
  const classId = all_data?.product?.relId || all_data?.productReference?.relId;
  const isWidth = isNewTrans || statusPay === "PENDING";

  const { data: detailClass } = useQuery(
    ["detailClass", classId],
    async () => {
      if (!sessionToken) return null;

      const response = await axios.get(
        process.env.API_BE + `/api/class/${classId}`,
        {
          headers: {
            Authorization: `Bearer ${sessionToken}`,
          },
        },
      );
      if (response.status !== 200) {
        throw new Error("Gagal mengambil data");
      }

      return response.data.data;
    },
    {},
  );

  useEffect(() => {
    if (statusPay === "SETTLEMENT" && detailClass) {

      const storedData = localStorage.getItem("channelActiveClass");

      const rooms = JSON.parse(storedData ?? "[]");

      const newRoom = "group." + detailClass?.chatRoom?.id;

      if (!rooms.includes(newRoom)) {
        rooms.push(newRoom);
        localStorage.setItem("channelActiveClass", JSON.stringify(rooms));
      } else {
        console.log("SUDAH JOIN", newRoom);
      }
    }
  }, [statusPay, detailClass]);

  return (
    <div className="bg-[#FAFAFA]">
      <Navbar user={user} />
      <div className="bg-[#FAFAFA] overflow-y-auto pt-[4.4rem] sm:pt-[5.2rem] pb-[3.8rem] lg:pb-0 pb-2.5 className flex flex-col min-h-screen w-full max-w-fit m-auto w-full px-5">
        <div className="relative w-full max-w-md rounded bg-gray-50 px-6 pt-8 shadow-lg my-auto">
          {false && (
            <div className="absolute inset-0 flex items-center justify-center bg-white/30">
              {" "}
              <Threedots
                width={60}
                height={60}
                strokeWidth={2}
                fill="#9D21E6"
              />{" "}
            </div>
          )}
          <img
            src="/static/images/logomain.svg"
            alt="chippz"
            className="mx-auto w-auto py-4"
          />
          <div className="flex flex-col  gap-1.5 mb-2">
            <div className="text-sm">
              {" "}
              Hello <span className="font-bold">{user?.fullName}</span>{" "}
            </div>

            {isLoading ? (
              <div className="absolute inset-0 flex items-center justify-center bg-white/30">
                {" "}
                <Threedots
                  width={60}
                  height={60}
                  strokeWidth={2}
                  fill="#9D21E6"
                />{" "}
              </div>
            ) : (
              statusPay !== "" && (
                <div className="font-medium sm:text-[15px] text-sm">
                  Status Pembayaran kamu{" "}
                  <span className={textStatus}>{status}</span>
                </div>
              )
            )}
            <div className="text-sm">
              {statusPay === "SETTLEMENT" ? (
                "Terima kasih , ya! Berikut detail pembayaranmu:"
              ) : statusPay === "PENDING" ? (
                <>
                  <span>Jika kamu sudah bayar Mohon ditunggu</span>{" "}
                  <span className="block">Berikut detail pembayaranmu:</span>
                </>
              ) : (
                "Berikut detail pembayaranmu:"
              )}
            </div>
          </div>
          <div className="flex flex-col gap-3 border border-gray-300 rounded-[10px] p-4 text-xs">
            <p className="flex sm:flex-row flex-col gap-1 justify-between">
              <span className="text-gray-600">Invoice No. :</span>
              <span>{all_data?.invoiceIdentifier}</span>
            </p>
            <p className="flex sm:flex-row flex-col gap-1 justify-between">
              <span className="text-gray-600">Jenis:</span>
              <span>Access Paid Class</span>
            </p>
            <p className="flex sm:flex-row flex-col gap-1 justify-between">
              <span className="text-gray-600">Nama Kelas:</span>
              <span className="font-bold">{all_data?.name}</span>
            </p>
            {payMethodShow && (
              <div className="flex sm:flex-row flex-col gap-1 justify-between sm:items-center">
                <span className="text-gray-600">Metode Pembayaran:</span>
                <div className="flex flex-col max-w-[100px]">
                  {all_data?.paymentMethod?.paymentIcon?.url ? (
                    <Image
                      src={all_data?.paymentMethod?.paymentIcon?.url ?? ""}
                      height={100}
                      width={100}
                      alt={
                        all_data?.paymentMethod?.paymentIcon?.originalName ??
                        "Logo"
                      }
                    />
                  ) : (
                    <span className="text-gray-700 font-medium">
                      {all_data?.paymentMethod?.name ?? "Bank"}
                    </span>
                  )}
                </div>
              </div>
            )}

            {payMethodShow && (
              <p className="flex sm:flex-row flex-col gap-1 justify-end sm:items-center opacity-80">
                <span className="text-xs italic">
                  *{all_data?.paymentMethod?.description}
                </span>
              </p>
            )}
          </div>
          <div className="flex flex-col gap-3 pb-6 pt-4 text-xs">
            <h3 className="text-[16px] sm:text-lg font-medium">
              Ringkasan Pembayaran
            </h3>
            <table className="w-full">
              <tbody>
                <tr className="h-8">
                  <td className="text-left text-gray-700">Subtotal</td>
                  <td className="text-right text-gray-700">
                    {formatCurrency(all_data?.productReference?.price)}
                  </td>
                </tr>
                <tr className="h-8">
                  <td className="text-left text-gray-700">Biaya Layanan</td>
                  <td className="text-right text-gray-700">
                    {formatCurrency(all_data?.paymentFee)}
                  </td>
                </tr>
                <tr className="h-8">
                  <td className="text-left text-gray-700">Jasa Aplikasi</td>
                  <td className="text-right text-gray-700">
                    {formatCurrency(0)}
                  </td>
                </tr>
                <tr className="h-2"></tr>
              </tbody>
              <tfoot className="border-t-2 border-dashed">
                <tr className="mb-10 h-10 text-[14px] text-[#9D21E6]">
                  <td className="text-left font-bold">Total Pembayaran</td>
                  <td className="text-right font-bold">
                    {formatCurrency(
                      all_data?.totalAmount ??
                        all_data?.productReference?.price,
                    )}
                  </td>
                </tr>
              </tfoot>
            </table>
          </div>
          <div className="flex items-stretch gap-x-3 justify-center">
            <button
              onClick={() => router.back()}
              className={` ${isWidth ? "w-full" : "w-fit"}   text-sm text-purple-600  hover:scale-110 focus:outline-none flex justify-center pr-4 pl-2 py-2 rounded-lg font-bold cursor-pointer my-5
                    hover:bg-purple-200  
                    bg-gray-100 
                    text-gray-700 
                    border duration-200 ease-in-out 
                    border-purple-600 transition `}
            >
              <div className="flex leading-4">
                <svg
                  xmlns="http://www.w3.org/2000/svg"
                  width="100%"
                  height="100%"
                  fill="none"
                  viewBox="0 0 24 24"
                  stroke="currentColor"
                  strokeWidth={3}
                  strokeLinecap="round"
                  strokeLinejoin="round"
                  className="feather feather-chevron-left w-4 h-4"
                >
                  <polyline points="15 18 9 12 15 6" />
                </svg>
                Kembali
              </div>
            </button>
            {statusPay === "PENDING" && (
              <Link
                href={
                  linkPay?.url
                    ? linkPay?.url
                    : linkPay?.redirect_url
                      ? linkPay.redirect_url
                      : "#"
                }
                target="_blank"
                className="w-full text-sm bg-purple-600 text-white  hover:scale-110 focus:outline-none flex justify-center pl-4 pr-2 py-2 rounded-lg font-bold cursor-pointer my-5
                            hover:bg-purple-700  
                            text-gray-700 
                            border duration-200 ease-in-out 
                            border-purple-600 transition"
              >
                <div className="flex leading-4">
                  Bayar
                  <svg
                    xmlns="http://www.w3.org/2000/svg"
                    width="100%"
                    height="100%"
                    fill="none"
                    viewBox="0 0 24 24"
                    stroke="currentColor"
                    strokeWidth={3}
                    strokeLinecap="round"
                    strokeLinejoin="round"
                    className="feather feather-chevron-left w-4 h-4 rotate-180"
                  >
                    <polyline points="15 18 9 12 15 6" />
                  </svg>
                </div>
              </Link>
            )}
            {isNewTrans && (
              <Link
                href={classId ? "/class/join/" + classId : "#"}
                target="_blank"
                className="w-full text-sm bg-purple-600 text-white  hover:scale-110 focus:outline-none flex justify-center pl-4 pr-2 py-2 rounded-lg font-bold cursor-pointer my-5
                            hover:bg-purple-700  
                            text-gray-700 
                            border duration-200 ease-in-out 
                            border-purple-600 transition"
              >
                <div className="flex leading-4">
                  Bayar
                  <svg
                    xmlns="http://www.w3.org/2000/svg"
                    width="100%"
                    height="100%"
                    fill="none"
                    viewBox="0 0 24 24"
                    stroke="currentColor"
                    strokeWidth={3}
                    strokeLinecap="round"
                    strokeLinejoin="round"
                    className="feather feather-chevron-left w-4 h-4 rotate-180"
                  >
                    <polyline points="15 18 9 12 15 6" />
                  </svg>
                </div>
              </Link>
            )}
          </div>
        </div>
      </div>
    </div>
  );
};

export default SuccessPayment;

export async function getServerSideProps(context: any) {
  const { invoiceIdentifier } = context.params;
  try {
    const session = await getSession(context);
    if (session) {
      const session = await getSession(context);
      const response = await fetch(
        process.env.API_URL +
          `/api/finance/invoice/detail/${invoiceIdentifier}`,
        {
          method: "GET",
          headers: {
            "Content-Type": "application/json",
            Authorization: `Bearer ${session?.user?.accessToken}`,
          },
        },
      );

      const res = await response.json(); // atau response.text();

      const financePayList = res?.data?.financePayList?.filter(
        (d) => d.status === "PENDING" || d.status === "SETTLEMENT",
      );
      const history = res?.data?.history?.filter(
        (d) => d.status === "PENDING" || d.status === "SETTLEMENT",
      );
      const items = res?.data?.items?.filter(
        (d) =>
          d.invoiceIdentifier === financePayList[0]?.invoiceIdentifier ||
          d.invoiceIdentifier === res?.data?.invoiceIdentifier,
      );
      const all_data = {
        ...(financePayList[0] ?? []),
        ...history[0],
        ...items[0],
      };

      const { user } = session;
      return {
        props: { user, res, all_data, invoiceIdentifier },
      };
    } else {
      return {
        redirect: {
          destination: `/login`,
          permanent: false,
        },
      };
    }
  } catch (error) {
    console.error("Kesalahan:", error);
    return {
      props: {
        error: "Terjadi kesalahan saat memuat sesi pengguna",
      },
    };
  }
}
