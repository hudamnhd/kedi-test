import { trans as enTrans } from "@/lang/en";
import { trans as idTrans } from "@/lang/id";

const ComparisonTable = () => {
  const bIndo = Object.values(idTrans);
  const bIndoLoop = Object.values(bIndo[0]);
  const bIng = Object.values(enTrans);
  const bIngLoop = Object.values(bIng[0]);

  return (
      <div className="grid grid-cols-2 max-h-[90vh] overflow-y-auto">
        <div className="">
          {bIndoLoop.map((language) => {
            const data = Object.entries(language); // Get key-value pairs
            return (
              <div key={language}>
                {data.map(([key, value], i) => (
                  <p key={key}>
                    {i + 1}. {value}
                  </p>
                ))}
              </div>
            );
          })}
        </div>
        <div>
        {bIngLoop.map((language) => {
          const data = Object.entries(language); // Get key-value pairs
          return (
            <div key={language}>
              {data.map(([key, value], i) => (
                <p key={key}>
                  {i + 1}. {value}
                </p>
              ))}
            </div>
          );
        })}
        </div>
      </div>
  );
};

export default ComparisonTable;
