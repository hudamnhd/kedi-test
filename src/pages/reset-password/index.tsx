import { NextPage } from "next";
import Link from "next/link";
import { LoadingSpinner } from "@/components/ui/loading";
import toast from "react-hot-toast";
import { useState } from "react";
import { signIn } from "next-auth/react";
import { useMutation } from "@tanstack/react-query";
import axios from "axios";
import { useRouter } from "next/router";
import { trans } from "@/lang";
import { Label } from "@/components/ui/label";

const SignIn: NextPage = (): JSX.Element => {
  const router = useRouter();
  const locale: "en" | "id" = router.locale as "en" | "id";
  const lang = trans[locale].profile;
  const { code } = router.query;
  const [userInfo, setUserInfo] = useState({ email: "" });
  const [toggle, setToggle] = useState(false);
  const [error, setError] = useState("");
  const [confirmPassword, setConfirmPassword] = useState("");
  const [showNewPass, setShowNewPass] = useState(false);
  const [showNewRePass, setShowNewRePass] = useState(false);
  const [_user, setUser] = useState({
    code: code,
    new_password: "",
  });

  const response = useMutation(async (email: string) => {
    if (email.length === 0) return toast.error("Email field empty");
    try {
      const res = await axios.post(
        process.env.API_BE + `/api/auth/request-reset-password`,
        {
          email: String(email),
        },
      );
      toast.success(res.data.data.message);
      setToggle(true);
      return res.data.data;
    } catch (error) {
      console.error(error);
      toast.error(error.response.data.data[0]);
      throw error;
    }
  });

  interface ApiResponse {
    data: {
      message: string;
    };
  }
  interface PostResponse extends ApiResponse {}
  type PostData = {
    code: String;
    new_password: String;
  };

  const changePass = useMutation(
    async (data: PostData): Promise<PostResponse> => {
      const { code, password } = data;
      if (_user.new_password !== confirmPassword) {
        setError("Password baru dan konfirmasi password tidak cocok");
        return;
      }
      try {
        const res = await axios.post<PostResponse>(
          process.env.API_BE + `/api/auth/reset-password`,
          {
            code,
            password,
          },
        );
        toast.success(res.data.data.message);

        setUser({
          code: "",
          password: "",
        });

        setToggle(true);
        return res.data.data;
      } catch (error) {
        toast.error(error.response.data.meta.message);
        throw error;
      }
    },
  );

  const Modal = () => {
    return (
      <>
        {/* component */}
        <div
          className="min-w-screen h-screen animated fadeIn faster  fixed  left-0 top-0 flex justify-center items-center inset-0 z-50 outline-none focus:outline-none"
          id="modal-id"
        >
          <div className="absolute bg-black/50 opacity-80 inset-0 z-0" />
          <div className="w-full  max-w-lg p-5 relative mx-auto my-auto rounded-xl shadow-lg  bg-white ">
            {/*content*/}
            <div className="">
              {/*body*/}
              <div className="text-center p-5 flex flex-col items-center justify-center">
                <svg
                  xmlns="http://www.w3.org/2000/svg"
                  fill="none"
                  viewBox="0 0 24 24"
                  strokeWidth={1.7}
                  stroke="currentColor"
                  className="w-16 h-16 text-[#9D21E6]"
                >
                  <path
                    strokeLinecap="round"
                    strokeLinejoin="round"
                    d="M9 12.75L11.25 15 15 9.75M21 12a9 9 0 11-18 0 9 9 0 0118 0z"
                  />
                </svg>
                <h2 className="text-xl font-bold pt-4 pb-2 text-[#9D21E6]">
                  {code ? changePass?.data?.message : response?.data?.message}
                </h2>
              </div>
              {/*footer*/}
              <div className="max-w-[150px] sm:text-base text-sm flex flex-col gap-y-2.5 justify-center mt-2  mx-auto">
                {code ? (
                  <Link
                    className="flex justify-center cursor-pointer bg-[#9D21E6] text-white rounded-[10px] px-4 py-2.5 font-semibold w-full border border-gray-400 shadow-lg"
                    type="button"
                    href="/"
                  >
                    OK
                  </Link>
                ) : (
                  <button
                    className="flex justify-center cursor-pointer bg-[#9D21E6] text-white rounded-[10px] px-4 py-2.5 font-semibold w-full border border-gray-400 shadow-lg"
                    type="button"
                    onClick={() => setToggle(false)}
                  >
                    OK
                  </button>
                )}
              </div>
            </div>
          </div>
        </div>
      </>
    );
  };

  return (
    <div className="relative">
      {toggle && <Modal />}
      {code ? (
        <div className="h-screen flex flex-col items-center px-4 bg-[#F4EBFE]">
          <h1 className="text-[#9D21E6] font-medium text-3xl mb-4 mt-12 ">
            Set Ulang Kata Sandi
          </h1>
          <div className="mx-10 flex flex-col items-center min-h-[70%] bg-white shadow-xl rounded-xl mt-4  px-6 py-10 max-w-lg w-full">
            <img
              className="w-24"
              alt="logo kedi"
              src="static/images/logo_login.png"
            />
            <div className="mt-6 w-full">
              <div className="mb-3">
                <div className="flex items-center w-full justify-between  gap-5">
                  <Label htmlFor="sesi-mode  font-medium">
                    {lang.newPassword}
                  </Label>
                </div>
                <div className="relative">
                  <input
                    type={showNewPass ? "text" : "password"}
                    value={_user.new_password}
                    onChange={(e) =>
                      setUser({
                        ..._user,
                        new_password: e.target.value,
                      })
                    }
                    required
                    placeholder="Katasandi Baru"
                    disabled={changePass.isLoading}
                    className="disabled:bg-gray-200 mt-3 px-4 py-2.5 w-full rounded-[10px] border border-gray-400 outline-none focus:ring-2 focus:border-[#9D21E6] ring-[#9D21E6] shadow-sm sm:text-sm"
                  />
                  <div
                    onClick={() => setShowNewPass(!showNewPass)}
                    className="absolute inset-y-0 top-3 right-0 pr-3 flex items-center text-sm leading-5"
                  >
                    {showNewPass ? (
                      <svg
                        className="h-5 text-gray-700"
                        fill="none"
                        xmlns="http://www.w3.org/2000/svg"
                        viewBox="0 0 576 512"
                      >
                        <path
                          fill="currentColor"
                          d="M572.52 241.4C518.29 135.59 410.93 64 288 64S57.68 135.64 3.48 241.41a32.35 32.35 0 0 0 0 29.19C57.71 376.41 165.07 448 288 448s230.32-71.64 284.52-177.41a32.35 32.35 0 0 0 0-29.19zM288 400a144 144 0 1 1 144-144 143.93 143.93 0 0 1-144 144zm0-240a95.31 95.31 0 0 0-25.31 3.79 47.85 47.85 0 0 1-66.9 66.9A95.78 95.78 0 1 0 288 160z"
                        ></path>
                      </svg>
                    ) : (
                      <svg
                        className="h-5 text-gray-700"
                        fill="none"
                        xmlns="http://www.w3.org/2000/svg"
                        viewBox="0 0 640 512"
                      >
                        <path
                          fill="currentColor"
                          d="M320 400c-75.85 0-137.25-58.71-142.9-133.11L72.2 185.82c-13.79 17.3-26.48 35.59-36.72 55.59a32.35 32.35 0 0 0 0 29.19C89.71 376.41 197.07 448 320 448c26.91 0 52.87-4 77.89-10.46L346 397.39a144.13 144.13 0 0 1-26 2.61zm313.82 58.1l-110.55-85.44a331.25 331.25 0 0 0 81.25-102.07 32.35 32.35 0 0 0 0-29.19C550.29 135.59 442.93 64 320 64a308.15 308.15 0 0 0-147.32 37.7L45.46 3.37A16 16 0 0 0 23 6.18L3.37 31.45A16 16 0 0 0 6.18 53.9l588.36 454.73a16 16 0 0 0 22.46-2.81l19.64-25.27a16 16 0 0 0-2.82-22.45zm-183.72-142l-39.3-30.38A94.75 94.75 0 0 0 416 256a94.76 94.76 0 0 0-121.31-92.21A47.65 47.65 0 0 1 304 192a46.64 46.64 0 0 1-1.54 10l-73.61-56.89A142.31 142.31 0 0 1 320 112a143.92 143.92 0 0 1 144 144c0 21.63-5.29 41.79-13.9 60.11z"
                        ></path>
                      </svg>
                    )}
                  </div>
                </div>
              </div>

              <div className="mb-3">
                <div className="flex items-center w-full justify-between  gap-5">
                  <Label htmlFor="sesi-mode  font-medium">
                    {lang.repPassword}
                  </Label>
                </div>
                <div className="relative">
                  <input
                    type={showNewRePass ? "text" : "password"}
                    value={confirmPassword}
                    onChange={(e) => setConfirmPassword(e.target.value)}
                    required
                    placeholder="Katasandi Baru"
                    disabled={changePass.isLoading}
                    className="disabled:bg-gray-200 mt-3 px-4 py-2.5 w-full rounded-[10px] border border-gray-400 outline-none focus:ring-2 focus:border-[#9D21E6] ring-[#9D21E6] shadow-sm sm:text-sm"
                  />

                  <div
                    onClick={() => setShowNewRePass(!showNewRePass)}
                    className="absolute inset-y-0 top-3 right-0 pr-3 flex items-center text-sm leading-5"
                  >
                    {showNewRePass ? (
                      <svg
                        className="h-5 text-gray-700"
                        fill="none"
                        xmlns="http://www.w3.org/2000/svg"
                        viewBox="0 0 576 512"
                      >
                        <path
                          fill="currentColor"
                          d="M572.52 241.4C518.29 135.59 410.93 64 288 64S57.68 135.64 3.48 241.41a32.35 32.35 0 0 0 0 29.19C57.71 376.41 165.07 448 288 448s230.32-71.64 284.52-177.41a32.35 32.35 0 0 0 0-29.19zM288 400a144 144 0 1 1 144-144 143.93 143.93 0 0 1-144 144zm0-240a95.31 95.31 0 0 0-25.31 3.79 47.85 47.85 0 0 1-66.9 66.9A95.78 95.78 0 1 0 288 160z"
                        ></path>
                      </svg>
                    ) : (
                      <svg
                        className="h-5 text-gray-700"
                        fill="none"
                        xmlns="http://www.w3.org/2000/svg"
                        viewBox="0 0 640 512"
                      >
                        <path
                          fill="currentColor"
                          d="M320 400c-75.85 0-137.25-58.71-142.9-133.11L72.2 185.82c-13.79 17.3-26.48 35.59-36.72 55.59a32.35 32.35 0 0 0 0 29.19C89.71 376.41 197.07 448 320 448c26.91 0 52.87-4 77.89-10.46L346 397.39a144.13 144.13 0 0 1-26 2.61zm313.82 58.1l-110.55-85.44a331.25 331.25 0 0 0 81.25-102.07 32.35 32.35 0 0 0 0-29.19C550.29 135.59 442.93 64 320 64a308.15 308.15 0 0 0-147.32 37.7L45.46 3.37A16 16 0 0 0 23 6.18L3.37 31.45A16 16 0 0 0 6.18 53.9l588.36 454.73a16 16 0 0 0 22.46-2.81l19.64-25.27a16 16 0 0 0-2.82-22.45zm-183.72-142l-39.3-30.38A94.75 94.75 0 0 0 416 256a94.76 94.76 0 0 0-121.31-92.21A47.65 47.65 0 0 1 304 192a46.64 46.64 0 0 1-1.54 10l-73.61-56.89A142.31 142.31 0 0 1 320 112a143.92 143.92 0 0 1 144 144c0 21.63-5.29 41.79-13.9 60.11z"
                        ></path>
                      </svg>
                    )}
                  </div>
                </div>
                {error && (
                  <p className="mt-1 text-sm" style={{ color: "red" }}>
                    {error}
                  </p>
                )}
              </div>

              <div className="sm:text-base text-sm flex flex-col gap-y-2.5 justify-center mt-4 sm:mt-6  mx-auto">
                <button
                  className="flex justify-center cursor-pointer bg-[#9D21E6] text-white rounded-[10px] px-4 py-2.5 font-semibold w-full border border-gray-400 shadow-lg"
                  type="button"
                  onClick={() =>
                    changePass.mutate({
                      code,
                      password: _user.new_password,
                    })
                  }
                >
                  {response.isLoading ? (
                    <LoadingSpinner stroke={`#FFF`} size={24} />
                  ) : (
                    "Submit"
                  )}
                </button>
                <Link href="/"
                  className="flex justify-center cursor-pointer hover:bg-[#9D21E6]/10 text-[#9D21E6] ring-1 ring-[#9D21E6] bg-white rounded-[10px] px-4 py-2.5 font-semibold w-full border  shadow-lg"
                  type="button"
                >
                  {response.isLoading ? (
                    <LoadingSpinner stroke={`#FFF`} size={24} />
                  ) : (
                    "Kembali"
                  )}
                </Link>
              </div>
            </div>
          </div>
        </div>
      ) : (
        <div className="h-screen flex flex-col items-center px-4 bg-[#F4EBFE]">
          <h1 className="text-[#9D21E6] font-medium text-3xl mb-4 mt-12 ">
            Lupa Kata Sandi
          </h1>
          <div className="mx-10 flex flex-col items-center min-h-[70%] bg-white shadow-xl rounded-xl mt-4  px-6 py-10 max-w-lg w-full">
            <img
              className="w-24"
              alt="logo kedi"
              src="static/images/logo_login.png"
            />
            <h1 className="text-xl sm:text-[24px] text-center mb-4 mt-5 font-medium">
              Silakan isi kolom dibawah dengan Email Anda yang terdaftar di
              Kelas Digital
            </h1>
            <p className="text-[#686464] text-center">
              Kita akan kirimkan Anda link untuk atur ulang kata sandi
            </p>
            <div className="mt-6 w-full">
              <div className="mb-3 sm:mb-5">
                <label
                  htmlFor="UserEmail"
                  className="block text-xs sm:text-sm  font-medium text-gray-700"
                >
                  Email
                </label>

                <input
                  value={userInfo.email}
                  onChange={({ target }) =>
                    setUserInfo({ ...userInfo, email: target.value })
                  }
                  placeholder="john@rhcp.com"
                  type="email"
                  disabled={response.isLoading}
                  className="disabled:bg-gray-200 mt-1.5 sm:mt-3 px-4 py-1.5 sm:py-2.5 w-full rounded-[10px] border border-gray-400 outline-none focus:ring-2 focus:border-[#9D21E6] ring-[#9D21E6] shadow-sm sm:text-sm"
                />
              </div>

              <div className="sm:text-base text-sm flex flex-col gap-y-2.5 justify-center mt-4 sm:mt-6  mx-auto">
                <button
                  className="flex justify-center cursor-pointer bg-[#9D21E6] text-white rounded-[10px] px-4 py-2.5 font-semibold w-full border border-gray-400 shadow-lg"
                  type="button"
                  onClick={() => response.mutate(userInfo.email)}
                >
                  {response.isLoading ? (
                    <LoadingSpinner stroke={`#FFF`} size={24} />
                  ) : (
                    "Kirim"
                  )}
                </button>
              </div>
            </div>
          </div>
        </div>
      )}
    </div>
  );
};

export default SignIn;
