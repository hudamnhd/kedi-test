import React, { useState, useId , useRef} from "react";
import Link from "next/link";
import toast from "react-hot-toast";
import Image from "next/image";
import axios from "axios";
import { Navbar } from "@/components/navbar/navbar";
import { getSession } from "next-auth/react";
import { Switch } from "@/components/ui/switch";
import { LoadingSpinner } from "@/components/ui/loading";
import { useMutation } from "@tanstack/react-query";
import { useSession } from "next-auth/react"
import { AsyncPaginate } from 'react-select-async-paginate';

const Home = ({ user, res }: any) => {
  const dataProfile = res.data;

  return (
      <div className="bg-[#FAFAFA]">
        <Navbar user={user} />
        <BuatKelas user={user} data={dataProfile} />
        <div className="flex items-center justify-center gap-5 text-3xl mb-10">
            
        {/*
         * Only trigger a session update, assuming you already updated the value server-side.
         * All `useSession().data` references will be updated.
         */}
        </div>
      </div>
  );
};
const BuatKelas = ({ user, data }: any) => {
  const sessionToken = user?.accessToken;
  const { data: session, status, update } = useSession()

  let initState = {
    public: {
      userId: parseInt(data.user.id),
      residentNo: data?.basicInformationProfile?.residentNo ?? "",
      username: data.user.username ?? "",
      fullName: data.user.fullName ?? "",
      phoneNumber: data.user.phone ?? "",
      email: data.user.email ?? "",
      profilePictureDocumentStorageId: null ?? "",
      university: data?.advanceInformationProfile?.public?.university ?? "",
      fromInstitution: data?.user?.fromInstitution ?? "",
      fromCity: data?.user?.fromCity ?? "",
    },
    lecturer: {
      userId: parseInt(data.user.id),
      university: data?.advanceInformationProfile?.lecturer?.university ?? "",
      lecturerNo: "90213123",
      programStudy: "Manajemen",
      username: data.user.username ?? "",
      fullName: data.user.fullName ?? "",
      phoneNumber: data.user.phone ?? "",
      email: data.user.email ?? "",
      profilePictureDocumentStorageId: null ?? "",
      fromInstitution: data?.user?.fromInstitution ?? "",
      fromCity: data?.user?.fromCity ?? "",
    },
    student: {
      userId: parseInt(data.user.id),
      residentNo: data?.basicInformationProfile?.residentNo ?? "",
      studentNo: "932010",
      university: data?.advanceInformationProfile?.student?.university ?? "",
      username: data.user.username ?? "",
      fullName: data.user.fullName ?? "",
      phoneNumber: data.user.phone ?? "",
      email: data.user.email ?? "",
      profilePictureDocumentStorageId: null ?? "",
      fromInstitution: data?.user?.fromInstitution ?? "",
      fromCity: data?.user?.fromCity ?? "",
    },
  };

  const type = data.basicInformationProfile.userType.userType;

  let isType;

  if (type === "3") {
    isType = initState.public;
  } else if (type === "2") {
    isType = initState.lecturer;
  } else {
    isType = initState.studentNo;
  }
const controlRef = useRef(null);
console.log('controlRef:',  controlRef);
  const [_data, setdata] = useState(isType);
  const [edit, setedit]  = useState(false);

  const [value]          = useState({ value: isType?.fromInstitution, label: isType?.fromInstitution });
  const [valueCity]      = useState({ value: isType?.fromCity, label: isType?.fromCity });

  const handleChange = (
    e: React.ChangeEvent<HTMLInputElement | HTMLTextAreaElement>,
  ) => {
    const name = e.target.name;

    setdata({
      ..._data,
      [name]: e.target.value,
    });
  };

    const loadOptions = async (searchQuery, loadedOptions, { page }) => {
        const response = await fetch(process.env.API_BE + `/api/university/search/univ?query=${searchQuery}&limit=50&page=${page}`);
        const data = await response.json();

        const options = [
            {
                value: 'Umum',
                label: 'Umum'
            },
            ...data.data.items.map(item => ({
                value: item.name,
                label: item.name
            }))
        ];

        return {
            options: options,
            hasMore: data.data.meta.currentPage < data.data.meta.totalPages,
            additional: {
                page: page + 1,
            },
        };
    };

    const loadOptionsCity = async (searchQuery, loadedOptions, { page }) => {
        const response = await fetch(process.env.API_BE + `/api/city?query=${searchQuery}&limit=50&page=${page}`);
        const data = await response.json();

        const options = [
            ...data.data.items.map(item => ({
                value: item["id_Kota/Kab"],
                label: item["Kota/Kab"] + ' ,  ' + item["Provinsi"] ,
            }))
        ];

        return {
            options: options,
            hasMore: data.data.meta.currentPage < data.data.meta.totalPages,
            additional: {
                page: page + 1,
            },
        };
    };

const customStyles = {
    control: (provided, state, innerRef) => {
        return {
            ...provided,
            padding: '4px',
            borderRadius: '8px',
            border: '1px solid #bdbdbd',
            boxShadow: state.isFocused ? '0 0 0 2px #9D21E6' : null,
            pointerEvents: state.isDisabled ? 'none' : 'auto',
            color: state.isDisabled ? '#000' : '#000',
            backgroundColor: state.isDisabled ? '#FFF' : '#FFF' ,
        };
    },
    option: (provided, state) => {
        return {
            ...provided,
            backgroundColor: state.isFocused ? '#9D21E6' : state.isSelected ? '#d7a6f5' : null,
            color: state.isFocused ? 'white' : state.isSelected ? '#000' : null,
        };
    },
};
  const [selectedImage, setSelectedImage] = useState(
    data.basicInformationProfile.photo || "/static/images/logokedi.svg",
  );
  const [selectedFile, setSelectedFile] = useState(null);
  const handleImageChange = (event) => {
    const file = event.target.files[0];

    if (file) {
      // Menggunakan URL.createObjectURL untuk membuat URL gambar dari file yang dipilih
      const imageUrl = URL.createObjectURL(file);
      setSelectedImage(imageUrl);
      setSelectedFile(file);
    }
  };

  const uploadFile = async (file: any) => {
    if (!file) return null;
    let opt = {
      headers: {
        "Content-Type": "multipart/form-data",
        Authorization: `Bearer ${sessionToken}`,
      },
    };

    const formData = new FormData();
    formData.append("file", file);
    formData.append("documentTypeId", "1");
    formData.append("isPrivate", "false");

    const response = await axios.post(
      process.env.API_BE + "/api/storage",
      formData,
      opt,
    );

    return response.data;
  };

  interface ApiResponse {
    data: {
      message: string;
    };
  }
  interface PostResponse extends ApiResponse {}
  type PostClass = {};

  const updateProfile: PostClass = useMutation(
    async (): Promise<PostResponse> => {
      if (!sessionToken) throw new Error("Missing session token");
      try {
        const uploading = await uploadFile(selectedFile);
        const newData = {
          ..._data,
          profilePictureDocumentStorageId: uploading?.documentStorage?.id
            ? parseInt(uploading?.documentStorage?.id)
            : uploading,
        };
        const response = await axios.put<PostResponse>(
          process.env.API_BE + `/api/auth/update-profile`,
          newData,
          {
            headers: {
              Authorization: `Bearer ${sessionToken}`,
            },
          },
        );
        const _res = response?.data?.data;
        const obj = {
            email: _res?.email,
            fullName: _res?.fullName,
            id: _res?.id,
            username: _res?.username,
            uuid: _res?.uuid,
            photo: _res?.profilePicture?.url,
        }

        update(obj);
        return response.data;
      } catch (error) {
        console.error(error);
        throw error;
      }
    },
    {
      onSuccess: () => {
        toast.success("Update Profile Berhasil");
        setedit(!edit);
      },

      onError: (error) =>
        toast.error(`Error: ${error.response?.data?.meta?.message ?? error}`),
    },
  );
  return (
    <>
      {updateProfile.isLoading && (
        <div className="fixed flex items-center justify-center w-full h-full bg-white/30 z-10">
          <LoadingSpinner size={70} strokeWidth={2} />
        </div>
      )}
      <div className="h-[3.6rem]" />

      <div className="max-w-3xl mx-auto">
        <Link
          href="/profile"
          className="mt-6 sm:mt-10 mb-6 sm:mb-8 flex items-center px-2"
        >
          <div className=" bg-purple-100 text-[#9d21e6] flex justify-center items-center rounded-full w-10 h-10  ">
            <svg
              stroke="currentColor"
              fill="currentColor"
              strokeWidth={0}
              viewBox="0 0 24 24"
              height={32}
              width={32}
              xmlns="http://www.w3.org/2000/svg"
            >
              <path d="M13.293 6.293 7.586 12l5.707 5.707 1.414-1.414L10.414 12l4.293-4.293z" />
            </svg>
          </div>
          <h1 className="ml-4 sm:ml-8 font-semibold text-[#9d21e6] text-xl sm:text-2xl md:text-3xl font-montserrat-semibold">
            Detail Profile
          </h1>
        </Link>
        <div className="w-full bg-white p-6 rounded-xl border shadow-md">
          <div className="">
            <form
              onSubmit={(e) => {
                e.preventDefault();
                updateProfile.mutate();
              }}
              className=""
            >
              <section>
                <div className="mx-auto h-48 w-48 text-center mb-5">
                  <div className="relative w-64">
                    <label className="cursor-pointer">
                      <input
                        disabled={!edit}
                        type="file"
                        accept="image/*"
                        className="hidden"
                        onChange={handleImageChange}
                      />
                      <Image
                        className="h-48 w-48 rounded-3xl absolute object-cover shadow-md border"
                        width={500}
                        height={500}
                        src={selectedImage}
                        alt="profile image"
                      />
                      {edit && (
                      <div className="w-48 h-48 group hover:bg-gray-300/50  rounded-3xl absolute flex justify-center items-center cursor-pointer transition duration-500">
                        <Image
                          className="hidden group-hover:block w-12 opacity-70"
                          width={500}
                          height={500}
                          src="/static/images/default/upload.svg"
                          alt="svg upload"
                        />
                      </div>
                      )}
                    </label>
                  </div>
                </div>

                <div className="mb-5">
                  <label
                    htmlFor="username"
                    className="mb-2 block text-xs sm:text-sm font-medium text-gray-700 s"
                  >
                    Username
                  </label>
                  <input
                    disabled={!edit}
                    value={_data?.username}
                    onChange={handleChange}
                    name="username"
                    type="text"
                    id="username"
                    placeholder="01234"
                    className={` p-2.5  w-full rounded-[10px] border border-gray-400  outline-none  shadow-sm focus:ring-2 ring-[#9d21e6] ${!edit ? "text-gray-400" : "text-gray-900"} `}
                  />
                </div>
                <div className="mb-5">
                  <label
                    htmlFor="fullName"
                    className="mb-2 block text-xs sm:text-sm font-medium text-gray-700"
                  >
                    Nama
                  </label>

                  <input
                    disabled={!edit}
                    type="text"
                    value={_data?.fullName}
                    onChange={handleChange}
                    name="fullName"
                    id="fullName"
                    placeholder="3"
                    className={` p-2.5  w-full rounded-[10px] border border-gray-400  outline-none  shadow-sm focus:ring-2 ring-[#9d21e6] ${!edit ? "text-gray-400" : "text-gray-900"} `}
                  />
                </div>
                <div className="mb-5">
                  <label
                    htmlFor="phoneNumber"
                    className="mb-2 block text-xs sm:text-sm font-medium text-gray-700"
                  >
                    No. Hp
                  </label>
                  <input
                    disabled={!edit}
                    value={_data?.phoneNumber}
                    onChange={handleChange}
                    name="phoneNumber"
                    type="text"
                    id="phoneNumber"
                    placeholder="+62"
                    className={` p-2.5  w-full rounded-[10px] border border-gray-400  outline-none  shadow-sm focus:ring-2 ring-[#9d21e6] ${!edit ? "text-gray-400" : "text-gray-900"} `}
                  />
                </div>
                <div className="mb-5">
                  <label
                    htmlFor="email"
                    className="mb-2 block text-xs sm:text-sm font-medium text-gray-700"
                  >
                    Email
                  </label>
                  <input
                    disabled={!edit}
                    value={_data?.email}
                    onChange={handleChange}
                    name="email"
                    type="email"
                    id="email"
                    placeholder="kelas@kelas.digital"
                    className={` p-2.5  w-full rounded-[10px] border border-gray-400  outline-none  shadow-sm focus:ring-2 ring-[#9d21e6] ${!edit ? "text-gray-400" : "text-gray-900"} `}
                  />
                </div>
              <div className="mb-2.5">
                  <label
                  htmlFor="UserUsername"
                  className="mb-2 block text-xs sm:text-sm font-medium text-gray-700"
                  >
                  Asal Kota/Kabupaten Domisili
                  </label>
                  <AsyncPaginate
                      defaultValue={valueCity?.value ? valueCity : null} 
                      onChange={(param) => setdata({ ..._data, fromCity: param?.value ?? null })}
                      loadOptions={loadOptionsCity}
                      isClearable
                      instanceId={useId()}
                      styles={customStyles}
                      isDisabled={true}
                      placeholder="Pilih Asal Kota/Kabupaten Domisili"
                      additional={{
                          page: 1,
                      }}
                  />
              </div>
              <div className="mb-2.5">
                  <label
                  htmlFor="UserUsername"
                  className="mb-2 block text-xs sm:text-sm  font-medium text-gray-700"
                  >
                  Institusi
                  </label>
                  <AsyncPaginate
                      defaultValue={value?.value ? value : null} 
                      onChange={(param) => setdata({ ..._data, institution: param?.value ?? null })}
                      loadOptions={loadOptions}
                      isClearable
                      instanceId={useId()}
                      styles={customStyles}
                      placeholder="Pilih Institusi"
                      isDisabled={true}
                      additional={{
                          page: 1,
                      }}
                  />
              </div>
                <div className="mb-5 hidden">
                  <label
                    htmlFor="programStudi"
                    className="mb-2 block text-xs sm:text-sm font-medium text-gray-700"
                  >
                    Akun Privasi
                  </label>

                  <div className="h-[50px] flex items-center justify-between px-3  mt-2 w-full rounded-[10px] border border-gray-400  outline-none  shadow-sm focus:ring-2 ring-[#9d21e6]">
                    <div className="flex items-center gap-x-3">
                      <svg
                        xmlns="http://www.w3.org/2000/svg"
                        width="22"
                        height="22"
                        viewBox="0 0 24 24"
                      >
                        <path
                          fill="currentColor"
                          d="M6 22q-.825 0-1.413-.588T4 20V10q0-.825.588-1.413T6 8h1V6q0-2.075 1.463-3.538T12 1q2.075 0 3.538 1.463T17 6v2h1q.825 0 1.413.588T20 10v10q0 .825-.588 1.413T18 22H6Zm0-2h12V10H6v10Zm6-3q.825 0 1.413-.588T14 15q0-.825-.588-1.413T12 13q-.825 0-1.413.588T10 15q0 .825.588 1.413T12 17ZM9 8h6V6q0-1.25-.875-2.125T12 3q-1.25 0-2.125.875T9 6v2ZM6 20V10v10Z"
                        />
                      </svg>
                      <span>Akun Privasi</span>
                    </div>
                    <Switch disabled={!edit} />
                  </div>
                </div>
                <div className=" items-stretch mx-auto mt-8">
                  {edit && (
                    <div className="space-y-3 max-w-[250px] mx-auto mb-8">
                      <button
                        type="button"
                        className="px-6  w-full shadow-lg font-medium rounded-[10px] py-2.5 flex justify-center items-center"
                      >
                        <svg
                          stroke="currentColor"
                          fill="currentColor"
                          strokeWidth={0}
                          version="1.1"
                          x="0px"
                          y="0px"
                          viewBox="0 0 48 48"
                          enableBackground="new 0 0 48 48"
                          className="inline-block mr-2 h-5 lg:h-6 w-5 lg:w-6"
                          height="1em"
                          width="1em"
                          xmlns="http://www.w3.org/2000/svg"
                        >
                          <path
                            fill="#FFC107"
                            d="M43.611,20.083H42V20H24v8h11.303c-1.649,4.657-6.08,8-11.303,8c-6.627,0-12-5.373-12-12
	c0-6.627,5.373-12,12-12c3.059,0,5.842,1.154,7.961,3.039l5.657-5.657C34.046,6.053,29.268,4,24,4C12.955,4,4,12.955,4,24
	c0,11.045,8.955,20,20,20c11.045,0,20-8.955,20-20C44,22.659,43.862,21.35,43.611,20.083z"
                          />
                          <path
                            fill="#FF3D00"
                            d="M6.306,14.691l6.571,4.819C14.655,15.108,18.961,12,24,12c3.059,0,5.842,1.154,7.961,3.039l5.657-5.657
	C34.046,6.053,29.268,4,24,4C16.318,4,9.656,8.337,6.306,14.691z"
                          />
                          <path
                            fill="#4CAF50"
                            d="M24,44c5.166,0,9.86-1.977,13.409-5.192l-6.19-5.238C29.211,35.091,26.715,36,24,36
	c-5.202,0-9.619-3.317-11.283-7.946l-6.522,5.025C9.505,39.556,16.227,44,24,44z"
                          />
                          <path
                            fill="#1976D2"
                            d="M43.611,20.083H42V20H24v8h11.303c-0.792,2.237-2.231,4.166-4.087,5.571
	c0.001-0.001,0.002-0.001,0.003-0.002l6.19,5.238C36.971,39.205,44,34,44,24C44,22.659,43.862,21.35,43.611,20.083z"
                          />
                        </svg>
                        {data.user.linkedWithGoogle
                          ? "Unlink Google Account"
                          : "Link Google Account"}
                      </button>
                      <button
                        className="hidden w-full bg-red-600 text-white shadow-lg px-6 py-2.5 rounded-[10px] font-medium"
                        type="button"
                      >
                        Hapus Akun
                      </button>
                    </div>
                  )}
                  <div
                    className={`grid ${
                      edit ? "grid-cols-2" : ""
                    } gap-x-3 max-w-[250px] w-full mx-auto`}
                  >
                    <button
                      onClick={() => setedit(!edit)}
                      className={`${
                        edit
                          ? "ring-1 ring-[#9d21e6] text-[#9d21e6] bg-white"
                          : "bg-[#9d21e6] text-white"
                      }  shadow-lg px-6 py-2.5 rounded-[10px] font-medium`}
                      type="button"
                    >
                      {edit ? "Cancel" : "Sunting Profile"}
                    </button>
                    {edit && (
                      <button disabled={updateProfile.isLoading}
                        className="w-full bg-[#9d21e6] text-white shadow-lg px-6 py-2.5 rounded-[10px] font-medium"
                        type="submit"
                      >
                        Save
                      </button>
                    )}
                  </div>
                </div>
              </section>
            </form>
          </div>
        </div>
      </div>
      <div className="h-[4.6rem]" />
    </>
  );
};

export default Home;

export async function getServerSideProps(context: any) {
  const session = await getSession(context);
  const response = await fetch(process.env.API_URL + "/api/auth/profile", {
    method: "GET",
    headers: {
      "Content-Type": "application/json",
      Authorization: `Bearer ${session?.user?.accessToken}`,
    },
  });
  const res = await response.json(); // atau response.text();

  if (response.status !== 200 || !session) {
    return {
      redirect: {
        destination: `/login`,
        permanent: false,
      },
    };
  } else {
    const { user } = session;
    return {
      props: { user, res },
    };
  }
}
