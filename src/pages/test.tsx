import { getSession } from "next-auth/react";
import { Tabs, TabsList, TabsTrigger } from "@/components/ui/tabs";
import React, { useEffect, useState, useRef } from "react";
import { trans } from "@/lang";
import { useRouter } from "next/router";
import { useShallow } from "zustand/react/shallow";

import { createWithEqualityFn } from "zustand/traditional";
import { shallow } from "zustand/shallow";

const useUserChat = createWithEqualityFn(
  () => ({
    isLoadMetadata: false,
    isFetchMessage: false,
    isFetchFirst: false,
    isTyping: {},
    isMobileView: false,
    initLoad: false,
    notifDelete: null,
    notifMessage: [],
    seenMessages: {},
    chatMode: "",
    responses: { channel: [] },
    userChatAktif: {},
    messageHistory: [],
    tabChat: "",
    privateMessage: [],
    groupChatMessage: [],
    classMessage: [],
    allClassMessage: [],
    groupItems: [],
    privateItems: [],
    classItems: [],
    newItemsGroup: [],
    newItemsPrivate: [],
    newItemsClass: [],
  }),
  shallow,
);

const Test = ({ user, viewportHeight }: any) => {
  const isMobileView = useUserChat((state) => state.isMobileView);
  const tabChat = useUserChat(useShallow((state) => state.tabChat));

  const router = useRouter();
  const locale: "en" | "id" = router.locale as "en" | "id";
  const lang = trans[locale].class;

  const setTabChat = (value) => {
    useUserChat.setState({
      tabChat: value,
    });
  };

  useEffect(() => {
    if (tabChat === "") {
      setTabChat("channelActiveClass");
    }
    useUserChat.setState({
      initLoad: true,
    });
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, []);

  console.log("tabChat:", tabChat);

  return (
    <div className="flex flex-col [@media(min-width:900px)]:flex-row gap-x-3 shadow-xl items-center">
      <div
        style={{ height: viewportHeight, overflow: "" }}
        className={`relative [@media(min-width:900px)]:max-w-[368px] w-full pt-[4.4rem] sm:pt-[5.2rem] pb-[3.8rem][@media(min-width:900px)]:pb-0 pb-2.5 className flex flex-col shadow-md px-2.5 ${isMobileView ? "hidden [@media(min-width:900px)]:block" : ""}`}
      ></div>

      <Tabs
        value={tabChat}
        onValueChange={setTabChat}
        className="relative [@media(min-width:900px)]:max-w-[368px] flex-1 pt-[4.4rem] sm:pt-[5.2rem] pb-[3.8rem] md:pb-0 pb-2.5 className flex flex-col h-screen shadow-md px-2.5"
      >
        <TabsList
          onClick={() =>
            useUserChat.setState({
              classMessage: ["value"],
            })
          }
          className="w-full grid grid-cols-3 bg-white  shadow-md"
        >
          <TabsTrigger value="channelActiveClass">
            {lang.activeCLass}{" "}
          </TabsTrigger>
          <TabsTrigger value="channelActivePrivate">{lang.chats} </TabsTrigger>
          <TabsTrigger value="channelActiveGroup">{lang.group} </TabsTrigger>
        </TabsList>
      </Tabs>
      {tabChat === "channelActivePrivate" && (
        <PrivateMsg user={user} viewportHeight={viewportHeight} />
      )}
      {tabChat === "channelActiveClass" && (
        <ClassMsg
          user={user}
          viewportHeight={viewportHeight}
          tabChat={tabChat}
        />
      )}
      {tabChat === "channelActiveGroup" && (
        <GroupMsg user={user} viewportHeight={viewportHeight} />
      )}
    </div>
  );
};

const ClassMsg = ({ user, viewportHeight, tabChat }: any) => {
  return (
    <div>
      <NewClassRoom
        user={user}
        viewportHeight={viewportHeight}
        tabChat={tabChat}
      />
    </div>
  );
};
const PrivateMsg = ({ user, viewportHeight }: any) => {
  return <div>PrivateMsg</div>;
};
const GroupMsg = ({ user, viewportHeight }: any) => {
  return <div>GroupMsg</div>;
};

const NewClassRoom = ({ user, viewportHeight, tabChat }: any) => {

  const classMessage = useUserChat((state) => state.classMessage);
console.log('classMessage:',  classMessage);
  useEffect(() => {
    setTimeout(() => {
      useUserChat.setState({
        classMessage: ["value"],
      });
    }, 100);
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, []);

  return <div>HAHAHA</div>;
};

export default Test;

export async function getServerSideProps(context: any) {
  try {
    const session = await getSession(context);
    if (session) {
      return {
        props: { user: session.user },
      };
    } else {
      return {
        redirect: {
          destination: `/login`,
          permanent: false,
        },
      };
    }
  } catch (error) {
    console.error("Kesalahan:", error);
    return {
      props: {
        error: "Terjadi kesalahan saat memuat sesi pengguna",
      },
    };
  }
}
