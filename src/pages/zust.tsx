import React from "react";
import { getSession } from "next-auth/react";
import useUserChat from "@/store/useUserChat";
import { createWithEqualityFn } from "zustand/traditional";
import { shallow } from "zustand/shallow";
import { useShallow } from "zustand/react/shallow";

const useMeals = createWithEqualityFn(
  () => ({
    isLoadMetadata: false,
    isFetchMessage: false,
    isFetchFirst: false,
    isTyping: {},
    isMobileView: false,
    initLoad: false,
    notifDelete: null,
    notifMessage: [],
    seenMessages: {},
    chatMode: "",
    responses: { channel: [] },
    userChatAktif: {},
    messageHistory: [],
    tabChat: "",
    privateMessage: [],
    groupChatMessage: [],
    classMessage: [],
    allClassMessage: [],
    groupItems: [],
    privateItems: [],
    classItems: [],
    newItemsGroup: [],
    newItemsPrivate: [],
    newItemsClass: [],
  }),
  shallow,
);

const BearNames = () => {
  const tabChat = useMeals(useShallow((state) => state.tabChat));

  const setTabChat = (value) => {
    useMeals.setState({
      tabChat: value,
    });
  };
  React.useEffect(() => {
    if (tabChat === "") {
      setTabChat("channelActiveClass");
    }
    useUserChat.setState({
      initLoad: true,
    });
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, []);

  console.log("tabChat:", tabChat);

  return (
    <>
      <div>HHH</div>

      <BearNamesChange />
    </>
  );
};

const BearNamesChange = () => {
  //const [allClassMessage, isMobileView] = useMeals( (state) => [state.allClassMessage, state.isMobileView], shallow,);

  const allClassMessage = useUserChat((state) => state.allClassMessage);
  const isMobileView = useUserChat((state) => state.isMobileView);

  React.useEffect(() => {
    useMeals.setState({
      allClassMessage: ["a large pizza"],
    });
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, []);
  return (
    <div className="flex flex-col items-center gap-5">
      <button
        onClick={() =>
          useMeals.setState({
            allClassMessage: ["a large pizza"],
          })
        }
      >
        Change allClassMessage
      </button>
      <button
        onClick={() =>
          useMeals.setState({
            isMobileView: ["huda"],
          })
        }
      >
        Change isMobileView
      </button>
      <button
        onClick={() =>
          useMeals.setState({
            tabChat: "a large pizza",
          })
        }
      >
        Change mamaBear
      </button>
    </div>
  );
};

const ExampleComponent = () => {
  const isFetchMessage = useUserChat((state) => state.isFetchMessage);
  console.log("isFetchMessage:", isFetchMessage);

  return <div></div>;
};

const ExampleComponent2 = () => {
  const chatMode = useUserChat((state) => state.chatMode);
  console.log("chatMode:", chatMode);

  return (
    <div>
      <button
        onClick={() =>
          useUserChat.setState({
            isFetchMessage: true,
          })
        }
      >
        Change mamaBear
      </button>
    </div>
  );
};

const Zust = ({ user }) => {
  return (
    <div>
      <BearNames />
    </div>
  );
};

export default Zust;

export async function getServerSideProps(context: any) {
  try {
    const session = await getSession(context);
    if (session) {
      return {
        props: { user: session.user },
      };
    } else {
      return {
        redirect: {
          destination: `/login`,
          permanent: false,
        },
      };
    }
  } catch (error) {
    console.error("Kesalahan:", error);
    return {
      props: {
        error: "Terjadi kesalahan saat memuat sesi pengguna",
      },
    };
  }
}
