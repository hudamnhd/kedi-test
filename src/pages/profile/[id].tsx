import React, { useState } from "react";
import { getSession } from "next-auth/react";
import toast from "react-hot-toast";
import Image from "next/image";
import Link from "next/link";
import axios from "axios";
import { trans } from "@/lang";
import { useRouter } from "next/router";
import { useQuery, useQueryClient, useMutation } from "@tanstack/react-query";
import useUserChat from "@/store/useUserChat";
import { getMessageUser } from "@/utils/getMessageUser";
import { getFileIcon, getFileType } from "@/utils/helper";
import { Label } from "@/components/ui/label";
import { Switch } from "@/components/ui/switch";
import { Navbar } from "@/components/navbar/navbar";
import { EmptyState } from "@/components/ui/empty-state";
import { PostsView } from "@/components/profile/postView";
import { LoadingSpinner } from "@/components/ui/loading";
import { ReusableAlertDialog } from "@/components/kelas/module/element/Alert";
import { Tabs, TabsContent, TabsList, TabsTrigger } from "@/components/ui/tabs";
import {
  Dialog,
  DialogContent,
  DialogHeader,
  DialogTitle,
} from "@/components/ui/dialog";
import {
  DropdownMenu,
  DropdownMenuContent,
  DropdownMenuItem,
  DropdownMenuLabel,
  DropdownMenuSeparator,
  DropdownMenuTrigger,
} from "@/components/ui/dropdown-menu";

interface ApiResponse {
  data: { message: string };
}

interface ToggleFavoriteResponse extends ApiResponse {}

const Home = ({ user, res, user_id }: any) => {
  return (
    <div>
      <Navbar user={user} />
      <BuatKelas user={user} profileData={res} user_id={user_id} />
    </div>
  );
};

const BuatKelas = ({ profileData, user, user_id }: any) => {
  const router = useRouter();
  const locale: "en" | "id" = router.locale as "en" | "id";
  const lang = trans[locale].profile;

  const isInstitution = profileData?.data?.isInstitution;
  const sessionToken = user?.accessToken;
  const queryClient = useQueryClient();

  const [modalMenuProfile, setModalMenuProfile] = useState(false);
  const [modalType, setModalType] = useState("");
  const [_user, setUser] = useState({
    email: user?.email,
    old_password: "",
    new_password: "",
  });

  const [confirmPassword, setConfirmPassword] = useState("");
  const [error, setError] = useState("");
  const [showPassword, setShowPassword] = useState(false);
  const [valueTab, setValueTab] = useState("feed");

  const [alertUnblock, setAlertUnblock] = useState(false);
  const [tempData, setTempData] = useState({
    id: 0,
    fullName: "",
  });

  interface ApiResponse {
    data: {
      message: string;
    };
  }
  interface PostResponse extends ApiResponse {}
  type PostData = {
    email: String;
    old_password: String;
    new_password: String;
  };

  const changePass = useMutation(
    async (data: PostData): Promise<PostResponse> => {
      const { email, old_password, new_password } = data;
      if (!sessionToken) throw new Error("Missing session token");
      if (_user.new_password !== confirmPassword) {
        setError("Password baru dan konfirmasi password tidak cocok");
        return;
      }
      try {
        const response = await axios.post<PostResponse>(
          process.env.API_BE + `/api/auth/change-password`,
          {
            email,
            old_password,
            new_password,
          },
          {
            headers: {
              Authorization: `Bearer ${sessionToken}`,
            },
          },
        );
        toast.success(`Berhasil Mengubah Password`);

        setUser({
          email: user?.email,
          old_password: "",
          new_password: "",
        });

        console.log(response.data);
        return response.data;
      } catch (error) {
        toast.error("Error Request");
        throw error;
      } finally {
        setModalMenuProfile(false);
      }
    },
    {
      onError: (error) =>
        toast.error(`Error: ${error.response.data.meta.message}`),
    },
  );

  const { data: contactsInst } = useQuery(
    ["contactsInst"],
    async () => {
      const response = await axios.get(
        process.env.API_BE +
          `/api/institution/contacts/${profileData?.data?.id}`,
        {
          headers: {
            Authorization: `Bearer ${sessionToken}`,
          },
        },
      );
      if (response.status !== 200) {
        throw new Error("Gagal mengambil data");
      }
      return response.data.data;
    },
    {
      enabled: isInstitution,
      refetchOnWindowFocus: true,
    },
  );

  const addFriend = useMutation(
    async (userId: number): Promise<ToggleFavoriteResponse> => {
      if (!sessionToken) throw new Error("Missing session token");
      try {
        const response = await axios.post<ToggleFavoriteResponse>(
          process.env.API_BE + `/api/friend/add`,
          {
            id: Number(userId),
          },
          {
            headers: {
              Authorization: `Bearer ${sessionToken}`,
            },
          },
        );
        toast.success(response.data.data.message);
        queryClient.invalidateQueries(["contactsInst"]);
        return response.data;
      } catch (error) {
        console.error(error);
        throw error;
      }
    },
  );

  const { data: dataBlock } = useQuery(
    ["listContactBlocked"],
    async () => {
      const response = await axios.get(
        process.env.API_BE + "/api/friend/block?limit=10&page=1",
        {
          headers: {
            Authorization: `Bearer ${sessionToken}`,
          },
        },
      );
      if (response.status !== 200) {
        throw new Error("Gagal mengambil data");
      }
      return response.data.data.items;
    },
    {
      refetchOnWindowFocus: false,
    },
  );

  const handleUnBlock = useMutation(
    async (userId): Promise<PostResponse> => {
      console.log(userId);
      if (!sessionToken) throw new Error("Missing session token");
      try {
        const response = await axios.post<PostResponse>(
          process.env.API_BE + `/api/friend/unblock`,
          { id: userId },
          {
            headers: {
              Authorization: `Bearer ${sessionToken}`,
            },
          },
        );
        toast.success(`${lang.successUnblock} {" "} ${tempData.fullName}`);

        setAlertUnblock(false);

        setTempData({
          id: 0,
          fullName: "",
        });

        console.log(response.data);
        return response.data;
      } catch (error) {
        toast.error("Error Request");
        throw error;
      } finally {
        queryClient.invalidateQueries(["listContactBlocked"]);
      }
    },
    {
      onError: (error) =>
        toast.error(`Error: ${error.response.data.meta.message}`),
    },
  );
  const filteredDataBlock = dataBlock?.filter((d) => d.id !== user.id);
  const titleUnblock = ` ${lang.promptUnblock} {" "} ${tempData.fullName}?`;

  const { data: allMaterialProfile, isLoading: isLoadMaterial } = useQuery(
    ["allMaterialProfile"],
    async () => {
      const response = await axios.get(
        process.env.API_BE + `/api/material/${profileData?.data?.id}`,
        {
          headers: {
            Authorization: `Bearer ${sessionToken}`,
          },
        },
      );
      if (response.status !== 200) {
        throw new Error("Gagal mengambil data");
      }
      return response.data.data.items;
    },
    {
      refetchOnWindowFocus: true,
    },
  );

  interface Material {
    // Definisikan struktur data material sesuai kebutuhan
  }
  const ItemsPerPage = 9;

  const [currentPage, setCurrentPage] = useState(1);

  // Fungsi untuk membagi data menjadi halaman-halaman
  const paginateData = (data: Material[], page: number) => {
    const startIndex = (page - 1) * ItemsPerPage;
    return data.slice(startIndex, startIndex + ItemsPerPage);
  };

  // Fungsi untuk mendapatkan total halaman
  const getTotalPages = (totalItems: number) => {
    return Math.ceil(totalItems / ItemsPerPage);
  };

  // Mendapatkan total halaman
  const totalPages = allMaterialProfile
    ? getTotalPages(allMaterialProfile.length)
    : 0;

  // Fungsi untuk mengubah halaman
  const handlePageChange = (page: number) => {
    setCurrentPage(page);
  };

  // Mendapatkan data untuk halaman saat ini
  const currentData = allMaterialProfile
    ? paginateData(allMaterialProfile, currentPage)
    : [];

  const setChatMode = (value) => {
    useUserChat.setState({
      chatMode: value,
    });
  };

  const setUserChatAktif = (value) => {
    useUserChat.setState({
      userChatAktif: value,
    });
  };

  const setTabChat = (value) => {
    useUserChat.setState({
      tabChat: value,
    });
  };

  const LogoMaterial = () => {
    return (
      <svg
        xmlns="http://www.w3.org/2000/svg"
        viewBox="0 0 95.043 114.464"
        width={60}
        height={60}
      >
        <g transform="translate(0)">
          <path
            d="M106.533,107.3,38.078,122.744a1.449,1.449,0,0,1-1.735-1.1L15.421,28.288a1.453,1.453,0,0,1,1.1-1.735L84.974,11.106a1.451,1.451,0,0,1,1.735,1.1l20.925,93.358A1.453,1.453,0,0,1,106.533,107.3Z"
            transform="translate(-14.008 -9.693)"
            fill="#9d21e6"
          />
          <path
            d="M108.414,109.013a2.817,2.817,0,0,1-.855.349L39.1,124.807a2.821,2.821,0,0,1-3.381-2.134L14.8,29.314a2.839,2.839,0,0,1,2.146-3.383L85.4,10.485a2.828,2.828,0,0,1,3.383,2.136l20.925,93.358A2.845,2.845,0,0,1,108.414,109.013ZM17.528,28.625a.086.086,0,0,0-.04.082l20.923,93.358a.082.082,0,0,0,.088.055l68.455-15.445h0a.085.085,0,0,0,.053-.088L86.082,13.23a.079.079,0,0,0-.088-.057L17.545,28.618A.021.021,0,0,0,17.528,28.625Z"
            transform="translate(-14.729 -10.414)"
          />
        </g>
        <g transform="translate(5.875 3.938)">
          <path
            d="M89.491,116.778l-69.969-5.591a1.45,1.45,0,0,1-1.334-1.563L25.843,14.28a1.455,1.455,0,0,1,1.565-1.332l69.969,5.593a1.454,1.454,0,0,1,1.336,1.56l-7.657,95.347A1.451,1.451,0,0,1,89.491,116.778Z"
            transform="translate(-16.803 -11.57)"
            fill="#fff"
          />
          <path
            d="M90.333,118.88c-.08,0-.155,0-.231-.008l-69.969-5.593a2.83,2.83,0,0,1-2.6-3.043L25.188,14.89A2.838,2.838,0,0,1,28.24,12.3L98.211,17.89a2.835,2.835,0,0,1,2.6,3.043l-7.655,95.347a2.837,2.837,0,0,1-2.821,2.6ZM28.015,15.04a.079.079,0,0,0-.076.069l-7.653,95.347a.083.083,0,0,0,.067.08l69.967,5.591h0c.044-.011.076-.034.08-.067l7.655-95.347a.072.072,0,0,0-.065-.08L28.021,15.04Z"
            transform="translate(-17.526 -12.29)"
          />
          <g transform="translate(13.28 21.694)">
            <g transform="translate(0 34.64)">
              <line
                x2="17.726"
                y2="1.416"
                transform="translate(0.921 0.917)"
                fill="#fff"
              />
              <path
                d="M42.5,42.363c-.025,0-.05,0-.076,0L24.694,40.945a.918.918,0,1,1,.149-1.829l17.724,1.416a.917.917,0,0,1-.071,1.831Z"
                transform="translate(-23.848 -39.112)"
                fill="#9d21e6"
              />
            </g>
            <g transform="translate(3.346)">
              <line
                x2="53.065"
                y2="4.238"
                transform="translate(0.919 0.922)"
                fill="#fff"
              />
              <path
                d="M79.425,28.7c-.027,0-.05,0-.076,0L26.287,24.453a.917.917,0,0,1-.842-.989.9.9,0,0,1,.991-.84L79.5,26.866a.917.917,0,0,1-.074,1.831Z"
                transform="translate(-25.442 -22.619)"
              />
            </g>
            <g transform="translate(2.428 7.267)">
              <line
                x2="53.062"
                y2="4.238"
                transform="translate(0.921 0.922)"
                fill="#fff"
              />
              <path
                d="M78.988,32.157c-.025,0-.05,0-.076,0L25.85,27.913a.917.917,0,0,1-.842-.989.9.9,0,0,1,.991-.84l53.062,4.242a.917.917,0,0,1-.074,1.831Z"
                transform="translate(-25.005 -26.079)"
              />
            </g>
            <g transform="translate(1.51 15.311)">
              <line
                x2="53.065"
                y2="4.24"
                transform="translate(0.921 0.92)"
                fill="#fff"
              />
              <path
                d="M78.553,35.987c-.027,0-.05,0-.076,0L25.413,31.743a.917.917,0,0,1-.842-.989.9.9,0,0,1,.991-.84l53.065,4.242a.917.917,0,0,1-.074,1.831Z"
                transform="translate(-24.568 -29.909)"
              />
            </g>
          </g>
          <g transform="translate(31.694 8.45)">
            <path
              d="M32.616,16.313"
              transform="translate(-32.616 -16.313)"
              fill="none"
              stroke="#233863"
              strokeLinecap="round"
              strokeLinejoin="round"
              strokeMiterlimit={10}
              strokeWidth={3}
            />
          </g>
        </g>
      </svg>
    );
  };

  return (
    <>
      <ReusableAlertDialog
        open={alertUnblock}
        onOpenChange={setAlertUnblock}
        title={titleUnblock}
        description="Tindakan ini bisa dibatalkan."
        textDelete="Remove"
        onDelete={() => handleUnBlock.mutate(tempData.id)}
      />
      <Dialog
        open={modalMenuProfile}
        onOpenChange={setModalMenuProfile}
        onOpenAutoFocus={(e) => e.preventDefault()}
      >
        <DialogContent className="relative">
          {changePass.isLoading && (
            <div className="fixed flex items-center justify-center w-full h-full bg-white/30 z-10">
              <LoadingSpinner size={70} strokeWidth={2} />
            </div>
          )}
          <DialogHeader>
            <DialogTitle className="text-center">
              {modalType === "Change Password" && lang.changePassword}
              {modalType === "Kontak Blocked" && lang.blockedContacts}
            </DialogTitle>
          </DialogHeader>
          {modalType === "Change Password" && (
            <div>
              <form
                onSubmit={(e) => {
                  e.preventDefault();
                  changePass.mutate(_user);
                }}
              >
                <div className="mb-3">
                  <div className="mb-3 flex items-center w-full justify-between  gap-5">
                    <Label htmlFor="sesi-mode  font-medium">
                      {lang.oldPassword}
                    </Label>
                  </div>
                  <input
                    type={showPassword ? "text" : "password"}
                    value={_user.old_password}
                    onChange={(e) =>
                      setUser({
                        ..._user,
                        old_password: e.target.value,
                      })
                    }
                    required
                    placeholder="Katasandi Lama"
                    className="disabled:bg-slate-100 p-2 w-full rounded-[10px] border border-gray-400  outline-none  shadow-sm focus:ring-2 ring-[#9d21e6]"
                  />
                </div>
                <div className="mb-3">
                  <div className="mb-3 flex items-center w-full justify-between  gap-5">
                    <Label htmlFor="sesi-mode  font-medium">
                      {lang.newPassword}
                    </Label>
                  </div>
                  <input
                    type={showPassword ? "text" : "password"}
                    value={_user.new_password}
                    onChange={(e) =>
                      setUser({
                        ..._user,
                        new_password: e.target.value,
                      })
                    }
                    required
                    placeholder="Katasandi Baru"
                    className="disabled:bg-slate-100 p-2 w-full rounded-[10px] border border-gray-400  outline-none  shadow-sm focus:ring-2 ring-[#9d21e6]"
                  />
                </div>
                <div className="mb-3">
                  <div className="mb-3 flex items-center w-full justify-between  gap-5">
                    <Label htmlFor="sesi-mode  font-medium">
                      {lang.repPassword}
                    </Label>
                  </div>
                  <input
                    type={showPassword ? "text" : "password"}
                    value={confirmPassword}
                    onChange={(e) => setConfirmPassword(e.target.value)}
                    required
                    placeholder="Ulang Katasandi Baru"
                    className="disabled:bg-slate-100 p-2 w-full rounded-[10px] border border-gray-400  outline-none  shadow-sm focus:ring-2 ring-[#9d21e6]"
                  />
                  {error && (
                    <p className="mt-1 text-sm" style={{ color: "red" }}>
                      {error}
                    </p>
                  )}
                </div>
                <div>
                  <div className="flex items-center gap-x-2">
                    <Switch
                      id="sesi-mode"
                      checked={showPassword}
                      onCheckedChange={setShowPassword}
                    />
                    <Label htmlFor="sesi-mode  font-medium  text-[16px]">
                      {showPassword ? lang.hidePassword : lang.showPassword}
                    </Label>
                  </div>
                </div>
                <div className="mt-5 flex items-center justify-center">
                  <button
                    disabled={changePass?.isLoading}
                    type="submit"
                    className="p-2.5  bg-[#9d21e6] text-white shadow-xl rounded-[10px] font-semibold"
                  >
                    {lang.changePassword}
                  </button>
                </div>
              </form>
            </div>
          )}
          {modalType === "Kontak Blocked" && (
            <div
              value="general"
              className="max-h-[75vh] overflow-y-auto px-1.5 pt-2"
            >
              {filteredDataBlock?.length > 0 ? (
                filteredDataBlock?.map((item: any) => (
                  <div
                    key={item.id}
                    className="w-full flex items-center justify-between items-center gap-x-2 hover:bg-[#9D21E6]/10 border p-2.5 pr-4 rounded-lg mb-2.5 cursor-pointer"
                  >
                    <div className="flex items-center gap-x-2.5 ">
                      <div className="relative h-12 w-12 border p-0.5 rounded-lg border-gray-300">
                        <Image
                          blurDataURL="/static/images/default/placeholder.png"
                          placeholder="blur"
                          width={60}
                          height={60}
                          src={
                            item?.profilePictureLink ??
                            `/static/images/logokedi.svg`
                          }
                          className=" absolute inset-0 h-full w-full object-cover  rounded-lg"
                          alt=""
                        />
                      </div>
                      <div>
                        <p className="font-semibold text-ellipsis">
                          {item.fullName}
                        </p>
                        {item.username && (
                          <p className="text-sm text-[#9D21E6]">
                            {"@" + item.username}
                          </p>
                        )}
                      </div>
                    </div>
                    <button
                      onClick={() => {
                        setAlertUnblock(true);

                        setTempData({
                          id: parseInt(item.id),
                          fullName: item.fullName ?? item.username,
                        });
                      }}
                      className="flex items-center text-sm gap-x-2 text-red-600 cursor-pointer font-semibold"
                    >
                      <svg
                        className="w-6 h-6"
                        xmlns="http://www.w3.org/2000/svg"
                        viewBox="0 0 24 24"
                      >
                        <path
                          fill="currentColor"
                          d="M12 2C6.48 2 2 6.48 2 12s4.48 10 10 10s10-4.48 10-10S17.52 2 12 2zM4 12c0-4.42 3.58-8 8-8c1.85 0 3.55.63 4.9 1.69L5.69 16.9A7.902 7.902 0 0 1 4 12zm8 8c-1.85 0-3.55-.63-4.9-1.69L18.31 7.1A7.902 7.902 0 0 1 20 12c0 4.42-3.58 8-8 8z"
                        ></path>
                      </svg>
                      <span>Unblock</span>
                    </button>
                  </div>
                ))
              ) : (
                <div className="h-full flex items-center justify-center">
                  {!dataBlock.isLoading && "Tidak Ada Kontak yang Diblokir"}
                </div>
              )}
            </div>
          )}
        </DialogContent>
      </Dialog>
      <div className="h-[3.7rem] sm:h-[4.7rem] " />
      <main className="w-full h-full flex justify-center p-2.5 ">
        <div className="w-full max-w-3xl mx-auto  bg-white p-4 sm:p-6 rounded-[10px] border">
          <div className="mb-6 flex justify-between">
            {/*backto home */}
            <Link href="/" className="flex items-center gap-x-2">
              <div className="bg-[#9D21E6] flex justify-center items-center rounded-full w-8 h-8 lg:w-10 lg:h-10 p-1">
                <svg
                  stroke="currentColor"
                  fill="currentColor"
                  strokeWidth={0}
                  viewBox="0 0 1024 1024"
                  className="text-white "
                  height={20}
                  width={20}
                  xmlns="http://www.w3.org/2000/svg"
                >
                  <path d="M872 474H286.9l350.2-304c5.6-4.9 2.2-14-5.2-14h-88.5c-3.9 0-7.6 1.4-10.5 3.9L155 487.8a31.96 31.96 0 0 0 0 48.3L535.1 866c1.5 1.3 3.3 2 5.2 2h91.5c7.4 0 10.8-9.2 5.2-14L286.9 550H872c4.4 0 8-3.6 8-8v-60c0-4.4-3.6-8-8-8z" />
                </svg>
              </div>
              <h1 className="ml-2 lg:ml-4 text-lg sm:text-xl md:text-2xl font-bold text-[#9D21E6] self-center ">
                {profileData?.data?.fullName}
              </h1>
            </Link>
            <div className="relative flex justify-center cursor-pointer items-center rounded-full w-6 h-6 lg:w-10 lg:h-10">
              {profileData?.data.id === user.id && (
                <Link
                  href="/profile/other"
                  className="rounded-xl hover:bg-purple-100 p-1.5"
                >
                  <svg
                    xmlns="http://www.w3.org/2000/svg"
                    fill="none"
                    viewBox="0 0 24 24"
                    strokeWidth={1.5}
                    stroke="currentColor"
                    className="w-6 h-6"
                  >
                    <path
                      strokeLinecap="round"
                      strokeLinejoin="round"
                      d="M3.75 6.75h16.5M3.75 12h16.5m-16.5 5.25h16.5"
                    />
                  </svg>
                </Link>
              )}
              <DropdownMenu>
                <DropdownMenuTrigger className=""></DropdownMenuTrigger>
                <DropdownMenuContent className="mr-8">
                  <DropdownMenuLabel>Menu Profile</DropdownMenuLabel>
                  <DropdownMenuSeparator />
                  <DropdownMenuItem
                    onClick={() => {
                      setModalMenuProfile(true);
                      setModalType("Change Password");
                    }}
                  >
                    {lang.changePassword}
                  </DropdownMenuItem>
                  <DropdownMenuItem
                    onClick={() => {
                      setModalMenuProfile(true);
                      setModalType("Kontak Blocked");
                    }}
                  >
                    {lang.blockedContacts}
                  </DropdownMenuItem>
                </DropdownMenuContent>
              </DropdownMenu>
            </div>
          </div>
          <div className="flex items-center gap-x-5">
            <div className="flex-none relative h-20 w-20 md:h-24 md:w-24">
              <Image
                blurDataURL="/static/images/default/placeholder.png"
                placeholder="blur"
                width={60}
                height={60}
                src={
                  profileData?.data?.profilePictureLink ??
                  `/static/images/logokedi.svg`
                }
                className="border absolute inset-0 h-full w-full object-cover  rounded-xl"
                alt=""
              />
            </div>
            <div className=" mt-2.5 text-sm sm:text-[16px] sm:mt-4 gap-x-3  grid grid-cols-3 mb-3 place-items-center">
              <div className="text-center line-clamp-1">
                <p>{lang.classActive}</p>
              </div>
              <div className="text-center line-clamp-1">
                <p>{lang.participant}</p>
              </div>
              <div className="text-center line-clamp-1">
                <p>{lang.bkd}</p>
              </div>
              <div className="text-center">
                <p className=" font-bold">0</p>
              </div>
              <div className="text-center">
                <p className=" font-bold">0</p>
              </div>
              <div className="text-center">
                <p className=" font-bold">0</p>
              </div>
            </div>
          </div>
          <div className="flex justify-between my-2.5">
            <div className="sm:w-2/3">
              {profileData?.data?.username && (
                <p className="sm:text-normal text-sm font-semibold text-[#9D21E6]">
                  @{profileData?.data?.username}
                </p>
              )}
            </div>
          </div>

          {profileData?.data.id === user.id && (
            <Link href="/detail-profile">
              <svg
                xmlns="http://www.w3.org/2000/svg"
                fill="none"
                viewBox="0 0 24 24"
                strokeWidth={1.5}
                stroke="currentColor"
                className="w-6 h-6  sm:hidden"
              >
                <path
                  strokeLinecap="round"
                  strokeLinejoin="round"
                  d="M16.862 4.487l1.687-1.688a1.875 1.875 0 112.652 2.652L10.582 16.07a4.5 4.5 0 01-1.897 1.13L6 18l.8-2.685a4.5 4.5 0 011.13-1.897l8.932-8.931zm0 0L19.5 7.125M18 14v4.75A2.25 2.25 0 0115.75 21H5.25A2.25 2.25 0 013 18.75V8.25A2.25 2.25 0 015.25 6H10"
                />
              </svg>

              <button className="sm:inline hidden border-[#9D21E6] border-2 font-medium shadow-md rounded-xl px-3 sm:px-6 py-1.5 text-[#9D21E6]">
                {lang.detailProfile}
              </button>
            </Link>
          )}
        </div>
      </main>

      <div className="max-w-3xl md:mx-auto border mx-3 rounded-lg">
        <Tabs
          value={valueTab}
          onValueChange={setValueTab}
          className="relative w-full bg-white gap-5 place-items-center px-2 sm:px-5 rounded-md mb-5 pt-3 pb-[3.2rem] md:pb-0"
        >
          <TabsList className="grid w-full grid-cols-2">
            <TabsTrigger value="feed">Kelas Umum</TabsTrigger>
            {isInstitution ? (
              <TabsTrigger value="contacts">Kontak</TabsTrigger>
            ) : (
              <TabsTrigger value="material">{lang.materials}</TabsTrigger>
            )}
          </TabsList>
          <TabsContent
            forcemount="true"
            hidden={valueTab !== "feed"}
            className="text-sm text-center"
            value="feed"
          >
            <PostsView user={user} user_id={user_id} />
          </TabsContent>

          {isInstitution ? (
            <TabsContent
              forcemount="true"
              hidden={valueTab !== "contacts"}
              value="contacts"
              className="overflow-y-auto px-1.5"
            >
              {contactsInst && contactsInst?.length > 0 ? (
                contactsInst?.map((item: any) => (
                  <div
                    key={item.id}
                    className="w-full flex items-center justify-between items-center gap-x-2 hover:bg-[#9D21E6]/10 border px-4 py-2.5 rounded-lg mb-2.5 "
                  >
                    <div className="flex items-center gap-x-2.5 ">
                      <div className="flex-none relative h-12 w-12 border p-0.5 rounded-lg border-gray-300">
                        <Link
                          onClick={() =>
                            setTimeout(() => {
                              setValueTab("feed");
                            }, 500)
                          }
                          href={`/profile/${item?.id}`}
                        >
                          <Image
                            blurDataURL="/static/images/default/placeholder.png"
                            placeholder="blur"
                            width={60}
                            height={60}
                            src={
                              item?.profilePictureLink ??
                              `/static/images/logokedi.svg`
                            }
                            className=" absolute inset-0 h-full w-full object-cover  rounded-lg"
                            alt=""
                          />
                        </Link>
                      </div>
                      <Link
                        onClick={() =>
                          setTimeout(() => {
                            setValueTab("feed");
                          }, 500)
                        }
                        href={`/profile/${item?.id}`}
                      >
                        <p className="text-sm sm:text-normal font-semibold text-ellipsis">
                          {" "}
                          {item.fullName}{" "}
                        </p>{" "}
                        {item.username && (
                          <p className="text-sm text-[#9D21E6]">
                            {" "}
                            {"@" + item.username}{" "}
                          </p>
                        )}
                      </Link>
                    </div>
                    {!item.isFriend ? (
                      <div
                        onClick={() => addFriend.mutate(item.id)}
                        className="text-[#9D21E6] cursor-pointer"
                      >
                        <svg
                          xmlns="http://www.w3.org/2000/svg"
                          width="24px"
                          height="24px"
                          viewBox="0 0 24 24"
                        >
                          <g
                            fill="none"
                            stroke="currentColor"
                            strokeLinecap="round"
                            strokeWidth="2.5"
                          >
                            <path
                              strokeDasharray="20"
                              strokeDashoffset="20"
                              d="M3 21V20C3 17.7909 4.79086 16 7 16H11C13.2091 16 15 17.7909 15 20V21"
                            >
                              <animate
                                fill="freeze"
                                attributeName="stroke-dashoffset"
                                dur="0.4s"
                                values="20;0"
                              ></animate>
                            </path>
                            <path
                              strokeDasharray="20"
                              strokeDashoffset="20"
                              d="M9 13C7.34315 13 6 11.6569 6 10C6 8.34315 7.34315 7 9 7C10.6569 7 12 8.34315 12 10C12 11.6569 10.6569 13 9 13Z"
                            >
                              <animate
                                fill="freeze"
                                attributeName="stroke-dashoffset"
                                begin="0.5s"
                                dur="0.4s"
                                values="20;0"
                              ></animate>
                            </path>
                            <path
                              strokeDasharray="8"
                              strokeDashoffset="8"
                              d="M15 6H21"
                            >
                              <animate
                                fill="freeze"
                                attributeName="stroke-dashoffset"
                                begin="1s"
                                dur="0.2s"
                                values="8;0"
                              ></animate>
                            </path>
                            <path
                              strokeDasharray="8"
                              strokeDashoffset="8"
                              d="M18 3V9"
                            >
                              <animate
                                fill="freeze"
                                attributeName="stroke-dashoffset"
                                begin="1.2s"
                                dur="0.2s"
                                values="8;0"
                              ></animate>
                            </path>
                          </g>
                        </svg>
                      </div>
                    ) : (
                      <div
                        onClick={() => {
                          if (item?.channel) {
                            if (router.pathname !== "/class") {
                              router.push("/class");
                            }

                            const opt = {
                              channel: item.channel,
                            };

                            getMessageUser(opt);
                            setChatMode("private");
                            setTabChat("channelActivePrivate");
                            setUserChatAktif({
                              channel: "private." + item.chatRoomId,
                              receiver: {
                                id: item.id,
                                name: item.fullName,
                                photo: item.profilePict,
                              },
                              sender: {
                                id: user?.uuid,
                                name: user?.fullName,
                                photo: user?.photo,
                              },
                            });
                          } else {
                            console.log("anda belum berteman");
                          }
                        }}
                        className="text-[#9D21E6] cursor-pointer mr-1"
                      >
                        <svg
                          xmlns="http://www.w3.org/2000/svg"
                          fill="none"
                          viewBox="0 0 24 24"
                          strokeWidth={3}
                          stroke="currentColor"
                          className="w-5 h-5"
                        >
                          <path
                            strokeLinecap="round"
                            strokeLinejoin="round"
                            d="M2.25 12.76c0 1.6 1.123 2.994 2.707 3.227 1.087.16 2.185.283 3.293.369V21l4.076-4.076a1.526 1.526 0 0 1 1.037-.443 48.282 48.282 0 0 0 5.68-.494c1.584-.233 2.707-1.626 2.707-3.228V6.741c0-1.602-1.123-2.995-2.707-3.228A48.394 48.394 0 0 0 12 3c-2.392 0-4.744.175-7.043.513C3.373 3.746 2.25 5.14 2.25 6.741v6.018Z"
                          />
                        </svg>
                      </div>
                    )}
                  </div>
                ))
              ) : (
                <div className="flex items-center justify-center mt-3 h-[45vh] md:h-[50vh]">
                  <EmptyState />
                </div>
              )}
            </TabsContent>
          ) : (
            <TabsContent
              forcemount="true"
              hidden={valueTab !== "material"}
              value="material"
              className="grid grid-cols-3 gap-2.5 sm:gap-5 mt-5"
            >
              {isLoadMaterial ? (
                <div className="absolute flex items-center justify-center w-[95%] h-full bg-white/30 z-10">
                  <LoadingSpinner size={60} strokeWidth={2} />
                </div>
              ) : totalPages === 0 ? (
                <div className="col-span-3 text-center flex items-center justify-center mt-5 h-[45vh] md:h-[50vh]">
                  {" "}
                  <EmptyState />{" "}
                </div>
              ) : (
                <>
                  {currentData &&
                    currentData?.map((d) => {
                      const fileName = d.reference.originalName;
                      const fileType = getFileType(fileName);
                      const fileIcon = getFileIcon(fileType);
                      return (
                        <Link
                          href={d.reference.url ?? "#"}
                          target="_blank"
                          key={d.id}
                          className="ring-1 hover:bg-purple-50 ring-gray-300 rounded-md w-full flex flex-col items-center p-3 sm:p-5"
                        >
                          {fileIcon ? (
                            <Image
                              blurDataURL="/static/images/default/placeholder.png"
                              placeholder="blur"
                              src={fileIcon}
                              alt={fileName}
                              width={60}
                              height={60}
                            />
                          ) : (
                            <LogoMaterial />
                          )}
                          <div className="md:text-md text-sm font-semibold mt-2.5 text-center">
                            {d.title}
                          </div>
                          <div className="sm:text-sm text-xs text-center  hyphens-auto break-all whitespace-pre-wrap">
                            {d.reference.originalName}
                          </div>
                        </Link>
                      );
                    })}
                </>
              )}
              {totalPages > 9 && (
                <div className="inline-flex items-center justify-end gap-2.5  p-1 rounded-lg col-span-3 mb-5">
                  <button
                    disabled={currentPage === 1}
                    onClick={() => handlePageChange(currentPage - 1)}
                    className="inline-flex size-8 items-center justify-center rounded bg-purple-700 border border-gray-100 text-white  rtl:rotate-180"
                  >
                    <span className="sr-only">Next Page</span>
                    <svg
                      xmlns="http://www.w3.org/2000/svg"
                      className="h-5 w-5"
                      viewBox="0 0 20 20"
                      fill="currentColor"
                    >
                      <path
                        fillRule="evenodd"
                        d="M12.707 5.293a1 1 0 010 1.414L9.414 10l3.293 3.293a1 1 0 01-1.414 1.414l-4-4a1 1 0 010-1.414l4-4a1 1 0 011.414 0z"
                        clipRule="evenodd"
                      />
                    </svg>
                  </button>

                  <p className="text-sm text-gray-900 font-medium">
                    {currentPage}
                    <span className="mx-0.25">/</span>
                    {totalPages}
                  </p>

                  <button
                    disabled={
                      !allMaterialProfile ||
                      allMaterialProfile.length <= currentPage * ItemsPerPage
                    }
                    onClick={() => handlePageChange(currentPage + 1)}
                    className="inline-flex size-8 items-center justify-center rounded bg-purple-700 border border-gray-100 text-white rtl:rotate-180"
                  >
                    <span className="sr-only">Next Page</span>
                    <svg
                      xmlns="http://www.w3.org/2000/svg"
                      className="h-5 w-5"
                      viewBox="0 0 20 20"
                      fill="currentColor"
                    >
                      <path
                        fillRule="evenodd"
                        d="M7.293 14.707a1 1 0 010-1.414L10.586 10 7.293 6.707a1 1 0 011.414-1.414l4 4a1 1 0 010 1.414l-4 4a1 1 0 01-1.414 0z"
                        clipRule="evenodd"
                      />
                    </svg>
                  </button>
                </div>
              )}
            </TabsContent>
          )}
        </Tabs>
      </div>
    </>
  );
};

export default Home;

export async function getServerSideProps(context: any) {
  const { id } = context.params;
  try {
    const session = await getSession(context);
    if (session) {
      const session = await getSession(context);
      const response = await fetch(
        process.env.API_URL + `/api/user/detail/${id}`,
        {
          method: "GET",
          headers: {
            "Content-Type": "application/json",
            Authorization: `Bearer ${session?.user?.accessToken}`,
          },
        },
      );
      const res = await response.json(); // atau response.text();
      const { user } = session;
      return {
        props: { user, res, user_id: id },
      };
    } else {
      return {
        redirect: {
          destination: `/login`,
          permanent: false,
        },
      };
    }
  } catch (error) {
    console.error("Kesalahan:", error);
    return {
      props: {
        error: "Terjadi kesalahan saat memuat sesi pengguna",
      },
    };
  }
}
