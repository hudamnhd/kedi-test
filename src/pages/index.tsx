import Link from "next/link";
import { useRouter } from "next/router";
import { getSession } from "next-auth/react";
import { trans } from "@/lang";
import { Navbar } from "@/components/navbar/navbar";
import { StoryFeed } from "@/components/beranda/storyFeed";
import { PostsView } from "@/components/beranda/postView";
import {
  Dialog,
  DialogContent,
  DialogHeader,
  DialogTrigger,
} from "@/components/ui/dialog";

const BuatKelas = () => {
  const router = useRouter();
  const locale: "en" | "id" = router.locale as "en" | "id";
  const lang = trans[locale].home;

  return (
    <>
      <div className="max-w-3xl mx-auto mb-2.5 px-2.5">
        <Dialog className="w-full">
          <DialogTrigger className="max-w-3xl p-2.5 text-center mx-auto w-full  bg-[#9d21e6] hover:text-[#9d21e6] hover:bg-white hover:ring-1 ring-[#9d21e6] duration-300 text-white shadow-xl rounded-[10px] font-semibold">
            {lang.create}
          </DialogTrigger>
          <DialogContent className="">
            <DialogHeader></DialogHeader>
            <Link
              href="/buat-kelas"
              type="button"
              className="p-2.5 text-center mx-auto w-full  bg-[#9d21e6] hover:text-[#9d21e6] hover:bg-white hover:ring-1 ring-[#9d21e6] duration-300 text-white shadow-xl rounded-[10px] font-semibold"
            >
              {lang.create} {lang.class}
            </Link>
            <Link
              href="/buat-post"
              type="button"
              className="p-2.5 text-center mx-auto w-full  bg-[#9d21e6] hover:text-[#9d21e6] hover:bg-white hover:ring-1 ring-[#9d21e6] duration-300 text-white shadow-xl rounded-[10px] font-semibold"
            >
              {lang.create} {lang.post}
            </Link>
          </DialogContent>
        </Dialog>
      </div>
    </>
  );
};

const Home = ({ user }: any) => {
  return (
    <div className="bg-[#FAFAFA]">
      <Navbar user={user} />
      <StoryFeed user={user} />
      <BuatKelas />
      <PostsView user={user} />
    </div>
  );
};

export default Home;

export async function getServerSideProps(context: any) {
  try {
    const session = await getSession(context);
    if (session) {
      return {
        props: { user: session.user },
      };
    } else {
      return {
        redirect: {
          destination: `/login`,
          permanent: false,
        },
      };
    }
  } catch (error) {
    console.error("Kesalahan:", error);
    return {
      props: {
        error: "Terjadi kesalahan saat memuat sesi pengguna",
      },
    };
  }
}
