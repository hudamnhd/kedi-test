import PubNub from "pubnub";
import React, { createContext } from "react";
const PubNubContext = createContext();

export const usePubNub = () => {
  const context = React.useContext(PubNubContext);
  if (context === undefined) {
    throw new Error(
      "`usePubNub` hook must be used within a `PubNubContextProvider` component",
    );
  }
  return context;
};

export const PubNubContextProvider = (props) => {
  const { children } = props;
  const storagePubnub = JSON.parse(localStorage.getItem("sessionPubnub") ?? "");
  const pubnub = new PubNub({
    publishKey: storagePubnub.publishKey,
    subscribeKey: storagePubnub.subscribeKey,
    uuid: storagePubnub.uuid,
  });


  return (
    <PubNubContext.Provider
      value={{
        pubnub,
      }}
    >
      {children}
    </PubNubContext.Provider>
  );
};
